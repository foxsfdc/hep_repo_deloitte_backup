<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Id</fullName>
        <description>Update unique id with the combination of &quot;PromotionId&quot;/&quot;RegionId&quot;/&quot;DateMatrixId&quot;/&quot;Customer Id&quot;/&quot;LanguageId&quot;/&quot;CatalogId&quot;</description>
        <field>Unique_Id__c</field>
        <formula>Territory__r.Name + &apos; / &apos; +
 HEP_Catalog__r.Unique_Id__c + &apos; / &apos; +
 Promotion__r.Unique_Id__c + &apos; / &apos; +
 DateType__c + &apos; / &apos; +
 Language__r.Name + &apos; / &apos; +
 Customer__r.CustomerName__c</formula>
        <name>Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Unique Id Dating</fullName>
        <actions>
            <name>Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
