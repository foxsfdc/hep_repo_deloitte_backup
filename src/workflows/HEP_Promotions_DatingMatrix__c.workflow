<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Dating_Matrix_Unique_Id</fullName>
        <description>Date Type / Promotion Territory / Region / Line Of Business Type / Media Type</description>
        <field>Unique_Id__c</field>
        <formula>Text(Date_Type__c) + &apos; / &apos; + Promotion_Territory__r.Name + &apos; / &apos; + Region__r.Name + &apos; / &apos; + Text(Media_Type__c) + &apos; / &apos; + Text(Line_of_Business_Type__c)+ &apos; / &apos; + HEP_Language__r.Name</formula>
        <name>HEP Update Dating Matrix Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Dating Matrix Unique Id</fullName>
        <actions>
            <name>HEP_Update_Dating_Matrix_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotions_DatingMatrix__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Workflow updates unique Id of HEP Dating Matrix with the below Combination.
Date Type / Promotion Territory / Region / Line Of Business Type / Media Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
