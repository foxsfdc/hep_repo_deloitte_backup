<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Local_Promotion_Code</fullName>
        <field>LocalPromotionCode__c</field>
        <formula>RIGHT( Name ,  LEN(Name) - 3 )</formula>
        <name>HEP Update Local Promotion Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HEP_Update_Promotion_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>IF(ISBLANK(Legacy_Id__c), RIGHT( Name , LEN(Name) - 3 ), Legacy_Id__c)</formula>
        <name>HEP Update Promotion Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Promotion_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>IF(ISBLANK(Legacy_Id__c), RIGHT( Name , LEN(Name) - 3 ), Legacy_Id__c)</formula>
        <name>Update Promotion Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Local Promotion Code</fullName>
        <actions>
            <name>HEP_Update_Local_Promotion_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion__c.LocalPromotionCode__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HEP Update Promotion Unique Id</fullName>
        <actions>
            <name>Update_Promotion_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Unique Id of Promotion</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
