<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_SKUTemplateDetails_Update_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>HEP_SKU_Template__r.Unique_Id__c + &apos; / &apos; +
SKU_Title__c +&apos; / &apos; + 
SKU_Title__c + &apos; / &apos; +
TEXT(Channel__c) + &apos; / &apos; +
TEXT(Format__c)</formula>
        <name>HEP SKUTemplateDetails Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Template_Details_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>HEP_SKU_Template__r.Unique_Id__c + &apos; / &apos; + SKU_Title__c + &apos; / &apos; + &apos; / &apos; +  Territory__r.LegacyId__c</formula>
        <name>Update Template Details Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP SKU Template Details Unique Id</fullName>
        <actions>
            <name>HEP_SKUTemplateDetails_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Template_Details__c.Name</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Template Details Unique Id</fullName>
        <actions>
            <name>Update_Template_Details_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Template_Details__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
