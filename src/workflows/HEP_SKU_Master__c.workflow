<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_SKU_Master_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>IF(ISBLANK( SKU_Number__c ) , 
    Name + &apos; / &apos; +  Territory__r.Name ,
    SKU_Number__c  + &apos; / &apos; +  Territory__r.Name
)</formula>
        <name>HEP Update SKU Master Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update SKU Master Unique Id</fullName>
        <actions>
            <name>HEP_Update_SKU_Master_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Master__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>SKU Number+ Territory
If SKU Number is blank Name will be used into Auto Number)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
