<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Interface_Update_Unique_ID</fullName>
        <description>Updates the unique Id on Interface record with Integration name</description>
        <field>Unique_Id__c</field>
        <formula>Integration_Name__c</formula>
        <name>HEP Interface Update Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Interface Unique ID</fullName>
        <actions>
            <name>HEP_Interface_Update_Unique_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Interface__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>Workflow Rule to Update Unique id on HEP Approval Type Object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
