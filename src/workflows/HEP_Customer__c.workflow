<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Customer_Update_Unique_ID</fullName>
        <field>Unique_Id__c</field>
        <formula>CustomerName__c + &apos;/&apos; +  Customer_Number__c + &apos;/&apos; +   Territory__r.LegacyId__c+&apos;/&apos;+Text(Customer_Type__c)</formula>
        <name>HEP Customer Update Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Customer Unique ID</fullName>
        <actions>
            <name>HEP_Customer_Update_Unique_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Customer__c.CustomerName__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
