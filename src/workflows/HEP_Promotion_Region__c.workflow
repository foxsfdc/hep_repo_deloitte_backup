<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Promotion_Region_Update_Unique_Id</fullName>
        <description>Field Update : Unique Id</description>
        <field>Unique_Id__c</field>
        <formula>Parent_Promotion__r.Unique_Id__c+ &apos; / &apos; +
  Territory__r.Name</formula>
        <name>HEP Promotion Region Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Promotion Region%09Unique Id</fullName>
        <actions>
            <name>HEP_Promotion_Region_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion__c.Name</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Workflow Rule for Updating Unique Id on Promotion Regions</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
