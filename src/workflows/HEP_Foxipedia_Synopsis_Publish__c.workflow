<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Foxipedia_Synopsis_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>WPR_ID__c + &apos; / &apos; +  Media_Code__c + &apos; / &apos; + Synopsis_Type__c</formula>
        <name>Update Foxipedia Synopsis Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Foxipedia Synopsis Unique Id</fullName>
        <actions>
            <name>Update_Foxipedia_Synopsis_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Foxipedia_Synopsis_Publish__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
