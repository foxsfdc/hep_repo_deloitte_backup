<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Language_Territory_Update_Unique_ID</fullName>
        <field>Unique_Id__c</field>
        <formula>Language__r.Name + &apos;/&apos; +  Territory__r.LegacyId__c</formula>
        <name>HEP Language Territory Update Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Language Territory Unique ID</fullName>
        <actions>
            <name>HEP_Language_Territory_Update_Unique_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Language__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
