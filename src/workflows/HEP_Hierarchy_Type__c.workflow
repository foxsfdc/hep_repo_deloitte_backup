<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Hierarchy_Type_Update_Unique_Id</fullName>
        <description>Field Update on Unique id with Format :  Hierarchy Name + &apos; / &apos; + Territory Name</description>
        <field>UniqueId__c</field>
        <formula>Name + &apos; / &apos; +  Territory__r.Name</formula>
        <name>HEP Hierarchy Type Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Hierarchy Type Unique Id</fullName>
        <actions>
            <name>HEP_Hierarchy_Type_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Hierarchy_Type__c.Name</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Workflow Rule to Update Unique id on HEP Hierarchy Type Object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
