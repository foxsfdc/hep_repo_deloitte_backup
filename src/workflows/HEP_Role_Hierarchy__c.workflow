<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Role_Hierarchy_Legacy_Id</fullName>
        <field>Legacy_Id__c</field>
        <formula>Hierarchy_Type__r.Name + &apos; / &apos; + Manager_Role__r.LegacyId__c + &apos; / &apos; + User_Role__r.Name</formula>
        <name>Update Role Hierarchy Legacy Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Role Hierarchy Legacy Id</fullName>
        <actions>
            <name>Update_Role_Hierarchy_Legacy_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Role_Hierarchy__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
