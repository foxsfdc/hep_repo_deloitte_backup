<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_SKU_Template_Price_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>SKU_Template_Details__r.HEP_SKU_Template__r.Name  + &apos; / &apos; + SKU_Template_Details__r.Name + &apos; / &apos; +  Price_Grade__r.Name + &apos; / &apos; + Region__r.Name</formula>
        <name>HEP Update SKU Template Price Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update SKU Template Price Unique Id</fullName>
        <actions>
            <name>HEP_Update_SKU_Template_Price_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Template_Details_Price__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
