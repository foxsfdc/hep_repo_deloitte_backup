<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_SKU_Template_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>Name + &apos; / &apos; + Territory__r.LegacyId__c + &apos; / &apos; +  Line_Of_Business__r.LegacyId__c</formula>
        <name>Update SKU Template Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update SKU Template Unique Id</fullName>
        <actions>
            <name>Update_SKU_Template_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Template__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
