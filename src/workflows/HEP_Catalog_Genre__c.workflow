<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Catalog_Genre_Unique_Id</fullName>
        <description>Update the Unique Id field on Catalog Genre:
Logic : Catalog Unique Id+&apos; / &apos;+ Genre__c</description>
        <field>Unique_Id__c</field>
        <formula>Catalog__r.Unique_Id__c + &apos; / &apos; +  TEXT(Genre__c)</formula>
        <name>HEP Update Catalog Genre Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Catalog Genre Unique Id</fullName>
        <actions>
            <name>HEP_Update_Catalog_Genre_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Catalog_Genre__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
