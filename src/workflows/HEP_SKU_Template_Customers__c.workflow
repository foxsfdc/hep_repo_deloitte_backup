<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_SKU_Template_Customer_Unique_ID</fullName>
        <field>Unique_Id__c</field>
        <formula>Customer__r.Unique_Id__c  + &apos; / &apos; + HEP_SKU_Template_Details__r.Unique_Id__c</formula>
        <name>Update SKU Template Customer Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HEP SKU Template Customers Update Unique ID</fullName>
        <actions>
            <name>Update_SKU_Template_Customer_Unique_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Template_Customers__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
