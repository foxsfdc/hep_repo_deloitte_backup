<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Populate_Promotion_SKU_Release_Id</fullName>
        <description>Populates Release Id field with Name if the release id is blank</description>
        <field>Release_Id__c</field>
        <formula>MID(Name, 4, 8)</formula>
        <name>HEP Populate Promotion SKU Release Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HEP_Update_Promotion_SKU_Unique_Id</fullName>
        <description>Promotion Catalog Id + SKU Master</description>
        <field>Unique_Id__c</field>
        <formula>Promotion_Catalog__r.Promotion__r.Unique_Id__c+ &apos; / &apos; +  SKU_Master__r.Unique_Id__c</formula>
        <name>HEP Update Promotion SKU Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Populate Promotion SKU Release Id</fullName>
        <actions>
            <name>HEP_Populate_Promotion_SKU_Release_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion_SKU__c.Release_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>update the custom object with the name field if the releaseid field is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HEP Update Promotion SKU Unique Id</fullName>
        <actions>
            <name>HEP_Update_Promotion_SKU_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion_SKU__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
