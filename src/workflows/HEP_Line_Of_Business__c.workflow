<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Line_Of_Business_Update_Unique_ID</fullName>
        <field>Unique_Id__c</field>
        <formula>Name + &apos;/&apos; +  TEXT( Type__c )</formula>
        <name>HEP Line Of Business Update Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Line Of Business Unique ID</fullName>
        <actions>
            <name>HEP_Line_Of_Business_Update_Unique_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Line_Of_Business__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
