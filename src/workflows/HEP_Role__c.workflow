<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Role_Update_Legacy_ID</fullName>
        <description>HEP; abielangovan@deloitte.com; Update Legacy ID with name</description>
        <field>LegacyId__c</field>
        <formula>Name</formula>
        <name>HEP Role Update Legacy ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Role Legacy ID</fullName>
        <actions>
            <name>HEP_Role_Update_Legacy_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Role__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
