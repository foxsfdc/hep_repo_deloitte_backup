<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Id</fullName>
        <description>&quot;CatalogId+ Territory (Catalog Id will be same for local/global catalog)
If Catalog Id is blank Name(Auto Number) will be used&quot;</description>
        <field>Unique_Id__c</field>
        <formula>IF(ISBLANK(CatalogId__c),Name + &apos; / &apos; + Territory__r.Name, CatalogId__c + &apos; / &apos; + Territory__r.Name)</formula>
        <name>Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Catalog Unique Id</fullName>
        <actions>
            <name>Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Catalog__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>&quot;CatalogId+ Territory (Catalog Id will be same for local/global catalog)
If Catalog Id is blank Name will be used which is Auto Number)&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique Id</fullName>
        <active>false</active>
        <criteriaItems>
            <field>HEP_Catalog__c.Catalog_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
