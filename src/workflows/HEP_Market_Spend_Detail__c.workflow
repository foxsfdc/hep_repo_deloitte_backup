<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Market_Spend_Detail_Update_Unique_Id</fullName>
        <description>Unique Id updation</description>
        <field>Unique_Id__c</field>
        <formula>IF(TEXT(Type__c) == &apos;Current&apos;,
    HEP_Market_Spend__r.Unique_Id__c + &apos; / &apos; +
    GL_Account__r.Name + &apos; / &apos; +
    TEXT(Type__c),
    HEP_Market_Spend__r.Unique_Id__c + &apos; / &apos; +
    GL_Account__r.Name + &apos; / &apos; +
    TEXT(Type__c) + &apos; / &apos; +
    Name
)</formula>
        <name>HEP Market Spend Detail Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Market Spend Detail Unique Id</fullName>
        <actions>
            <name>HEP_Market_Spend_Detail_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Market_Spend__c.Name</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
