<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Approval_Type_Update_Unique_Id</fullName>
        <description>Field Update on Unique id with Format : HEP Approval Type Name</description>
        <field>UniqueId__c</field>
        <formula>Name</formula>
        <name>HEP Approval Type Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Approval Type Unique Id</fullName>
        <actions>
            <name>HEP_Approval_Type_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Approval_Type__c.Approval_Field__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Workflow Rule to Update Unique id on HEP Approval Type Object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
