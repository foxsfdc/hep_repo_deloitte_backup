<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_SKU_Price_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>IF(ISBLANK(Promotion_SKU__c),Region__r.Name + &apos; / &apos; + SKU_Master__r.Unique_Id__c + &apos; / &apos; + TEXT(Start_Date__c), Region__r.Name + &apos; / &apos; + Promotion_SKU__r.Unique_Id__c)</formula>
        <name>HEP Update SKU Price Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update SKU Price Unique Id</fullName>
        <actions>
            <name>HEP_Update_SKU_Price_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Price__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
