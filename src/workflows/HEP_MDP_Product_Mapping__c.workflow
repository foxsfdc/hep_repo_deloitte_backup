<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Product_Mapping_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>HEP_Customer__r.CustomerName__c + &apos; / &apos; + Text(Channel__c) + &apos; / &apos; + Text(Format__c) + &apos; / &apos; + Adam_Id__c + &apos; / &apos; + Google_Id__c + &apos; / &apos; + ASIN__c + &apos; / &apos; + Big_Id__c + &apos; / &apos; + Media_Id__c + &apos; / &apos; + HEP_Territory__r.Name</formula>
        <name>HEP Product Mapping Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Product Mapping Unique Id</fullName>
        <actions>
            <name>HEP_Product_Mapping_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_MDP_Product_Mapping__c.Name__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>This will be the combination of different per different customers</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
