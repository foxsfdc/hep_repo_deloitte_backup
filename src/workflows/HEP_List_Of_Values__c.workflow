<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_List_Of_Values_Update_Unique_Id</fullName>
        <description>Field Update for Unique Id Field in LOV with Format :Parent_Value__c + Type__c + Values__c + Name</description>
        <field>UniqueId__c</field>
        <formula>LEFT((Parent_Value__c + &apos; / &apos; +  Type__c + &apos; / &apos; +  Name + &apos; / &apos; +  Values__c), 255)</formula>
        <name>HEP List Of Values	Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP List Of Values%09Unique Id</fullName>
        <actions>
            <name>HEP_List_Of_Values_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_List_Of_Values__c.Type__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Updation of Unique Id in HEP List Of Values	object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
