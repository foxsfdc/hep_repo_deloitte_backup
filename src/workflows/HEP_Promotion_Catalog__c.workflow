<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Product_Catalog_Unique_Id</fullName>
        <description>Promotion Id + Catalog Master Id</description>
        <field>Unique_Id__c</field>
        <formula>Promotion__r.Unique_Id__c+ &apos; / &apos; + Catalog__r.Unique_Id__c</formula>
        <name>HEP Update Product Catalog Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_Id</fullName>
        <description>Promotion__c /  Territory__c /  HEP_Promotions_Dating_Matrix__c / Customer__c / Language__c / HEP_Catalog__c</description>
        <field>Unique_Id__c</field>
        <formula>CASESAFEID(Promotion__c) + &apos; / &apos; +  
CASESAFEID(Catalog__c)</formula>
        <name>Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>field_update</fullName>
        <field>Comments__c</field>
        <formula>Promotion__c</formula>
        <name>field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Catalog__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Promotion Catalog Unique Id</fullName>
        <actions>
            <name>HEP_Update_Product_Catalog_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Promotion_Catalog__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
