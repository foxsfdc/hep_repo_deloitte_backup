<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Product_Detail_Unique_Id</fullName>
        <description>Product Id + Channel + Format</description>
        <field>HEP_Unique_Id__c</field>
        <formula>CASESAFEID(HEP_Promotion_MDP_Product__r.Id) + &apos; / &apos; +  Text(Channel__c) + &apos; / &apos; + Text(Format__c)</formula>
        <name>HEP Update Product Detail Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update MDP Promotion Product Detail Unique Id</fullName>
        <actions>
            <name>HEP_Update_Product_Detail_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>HEP_MDP_Promotion_Product_Detail__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
