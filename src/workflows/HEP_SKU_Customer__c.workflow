<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_SKU_Customer_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>SKU_Master__r.Unique_Id__c + &apos; / &apos; +  Customer__r.Unique_Id__c</formula>
        <name>HEP Update SKU Customer Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update SKU Customer Unique Id</fullName>
        <actions>
            <name>HEP_Update_SKU_Customer_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_SKU_Customer__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Customer Id + SKU Master</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
