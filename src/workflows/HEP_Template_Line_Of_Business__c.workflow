<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Template_LOB_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>Line_Of_Business__r.Unique_Id__c + &apos; / &apos; +  Template__r.Unique_Id__c</formula>
        <name>Update Template LOB Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Template LOB Unique Id</fullName>
        <actions>
            <name>Update_Template_LOB_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Template_Line_Of_Business__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
