<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Spend_Template_Details_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>Market_Spend_Template__r.Unique_Id__c + &apos; / &apos; +  Department__c</formula>
        <name>Update Spend Template Details Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Spend Template Details Unique Id</fullName>
        <actions>
            <name>Update_Spend_Template_Details_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Market_Spend_Template_Detail__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
