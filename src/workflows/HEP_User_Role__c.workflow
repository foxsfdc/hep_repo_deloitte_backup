<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Search_Id</fullName>
        <description>Concatenation Of:  Role Id+&apos; / &apos;+Territory Id</description>
        <field>Search_Id__c</field>
        <formula>CASESAFEID(Role__c) + &apos; / &apos; + CASESAFEID(Territory__c)</formula>
        <name>Update Search Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_Id</fullName>
        <description>User + Role + Territory</description>
        <field>Unique_Id__c</field>
        <formula>User__r.Username  + &apos; / &apos; +  Role__r.Name + &apos; / &apos; +  Territory__r.Name</formula>
        <name>Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update Unique Id</fullName>
        <actions>
            <name>Update_Search_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_User_Role__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
