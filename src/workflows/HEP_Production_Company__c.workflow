<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Production_Company_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>Title__r.FOX_ID__c&amp; &apos; / &apos; &amp;  Production_Company_Code__c</formula>
        <name>HEP Production Company Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Production Company Unique Id</fullName>
        <actions>
            <name>HEP_Production_Company_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow updates the unique ID field on HEP Production Company object. Combination is Title Fox Id + &apos;-&apos; + Production Company Code.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
