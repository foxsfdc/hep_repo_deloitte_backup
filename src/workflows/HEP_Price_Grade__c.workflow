<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Price_Grade_UniqueId_Update</fullName>
        <description>Update unique Id on insert and update of a Price Grade record</description>
        <field>Unique_Id__c</field>
        <formula>LEFT((TEXT(Type__c )+&apos;/&apos;+Territory__r.Name+&apos;/&apos;+ Channel__c +&apos;/&apos;+  Format__c+&apos;/&apos;+ PriceGrade__c +&apos;/&apos;+ MDP_Business_Unit__c +&apos;/&apos;+ MDP_Product_Type__c +&apos;/&apos;+ MDP_Product_Sub_Type__c +&apos;/&apos;+ MDP_Duration_Start__c +&apos;/&apos;+ MDP_Duration_End__c +&apos;/&apos;+ MDP_PromoWSP__c +&apos;/&apos;+ MM_RateCard_ID__c), 255)</formula>
        <name>HEP Price Grade UniqueId Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Price Grade UniqueId Update</fullName>
        <actions>
            <name>HEP_Price_Grade_UniqueId_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Price_Grade__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
