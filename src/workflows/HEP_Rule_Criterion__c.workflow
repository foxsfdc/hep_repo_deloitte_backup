<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Rule_Criteria_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>HEP_Rule__r.HEP_External_Id__c + &apos; / &apos; +  HEP_Source_SObject_Field__c + &apos; / &apos; + HEP_Value_To_Compare__c</formula>
        <name>Update Rule Criteria Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Rule Criteria Unique Id</fullName>
        <actions>
            <name>Update_Rule_Criteria_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Rule_Criterion__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
