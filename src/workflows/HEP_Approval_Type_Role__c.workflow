<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Approval_Type_Role_Update_Unique_Id</fullName>
        <description>Field Update on Unique id with Format : Approval Type Role Name + &apos; / &apos; + Role Name + &apos; / &apos; + Rule Name</description>
        <field>UniqueId__c</field>
        <formula>Name + &apos; / &apos; +  Approver_Role__r.Name + &apos; / &apos; +  Rule__r.Name</formula>
        <name>HEP Approval Type Role Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Approval Type Role Unique Id</fullName>
        <actions>
            <name>HEP_Approval_Type_Role_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Approval_Type_Role__c.Approval_Routing_Type__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Workflow Rule to Update Unique id on HEP Approval Type Role Object</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
