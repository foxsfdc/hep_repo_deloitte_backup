<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Market_Spend_Update_Unique_Id</fullName>
        <field>Unique_Id__c</field>
        <formula>Promotion__r.Unique_Id__c</formula>
        <name>HEP Market Spend	 Update Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Market Spend%09 Unique Id</fullName>
        <actions>
            <name>HEP_Market_Spend_Update_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_Market_Spend__c.Name</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
