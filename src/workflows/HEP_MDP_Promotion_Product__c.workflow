<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>HEP_Update_Promotion_Product_Unique_Id</fullName>
        <description>Promotion Id +Region Name + WPR + Catalog Number + Local Description</description>
        <field>HEP_Unique_Id__c</field>
        <formula>IF(ISPICKVAL( HEP_Promotion__r.Promotion_Type__c ,&apos;National&apos;),CASESAFEID(HEP_Promotion__r.Id) + &apos; / &apos; + HEP_Territory__r.Name + &apos; / &apos; + Title_EDM__r.FIN_PROD_ID__c + &apos; / &apos; + HEP_Catalog__r.UI_Catalog_Number__c + &apos; / &apos; 
+ Local_Description__c + &apos; / &apos; + Id,IF(AND((ISNULL(Product_Start_Date__c)),
(ISNULL( Product_End_Date__c) ) ,
(ISBLANK(Product_Start_Date__c)),(ISBLANK(Product_End_Date__c))), 
CASESAFEID(HEP_Promotion__r.Id) + &apos; / &apos; + HEP_Territory__r.Name + &apos; / &apos; + Title_EDM__r.FIN_PROD_ID__c + &apos; / &apos; + HEP_Catalog__r.UI_Catalog_Number__c + &apos; / &apos; 
+ Local_Description__c + &apos; / &apos; + Id , IF(OR(ISPICKVAL(Record_Status__c,&apos;Deleted&apos;) ,ISPICKVAL(Record_Status__c,&apos;Inactive&apos;) ),HEP_Unique_Id__c+ &apos; / &apos;+
Text(Record_Status__c)+&apos; / &apos;+Id,CASESAFEID(HEP_Promotion__r.Id) + &apos; / &apos; + HEP_Territory__r.Name + &apos; / &apos; + Title_EDM__r.FIN_PROD_ID__c + &apos; / &apos; +
 HEP_Catalog__r.UI_Catalog_Number__c + &apos; / &apos; + Local_Description__c + &apos; / &apos; + (TEXT(MONTH(Product_Start_Date__c))+&quot;/&quot; +TEXT(DAY(Product_Start_Date__c))+
 &quot;/&quot; +TEXT(YEAR(Product_Start_Date__c))) + &apos; / &apos; + 
(TEXT(MONTH(Product_End_Date__c))+&quot;/&quot; +TEXT(DAY(Product_End_Date__c))+&quot;/&quot; +TEXT(YEAR(Product_End_Date__c))))))</formula>
        <name>HEP Update Promotion Product Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>HEP Update MDP Promotion Product Unique Id</fullName>
        <actions>
            <name>HEP_Update_Promotion_Product_Unique_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HEP_MDP_Promotion_Product__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
