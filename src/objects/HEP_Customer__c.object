<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>HEP;Object to store customer detail;anshuljain787@deloitte.com;01/22/2018</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Channel__c</fullName>
        <externalId>false</externalId>
        <label>Channel</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>VOD</fullName>
                    <default>false</default>
                    <label>VOD</label>
                </value>
                <value>
                    <fullName>EST</fullName>
                    <default>false</default>
                    <label>EST</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CustomerName__c</fullName>
        <description>HEP;Field to store customer name;anshuljain787@deloitte.com;01/22/2018</description>
        <externalId>false</externalId>
        <label>Customer Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_Number__c</fullName>
        <externalId>true</externalId>
        <label>Customer Number</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_Status__c</fullName>
        <externalId>false</externalId>
        <label>Customer Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                    <label>Submitted</label>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                    <label>Approved</label>
                </value>
                <value>
                    <fullName>Live</fullName>
                    <default>false</default>
                    <label>Live</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Customer_Type__c</fullName>
        <externalId>false</externalId>
        <label>Customer Type</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>SKU Customers</fullName>
                    <default>false</default>
                    <label>SKU Customers</label>
                </value>
                <value>
                    <fullName>MDP Customers</fullName>
                    <default>false</default>
                    <label>MDP Customers</label>
                </value>
                <value>
                    <fullName>Dating Customers</fullName>
                    <default>false</default>
                    <label>Dating Customers</label>
                </value>
                <value>
                    <fullName>MDP Child Customers</fullName>
                    <default>false</default>
                    <label>MDP Child Customers</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Date_Type__c</fullName>
        <description>HEP; field to store the date type to which the customer is associated to; abielangovan@deloitte.com; 5/10/2018</description>
        <externalId>false</externalId>
        <label>Date Type</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LegacyId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>LegacyId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Parent_Customer_Formula__c</fullName>
        <description>Create for data load of MDP planner customer</description>
        <externalId>false</externalId>
        <formula>CustomerName__c + Territory__r.Name +Text( Customer_Type__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Parent Customer Formula</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent_Customer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Parent Customer</label>
        <referenceTo>HEP_Customer__c</referenceTo>
        <relationshipLabel>HEP Customers</relationshipLabel>
        <relationshipName>HEP_Customers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Record_Status__c</fullName>
        <description>HEP; Field to hold active records for an object; gmehrishi@deloitte.com;03/04/2018</description>
        <externalId>false</externalId>
        <label>Record Status</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                    <label>Active</label>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Territory__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>HEP;Lookup to territory object;anshuljain787@deloitte.com;01/22/2018</description>
        <externalId>false</externalId>
        <label>Territory</label>
        <referenceTo>HEP_Territory__c</referenceTo>
        <relationshipLabel>HEP Customers</relationshipLabel>
        <relationshipName>HEP_Customers</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Unique_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>HEP;Stores unique id as unique record identifier from an external system;anshuljain787@deloitte.com;01/22/2018</description>
        <externalId>true</externalId>
        <label>Unique Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>HEP Customer</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Customer_Number__c</columns>
        <columns>CustomerName__c</columns>
        <columns>Territory__c</columns>
        <columns>Customer_Type__c</columns>
        <columns>Record_Status__c</columns>
        <columns>Unique_Id__c</columns>
        <columns>Date_Type__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>SKU_Customers</fullName>
        <columns>NAME</columns>
        <columns>CustomerName__c</columns>
        <columns>Customer_Number__c</columns>
        <columns>Customer_Status__c</columns>
        <columns>Record_Status__c</columns>
        <columns>Territory__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Customer_Type__c</field>
            <operation>equals</operation>
            <value>SKU Customers</value>
        </filters>
        <label>SKU Customers</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>C-{0000000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>HEP Customers</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
