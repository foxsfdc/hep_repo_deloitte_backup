<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Catalog_Number__c</fullName>
        <externalId>false</externalId>
        <label>Catalog Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Country_Code__c</fullName>
        <externalId>false</externalId>
        <label>Country Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FOX_ID__c</fullName>
        <description>Fox_Id of the Title EDM record of the HEP Catalog</description>
        <externalId>false</externalId>
        <label>FOX ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fox_Version_ID__c</fullName>
        <description>Fox Version Id of the Title EDM record of the HEP Catalog</description>
        <externalId>false</externalId>
        <label>Fox Version ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HEP_Catalog__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to HEP_Catalog for which the Metadata is being published</description>
        <externalId>false</externalId>
        <label>HEP Catalog</label>
        <referenceTo>HEP_Catalog__c</referenceTo>
        <relationshipLabel>HEP Foxipedia Synopsis Publish</relationshipLabel>
        <relationshipName>HEP_Foxipedia_Synopsis_Publish</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Language_Code__c</fullName>
        <externalId>false</externalId>
        <label>Language Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Media_Code__c</fullName>
        <externalId>false</externalId>
        <label>Media Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Synopsis_Row_Id__c</fullName>
        <externalId>false</externalId>
        <label>Synopsis Row Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Synopsis_Text__c</fullName>
        <externalId>false</externalId>
        <label>Synopsis Text</label>
        <length>1600</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Synopsis_Type__c</fullName>
        <externalId>false</externalId>
        <label>Synopsis Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unique_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>WPR_ID__c + Media_Code__c + Synopsis_Type__c</description>
        <externalId>true</externalId>
        <label>Unique Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Version_Type__c</fullName>
        <externalId>false</externalId>
        <label>Version Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>WPR_ID__c</fullName>
        <description>WPR_Id of the Title EDM record  of the HEP Catalog</description>
        <externalId>false</externalId>
        <label>WPR ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>HEP Foxipedia Synopsis Publish</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>HEP Foxipedia Synopsis Publish Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>HEP Foxipedia Synopsis Publish</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
