<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>HEP;Used to capture the exceptions encountered by users;uveliventi;21/02/2018</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Error_Type__c</fullName>
        <description>HEP;used to determine the type of error that occured;uveliventi;21-02-2018</description>
        <externalId>false</externalId>
        <label>Error Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Apex Trigger</fullName>
                    <default>false</default>
                    <label>Apex Trigger</label>
                </value>
                <value>
                    <fullName>Business Validation</fullName>
                    <default>false</default>
                    <label>Business Validation</label>
                </value>
                <value>
                    <fullName>Component</fullName>
                    <default>false</default>
                    <label>Component</label>
                </value>
                <value>
                    <fullName>DML Errors</fullName>
                    <default>false</default>
                    <label>DML Errors</label>
                </value>
                <value>
                    <fullName>Integration Batch Job</fullName>
                    <default>false</default>
                    <label>Integration Batch Job</label>
                </value>
                <value>
                    <fullName>Integration Inbound</fullName>
                    <default>false</default>
                    <label>Integration Inbound</label>
                </value>
                <value>
                    <fullName>Integration Outbound</fullName>
                    <default>false</default>
                    <label>Integration Outbound</label>
                </value>
                <value>
                    <fullName>Javascript</fullName>
                    <default>false</default>
                    <label>Javascript</label>
                </value>
                <value>
                    <fullName>VF Controller</fullName>
                    <default>false</default>
                    <label>VF Controller</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Exception_Type__c</fullName>
        <description>HEP;Used to represent the exception type;uveliventi;21-02-2018</description>
        <externalId>false</externalId>
        <label>Exception Type</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HEP_Class_Method__c</fullName>
        <description>HEP;used to represent the method from which exception occured;uveliventi;21-02-2018</description>
        <externalId>false</externalId>
        <label>HEP Class Method</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HEP_Class_Name__c</fullName>
        <description>HEP;Used to represent the class where exception occured;uveliventi;21-2-2018</description>
        <externalId>false</externalId>
        <label>HEP Class Name</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HEP_Error_Message__c</fullName>
        <description>HEP;Used to hold the error message;uveliventi;21-02-2018</description>
        <externalId>false</externalId>
        <label>HEP Error Message</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HEP_Object_Id__c</fullName>
        <description>HEP;Used to hold the Object details (SFDC/External id);uveliventi;21-2-2018</description>
        <externalId>false</externalId>
        <label>HEP Object Id</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Message__c</fullName>
        <description>HEP;Used to hold the stack trace of the error;uveliventi;21-2-2018</description>
        <externalId>false</externalId>
        <label>Message</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Source_URL__c</fullName>
        <description>HEP;Holds the url details;uveliventi;21-2-2018</description>
        <externalId>false</externalId>
        <label>Source URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Username__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>HEP;lookup to user who triggered the transaction;uveliventi;21-02-2018</description>
        <externalId>false</externalId>
        <label>Username</label>
        <referenceTo>User</referenceTo>
        <relationshipName>HEP_Error_logs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>HEP Error Log</label>
    <listViews>
        <fullName>ALL_errors</fullName>
        <columns>NAME</columns>
        <columns>Error_Type__c</columns>
        <columns>Exception_Type__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>ALL errors</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>HEP_Class_Method__c</columns>
        <columns>HEP_Class_Name__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Approval_Fw_errors</fullName>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5</booleanFilter>
        <columns>NAME</columns>
        <columns>HEP_Class_Name__c</columns>
        <columns>HEP_Class_Method__c</columns>
        <columns>HEP_Error_Message__c</columns>
        <columns>Exception_Type__c</columns>
        <columns>Error_Type__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>HEP_Class_Name__c</field>
            <operation>equals</operation>
            <value>HEP_Approval_Utility</value>
        </filters>
        <filters>
            <field>HEP_Class_Name__c</field>
            <operation>equals</operation>
            <value>HEP_ApprovalProcess</value>
        </filters>
        <filters>
            <field>HEP_Class_Name__c</field>
            <operation>equals</operation>
            <value>HEP_Queue_Email_Alerts</value>
        </filters>
        <filters>
            <field>HEP_Class_Name__c</field>
            <operation>equals</operation>
            <value>HEP_Record_Approver_TriggerHandler</value>
        </filters>
        <filters>
            <field>HEP_Class_Name__c</field>
            <operation>equals</operation>
            <value>HEP_Approval_Component_Controller</value>
        </filters>
        <label>Approval Fw errors</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Catalog_Errors</fullName>
        <columns>NAME</columns>
        <columns>Exception_Type__c</columns>
        <columns>HEP_Class_Method__c</columns>
        <columns>HEP_Error_Message__c</columns>
        <columns>HEP_Object_Id__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>HEP_Class_Name__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>equals</operation>
            <value>Coline Feuillette</value>
        </filters>
        <label>Catalog Errors</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>HEP Error Log Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>HEP Error logs</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>OWNER.ALIAS</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
