trigger HEP_Promotion_Catalog_Trigger on HEP_Promotion_Catalog__c(after Update, after insert) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_Catalog_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_Catalog_Trigger') == 'true') {
                    System.debug('---------Entering Trigger Now ---------');
                    new HEP_Promotion_Catalog_TriggerHandler().run();             
            }
        }
    }
}