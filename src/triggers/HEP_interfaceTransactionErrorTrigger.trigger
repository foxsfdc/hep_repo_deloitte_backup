/************************************Fox HEP**********************************
This trigger is for Interface transaction record and is called whenever an error
occurs in a interface transaction during the inteface callouts.
------------------------------------------------------------------------------
Name:              HEP_interfaceTransactionErrorTrigger
Description:       Interface transaction Error Trigger
Created By:        Abhishek Mishra
Created On:        20-06-2018
Version:           1.0

-------------------------------------------------------------------------------
                    Change Log:
-------------------------------------------------------------------------------
Date            Modified By         LOC changed     

******************************************************************************/
trigger HEP_interfaceTransactionErrorTrigger on HEP_Interface_Transaction_Error__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //All trigger before context 
    if(trigger.isBefore){
        if(trigger.isInsert){}
        if(trigger.isUpdate){}
        if(trigger.isDelete){}
    }
    //All trigger after context
    if(trigger.isAfter){
        if(trigger.isInsert){
            if(HEP_Utility.getConstantValue('Enable_HEP_interfaceTxnErrorTrigger') != null)
                {
                    if( HEP_Utility.getConstantValue('Enable_HEP_interfaceTxnErrorTrigger') == 'true')
                        {
                            System.debug('---------Entering Trigger Now ---------');
                            HEP_InterfaceTxnErrorTriggerHandler.handleAfterInsert(trigger.New);
                            HEP_InterfaceTxnErrorTriggerHandler.sendiTunesInterfaceErrorEmail(trigger.New);
                        }
                }
        }
        if(trigger.isUpdate){}
        if(trigger.isDelete){}
        if(trigger.isUndelete){}
    }    
}