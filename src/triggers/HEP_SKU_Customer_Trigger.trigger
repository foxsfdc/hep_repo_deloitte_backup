/**
 * HEP_SKU_Customer_Trigger -- Trigger on HEP_SKU_Customer__c
 * object to run integration with siebel after insert and delete
 * @author    Mayank Jaggi
 */
trigger HEP_SKU_Customer_Trigger on HEP_SKU_Customer__c(after insert, after update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_SKU_Customer_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_SKU_Customer_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_SKU_Customer_TriggerHandler().run();
            }
        }
    }
}