/**
 * HEP_MDP_ProductDetailTrigger --- Trigger for HEP_MDP_Promotion_Product_Detail__c
 * @author    Nidhin VK
 */
trigger HEP_MDP_ProductDetailTrigger on HEP_MDP_Promotion_Product_Detail__c(before insert,after insert, before update, after update) {
   if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_MDP_ProductDetailTrigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_MDP_ProductDetailTrigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_MDP_ProductDetailTriggerHandler().run();
            }
        }
    }
}