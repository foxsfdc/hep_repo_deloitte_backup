/**
 * HEP_SKUMasterTrigger --- Trigger for HEP_SKU_Master__c
 * @author  Nidhin V K
 */
trigger HEP_SKUMasterTrigger on HEP_SKU_Master__c(after insert, after update, before insert, before update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        System.debug('checking the sku master');
        if (HEP_Utility.getConstantValue('Enable_HEP_SKUMasterTrigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_SKUMasterTrigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_SKUMasterTriggerHandler().run();
            }
        }
    }
}