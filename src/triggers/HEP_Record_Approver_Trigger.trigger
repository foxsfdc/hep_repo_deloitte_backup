/**
 *HEP_Record_Approver_Trigger --- Class which houses the utility method to create application alerts
 *@author  Nishit Kedia
 */
trigger HEP_Record_Approver_Trigger on HEP_Record_Approver__c(after insert, after update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        // Instantiate Trigger Handler
        HEP_Record_Approver_TriggerHandler objApprovalRecordHandler = new HEP_Record_Approver_TriggerHandler();

        //Trigger should fire on After Update
        if (Trigger.isAfter) {

            //if trigger is invoked in Insert context
            if (Trigger.isInsert) {
                if (HEP_Utility.getConstantValue('Enable_HEP_Record_Approver_Trigger') != null) {
                    if (HEP_Utility.getConstantValue('Enable_HEP_Record_Approver_Trigger') == 'true') {
                        System.debug('---------Entering Trigger Now ---------');
                        objApprovalRecordHandler.handleAfterInsert(Trigger.new);
                    }
                }
            }
            //if trigger is invoked in Update context
            if (Trigger.isUpdate) {
                if (HEP_Utility.getConstantValue('Enable_HEP_Record_Approver_Trigger') != null) {
                    if (HEP_Utility.getConstantValue('Enable_HEP_Record_Approver_Trigger') == 'true') {
                        System.debug('---------Entering Trigger Now ---------');
                        objApprovalRecordHandler.handleAfterUpdate(Trigger.new);
                    }
                }
            }
        }
    }
}