/**
 * HEP_MarketSpendTrigger --- Trigger for HEP_Market_Spend__c
 * @author  Nidhin V K
 */
trigger HEP_MarketSpendTrigger on HEP_Market_Spend__c(
    after update, after insert) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        //new HEP_MarketSpendTriggerHandler().run();
        if (HEP_Utility.getConstantValue('Enable_HEP_MarketSpendTrigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_MarketSpendTrigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                //System.debug('Inside Spend Trigger Before Call');
                new HEP_MarketSpendTriggerHandler().run();
                //HEP_MarketSpendTriggerHandler.createSpendHistory(Trigger.oldMap, Trigger.newMap);
                System.debug('Inside Spend Trigger After Call');
            }
        }
    }
}