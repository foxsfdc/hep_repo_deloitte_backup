trigger HEP_promotionTrigger on HEP_Promotion__c(after Update, after insert) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_promotionTrigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_promotionTrigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_PromotionTriggerHandler().run();
            }
        }
    }
}