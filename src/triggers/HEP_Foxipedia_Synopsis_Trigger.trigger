trigger HEP_Foxipedia_Synopsis_Trigger on HEP_Foxipedia_Synopsis_Publish__c(before insert, before update, before delete, after insert, after update,
    after delete, after undelete) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_Foxipedia_Synopsis_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_Foxipedia_Synopsis_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_Foxipedia_Synopsis_TriggerHandler().run();
            }
        }
    }
}