trigger HEP_Promotion_Dating_Trigger on HEP_Promotion_Dating__c(before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_Dating_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_Dating_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_Promotion_Dating_TriggerHandler().run();
            }
        }

    }
}