/**
 * HEP_Promotion_SKU_Trigger -- Trigger on HEP_Promotion_SKU__c
 * object to run integration with siebel after insert and update
 * @author    Mayank Jaggi
 */
trigger HEP_Promotion_SKU_Trigger on HEP_Promotion_SKU__c(after insert, before update, after update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_SKU_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_Promotion_SKU_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_Promotion_SKU_TriggerHandler().run();
            }
        }
    }
}