trigger HEP_SKU_Price_Trigger on HEP_SKU_Price__c(after insert, after update, before delete) {
   if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_SKU_Price_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_SKU_Price_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_SKU_Price_TriggerHandler().run();
            }
        }
    }
}