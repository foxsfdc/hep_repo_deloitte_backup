trigger HEP_Catalog_Genre_Trigger on HEP_Catalog_Genre__c(before insert, before update, before delete, after insert, after update,
    after delete, after undelete) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_Catalog_Genre_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_Catalog_Genre_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new Hep_Catalog_Genre_TriggerHandler().run();
            }
        }
    }
}