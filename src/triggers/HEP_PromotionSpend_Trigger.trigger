trigger HEP_PromotionSpend_Trigger on HEP_Market_Spend_Detail__c(before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_PromotionSpend_Trigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_PromotionSpend_Trigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_PromotionSpend_TriggerHander().run();
            }
        }
    }
}