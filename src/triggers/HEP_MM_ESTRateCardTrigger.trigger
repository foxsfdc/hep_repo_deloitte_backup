trigger HEP_MM_ESTRateCardTrigger on HEP_MM_ESTRateCard__c(after Update, after insert) {
    if(HEP_Utility.getConstantValue('Enable_HEP_MM_ESTRateCardTrigger') != null)
        {
            if( HEP_Utility.getConstantValue('Enable_HEP_MM_ESTRateCardTrigger') == 'true')
                {
                    System.debug('---------Entering Trigger Now ---------');
                    new HEP_MM_ESTRateCardTriggerHandler().run();
                }
        }
}