/**
 * HEP_MDP_PromotionProductTrigger --- Trigger for HEP_MDP_Promotion_Product__c
 * @author  Nidhin V K
 */
trigger HEP_MDP_PromotionProductTrigger on HEP_MDP_Promotion_Product__c(after insert, after update,before insert, before update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (HEP_Utility.getConstantValue('Enable_HEP_MDP_PromotionProductTrigger') != null) {
            if (HEP_Utility.getConstantValue('Enable_HEP_MDP_PromotionProductTrigger') == 'true') {
                System.debug('---------Entering Trigger Now ---------');
                new HEP_PromotionProductTriggerHandler().run();
            }
        }
    }
}