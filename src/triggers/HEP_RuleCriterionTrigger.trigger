/* Trigger Name   : HEP_RuleCriterionTrigger
 * Description  :    
 * Created By   : Nidhin V K
 * Created On   : 07-26-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-26-2016        1000            Initial version
 *
 *
 */
trigger HEP_RuleCriterionTrigger on HEP_Rule_Criterion__c(before delete, before insert, before update) {
    if (HEP_CheckRecursive.runOnce(Trigger.oldMap, Trigger.newMap, Trigger.isBefore)) {
        if (trigger.isBefore) {
            if (trigger.isDelete) {
                /*Call HEP_RuleCriterionTriggerHandler.beforeDelete to check the following:
                 *Prevent users from deleting rule criteria if rule or rule version is active
                 */
                if (HEP_Utility.getConstantValue('Enable_HEP_RuleCriterionTrigger') != null) {
                    if (HEP_Utility.getConstantValue('Enable_HEP_RuleCriterionTrigger') == 'true') {
                        System.debug('---------Entering Trigger Now ---------');
                        HEP_RuleCriterionTriggerHandler.beforeDelete(trigger.oldMap);
                    }
                }
            }
        }
    }
}