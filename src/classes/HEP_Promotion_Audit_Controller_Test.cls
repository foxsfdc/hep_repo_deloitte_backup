//Test class for  HEP_Promotion_Audit_Controller
@isTest
public class HEP_Promotion_Audit_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
    }
    static testMethod void createData() {
        //insert Territory
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Territory__c objAustria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '890', 'JDE', true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true);
        //Create PRoduct Type Object.
        EDM_REF_PRODUCT_TYPE__c objEDM_REF_PRODUCT_TYPE = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature', 'FEATR', true);
        //Create Title Object.
        EDM_GLOBAL_TITLE__c objFinancialTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('TESTFinancial', '908763', 'PUBLC', objEDM_REF_PRODUCT_TYPE.Id, true);
        //Promotion
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion1', 'National', null, objTerritory.Id, objLOB.Id, null, null, true);
        HEP_Promotion__History objPrHistory = new HEP_Promotion__History();
        objPrHistory.ParentId = objPromotion.id;
        objPrHistory.Field = 'created';
        insert objPrHistory;
        //DATING
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE'), objTerritory.Id, objAustria.Id, true);
        HEP_Promotion_Dating__c objDatingVOD = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id, objPromotion.id, objDatingMatrix.id, false);
        objDatingVOD.Date_Type__c = HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE');
        insert objDatingVOD;
        HEP_Promotion_Dating__History objDatingHistory = new HEP_Promotion_Dating__History();
        objDatingHistory.ParentId = objDatingVOD.id;
        objDatingHistory.Field = 'created';
        insert objDatingHistory;
        //CATALOG HISTORY
        HEP_SKU_Template__c objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU TEMP', objTerritory.id, objLOB.id, true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;
        HEP_Catalog__History objCatalogHistory = new HEP_Catalog__History();
        objCatalogHistory.ParentId = objCatalog.id;
        objCatalogHistory.Field = 'created';
        insert objCatalogHistory;
        //PromotionSKU
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, objSKUTemp.id, false);
        objPromoCat.Record_Status__c = 'Active';
        insert objPromoCat;
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objTerritory.Id, 'SKUTEST', 'KINGSMAN BD', 'Master', 'Physical', 'FOX', 'HD', true);
        HEP_Promotion_SKU__c objPromotionSku = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat.id, objSKU.id, 'Pending', 'Locked', false);
        objPromotionSku.Record_Status__c = 'Active';
        insert objPromotionSku;
        HEP_Promotion_SKU__History objpromotionSkuHist = new HEP_Promotion_SKU__History();
        objpromotionSkuHist.ParentId = objPromotionSku.id;
        objpromotionSkuHist.Field = 'created';
        insert objpromotionSkuHist;
        HEP_MDP_Promotion_Product__c objMDPProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id, objFinancialTitle.id, objTerritory.Id, false);
        objMDPProduct.Record_Status__c = 'Active';
        insert objMDPProduct;
        HEP_MDP_Promotion_Product__History objPrdHist = new HEP_MDP_Promotion_Product__History();
        objPrdHist.ParentId = objMDPProduct.id;
        objPrdHist.Field = 'created';
        insert objPrdHist;
        HEP_MDP_Promotion_Product_Detail__c objPrdDetail = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objMDPProduct.id, 'HD', 'EST', false);
        objPrdDetail.Record_Status__c = 'Active';
        insert objPrdDetail;
        HEP_MDP_Promotion_Product_Detail__History objPrdDetailHist = new HEP_MDP_Promotion_Product_Detail__History();
        objPrdDetailHist.ParentId = objPrdDetail.id;
        objPrdDetailHist.Field = 'created';
        insert objPrdDetailHist;
        //HEP_Market_Spend__
        HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('REQ77', objPromotion.id, 'Pending', false);
        objSpend.Record_Status__c = 'Active';
        insert objSpend;
        HEP_GL_Account__c objGL = new HEP_GL_Account__c();
        objGL.Record_Status__c = 'Active';
        insert objGL;
        HEP_Market_Spend_Detail__c objSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objSpend.id, objGL.id, 'Active', false);
        objSpendDetail.Type__c = 'Current';
        insert objSpendDetail;
        HEP_Market_Spend_Detail__History objSpendHist = new HEP_Market_Spend_Detail__History();
        objSpendHist.ParentId = objSpendDetail.id;
        objSpendHist.Field = 'created';
        insert objSpendHist;
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.id, null, null, true);
        HEP_SKU_Price__c objSkuPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKU.id, objAustria.id, objPromotionSku.id, null, 'Active', true);
        HEP_SKU_Price__History objSKUHIStory = new HEP_SKU_Price__History();
        objSKUHIStory.ParentId = objSkuPrice.id;
        objSKUHIStory.Field = 'created';
        insert objSKUHIStory;
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        System.runAs(u) {
            test.startTest();
            //System.runAs(u) {
            HEP_Promotion_Audit_Controller objaudit = new HEP_Promotion_Audit_Controller();
            HEP_Promotion_Audit_Controller.extractHistory(objPromotion.id, 'All', null, null);
            test.stopTest();
        }
    }
}