/**
* HEP_Internal_ScheduleReport -- Test class for the HEP_Internal_ScheduleReport for REports. 
* @author    Ritesh Konduru
*/
@isTest
private class HEP_Internal_ScheduleReport_Test {

    static testMethod void ValidateTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        objTerritory.SKU_Interface__c = 'ESCO';
        insert objTerritory;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objTerritory.Id, 'Draft', 'Request', true);
        HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objPromotion.Id, null, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '456', 'New SKU', 'Master', 'Physical', 'DD', 'DVD', true);
        objSKU.Current_Release_Date__c = date.today();
    	objSKU.Territory__c = objTerritory.id;
    	update objSKU;
        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,  'Approved', 'Unlocked', true);
        objPromotionSKU.Record_Status__c = 'Active';
        update objPromotionSKU;

        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.id, '10', 768.00, null);
        insert objPriceGrade;
        
        HEP_SKU_Price__c objSKUPriceSD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKU.Id, objTerritory.Id, objPromotionSKU.id, null, 'Active', false);
        insert objSKUPriceSD;       

        string lstOfTerritories = HEP_Internal_ScheduleReport.territoryLoad();

        HEP_Internal_ScheduleReport.HEP_InternalScheduleWrap objInteanalSchWrap = (HEP_Internal_ScheduleReport.HEP_InternalScheduleWrap) JSON.deserialize((lstOfTerritories), HEP_Internal_ScheduleReport.HEP_InternalScheduleWrap.class);

        List < HEP_Utility.MultiPicklistWrapper > listTerriotires = objInteanalSchWrap.lstTerrioryWrap;	
		if(listTerriotires[0] != null && listTerriotires.size() >0){
        	listTerriotires[0].bSelected = true;
    	}
    	system.debug('listTerriotires ' + listTerriotires);
         System.runAs(u) {  
        HEP_Internal_ScheduleReport.initialLoad(objInteanalSchWrap.dtStartDate , objInteanalSchWrap.dtStartDate , listTerriotires);
        System.assertEquals('1234',objCatalog.CatalogId__c);
    }

      }
}