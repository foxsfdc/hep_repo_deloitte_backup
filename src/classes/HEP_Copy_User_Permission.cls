/**
* HEP_Copy_User_Permission --- Class to get the clone user roles from a system user to another system user and to provide default access
* @author  Gaurav Mehrishi
*/
global class HEP_Copy_User_Permission{
    public static String sActive;
    public static String sDefaultAccessRole;
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sDefaultAccessRole = HEP_Utility.getConstantValue('HEP_ROLE_NAME_TO_EXPAND');
    }

    webservice static void copyPermissions(Id sourceUserId , Id destinationUserId, String sTransferOrReplace){
        try{
            List<User> lstDestinationUser = [SELECT Id,UserName 
                                                FROM User 
                                                WHERE Id =: destinationUserId
                                                ];
            String sDestinationUserName = lstDestinationUser[0].UserName; 
            system.debug('----------->' + sTransferOrReplace);                                    
            //List to upsert user role records for Destination user
            LIST<HEP_User_Role__c> lstUserRoleDestinationUserForUpsert = new LIST<HEP_User_Role__c>();           
            if(sTransferOrReplace.equalsIgnoreCase('Add Default Access') && destinationUserId != null){
                List<HEP_Territory__c> lstTerritory = [SELECT Id,Name 
                                                FROM HEP_Territory__c 
                                                WHERE Parent_Territory__c = null
                                                ];
                List<HEP_Role__c> lstDefaultAccessRole = [SELECT Id,Name 
                                                FROM HEP_Role__c
                                                WHERE Name =: sDefaultAccessRole
                                                ];
                //Iterate over Every territory in HEP and clone user roles based on it
                for(HEP_Territory__c objTerritory: lstTerritory){                                                               
                    HEP_User_Role__c objuserToInsert =  new HEP_User_Role__c();
                    objuserToInsert.Territory__c = objTerritory.Id;
                    objuserToInsert.Read__c = true;
                    objuserToInsert.Write__c = false;  
                    objuserToInsert.User__c = lstDestinationUser[0].id;
                    objuserToInsert.Role__c = lstDefaultAccessRole[0].id;           
                    objuserToInsert.Unique_Id__c = sDestinationUserName +' / '+ sDefaultAccessRole + ' / ' + objTerritory.Name;
                    lstUserRoleDestinationUserForUpsert.add(objuserToInsert);
                }
                System.debug('List of User roles for default access-->'+lstUserRoleDestinationUserForUpsert);
                
            } else if(!String.isEmpty(sourceUserId) && (sTransferOrReplace.equalsIgnoreCase('Replace Destination user role with Source user') || sTransferOrReplace.equalsIgnoreCase('Append Source user role to Destination user'))) {
                //1. Take out all the permissions for source User...
                LIST<HEP_User_Role__c> lstUserRoleSourceUser = [SELECT Territory__c, Territory__r.Name , Role__c , Role__r.Name, 
                                                            User__c , User__r.Name , User__r.UserName , Read__c , Write__c, Unique_Id__c , Search_Id__c
                                                            FROM HEP_User_Role__c
                                                            WHERE User__c =: sourceUserId];
                //2. Take out all the existing permissions for destination user..
                LIST<HEP_User_Role__c> lstUserRoleDestinationUser = [SELECT Territory__c, Territory__r.Name , Role__c , Role__r.Name, 
                                                            User__c , User__r.Name , User__r.UserName, Read__c , Write__c, Unique_Id__c , Search_Id__c
                                                            FROM HEP_User_Role__c
                                                            WHERE User__c =: destinationUserId];
                String sSourceUserName = lstUserRoleSourceUser[0].User__r.UserName;                 
                for(HEP_User_Role__c objUserRoleSourceUser : lstUserRoleSourceUser){
                    HEP_User_Role__c objUserRoleDestinationUser = objUserRoleSourceUser.clone(false, false, false, false);
                    objUserRoleDestinationUser.Unique_Id__c = objUserRoleDestinationUser.Unique_Id__c.replace(sSourceUserName , sDestinationUserName);
                    objUserRoleDestinationUser.user__c = lstDestinationUser[0].Id;              
                    lstUserRoleDestinationUserForUpsert.add(objUserRoleDestinationUser);
                }
                //5. Delete Destination User's Old permissions if it's a Replace...
                if(sTransferOrReplace.equalsIgnoreCase('Replace Destination user role with Source user')){
                    delete lstUserRoleDestinationUser;
                }
                System.debug('Transfer/Replace user role records for Destination user-->'+lstUserRoleDestinationUserForUpsert);
            }            
            if(lstUserRoleDestinationUserForUpsert.size()>0){
                  //Upsert records using Unique Id for Destination user
                  Schema.SObjectField UniqueId =  HEP_User_Role__c.Fields.Unique_Id__c;
                  Database.upsert(lstUserRoleDestinationUserForUpsert, UniqueId, false); 
                }           
        }catch(Exception ex){
            HEP_Error_Log.genericException('Copy User Permission','DML Errors',ex,'HEP_Copy_User_Permission','copyPermissions',null,null); 
        }
    }
    
    /**
     * Clear source & destination user field
     * @return N/A
     */
    webservice static void clearUsers(Id RoleId){
        try{
        HEP_Role__c clear_Users_Role = new HEP_Role__c();
        clear_Users_Role.Id = RoleId;       
        clear_Users_Role.Source_User__c = null;
        clear_Users_Role.Destination_User__c = null;
        clear_Users_Role.Copy_Permission_Type__c = null;
        update clear_Users_Role;
        }
        catch(exception ex){
          HEP_Error_Log.genericException('Copy User Permission','DML Errors',ex,'HEP_Copy_User_Permission','clearUsers',null,null); 
         }
    }
}