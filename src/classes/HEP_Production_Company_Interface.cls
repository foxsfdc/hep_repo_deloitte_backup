/**
* HEP_Production_Company_Interface -- HEP_Production_Company_Interface for the creation of Production Company Records 
* @author    Lakshman Jinnuri
*/
public class HEP_Production_Company_Interface implements HEP_IntegrationInterface
{   
    /**
    * ProductionWrapper -- Wrapper to store Production details 
    * @author    Lakshman Jinnuri
    */
    public class ProductionWrapper
    {
        public String EventName;
        public String EventType;
        public cls_Data Data;
        
        public ProductionWrapper()
        {
            EventName = '';
            EventType = '';
            Data = new cls_Data();
        }       
    }
    /**
    * cls_Payload -- Wrapper to store Payload details 
    * @author    Lakshman Jinnuri
    */
    public class cls_Payload {
        public String MessageID;
        public String TimeStamp;
        public String SubTypeCode;
        public String ChangedEntity;
        public String BaseObject;
        public String Operation;
        public cls_Change Change;
        
        public cls_Payload()
        {
            MessageID = '';
            TimeStamp = ''; 
            SubTypeCode = '';
            ChangedEntity = '';
            BaseObject = '';
            Operation = '';
            Change = new cls_Change();
        }
    }
    /**
    * cls_Data -- Wrapper to store Relationships details 
    * @author    Lakshman Jinnuri
    */
    public class cls_Data {
        public cls_Payload Payload;
        public cls_Data()
        {
            Payload = new cls_Payload();
        }   
    }
    /**
    * cls_Change -- Wrapper to store Change details 
    * @author    Lakshman Jinnuri
    */
    public class cls_Change {
        public String rowidObject;
        public String rowidTitle;
        public String foxId;
        public String hubStateInd;
        public String titleSubTypCd;
        public String level1;
        public String level2;
        public String changedEntity;
        
        public cls_Change()
        {
            rowidObject = '';
            rowidTitle = '';
            foxId = '';
            hubStateInd = '';
            titleSubTypCd = '';
            level1 = '';
            level2 = '';
            changedEntity = '';
            
        }     
    }

    /**
    * performTransaction -- Method for creation of Production Company Records 
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_Services__c objServiceDetails;
        Id idParentTitle;
        String sFoxId;
        //Variables to hold the custom Settings data
        String sHEP_FOXIPEDIA_OAUTH = HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_ProductionCompanyInterface = HEP_Utility.getConstantValue('HEP_PRODUCT_COMPANY_INTERFACE');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');   
        if(sHEP_FOXIPEDIA_OAUTH != null)      
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_FOXIPEDIA_OAUTH);
        System.debug('sAccessToken++++'+sAccessToken);
        if(objTxnResponse != null){
            ProductionWrapper objProdWrapper = (ProductionWrapper)JSON.deserialize(objTxnResponse.sRequest, ProductionWrapper.class);
            sFoxId = objProdWrapper.Data.Payload.Change.foxId;    
            System.debug('sFoxId'+sFoxId);   
            List<EDM_GLOBAL_TITLE__c> lstTitle = [SELECT Id FROM EDM_GLOBAL_TITLE__c WHERE FOX_ID__c = :sFoxId];
            if(!lstTitle.isEmpty())
                idParentTitle = lstTitle.get(0).id;
        }    
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null && idParentTitle != null){
            System.debug('idParentTitle'+idParentTitle);        
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Details from custom setting for Authentication
            if(sHEP_ProductionCompanyInterface != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_ProductionCompanyInterface);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?foxId=' + sFoxId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('system',System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(sHEP_SUCCESS != null && objHttpResp.getStatus().equals(sHEP_SUCCESS)){
                    //Gets the details from Foxipedia interface and stores in the wrapper 
                    HEP_Foxipedia_RunWrapper objWrapper = (HEP_Foxipedia_RunWrapper)JSON.deserialize(objHttpResp.getBody(),HEP_Foxipedia_RunWrapper.class);
                    System.debug('objWrapper :' + objWrapper );
                    Map<String,HEP_Production_Company__c> mapProdCompany = new Map<String,HEP_Production_Company__c>();
                    if(objWrapper.data != null)
                    for(HEP_Foxipedia_RunWrapper.Data objDataInst:objWrapper.data)
                    {
                        if(objDataInst.attributes == null || objDataInst.attributes.prodCompany == null )
                            continue;
                        for(HEP_Foxipedia_RunWrapper.ProdCompany objProdCompany:objDataInst.attributes.prodCompany)
                        {
                            if(String.isEmpty(objProdCompany.foxId) || String.isEmpty(objProdCompany.productionCompanyCode))
                                continue;
                            Integer iSortOrder = (String.isNotEmpty(objProdCompany.productionCompanySortOrder))?Integer.valueOf(objProdCompany.productionCompanySortOrder):0;
                            String sDescription = (String.isNotEmpty(objProdCompany.productionCompanyDescription))?objProdCompany.productionCompanyDescription:'';
                            String sUniqueId = objProdCompany.foxId + ' / ' +objProdCompany.productionCompanyCode;
                            HEP_Production_Company__c objHEPProdCompany = new HEP_Production_Company__c(Title__c = idParentTitle,
                                                                                                        Sort_Order__c = iSortOrder,
                                                                                                        Production_Company_Code__c = objProdCompany.productionCompanyCode,
                                                                                                        Production_Company_Description__c = sDescription,
                                                                                                        Unique_Id__c = sUniqueId);
                            
                            
                            mapProdCompany.put(sUniqueId,objHEPProdCompany);
                        }
                    }
                    
                    if(!mapProdCompany.isEmpty())
                    for(HEP_Production_Company__c rec:[SELECT Id,Unique_Id__c,Title__c FROM HEP_Production_Company__c WHERE Unique_Id__c IN :mapProdCompany.keySet()])
                        mapProdCompany.get(rec.Unique_Id__c).id = rec.id;
                    upsert mapProdCompany.values();
        
                    if(sHEP_FAILURE != null && objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(sHEP_Int_Txn_Response_Status_Success != null)         
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                        objTxnResponse.bRetry = false;
                    }
                }
                else{
                    if(sHEP_FAILURE != null)
                        objTxnResponse.sStatus = sHEP_FAILURE;
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }     
    }    
    
    
}