/**
* HEP_Promotion_SKU_Siebel_Wrapper -- Promotion SKU Wrapper for getting field names for HEP_Promotion_SKU__c
* object and then converting them to JSon output fields
* @author    Mayank Jaggi
*/

public class HEP_Promotion_SKU_Siebel_Wrapper implements HEP_IntegrationInterface
{
    /**
    * Item --- record level wrapper to hold the record details
    * @author   Mayank Jaggi
    */
    public class Item
    {
        public string objectType;
        public string createdDate;
        public string createdBy;
        public string updatedDate;
        public string updatedBy;
        public string recordId;
        public string recordStatus;
        public string territoryId;
        public string territoryName;
        public string promotionId;
        public string promotionName;
        public string promotionCode;
        public string releaseName;
        public integer releaseId;
        public string catalogId;
        public integer catalogNumber;
        public string skuId;
        public string skuNumber;
        public string channel;                  
        public string format;                  
        public string revenueShareFlag;         
        public string promotionSKUStatus;      
        public string legacyId;
    }

    public class Payload
    {
        public List<Item> items;    
    }

    public class Data
    {
        public Payload Payload;
        public Data(Payload PromotionSKUPayload)
        {
            this.Payload = PromotionSKUPayload;
        }
    }

    /**
    * PromotionSKU --- main wrapper which holds the entire request
    * @author    Mayank Jaggi
    */
    public class PromotionSKU
    {
        public string ProducerID;
        public string EventType;
        public string EventName;
        public Data Data;
        public PromotionSKU(Data PromotionSKUData)
        {
            this.Data = PromotionSKUData;
            this.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');    
            this.EventType = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
            this.EventName = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        }
    }

    /**
    * ErrorResponseWrapper --- Wrapper class for response parse
    * @author    Mayank Jaggi
    */
    public class ErrorResponseWrapper 
     {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage)
        {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }

    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Mayank Jaggi
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        List <String> lstRecordIds = new List <String>();
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();

        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            
            // HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');

            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                if(String.isNotBlank(objTxnResponse.sSourceId))
                {
                    lstRecordIds = objTxnResponse.sSourceId.split(',');                 
                }
                else
                {
                    system.debug('objTxnResponse.sSourceId is Null');
                }
                httpRequest.setBody(getPromotionSKUFields(lstRecordIds));

                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK') || httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')){
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE'); 
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }


    /**
    * Get the serialized json of the request to be sent
    * @param  list of record ids
    * @return 
    * @author Mayank Jaggi
    */
    public static string getPromotionSKUFields(List <String> idPromotionSKUIdList)
    {
        String JsonOutput = ' ';
        string HEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        String sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
        String sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
        List<String> lstChannelFormatTypes = new List<String>{sCHANNEL,sFORMAT};
        map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(lstChannelFormatTypes, 'Name', 'Values__c');
        System.debug('mapLOVMapping>>' + mapLOVMapping);
        List <HEP_Promotion_SKU__c> PromotionSKURecords = new  List <HEP_Promotion_SKU__c>();
        PromotionSKURecords = [Select CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name,
                                Record_Status__c, SKU_Master__r.Territory__r.Id, SKU_Master__r.Territory__r.Name,
                                Promotion_Catalog__r.Promotion__r.Id, Promotion_Catalog__r.Promotion__r.PromotionName__c, Release_Id__c,
                                Promotion_Catalog__r.Catalog__r.CatalogId__c, Name,                            
                                Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c, SKU_Master__r.SKU_Title__c,
                                Promotion_Catalog__c, SKU_Master__r.Channel__c, Approval_Status__c,
                                Promotion_Catalog__r.Catalog__c, SKU_Master__r.Format__c, SKU_Master__r.Revenue_Share__c,
                                Promotion_Catalog__r.Catalog__r.Name,
                                SKU_Master__r.Name, SKU_Master__r.SKU_Number__c,
                                Promotion_Catalog__r.Record_Status__c, Legacy_Id__c from HEP_Promotion_SKU__c where 
                                HEP_Promotion_SKU__c.Id in :idPromotionSKUIdList];
                                // removed filter criteria and added to trigger handler 5/29
        System.debug('PromotionSKURecords '+PromotionSKURecords);
        if(!PromotionSKURecords.isEmpty())
        {
            List<Item> lstListitem = new List<Item>();
            for (HEP_Promotion_SKU__c ProSKU : PromotionSKURecords)
            {   
                Item PromoSKUItem = new Item();
                PromoSKUItem.objectType = HEP_Utility.getConstantValue('HEP_SKU_Promotion_objecttype'); 
                PromoSKUItem.createdBy = ProSKU.CreatedBy.Name;
                PromoSKUItem.createdDate = String.ValueOf(ProSKU.CreatedDate) ;
                PromoSKUItem.updatedDate = String.ValueOf(ProSKU.LastModifiedDate);
                PromoSKUItem.updatedBy = ProSKU.LastModifiedBy.Name;
                PromoSKUItem.recordId = ProSKU.Id;
                PromoSKUItem.recordStatus = ProSKU.Record_Status__c ;
                PromoSKUItem.territoryName = ProSKU.SKU_Master__r.Territory__r.Name ;
                PromoSKUItem.territoryId = ProSKU.SKU_Master__r.Territory__c ;
                PromoSKUItem.promotionId = ProSKU.Promotion_Catalog__r.Promotion__c ;
                PromoSKUItem.promotionName = ProSKU.Promotion_Catalog__r.Promotion__r.PromotionName__c ;
                PromoSKUItem.promotionCode = ProSKU.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c ;
                PromoSKUItem.releaseName = ProSKU.SKU_Master__r.SKU_Title__c ;
                PromoSKUItem.catalogId = ProSKU.Promotion_Catalog__r.Catalog__c ;
                PromoSKUItem.catalogNumber = Integer.ValueOf(ProSKU.Promotion_Catalog__r.Catalog__r.CatalogId__c) ;
                PromoSKUItem.skuId = ProSKU.SKU_Master__c ;
                PromoSKUItem.skuNumber = ProSKU.SKU_Master__r.SKU_Number__c ;
                if (ProSKU.SKU_Master__r.Channel__c != null){
                    String sChannelMeaning = mapLOVMapping.containsKey(sCHANNEL + '*' + ProSKU.SKU_Master__r.Channel__c) ? mapLOVMapping.get(sCHANNEL + '*' + ProSKU.SKU_Master__r.Channel__c) : ProSKU.SKU_Master__r.Channel__c;
                    PromoSKUItem.channel = sChannelMeaning;
                }else{
                    PromoSKUItem.channel = ProSKU.SKU_Master__r.Channel__c;
                }

                if (ProSKU.SKU_Master__r.Format__c != null){
                    String sFormatMeaning = mapLOVMapping.containsKey(sFORMAT + '*' + ProSKU.SKU_Master__r.Format__c) ? mapLOVMapping.get(sFORMAT + '*' + ProSKU.SKU_Master__r.Format__c) : ProSKU.SKU_Master__r.Format__c;
                    PromoSKUItem.format = sFormatMeaning;
                }else{
                    PromoSKUItem.format = ProSKU.SKU_Master__r.Format__c;
                }
                if(ProSKU.Release_Id__c != null)
                    PromoSKUItem.releaseId = Integer.valueOf(ProSKU.Release_Id__c);
                else
                    PromoSKUItem.releaseId = Integer.valueOf(ProSKU.Name.right(ProSKU.Name.length()-3));
                
                if(ProSKU.SKU_Master__r.Revenue_Share__c == true)
                {
                    PromoSKUItem.revenueShareFlag = 'Y';
                }
                else
                {
                    PromoSKUItem.revenueShareFlag = 'N';
                }
                PromoSKUItem.promotionSKUStatus = ProSKU.Approval_Status__c ;
                PromoSKUItem.legacyId = ProSKU.Legacy_Id__c ;
                lstListitem.add(PromoSKUItem);
            }
            System.debug('lstListitem '+lstListitem);
            Payload PromotionSKUPayload = new Payload();
            PromotionSKUPayload.items = lstListitem;   
            Data PromotionSKUData = new Data(PromotionSKUPayload);
            PromotionSKU ProSKUInstance = new PromotionSKU(PromotionSKUData);

            JsonOutput = Json.serialize(ProSKUInstance); 
            system.debug('JsonOutput '+JsonOutput);
            
        }
    return JsonOutput;
    }
}