@isTest
private class HEP_Promotion_Dating_TriggerHandler_Test {
  
  @isTest static void test_method_one() {
    list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
    list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();

    //Create User
    User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
                                'test@deloitte.com','testL','testL','Fox - New Release',true);

    //Create Line of Business
    HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);

    //Create Role
    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','Promotion',true);

    //Create EDM Product Type.
    EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

    //Create EDM Title Record.
    EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);

    //Create Territory
    List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
                                    HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
                                    HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false)};

    lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
    lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
    lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
    insert lstTerritory;
    System.debug('Territory : ' + lstTerritory);

    //Create User Role
    List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,objUser.Id,false),
                                     HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,objUser.Id,false),
                                     HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,objUser.Id,false)};

    insert lstUserRoles;

       //Create Promotion
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                      HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                      HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)};
        
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;
        insert lstPromotions;

        lstPromotions[1].Global_Promotion__c = lstPromotions[0].Id;

        Map<Id , HEP_Promotion__c> mapPromotions = new Map<Id , HEP_Promotion__c>();
        for(HEP_Promotion__c objProm : lstPromotions){
          mapPromotions.put(objProm.Id , objProm);
        }

        update lstPromotions;
        HEP_Catalog__c objCatalog1 = HEP_Test_Data_Setup_Utility.createCatalog('52560', 'ARTHUR', 'Single', null, lstTerritory[0].Id, 'Approved', 'Master', false);
        insert objCatalog1;
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog1.Id, lstTerritory[0].Id, '12345', 'The Age of empire', 'Master', 'Physical', 'VOD', 'HD', false);
        insert objSKU;
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(lstTerritory[0].Id, '$44.95 - $31.50', 44.95, false);
        insert objPriceGrade;
        //Create Notification Record..
        HEP_Notification_Template__c objNotification = new HEP_Notification_Template__c(Name = 'Global_FAD_Date_Change',HEP_Body__c = '<old Global FAD> updated to <new Global FAD>',HEP_Object__c = 'HEP_Promotion__c',Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = 'Global_FAD_Date_Change',Status__c='-');
		//insert objNotification;

        HEP_Notification_Template__c objNotification1 = new HEP_Notification_Template__c(Name = HEP_Utility.getConstantValue('GLOBAL_PROMOTION_CREATION'),HEP_Body__c = 'A catalog has been added to <Promotion__r.PromotionName__c> - <Promotion__r.LocalPromotionCode__c>. Please review your local promotion and update as needed.',HEP_Object__c = 'HEP_Promotion__c',Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = HEP_Utility.getConstantValue('GLOBAL_PROMOTION_CREATION'),Status__c='-');
        //insert objNotification;

        insert new List<HEP_Notification_Template__c>{objNotification , objNotification1};
        //HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_NOTIFICATION_CONFIRMED'), 'HEP_Promotion_Dating__c','<Date_Type__c>','test type' ,'Active','Approved', 'test',true);
        List <HEP_Promotion_Dating__c> lstPrDating =  new List <HEP_Promotion_Dating__c>();
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Avail Date', lstTerritory[0].Id, lstTerritory[1].Id, true);
        HEP_Promotion_Dating__c objPromDating = HEP_Test_Data_Setup_Utility.createDatingRecord(lstTerritory[0].Id, lstPromotions[0].Id, objDatingMatrix.id,false);
        objPromDating.Date_Type__c = 'EST Release Date';
        objPromDating.Status__c = 'Confirmed';
        objPromDating.Record_Status__c = 'Active';
        objPromDating.Date__c = Date.today();
        objPromDating.Locked_Status__c = 'Unlocked';
        lstPrDating.add(objPromDating);
        insert lstPrDating;
        HEP_CheckRecursive.clearSetIds();
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKU.Id, lstTerritory[0].Id, null, objPromDating.Id, 'Active', false);
        objSKUPrice.Changed_Flag__c = true;
        insert objSKUPrice;
        Map<Id , HEP_Promotion_Dating__c> mapPromotionDating = new Map<Id , HEP_Promotion_Dating__c>();
        Set<Id> setPromotionDatingIds = new Set<Id>();
        for(HEP_Promotion_Dating__c objProm : lstPrDating){
          mapPromotionDating.put(objProm.Id ,objProm);
          setPromotionDatingIds.add(objProm.Id);
        }
        lstPrDating[0].Record_Status__c = 'Inactive';
        update lstPrDating;
        
        //new HEP_Promotion_Dating_TriggerHandler().afterUpdate();
        //HEP_Promotion_Dating_TriggerHandler.handleBeforeUpsert(lstPrDating,mapPromotionDating);
        //HEP_Promotion_Dating_TriggerHandler.handleAfterUpsert(lstPrDating);
        //HEP_Promotion_Dating_TriggerHandler.stampDatingRecordsAfterUpdate(lstPrDating,mapPromotionDating);
        //HEP_Promotion_Dating_TriggerHandler.AlertsAfterUpdate(lstPromotions , mapPromotions);
  }
  
}