/**
* HEP_Create_Global_Promotion_Controller --- Class for the Global Promotion Set-Up Page
* @author    Ayan Sarkar
*/
public class HEP_Create_Global_Promotion_Controller {

    public static String sGlobalTerritoryId;

    /**
    * checkPermissions --- To check if the current user have access to the page is in.
    * @return PageReference
    * @author    Ayan Sarkar
    */
    @RemoteAction
    public static PageReference checkPermissions(){

        sGlobalTerritoryId = '';
        List<HEP_Territory__c> lstTerritory = [SELECT Id,
                                                Name
                                                FROM HEP_Territory__c
                                                WHERE Name =: HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME')
                                                LIMIT 1 ];

        if(lstTerritory!=null && !lstTerritory.isEmpty()){
            sGlobalTerritoryId = lstTerritory[0].Id;
        }

        MAP<String, String> mapCustomPermissions = HEP_Utility.fetchCustomPermissions(sGlobalTerritoryId, 'HEP_Create_Global_Promotion');
        system.debug('CUSTOM PERMISSIONS......'+ mapCustomPermissions);     
        //mapCustomPermissions.put('WRITE','FALSE');
        if(mapCustomPermissions.get('WRITE').equalsIgnoreCase('FALSE')){
            PageReference retURL = new PageReference('/apex/HEP_AccessDenied');
            retURL.setRedirect(true);
            return retURL;
        }else{
            return null;
        }
    }


    /**
    * PickLists --- Class to hold all the pickList field values 
    * @author    Ayan Sarkar
    */
    public class PickLists{
        public List<LineOfBusinessWrapper> lstLineOfBusiness;
        public List<MarketingManagerWrapper> lstMarketingManagers;
        /**
        * Picklists Contructor --- Constructor to initialize the List vairables
        * @return   N/A
        */
        public PickLists(){
            lstLineOfBusiness = new List<LineOfBusinessWrapper>();
            lstMarketingManagers = new List<MarketingManagerWrapper>();
        }
    }

    
     /**
    * LineOfBusinessWrapper --- Class to hold data related to Line Of Business
    * @author    Ayan Sarkar
    */
    public class LineOfBusinessWrapper{
        public String sValue;
        public String sKey;
        public String sLineOfBusinessType;
       /**
        * LineOfBusinessWrapper Contructor --- Constructor to initialize the List vairables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @param  sLineOfBusinessType String field to hold the line of business type
         * @return   N/A
        */
        public LineOfBusinessWrapper(String sKey, String sValue, String sLineOfBusinessType){
            this.sValue = sValue;
            this.sKey = sKey;
            this.sLineOfBusinessType = sLineOfBusinessType;
        }
        public LineOfBusinessWrapper(){}
    }

    
    /**
    * Region --- Class to hold data for Marketing Managers 
    * @author    Ayan Sarkar
    */
    public class MarketingManagerWrapper{
        public String sKey;
        public String sValue;
        public String sLOB;
        public String sEmail;
        /**
        * MarketingManagerWrapper Contructor --- Constructor to initialize the List vairables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @param  sLOB Salesforce ID for Line Of Business Record
         * @return   N/A
        */
        public MarketingManagerWrapper(String sKey, String sValue, String sLOB , String sEmail){
            this.sKey = sKey;
            this.sValue = sValue;
            this.sLOB = sLOB;
            this.sEmail = sEmail;
        }
        public MarketingManagerWrapper(){}
    }
    
    
    /**
    * GlobalPromotionPageData --- Class to represent single instance of a Customer Promotion
    * @author    Ayan Sarkar
    */
    public class GlobalPromotionPageData{
        public HEP_Utility.PickListWrapper objFinancialTitleId;
        public String sPromotionName;
        public LineOfBusinessWrapper objLineOfBusiness;
        public String sMediaType;
        public String sGlobalFAD;
        public Date dGlobalFAD;
        public MarketingManagerWrapper objDomesticMarketingManager;
        public MarketingManagerWrapper objInternationalMarketingManager;
        public String sUserLocale;
        public String sDateFormat;
        public String sGlobalTerritoryId;
        public PickLists picklist;
        public Map<String, String> mapErrorMessages;
    
        /**
        * GlobalPromotionPageData Contructor --- Constructor to initialize the List vairables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @param  sLineOfBusinessType String field to hold the line of business type
         * @return   N/A
        */
        public GlobalPromotionPageData(){
            mapErrorMessages = HEP_Utility.getAllCreatePromotionErrorMessages();
            pickList = new PickLists();
        }

    }
    
    
    /**
     * Page Data to be shown on the inital load of Create Customer Promotion Page
     * @return instance of HEP_Create_Customer_Promotion_Controller.
     */
    @RemoteAction
    public static GlobalPromotionPageData fetchGlobalPromotionDefaultValues(){
        GlobalPromotionPageData globPromotionPageData = new GlobalPromotionPageData();
        globPromotionPageData.objFinancialTitleId = new HEP_Utility.PickListWrapper('','');
        globPromotionPageData.sPromotionName = '';
        globPromotionPageData.objLineOfBusiness = new LineOfBusinessWrapper('' , '' , '');
        globPromotionPageData.sMediaType = 'Both';
        globPromotionPageData.sGlobalFAD = '';
        globPromotionPageData.objDomesticMarketingManager = new MarketingManagerWrapper('','','', '');
        globPromotionPageData.objInternationalMarketingManager = new MarketingManagerWrapper('','','', '');
        globPromotionPageData.sDateFormat = HEP_Utility.getDateTimeLocaleFormat(true, false);
        globPromotionPageData.sUserLocale = UserInfo.getLocale();
        
        
        try{
            //Pre-Populate Line Of Business dropdown
            String sLineOfBusinessWhereClause = ' WHERE Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\'';
            String sLineOfBusinessQuery = HEP_Utility.buildQueryAllString('HEP_Line_Of_Business__c',null,sLineOfBusinessWhereClause);
            System.debug('!@#$%^&* sLineOfBusinessQuery ' + sLineOfBusinessQuery);
            sLineOfBusinessQuery += ' Order By Name ASC'; 
            List<HEP_Line_Of_Business__c> lstLineOfBusiness = (List<HEP_Line_Of_Business__c>) Database.query(sLineOfBusinessQuery);
            MAP<String, HEP_Line_Of_Business__c> mapLineOfBusiness = new MAP<String, HEP_Line_Of_Business__c>(); 
            for(HEP_Line_Of_Business__c lineOfBusiness : lstLineOfBusiness){
                globPromotionPageData.pickList.lstLineOfBusiness.add(new LineOfBusinessWrapper(lineOfBusiness.Id, lineOfBusiness.Name, lineOfBusiness.Type__c));
                mapLineOfBusiness.put(lineOfBusiness.Name, lineOfBusiness);
            }

            System.debug(''+globPromotionPageData.pickList);
            //Pre-Populate Global Promotions dropdown
            String sGlobalPromotionQuery = HEP_Utility.buildQueryAllString('HEP_Territory__c',null,' WHERE Name Like \'%Global%\'');
            System.debug('!@#@! sGlobalPromotionQuery ----> ' + sGlobalPromotionQuery);
            List<HEP_Territory__c> lstGlobalPromotion = (List<HEP_Territory__c>) Database.query(sGlobalPromotionQuery);
            if(!lstGlobalPromotion.isEmpty())
                  globPromotionPageData.sGlobalTerritoryId = lstGlobalPromotion[0].Id;
            
            //Pre-Populate Marketing Mangers Dropdowns
            String sMarketingManagersQueryWhereClause = ' WHERE Territory__r.Name = \'Global\' AND Role__c in (SELECT Id from HEP_Role__c where Name Like \'Marketing Manager%\')  AND Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\' AND Write__c = true';
            String sMarketingManagersQuery = HEP_Utility.buildQueryAllString('HEP_User_Role__c', 'SELECT User__r.Name,User__r.Email , User__r.HEP_Line_Of_Business__c, ' , sMarketingManagersQueryWhereClause);
            List<HEP_User_Role__c> lstMarketingManagersRecs = (List<HEP_User_Role__c>) Database.query(sMarketingManagersQuery);
            Map<String , MarketingManagerWrapper> mapUsersToShow = new Map<String , MarketingManagerWrapper>(); 
            for(HEP_User_Role__c marketingManagerRec : lstMarketingManagersRecs){
                mapUsersToShow.put(marketingManagerRec.User__r.Name, new MarketingManagerWrapper(marketingManagerRec.User__c, marketingManagerRec.User__r.Name,  marketingManagerRec.User__r.HEP_Line_Of_Business__c , marketingManagerRec.User__r.Email));
                if(UserInfo.getUserId() == marketingManagerRec.User__c){
                    globPromotionPageData.objDomesticMarketingManager = new MarketingManagerWrapper(marketingManagerRec.User__c, marketingManagerRec.User__r.Name,  marketingManagerRec.User__r.HEP_Line_Of_Business__c , marketingManagerRec.User__r.Email);
                    globPromotionPageData.objInternationalMarketingManager = new MarketingManagerWrapper(marketingManagerRec.User__c, marketingManagerRec.User__r.Name,  marketingManagerRec.User__r.HEP_Line_Of_Business__c , marketingManagerRec.User__r.Email);
                    if(String.isNotBlank(marketingManagerRec.User__r.HEP_Line_Of_Business__c) && mapLineOfBusiness.containsKey(marketingManagerRec.User__r.HEP_Line_Of_Business__c)){
                        globPromotionPageData.objLineOfBusiness = new LineOfBusinessWrapper(mapLineOfBusiness.get(marketingManagerRec.User__r.HEP_Line_Of_Business__c).Id , mapLineOfBusiness.get(marketingManagerRec.User__r.HEP_Line_Of_Business__c).Name, mapLineOfBusiness.get(marketingManagerRec.User__r.HEP_Line_Of_Business__c).Type__c);
                    }
                }
            }
            globPromotionPageData.pickList.lstMarketingManagers.addAll(mapUsersToShow.values());
        }catch(Exception e){
            throw e;

            HEP_Error_Log.genericException('Error occured during Creation of Customer Promotion','VF Controller',e,'HEP_Create_Global_Promotion','getTerritoriesDefaultValues', null,'');
        }
        return globPromotionPageData;
    }
    
    
    /**
     * Helps in getting the TypeAhead for Marketing Managers
     * @param  fieldName API name of the field to be searched
     * @param  objectName API name of the object to be searched
     * @param  value that user types
     * @param  extraWhere extra where clause for further filtering
     * @return List of PickList Wrapper
     */
    @RemoteAction
    public static List<HEP_Utility.PickListWrapper> getTypeAheadValues(String sFieldName, String sObjectName, String sValue, String sExtraWhere){       
        return HEP_Utility.getTypeAheadValues(sFieldName, sObjectName, sValue, sExtraWhere);        
    }
    
    
     /**
     * Helps in getting the TypeAhead for Finacial Title Id
     * @param  value that user types
     * @return List of PickList Wrapper
     */
    @RemoteAction
    public static List<HEP_Utility.PickListWrapper> getFinacialTitleId(String sValue){
        return HEP_Utility.getFinacialTitleId(sValue);
    }
    
    
    /**
     * Method to Save the Data for Global Promotion into DataBase
     * @param  objPageData Wrapper class having all the required values to be updated in the Database
     * @return String
     */
    @RemoteAction
    public static String saveGlobalPromotion(GlobalPromotionPageData objPageData){
        HEP_Promotion__c promotion;
        String toReturn = null;
        List<HEP_Promotion_Dating__c> lstDates = new List<HEP_Promotion_Dating__c>();
        Savepoint objSavePoint = Database.setSavepoint();
        try{

            MAP<String, String> mapCustomPermissions = HEP_Utility.fetchCustomPermissions(sGlobalTerritoryId, 'HEP_Create_Global_Promotion');
            
            if(mapCustomPermissions.get('WRITE').equalsIgnoreCase('FALSE')){
                HEP_Utility.redirectToErrorPage();
            }else{
                //Create the record for Global Promotion

                //Duplicate check for GLobal Promotion based on Name.
                List<HEP_Promotion__c> lstGlobalPromotion = [SELECT ID , PromotionName__c
                                                             FROM HEP_Promotion__c
                                                             WHERE PromotionName__c =: objPageData.sPromotionName AND    Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND
                                                             Promotion_Type__c =: HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME')];

                if(!lstGlobalPromotion.isEmpty()){
                    toReturn = HEP_Utility.getConstantValue('HEP_Global_Promotion_Duplicate_Error');
                }
                else{
                    promotion = new HEP_Promotion__c();
                    promotion.PromotionName__c = objPageData.sPromotionName;
                    promotion.Title__c = objPageData.objFinancialTitleId != NULL && String.isNotBlank(objPageData.objFinancialTitleId.sKey)? objPageData.objFinancialTitleId.sKey : null;
                    promotion.LineOfBusiness__c = objPageData.objLineOfBusiness.sKey;
                    promotion.Digital_Physical__c = objPageData.sMediaType;
                    promotion.FirstAvailableDate__c = objPageData.dGlobalFAD != null ? objPageData.dGlobalFAD : null;
                    promotion.Domestic_Marketing_Manager__c = String.isNotBlank(objPageData.objDomesticMarketingManager.sKey) ? objPageData.objDomesticMarketingManager.sKey : null;
                    promotion.International_Marketing_Manager__c = String.isNotBlank(objPageData.objInternationalMarketingManager.sKey) ? objPageData.objInternationalMarketingManager.sKey : null;
                    promotion.Territory__c = objPageData.sGlobalTerritoryId;
                    promotion.Promotion_Type__c = HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME');
                    promotion.Status__c = 'Draft';
                    insert promotion;
                    toReturn = promotion.Id;
        
                    String sLOBType = objPageData.objLineOfBusiness.sLineOfBusinessType;
                    //if(objPageData.objLineOfBusiness.sLineOfBusinessType.equalsIgnoreCase(HEP_Utility.getConstantValue('PROMOTION_LOB_NEW_RELEASE'))){
                        List<String> lstMediaType = new List<String>();
                        if(String.isNotBlank(objPageData.sMediaType) && !objPageData.sMediaType.equalsIgnoreCase('Both')){
                            lstMediaType.add(objPageData.sMediaType);
                        }else{     
                            lstMediaType.add(HEP_Utility.getConstantValue('HEP_PHYSICAL_MEDIA_TYPE'));
                            lstMediaType.add(HEP_Utility.getConstantValue('HEP_DIGITAL_MEDIA_TYPE'));
                        }
                        String sDatingMatrixQuery = HEP_Utility.buildQueryAllString('HEP_Promotions_DatingMatrix__c',null,' WHERE Promotion_Territory__r.Name = \'Global\' AND Region__r.Type__c = \'Licensee\' AND Date_Category__c != \'Promotion\' AND Dating_Flag__c = true AND Record_Status__c = \'Active\' AND Line_of_Business_Type__c = \''+ sLOBType +'\' AND Media_Type__c in: lstMediaType AND Record_Status__c = \'Active\'');
                        System.debug('sDatingMatrixQuery -----> ' + sDatingMatrixQuery);
                        List<HEP_Promotions_DatingMatrix__c> allDatingMatrix = Database.Query(sDatingMatrixQuery);
                        System.debug('allDatingMatrix -----> ' + allDatingMatrix);
                        for(HEP_Promotions_DatingMatrix__c datingMatrixRec : allDatingMatrix){
                            HEP_Promotion_Dating__c dateRec = new HEP_Promotion_Dating__c();
                            dateRec.Promotion__c = promotion.Id;
                            dateRec.Status__c = 'Tentative';
                            dateRec.Date_Type__c = datingMatrixRec.Date_Type__c;
                            if(String.isNotBlank(datingMatrixRec.HEP_Language__c)){
                                dateRec.Language__c = datingMatrixRec.HEP_Language__c;
                            }else{
                                dateRec.Language__c = null;
                            }
                            
                            dateRec.HEP_Promotions_Dating_Matrix__c = datingMatrixRec.Id;
                            dateRec.Date__c = null;
                            dateRec.Territory__c = datingMatrixRec.Region__c;
                            dateRec.Status__c = HEP_Utility.getConstantValue('HEP_TENTATIVE_DATING_STATUS'); 
                            lstDates.add(dateRec);
                        }
                        if(!lstDates.isEmpty())
                            insert lstDates;
                    //}
                    sendGlobalCreationEmail(objPageData.objLineOfBusiness.sLineOfBusinessType, toReturn);
                   
                }
            }
        }catch(Exception e){
            throw e;
            //Code to log error..
            HEP_Error_Log.genericException('Error occured during Creation of Customer Promotion','VF Controller', e,'HEP_Create_Global_Promotion', 'saveGlobalPromotion', null,'');
            Database.RollBack(objSavePoint);
        }
        return toReturn;
    }
    
    
    @future 
    public static void sendGlobalCreationEmail(String sLOBType, String toReturn)
    {
        
         //Send Email to Local Marketing Manager. ---- To be modified.
        //EmailTemplate objiTETemplate = [Select Id, DeveloperName from EmailTemplate where developername = 'HEP_Global_Promotion_Created'];
        
        List<String> lstEmailAddresses =  new List<String>();
        Set<String> setEmailAddresses = new Set<String>();
        Set<String> setToEmailAddresses = new Set<String>();
        Set<String> setFromEmailAddresses = new Set<String>();
        List<Id> lstUserId = new List<Id>();
        Set<Id> setUserId = new Set<Id>();
        //String sLOBType = ;
        String sRole;                   
        system.debug('LOB---->' + sLOBType);
        if (sLOBType.equalsIgnoreCase(HEP_Utility.getConstantValue('PROMOTION_LOB_TV'))) {
            sRole = HEP_Utility.getConstantValue('HEP_Marketing_Manager_TV');
        } else if (sLOBType.equalsIgnoreCase(HEP_Utility.getConstantValue('PROMOTION_LOB_CATALOG'))) {
            sRole = HEP_Utility.getConstantValue('HEP_Marketing_Manager_CTLG');
        } else if (sLOBType.equalsIgnoreCase(HEP_Utility.getConstantValue('PROMOTION_LOB_NEW_RELEASE'))) {
            sRole = HEP_Utility.getConstantValue('HEP_Marketing_Manager_NR');
        }

        //Code to send Emails to Marketing Manager based on LOB
        List<HEP_User_Role__c> lstEmailLOBs = [SELECT ID , User__c , Role__c , User__r.Email
                                                    FROM HEP_User_Role__c
                                                    WHERE Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')
                                                    AND Role__r.Name =: sRole];
        if (!lstEmailLOBs.isEmpty()) {
            for (HEP_User_Role__c objUserRole : lstEmailLOBs) {
                setEmailAddresses.add(objUserRole.User__r.Email);
                setUserId.add(objUserRole.User__c);
            }
        }

        System.debug('1. Email Ids TO : ----> ' + setEmailAddresses);

        //Code to add additional Email Ids for Fox Users....
        String sEmailTemplateName = HEP_Utility.getConstantValue('HEP_Global_Promotion_Created');
        String sUsersInEMailCC='';

        //Fetch user need to kept in Email CC for the Territory: This will be coming from LOV Mapping 
        for (HEP_List_Of_Values__c objTempLstRec: [Select Values__c, Name_And_Parent__c from HEP_List_Of_Values__c where
                                Name =: sEmailTemplateName 
                                AND Type__c = 'EMAIL_CC_USERS'
                                And Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')]) {
            if(sUsersInEMailCC=='')
                sUsersInEMailCC = objTempLstRec.Values__c;
            else
                sUsersInEMailCC = sUsersInEMailCC + ' ; ' + objTempLstRec.Values__c;
        }

        System.debug('2. Email Ids CC : ----> ' + sUsersInEMailCC);

        if(setEmailAddresses != NULL && !setEmailAddresses.isEmpty()){
            lstEmailAddresses.addAll(setEmailAddresses);
        }

        if(!lstEmailAddresses.isEmpty()){
                     
            //Create Outbound Email record before sending the Email.

            List<HEP_Outbound_Email__c> lstOutboundEmailsToInsert = new List<HEP_Outbound_Email__c>();

            HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c(); //Single Instance of the Object
            objOutboundEmail.Record_Id__c = toReturn; // The merge Id that will be accessed by the Template (WhatId)
            objOutboundEmail.Object_API__c = 'HEP_Promotion__c'; //The Object Name of the above Record
            objOutboundEmail.Email_Template_Name__c = HEP_Utility.getConstantValue('HEP_Global_Promotion_Created');      // The Email Template Name

            String sEmailAddresses = String.join(lstEmailAddresses , ';');
            objOutboundEmail.To_Email_Address__c = sEmailAddresses;

            if (String.isNotBlank(sUsersInEMailCC)) {           
                objOutboundEmail.CC_Email_Address__c = sUsersInEMailCC;
            }

            lstOutboundEmailsToInsert.add(objOutboundEmail);

            if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                insert lstOutboundEmailsToInsert;
                system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
                HEP_Send_Emails.sendOutboundEmails(lstOutboundEmailsToInsert);
            }
        }
        List<HEP_Promotion__c> lstNewPromotion = [Select id from HEP_Promotion__c where id =: toReturn];
        lstUserId.addAll(setUserId);
        if(!lstUserId.isEmpty() && !lstNewPromotion.isEmpty()){
            HEP_Notification_Utility.createNotificationAlerts(lstNewPromotion,HEP_Utility.getConstantValue('GLOBAL_PROMOTION_CREATION'),lstUserId);
        }
        
    }
}