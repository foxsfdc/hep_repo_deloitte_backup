/**
 * HEP_Auto_Lock_Promotion_Dating_Batch -- This class gets the details of all the Promotion Datings that are confirmed and sends an email to the Predefined set of Users   
 * @author    Lakshman Jinnuri
 */
global class HEP_Auto_Lock_Promotion_Dating_Batch implements Database.Batchable < sObject > {

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // selects all the promotion dating records that are in Global & national which are not (reconfirm, not released, confirmed)

        String query = 'SELECT Promotion__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c =\'Active\' AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null AND Status__c  !=\'Reconfirm\' AND Status__c  !=\'Confirmed\' AND Record_Status__c =\'Active\' AND Status__c  !=\'Not Released\' AND FAD_Approval_Status__c !=\'Rejected\' AND FAD_Approval_Status__c !=\'Pending\' AND ((Promotion__r.Promotion_Type__c = \'National\') OR (Promotion__r.Promotion_Type__c = \'Global\')) and Locked_Status__c=\'Unlocked\'';
        query = 'Select Id from hep_Promotion__c  where id in (' + query + ')';
        //      query = query+ ' limit 100';
        //  List<HEP_Promotion_Dating__c> test = Database.Query(query);
        //  system.debug('query '+ test.size() );
        return Database.getQueryLocator(query);
    }
    /**
     * execute -- get the details cutoff date on territory and locks the promotional dating records
     * @author  :  Lakshman Jinnuri
     * @return no return value
     */
    global void execute(Database.BatchableContext BC, List < sObject > scope) {
        try {
            system.debug('scope ' + scope.size());


            Map < String, HEP_Promotion_Dating__c > mapPromotionDating = new Map < String, HEP_Promotion_Dating__c > (); // for global promotions
            //List < HEP_Promotion_Dating__c > globalNullPromotionDating = new List < HEP_Promotion_Dating__c > ();

            //List to hold all records which will be locked in after 7 days.
            //List < HEP_Promotion_Dating__c > lstRecordsToSendLockReminders = new List < HEP_Promotion_Dating__c > ();
            Map < String, HEP_Promotion_Dating__c > mapRecordsToSendLockReminders = new Map < String, HEP_Promotion_Dating__c > ();
            //List < HEP_Promotion_Dating__c > lstGlobalNullPromotionDatingToSendLockReminders = new List < HEP_Promotion_Dating__c > ();

            Set < Id > setNewReleaseId = new Set < Id > ();
            Set < Id > setGlobalPromotionId = new Set < Id > ();
            Set < Id > setPromotionWithDateId = new Set < Id > ();
            Set < Id > setPromotionWithoutDateId = new Set < Id > ();
            Set < Id > setPromotionDatesFAD = new Set < Id > ();
            Set < Id > setallScopePromotion = new Set < Id > ();
            Set < Id > setNationalTVDigitalDates = new Set < Id > ();
            Set < Id > setNationalTVPhysicalDates = new Set < Id > ();
            Set < Id > setNationalCatalogDates = new Set < Id > ();

            //Set for Alerts
            Set < Id > setNationalTVDigitalDatesLockReminder = new Set < Id > ();
            Set < Id > setNationalTVPhysicalDatesLockReminder = new Set < Id > ();
            Set < Id > setNationalCatalogDatesLockReminder = new Set < Id > ();
            Set < Id > setGlobalPromotionIdLockReminder = new Set < Id > ();
            Set < Id > setNewReleaseIdLockReminder = new Set < Id > ();


            for (sobject objPromotionDating: [SELECT Id, Promotion__c, FAD_Approval_Status__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Line_of_Business_Type__c, territory__r.type__c, Promotion__r.Promotion_Type__c, Promotion__r.LocalPromotionCode__c, Promotion__r.MinPromotionDt_Date__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__r.PromotionName__c, Date__c, Date_Type__c, LastModifiedById, LastModifiedDate, HEP_Promotions_Dating_Matrix__r.Media_Type__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                    AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null AND Status__c != 'Reconfirm'
                    AND Status__c != 'Confirmed'
                    AND Record_Status__c = 'Active'
                    AND Status__c != 'Not Released'
                    AND FAD_Approval_Status__c != 'Rejected'
                    AND FAD_Approval_Status__c != 'Pending'
                    AND((Promotion__r.Promotion_Type__c = 'National') OR(Promotion__r.Promotion_Type__c = 'Global')) and Locked_Status__c = 'Unlocked'
                    and Promotion__c =: scope]) 
            {

                HEP_Promotion_Dating__c PromotionDating = (HEP_Promotion_Dating__c) objPromotionDating;

                setallScopePromotion.add(PromotionDating.Promotion__c);

                // logic for global promotions and FAD Approval is not pending OR Rejected
                // update from FAD Approved to FAD Rejected
                if (HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c) && PromotionDating.HEP_Promotions_Dating_Matrix__r.Line_of_Business_Type__c == HEP_Constants__c.getValues('PROMOTION_LOB_TV').Value__c) {
                    Integer idaysToLock = Integer.valueOf(promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (PromotionDating.Date__c != null && System.today().daysBetween(promotionDating.Date__c) <= promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c && HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c) && (!HEP_constants__c.getValues('HEP_APPROVAL_PENDING').Value__c.equals(PromotionDating.FAD_Approval_Status__c) && !HEP_constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c.equals(promotionDating.FAD_Approval_Status__c))) {
                        if (PromotionDating.HEP_Promotions_Dating_Matrix__r.Media_Type__c == HEP_Constants__c.getValues('HEP_DIGITAL_MEDIA_TYPE').Value__c)
                            setNationalTVDigitalDates.add(PromotionDating.Promotion__c);
                        else if (PromotionDating.HEP_Promotions_Dating_Matrix__r.Media_Type__c == HEP_Constants__c.getValues('HEP_PHYSICAL_MEDIA_TYPE').Value__c)
                            setNationalTVPhysicalDates.add(PromotionDating.Promotion__c);
                    }

                    //If record is Unlocked and will be locked in (offset+7) days, add the record in set.
                    if (PromotionDating.Date__c != null && System.today().daysBetween(promotionDating.Date__c) == idaysToLock && HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c) && (!HEP_constants__c.getValues('HEP_APPROVAL_PENDING').Value__c.equals(PromotionDating.FAD_Approval_Status__c) && !HEP_constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c.equals(promotionDating.FAD_Approval_Status__c))) {
                        if (PromotionDating.HEP_Promotions_Dating_Matrix__r.Media_Type__c == HEP_Constants__c.getValues('HEP_DIGITAL_MEDIA_TYPE').Value__c)
                            setNationalTVDigitalDatesLockReminder.add(PromotionDating.Promotion__c);
                        else if (PromotionDating.HEP_Promotions_Dating_Matrix__r.Media_Type__c == HEP_Constants__c.getValues('HEP_PHYSICAL_MEDIA_TYPE').Value__c)
                            setNationalTVPhysicalDatesLockReminder.add(PromotionDating.Promotion__c);
                    }

                }
                if ((HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c) && PromotionDating.HEP_Promotions_Dating_Matrix__r.Line_of_Business_Type__c == HEP_Constants__c.getValues('PROMOTION_LOB_CATALOG').Value__c) && (!HEP_constants__c.getValues('HEP_APPROVAL_PENDING').Value__c.equals(PromotionDating.FAD_Approval_Status__c) && !HEP_constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c.equals(promotionDating.FAD_Approval_Status__c))) {
                    Integer idaysToLock = Integer.valueOf(promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (PromotionDating.Date__c != null && System.today().daysBetween(promotionDating.Date__c) <= promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c && HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c)) {
                        setNationalCatalogDates.add(PromotionDating.Promotion__c);
                    }
                    //If record is Unlocked and will be locked in (offset+7) days, add the record in set.
                    if (PromotionDating.Date__c != null && System.today().daysBetween(promotionDating.Date__c) == idaysToLock && HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c)) {
                        setNationalCatalogDatesLockReminder.add(PromotionDating.Promotion__c);
                    }
                }
                if ((HEP_Constants__c.getValues('HEP_PROMOTION_TYPE_GLOBAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c)) && (!HEP_constants__c.getValues('HEP_APPROVAL_PENDING').Value__c.equals(PromotionDating.FAD_Approval_Status__c) && !HEP_constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c.equals(promotionDating.FAD_Approval_Status__c)))
                // add another OR for logic for National Dating and LOB  equal to Catalog OR TV to use same logic as Global Promotion 
                {
                    Integer idaysToLock = Integer.valueOf(promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    // if Dates exist, Date Type Exists and the difference between Todays date and the Dating Record's date is less than or Equal to the 
                    // cut of date on the Date Matrix record then add the promotion for locking (if any dating record is to be locked lock all)
                    if (PromotionDating.Date__c != null && PromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && System.today().daysBetween(promotionDating.Date__c) <= promotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c && !HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c)) {
                        // if National && TV SKip go to else
                        setGlobalPromotionId.add(PromotionDating.Promotion__c);
                        // else if Media type is DIdital
                        //  put it to a Set for DIgital Ids
                        //  elsse put it into set for Physical
                    }
                    //If record is Unlocked and will be locked in (offset+7) days, add the record in set.
                    if (PromotionDating.Date__c != null && PromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && System.today().daysBetween(promotionDating.Date__c) == idaysToLock && !HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c)) {
                        setGlobalPromotionIdLockReminder.add(PromotionDating.Promotion__c);
                    }
                } // logic for National Dating   
                // needs to be modified for LOB New Release Only      
                else if (HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(PromotionDating.Promotion__r.Promotion_Type__c) && PromotionDating.Date__c != null && PromotionDating.HEP_Promotions_Dating_Matrix__r.Line_of_Business_Type__c == HEP_Constants__c.getValues('PROMOTION_LOB_NEW_RELEASE').Value__c && (!HEP_constants__c.getValues('HEP_APPROVAL_PENDING').Value__c.equals(PromotionDating.FAD_Approval_Status__c) && !HEP_constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c.equals(promotionDating.FAD_Approval_Status__c))) { //if Dates exist, Date Type Exists and the difference between Todays date and the Dating Record's date is less than or Equal to the 
                    Integer idaysToLock = Integer.valueOf(PromotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (PromotionDating.Promotion__r != null && System.today().daysBetween(PromotionDating.Date__c) <= PromotionDating.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {
                        setNewReleaseId.add(PromotionDating.Id);
                    }
                    //If record is Unlocked and will be locked in (offset+7) days, add the record in set.
                    if (PromotionDating.Promotion__r != null && System.today().daysBetween(PromotionDating.Date__c) == idaysToLock) {
                        setNewReleaseIdLockReminder.add(PromotionDating.Id);
                    }

                }
                // set to get all global promotions that have atleast 1 date populated on the dating records
                if (PromotionDating.Promotion__r.MinPromotionDt_Date__c == null) {
                    setPromotionWithoutDateId.add(PromotionDating.Promotion__c);
                }

            }
            System.debug('setNewReleaseId ' + setNewReleaseId + ' @#$%^&* #$%^&*( setNationalTVPhysicalDates ' + setNationalTVPhysicalDates + ' $%^&*( ' + ' setNationalTVDigitalDates ' + setNationalTVDigitalDates);
            system.debug('@@@@@@@@@@@@@@@@####' + json.serialize(setPromotionWithDateId));
            // search to exclude the promotions that have at least 1 date populated in the dating records from all the promotions in the code 
            for (HEP_Promotion_Dating__c objPromotionDatesNull: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, Promotion__r.Promotion_Type__c, Promotion__c, promotion__r.FirstAvailableDate__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Locked_Date__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                    AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null AND Date__c = null AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND Promotion__r.Promotion_Type__c =: HEP_Constants__c.getValues('HEP_PROMOTION_TYPE_GLOBAL').Value__c AND Locked_Status__c = 'Unlocked'
                    AND Promotion__c in: setPromotionWithoutDateId
                ]) {
                if (objPromotionDatesNull.promotion__r.FirstAvailableDate__c != null && objPromotionDatesNull.HEP_Promotions_Dating_Matrix__c != null) {
                    Integer idaysToLock = Integer.valueOf(objPromotionDatesNull.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (System.today().daysBetween(objPromotionDatesNull.promotion__r.FirstAvailableDate__c) <= objPromotionDatesNull.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {
                        objPromotionDatesNull.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                        objPromotionDatesNull.Status__c = HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c;
                        //globalNullPromotionDating.add(objPromotionDatesNull);
                        mapPromotionDating.put(objPromotionDatesNull.Id, objPromotionDatesNull);
                    }
                    //If record is Unlocked and will be locked in (offset+7) days, add the record in set.
                    if (System.today().daysBetween(objPromotionDatesNull.promotion__r.FirstAvailableDate__c) == idaysToLock) {
                        //Date dReleaseDate = objPromotionDatesNull.promotion__r.FirstAvailableDate__c;
                        Date dReleaseDate = Date.today();
                        objPromotionDatesNull.Locked_Date__c = dReleaseDate.addDays(7);
                        //lstGlobalNullPromotionDatingToSendLockReminders.add(objPromotionDatesNull);
                        mapRecordsToSendLockReminders.put(objPromotionDatesNull.Id, objPromotionDatesNull);
                    }
                }
            }
            // Getting all the dating records that were added to the set above for global Dating locking
            // add logic for global promotions and FAD Approval is not pending OR Rejected
            // GLobal + National Catalog
            for (HEP_Promotion_Dating__c objPromotion: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c, FAD_Approval_Status__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                    AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c AND Locked_Status__c = 'Unlocked'
                    AND(Promotion__r.id in: setGlobalPromotionId OR Promotion__r.id in: setNationalCatalogDates)
                ]) {
                if (objPromotion.Date__c != null && System.today().daysBetween(objPromotion.Date__c) <= objPromotion.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {
                    objPromotion.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                    objPromotion.Status__c = HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c;
                    mapPromotionDating.put(objPromotion.Id, objPromotion);
                } else if(objPromotion.Date__c == null) {
                    objPromotion.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                    objPromotion.Status__c = HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c;
                    //lstPromotionDating.add(objPromotion);
                    mapPromotionDating.put(objPromotion.Id, objPromotion);
                }
            }
            //Sending Reminder Alerts prior to 1 week Logic for GLobal + National Catalog
            for (HEP_Promotion_Dating__c objPromotion: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c, FAD_Approval_Status__c, Locked_Date__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                    AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c AND Locked_Status__c = 'Unlocked'
                    AND(Promotion__r.id in: setGlobalPromotionIdLockReminder OR Promotion__r.id in: setNationalCatalogDatesLockReminder)
                ]) {
                Integer idaysToLock = Integer.valueOf(objPromotion.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                if (objPromotion.Date__c != null && System.today().daysBetween(objPromotion.Date__c) == idaysToLock) {
                    //Date dReleaseDate = objPromotion.Date__c;
                    Date dReleaseDate = Date.today();
                    objPromotion.Locked_Date__c = dReleaseDate.addDays(7);
                    //lstRecordsToSendLockReminders.add(objPromotion);
                    mapRecordsToSendLockReminders.put(objPromotion.Id, objPromotion);
                }
            }

            System.debug('setNationalTVDigitalDates' + setNationalTVDigitalDates);
            System.debug('setNationalTVDigitalDatesLockReminder' + setNationalTVDigitalDatesLockReminder);
            //Locking the dates for TV Physical and New Release
            if (!setNationalTVDigitalDates.isEmpty()) {
                for (HEP_Promotion_Dating__c objPromotionTVDigital: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                        AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c) AND HEP_Promotions_Dating_Matrix__r.Media_Type__c =: HEP_Constants__c.getValues('HEP_DIGITAL_MEDIA_TYPE').Value__c AND Locked_Status__c = 'Unlocked'
                        AND Promotion__r.id in: setNationalTVDigitalDates
                    ]) {
                    if (objPromotionTVDigital.Date__c != null && System.today().daysBetween(objPromotionTVDigital.Date__c) <= objPromotionTVDigital.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {
                        objPromotionTVDigital.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                        objPromotionTVDigital.Status__c = HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c;
                        // lstPromotionDating.add(objPromotionTVDigital);
                        mapPromotionDating.put(objPromotionTVDigital.Id, objPromotionTVDigital);
                    } else if(objPromotionTVDigital.Date__c == null) {
                        objPromotionTVDigital.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                        objPromotionTVDigital.Status__c = HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c;
                        // lstPromotionDating.add(objPromotionTVDigital);
                        mapPromotionDating.put(objPromotionTVDigital.Id, objPromotionTVDigital);
                    }
                }
            }
            //Sending Reminder Alerts prior to 1 week Logic for TV Digital and New Release
            if (!setNationalTVDigitalDatesLockReminder.isEmpty()) {
                for (HEP_Promotion_Dating__c objPromotionTVDigital: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c, Locked_Date__c FROM HEP_Promotion_Dating__c WHERE(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c) AND HEP_Promotions_Dating_Matrix__r.Media_Type__c =: HEP_Constants__c.getValues('HEP_DIGITAL_MEDIA_TYPE').Value__c AND Locked_Status__c = 'Unlocked'
                        AND Promotion__r.id in: setNationalTVDigitalDatesLockReminder
                    ]) {
                    Integer idaysToLock = Integer.valueOf(objPromotionTVDigital.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (objPromotionTVDigital.Date__c != null && System.today().daysBetween(objPromotionTVDigital.Date__c) == idaysToLock) {
                        //Date dReleaseDate = objPromotionTVDigital.Date__c;
                        Date dReleaseDate = Date.today();
                        objPromotionTVDigital.Locked_Date__c = dReleaseDate.addDays(7);
                        //lstRecordsToSendLockReminders.add(objPromotionTVDigital);
                        mapRecordsToSendLockReminders.put(objPromotionTVDigital.Id, objPromotionTVDigital);

                    }
                }
            }
            if (!setNationalTVPhysicalDates.isEmpty()) {
                for (HEP_Promotion_Dating__c objPromotionTVPhysical: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                        AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c) AND HEP_Promotions_Dating_Matrix__r.Media_Type__c =: HEP_Constants__c.getValues('HEP_PHYSICAL_MEDIA_TYPE').Value__c AND Locked_Status__c = 'Unlocked'
                        AND Promotion__r.id in: setNationalTVPhysicalDates
                    ]) {
                    if (objPromotionTVPhysical.Date__c != null && System.today().daysBetween(objPromotionTVPhysical.Date__c) <= objPromotionTVPhysical.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {
                        objPromotionTVPhysical.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                        objPromotionTVPhysical.Status__c = HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c;
                        //  lstPromotionDating.add(objPromotionTVPhysical);
                        mapPromotionDating.put(objPromotionTVPhysical.Id, objPromotionTVPhysical);
                    } else if(objPromotionTVPhysical.Date__c == null) {
                        objPromotionTVPhysical.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                        objPromotionTVPhysical.Status__c = HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c;
                        //  lstPromotionDating.add(objPromotionTVPhysical);
                        mapPromotionDating.put(objPromotionTVPhysical.Id, objPromotionTVPhysical);
                    }
                }
            }
            //Sending Reminder Alerts prior to 1 week Logic for TV Digital
            if (!setNationalTVPhysicalDatesLockReminder.isEmpty()) {
                for (HEP_Promotion_Dating__c objPromotionTVPhysical: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c, Promotion__c, Locked_Date__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                        AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c) AND HEP_Promotions_Dating_Matrix__r.Media_Type__c =: HEP_Constants__c.getValues('HEP_PHYSICAL_MEDIA_TYPE').Value__c AND Locked_Status__c = 'Unlocked'
                        AND Promotion__r.id in: setNationalTVPhysicalDatesLockReminder
                    ]) {
                    Integer idaysToLock = Integer.valueOf(objPromotionTVPhysical.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (objPromotionTVPhysical.Date__c != null && System.today().daysBetween(objPromotionTVPhysical.Date__c) == idaysToLock) {
                        //Date dReleaseDate = objPromotionTVPhysical.Date__c;
                        Date dReleaseDate = Date.today();
                        objPromotionTVPhysical.Locked_Date__c = dReleaseDate.addDays(7);
                        //lstRecordsToSendLockReminders.add(objPromotionTVPhysical);
                        mapRecordsToSendLockReminders.put(objPromotionTVPhysical.Id, objPromotionTVPhysical);
                    }
                }
            }

            // Search for above global code search all dating recorods in both the set for Physical / Digital for national

            //{
            //  if the Physical set then logic
            //  else if present in DIgital set then lock
            //  else skip

            //}
            //  system.debug('121212121' + lstPromotionDating);     
            // logic for national New Release
            for (HEP_Promotion_Dating__c objDatingPromotion: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c,HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                    AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c) AND Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c AND Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c AND Date__c != null AND Promotion__r.Promotion_Type__c =: HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c AND Locked_Status__c = 'Unlocked'
                    AND Id in: setNewReleaseId
                ]) {
                if (System.today().daysBetween(objDatingPromotion.Date__c) <= objDatingPromotion.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) {    
                    objDatingPromotion.Locked_Status__c = HEP_Constants__c.getValues('HEP_PROMOTION_STATUS_LOCKED').Value__c;
                    objDatingPromotion.Status__c = HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c;
                    // lstPromotionDating.add(objDatingPromotion);
                    mapPromotionDating.put(objDatingPromotion.Id, objDatingPromotion);
                }
            }
            if (!setNewReleaseIdLockReminder.isEmpty()) {
                //Sending Reminder Alert logic for national New Release
                for (HEP_Promotion_Dating__c objDatingPromotion: [SELECT Id, Locked_Status__c, Status__c, Date__c, HEP_Promotions_Dating_Matrix__r.Date_Type__c, Locked_Date__c, HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c FROM HEP_Promotion_Dating__c WHERE HEP_Promotions_Dating_Matrix__r.Record_Status__c = 'Active'
                        AND HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c != null AND HEP_Promotions_Dating_Matrix__c != null And Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND(FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c AND FAD_Approval_Status__c !=: HEP_Constants__c.getValues('HEP_APPROVAL_PENDING').Value__c) AND Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_NOT_RELEASED').Value__c AND Status__c !=: HEP_Constants__c.getValues('HEP_STATUS_CONFIRMED').Value__c AND Date__c != null AND Promotion__r.Promotion_Type__c =: HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c AND Locked_Status__c = 'Unlocked'
                        AND Id in: setNewReleaseIdLockReminder
                    ]) {
                    Integer idaysToLock = Integer.valueOf(objDatingPromotion.HEP_Promotions_Dating_Matrix__r.Auto_Lock_Cut_Off__c) + 7;
                    if (System.today().daysBetween(objDatingPromotion.Date__c) == idaysToLock) {
                        //Date dReleaseDate = objDatingPromotion.Date__c;
                        Date dReleaseDate = Date.today();
                        objDatingPromotion.Locked_Date__c = dReleaseDate.addDays(7);
                        //lstRecordsToSendLockReminders.add(objDatingPromotion);
                        mapRecordsToSendLockReminders.put(objDatingPromotion.Id, objDatingPromotion);
                    }
                }
            }


            System.debug('mapPromotionDating #$%%^&*( ' + mapPromotionDating.values());
            System.debug('datingRecordsForUpdate #$%%^&*( ' + mapPromotionDating.size());


            if (!mapPromotionDating.isempty()) {
                update mapPromotionDating.values();
                // The Siebel queueable is chained inside the JDE as JDE handles only 1 promotion at a time
                System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(scope[0].Id, new List < String > (mapPromotionDating.keyset())));

                //          if(HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING') != null && HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c != null) 
                //          System.enqueueJob(new HEP_Siebel_Queueable(String.join(new List<String>(mapPromotionDating.keyset()), ',') , HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c));


            }

            system.debug('Record for which reminder needs to be sent :' + mapRecordsToSendLockReminders.values());
            system.debug('Number of records for which reminder needs to be sent :' + mapRecordsToSendLockReminders.size());
            if (!mapRecordsToSendLockReminders.isempty())
                update mapRecordsToSendLockReminders.values();

            if (!mapRecordsToSendLockReminders.isempty()) {
                HEP_Promotion_Dating_Batch_Handler.sendReminderEmail(mapRecordsToSendLockReminders.values(), HEP_Utility.getConstantValue('HEP_Date_Lock_Alert'));
            }

            if (!mapPromotionDating.isempty()) {
                HEP_Promotion_Dating_Batch_Handler.sendReminderEmail(mapPromotionDating.values(), HEP_Utility.getConstantValue('HEP_Date_Locked_Notification'));
            }
        } catch (exception ex) {
            HEP_Error_Log.genericException('Auto Lock LIO and National Dates', 'DML Errors', ex, 'HEP_Auto_Lock_Promotion_Dating_Batch', 'execute', null, null);
            throw ex;
        }

    }

    global void finish(Database.BatchableContext BC) {

    }
}