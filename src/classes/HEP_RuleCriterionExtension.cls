/* Class Name   : HEP_RuleCriterionExtension
 * Description  :    
 * Created By   : Nidhin V K
 * Created On   : 07-26-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-26-2016        1000            Initial objRule  
 *
 */
public class HEP_RuleCriterionExtension {
    
    public List<SelectOption> lstTaskFields{get; set;}
    
    public List<SelectOption> lstTargetObjectFields{get; set;}

    public Id selectedVal{get;set;}
    
    public Boolean bShowTask{get; set;}
    
    public Boolean bShowError{get; set;}

    public Boolean bShowValueCompare{get; set;}

    public Boolean bShowMapValue{get;set;}

    public Boolean bShowSourceField{get;set;}

    public Boolean bShowDueDate{get;set;}
    
    public HEP_Rule_Criterion__c objRuleCriteria;

    public String sDaysToAdd{get;set;}
    
    HEP_Rule__c objRule;
    
    public String selectedRTName;
    
    
    /* HEP_RuleCriteriaExtension constructor does the basic functionalities like 
     * initialization of objects and get some needed data
     * Enable/Disable Buttons
     * Show/Hide fields
     * @params : controller
     */
    public HEP_RuleCriterionExtension(ApexPages.StandardController controller) {
        
        
        //Get the fileds which are not used in the Page
        if(!Test.isRunningTest())
            controller.addFields(new List<String>{HEP_Rule_Constants.HEP_FIELD_RULE, HEP_Rule_Constants.HEP_FIELD_SL_NO});
        this.objRuleCriteria = (HEP_Rule_Criterion__c)controller.getRecord();
        System.debug(objRuleCriteria);

        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('RecordType')) &&
            ApexPages.currentPage().getParameters().get('RecordType') != null){
            selectedVal = ApexPages.currentPage().getParameters().get('RecordType');
        }
        else{
            selectedVal = objRuleCriteria.RecordTypeId;
        }    

        System.debug('selectedVal' +selectedVal);

        lstTargetObjectFields = new List<SelectOption>();
        
        //Get the selected Rule details
        objRule = [SELECT
                            Id,
                            HEP_Target_SObject_API_Name__c,
                            HEP_IsActive__c,
                            (
                                SELECT Id FROM HEP_Rule_Criteria__r
                            )
                        FROM 
                            HEP_Rule__c
                        WHERE
                            Id =: objRuleCriteria.HEP_Rule__c];
                            
                            //System.debug('RuleVer--->' +HEP_Rule__c.HEP_Target_SObject_API_Name__c);
        
        //If active then show message while editing
        if(objRule.HEP_IsActive__c){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.HEP_LABEL_DEACTIVATE_RULE));
            bShowError = true;
            return;
        }
        
        //Populate Task Fields
        //lstTaskFields = new List<SelectOption>{new SelectOption(HEP_Rule_Constants.HEP_EMPTY, HEP_Rule_Constants.HEP_NONE)};
        lstTaskFields = new List<SelectOption>();
        //lstTaskFields.addAll(populateSObjectFields(HEP_Rule_Constants.HEP_OBJECT_TASK));
        
        //Populate Target Sobejct Fields
        lstTargetObjectFields = populateSObjectFields(objRule.HEP_Target_SObject_API_Name__c);
        
        Map<Id, Schema.RecordTypeInfo> mapRTbyId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosById();
        if(mapRTbyId.containsKey(selectedVal)){ 
            Schema.RecordTypeInfo selectedRTInfo =  mapRTbyId.get(selectedVal);
            selectedRTName = selectedRTInfo.getName();
        }
        enableDisableTaskField();
        
    }
    
    /* enableDisableTaskField 
     * Action support method for enabling and disabling task and value compare field
     * @params : Not Applicable
     * @return : null
     */
    public PageReference enableDisableTaskField() {
        System.debug('selectedRTName>>' + selectedRTName);
        if(HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_SOBJECT.equals(selectedRTName))
        {
            bShowTask = true;
            bShowMapValue = false;
            bShowValueCompare = false;
            bShowSourceField = true;
            bShowDueDate = false;

        }else if(HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE.equals(selectedRTName)){
            bShowTask = false;
            bShowMapValue = false;
            bShowValueCompare = true;
            bShowSourceField =true;
            bShowDueDate = false;

        }else if(HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_STATIC.equals(selectedRTName)){
            bShowTask = true;
            bShowMapValue = true;
            bShowValueCompare = false;
            bShowSourceField = false;
            bShowDueDate = false;

            if(objRuleCriteria.HEP_Target_Task_Field__c == HEP_Rule_Constants.HEP_FIELD_DUE_DATE){
                sDaysToAdd = objRuleCriteria.HEP_Value_to_Map__c;
                bShowDueDate = true;
                bShowMapValue = false;
            }
        }

        return null;
    }

    /* populateSObjectFields 
     * Populate the sobject fileds based on the object API Name
     * @params : strObjName
     * @return : List<SelectOption> lstSObjectFields
     */
    public List<SelectOption> populateSObjectFields(String strObjName){
        
        List<SelectOption> lstSObjectFields = new List<SelectOption>(); 
        
        try {
        
            //Populate the sObject Fields
            System.debug('strObjName>>' + strObjName);
            Map<String, Schema.SObjectField> mapObjectFields 
                    = Schema.getGlobalDescribe().get(strObjName).getDescribe().fields.getMap();
            
            //Return all the fields
            for(Schema.SObjectField fld :mapObjectFields.values()){
                schema.describeFieldResult dfield = fld.getDescribe();
                if((dfield.isUpdateable()
                    && dfield.isCreateable())||dfield.isCalculated()){
                    
                    String sFieldName = dfield.getName();
                    if(!HEP_Rule_Constants.HEP_RESTRICTED_FIELDS.contains(sFieldName))        
                        lstSObjectFields.add(new SelectOption(sFieldName, dfield.getLabel()));
                }
            }
        
        } catch(Exception ex){
        	HEP_Error_Log.genericException('Rule Criteria Errors','DML Errors',ex,'HEP_RuleCriteriaExtension','populateSObjectFields',null,null);
            //throw error
            System.debug('Exception on Class : HEP_RuleCriteriaExtension - populateSObjectFields, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            
            bShowError = true;             
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.HEP_LABEL_CONTACT_ADMIN));

            return NULL;
        }
        
        return lstSObjectFields;
    }
    

    
    /* save 
     * Override save method to save the record with additional custom values
     * @params :
     * @return :
     */
    public PageReference save(){
        
        try{
            //Show error if sObject field is NULL
            if(String.isBlank(objRuleCriteria.HEP_Source_SObject_Field__c) && (HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_SOBJECT.equals(selectedRTName)
                                || HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE.equals(selectedRTName)) ){
                bShowError = true;             
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.HEP_LABEL_NO_FIELDS_SELECTED));
                return NULL;
            }


            objRuleCriteria.RecordTypeId = selectedVal;
            
            if(HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_STATIC.equals(selectedRTName)){
                objRuleCriteria.HEP_Source_SObject_Field_Type__c = HEP_Rule_Constants.HEP_EMPTY;
                objRuleCriteria.HEP_Source_SObject_Field__c = HEP_Rule_Constants.HEP_EMPTY;
                objRuleCriteria.HEP_Value_To_Compare__c = HEP_Rule_Constants.HEP_EMPTY;
                objRuleCriteria.HEP_Operator__c = HEP_Rule_Constants.HEP_EMPTY;

               if(objRuleCriteria.HEP_Target_Task_Field__c == HEP_Rule_Constants.HEP_FIELD_DUE_DATE){
                    objRuleCriteria.HEP_Value_to_Map__c = sDaysToAdd;
                }
            }
            else if(HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_SOBJECT.equals(selectedRTName)){
                objRuleCriteria.HEP_Value_to_Map__c = HEP_Rule_Constants.HEP_EMPTY;
                objRuleCriteria.HEP_Value_To_Compare__c = HEP_Rule_Constants.HEP_EMPTY;
                objRuleCriteria.HEP_Operator__c = HEP_Rule_Constants.HEP_EMPTY;

                //Populate the data type of the field
                Schema.DisplayType dType 
                    = Schema.getGlobalDescribe().get(objRule.HEP_Target_SObject_API_Name__c)
                    .getDescribe().fields.getMap().get(objRuleCriteria.HEP_Source_SObject_Field__c)
                    .getDescribe().getType();
            
                objRuleCriteria.HEP_Source_SObject_Field_Type__c = dType.name();
            }
            else if(HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE.equals(selectedRTName)){

                //Populate the data type of the field
                Schema.DisplayType dType 
                    = Schema.getGlobalDescribe().get(objRule.HEP_Target_SObject_API_Name__c)
                    .getDescribe().fields.getMap().get(objRuleCriteria.HEP_Source_SObject_Field__c)
                    .getDescribe().getType();
            
                objRuleCriteria.HEP_Source_SObject_Field_Type__c = dType.name();
                objRuleCriteria.HEP_Value_to_Map__c = HEP_Rule_Constants.HEP_EMPTY;  
                
                //Get only the compare rule criterias
                List<HEP_Rule_Criterion__c> lstCompareCriteria = [SELECT HEP_Rule__c, 
                                        HEP_Serial_Number__c, 
                                        Id,
                                        RecordTypeId FROM HEP_Rule_Criterion__c 
                                        WHERE HEP_Rule__c =: objRuleCriteria.HEP_Rule__c 
                                        AND RecordType.Name  =: HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE];
                
                System.debug('<<This block>>'+objRuleCriteria.HEP_Serial_Number__c);
                
                if(objRuleCriteria.HEP_Serial_Number__c == null){
                    //auto populate the serial number for Compare Record Type
                    Set<Integer> setSlNos = new Set<Integer>();
                    objRuleCriteria.HEP_Serial_Number__c = 1;
                    
                    for(HEP_Rule_Criterion__c objRC : lstCompareCriteria){
                        setSlNos.add(Integer.valueOf(objRC.HEP_Serial_Number__c));
                    }
                    Integer num = 1;
                    while(setSlNos.contains(num)){
                        objRuleCriteria.HEP_Serial_Number__c = ++num;
                    }  
                }  
                
            }

            System.debug('<<This block>>'+objRuleCriteria.HEP_Serial_Number__c);

            upsert objRuleCriteria;

        } catch(Exception ex){
        	HEP_Error_Log.genericException('Rule Criteria Save Errors','DML Errors',ex,'HEP_RuleCriteriaExtension','save',null,null);
            //throw error
            System.debug('Exception on Class : HEP_RuleCriteriaExtension - save, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            
            bShowError = true;             
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.HEP_LABEL_CONTACT_ADMIN));

            return NULL;
        }
        
        return new PageReference(HEP_Rule_Constants.HEP_URL_VIEW_RULE + '?id=' + objRuleCriteria.HEP_Rule__c);
    }
    
    
    /* deleteCriteria 
     * Delete Criteria and redirect to Rule Version View VF Page
     * @params :
     * @return : PageReference
     */
    public PageReference deleteCriteria(){
        try{
            //delete if it is not null
            if(objRuleCriteria != NULL){
                delete objRuleCriteria;
            }
            return new PageReference(HEP_Rule_Constants.HEP_URL_VIEW_RULE + '?id=' + objRuleCriteria.HEP_Rule__c);
        }catch(Exception ex){
        	HEP_Error_Log.genericException('Rule Criteria Delete Errors','DML Errors',ex,'HEP_RuleCriteriaExtension','save',null,null);
            return null;
        }
    }

}