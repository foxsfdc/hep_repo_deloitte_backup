/**
* HEP_InterfaceScheduler --- Class to schedule the execution of JDE Master Feed integration 
* @author    Avinash Harwani
*/
global class HEP_INT_DheSkuDetails_Scheduler implements Schedulable{
    
    public void execute(SchedulableContext sc){
    
        Map<String,String> mapClassToInterfaces = new Map<String,String>{'HEP_INT_DheSkuMaster_Details'=> 'HEP_JDE_SKU_MASTERDETAILS', 
                                                                         'HEP_INT_DheSkuPrice_Details'=> 'HEP_JDE_SKU_PRICEDETAILS',
                                                                         'HEP_INT_DheSkuBom_Details'=> 'HEP_JDE_SKU_BOMDETAILS',
                                                                         'HEP_INT_DheSkuCustomer_Details'=> 'HEP_JDE_SKU_CUSTOMERDETAILS'};
        Map<String,HEP_INT_DheSkuInvoiceWrapperUtility> mapClassToObjWrap = new Map<String,HEP_INT_DheSkuInvoiceWrapperUtility>();
        //Get records from custom setting and populate wrapper
        HEP_INT_DheSkuInvoiceWrapperUtility obj;
        for(String className : mapClassToInterfaces.keySet()){
            obj = new HEP_INT_DheSkuInvoiceWrapperUtility();
            HEP_Services__c service = HEP_Services__c.getInstance(mapClassToInterfaces.get(className));
            obj.sTimeStamp= service.Hep_Jde_Time__c;
            obj.sLastPulledDate= service.Hep_Jde_Date__c;
            obj.sNoOfRows= Label.HEP_JDE_PAGINATION;
            obj.sStartingFrom= Label.HEP_JDE_PAGINATION_START;
            mapClassToObjWrap.put(className,obj);
        }
        //Call Interface method with batches of 1000 Using queuable maybe
        System.enqueueJob(new HEP_INT_DheSku_Queueable(mapClassToInterfaces,mapClassToObjWrap, sc.getTriggerId()));
    }
}