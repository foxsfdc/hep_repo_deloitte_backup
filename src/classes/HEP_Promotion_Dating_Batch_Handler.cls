/**
 * Batch Handler class to send Email and Application alert of Promotion Dating records prior to 1 week before locking.
 * @author    Gaurav Mehrishi
 */
public class HEP_Promotion_Dating_Batch_Handler {
    public static String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');     
    /**
     * sendReminderEmail method - accepts the list of Unlocked and Active Promotion Dating Records and sends email prior to 1 week before locking
     * @param List of Promotion Dating
     * @return No return value.
     */
    public static void sendReminderEmail(List < HEP_Promotion_Dating__c > lstRecordsToSendLockReminders , String sAlertType) {
        //Send Email to Local Marketing Manager.              
        List < String > lstEmailAddresses = new List < String > ();
        List < Id > lstUserId = new List < Id > ();
        Set < Id > setUserId = new Set < Id > ();
        Set < Id > setPromotionDatingId = new set < Id > ();
        for (HEP_Promotion_Dating__c objPromoDating: lstRecordsToSendLockReminders) {
            setPromotionDatingId.add(objPromoDating.Id);
        }
        List < HEP_Promotion_Dating__c > lstPromotionDating = [Select Id, Locked_Date__c, Promotion__r.Domestic_Marketing_Manager__r.Email,
            Promotion__r.International_Marketing_Manager__r.Email,
            Promotion__r.Domestic_Marketing_Manager__c,
            Promotion__r.International_Marketing_Manager__c,
            Promotion__r.Territory__r.Name,
            Date_Type__c
            FROM HEP_Promotion_Dating__c
            WHERE Id IN: setPromotionDatingId
        ];
        if (sAlertType.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_Date_Lock_Alert'))) {
            if (!lstPromotionDating.isEmpty()) {
                //Create Outbound Email record before sending the Email.
                List < HEP_Outbound_Email__c > lstOutboundEmailsToInsert = new List < HEP_Outbound_Email__c > ();
                for (HEP_Promotion_Dating__c objPrDating: lstPromotionDating) {
                    HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c();
                    objOutboundEmail.To_Email_Address__c = objPrDating.Promotion__r.Domestic_Marketing_Manager__r.Email;
                    objOutboundEmail.Record_Id__c = objPrDating.Id;
                    objOutboundEmail.Object_API__c = objPrDating.Id.getSObjectType().getDescribe().getName();
                    objOutboundEmail.Email_Template_Name__c = 'HEP_Dating_Lock_Email_Reminder';
                    
                    //Fetch user need to kept in Email CC for the Territory: This will be coming from LOV Mapping 
                    String sUsersInEMailCC='';
                    String sTemplateDetails = 'HEP_Dating_Lock_Email_Reminder' + objPrDating.Promotion__r.Territory__r.Name;
                    for (HEP_List_Of_Values__c objTempLstRec: [Select Values__c, Name_And_Parent__c from HEP_List_Of_Values__c where Name_And_Parent__c =: sTemplateDetails AND Type__c = 'EMAIL_CC_USERS' And Record_Status__c =: sACTIVE]) {
                        if(sUsersInEMailCC=='')
                            sUsersInEMailCC = objTempLstRec.Values__c;
                        else    
                            sUsersInEMailCC = sUsersInEMailCC+ + ' ; ' + objTempLstRec.Values__c;
                    }
                    if (String.isNotBlank(sUsersInEMailCC)) {           
                        objOutboundEmail.CC_Email_Address__c = sUsersInEMailCC;
                    }
                    //Add in List
                    lstOutboundEmailsToInsert.add(objOutboundEmail);
                }

                if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                    insert lstOutboundEmailsToInsert;
                    system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
                    List < Id > lstOutboundEmails = new List < Id > ();
                    for (HEP_Outbound_Email__c objEmail: lstOutboundEmailsToInsert) {
                        lstOutboundEmails.add(objEmail.Id);
                    }
                    HEP_Send_Emails.sendOutboundEmails(lstOutboundEmails);
                }
            }
        }
        
        //Create Notification record before creating notification user action record
        
        List < HEP_Notification_User_Action__c > lstNotificationUsers = new List < HEP_Notification_User_Action__c > ();
        Map < String, HEP_Notification__c > mapNotifications = new Map < String, HEP_Notification__c > ();
        //String sNotifName = HEP_Utility.getConstantValue('HEP_Date_Lock_Alert');
        ID nType = lstPromotionDating[0].id;
        String sNotifType = nType.getSObjectType().getDescribe().getName();
        system.debug('>>>>>>>>>' + sNotifType);
        HEP_Notification_Template__c objHepNotifTemplate = HEP_Notification_Utility.getTemplate(sAlertType, sNotifType);
        String sNotifBody = objHepNotifTemplate.HEP_Body__c;
        for (HEP_Promotion_Dating__c objHPD: lstPromotionDating) {
            // Create Notification Records              
            HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
            objHepNewNotif.HEP_Message__c = HEP_Notification_Utility.CreateNotificationBody(objHPD, sNotifBody);
            objHepNewNotif.HEP_Template_Name__c = sAlertType;
            objHepNewNotif.HEP_Object_API__c = objHepNotifTemplate.HEP_Object__c;
            objHepNewNotif.Notification_Template__c = objHepNotifTemplate.id;
            objHepNewNotif.HEP_Record_ID__c = objHPD.id;
            mapNotifications.put(objHPD.id,objHepNewNotif);
        }
        if (mapNotifications != NULL && !mapNotifications.isEmpty()) {
            System.debug('Map-->' + mapNotifications);
            System.debug('Maps Keyset is-->' + mapNotifications.keySet());
            insert mapNotifications.values();
            
            for (HEP_Promotion_Dating__c objHPD: lstPromotionDating) {
                if (mapNotifications.containsKey(objHPD.id)) {
                    String sParentRecordId = mapNotifications.get(objHPD.id).Id;  
                    system.debug('------------>' + sParentRecordId);                                                      
                    HEP_Notification_User_Action__c objNotificationUsers = new HEP_Notification_User_Action__c();
                    objNotificationUsers.Notification_User__c = objHPD.Promotion__r.Domestic_Marketing_Manager__c;
                    objNotificationUsers.HEP_Notification__c = sParentRecordId;
                    lstNotificationUsers.add(objNotificationUsers);                                                      
                }
            }
            if (lstNotificationUsers.size() > 0) {
                insert lstNotificationUsers;
                system.debug('lstNotificationAlerts----> ' + lstNotificationUsers);
            }
        }
    }  
}