/**
* HEP_HEP_iTETriggerHandler --- Interface Transaction Error Object trigger Handler
* @author    Abhishek Mishra
*/
Public Class HEP_InterfaceTxnErrorTriggerHandler{

   /**
    * List of Interface Transaction Error records after insert
    * @param lstInterfaceTransactionError interface transaction error records
    * @return void
    * @author Abhishek Mishra
    */
    public static void handleAfterInsert(List<HEP_Interface_Transaction_Error__c> lstInterfaceTransactionError){
        try{
            system.debug('Here in Transaction Error trigger 1');
            string sHEP_INTERFACE_TRANSACTION_ERROR = HEP_Utility.getConstantValue('HEP_INTERFACE_TRANSACTION_ERROR'); 
            //All Interface Transaction Error Emails
            List<Messaging.singleemailMessage> lstEmails = new List<Messaging.singleemailMessage>();
            //Email Template for Interface Transaction Error
            list<EmailTemplate> lstEmailTemplate = [Select Id from EmailTemplate where developername = :sHEP_INTERFACE_TRANSACTION_ERROR];
            system.debug('Here in Transaction Error trigger 2 : lstEmailTemplate : ' + lstEmailTemplate);
            if(lstEmailTemplate != null && !lstEmailTemplate.isEmpty()){
                //Code needs to be changed - Start - Mail to be sent to proper user
                List<string> lstToAddresses = new List<string>();
                if(string.isNotEmpty(System.Label.HEP_InterfaceTransactionError_NotificationEmailList))
                    lstToAddresses = System.Label.HEP_InterfaceTransactionError_NotificationEmailList.split(',');  
                //Email check to restrict Fox Email Ids
                lstToAddresses = HEP_Utility.getActualEmailIds(lstToAddresses);    
                //Code needs to be changed - End    
                system.debug('Here in Transaction Error trigger 3 : lstToAddresses : ' + lstToAddresses); 
                if(!lstToAddresses.isEmpty()){ 
                    //Fetching Target Transactin id
                    Id TargetObjectId = UserInfo.getUserId();
                    List<Messaging.Emailfileattachment> listFileAttachments = new List<Messaging.Emailfileattachment>();
                    //Fetching request and response from content object respective to transaction record
                    map<string, HEP_Interface_Transaction_Error__c> mapIdvsInterfaceTranErrors = new map<string, HEP_Interface_Transaction_Error__c>(lstInterfaceTransactionError);
                    set<string> setInterfaceTranErrorIds = mapIdvsInterfaceTranErrors.keyset();
                    set<string> setContentDocumentIds = new set<string>();
                    map<string, blob> mapReqResVsDocuments = new map<string, blob>();
                    //Fetch ContentDocumentLink records linked to the interface Transaction error 
                    List<ContentDocumentLink> lstContentDocumentLinks = [select id, ContentDocumentId from ContentDocumentLink where LinkedEntityId in :setInterfaceTranErrorIds];
                    for(ContentDocumentLink objConDocLink : lstContentDocumentLinks ){
                        setContentDocumentIds.add(objConDocLink.ContentDocumentId);
                    }
                    //Fetching Content version based on ContentDocumentId
                    List<ContentVersion> lstContentVersions = [select id, VersionData,Title from contentVersion where ContentDocumentId in :setContentDocumentIds];
                    //Filling map data for request and response 
                    for(ContentVersion objContentVersion : lstContentVersions){
                        mapReqResVsDocuments.put(objContentVersion.Title, objContentVersion.VersionData);
                    }
                    //Adding request response attachments to the Error Email
                    for(HEP_Interface_Transaction_Error__c objInterfaceTranError : lstInterfaceTransactionError){
                        //Mail attachmet for request
                        if(mapReqResVsDocuments.containsKey(HEP_Utility.getConstantValue('HEP_INTEGRATION_REQUEST'))){
                             Messaging.Emailfileattachment objEmailAttachmentRequest = new Messaging.Emailfileattachment();
                             objEmailAttachmentRequest.setContentType('application/pdf');
                             objEmailAttachmentRequest.setFileName(objInterfaceTranError.Name + '-Request Playload.PDF');
                             //objEmailAttachmentRequest.setBody(blob.toPDF(objInterfaceTranError.Request_Payload__c));
                             if(EncodingUtil.base64Encode(mapReqResVsDocuments.get(HEP_Utility.getConstantValue('HEP_INTEGRATION_REQUEST'))) != null)
                                objEmailAttachmentRequest.setBody(blob.toPDF(EncodingUtil.base64Decode(EncodingUtil.base64Encode(mapReqResVsDocuments.get(HEP_Utility.getConstantValue('HEP_INTEGRATION_REQUEST')))).toString()));
                             listFileAttachments.add(objEmailAttachmentRequest);
                        }
                        //Mail attachment for Response
                        if(mapReqResVsDocuments.containsKey(HEP_Utility.getConstantValue('HEP_INTEGRATION_RESPONSE'))){
                             Messaging.Emailfileattachment objEmailAttachmentResponse = new Messaging.Emailfileattachment();
                             objEmailAttachmentResponse.setFileName(objInterfaceTranError.Name + '-Response Playload.PDF');
                             //objEmailAttachmentResponse.setBody(blob.toPDF(objInterfaceTranError.Response_Payload__c));
                            if(EncodingUtil.base64Encode(mapReqResVsDocuments.get(HEP_Utility.getConstantValue('HEP_INTEGRATION_RESPONSE'))) != null)
                                objEmailAttachmentResponse.setBody(blob.toPDF(EncodingUtil.base64Decode(EncodingUtil.base64Encode(mapReqResVsDocuments.get(HEP_Utility.getConstantValue('HEP_INTEGRATION_RESPONSE')))).toString()));
                             listFileAttachments.add(objEmailAttachmentResponse);
                        }  
                        //Creating mail structure using Email utility               
                        Messaging.SingleEmailMessage message = HEP_Integration_Util.createSingleEmailMessage(lstToAddresses, TargetObjectId, objInterfaceTranError.id, lstEmailTemplate[0].id, listFileAttachments);
                        lstEmails.add(Message);
                    }
                    //Email Send Util Call for all Errors
                    system.debug('Here in Transaction Error trigger 4 : lstEmails : ' + lstEmails); 
                    if(lstEmails != null && !lstEmails.isEmpty())
                        Messaging.sendEmail(lstEmails);
                }            
            }
        }catch(exception ex){
             //Code to log error..
            HEP_Error_Log.genericException('Error while sending interface transaction error mail','Apex Class',ex,'HEP_InterfaceTxnErrorTriggerHandler','handleAfterInsert',null,'');
        }           
    }
     /**
     * sendiTunesInterfaceErrorEmail -- Create email notification record on iTunes interface transaction Error
     * @param lstInterfaceTransactionError interface transaction error records
     * @return nothing
     */
    public static void sendiTunesInterfaceErrorEmail(List<HEP_Interface_Transaction_Error__c> lstInterfaceTransactionError)
    {       
        List<string> lstToAddresses = new List<string>();
        List<HEP_Interface_Transaction_Error__c> lstITE = new List<HEP_Interface_Transaction_Error__c> ();
        List<HEP_Outbound_Email__c> lstOutboundEmailsToInsert = new List<HEP_Outbound_Email__c>(); 
        Set<id> setIds = new set <id>();
        //Populate the Values in Set with interface transaction Error record id
        setIds = new Map<Id, HEP_Interface_Transaction_Error__c>(lstInterfaceTransactionError).keySet();
        system.debug('---------->' + setIds);
        if(string.isNotEmpty(System.Label.HEP_iTunesInterfaceTransactionError_NotificationEmailList))
            lstToAddresses = System.Label.HEP_iTunesInterfaceTransactionError_NotificationEmailList.split(',');
        system.debug('Email Address of Admins' + lstToAddresses);
        //Query to get all interface transaction error records for the iTunes interface
        for(HEP_Interface_Transaction_Error__c objITE : [SELECT Id,
                                    Name, 
                                    Error_Datetime__c,
                                    Error_Description__c,
                                    HEP_Interface_Transaction__r.Name,
                                    HEP_Interface_Transaction__r.HEP_Interface__r.Name,
                                    HEP_Interface_Transaction__c,
                                    HEP_Interface_Transaction__r.Object_Id__c,
                                    LastModifiedBy.Name, 
                                    Status__c
                        FROM HEP_Interface_Transaction_Error__c
                        WHERE HEP_Interface_Transaction__r.HEP_Interface__r.Name =: 'HEP_iTunes_Price'
                        AND Id IN: setIds
                        ]){
                            lstITE.add(objITE);                        
                        }
        system.debug('Interface Transaction Error Records for Itunes :' + lstITE); 
        String sEmailTemplateName = HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify');
        if(!lstToAddresses.isEmpty()){
            for(HEP_Interface_Transaction_Error__c objITE : lstITE){                
                //Create Outbound Email record before sending the Email.                
                HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c(); //Single Instance of the Object
                objOutboundEmail.Record_Id__c = objITE.Id; // The merge Id that will be accessed by the Template (WhatId)
                objOutboundEmail.Object_API__c = 'HEP_Interface_Transaction_Error__c'; //The Object Name of the above Record
                objOutboundEmail.Email_Template_Name__c = HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify');// The Email Template Name
                String sEmailAddresses = String.join(lstToAddresses , ';');
                objOutboundEmail.To_Email_Address__c = sEmailAddresses;
                lstOutboundEmailsToInsert.add(objOutboundEmail);
            }           
            system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
            if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                insert lstOutboundEmailsToInsert;
            }
        }
    }
}