public with sharing class HEP_BoxAuth{

    public static String financeFolderId { get; set; }

    public static PageReference token() {
        //getToken();
        return null;
    }

    public static string valueToShow {
      get;
      set;
     }
     
     public static string accessToken{get;set;}
     public static string refreshToken;
     public static string folderId{get;set;}
     static string codeFromBox;
     
     static HEP_Services__c   token = HEP_Services__c.getInstance('Box_OAuthToken');
     static string boxClientID = token.Client_Id__c;
     static string boxSecretCode = token.Client_Secret__c;
      static string redirectURI = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_BoxConnect';
     
     
     
     
     public HEP_BoxAuth() {
       valueToShow = 'Start';
       codeFromBox = System.currentPageReference().getParameters().get('code');
       if (codeFromBox != null && codeFromBox != '') {
         String result = getBoxToken(); 
         HEP_FolderWrapper.TokenWrapper token = (HEP_FolderWrapper.TokenWrapper)Json.deserialize(result,HEP_FolderWrapper.TokenWrapper.class);
         accessToken = token.access_token;
         valueToShow = 'Refresh token = '+token.refresh_token;
         HEP_FolderWrapper.RequestFolder promoRoot = new HEP_FolderWrapper.RequestFolder();
         //promoRoot = HEP_BoxServices.parseRequestFolder(Label.HEP_BoxAppName+' Documents','0');
         promoRoot = HEP_BoxServices.parseRequestFolder(HEP_Utility.getConstantValue('HEP_BoxAppName')+' Documents','0');
         System.debug(accessToken+'****');
         String sResult = HEP_BoxServices.createFolder(json.serializePretty(promoRoot), accessToken);
         if(sResult != ''){
            HEP_FolderWrapper.FolderDetails rootFolder = (HEP_FolderWrapper.FolderDetails)JSON.deserialize(sResult, HEP_FolderWrapper.FolderDetails.class);        
            folderId = rootFolder.id;
         }
         //promoRoot = HEP_BoxServices.parseRequestFolder(Label.HEP_BoxAppName+' Finance Documents','0');
         promoRoot = HEP_BoxServices.parseRequestFolder(HEP_Utility.getConstantValue('HEP_BoxAppName')+' Finance Documents','0');
         System.debug(accessToken+'****');
          sResult = HEP_BoxServices.createFolder(json.serializePretty(promoRoot), accessToken);
         if(sResult != ''){
            HEP_FolderWrapper.FolderDetails rootFolder = (HEP_FolderWrapper.FolderDetails)JSON.deserialize(sResult, HEP_FolderWrapper.FolderDetails.class);        
            financeFolderId = rootFolder.id;
         }
       }
     } 
    
    
     public static pageReference boxConnect(){ 
         PageReference pr = new PageReference('https://www.box.com/api/oauth2/authorize?' + 'response_type=code' + '&client_id=' + boxClientID + '&redirect_uri=' + redirectURI);
         return pr;
     } 
     private static String getBoxToken()
     { 
         Http h = new Http(); 
         HttpRequest req = new HttpRequest(); 
         string endPointValue = 'https://www.box.com/api/oauth2/token'; 
         req.setEndpoint(endPointValue);
         req.setBody('Content-Type=' + EncodingUtil.urlEncode('application/x-www-form-urlencoded', 'UTF-8') 
             + '&charset=' + EncodingUtil.urlEncode('UTF-8', 'UTF-8') + 
             '&grant_type=' + EncodingUtil.urlEncode('authorization_code', 'UTF-8') +
             '&code=' + EncodingUtil.urlEncode(codeFromBox, 'UTF-8') +
             '&client_id=' + EncodingUtil.urlEncode(boxClientID, 'UTF-8') + 
             '&client_secret=' + EncodingUtil.urlEncode(boxSecretCode, 'UTF-8') + 
             '&redirect_uri=' + EncodingUtil.urlEncode(redirectURI, 'UTF-8'));
         req.setMethod('POST'); 
         system.debug(codeFromBox+' &&&&&');
         HttpResponse res = h.send(req); 
         SYSTEM.DEBUG(res.getBody());
         return res.getBody();
     }
     
     @TestVisible    
     private static void parseAuthJSON(string JSONValue)
     { 
         JSONParser parser = JSON.createParser(JSONValue);
         accessToken = ''; refreshToken = ''; 
         while (parser.nextToken() != null) 
         {
             if(parser.getCurrentToken() == JSONToken.FIELD_NAME)
             {
                 if(parser.getText() == 'access_token')
                 {
                     parser.nextToken(); 
                     accessToken = parser.getText(); 
                 }
                 if(parser.getText() == 'refresh_token')
                   { 
                       parser.nextToken(); 
                       refreshToken = parser.getText(); 
                   } 
               } 
               if(accessToken != '' && refreshToken != '')
               { 
               break;
               }
           } 
    } 
    /*
    public static String getToken(){
        string s;
        HEP_Services__c servicesData;
        HEP_Services__c token;  
        String ref ;
        system.debug(refreshToken);
   
        HTTPRequest req = new HTTPRequest();
        HTTP http = new HTTP();
        try{
        token = HEP_Services__c.getInstance('BoxToken');
        HEP_Services__c accessToken = HEP_Services__c.getInstance('OAuthToken');
                ref=(refreshToken != null && refreshToken!='')?refreshToken:token.Token__c;
                    system.debug(ref);
        String endpointurl = accessToken.Endpoint_URL__c+accessToken.Service_URL__c;
         String body=   'grant_type='+EncodingUtil.urlEncode('refresh_token','UTF-8')+
                        '&refresh_token='+EncodingUtil.urlEncode( ref,'UTF-8') + 
                                                        '&client_id=' + EncodingUtil.urlEncode(token.Client_Id__c,'UTF-8')+
                                                        '&client_secret=' +EncodingUtil.urlEncode( token.Client_Secret__c,'UTF-8') +
                                                '&charset=' + EncodingUtil.urlEncode('UTF-8', 'UTF-8');
        req.setEndpoint(endpointurl );
        req.setTimeout(50000);
       req.setBody(body);
        req.setMethod('POST');
        
            HTTPResponse res= http.send(req);
            s = res.getBody();
            parseAuthJSON(res.getBody());
            
        }
        catch(Exception ex){
            system.debug(ex.getMessage()+ex.getCause());
        }
        return s;
    }*/
        
    public class  tokenWrapper{
        public String access_token {Get;set;}
        public String refresh_token{get;set;}
        public String expires_in {get;set;}
    }
}