/**
 * HEP_Foxipedia_Catalog -- class will be used to handle the Inbound integration Foxipedia to HEP to create the Catalog records as required.
 * @author    Gaurav Mehrishi
 */
public without sharing class HEP_Foxipedia_Catalog implements HEP_IntegrationInterface {
    public HEP_Foxipedia_Catalog() {

    }

    //Wrappers for Catalog

    //Wrapper for Payload (EDF structure)
    public class PayloadWrapper {
        public ChangeWrapper Change;
        public String MessageID;
        public String TimeStamp;
        public String ChangedEntity;
        public String BaseObject;
        public String Operation;
    }

    //Wrapper for change (EDF structure)

    public class ChangeWrapper {
        public string rowidObject;
        public String ctlgId;
        public String ctlgTypCd;
        public String hubStateInd;
        public String level1;
        public String changedEntity;
        public String chldRowidCtlg;
        public String chldCtlgId;
        public String chldCtlgTypCd;
        public String prntRowidTitleVer;
        public String foxVersionId;
        public String prntRowidTitle;
        public String foxId;
        public String titleSubTypCd;
        public String prntRowidCtlg;
        public String prntCtlgId;
        public String prntCtlgTypCd;
    }

    //Wrapper for Data (EDF structure)
    public class DataWrapper {
        public PayloadWrapper Payload;
    }

    //Wrapper for Root of JSON being received for Catalog
    public class EDFRequestRoot {
        public string EventName;
        public string EventType;
        public string ProducerId;
        public DataWrapper Data;
    }
    // Record Status Active Constraints
    public static String sACTIVE;
    public static String sMasterType;
    public static String sINACTIVE;
    static{
       sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
       sINACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE');
       sMasterType = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER');
    }
    //this method converts set to String with values separated by comma
    public String setToString(Set<String> vals)
    {
        String temp = '';
        vals.remove(null);
        for(String s:vals)
        {
            temp += s + ', ';
        }
        temp = temp.trim().removeEnd(',');
        return temp;
    }
    /**
     * performTransaction -- makes the callout and updates the response object
     * @param HEP_InterfaceTxnResponse object 
     * @return no return value
     */

    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse) {
        String sParameters = '';
        String sAccessToken;
        system.debug('>>>>>>>>>>>>>>>>>>' + objTxnResponse);
        EDFRequestRoot cRoots = (EDFRequestRoot) JSON.deserialize(objTxnResponse.sRequest, EDFRequestRoot.class);
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>' + cRoots);

        //Deserialize the json to check the BaseObject and extract the catalog Id based on the BaseObject.      
        //HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        ChangeWrapper CatRec = cRoots.Data.Payload.Change;
        if (cRoots.Data.Payload.BaseObject.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_BASEOBJECT'))) {
            objTxnResponse.sSourceId = CatRec.ctlgId;
        } else if (cRoots.Data.Payload.BaseObject.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_BASEOBJECT_CHILD'))) {
            objTxnResponse.sSourceId = CatRec.chldCtlgId;
        } else if (cRoots.Data.Payload.BaseObject.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_BASEOBJECT_PARENT'))) {
            objTxnResponse.sSourceId = CatRec.prntCtlgId;
        } else if (cRoots.Data.Payload.BaseObject.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_BASEOBJECT_ALT'))) {
            objTxnResponse.sSourceId = CatRec.ctlgId;
        }
        system.debug('-------' + objTxnResponse);
        HEP_Foxipedia_CatalogWrapper objWrapper = new HEP_Foxipedia_CatalogWrapper();
        HEP_Interface_Transaction__c objInterfaceTxn = new HEP_Interface_Transaction__c();
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH'));
        if (sAccessToken != null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null) {
            String sCatalogId = objTxnResponse.sSourceId;
            HTTPRequest objHttpRequest = new HTTPRequest();

            //Gets the Details from custom setting for Authentication
            HEP_Services__c objServiceDetails = HEP_Services__c.getInstance('Foxipedia_Catalog_Service');
            system.debug('objServiceDetails:' + objServiceDetails);
            //Using the Catalog Id retrieved and make a callout to Foxipedia API to pull the Catalog details
            if (objServiceDetails != null) {
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?productId=' + sCatalogId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization', sAccessToken);
                objHttpRequest.setHeader('system', System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(Integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                //Response from callout
                objTxnResponse.sResponse = objHttpResp.getBody();
                if (objHttpResp.getStatus().equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATUS_OK'))) {
                    //De-serialize the response into a wrapper
                    objWrapper = (HEP_Foxipedia_CatalogWrapper) JSON.deserialize(objHttpResp.getBody(), HEP_Foxipedia_CatalogWrapper.class);
                    system.debug('objWrapper:' + objWrapper);
                    createCatalogs(objWrapper, sCatalogId);
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    system.debug('Status-->' + objTxnResponse.sStatus);
                    objTxnResponse.bRetry = false;
                    if (objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)) {
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        system.debug('Status-->' + objTxnResponse.sStatus);
                        if (objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length() > 254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        System.debug('in success with serialize');
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c != null)             
                            objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c;
                            objTxnResponse.bRetry = false;
                    }
                } else {
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if (objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                }
            }
            //if the token is not valid or if we get the invalid transaction response
        } else {
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }
    }

    /**
     * createCatalogs -- this method will take the objWrapper as input create/update the Catalog records in HEP
     * @param objWrapper deserialize Json and sCatalogId received in Json
     * @return no return value
     */

    public void createCatalogs(HEP_Foxipedia_CatalogWrapper objWrapper, String sCatalogId) {
        string sCNFDL = HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL');
        string sEPSD = HEP_Utility.getConstantValue('HEP_TITLE_CODE_EPSD');
        string sSEASN = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SEASN');
        string sSRIES = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SRIES');
        string sFILM =  HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_FILM');
        string sTV = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_TV');
        List < HEP_Foxipedia_CatalogWrapper.DataWrapper > lstOfData = objWrapper.data;
        if (!lstOfData.isEmpty()) {
            HEP_Foxipedia_CatalogWrapper.Attributes objRec = lstOfData[0].attributes;
            system.debug('---------------' + objRec);
            //list of Global and DHE territory to create global and DHE catalog records
            List < HEP_Territory__c > lstTerritory = [Select id, Name, Auto_Catalog_Creation__c
                                                FROM HEP_Territory__c 
                                                ];
            system.debug('lstTerritory ' + lstTerritory);
            //Id's of Territories with Auto_Catalog_Creation__c as true and Global Territory
            List<HEP_Territory__c> lstAutoCreationCatalogTerritory = new List<HEP_Territory__c>();
            Id GlobalId;
            for (HEP_Territory__c objTerritory : lstTerritory) {
                if (objTerritory.Name.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME')))
                    GlobalId = objTerritory.id;
                else if (objTerritory.Auto_Catalog_Creation__c){
                    lstAutoCreationCatalogTerritory.add(objTerritory);
                }   
                    
            }
            system.debug('lstAutoCreationCatalogTerritory' + lstAutoCreationCatalogTerritory);
            //set of Financial Product Id's we are getting in product title list
            Set < String > setProductTitle = new Set < String > ();
            String sFinancialDivision;
            //List of Release Date we are getting in product title list
            List<String> lstReleaseDate = new List<String>();
            for (HEP_Foxipedia_CatalogWrapper.DataWrapper objData: objWrapper.data) {
                HEP_Foxipedia_CatalogWrapper.Attributes objAttributes = objData.attributes;
                for (HEP_Foxipedia_CatalogWrapper.productTitleWrapper objProductTitle: objAttributes.productTitleList) {
                    setProductTitle.add(objProductTitle.financialProductId);
                    sFinancialDivision = objProductTitle.financialDivisionCode;
                    if(objProductTitle.releaseDate != null){
                        lstReleaseDate.add(objProductTitle.releaseDate);
                    }
                }
            }
            system.debug('lstReleaseDate' + lstReleaseDate);
            system.debug('-----' + setProductTitle);
            //set of Title Id's
            Set < Id > setIds = new Set < Id > ();      
            Map < Id, String > mapCountEpisodes = new Map < Id, String > ();
            EDM_GLOBAL_TITLE__c objGlTitle = new EDM_GLOBAL_TITLE__c();
            if (setProductTitle.size() > 0 && !setProductTitle.isEmpty()) {
                //Kunal - changed = to != in filter for Confidential
                Map < Id, EDM_GLOBAL_TITLE__c > mapTitle = new Map < Id, EDM_GLOBAL_TITLE__c >([Select id,TITLE_SUB_TYPE_CD__c,PROD_TYP_CD__r.PROD_TYP_CD__c 
                                                                                                FROM EDM_GLOBAL_TITLE__c 
                                                                                                WHERE FIN_PROD_ID__c IN: setProductTitle 
                                                                                                and LIFE_CYCL_STAT_GRP_CD__c != :sCNFDL]);
                system.debug('mapTitle' + mapTitle);
                setIds = mapTitle.keySet();
                if (setIds.size() > 0) objGlTitle = mapTitle.get((new list < Id > (setIds))[0]); 
                system.debug('objGlTitle-->' + objGlTitle);             
            }
            //String to store parent tile id
            String sParentKey_Maximumchildren = '';
            //To get Season for which there are max no of Episodes as part of the Coupling
            //Kunal - added check for only Coupling to go into this loop
            if (setIds.size() > 0 && objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_COUPLING'))) {
                List < EDM_GLOBAL_TITLE_HIERARCHY__c > lstTitleHierarchy = [Select ID, CHILD_FOX_ID__c, CHILD_COUNT__c, PARENT_FOX_ID__c, HIERARCHY_CODE__c, PARENT_FOX_ID__r.SEAS_NM__c
                    FROM EDM_GLOBAL_TITLE_HIERARCHY__c
                    WHERE HIERARCHY_CODE__c =: HEP_Utility.getConstantValue('HEP_HIERARCHY_CODE_BCAST')
                    AND
                    CHILD_FOX_ID__c IN: setIds
                ];
                if (lstTitleHierarchy.size() == 0) {
                    lstTitleHierarchy = [Select ID, CHILD_FOX_ID__c, CHILD_COUNT__c, PARENT_FOX_ID__c, HIERARCHY_CODE__c, PARENT_FOX_ID__r.SEAS_NM__c
                        FROM EDM_GLOBAL_TITLE_HIERARCHY__c
                        WHERE HIERARCHY_CODE__c =: HEP_Utility.getConstantValue('HEP_HIERARCHY_CODE_PRODN')
                        AND
                        CHILD_FOX_ID__c IN: setIds
                    ];
                }

                if (lstTitleHierarchy.size() > 0) {
                    Integer iMaximum = 0;
                    Integer iSEASNM = 0;
                    Integer iChildCount = 0;
                    for (EDM_GLOBAL_TITLE_HIERARCHY__c objGlobalTitle: lstTitleHierarchy) {
                        Integer iMaxchildren = iMaximum;
                        If(objGlobalTitle.CHILD_COUNT__c != NULL){
                            iChildCount = Integer.ValueOf(objGlobalTitle.CHILD_COUNT__c);                        
                        }
                        if (iChildCount > iMaxchildren) {
                            iMaximum = iChildCount;
                            sParentKey_Maximumchildren = objGlobalTitle.PARENT_FOX_ID__c;
                            if(objGlobalTitle.PARENT_FOX_ID__r.SEAS_NM__c != NULL){
                                iSEASNM = Integer.ValueOf(objGlobalTitle.PARENT_FOX_ID__r.SEAS_NM__c);
                            }
                        } else if (iChildCount == iMaxchildren) {
                            if(objGlobalTitle.PARENT_FOX_ID__r.SEAS_NM__c != NULL){
                                if (Integer.ValueOf(objGlobalTitle.PARENT_FOX_ID__r.SEAS_NM__c) > iSEASNM) {
                                    sParentKey_Maximumchildren = objGlobalTitle.PARENT_FOX_ID__c;
                                }
                            }
                        }
                    }
                }

                system.debug('lstTitleHierarchy' + lstTitleHierarchy);
                system.debug('sParentKey_Maximumchildren' + sParentKey_Maximumchildren);
            }
            //To get latest Release Date for the titles related to Catalogs
            List<String> lstLatestReleaseDate = new List<String>();
            Datetime dateLatestReleaseDate;           
            if (lstReleaseDate.size() > 0 && !lstReleaseDate.isEmpty()) {
                lstReleaseDate.sort();
                for(Integer i = lstReleaseDate.size()-1; i>=0;i--){
                    lstLatestReleaseDate.add(lstReleaseDate.get(i));
                }
                if (lstReleaseDate.size() > 0 && !lstReleaseDate.isEmpty()) {
                    dateLatestReleaseDate = Datetime.valueOf(lstLatestReleaseDate[0]);
                    System.debug('Check the Order -->'+lstLatestReleaseDate);
                }    
            }            
            //List to store Global catalogs for upsert
            List < HEP_Catalog__c > lstCatalogsToUpsert = new List < HEP_Catalog__c > ();
            //List to store DHE catalogs for auto-creation
            List < HEP_Catalog__c > lstDHECatalogsToUpsert = new List < HEP_Catalog__c > ();
            //Iterate over Catalogs received and upsert records in HEP
            if (String.isNotBlank(sCatalogId)) {
                system.debug('Entered IF');
                HEP_Catalog__c objCatRec = new HEP_Catalog__c();
                objCatRec.Territory__c = GlobalId;
                system.debug('Value' + objGlTitle.TITLE_SUB_TYPE_CD__c);
                system.debug('Value' + objGlTitle.PROD_TYP_CD__r.PROD_TYP_CD__c);
                // To Populate TV/FILM in product type field in Catalog
                if(objGlTitle.TITLE_SUB_TYPE_CD__c != null && (objGlTitle.TITLE_SUB_TYPE_CD__c.equalsIgnoreCase(sEPSD) || objGlTitle.TITLE_SUB_TYPE_CD__c.equalsIgnoreCase(sSEASN) || objGlTitle.TITLE_SUB_TYPE_CD__c.equalsIgnoreCase(sSRIES))) {                  
                    objCatRec.Product_Type__c = sTV;
                } else if (objGlTitle.TITLE_SUB_TYPE_CD__c != null && objGlTitle.TITLE_SUB_TYPE_CD__c.equalsIgnoreCase(sFILM)){                 
                    objCatRec.Product_Type__c = sFILM;
                }
                /* To Populate Product SubType in Catalog
                if(objGlTitle.PROD_TYP_CD__r.PROD_TYP_CD__c != null) {                  
                    objCatRec.Product_Sub_Type__c = objGlTitle.PROD_TYP_CD__r.PROD_TYP_CD__c;
                }*/                  
                // Stamping Title on Catalog records                
                if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_SINGLE'))) {
                    if (setIds.size() > 0) {
                        objCatRec.Title_EDM__c = (new list < Id > (setIds))[0];                                            
                    }
                } else if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BXSET'))) {
                        objCatRec.Title_EDM__c = NULL;

                } else if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_COUPLING'))) {
                    if (String.isNotBlank(sParentKey_Maximumchildren)) {
                        objCatRec.Title_EDM__c = sParentKey_Maximumchildren;                        
                    } else if (objGlTitle.TITLE_SUB_TYPE_CD__c != null && objGlTitle.TITLE_SUB_TYPE_CD__c != sEPSD && setIds.size() == 1) {
                        objCatRec.Title_EDM__c = objGlTitle.id; 
                    }
                }
                if (String.isNotBlank(objRec.productId)) {
                    objCatRec.CatalogId__c = objRec.productId;
                    objCatRec.Unique_Id__c = objCatRec.CatalogId__c + ' / ' + HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME');
                }
                objCatRec.Catalog_Name__c = objRec.productName;
                objCatRec.Local_Catalog_Name__c = objCatRec.Catalog_Name__c;
                
                objCatRec.Latest_Release_Date__c = dateLatestReleaseDate;
                
                system.debug('>>>>>>>>>>>>' + objCatRec.Catalog_Name__c);
                if (String.isNotBlank(objRec.productStatusDescription)){
                    objCatRec.Foxipedia_Status__c = objRec.productStatusDescription;
                    system.debug('>>>>' + objCatRec.Foxipedia_Status__c);
                }               
                objCatRec.LegalTitleNumber__c = '';
                objCatRec.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                //Get Catalog Type based on Product type code
                if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_SINGLE'))) {
                    objCatRec.Catalog_Type__c = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_SINGLE');
                } else if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_COUPLING'))) {
                    objCatRec.Catalog_Type__c = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_COUPLING');
                } else if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BXSET'))) {
                    objCatRec.Catalog_Type__c = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BXSET');
                } else if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BNDL'))) {
                    objCatRec.Catalog_Type__c = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BNDL');
                }
                //Populate Primary Catalog Id and Financial Division for Bundle Catalogs
                if (objRec.productTypeCode.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BNDL'))) {
                    objCatRec.Primary_Catalog_Id__c = objRec.primaryCatalogId;
                    objCatRec.Financial_Division_Code__c = sFinancialDivision;
                    objCatRec.Bundle_Title_WPRs__c = setToString(setProductTitle);
                    objCatRec.Title_EDM__c = NULL;
                }

                //Kunal - update Record Status to Inactive if Product Status Code is DNUSE
                if(String.isNotBlank(objRec.productStatusCode) && objRec.productStatusCode.equalsIgnoreCase('DNUSE'))
                    objCatRec.Record_Status__c = sINACTIVE;
                else
                    objCatRec.Record_Status__c = sACTIVE;

                objCatRec.Type__c = sMasterType;
                if(objCatRec.Catalog_Type__c != null && (objCatRec.Catalog_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_SINGLE')) || objCatRec.Catalog_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_COUPLING')) || objCatRec.Catalog_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BNDL')) || objCatRec.Catalog_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BXSET')))){
                    lstCatalogsToUpsert.add(objCatRec);
                    system.debug('><><><>' + lstCatalogsToUpsert);                            
                Schema.SObjectField UniqueId = HEP_Catalog__c.Fields.Unique_Id__c;
                system.debug('UniqueId' + UniqueId);
                if (lstCatalogsToUpsert != NULL && lstCatalogsToUpsert.size() > 0) {
                    Database.UpsertResult[] lstUpsertResult = Database.upsert(lstCatalogsToUpsert, UniqueId, false);
                    system.debug('lstCatalogsToUpsert ' + lstCatalogsToUpsert);
                    for (Database.UpsertResult objUpsertResult : lstUpsertResult) {
                        system.debug('objUpsertResult.isSuccess()' + objUpsertResult.isSuccess());
                        system.debug('objUpsertResult.getErrors()' + objUpsertResult.getErrors());
                        system.debug('objUpsertResult.isCreated()' + objUpsertResult.isCreated());
                        for (HEP_Territory__c objTerritory : lstAutoCreationCatalogTerritory) {
                            if (objUpsertResult.isSuccess()) {                          
                               HEP_Catalog__c objDHECat = objCatRec.clone(false, false, false, false);
                               objDHECat.Global_Catalog__c = objUpsertResult.getId();
                               objDHECat.Territory__c = objTerritory.Id;
                               objDHECat.Unique_Id__c = objDHECat.CatalogId__c + ' / ' + objTerritory.Name;
                               //objDHECat.Local_Catalog_Name__c = objCatRec.Catalog_Name__c;
                               lstDHECatalogsToUpsert.add(objDHECat);
                            }
                        }
                    }
                    if (lstDHECatalogsToUpsert != NULL && lstDHECatalogsToUpsert.size() > 0) {
                        Schema.SObjectField UniqueDHEId = HEP_Catalog__c.Fields.Unique_Id__c;
                        Database.upsert(lstDHECatalogsToUpsert, UniqueDHEId, false);
                        system.debug('lstDHECatalogsToUpsert ' + lstDHECatalogsToUpsert);
                    }
                }
                }
                else {
                    system.debug('We DON’T need to save any Catalogs of any type other than Single, Coupling, Boxset and Bundle.');
                }
            }
        }
    }
}