/**
* HEP_INT_NTSPurchaseOrder_Details -- purchase Order details will be stored from NTS And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_NTSPurchaseOrder_Details implements HEP_IntegrationInterface{ 
    /**
    * HEP_INT_NTSPurchaseOrder_Details -- Rating details to store Purchase details
    * @return  :  no return value
    * @author  :  Ram Bairwa
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        String sHEP_CURRENCY = HEP_Utility.getConstantValue('HEP_CURRENCY');
        String sHEP_REPLACE_CURRENCY = HEP_Utility.getConstantValue('HEP_REPLACE_CURRENCY');
        String sHEP_OBJECT = HEP_Utility.getConstantValue('HEP_OBJECT');
        String sHEP_REPLACE_OBJECT_NO = HEP_Utility.getConstantValue('HEP_REPLACE_OBJECT_NO');
        String sHEP_PO = HEP_Utility.getConstantValue('HEP_PO');
        String sHEP_REPLACE_PO_NO = HEP_Utility.getConstantValue('HEP_REPLACE_PO_NO');
        String sHEP_POLine = HEP_Utility.getConstantValue('HEP_POLineNo');
        String sHEP_REPLACE_POLine_NO = HEP_Utility.getConstantValue('HEP_REPLACE_POLine_NO');
        
        HEP_NTSPurchaseOrderWrapper objWrapper = new HEP_NTSPurchaseOrderWrapper();         
        if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sCatalog;
            String sTerritory;
            String sFrom_Date;
            String sTo_Date; 
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Territory and PromoId details to get the purchase order details through integration 
            HEP_NTSInvoiceWrapper objNTSInvoiceWrapper = (HEP_NTSInvoiceWrapper)JSON.deserialize(sDetails,HEP_NTSInvoiceWrapper.class);
            if(objNTSInvoiceWrapper != null){
            sCatalog = objNTSInvoiceWrapper.sCatalog;
            sTerritory = objNTSInvoiceWrapper.sTerritory;
            sFrom_Date = objNTSInvoiceWrapper.sFrom_Date;
            sTo_Date = objNTSInvoiceWrapper.sTo_Date;
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_E1_PODETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_NTS_E1_PODETAILS'));   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?TERRITORY=' + sTerritory + '&CATALOG=' + sCatalog+ '&FROM_DATE=' + sFrom_Date+ '&TO_DATE=' + sTo_Date);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(HEP_Constants__c.getValues('HEP_STATUS_OK') != null  && HEP_Utility.getConstantValue('HEP_STATUS_OK') != null
                    && objHttpResp.getStatus().equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    String sResponse = objHttpResp.getBody();
                    sResponse = sResponse.replace(sHEP_CURRENCY , sHEP_REPLACE_CURRENCY);
                    sResponse = sResponse.replace(sHEP_POLine , sHEP_REPLACE_POLine_NO);
                    sResponse = sResponse.replace(sHEP_OBJECT , sHEP_REPLACE_OBJECT_NO);
                    sResponse = sResponse.replace(sHEP_PO , sHEP_REPLACE_PO_NO);
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    objWrapper = (HEP_NTSPurchaseOrderWrapper)JSON.deserialize(sResponse, HEP_NTSPurchaseOrderWrapper.class);	
                    //objWrapper = (HEP_NTSPurchaseOrderWrapper)JSON.deserialize(objHttpResp.getBody(), HEP_NTSPurchaseOrderWrapper.class);
                     System.debug('objWrapper :' + objWrapper );
                     if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        objTxnResponse.bRetry = false;
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }        
    }    
    
     /**
    * HEP_INT_NTSInvoiceWrapper  -- Class to hold Input parameter details to be sent to the NTS and E1
    * @author    Ram Bairwa
    */ 
public class HEP_NTSInvoiceWrapper {
    public String sCatalog;
    public String sTerritory;
    public String sFrom_Date;
    public String sTo_Date;
}
}