/**
* HEP_ScheduledBatchableAutoLock -- class for Scheduling the auto lock module
* @author    Lakshman Jinnuri
*/
global class HEP_ScheduledBatchableAutoLock implements Schedulable {
   global void execute(SchedulableContext sc) {
      HEP_Auto_Lock_Promotion_Dating_Batch autoLock = new HEP_Auto_Lock_Promotion_Dating_Batch(); 
      // changing the job to run for each record as it iterates over the promotion Id to handle the JDE outbound for updates
      database.executebatch(autoLock,1);
   }
}