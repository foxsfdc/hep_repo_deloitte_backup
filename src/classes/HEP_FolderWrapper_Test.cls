@isTest
private class HEP_FolderWrapper_Test {
	
	@isTest static void test_method_one() {
		HEP_FolderWrapper objFolderWrapper = new HEP_FolderWrapper();
		HEP_FolderWrapper.RequestFolder objRequestFolder = new HEP_FolderWrapper.RequestFolder();
		objRequestFolder.name = 'test';
		objRequestFolder.parent = new HEP_FolderWrapper.ParentDetails();
		objRequestFolder.parent.Id = 'test';

		HEP_FolderWrapper.Context objContext = new HEP_FolderWrapper.Context();
		objContext.conflicts = new List<HEP_FolderWrapper.FolderDetails>{new HEP_FolderWrapper.FolderDetails()};

		HEP_FolderWrapper.Shared_Link objSharedLink = new HEP_FolderWrapper.Shared_Link();
		objSharedLink.url = 'asdf';
		objSharedLink.download_url = 'asdf';
		objSharedLink.access = 'asdf';
		objSharedLink.is_password_enabled = 'asdf';

		HEP_FolderWrapper.FolderDetails objFolderDetails = new HEP_FolderWrapper.FolderDetails();
		objFolderDetails.type = 'test';
		objFolderDetails.Id = 'test';
		objFolderDetails.name = 'test';
		objFolderDetails.context_info = objContext;
		objFolderDetails.Shared_Link = objSharedLink;

		HEP_FolderWrapper.TokenWrapper objTokenWrapper = new HEP_FolderWrapper.TokenWrapper();
		objTokenWrapper.access_token = 'asdf';
		objTokenWrapper.refresh_token = 'asdf';
		objTokenWrapper.expires_in='asdf';

		HEP_FolderWrapper.Entry objEntry = new HEP_FolderWrapper.Entry();
		objEntry.type = 'asdf';
		objEntry.id = 'asdf';
		objEntry.sequence_id = 'asdf';
		objEntry.etag = 'asdf';
		objEntry.name = 'asdf';

		HEP_FolderWrapper.FolderItems objFolderItems = new HEP_FolderWrapper.FolderItems();
		objFolderItems.total_count = 'asdf';
		objFolderItems.entries = new List<HEP_FolderWrapper.Entry>{objEntry};
	}
	
	
}