/**
* HEP_Siebel_CatalogGenreDetails_Test -- Test class for the HEP_Siebel_CatalogGenreDetails for Siebel Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Siebel_CatalogGenreDetails_Test {
    /**
    * HEP_CatalogGenreDetails_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogGenreDetails_SuccessTest() {
        HEP_Territory__c territory = HEP_Test_Data_Setup_Utility.createHEPTerritory('France', 'EMEA', 'Subsidiary', null, null, 'EUR', 'FRA', '170',null, true);
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<ID> lstCatalogGenreIds = new list<ID>();
        Test.startTest();
        HEP_Catalog__c catalog = HEP_Test_Data_Setup_Utility.createCatalog('34939493', 'TitleCatalog', 'Single', null, territory.Id, 'Pending', 'Request', true);
        HEP_Catalog_Genre__c catalogGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(catalog.Id, true);
        lstCatalogGenreIds.add(catalogGenre.Id);
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '["'+ catalogGenre.Id +'"]'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogGenreDetails demo = new HEP_Siebel_CatalogGenreDetails();
        demo.performTransaction(objTxnResponse);
        demo.getRequestBody(lstCatalogGenreIds);
        Test.stoptest();
    }
    /**
    * HEP_CatalogGenreDetails_FailureTest --  Test method for Failure response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
     @isTest static void HEP_CatalogGenreDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '[]'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogGenreDetails demo = new HEP_Siebel_CatalogGenreDetails();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    /**
    * HEP_CatalogGenreDetails_InvalidAcessTokenTest -- Test method for Invalid Access Token
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogGenreDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '["0013D00000QBoZL", "0013D00000QBoZB"]'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogGenreDetails demo = new HEP_Siebel_CatalogGenreDetails();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

}