/**
 * HEP_Customer_Promotion_SUBMITTED --- Dynamically fetch records in Email Template
 * @author    Ayan Sarkar
 */
global class HEP_Customer_Promotion_SUBMITTED {
    //Current Customer Promotion Id.
    global String customerPromotionId {get;set;}
    global String sRecordApproverId {get;set;}
    global Date dtDate{get;set;}
    global String templateName{get;set;}

    //Static Block For Active Status
    global static String sACTIVE;
    global static String sREJECTED;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sREJECTED = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
    }

    //This variable is used to render table on Email template
    global CustomerPromotionWrapper objCustomerPromotionWrapper{ 
        get{
            if(objCustomerPromotionWrapper == null){
                fetchCustomerPromotionDetials();
            }
            return objCustomerPromotionWrapper;
        }
        set;
    }

    /**
    * fetchCustomerPromotionDetials --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author    Ayan Sarkar
    */ 
    global void fetchCustomerPromotionDetials(){
        List<HEP_Outbound_Email__c> lstOutBoundEmail = new List<HEP_Outbound_Email__c>([Select HEP_Promotion__c,
                                                        Record_Id__c,
                                                        Email_Sent__c,CreatedDate 
                                                    from HEP_Outbound_Email__c
                                                    Where Email_Sent__c = False
                                                        AND Email_Template_Name__c = :templateName
                                                        
                                                        AND HEP_Promotion__c =:customerPromotionId LIMIT 1]);
            if(!lstOutBoundEmail.isEmpty())
                sRecordApproverId = lstOutBoundEmail.get(0).Record_Id__c;
         //Instantiate the Wrapper
            objCustomerPromotionWrapper = new CustomerPromotionWrapper(); 
            try{
                if(customerPromotionId != NULL){

                    //Code to get the Promotion Date.
                    System.debug('customerPromotionId****'+customerPromotionId);
                      HEP_Record_Approver__c objRecordApprover = [SELECT Id, 
                                                                         Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__c,
                                                                         Approver__c,
                                                                         Approver__r.Name,
                                                                         Approver_Role__r.Name,
                                                                         Status__c,
                                                                         Comments__c,
                                                                         IsActive__c,
                                                                         LastModifiedDate
                                                                         FROM HEP_Record_Approver__c
                                                                         WHERE Id =: sRecordApproverId
                                                                         LIMIT 1];

                    //Query the Record based on Record Id
                    HEP_Promotion__c objCurrentCustomerPromotion = [SELECT ID, Name , PromotionName__c,
                                                                    Customer__c, Customer__r.CustomerName__c ,
                                                                    Status__c, StartDate__c , EndDate__c, Requestor__c , Requestor__r.Name,
                                                                    Domestic_Marketing_Manager__c,Domestic_Marketing_Manager__r.Name,
                                                                    Promotion_Type__c,
                                                                    Submitted_Date__c , Priority__c, CustomerNotes__c,
                                                                    Territory__c , Territory__r.Name
                                                                    FROM HEP_Promotion__c
                                                                    WHERE Id =: customerPromotionId
                                                                    //WHERE Id =: objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__c
                                                                    AND Record_Status__c =:sACTIVE
                                                                    LIMIT 1];


                       System.debug('objCurrentCustomerPromotion*****'+objCurrentCustomerPromotion);
                                                                    

                    

                    //Code to fetcht the Brand Manager...
                    /*HEP_User_Role__c objUserRole = [SELECT ID, Name , User__c , User__r.Name
                                                    FROM HEP_User_Role__c
                                                    WHERE Record_Status__c =: sACTIVE
                                                    AND Role__r.Name =: HEP_Utility.getConstantValue('HEP_MARKETING_MANAGER')
                                                    AND Territory__c =: objCurrentCustomerPromotion.Territory__c
                                                    LIMIT 1];*/

                    objCustomerPromotionWrapper.sPromotionName  = objCurrentCustomerPromotion.PromotionName__c;
                    objCustomerPromotionWrapper.sPromotionId  = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_Promotion_Details?promoId='+objCurrentCustomerPromotion.Id;
                    objCustomerPromotionWrapper.sStatus         = objCurrentCustomerPromotion.Status__c;
                    objCustomerPromotionWrapper.sVendor         = objCurrentCustomerPromotion.Customer__r.CustomerName__c;
                    objCustomerPromotionWrapper.sRequestor      = objCurrentCustomerPromotion.Requestor__r.Name;

                    //Get the dates and populate:
                    //1. Start Date
                    DateTime dttStartDate;
                    Date dtStartDate            = objCurrentCustomerPromotion.StartDate__c;
                    //System.assert(1==2, customerPromotionId    +'Return Result ----->' + dtStartDate);
                    if(dtStartDate != NULL)
                        dttStartDate       = DateTime.newInstance(dtStartDate.year(), dtStartDate.month(), dtStartDate.day());
                    if(dttStartDate != NULL)
                        objCustomerPromotionWrapper.sStartDate      = dttStartDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    
                    //2. End Date
                    DateTime dttEndDate;
                    Date dtEndDate              = objCurrentCustomerPromotion.EndDate__c;
                    if(dtEndDate != NULL)
                        dttEndDate         = DateTime.newInstance(dtEndDate.year(), dtEndDate.month(), dtEndDate.day());
                    if(dttEndDate != NULL)
                        objCustomerPromotionWrapper.sEndDate        = dttEndDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));

                    //3. Submitted Date
                    DateTime dttSubmittedDate;
                    
                    if(!lstOutBoundEmail.isEmpty())
                        dttSubmittedDate = lstOutBoundEmail.get(0).createdDate;
                    
                   /* Date dtSubmittedDate        = dtDate 
                    if(dtSubmittedDate != NULL)
                        dttSubmittedDate   = DateTime.newInstance(dtSubmittedDate.year(), dtSubmittedDate.month(), dtSubmittedDate.day()); */
                    if(dttSubmittedDate != NULL)    
                        objCustomerPromotionWrapper.sSubmittedDate  = dttSubmittedDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));

                    objCustomerPromotionWrapper.sPriority       = (objCurrentCustomerPromotion.Priority__c ? 'Expedited' : 'Normal');
                    
                    if(objRecordApprover.Status__c.equalsIgnoreCase(sREJECTED)){
                        objCustomerPromotionWrapper.sNotes = objRecordApprover.Comments__c;
                        objCustomerPromotionWrapper.sRejector = objRecordApprover.Approver__r.Name;
                        DateTime dttRejectedDate;
                        dttRejectedDate = objRecordApprover.LastModifiedDate;                   
                        objCustomerPromotionWrapper.sRejectedDate = dttRejectedDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    } else{
                        objCustomerPromotionWrapper.sNotes = objCurrentCustomerPromotion.CustomerNotes__c;
                    }
                    
                    objCustomerPromotionWrapper.sBrandManager   = objCurrentCustomerPromotion.Domestic_Marketing_Manager__r != null ?objCurrentCustomerPromotion.Domestic_Marketing_Manager__r.Name:'' ;
                } 
                //System.assert(1==2, 'customerPromotionId    Return Result ----->' + JSON.serializePretty(objCustomerPromotionWrapper));
                System.debug('Return Result ----->' + JSON.serializePretty(objCustomerPromotionWrapper));
            }catch(Exception objException){
                HEP_Error_Log.genericException('Exception occured when Sending MDP emails','DML Errors',objException,'HEP_Customer_Promotion_SUBMITTED','fetchCustomerPromotionDetials',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
                //System.assert(1==2, 'Return Result ----->' + ex + '--' + objException.getLineNumber());
            }
            //return objCustomerPromotionWrapper;
    }

    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author    Nishit Kedia
    */
    global class CustomerPromotionWrapper{
        
        public String   sPromotionName  {get; set;}
        public String   sPromotionId    {get; set;}
        public String   sStatus         {get; set;}
        public String   sVendor         {get; set;}
        public String   sRequestor      {get; set;}
        public String   sStartDate      {get; set;}
        public String   sEndDate        {get; set;}
        public String   sSubmittedDate  {get; set;}
        public String   sPriority       {get; set;}
        public String   sNotes          {get; set;}
        public String   sBrandManager   {get; set;}
        public String   sRejector       {get; set;}
        public String sRejectedDate {get; set;}
    }
}