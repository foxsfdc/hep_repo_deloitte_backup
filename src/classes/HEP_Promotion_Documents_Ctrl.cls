/**
* HEP_Promotion_Documents_Ctrl --- Controller class for HEP Documents tab to fetch/create box folders
* @author    Tejaswini Veliventi
*/
public with sharing class HEP_Promotion_Documents_Ctrl {
    
    public Map<String , String> mapPermissions{get;set;}
    public Boolean bEditPermission{get;set;}

    /**
    * Class constructor to initialize the class variables
    * @return nothing
    */
    public HEP_Promotion_Documents_Ctrl(){
    }

    public void fetchPermissions(){

        HEP_Promotion__c objPromotion = [SELECT Id, Territory__c FROM HEP_Promotion__c WHERE Id =: ApexPages.currentPage().getParameters().get('promoId')];
        mapPermissions = HEP_Utility.fetchCustomPermissions(objPromotion.Territory__c , 'HEP_Promotion_Documents');
        if(mapPermissions !=null){
            if(mapPermissions .get('WRITE').equalsIgnoreCase('FALSE')){
                bEditPermission  = false;                
            }else{
                bEditPermission  = true;
            }
        }
        //bEditPermission = Boolean.valueOf(mapPermissions.get('WRITE'));
    }

    /**
    * takes promotion id as Input and creates the root folders and enqueues the creation of sub-folders
    * @return callout response
    */
    @remoteAction
    public static String createPromoFolders(String sPromoId){
        return HEP_BoxServices.createPromoFolders(sPromoId);
    }
    
    /**
    * takes promotion id as Input and creates the root folders and enqueues the creation of sub-folders
    * @return callout response
    */
    @remoteAction
    public static Map<String,String> getFolderDetails(String sPromotionId){
        return HEP_BoxServices.getFolderDetails(sPromotionId);
    }

}