/**
* HEP_INT_JDE_Integration_Test -- Test class for the HEP_INT_JDE_Integration for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_INT_JDE_Integration_Test{
    @testsetup 
    static void CreateData(){         
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US/CAN','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        objTerritory.SKU_Interface__c = 'JDE';
        insert objTerritory;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_INT_JDE_PromoType__c> objIntJdePromoType = HEP_Test_Data_Setup_Utility.createHepJDEPromoTypes();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_JDE_Promotion_Dating_Date_Type__c> objDateType = HEP_Test_Data_Setup_Utility.createHEPJDEPromotionDatingDateType();
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - Catalog','New Release',false);
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Box Set',null,objTerritory.Id, 'Draft', 'Request', true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '456', 'New SKU', 'Request', 'Physical', 'DD', 'DVD', true);
        objSKU.Current_Release_Date__c = date.today();
        objSKU.Territory__c = objTerritory.id;
        objSKU.SKU_Configuration__c = 'MA';
        objSKU.Same_Base_UPC__c = true;
        objSKU.Rental_Ready__c = '12';
        update objSKU;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id, '123', 'SKU Customers', '',true);
        HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id, objSKU.Id, true);
        HEP_Boxset_Catalog_SKU__c objBoxetCtalogSku = HEP_Test_Data_Setup_Utility.createBoxetCatalogSku(objCatalog.Id,objSKU.Id,objSKU.Id,'Active',true);
        HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objGlobalPromotion.Id, null, true);
        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,  'Approved', 'Unlocked', true);
        objPromotionSKU.Record_Status__c = 'Active';
        objPromotionSKU.SKU_Master__c = objSKU.Id; 
        update objPromotionSKU;
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id, '$44.95 - $31.50', 44.95, false);
        insert objPriceGrade;
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKU.Id, objTerritory.Id, null, null, 'Active', false);
        objSKUPrice.Changed_Flag__c = true;
        objSKUPrice.Promotion_SKU__c = objPromotionSKU.Id;
        insert objSKUPrice;
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/edf/events1';
        lstServices.add(objService);
        insert lstServices;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Pull',true,true,true);
    }
    /**
    * HEP_INT_JDE_Integration_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Integration_SuccessTest() {
        HEP_Promotion__c objGlobalPromotion = [Select Id from HEP_Promotion__c where PromotionName__c =: 'Global Promotion'];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
	    HEP_INT_JDE_Integration objInt = new HEP_INT_JDE_Integration();
	    HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
	    objInt.performTransaction(objTxnResponse);
        objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objGlobalPromotion.id,'','HEP_JDE_PromoSKU');
        System.assertEquals('HEP_JDE_PromoSKU',objTxnResponse.sInterfaceName);
	    HEP_INT_JDE_Integration.ErrorResponseWrapper objError = new HEP_INT_JDE_Integration.ErrorResponseWrapper('','');
        Test.stoptest();
    }
    /**
    * HEP_INT_JDE_Integration_SuccessTest2 --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Integration_SuccessTest2() {
        HEP_Territory__c objTerritory = [SELECT Id from HEP_Territory__c WHERE Name =: 'US/CAN'];
        HEP_Promotion__c objGlobalPromotion = [Select Id from HEP_Promotion__c where PromotionName__c =: 'Global Promotion'];
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        objDatingRecord.Date_Type__c = 'EST Suppression Lift Date';
        insert objDatingRecord;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objGlobalPromotion.id,'','HEP_JDE_PromoSKU');
        System.assertEquals('HEP_JDE_PromoSKU',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_INT_JDE_Integration_SuccessTest3 --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Integration_SuccessTest3() {
        HEP_Territory__c objTerritory = [SELECT Id from HEP_Territory__c WHERE Name =: 'US/CAN'];
        HEP_Promotion__c objGlobalPromotion = [Select Id from HEP_Promotion__c where PromotionName__c =: 'Global Promotion'];
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        objDatingRecord.Date_Type__c = 'EST Avail Date';
        insert objDatingRecord;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objGlobalPromotion.id,'','HEP_JDE_PromoSKU');
        System.assertEquals('HEP_JDE_PromoSKU',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_INT_JDE_Integration_SuccessTest4 --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Integration_SuccessTest4() {
        HEP_Territory__c objTerritory = [SELECT Id from HEP_Territory__c WHERE Name =: 'US/CAN'];
        HEP_Promotion__c objGlobalPromotion = [Select Id from HEP_Promotion__c where PromotionName__c =: 'Global Promotion'];
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        objDatingRecord.Date_Type__c = 'Physical Order Date';
        insert objDatingRecord;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objGlobalPromotion.id,'','HEP_JDE_PromoSKU');
        System.assertEquals('HEP_JDE_PromoSKU',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
     /**
    * HEP_INT_JDE_Integration_SuccessTest5 --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Integration_SuccessTest5() {
        HEP_Territory__c objTerritory = [SELECT Id from HEP_Territory__c WHERE Name =: 'US/CAN'];
        HEP_Promotion__c objGlobalPromotion = [Select Id from HEP_Promotion__c where PromotionName__c =: 'Global Promotion'];
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;

        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        objDatingRecord.Date_Type__c = 'VOD Avail Date';
        insert objDatingRecord;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objGlobalPromotion.id,'','HEP_JDE_PromoSKU');
        System.assertEquals('HEP_JDE_PromoSKU',objTxnResponse.sInterfaceName);
        Test.stoptest();
    } 
}