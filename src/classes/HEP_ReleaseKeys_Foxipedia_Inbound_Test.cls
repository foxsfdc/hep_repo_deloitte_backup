@isTest (seeAllData = false)
public class HEP_ReleaseKeys_Foxipedia_Inbound_Test {
    @isTest
    static void testPerformTransaction (){
        HEP_List_Of_Values__c objlistValueHD = new HEP_List_Of_Values__c(Values__c = 'EST',Order__c = '25',Type__c = 'COPPER_CHANNEL',Parent_Value__c='SD');
        insert objlistValueHD;
        

        // HEP_List_Of_Values__c objlistValueSD = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='SD');
        // insert objlistValueSD; 
         EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;
        HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia1', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS1', 'AUS1', '', false);
            objTerritory1.Flag_Country_Code__c = 'AU';
            insert objTerritory1;
             HEP_Promotion__c objPromotionNational = HEP_Test_Data_Setup_Utility.createPromotion('Global National 1', 'National', null,objTerritory1.Id, null, null,null,false);
        objPromotionNational.Record_Status__c= 'Active'; 
        objPromotionNational.StartDate__c = date.today();
        objPromotionNational.EndDate__c = date.today()+20;
        // objPromotion.Customer__c = objCustomer.id;
        insert objPromotionNational;
             HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory1.Id, 'Approved', 'Request', null);
            objCatalog.Title_EDM__c = objTitle.id;
            objCatalog.Record_Status__c = 'Active';
            objCatalog.Catalog_Type__c = 'Bundle'; 
            insert objCatalog; 
            List<String> terrList = new List<String>();
            terrList.add(objTerritory1.Id);
            HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory1.Id, objTerritory1.Id, false);
        objDatingMatrix.Dating_Flag__c=Boolean.valueOf('true');
        // objDatingMatrix.channel__c ='EST';
        insert objDatingMatrix;
        HEP_Promotions_DatingMatrix__c testdata = [SELECT channel__c
from HEP_Promotions_DatingMatrix__c where id=:objDatingMatrix.Id];
        system.debug('testdata'+testdata);
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory1.id,objPromotionNational.Id,objDatingMatrix.id,False);
        objDatingRecord.Record_Status__c = 'Active';
        objDatingRecord.Status__c =  'Confirmed';
        objDatingRecord.Date_Type__c =  'EST Release Date';
        
        objDatingRecord.HEP_Catalog__c = objCatalog.id;
        // objDatingRecord.Customer__c = objCustomer.id;
        insert objDatingRecord;
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_InterfaceTxnResponse objWrap = new HEP_InterfaceTxnResponse();
        objWrap.sRequest = '2018-07-20T00:00:00Z';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        HEP_ReleaseKeys_Foxipedia_Inbound copp = new HEP_ReleaseKeys_Foxipedia_Inbound();
        copp.performTransaction(objWrap);
        
        objWrap.sRequest = '2018-06-01T00:00:00Z';
        HEP_ReleaseKeys_Foxipedia_Inbound copp1 = new HEP_ReleaseKeys_Foxipedia_Inbound();
        copp1.performTransaction(objWrap);
        HEP_ReleaseKeys_Foxipedia_Inbound.ReleaseKeys rk = new HEP_ReleaseKeys_Foxipedia_Inbound.ReleaseKeys('345',terrList);
        Test.stopTest();
        
    }
}