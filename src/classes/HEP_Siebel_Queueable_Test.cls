/**
* HEP_Siebel_Queueable_Test-- Test class for the HEP_Siebel_Queueable class for all Interfaces 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Siebel_Queueable_Test{
    /**
    * HEP_Siebel_Queueable_Method --  Test method for the HEP_Siebel_Queueable
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_Siebel_Queueable_Method(){
        HEP_Siebel_Queueable objQueueable = new HEP_Siebel_Queueable('json','type');
        Test.startTest();        
        System.enqueueJob(objQueueable);
        System.assertEquals('type',objQueueable.sObjType);
        System.assertEquals('json',objQueueable.sJson);
        Test.stopTest();
    }
    /**
    * HEP_E1_Queueable_Method --  Test method for the HEP_E1_Queueable
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_E1_Queueable_Method(){
        HEP_MarketSpendTriggerHandler.E1Wrapper objWrap = new HEP_MarketSpendTriggerHandler.E1Wrapper();
        objWrap.sPromoId = 'TestPromoId';
        objWrap.sTransactionId = 'TestTransId';
        String sSerlizedWrap = JSON.serialize(objWrap);
        HEP_E1_Queueable objQueueable = new HEP_E1_Queueable(sSerlizedWrap,'type');
        Test.startTest();        
        System.enqueueJob(objQueueable);
        Test.stopTest();
    }

    /**
    * HEP_E1_Queueable_Method2 --  Test method for the HEP_E1_Queueable
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_E1_Queueable_Method2(){
        HEP_MarketSpendTriggerHandler.E1Wrapper objWrap = new HEP_MarketSpendTriggerHandler.E1Wrapper();
        objWrap.sPromoId = 'TestPromoId';
        objWrap.sTransactionId = 'TestTransId';
        String sSerlizedWrap = JSON.serialize(objWrap);
        List<Id> lstPrSKUIds = new List<Id>();
        HEP_E1_Queueable objQueueable = new HEP_E1_Queueable(sSerlizedWrap,'type',lstPrSKUIds);
        Test.startTest();        
        System.enqueueJob(objQueueable);
        Test.stopTest();
    }
   
}