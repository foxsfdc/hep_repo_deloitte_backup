@isTest(SEEALLDATA = false)
private class HEP_Promotion_Reallocate_Controller_Test {
    
    @TestSetup
    static void testSetupMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();

        //Create User
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
                                                              'test@deloitte.com','testL','testL','Fox - New Release',true);

        //Create Line of Business
        HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);

        //Create Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','Promotion',true);

        //Create EDM Product Type.
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

        //Create EDM Title Record.
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);

        //Create Territory
        List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false)};

        lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
        lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
        lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
        lstTerritory[0].E1_Code__c  = '12341';
        lstTerritory[1].E1_Code__c  = '12342';
        lstTerritory[2].E1_Code__c  = '12343';
        insert lstTerritory;
        System.debug('Territory : ' + lstTerritory);

        //Create User Role
        List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,objUser.Id,false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,objUser.Id,false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,objUser.Id,false)};

        insert lstUserRoles;

        //Insert Spend Templates
        List<HEP_Market_Spend_Template__c> lstSpendTemplates = new List<HEP_Market_Spend_Template__c>{HEP_Test_Data_Setup_Utility.createSpendTemplate('Global Template',null,'300',lstTerritory[0].Id,false),
                                                                                                      HEP_Test_Data_Setup_Utility.createSpendTemplate('Germany Template',null,'301',lstTerritory[1].Id,false),
                                                                                                      HEP_Test_Data_Setup_Utility.createSpendTemplate('DHE Template',null,'302',lstTerritory[2].Id,false)};

        
        lstSpendTemplates[0].Unique_Id__c = lstSpendTemplates[0].Name + ' / Global';
        lstSpendTemplates[1].Unique_Id__c = lstSpendTemplates[1].Name + ' / Germany';
        lstSpendTemplates[2].Unique_Id__c = lstSpendTemplates[1].Name + ' / DHE';
        System.debug(lstSpendTemplates);
        insert lstSpendTemplates;

        //Create Template LOB
        List<HEP_Template_Line_Of_Business__c> lstTemplateLOBs = new List<HEP_Template_Line_Of_Business__c>{HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[0].Id , false),
                                                                                                            HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[1].Id , false),
                                                                                                            HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[2].Id , false)};

        insert lstTemplateLOBs;

        //Create GL Account
        List<HEP_GL_Account__c> lstGLAccount = new List<HEP_GL_Account__c>{HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 1',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 2',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 3',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 4',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 5',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 6',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 7',null,null,null,'Active',false),
                                                                           HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 8',null,null,null,'Active',false)};

        lstGLAccount[0].Account_Department__c = 'Department 1';
        lstGLAccount[1].Account_Department__c = 'Department 2';
        lstGLAccount[2].Account_Department__c = 'Department 3';
        lstGLAccount[3].Account_Department__c = 'Department 4';
        lstGLAccount[4].Account_Department__c = 'Department 5';
        lstGLAccount[5].Account_Department__c = 'Department 6';
        lstGLAccount[6].Account_Department__c = 'Department 7';
        lstGLAccount[7].Account_Department__c = 'Department 8';

        insert lstGLAccount;

        //Create Spend Template Details
        List<HEP_Market_Spend_Template_Detail__c> lstSpendTemplateDetails = new List<HEP_Market_Spend_Template_Detail__c>{HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[0].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[0].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[0].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[0].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[1].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[1].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[1].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[1].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[2].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[2].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[2].Id , false),
                                                                                                                          HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[2].Id , false)};

        insert lstSpendTemplateDetails;

        //Create Promotion
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('Global1 Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('Germany1 Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('DHE1 Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)};
        
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;
        lstPromotions[3].FirstAvailableDate__c = Date.today();
        lstPromotions[3].Title__c = objTitle.Id;
        lstPromotions[4].FirstAvailableDate__c = Date.today();
        lstPromotions[4].Title__c = objTitle.Id;
        lstPromotions[5].FirstAvailableDate__c = Date.today();
        lstPromotions[5].Title__c = objTitle.Id;
        insert lstPromotions;

        //Create Spends.

        List<HEP_Market_Spend__c> lstSpends = new List<HEP_Market_Spend__c>{HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[0].Id , 'Pending',false),
                                                                            HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[1].Id , 'Pending',false),
                                                                            HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[2].Id , 'Pending',false)};

        insert lstSpends;

        List<HEP_Market_Spend__c> lstSpends1 = new List<HEP_Market_Spend__c>{HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[3].Id , HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED') ,false),
                                                                            HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[4].Id , HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED'),false),
                                                                            HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[5].Id , HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED'),false)};

        //insert lstSpends1;

        //Create Spend Details.
        List<HEP_Market_Spend_Detail__c> lstSpendDetails1 = new List<HEP_Market_Spend_Detail__c>{HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[4].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[5].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[6].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[7].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[4].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[5].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[6].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[7].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[4].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[5].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[6].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[7].Id,'Active',false)};

        List<HEP_Market_Spend_Detail__c> lstSpendDetails = new List<HEP_Market_Spend_Detail__c>{HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[0].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[1].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[2].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[0].Id , lstGLAccount[3].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[0].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[1].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[2].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[1].Id , lstGLAccount[3].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[0].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[1].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[2].Id,'Active',false),
                                                                                                HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(lstSpends[2].Id , lstGLAccount[3].Id,'Active',false)};

        Integer i = 3;
        for(HEP_Market_Spend_Detail__c objSpendDetail : lstSpendDetails){
            objSpendDetail.Type__c = 'Current';
            objSpendDetail.RequestAmount__c = 12000;
            objSpendDetail.Budget__c = 12000;
        }
        for(HEP_Market_Spend_Detail__c objSpendDetail : lstSpendDetails1){
            objSpendDetail.Type__c = 'Current';
            objSpendDetail.RequestAmount__c = 12000;
            objSpendDetail.Budget__c = 0;
        }

        insert lstSpendDetails;
        insert lstSpendDetails1;

        Map<Id , HEP_Market_Spend__c> mapSpend = new Map<Id , HEP_Market_Spend__c>();
        Map<Id , HEP_Market_Spend__c> mapSpend1 = new Map<Id , HEP_Market_Spend__c>();
        for(HEP_Market_Spend__c objSpend : lstSpends){
            mapSpend.put(objSpend.Id , objSpend);
        }
        for(HEP_Market_Spend__c objSpend : lstSpends1){
            mapSpend1.put(objSpend.Id , objSpend);
        }
        //Cover the trigger code..
        HEP_MarketSpendTriggerHandler.createSpendHistory(mapSpend , mapSpend);

        //new HEP_MarketSpendTriggerHandler().callQueueableForE1(mapSpend1);

        //Create Catalog
        List<HEP_Catalog__c> lstCatalogs = new List<HEP_Catalog__c>{HEP_Test_Data_Setup_Utility.createCatalog('11111','Global Catalog','Single',null,lstTerritory[0].Id, 'Approved','Master',false),
                                                                    HEP_Test_Data_Setup_Utility.createCatalog('11111','Germany Catalog','Single',null,lstTerritory[1].Id, 'Approved','Master',false),
                                                                    HEP_Test_Data_Setup_Utility.createCatalog('11111','DHE Catalog','Single',null,lstTerritory[2].Id, 'Approved','Master',false)};

        lstCatalogs[0].Title_EDM__c = objTitle.Id;
        lstCatalogs[1].Title_EDM__c = objTitle.Id;
        lstCatalogs[2].Title_EDM__c = objTitle.Id;
        insert lstCatalogs[0];
        lstCatalogs[1].Global_Catalog__c = lstCatalogs[0].Id;
        lstCatalogs[2].Global_Catalog__c = lstCatalogs[0].Id;

        insert new List<HEP_Catalog__c>{lstCatalogs[1] , lstCatalogs[2]};

        //Create PromotionCatalog
        List<HEP_Promotion_Catalog__c> lstPromotionCatalogs = new List<HEP_Promotion_Catalog__c>{HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[0].Id,lstPromotions[0].Id,null,false),
                                                                                                 HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[1].Id,lstPromotions[1].Id,null,false),
                                                                                                 HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[2].Id,lstPromotions[2].Id,null,false)};

        insert lstPromotionCatalogs;

        //Create Notification Record..
        HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',true);

        //Create Approval Records.
        HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test','HEP_Market_Spend__c','RecordStatus__c','Approved','Rejected','SPEND','Direct','Pending',true);
        HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalType.Id, null , false);
        objApproval.Record_ID__c = lstSpends[0].Id;
        objApproval.HEP_Market_Spend__c = lstSpends[0].Id;
        objApproval.History_Data__c = JSON.serializePretty(new HEP_Promotion_Spend_Controller.ApprovalHistory('Test Comment' , 1000, 2000));
        insert objApproval;
        HEP_Record_Approver__c objRecordApprovers = HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, UserInfo.getUserId(), objRole.Id, 'Pending',false);
        objRecordApprovers.IsActive__c = true;
        objRecordApprovers.Approver__c = null;
        objRecordApprovers.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        insert objRecordApprovers;
    }

    @isTest
    static void test_method_one(){
        HEP_Promotion_Reallocate_Controller.SpendDetailsWrapper obj = new HEP_Promotion_Reallocate_Controller.SpendDetailsWrapper();
        Test.setCurrentPageReference(new PageReference('Page.HEP_Promotion_MyApprovalsBudgetRAView'));
        List<HEP_Promotion__c> lstPromotions = [SELECT Id , Name FROM HEP_Promotion__c];
        List<HEP_Market_Spend__c> lstSpend = [SELECT ID , Name FROM HEP_Market_Spend__c WHERE Promotion__c =: lstPromotions[0].id];
        System.currentPageReference().getParameters().put('spendId', lstSpend[0].Id);

        List<HEP_Approvals__c> lstApproval = [SELECT Id , Name FROM HEP_Approvals__c];

        HEP_Promotion_Reallocate_Controller objPromotionSpend = new HEP_Promotion_Reallocate_Controller();

        HEP_Promotion_Reallocate_Controller.getPurchaseOrder(lstPromotions[1].Id, 'DHE', '27933');
        HEP_Promotion_Reallocate_Controller.getInvoice(lstPromotions[1].Id, 'Germany', '40550');

        List<HEP_Record_Approver__c> lstRecordApprovers = [SELECT Id FROM HEP_Record_Approver__c];
        for(HEP_Record_Approver__c objRecApp : lstRecordApprovers){
            objRecApp.Approver__c = NULL;
        }
        update lstRecordApprovers;
        HEP_Promotion_Reallocate_Controller.approveRejectRecords(true , lstApproval[0].id , null);

        HEP_Promotion_Reallocate_Controller.ReallocatePageDataWrapper objReal = HEP_Promotion_Reallocate_Controller.fetchReallocatePageData(lstSpend[0].Id , HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT') , lstApproval[0].Id);
        Map<String, Schema.SObjectField> mapRecordApproverField = Schema.getGlobalDescribe().get('HEP_Record_Approver__c').getDescribe().fields.getMap();
        list<String> listRecordApproverField = new list<String>();
        listRecordApproverField.addAll(mapRecordApproverField.keyset());
        //Query to get the details for the Spends under the current Promotion.
        String sRecordApproverChildQuery = String.join(listRecordApproverField, ', ');
        sRecordApproverChildQuery += ' FROM Record_Approvers__r '; 
        sRecordApproverChildQuery = 'SELECT CreatedBy.Name ,  (SELECT ' + sRecordApproverChildQuery + ' WHERE Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' ORDER BY Sequence__c ASC),';
        //String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND HEP_Market_Spend__c = \'' + sSpendId + '\'';
        String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND Id = \'' + lstApproval[0].Id + '\'';
        String sApprovalQuery = HEP_Utility.buildQueryAllString('HEP_Approvals__c', sRecordApproverChildQuery, sApprovalWhereClause);
        
        List<HEP_Approvals__c> lstSpendApprovals = Database.query(sApprovalQuery);
        System.debug('sApprovalQuery ---> ' + sApprovalQuery);
        System.debug('lstSpendApprovals ---> ' + lstSpendApprovals);

        if(lstSpendApprovals != NULL && !lstSpendApprovals.isEmpty()){
            //objReallocatePageDataWrapper.objSpendApproval = lstSpendApprovals[0];
            objReal.objSpendApproval.objApprovalRec = new HEP_Approvals__c(Id = lstSpendApprovals[0].Id,
                                                                                Approval_Status__c = lstSpendApprovals[0].Approval_Status__c,
                                                                                Approval_Type__c = lstSpendApprovals[0].Approval_Type__c,
                                                                                Comments__c = lstSpendApprovals[0].Comments__c,
                                                                                HEP_Market_Spend__c = lstSpendApprovals[0].HEP_Market_Spend__c,
                                                                                HEP_Promotion_Dating__c = lstSpendApprovals[0].HEP_Promotion_Dating__c,
                                                                                HEP_Promotion_SKU__c = lstSpendApprovals[0].HEP_Promotion_SKU__c,
                                                                                Prior_Submitter__c = lstSpendApprovals[0].Prior_Submitter__c,
                                                                                Record_ID__c = lstSpendApprovals[0].Record_ID__c);
            
            

            HEP_Record_Approver__c objRecordApprover = new HEP_Record_Approver__c(Approval_Record__c = lstSpendApprovals[0].Id,
                                                                              Approver__c = UserInfo.getUserId(),
                                                                              Approver_Role__c = [SELECT ID FROM HEP_Role__c][0].Id,
                                                                              Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'),
                                                                              IsActive__c = Boolean.valueOf(HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE')),
                                                                              Status__c = 'Pending');
            //insert objRecordApprover;

            HEP_Record_Approver__c objRecordApprover1 = objRecordApprover.clone();
            objRecordApprover1.IsActive__c = false;
            

            List<HEP_Record_Approver__c> lstRecApps = new List<HEP_Record_Approver__c>{objRecordApprover , objRecordApprover1};
            insert lstRecApps;

            objReal.objSpendApproval.lstRecordApprover = lstRecApps;

            System.debug('Child Record Approvers ---> ' + objReal.objSpendApproval.lstRecordApprover);
            System.debug('Error might come ----> ' + objReal.objSpendApproval.objApprovalRec);

            DateTime dtSubDate = DateTime.newInstance(lstSpendApprovals[0].CreatedDate.year(), lstSpendApprovals[0].CreatedDate.month(), lstSpendApprovals[0].CreatedDate.day());
            String formattedDate = dtSubDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));

            objReal.sRequestor = lstSpendApprovals[0].CreatedBy.Name;
            objReal.sDateSubmitted = formattedDate;
            objReal.sBudgetRequestStatus = lstSpendApprovals[0].Approval_Status__c;
        }
        HEP_Promotion_Reallocate_Controller.saveReallocate(objReal);

        Test.startTest();
            
        Test.stopTest();

        System.debug('huha-->'+[SELECT Id, Approver__c,IsActive__c, Approval_Record__r.Id FROM HEP_Record_Approver__c]);    
    }

    @isTest
    static void test_method_two(){
        Test.setCurrentPageReference(new PageReference('Page.HEP_Promotion_MyApprovalsBudgetRAView'));
        List<HEP_Promotion__c> lstPromotions = [SELECT Id , Name FROM HEP_Promotion__c];
        List<HEP_Market_Spend__c> lstSpend = [SELECT ID , Name, 
                                             (SELECT Id , Name , RequestAmount__c , Budget__c
                                              FROM HEP_Market_Spend_Details__r)
                                              FROM HEP_Market_Spend__c WHERE Promotion__c =: lstPromotions[0].id];
        System.currentPageReference().getParameters().put('spendId', lstSpend[0].Id);

        List<HEP_Approvals__c> lstApproval = [SELECT Id , Name FROM HEP_Approvals__c];

        

        HEP_Promotion_Reallocate_Controller objPromotionSpend = new HEP_Promotion_Reallocate_Controller();

        HEP_Promotion_Reallocate_Controller.getPurchaseOrder(lstPromotions[1].Id, 'DHE', '27933');
        HEP_Promotion_Reallocate_Controller.getInvoice(lstPromotions[1].Id, 'Germany', '40550');

        List<HEP_Record_Approver__c> lstRecordApprovers = [SELECT Id FROM HEP_Record_Approver__c];
        for(HEP_Record_Approver__c objRecApp : lstRecordApprovers){
            objRecApp.Approver__c = NULL;
        }
        update lstRecordApprovers;
        HEP_Promotion_Reallocate_Controller.approveRejectRecords(true , lstApproval[0].id , null);

        HEP_Promotion_Reallocate_Controller.ReallocatePageDataWrapper objReal = HEP_Promotion_Reallocate_Controller.fetchReallocatePageData(lstSpend[0].Id , HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT') , lstApproval[0].Id);

        objReal.dTotalAmountNew = 2;
        objReal.dTotalAmount    = 1;

        Map<String, Schema.SObjectField> mapRecordApproverField = Schema.getGlobalDescribe().get('HEP_Record_Approver__c').getDescribe().fields.getMap();
        list<String> listRecordApproverField = new list<String>();
        listRecordApproverField.addAll(mapRecordApproverField.keyset());
        //Query to get the details for the Spends under the current Promotion.
        String sRecordApproverChildQuery = String.join(listRecordApproverField, ', ');
        sRecordApproverChildQuery += ' FROM Record_Approvers__r '; 
        sRecordApproverChildQuery = 'SELECT CreatedBy.Name ,  (SELECT ' + sRecordApproverChildQuery + ' WHERE Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' ORDER BY Sequence__c ASC),';
        //String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND HEP_Market_Spend__c = \'' + sSpendId + '\'';
        String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND Id = \'' + lstApproval[0].Id + '\'';
        String sApprovalQuery = HEP_Utility.buildQueryAllString('HEP_Approvals__c', sRecordApproverChildQuery, sApprovalWhereClause);
        
        List<HEP_Approvals__c> lstSpendApprovals = Database.query(sApprovalQuery);
        System.debug('sApprovalQuery ---> ' + sApprovalQuery);
        System.debug('lstSpendApprovals ---> ' + lstSpendApprovals);

        if(lstSpendApprovals != NULL && !lstSpendApprovals.isEmpty()){
            //objReallocatePageDataWrapper.objSpendApproval = lstSpendApprovals[0];
            objReal.objSpendApproval.objApprovalRec = new HEP_Approvals__c(Id = lstSpendApprovals[0].Id,
                                                                                Approval_Status__c = lstSpendApprovals[0].Approval_Status__c,
                                                                                Approval_Type__c = lstSpendApprovals[0].Approval_Type__c,
                                                                                Comments__c = lstSpendApprovals[0].Comments__c,
                                                                                HEP_Market_Spend__c = lstSpendApprovals[0].HEP_Market_Spend__c,
                                                                                HEP_Promotion_Dating__c = lstSpendApprovals[0].HEP_Promotion_Dating__c,
                                                                                HEP_Promotion_SKU__c = lstSpendApprovals[0].HEP_Promotion_SKU__c,
                                                                                Prior_Submitter__c = lstSpendApprovals[0].Prior_Submitter__c,
                                                                                Record_ID__c = lstSpendApprovals[0].Record_ID__c);
            
            

            HEP_Record_Approver__c objRecordApprover = new HEP_Record_Approver__c(Approval_Record__c = lstSpendApprovals[0].Id,
                                                                              Approver__c = UserInfo.getUserId(),
                                                                              Approver_Role__c = [SELECT ID FROM HEP_Role__c][0].Id,
                                                                              Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'),
                                                                              IsActive__c = Boolean.valueOf(HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE')),
                                                                              Status__c = 'Pending');
            insert objRecordApprover;

            objReal.objSpendApproval.lstRecordApprover = new List<HEP_Record_Approver__c>{objRecordApprover};

            System.debug('Child Record Approvers ---> ' + objReal.objSpendApproval.lstRecordApprover);
            System.debug('Error might come ----> ' + objReal.objSpendApproval.objApprovalRec);

            DateTime dtSubDate = DateTime.newInstance(lstSpendApprovals[0].CreatedDate.year(), lstSpendApprovals[0].CreatedDate.month(), lstSpendApprovals[0].CreatedDate.day());
            String formattedDate = dtSubDate.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));

            objReal.sRequestor = lstSpendApprovals[0].CreatedBy.Name;
            objReal.sDateSubmitted = formattedDate;
            objReal.sBudgetRequestStatus = lstSpendApprovals[0].Approval_Status__c;
        }

        HEP_Promotion_Reallocate_Controller.saveReallocate(objReal);
    }
}