/**
 * HEP_CustomerPromotionApprovalController --- Approval for MDP Products
 * @author  Nidhin V K
 */
public with sharing class HEP_CustomerPromotionApprovalController {
    
    public static String sPENDING, sCUSTOMER, sAPPROVED, sREJECTED, sACTIVE, sNATIONAL, sUSD;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sPENDING = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        sCUSTOMER = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
        sNATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sREJECTED = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
        sUSD = HEP_Utility.getConstantValue('HEP_USD');
    }
    
    /**
    * loadPageData --- load the initial data including the picklist values
    * params
    * return
    * @author   Nidhin
    */
    @RemoteAction
    public static PageData loadPageData(String sRequestType){
        Set<String> setUserRoleSearchIds = new Set<String>();
        Set<String> setLOBs = new Set<String>();
        Set<String> setCurrencyCode = new Set<String>();
        String sQuery;
        integer count = 0;
        PageData objPageData = new PageData();
        List<HEP_MDP_Promotion_Product__c> lstMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
        List<String> lstCurrencyCodes = new List<String>();
        for(HEP_User_Role__c objUserRole : HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId())){
            setUserRoleSearchIds.add(objUserRole.Search_Id__c);
            if(String.isNOtBlank(objUserRole.Role__r.LOB__c))
                setLOBs.addAll(objUserRole.Role__r.LOB__c.split(';'));
        }

        //security starts
        Map<String, String> customPermissionsMapNational = HEP_Utility.fetchCustomPermissions(null, 'HEP_Promotion_MyApprovalsNational');
        objPageData.mapCustomPermissionNationalPromotion = customPermissionsMapNational;
        
        Map<String, String> customPermissionsMapCustomer = HEP_Utility.fetchCustomPermissions(null, 'HEP_Promotion_MyApprovalsCustomer');
        objPageData.mapCustomPermissionCustomerPromotion = customPermissionsMapCustomer;

        //security ends
                
        Set<Id> setRecordIds = new Set<Id>();
        Map<Id, Id> mapRecordIdToApprovalIds = new Map<Id, Id>();
        Map<Id, String> mapRecordToRequestorName = new Map<Id, String>();
        Map<Id, Date> mapRecordToRequestDate = new Map<Id, Date>();
        Map<id, String> mapRecordToRequestId = new Map<id, String>();
        Map<id, String> mapRecordToComments = new Map<id, String>();
        for(HEP_Record_Approver__c objApprover : [SELECT
                                                    Id, Approval_Record__r.Record_ID__c,Approval_Record__r.Request_Id__c,Comments__c, Approval_Record__r.CreatedBy.Name, CreatedDate
                                                FROM
                                                    HEP_Record_Approver__c
                                                WHERE
                                                    Approval_Record__r.Approval_Status__c = :sPENDING
                                                AND
                                                    IsActive__c = true
                                                AND
                                                    Search_Id__c IN :setUserRoleSearchIds
                                                AND
                                                    Approval_Record__r.Record_Status__c = :sACTIVE
                                                AND
                                                    Record_Status__c = :sACTIVE]){
            setRecordIds.add(objApprover.Approval_Record__r.Record_ID__c);
            mapRecordIdToApprovalIds.put(objApprover.Approval_Record__r.Record_ID__c, objApprover.Id);
            mapRecordToRequestId.put(objApprover.Approval_Record__r.Record_ID__c, objApprover.Approval_Record__r.Request_Id__c);
            mapRecordToRequestorName.put(objApprover.Approval_Record__r.Record_ID__c, objApprover.Approval_Record__r.CreatedBy.Name);
            mapRecordToRequestDate.put(objApprover.Approval_Record__r.Record_ID__c, Date.valueOf(objApprover.CreatedDate));
            mapRecordToComments.put(objApprover.Approval_Record__r.Record_ID__c,objApprover.Comments__c);
        }
        
        sQuery = 'SELECT Id, HEP_Catalog__r.Catalog_Name__c,HEP_Catalog__c,Title_EDM__c,WPR_Id__c,Title_EDM__r.Name,HEP_Promotion__r.Promotion_Type__c, Title_EDM__r.FIN_PROD_ID__c,Product_Start_Date__c,Product_End_Date__c,HEP_Promotion__c, HEP_Promotion__r.Priority__c, HEP_Promotion__r.PromotionName__c, HEP_Territory__c,HEP_Territory__r.CurrencyCode__c,HEP_Territory__r.Name, HEP_Promotion__r.Customer__c,HEP_Promotion__r.Customer__r.CustomerName__c, Duration__c,HEP_Promotion__r.StartDate__c, HEP_Promotion__r.EndDate__c,Note__c, HEP_Promotion__r.Requestor__c,HEP_Promotion__r.Requestor__r.Name,(SELECT Id, Standard_Wholesale_Price__c, Channel__c, Format__c,Promo_SRP__c, Promo_WSP__c, HEP_Promotion_MDP_Product__r.Local_Description__c, Everyday_SRP__c FROM HEP_Promotion_MDP_Product_Details__r WHERE Record_Status__c = :sACTIVE) FROM HEP_MDP_Promotion_Product__c WHERE Id IN :setRecordIds AND Record_Status__c = :sACTIVE'; 
            
            if(sCUSTOMER == sRequestType){
                sQuery += ' AND HEP_Promotion__r.Promotion_Type__c = :sCUSTOMER';
            }
            if(sNATIONAL == sRequestType){
                sQuery += ' AND HEP_Promotion__r.Promotion_Type__c = :sNATIONAL';
            }
            
            lstMDPProducts = Database.Query(sQuery);
            system.debug('!!!!!!!!!'+ sRequestType);
            system.debug('----->Query '+ sQuery);
            system.debug('list-------->'+lstMDPProducts);
        for(HEP_MDP_Promotion_Product__c objProduct : lstMDPProducts){
            if(objProduct.HEP_Territory__r.CurrencyCode__c != null){    
                lstCurrencyCodes.add(objProduct.HEP_Territory__r.CurrencyCode__c);
                setCurrencyCode.add(objProduct.HEP_Territory__r.CurrencyCode__c);
            }
            else if(!setCurrencyCode.contains(objProduct.HEP_Territory__r.CurrencyCode__c)){
                lstCurrencyCodes.add(sUSD);
            }
        }

        objPageData.mapCurrencyCodeToSymbol = fetchCurrencyFormats(lstCurrencyCodes);

        for(HEP_MDP_Promotion_Product__c objPromoProduct : lstMDPProducts){
            count = count+1;
            Promotion objPromotion = new Promotion();
            objPromotion.sProductApprovalId = mapRecordIdToApprovalIds.get(objPromoProduct.Id);
            objPromotion.sRecordId = objPromoProduct.Id;
            objPromotion.sRequestId = mapRecordToRequestId.get(objPromoProduct.Id);
            if(objPromoProduct.Title_EDM__c != null){
                objPromotion.sTitleId = objPromoProduct.Title_EDM__c;
                objPromotion.sTitleName = objPromoProduct.Title_EDM__r.Name;
            }else{
                 objPromotion.sTitleId = objPromoProduct.HEP_Catalog__c;
                objPromotion.sTitleName = objPromoProduct.HEP_Catalog__r.Catalog_Name__c;
            }
            
            objPromotion.sPromotionId = objPromoProduct.HEP_Promotion__c;
            objPromotion.sPromotionName = objPromoProduct.HEP_Promotion__r.PromotionName__c;
            objPromotion.sTerritoryId = objPromoProduct.HEP_Territory__c;
            objPromotion.sTerritoryName = objPromoProduct.HEP_Territory__r.Name;
            objPromotion.sCustomerId = objPromoProduct.HEP_Promotion__r.Customer__c;
            objPromotion.sCustomerName = objPromoProduct.HEP_Promotion__r.Customer__r.CustomerName__c;
            objPromotion.sWPRId = objPromoProduct.WPR_Id__c;
            objPromotion.dtSubmittedDate = mapRecordToRequestDate.get(objPromoProduct.Id);
            if(objPromoProduct.HEP_Promotion__r.Promotion_Type__c == sCUSTOMER){
                objPromotion.dtStartDate = objPromoProduct.HEP_Promotion__r.StartDate__c;
                objPromotion.dtEndDate = objPromoProduct.HEP_Promotion__r.EndDate__c;
            }
            else if(objPromoProduct.HEP_Promotion__r.Promotion_Type__c == sNATIONAL){
                objPromotion.dtStartDate = objPromoProduct.Product_Start_Date__c;
                objPromotion.dtEndDate = objPromoProduct.Product_End_Date__c;
            }
            objPromotion.sDuration = objPromoProduct.Duration__c == NULL? '' : String.valueOf(objPromoProduct.Duration__c);
            objPromotion.sNote = objPromoProduct.Note__c;
            objPromotion.sCurrencyCode = objPromoProduct.HEP_Territory__r.CurrencyCode__c == NULL? sUSD : objPromoProduct.HEP_Territory__r.CurrencyCode__c;
            objPromotion.sRejectComments = mapRecordToComments.get(objPromoProduct.Id);


            //objPromotion.mapCurrencyCodeToSymbol = fetchCurrencyFormats(lstCurrencyCodes);

            if(String.isEmpty(objPromoProduct.Note__c)){
                objPromotion.sNotePresent = 'No';
            }
            else{
                objPromotion.sNotePresent = 'Yes';
            }
            objPromotion.sRequestorId = objPromoProduct.HEP_Promotion__r.Requestor__c;
            objPromotion.sRequestorName = mapRecordToRequestorName.get(objPromoProduct.Id);
            if(objPromoProduct.HEP_Promotion__r.Priority__c == true){
                objPromotion.sPriority = 'Yes';
            }
            else{
                objPromotion.sPriority = 'No';
            }
            
            for(HEP_MDP_Promotion_Product_Detail__c objProductDetail : objPromoProduct.HEP_Promotion_MDP_Product_Details__r){
                Product objProduct = new Product();
                objProduct.sProductId = objProductDetail.Id;
                objProduct.sChannel = objProductDetail.Channel__c;
                objProduct.sFormat = objProductDetail.Format__c;
                objProduct.dPromoWSP = objProductDetail.Promo_WSP__c;
                objProduct.dPromoSRP = objProductDetail.Promo_SRP__c;
                objProduct.dEverydaySRP = objProductDetail.Everyday_SRP__c;
                objProduct.sDescription = objProductDetail.HEP_Promotion_MDP_Product__r.Local_Description__c;
                objProduct.dWholeSaleSRP = objProductDetail.Standard_Wholesale_Price__c;
                objPromotion.lstProducts.add(objProduct);

                
            }
            objPageData.lstPromotions.add(objPromotion);
        }
        system.debug('--------->count = '+count);
        objPageData.iNoOfPendingApprovals = count;
        system.debug('12121212------>'+objPageData.iNoOfPendingApprovals);

        

        return objPageData;
    }

    public static map<String, CurrencyWrap> fetchCurrencyFormats(List<String> lstCurrencyCodes){
        try{
            list<HEP_User_Locale__mdt> lstHEPUserLocale = new list<HEP_User_Locale__mdt>();
            map<String, CurrencyWrap> mapCurrencyCodeSymbol = new map<String, CurrencyWrap>();

            for (HEP_User_Locale__mdt objHEPUserLocale : [SELECT MasterLabel, Type__c, Value__c
                                FROM HEP_User_Locale__mdt WHERE MasterLabel IN :lstCurrencyCodes AND Type__c =: HEP_Utility.getConstantValue('HEP_USER_LOCALE_CURRENCY_FORMAT')]) {
                String sValue = objHEPUserLocale.Value__c.replace('\'','\"');
                CurrencyWrap objCurrencyWrap = (CurrencyWrap)JSON.deserialize(sValue, CurrencyWrap.class);
                mapCurrencyCodeSymbol.put(objHEPUserLocale.MasterLabel, objCurrencyWrap);
            }
            return mapCurrencyCodeSymbol;
        }catch(exception ex){
            return null;
        }
    }
    
    /**
    * approveProducts --- approve/reject records
    * params
    * return
    * @author   Nidhin
    */
    @RemoteAction
    public static Boolean updateApprovalRecords(String objApprovalRecordSave, Boolean bIsApproved, String sReason){
        PageData objApprovalRecord = (PageData)JSON.deserialize(objApprovalRecordSave,PageData.class);
        List<HEP_Record_Approver__c> lstApprovers = new List<HEP_Record_Approver__c>();
        for(Promotion objPromotion : objApprovalRecord.lstPromotions){
            HEP_Record_Approver__c objApprover = new HEP_Record_Approver__c(
                                            Id = objPromotion.sProductApprovalId, 
                                            Approver__c = UserInfo.getUserId(),
                                            Status__c = bIsApproved ? sAPPROVED : sREJECTED,
                                            Comments__c = bIsApproved ? '' : sReason);
            lstApprovers.add(objApprover);
        }
        system.debug('listapprover---->'+lstApprovers);
        try{
            if(lstApprovers.size() > 0)
                update lstApprovers;
        } catch(Exception ex){
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_CustomerPromotionApprovalController','updateApprovalRecords',null,null); 
            //throw error
            System.debug('Exception on Class : HEP_CustomerPromotionApprovalController - updateApprovalRecords, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            return false;
        }
        return true;
    }
    
    /**
    * PageData --- Wrapper for page data
    * @author   Nidhin
    */
    public class PageData{
        public List<Promotion> lstPromotions;
        Integer iNoOfPendingApprovals;
        map<String,String> mapCustomPermissionNationalPromotion;
        map<String,String> mapCustomPermissionCustomerPromotion;
        public map<String , CurrencyWrap> mapCurrencyCodeToSymbol;
        public PageData(){
            lstPromotions = new List<Promotion>();
            iNoOfPendingApprovals = 0;
        }
    }
    
    /**
    * Promotion --- Promotion related data
    * @author   Nidhin
    */
    public class Promotion{
        public String sTitleId, sTitleName, sPromotionId, sPromotionName, sTerritoryId, sTerritoryName;
        public String sCustomerId, sCustomerName, sWPRId, sRequestorId, sRequestorName, sDuration, sNote;
        public String sProductApprovalId, sRecordId, sPriority, sNotePresent, sRejectComments, sRequestId;
        public String sCurrencyCode;
        public Boolean bIschecked;
        public Boolean bIsCollapsed;
        public Date dtStartDate, dtEndDate, dtSubmittedDate;
        public List<Product> lstProducts;
        public Promotion(){
            lstProducts = new List<Product>();
            this.bIschecked = false;
            this.bIsCollapsed=false;
        }
    }
    
    /**
    * Product --- Product related data
    * @author   Nidhin
    */
    public class Product{
        public String sProductId, sChannel, sFormat, sDescription;
        public Decimal dPromoWSP, dPromoSRP, dWholeSaleSRP, dEverydaySRP;
       
    }

    /**
    * CurrencyWrap -- class to store all the currency formats depending on the Territory 
    * @author    Arjun Narayanan
    */
    public class CurrencyWrap{
        public String sSymbol;
        public String sPlacement;
        public Integer iNoOfDecimalPlaces;
    }
}