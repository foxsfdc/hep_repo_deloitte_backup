@isTest
private class HEP_Promotion_Documents_Ctrl_Test {

    @testSetup
    static void HEP_BoxServices_createData() {
        
         // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Jain', 'anshuljain787@deloitte.com', 'AJ', 'AJ', 'AJ', '', false);
        insert objUser;
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'Domestic', 'Subsidiary', null, null, 'EUR', 'USD', '509', null, false);
        objTerritory.Flag_Country_Code__c = 'US';
        objTerritory.Territory_Code__c = '12355';
        insert objTerritory;
        
        // HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Subsidiary', null, null, 'EUR', 'USD', '509', null, false);
        // objTerritory1.Flag_Country_Code__c = 'GO';
        // objTerritory1.Territory_Code__c = '12357';
        // insert objTerritory1;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season', 'SEASN', false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id, false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB', 'New Release', false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion', 'Global', '', objTerritory.Id, objLOB.Id, '', '', false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;

    }

    @isTest
    static void testcreatePromoFolders() {
        HEP_Promotion__c objGlobalPromotion = [Select Id FROM HEP_Promotion__c WHERE PromotionName__c = 'Global Promotion'];
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        PageReference objVfPage = Page.HEP_Promotion_Documents;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId', objGlobalPromotion.Id);
        System.runAs(lstUser[0]) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            String sResult = HEP_Promotion_Documents_Ctrl.createPromoFolders((String) objGlobalPromotion.Id);
            System.debug('-------->' + sResult);
            Test.stoptest();
        }
    }

    @isTest
    static void testgetFolderDetails() {
        HEP_Promotion__c objGlobalPromotion = [Select Id FROM HEP_Promotion__c WHERE PromotionName__c = 'Global Promotion'];
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        PageReference objVfPage = Page.HEP_Promotion_Documents;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId', objGlobalPromotion.Id);
        HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        System.runAs(lstUser[0]) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            Map < String, String > mapResult = HEP_Promotion_Documents_Ctrl.getFolderDetails((String) objGlobalPromotion.Id);
            System.debug('-------->' + mapResult);
            Test.stoptest();
        }
    }

    @isTest
    static void testCheckPermissions1() {
        
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Territory__c > lstTerritory = [SELECT Id FROM HEP_Territory__c WHERE Name = 'US'];
        List < HEP_Role__c > lstRole = [SELECT Id FROM HEP_Role__c WHERE Name = 'Brand Manager'];
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id, lstRole[0].Id, lstUser[0].Id, false);
        insert objUserRole;
        
        HEP_Promotion__c objGlobalPromotion = [Select Id FROM HEP_Promotion__c WHERE PromotionName__c = 'Global Promotion'];
        PageReference objVfPage = Page.HEP_Promotion_Documents;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId', objGlobalPromotion.Id);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Promotion_Documents_Ctrl objDocument = new HEP_Promotion_Documents_Ctrl();
            objDocument.fetchPermissions();
            test.stopTest();
        }
    }
    
    @isTest
    static void testCheckPermissions2() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        HEP_Promotion__c objGlobalPromotion = [Select Id FROM HEP_Promotion__c WHERE PromotionName__c = 'Global Promotion'];
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(objGlobalPromotion.Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Promotion_Documents_Ctrl objDocument = new HEP_Promotion_Documents_Ctrl();
            objDocument.fetchPermissions();
            
            test.stopTest();
        }
    }
    
    
    // @isTest
    // static void testClassObject() {
    //     HEP_Promotion__c objGlobalPromotion = [Select Id FROM HEP_Promotion__c WHERE PromotionName__c = 'Global Promotion'];
    //     list < HEP_Constants__c > objHEPConstant = HEP_Test_Data_Setup_Utility.createHEPConstants();
    //     list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    //     List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
    //     PageReference objVfPage = Page.HEP_Promotion_Documents;
    //     Test.setCurrentPage(objVfPage);
    //     ApexPages.currentPage().getParameters().put('promoId', objGlobalPromotion.Id);
    //     HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
    //     System.runAs(lstUser[0]) {
    //         Test.startTest();
    //         Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
    //         Map < String, String > mapResult = HEP_Promotion_Documents_Ctrl.getFolderDetails((String) objGlobalPromotion.Id);
    //         System.debug('-------->' + mapResult);
    //         Test.stoptest();
    //     }
    // }

}