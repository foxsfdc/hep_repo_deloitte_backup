/**
* HEP_ProductDetails -- Controller to store Product and Catalog details 
* @author    Lakshman Jinnuri
*/
public class HEP_ProductDetails implements HEP_IntegrationInterface{ 
    /**
    * performTransaction -- Method to store Catalog details 
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_ProductCatalogWrapper objWrapper = new HEP_ProductCatalogWrapper();  
        if(HEP_Constants__c.getValues('HEP_FOXIPEDIA_OAUTH') != null && HEP_Constants__c.getValues('HEP_FOXIPEDIA_OAUTH').Value__c != null)   
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Constants__c.getValues('HEP_FOXIPEDIA_OAUTH').Value__c);
        System.debug('sAccessToken++++'+sAccessToken);

        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c != null            
            && sAccessToken.startsWith(HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c) 
            && objTxnResponse != null){
            String sFoxId;
            if(objTxnResponse.sSourceId != null)
                sFoxId = objTxnResponse.sSourceId;
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for Authentication
            if(HEP_Constants__c.getValues('HEP_PRODUCT_DETAILS') != null && HEP_Constants__c.getValues('HEP_PRODUCT_DETAILS').Value__c != null) 
                objServiceDetails = HEP_Services__c.getInstance(HEP_Constants__c.getValues('HEP_PRODUCT_DETAILS').Value__c);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?foxId=' + sFoxId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('system',System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                System.debug('objHttpResp.getBody()++++' + objHttpResp.getBody());
                if(HEP_Constants__c.getValues('HEP_STATUS_OK') != null  && HEP_Constants__c.getValues('HEP_STATUS_OK').Value__c != null){
                    if(objHttpResp.getStatus().equals(HEP_Constants__c.getValues('HEP_STATUS_OK').Value__c)){
                        //Gets the details from Foxipedia interface and stores in the wrapper 
                        objWrapper = (HEP_ProductCatalogWrapper)JSON.deserialize(objHttpResp.getBody(), HEP_ProductCatalogWrapper.class);    
                        System.debug('objWrapper :' + objWrapper );
                        if(objWrapper.errors != null && !objWrapper.errors.isEmpty() && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                            System.debug(objWrapper.errors[0].detail);
                            if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null)
                                objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                            if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length() > 254)
                                objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                            else
                                objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                            objTxnResponse.bRetry = true;
                        }else{
                            if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c != null)         
                                objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c;
                            objTxnResponse.bRetry = false;
                        }
                    }
                    else{
                        if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null )
                            objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                        else    
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    }
                }        
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Constants__c.getValues('HEP_INVALID_TOKEN').Value__c != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null ){
                objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                objTxnResponse.sErrorMsg = HEP_Constants__c.getValues('HEP_INVALID_TOKEN').Value__c ;
                objTxnResponse.bRetry = true;
            }    
        }     
    }    
}