/**
* HEP_Catalog_Commponents_Extractor  -- Fetches all catalog components for a catalog
* @author    Abhishek Mishra
*/
public with sharing class HEP_Catalog_Components_Extractor implements HEP_IntegrationInterface{ 
    public HEP_Catalog_Components_Extractor(){}
    /**
    * performTransaction -- Fetches real time catalog components from Foxipedia 
    * @author  :  Abhishek Mishra
    * @return no return value
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        string sAccessToken;
        HEP_Foxipedia_CatalogWrapper objWrapper = new HEP_Foxipedia_CatalogWrapper();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH'));
        system.debug('sAccessToken : ' + sAccessToken);
         if (sAccessToken != null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null) {
            String sCatalogId = objTxnResponse.sSourceId;
            HTTPRequest objHttpRequest = new HTTPRequest();
        
            //Gets the Details from custom setting for Authentication
            HEP_Services__c objServiceDetails = HEP_Services__c.getInstance('Foxipedia_Catalog_Service');
            system.debug('objServiceDetails:' + objServiceDetails);
            //Using the Catalog Id retrieved and make a callout to Foxipedia API to pull the Catalog details
            if (objServiceDetails != null) {
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?productId=' + sCatalogId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization', sAccessToken);
                objHttpRequest.setHeader('system', System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(Integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                system.debug('objHttpResp : ' + objHttpResp);
                //Response from callout
                objTxnResponse.sResponse = objHttpResp.getBody();
                if (objHttpResp.getStatus().equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATUS_OK'))) {
                    //De-serialize the response into a wrapper
                    system.debug('objHttpResp.getBody() : ' + objHttpResp.getBody());
                    objWrapper = (HEP_Foxipedia_CatalogWrapper) JSON.deserialize(objHttpResp.getBody(), HEP_Foxipedia_CatalogWrapper.class);
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                    if (objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)) {
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if (objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length() > 254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }
                } else {
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if (objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                }
            }
            //if the token is not valid or if we get the invalid transaction response
        } else {
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }
    }
}