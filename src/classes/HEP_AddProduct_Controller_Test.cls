@isTest
Public class HEP_AddProduct_Controller_Test{
    //test class

    @testSetup 
    Static void setup(){

            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
            // List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
            


            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
            insert objTitle;
            
            GTS_Title__c emdTitle = HEP_Test_Data_Setup_Utility.createGTSTitle('Test', '23456', 'tyu', '123', false);
            emdTitle.WPR__c = '123';
            emdTitle.US_Release_Date__c =Date.Today();
            insert emdTitle;
            
            TVD_Release_Dates__c tvdrd = new TVD_Release_Dates__c();
            
            tvdrd.Title_EDM__c = objTitle.Id;
            tvdrd.TVD_Release_Date__c = Date.Today();
            tvdrd.Country_Code__c = 'US';
            insert tvdrd;

            HEP_Notification_Template__c objNotification = new HEP_Notification_Template__c(Name = 'Global_FAD_Date_Change',
                                                    HEP_Body__c = '<old Global FAD> updated to <new Global FAD>',HEP_Object__c = 'HEP_Promotion__c',
                                                    Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = 'Global_FAD_Date_Change',Status__c='-');
    

            insert objNotification;


            HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
            objTerritory.Flag_Country_Code__c = 'US';
            objTerritory.MM_Territory_Code__c = 'US';
            insert objTerritory;

            HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia1', 'EMEA', 'Subsidiary', null, objTerritory.Id, 'ARS', 'AUS1', 'AUS1', '', false);
            objTerritory1.Flag_Country_Code__c = 'AU';
            insert objTerritory1;

            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test User',objTerritory.id,'1111','MDP Customers',NUll,False);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;

            HEP_Customer__c objCustomer1 = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test User1',objTerritory.id,'11111','MDP Customers',NUll,False);
            objCustomer1.Record_Status__c = 'Active';
            objCustomer1.Parent_Customer__c = objCustomer.id;
            objCustomer1.Channel__c = 'EST';
            objCustomer1.Customer_Number__c = '11111';
            insert objCustomer1;


            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'National', null,objTerritory.Id, null, null,null,false);
            objPromotion.Record_Status__c= 'Active'; 
            objPromotion.StartDate__c = date.today();
            objPromotion.EndDate__c = date.today()+20;
            objPromotion.Customer__c = objCustomer.id;
            objPromotion.TPR_Only__c = false;
            objPromotion.Status__c = 'Published';
            insert objPromotion;

            HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
            objCatalog.Title_EDM__c = objTitle.id;
            objCatalog.Record_Status__c = 'Active';
            objCatalog.Catalog_Type__c = 'Bundle'; 
            insert objCatalog;        
            


            HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,objTitle.id,objTerritory.id,false);
            objPromotionProduct.Product_End_Date__c = date.today()+15;
            objPromotionProduct.Product_Start_Date__c = date.today()+10; 
            objPromotionProduct.Approval_Status__c = 'Approved';
            objPromotionProduct.Record_Status__c = 'Active';
            objPromotionProduct.HEP_Catalog__c = objCatalog.id;
            objPromotionProduct.customer_id__c = '694505519';
            insert objPromotionProduct;         

            HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsHD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'HD','EST',false);
            objPromotionDetialsHD.Promo_WSP__c = 10;
            objPromotionDetialsHD.Record_Status__c = 'Active';
            objPromotionDetialsHD.CustomerId__c = '694505519';
            insert objPromotionDetialsHD;
        
            HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsSD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'SD','EST',false);
            objPromotionDetialsSD.Promo_WSP__c = 10;
            objPromotionDetialsSD.Record_Status__c = 'Active';
            objPromotionDetialsSD.CustomerId__c = '694505519';
            insert objPromotionDetialsSD;      

            HEP_Catalog__c objCatalog1 = HEP_Test_Data_Setup_Utility.createCatalog('3435','Catalog Sample1', 'Box Set1', null, objTerritory.Id, 'Approved', 'Request', null);
            objCatalog1.Title_EDM__c = objTitle.id;
            objCatalog1.Record_Status__c = 'Active';
            objCatalog1.Catalog_Type__c = 'Single'; 
            objCatalog1.Digital_Physical__c = 'Digital';
            insert objCatalog1;   


            HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objPromotion.Id, null, true);
            HEP_Promotion_Catalog__c objPromoCatalog1 =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog1.Id, objPromotion.Id, null, true);

            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, false);
            objUserRole.Record_Status__c = 'Active';
            insert objUserRole;

            HEP_Promotion_Region__c  objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id,objTerritory.Id,true);

            HEP_MM_Contract__c objMMContract = HEP_Test_Data_Setup_Utility.createMMContract('US Test User',null,null,false);
            objMMContract.MM_CustomerID__c = '11111';
            objMMContract.MM_ContractID__c = '11111';
            objMMContract.Status__c  = 'Active';
            insert objMMContract;

            HEP_MM_ContractProduct__c objMMContractProduct =  HEP_Test_Data_Setup_Utility.createMMContractProduct('11111',null,null,null,false);
            objMMContractProduct.Financial_ProductID__c ='1234';
            insert objMMContractProduct;

             HEP_MDP_Product_Mapping__c objPromotionProductMapping = HEP_Test_Data_Setup_Utility.createPromotionProductMapping(objCatalog.Id,objTerritory.Id,objCustomer.id,objTitle.id,false);
           objPromotionProductMapping.channel__c = 'EST';
           objPromotionProductMapping.Format__c='HD';
           objPromotionProductMapping.Record_Status__c='Active';
           objPromotionProductMapping.Local_Description__c ='test';
           insert objPromotionProductMapping;
            //   HEP_MDP_Product_Mapping__c objPromotionProductMapping1= HEP_Test_Data_Setup_Utility.createPromotionProductMapping(objTitle.Id,objTerritory.Id,objCustomer.id,objTitle.id,false);
            //  insert objPromotionProductMapping1;
            
            //  list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
             List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
                HEP_Constants__c objcon = new HEP_Constants__c();
                objcon.Name = 'HEP_Global_FAD_Date_Change';
                objcon.Value__c = 'Global_FAD_Date_Change';
                insert objcon;
             list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
             list<HEP_List_Of_Values__c> lstHEPListofValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();

    }

    @isTest 
    static void Test1(){   

    user u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
    

    System.runAs(u){
    

        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c limit 1];
        EDM_GLOBAL_TITLE__c objTitle = [select id from EDM_GLOBAL_TITLE__c limit 1];
        HEP_MDP_Promotion_Product__c objMDPPromotionProduct = [select id from HEP_MDP_Promotion_Product__c limit1];
        HEP_Catalog__c objCatalog = [select id from HEP_Catalog__c limit 1];
        HEP_Territory__c objTerritory = [select id from HEP_Territory__c limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        HEP_AddProduct_Controller  obj = new HEP_AddProduct_Controller();
        HEP_AddProduct_Controller.searchData('The Simpsons','Titles',null,null,objPromotion.id);

        HEP_AddProduct_Controller.mdpProductDetails wrapProd = new  HEP_AddProduct_Controller.mdpProductDetails();
        list<HEP_AddProduct_Controller.mdpProductDetails> lstwrapProd = new  list<HEP_AddProduct_Controller.mdpProductDetails>();
        wrapProd.sFormat = 'EST';
        wrapProd.sChannel = 'SD';
        wrapProd.dwsp = 10.00;
        wrapProd.dsrp = 20.00;
        lstwrapProd.add(wrapProd);


        list<HEP_AddProduct_Controller.searchproductWrapper> lstwrapobj = new list<HEP_AddProduct_Controller.searchproductWrapper>();
        list<String> lstChannel = new list<String>();
        list<String> lstFormat = new list<String>();
        HEP_AddProduct_Controller.searchproductWrapper wrapobj = new HEP_AddProduct_Controller.searchproductWrapper
                                                                ('Titles',
                                                                'The Simpsons',objTitle.id,NULL,objMDPPromotionProduct.id,
                                                                'Global Promotion 1','1234','FOX',NULL,objTerritory.id,NULL,'Titles',date.today()-1,
                                                                date.today()+20,date.today(),date.today(),Null,NULL);
        system.debug(lstwrapProd);
        system.debug('lstwrapProd');
        wrapobj.lstMdpProductDetails = lstwrapProd;
        lstwrapobj.add(wrapobj);

        String svalue = JSON.serialize(lstwrapobj); 
        system.debug('aaaaaaaaa'+svalue); 
        lstChannel.add('HD');lstChannel.add('SD');
        String schannel = JSON.serialize(lstChannel); 
        lstFormat.add('EST');lstFormat.add('VOD');
        String sFormate = JSON.serialize(lstFormat);
        HEP_AddProduct_Controller.saveData(svalue,objPromotion.id,sChannel,sFormate,'National');
        Test.stopTest();
    
        }   

    }   


    @isTest 
    static void Test2(){   

    user u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];


    System.runAs(u){
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id,Promotion_Type__c from HEP_Promotion__c limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        HEP_AddProduct_Controller  obj = new HEP_AddProduct_Controller();
        HEP_AddProduct_Controller.searchData('','Promotions Name',date.today()-1,date.today()+6,objPromotion.id);
        objPromotion.Promotion_Type__c = 'Customer';
        update objPromotion;
        EDM_GLOBAL_TITLE__c objTitle = [select id from EDM_GLOBAL_TITLE__c limit 1];
        HEP_MDP_Promotion_Product__c objMDPPromotionProduct = [select id from HEP_MDP_Promotion_Product__c limit1];
        HEP_Catalog__c objCatalog = [select id from HEP_Catalog__c limit 1];
        HEP_Territory__c objTerritory = [select id from HEP_Territory__c limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);

        HEP_AddProduct_Controller.searchData('The Simpsons','Titles',null,null,objPromotion.id);

        HEP_AddProduct_Controller.mdpProductDetails wrapProd = new  HEP_AddProduct_Controller.mdpProductDetails();
        list<HEP_AddProduct_Controller.mdpProductDetails> lstwrapProd = new  list<HEP_AddProduct_Controller.mdpProductDetails>();
        wrapProd.sFormat = 'EST';
        wrapProd.sChannel = 'SD';
        wrapProd.dwsp = 10.00;
        wrapProd.dsrp = 20.00;
        lstwrapProd.add(wrapProd);


        list<HEP_AddProduct_Controller.searchproductWrapper> lstwrapobj = new list<HEP_AddProduct_Controller.searchproductWrapper>();
        list<String> lstChannel = new list<String>();
        list<String> lstFormat = new list<String>();
        HEP_AddProduct_Controller.searchproductWrapper wrapobj = new HEP_AddProduct_Controller.searchproductWrapper
                                                                ('Promotions Name',
                                                                'The Simpsons',objTitle.id,NULL,objMDPPromotionProduct.id,
                                                                'Global Promotion 1','1234','FOX',NULL,objTerritory.id,NULL,'Titles',date.today()-1,
                                                                date.today()+20,date.today(),date.today(),Null,NULL);
        wrapobj.lstMdpProductDetails = lstwrapProd;
        lstwrapobj.add(wrapobj);

        String svalue = JSON.serialize(lstwrapobj); 
        system.debug('aaaaaaaaa'+svalue); 
        lstChannel.add('HD');lstChannel.add('SD');
        String schannel = JSON.serialize(lstChannel); 
        lstFormat.add('EST');lstFormat.add('VOD');
        String sFormate = JSON.serialize(lstFormat);
        HEP_AddProduct_Controller.saveData(svalue,objPromotion.id,sChannel,sFormate,'Digital Account');


        Test.stopTest();
        }   

    }

    @isTest 
    static void Test3(){   

    user u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];

    System.runAs(u){
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        HEP_AddProduct_Controller  obj = new HEP_AddProduct_Controller();
        HEP_AddProduct_Controller.searchData('','',date.today()-1,date.today()+60,objPromotion.id);
        Test.stopTest();
    
        }   

    }

     @isTest 
    static void Test4(){   

    user u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];


    System.runAs(u){
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id,Promotion_Type__c from HEP_Promotion__c limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        HEP_AddProduct_Controller  obj = new HEP_AddProduct_Controller();
        HEP_AddProduct_Controller.searchData('','Promotions Name',date.today()-1,date.today()+6,objPromotion.id);
        objPromotion.Promotion_Type__c = 'Customer';
        update objPromotion;
        EDM_GLOBAL_TITLE__c objTitle = [select id from EDM_GLOBAL_TITLE__c limit 1];
        HEP_MDP_Promotion_Product__c objMDPPromotionProduct = [select id from HEP_MDP_Promotion_Product__c limit1];
        HEP_Catalog__c objCatalog = [select id from HEP_Catalog__c limit 1];
        HEP_Territory__c objTerritory = [select id from HEP_Territory__c  limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Digital_AddProducts'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);

        HEP_AddProduct_Controller.searchData('The Simpsons','Titles',null,null,objPromotion.id);

        HEP_AddProduct_Controller.mdpProductDetails wrapProd = new  HEP_AddProduct_Controller.mdpProductDetails();
        list<HEP_AddProduct_Controller.mdpProductDetails> lstwrapProd = new  list<HEP_AddProduct_Controller.mdpProductDetails>();
        wrapProd.sFormat = 'EST';
        wrapProd.sChannel = 'SD';
        wrapProd.dwsp = 10.00;
        wrapProd.dsrp = 20.00;
        lstwrapProd.add(wrapProd);


        list<HEP_AddProduct_Controller.searchproductWrapper> lstwrapobj = new list<HEP_AddProduct_Controller.searchproductWrapper>();
        list<String> lstChannel = new list<String>();
        list<String> lstFormat = new list<String>();
        HEP_AddProduct_Controller.searchproductWrapper wrapobj = new HEP_AddProduct_Controller.searchproductWrapper
                                                                ('Titles','The Simpsons',objTitle.id,NULL,NULL,
                                                                'Global Promotion 1','1234','FOX',NULL,objTerritory.id,NULL,'Titles',date.today()-1,
                                                                date.today()+20,date.today(),date.today(),NULL,NULL);
        wrapobj.lstMdpProductDetails = lstwrapProd;
        lstwrapobj.add(wrapobj);

        String svalue = JSON.serialize(lstwrapobj); 
        system.debug('aaaaaaaaa'+svalue); 
        lstChannel.add('HD');lstChannel.add('SD');
        String schannel = JSON.serialize(lstChannel); 
        lstFormat.add('EST');lstFormat.add('VOD');
        String sFormate = JSON.serialize(lstFormat);
        HEP_AddProduct_Controller.saveData(svalue,objPromotion.id,sChannel,sFormate,'Digital Account');


        Test.stopTest();
        }   

    }
}