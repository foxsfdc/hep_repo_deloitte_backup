/**
 * HEP_FAD_Override_Approval_Process -tee-- Test class for HEP_FAD_Override_Approval_Process
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_PromotionTriggerHandler_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', true);
        HEP_Territory__c objLocalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('South Korea', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Territory__c objAutoTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '183', 'JDE', true);
        HEP_Territory__c objUSTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '184', 'JDE', true);
        objAutoTerritory.Auto_Catalog_Creation__c = true;
        update objAutoTerritory;

        //Line of business
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        //Global Promotion
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestGlobalPromotion', 'Global', '', objGlobalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objGlobalPromotion.LocalPromotionCode__c = '123456789';
        objGlobalPromotion.FirstAvailableDate__c = system.today() + 2;
        insert objGlobalPromotion;

        //National SK Promotion
        HEP_Promotion__c objNationalSKPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestSKPromotion', 'National', objGlobalPromotion.id, objLocalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objNationalSKPromotion.LocalPromotionCode__c = '123456710';
        insert objNationalSKPromotion;

        //National DHE Promotion
        HEP_Promotion__c objNationalDHEPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestDHEPromotion', 'National', objGlobalPromotion.id, objAutoTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objNationalDHEPromotion.LocalPromotionCode__c = '123456710';
        insert objNationalDHEPromotion;

        objNationalDHEPromotion.Record_status__C = 'Active';
        update objNationalDHEPromotion;

        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c objDatingMatrixDHE = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objAutoTerritory.Id, objAutoTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixSK = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objLocalTerritory.Id, objLocalTerritory.Id, true);

        //Create dating records 
        HEP_Promotion_Dating__c DatingSKRec = HEP_Test_Data_Setup_Utility.createDatingRecord(objLocalTerritory.Id, objNationalSKPromotion.Id, objDatingMatrixSK.id, true);
        HEP_Promotion_Dating__c DatingDHERec = HEP_Test_Data_Setup_Utility.createDatingRecord(objLocalTerritory.Id, objNationalDHEPromotion.Id, objDatingMatrixDHE.id, true);
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objLocalTerritory.Id, objGlobalPromotion.Id, objDatingMatrixSK.id, true);
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(objUSTerritory.Id, objNationalSKPromotion.Id, objDatingMatrixDHE.id, true);

        HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',true); 
    }                                                   

    public static testMethod void test() {
        
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Promotion__c> lstSKPromotion = [SELECT Id, Global_Promotion__c	 FROM HEP_Promotion__c where PromotionName__c = 'TestSKPromotion'];
        list<HEP_Promotion__c> lstDHEPromotion = [SELECT Id FROM HEP_Promotion__c where PromotionName__c = 'TestDHEPromotion'];
        list<HEP_Promotion__c> lstGlobalPromotion = [SELECT Id,FirstAvailableDateFormula__c,Promotion_Type__c FROM HEP_Promotion__c where PromotionName__c = 'TestGlobalPromotion'];
        lstDHEPromotion[0].Global_Promotion__c = null;
        lstDHEPromotion[0].Status__c = 'Published';
        update lstDHEPromotion;
       	map<id, HEP_Promotion__c> mapOldSKPromotions = new map<id, HEP_Promotion__c>(lstSKPromotion);
       	list<HEP_Promotion__c> lstUpdatePromotion = new list<HEP_Promotion__c>(lstGlobalPromotion);
       	lstUpdatePromotion[0].FirstAvailableDate__c = system.today() + 10;
       	update lstUpdatePromotion;
       	map<id, HEP_Promotion__c> mapOldGlobalPromotions = new map<id, HEP_Promotion__c>(lstUpdatePromotion);

        System.runAs(u) {
            Test.startTest();
                HEP_PromotionTriggerHandler.handleAfterUpdate(lstSKPromotion, mapOldSKPromotions);
                HEP_PromotionTriggerHandler.AlertsAfterUpdate(lstGlobalPromotion, mapOldGlobalPromotions);
            Test.stoptest();
        }
    }
}