@isTest
Public class HEP_FMC_JWT_Token_Test{
   
   
   
    @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }    
        
    @isTest 
    static void Success_Test(){   
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = HEP_Utility.getConstantValue('PROMOTION_LANDING_PAGE');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_JWT_Token  demo = new HEP_FMC_JWT_Token ();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest 
    static void Error_Test(){   
      
        HEP_Services__c objService = new HEP_Services__c();
        objService = [select id,Name,Endpoint_URL__c from HEP_Services__c where Name = 'HEP_FMC_JWT_TOKEN'];
        objService .Endpoint_URL__c = '/fmcproxy/fmcapi/v2/token/jwt/errors';
        update objService;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_JWT_Token  demo = new HEP_FMC_JWT_Token ();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest 
    static void Access_Error_Test(){   
     
        HEP_Constants__c objconstant = new HEP_Constants__c();
        objconstant = [select id,Name from HEP_Constants__c where Name = 'HEP_SESSION_ID'];
        delete objconstant;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_JWT_Token  demo = new HEP_FMC_JWT_Token ();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
}