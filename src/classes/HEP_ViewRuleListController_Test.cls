/**
 * HEP_ViewRuleListController_Test --- Test Class for HEP_ViewRuleListController
 * @author  Nidhin V K
 */
@isTest
private class HEP_ViewRuleListController_Test {
    
    @testSetup static void setupData() {
        HEP_Test_Data_Setup_Utility.createRuleCriteria(HEP_Test_Data_Setup_Utility.createRule(true).Id, TRUE);
    }
    
    @isTest static void testRuleCriteriaMethods() {
        Test.startTest();
        PageReference pageRef = Page.HEP_ViewRuleList;
        HEP_ViewRuleListController objExtension = new HEP_ViewRuleListController();
        System.assert(objExtension.getRules().size() > 0);
        Test.stopTest();
    }
}