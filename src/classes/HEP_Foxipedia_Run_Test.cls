@isTest
Public class HEP_Foxipedia_Run_Test{
    
    @testSetup
    static void testDataSetUpMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    

    /*public static testmethod void test(){
         String catalogFoxId = '5818497';
         HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(catalogFoxId, '', 'HEP_Foxipedia_Run');
         HEP_Foxipedia_Run obj = new HEP_Foxipedia_Run();
         obj.performTransaction(objIntTxnRsp);
    }*/
    
    
    @isTest 
    static void HEP_Foxipedia_Run_InvalidAcessTokenTest() {
        
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
       objService.Endpoint_URL__c = 'https://ms-qaapi.foxinc.com';
        objService.Service_URL__c = '/v1/foxipedia/global/api/title/details';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer54444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Run demo = new HEP_Foxipedia_Run();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
     @isTest 
     static void HEP_Foxipedia_Run_SuccessTest() {
          
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '67728'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Run demo = new HEP_Foxipedia_Run();
       // system.debug('TRANSAC RESPONSE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    @isTest 
    static void HEP_Foxipedia_Run_FailureTest() {
        
      //  list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer567854444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Run demo = new HEP_Foxipedia_Run();
    //    system.debug('TRANSAC RESPONSE FAILURE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

}