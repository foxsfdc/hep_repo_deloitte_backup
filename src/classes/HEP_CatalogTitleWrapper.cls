/**
* HEP_RatingWrapper -- Wrapper to store Catalog Details on Master Page 
* @author    Roshi Rai
*/
public class HEP_CatalogTitleWrapper{
    public list < Data > data{get;set;}
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public LinksOuter links{get;set;}  
    public list<String> HEPerrors { get; set; }  
    public list<Errors> errors { get; set; }
     
    /**
    * Data -- Wrapper to store Data details 
    * @author    Roshi Rai
    */
    public class Data {
        public Attributes attributes{get;set;}
        public String id{get;set;}
        public String type{get;set;}
        public Relationships relationships{get;set;}
       
    }

    /**
    * Attributes -- Wrapper to store Attributes details 
    * @author    Roshi Rai
    */
    public class Attributes {
        public String type{get;set;}
        public String foxId{get;set;}
        public String primaryClassificationCode{get;set;}
        public String productTypeCode{get;set;}
        public String productTypeDescription{get;set;}
        public String productId{get;set;}
        public String productName{get;set;}
        public String productStatusCode{get;set;}
        public String productStatusDescription{get;set;}
        public String globalEpisodeConfigIndicator{get;set;}
        public String productStatusComment{get;set;}
        public String productComment{get;set;}
        public String primaryClassificationDescription{get;set;}
        
    }

    /**
    * Relationships -- Wrapper to store Relationships details 
    * @author    Roshi Rai
    */
    public class Relationships {
        public Title title{get;set;}
    }

    /**
    * Title -- Wrapper to store Title details 
    * @author    Roshi Rai
    */
    public class Title {
        public Links links{get;set;}
    }

    /**
    * Links -- Wrapper to store Links details 
    * @author    Roshi Rai
    */
    public class Links {
        public String related{get;set;}
    }
    
    /**
    * Links_Outer -- Wrapper to store Links details 
    * @author   Roshi Rai
    */
    public class LinksOuter {
        public String self {get;set;} 
    }    
    
    /**
    * Errors -- Class to hold related Error details 
    * @author    Roshi Rai
    */
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }
}