/**
* HEP_Auto_Lock_Promo_Dating_Batch_Test -- Test class for the HEP_Auto_Lock_Promo_Dating_Batch. 
* @author    Lakshman Jinnuri
*/
@isTest private class HEP_Auto_Lock_Promo_Dating_Batch_Test{
    /**
    * CreateData --  Test method to get Crete the Catalog Genre Details
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    
    @testSetup
     static void createUsers(){
     	
     	list < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
        objRoleOperations.Destination_User__c = u.Id;
        objRoleOperations.Source_User__c = u.Id;
        insert objRoleOperations;
        HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
        objRoleFADApprover.Destination_User__c = u.Id;
        objRoleFADApprover.Source_User__c = u.Id;
        insert objRoleFADApprover;
    }
    
    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_National() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','US','5096',null,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('S&D - Catalog','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','VOD Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Digital';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotions_DatingMatrix__c objDatingMatrix1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix1.Line_of_Business_Type__c = 'TV';
        objDatingMatrix1.Media_Type__c = 'Digital';
        objDatingMatrix1.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix1;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Record_Status__c = 'Active';
        objDatingRecord.Date__c =  System.today()+57;
        objDatingRecord.Date_Type__c = 'VOD Release Date';
        insert objDatingRecord;
        HEP_Promotion_Dating__c objDatingRecord1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix1.Id,false);      
        objDatingRecord1.Status__c = 'Draft';
        objDatingRecord1.FAD_Approval_Status__c = 'Approved';
        objDatingRecord1.Record_Status__c = 'Active';
        objDatingRecord1.Date__c =  System.today()+57;
        objDatingRecord1.Date_Type__c = 'EST Release Date';
        insert objDatingRecord1;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }

    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_National1() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','US','5096',null,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('S&D - Catalog','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','VOD Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'Catalog';
        objDatingMatrix.Media_Type__c = 'Digital';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotions_DatingMatrix__c objDatingMatrix1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix1.Line_of_Business_Type__c = 'Catalog';
        objDatingMatrix1.Media_Type__c = 'Digital';
        objDatingMatrix1.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix1;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Record_Status__c = 'Active';
        objDatingRecord.Date__c =  System.today()+57;
        objDatingRecord.Date_Type__c = 'VOD Release Date';
        insert objDatingRecord;
        HEP_Promotion_Dating__c objDatingRecord1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix1.Id,false);      
        objDatingRecord1.Status__c = 'Draft';
        objDatingRecord1.FAD_Approval_Status__c = 'Approved';
        objDatingRecord1.Record_Status__c = 'Active';
        objDatingRecord1.Date__c =  System.today()+57;
        objDatingRecord1.Date_Type__c = 'EST Release Date';
        insert objDatingRecord1;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }

    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_NationalPhysical() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK','Domestic','Subsidiary',null, null,'EUR','UK','5093',null,false);
        insert objTerritory;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Physical';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c =  System.today()+5;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }

    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_NationalDI() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK','Domestic','Subsidiary',null, null,'EUR','UK','5093',null,false);
        insert objTerritory;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Digital';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c =  System.today()+5;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }

    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_NationalDI1() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK','Domestic','Subsidiary',null, null,'EUR','UK','5093',null,false);
        insert objTerritory;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Physical';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c =  System.today()+57;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }

    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_NationalDI2() {
          
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK','Domestic','Subsidiary',null, null,'EUR','UK','5093',null,false);
        insert objTerritory;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'New Release';
        objDatingMatrix.Media_Type__c = 'Physical';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c =  System.today()+57;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        Test.stoptest();
    }


    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_Global() {
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
          
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','Domestic','Subsidiary',null, null,'EUR','AT','5094','JDE',false);
        insert objTerritory;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        objGlobalPromotion.FirstAvailableDate__c = System.today();
        insert objGlobalPromotion;    
        system.debug('global FirstAvailableDate__c' + objGlobalPromotion.FirstAvailableDate__c);
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objGlobalPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Digital';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.Date_Type__c = 'PVOD Release Date';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c = null;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        try{
        DataBase.executeBatch(objBatchAutoLocking);
        }catch(Exception e){

        }
        objDatingMatrix.Line_of_Business_Type__c = 'Catalog';
        Test.stoptest();
    }


    @isTest static void HEP_Auto_Lock_Promo_Dating_Batch_Globalnotnull() {
         
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout()); 
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_JDE_PromoSKU','HEP_INT_JDE_Integration',true,10,10,'Outbound-Push',true,true,true);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('IT','Domestic','Subsidiary',null, null,'EUR','IT','5095',null,false);
        insert objTerritory;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Notification_Template__c objTempLockedNotifications = HEP_Test_Data_Setup_Utility.createTemplate('Date_Lock_Alert','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotifications.Unique__c = 'Date_Lock_Alert';
        insert objTempLockedNotifications;
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objGlobalPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        objDatingMatrix.Line_of_Business_Type__c = 'TV';
        objDatingMatrix.Media_Type__c = 'Digital';
        objDatingMatrix.Auto_Lock_Cut_Off__c = 50; 
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objGlobalPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Draft';
        objDatingRecord.Date_Type__c = 'PVOD Release Date';
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        objDatingRecord.Date__c = System.today()+5;
        insert objDatingRecord;
        Test.startTest();
        HEP_Auto_Lock_Promotion_Dating_Batch objBatchAutoLocking = new HEP_Auto_Lock_Promotion_Dating_Batch();
        DataBase.executeBatch(objBatchAutoLocking);
        objDatingMatrix.Line_of_Business_Type__c = 'Catalog';
        Test.stoptest();
    }

}