/**
 * HEP_SKU_Promotion_Creation_Canada --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_SKU_Promotion_Creation_Canada{
    
     //Promotion record id is passed in this variable
    public Id promotionId {get; set;}
    public String sHref {get; set;}
    //This variable is used to render table on Email template
    public List<EmailWrapper> lstEmailWrapper{
            get{
                if(lstEmailWrapper == null){
                    fetchDetails();
                }
                return lstEmailWrapper;
            }
            set;
    }
     
    
    /**
    * Helps to get details of Created SKU Promotion records for Canada passed from VF component.
    * @return null
    */ 
    public void fetchDetails(){
        
        try{
            lstEmailWrapper = new List<EmailWrapper>();   
            EmailWrapper objMail; 
            //String sPromotionId;            
            //Helps to get the details of the Promotion SKU records where status is Draft.           
            if(promotionId != null){  
                List<HEP_Promotion_SKU__c> lstSKUPromotion = new List<HEP_Promotion_SKU__c> ();         
                //Query to get all SKU Promotion records for the Canada Specific promotion
                for(HEP_Promotion_SKU__c objPrSKU : [SELECT Id,                                                  
                        Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                        Promotion_Catalog__r.Promotion__c,
                        Promotion_LOB__c,
                        SKU_Master__r.SKU_Number__c,
                        SKU_Master__r.SKU_Title__c,
                        SKU_Master__r.Format__c,
                        SKU_Master__r.Channel__c,
                        LastModifiedDate,
                        CreatedDate,
                        Approval_Status__c,
                        (Select Id,Region__r.Name,Promotion_SKU__c from HEP_SKU_Region_Prices__r)
                        FROM HEP_Promotion_SKU__c
                        WHERE Promotion_Catalog__r.Promotion__c =: promotionId 
                        AND Approval_Status__c =: HEP_Utility.getConstantValue('HEP_CATALOG_STATUS_DRAFT')
                        AND (Promotion_LOB__c LIKE : ('%'+HEP_Utility.getConstantValue('PROMOTION_LOB_TV')+'%')
                        OR Promotion_LOB__c LIKE : ('%'+HEP_Utility.getConstantValue('PROMOTION_LOB_NEW_RELEASE')+'%'))
                        AND CreatedDate >: Datetime.now().addMinutes(-60) AND CreatedDate <: Datetime.now().addSeconds(20)
                        ]){
                            for (HEP_SKU_Price__c objSKUPrice : objPrSKU.HEP_SKU_Region_Prices__r){
                                if(objSKUPrice.Region__r.Name.equalsIgnoreCase('Canada')){
                                    lstSKUPromotion.add(objPrSKU);
                                    Break;
                                }                           
                            }                                                       
                        }
                
                if(lstSKUPromotion != null && !lstSKUPromotion.isEmpty()){
                    for(HEP_Promotion_SKU__c objPrSKU : lstSKUPromotion){                        
                            objMail = new EmailWrapper();
                            objMail.sRegion = HEP_Utility.getConstantValue('HEP_TERRITORY_CANADA');
                            objMail.sPrCode = objPrSKU.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c;
                            objMail.sSKUName = objPrSKU.SKU_Master__r.SKU_Title__c;
                            objMail.sSKUNumber = objPrSKU.SKU_Master__r.SKU_Number__c;
                            objMail.sFormat = objPrSKU.SKU_Master__r.Format__c;
                            objMail.sChannel = objPrSKU.SKU_Master__r.Channel__c;
                            sHref = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_Promotion_Details?promoId=' + promotionId +'&tab=HEP_Promotion_Products';
                            lstEmailWrapper.add(objMail);  
                           
                    }
                }
                
                
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

       
    /*
    **  EmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class EmailWrapper{
        
        public String sPrCode {get; set;}
        public String sSKUName {get; set;}
        public String sSKUNumber{get; set;}     
        public String sRegion {get; set;}      
        public String sChannel {get; set;}      
        public String sFormat {get; set;}      
        public String sHref {get;set;}    
    }

}