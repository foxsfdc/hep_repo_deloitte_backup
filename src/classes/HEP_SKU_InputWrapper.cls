/* HEP_SKU_InputWrapper-- Wrapper to store Input details
* @author    Nishit Kedia
*/

public class HEP_SKU_InputWrapper{
    
    //Json outer Members 
    public List<String> lstSkuObjectNumber;
    public String sTerritoryCode;
}