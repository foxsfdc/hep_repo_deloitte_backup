/**
 * HEP_SKUMasterTriggerHandler --- Handler class for HEP_SKUMasterTrigger
 * @author  Nidhin VK
 */
public class HEP_SKUMasterTriggerHandler extends TriggerHandler {
    
    
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * afterInsert --- all after update logic
    * @param
    * @exception Any exception
    * @return void
    */
    public override void afterInsert(){
        System.debug('newMap>>' + Trigger.newMap);
        callQueuableMethod((Map<Id, HEP_SKU_Master__c>) Trigger.newMap);
    }
    
    /**
    * afterUpdate --- all after update logic
    * @param 
    * @exception Any exception
    * @return void
    */
    public override void afterUpdate(){
        if(Trigger.oldMap != null){
        System.debug('oldMap>>' + Trigger.oldMap);
        System.debug('newMap>>' + Trigger.newMap);
        Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_SKU_Master__c', 'SKU_Master_Field_Set');
               System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
        if(checkFieldUpdate)
            callQueuableMethod((Map<Id, HEP_SKU_Master__c>) Trigger.newMap);
        }   
    }
    /**
    * beforeUpdate --- all before update logic
    * @param 
    * @exception Any exception
    * @return void
    */    
    public override void beforeUpdate(){
        //links sku master to catalog
        //check for validation of channel and format with the LOV Values
        updateSKUCatalogIds(Trigger.new);
    }
    /**
    * beforeInsert --- all before insert logic
    * @param 
    * @exception Any exception
    * @return void
    */      
    public override void beforeInsert(){
        //links sku master to catalog
        //check for validation of channel and format with the LOV Values
        updateSKUCatalogIds(Trigger.new);
    }
    
    /**
    * updateSKUCatalogIds --- Does the ChannelFormat Validation and Based on the INTL_Catalog_Number__c field, finds the HEP Catalog and stamps it in the Catalog_Master__c field on SKU Master object.
    * @param List<HEP_SKU_Master__c> lstSKUs
    * @exception Any exception
    * @return void
    * @author Himanshi Mayal
    */
    public static void updateSKUCatalogIds(List<HEP_SKU_Master__c> lstSKUs){
        System.debug('Enter Validation Check----- ');
        Set<String> setCatalogNumbers = new Set<String>();
        Set<Id> setTerritoryIds = new Set<Id>();        
        for(HEP_SKU_Master__c objSkuMaster : lstSKUs){
            if(objSkuMaster.INTL_ETL_SKU__c == true) {
                if(String.isNotBlank(objSkuMaster.INTL_Catalog_Number__c)
                    && String.isNotBlank(objSkuMaster.Territory__c)){

                    setCatalogNumbers.add(objSkuMaster.INTL_Catalog_Number__c);
                    setTerritoryIds.add(objSkuMaster.Territory__c);
                } 
                //Kunal - commenting this line as it is checking this checkbox for all inserts
                //else
                    //objSkuMaster.INTL_SKU_Validation_Error__c = true;  
            }
        }
        System.debug('setCatalogNumbers>>' + setCatalogNumbers);
        System.debug('setTerritoryIds>>' + setTerritoryIds);
        if(setCatalogNumbers.size() == 0 || setTerritoryIds.size() == 0)
            return;
        
        Map<id,HEP_Territory__c> mapTerritories = new Map<id,HEP_Territory__c>([SELECT 
                                                                                Id, Name 
                                                                            FROM 
                                                                                HEP_Territory__c
                                                                            WHERE 
                                                                                Id IN :setTerritoryIds]);
        
        System.debug('mapTerritories>>' + mapTerritories);
        Map<String,id> mapCatalogIds = new Map<String,id>();
        //Map the Catalog Number with the catalog ids        
        for(HEP_Catalog__c objCatalog : [SELECT 
                                            Id, Unique_Id__c
                                        FROM 
                                            HEP_Catalog__c 
                                        WHERE 
                                            CatalogId__c IN :setCatalogNumbers
                                        AND
                                            Territory__c IN :setTerritoryIds
                                        AND
                                            Unique_Id__c != NULL
                                        AND
                                        	Record_Status__c = :sACTIVE]){

            mapCatalogIds.put(objCatalog.Unique_Id__c, objCatalog.Id);
        }
        System.debug('mapCatalogIds>>' + mapCatalogIds);
        
        String sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
        String sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
        Set<String> setChannelLOV = new Set<String>();
        Set<String> setFormatLOV = new Set<String>();
        for(HEP_List_Of_Values__c objLOV : [SELECT 
                                                Type__c, Parent_Value__c 
                                            FROM 
                                                HEP_List_Of_Values__c 
                                            WHERE 
                                                Type__c IN (:sCHANNEL, :sFORMAT)
                                            AND
                                            	Record_Status__c = :sACTIVE]){
            if(objLOV.Type__c == sCHANNEL)
                setChannelLOV.add(objLOV.Parent_Value__c);
            else
                setFormatLOV.add(objLOV.Parent_Value__c);
        }
        System.debug('setChannelLOV>>' + setChannelLOV);
        System.debug('setFormatLOV>>' + setFormatLOV);
        
        for(HEP_SKU_Master__c objSKUMaster : lstSKUs){
            Boolean bValidateFlag = false;
            Boolean bUpdateFlag = false;
            
		    System.debug('objSKUMaster.Channel__c>>' + objSKUMaster.Channel__c);
		    System.debug('objSKUMaster.Format__c>>' + objSKUMaster.Format__c);
            if(setChannelLOV.contains(objSKUMaster.Channel__c) 
                && setFormatLOV.contains(objSKUMaster.Format__c)){
                bValidateFlag = true;
            }
            
            String sUniqueId = '';
            if(mapTerritories.containsKey(objSKUMaster.Territory__c))
                sUniqueId = objSKUMaster.INTL_Catalog_Number__c + ' / ' + mapTerritories.get(objSKUMaster.Territory__c).Name;
            
    		System.debug('sUniqueId>>' + sUniqueId);	
            if(mapCatalogIds.containsKey(sUniqueId)){
                objSKUMaster.Catalog_Master__c = mapCatalogIds.get(sUniqueId);     
                bUpdateFlag = true;
            }
        	System.debug('bValidateFlag>>'+bValidateFlag);
        	System.debug('bUpdateFlag>>'+bUpdateFlag);
            if(bValidateFlag && bUpdateFlag){
                objSkuMaster.INTL_ETL_SKU__c = false;
                objSkuMaster.Type__c = HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_MASTER');
                objSKUMaster.INTL_SKU_Validation_Error__c = false;
            }       
            else
                objSKUMaster.INTL_SKU_Validation_Error__c = true;                  
        }
        System.debug('lstSKUs>>'+lstSKUs);
    }
    
    /**
    * callQueuableMethod --- Calls the queuable method
    * @param Map<Id, HEP_SKU_Master__c> mapSKUs
    * @exception Any exception
    * @return void
    */
    public static void callQueuableMethod(Map<Id, HEP_SKU_Master__c> mapSKUs){
    	System.debug('Inside callQueuableMethod>>');
        List<Id> lstSKUIds = new List<Id>();
        for(HEP_SKU_Master__c objSKU : mapSKUs.values()){
        	if(objSKU.INTL_ETL_SKU__c == false)
        		lstSKUIds.add(objSKU.Id);
        }
		if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())
        	&& lstSKUIds.size() > 0){
            HEP_Siebel_Queueable objQueueable = new HEP_Siebel_Queueable(String.join(lstSKUIds, ','), 'HEP_Siebel_SkuMaster');
            System.enqueueJob(objQueueable);
        }
    }
    
}