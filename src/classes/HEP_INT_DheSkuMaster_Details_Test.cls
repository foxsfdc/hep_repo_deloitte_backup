@isTest
public class HEP_INT_DheSkuMaster_Details_Test {
     @testsetup
    static void HEP_CreateData() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        objTerritory.SKU_Interface__c = 'JDE';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_List_Of_Values__c> objHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        insert objDatingRecord;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        objCatalog.Catalog_Type__c = 'Bundle'; 
        insert objCatalog;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple iTunes',objTerritory.id,'1111','MDP Customers',NUll,False);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;  
        HEP_MDP_Product_Mapping__c objPromotionProductMapping = HEP_Test_Data_Setup_Utility.createPromotionProductMapping(objCatalog.Id,objTerritory.Id,objCustomer.id,objTitle.id,false);
        objPromotionProductMapping.Adam_Id__c = '756481';
        objPromotionProductMapping.Local_Description__c = '756481';
        insert objPromotionProductMapping;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;    
        HEP_SKU_Template__c objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU TEMP',objTerritory.id,objLOB.id,true);
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, objSKUTemp.id, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objTerritory.Id, '1', 'KINGSMAN BD', 'Request', 'Physical', 'FOX', 'HD', true);
        HEP_Boxset_Catalog_SKU__c objBoxetCtalogSku = HEP_Test_Data_Setup_Utility.createBoxetCatalogSku(objCatalog.Id,objSKU.Id,objSKU.Id,'Active',true);  
        HEP_Promotion_SKU__c objPromotionSku = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat.id, objSKU.id, 'Pending', 'Locked',true);
        HEP_Approvals__c  objHEPApprvals = HEP_Test_Data_Setup_Utility.createApproval('Pending', null, objPromotionSku.id , true);
        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objHEPApprvals.id, u.id, objRole.id, 'Pending',true);
       
    }  

    @isTest 
    static void Success_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180413","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuMaster_Details demo = new HEP_INT_DheSkuMaster_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    } 
    
    @isTest 
    static void Failure_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180412","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuMaster_Details demo = new HEP_INT_DheSkuMaster_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }  
    
    @isTest 
    static void Access_Token_Failure_Test(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_JDE_E1_OAUTH']; 
        delete objConstant;      
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180412","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuMaster_Details demo = new HEP_INT_DheSkuMaster_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }

    @isTest 
    static void updateMapping_Test(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_JDE_E1_OAUTH']; 
        delete objConstant;  
        HEP_Promotion_SKU__c  objPromotionSku =[ SELECT id,Name, SKU_Master__c from HEP_Promotion_SKU__c where Approval_Status__c = 'Pending']; 
        System.debug('objPromotionSku- '+objPromotionSku); 
          
        HEP_SKU_Master__c  objSKU =[ SELECT id,Name, SKU_Number__c from HEP_SKU_Master__c where SKU_Number__c = '1']; 
        objSKU.SKU_Number__c=objPromotionSku.id;
        update objSKU; 

        System.debug('objSKUobjSKU***- '+objSKU); 
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180412","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuMaster_Details demo = new HEP_INT_DheSkuMaster_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        String sDetails = '{"dhe_sku_master":[{"MPAA_Rating":"NR","Number_of_Discs":"2","Price_Rule":"C7C","Wholesale_Price_US":"0","User_ID":"DAVIDR","Channel":"Retail","Licensor":"FOX","ActionCode":"C","Feature_Run":"","Territory":"US","BW/Color":"CLR","Quebec_Rating":"","Additional_Media":"","HEP_Promotion_SKU_Id":"'+objPromotionSku.Name+'","UV_Code":"N","UPC_Code":"02454352826551","Customer_Exclusive":"Y","Digital_Copy_Flag":"","Current_Release_Date":"05/20/08","Packaging":"AMRY DBL","Catalog#":"33060","Local_PromoCode":"1J583J","Pack_Type":"SET - Multi/Giftset/Value Pack","Primary_Language":"ENGLISH","Aspect_Ratio":"W/S","Subtitle_Language":"","Request_Id":"'+objPromotionSku.id+'","Total_Run":"","BD_Enhancement":"","Canadian_Rating":"","Gift_With_Purchase_Flag":"","BD_Plus":"","Item_Number":"2252826","Prelim_flag":"","SMF_Run":"","Item_Description":"BIG MOMMA SE+2 DF WM","e-Copy_Flag":"N","Date_Updated":"10/26/12","Dubbed_Language":"","Enhanced_Packaging":"","Time_Updated":"101316","Item_Status":"Retired","Media_Type":"DVD","3D_Flag":"N","Wholesale_Price_CAN":"0"}]}';
      
        HEP_INT_DheSkuMasterWrapper objWrap = (HEP_INT_DheSkuMasterWrapper)JSON.deserialize(sDetails,HEP_INT_DheSkuMasterWrapper.class);
        HEP_INT_DheSkuMaster_Details.updateMapping(objWrap);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }     

     @isTest 
    static void updateMapping_Test2(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_JDE_E1_OAUTH']; 
        delete objConstant;  
        HEP_Promotion_SKU__c  objPromotionSku =[ SELECT id,Name, SKU_Master__c from HEP_Promotion_SKU__c where Approval_Status__c = 'Pending']; 
        System.debug('objPromotionSku- '+objPromotionSku); 
        
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180412","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuMaster_Details demo = new HEP_INT_DheSkuMaster_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        String sDetails = '{"dhe_sku_master":[{"MPAA_Rating":"NR","Number_of_Discs":"2","Price_Rule":"C7C","Wholesale_Price_US":"0","User_ID":"DAVIDR","Channel":"Retail","Licensor":"FOX","ActionCode":"C","Feature_Run":"","Territory":"US","BW/Color":"CLR","Quebec_Rating":"","Additional_Media":"","HEP_Promotion_SKU_Id":"'+objPromotionSku.Name+'","UV_Code":"N","UPC_Code":"02454352826551","Customer_Exclusive":"Y","Digital_Copy_Flag":"","Current_Release_Date":"05/20/08","Packaging":"AMRY DBL","Catalog#":"33060","Local_PromoCode":"1J583J","Pack_Type":"SET - Multi/Giftset/Value Pack","Primary_Language":"ENGLISH","Aspect_Ratio":"W/S","Subtitle_Language":"","Request_Id":"'+objPromotionSku.id+'","Total_Run":"","BD_Enhancement":"","Canadian_Rating":"","Gift_With_Purchase_Flag":"","BD_Plus":"","Item_Number":"2252826","Prelim_flag":"","SMF_Run":"","Item_Description":"BIG MOMMA SE+2 DF WM","e-Copy_Flag":"N","Date_Updated":"10/26/12","Dubbed_Language":"","Enhanced_Packaging":"","Time_Updated":"101316","Item_Status":"Retired","Media_Type":"DVD","3D_Flag":"N","Wholesale_Price_CAN":"0"}]}';
        HEP_INT_DheSkuMasterWrapper objWrap = (HEP_INT_DheSkuMasterWrapper)JSON.deserialize(sDetails,HEP_INT_DheSkuMasterWrapper.class);
        HEP_INT_DheSkuMaster_Details.updateMapping(objWrap);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }               
}