@isTest
private class HEP_Foxipedia_GenreWrapper_Test {
    
    @isTest static void HEP_Foxipedia_GenreWrapper_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        HEP_Foxipedia_GenreWrapper objGenreWrapper = new HEP_Foxipedia_GenreWrapper();
        objGenreWrapper.TxnId=null;
        objGenreWrapper.calloutStatus=null; 
        HEP_Foxipedia_GenreWrapper.Attributes objAttributes = new HEP_Foxipedia_GenreWrapper.Attributes();
        objAttributes.type = 'TITLE';
        objAttributes.foxId = '363029';
        objAttributes.categoryId = '363029';
        objAttributes.categoryTypeCode = '690061';
        objAttributes.categoryTypeDescription = '67728';
        objAttributes.categoryCode = '363029';
        objAttributes.categoryDescription = 'New Zealand';
        objAttributes.categorySortOrder = 'NZ';
        objAttributes.primaryTitleIndicator = 'Home Entertainment';
        objAttributes.foxVersionId = 'VID';
        objAttributes.countryDescription = 'OFLC - Office of Film and Literature Classification';
        objAttributes.countryCode = 'OFLC';
        objAttributes.mediaDescription = 'R16';
        objAttributes.mediaCode = 'OFLC-R16';
        objAttributes.titleVersionTypeCode = null;
        objAttributes.titleVersionTypeDescription = 'UNR';
        objAttributes.titleVersionDescription = 'Unrated';
        objAttributes.jdeVersionId = 'INTERNATIONAL/UNRATED/EXTENDED VERSION';
        objAttributes.rowIdTitleVersion = 'Restricted to persons 16 Years and over';
        objAttributes.rowIdObject = 'OFLC';
        list<HEP_Foxipedia_GenreWrapper.Data> lstData = new list<HEP_Foxipedia_GenreWrapper.Data>();
        HEP_Foxipedia_GenreWrapper.Data objData = new HEP_Foxipedia_GenreWrapper.Data();
        objData.attributes = objAttributes;
        objData.type = 'RATING';
        objGenreWrapper.errors=null;
        HEP_Foxipedia_GenreWrapper.Links objLinks = new HEP_Foxipedia_GenreWrapper.Links();
        objLinks.related = '/foxipedia/global/api/title/details?foxId=67728';
        HEP_Foxipedia_GenreWrapper.Title objTtle = new HEP_Foxipedia_GenreWrapper.Title();
        objTtle.links = objLinks;
        HEP_Foxipedia_GenreWrapper.Errors objErrors = new HEP_Foxipedia_GenreWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_Foxipedia_GenreWrapper.Relationships objRelationships = new HEP_Foxipedia_GenreWrapper.Relationships();
        objRelationships.title = null;       
        Test.stoptest();
    }
}