/*------------------------------------Fox HEP----------------------------------
This class handles the business logic for inbound REST from Foxipedia for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
public class HEP_ReleaseKeys_Foxipedia_Inbound implements HEP_IntegrationInterface {
    public HEP_ReleaseKeys_Foxipedia_Inbound() {

    }
    
    /**
    * @param objWrap - Integration Transaction Response Object
    * @return void 
    */
    public void performTransaction(HEP_InterfaceTxnResponse objWrap) {
        String modifiedSince = objWrap.sRequest; 

        Datetime lastModifiedDate; 
        boolean isUnix = modifiedSince.isNumeric();

        if (isUnix) { // Convert Unix datetime 
            lastModifiedDate = Datetime.newInstance(Long.valueOf(modifiedSince));
        }
        else { // Convert ISO datetime 
            lastModifiedDate = (DateTime) JSON.deserialize('"' + modifiedSince + '"', DateTime.class);
        }

        List<ReleaseKeys> prkList = new List<ReleaseKeys>();
        List<HEP_Promotion_Dating__c> pdList = [SELECT  LastModifiedDate, 
                                                        HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, 
                                                        Territory__r.Territory_Code__c, 
                                                        Territory__r.Send_to_Prophet__c,
                                                        Territory__r.Territory_Code_Integration__c  

                                                FROM    HEP_Promotion_Dating__c 
                                                WHERE   LastModifiedDate >= :lastModifiedDate AND 
                                                        HEP_Catalog__c != NULL AND 
                                                        Date__c != NULL AND 
                                                        DateType__c LIKE '%Release Date%' AND 
                                                        Customer__c = NULL AND 
                                                        Territory__r.Send_to_Prophet__c = true
                                                        //Status__c = 'Confirmed' // There are no 'Confirmed' records atm
                                                        ]; 
        
        // Get set of unique FIN_PROD_ID__c
        Set<String> uniqueProdIDs = new Set<String>(); 
        for (HEP_Promotion_Dating__c hpd : pdList) {
            uniqueProdIDs.add(hpd.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c); 
        }

        // Save list of Prophet Release Keys
        for (String finProdID : uniqueProdIDs) {
            if (finProdID != null) {
                List<String> terrList = new List<String>(); 
                for (HEP_Promotion_Dating__c hpd : pdList) {
                    if (hpd.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c == finProdID && hpd.Territory__r.Territory_Code_Integration__c != null) {
                        if (String.valueOf(hpd.Territory__r.Territory_Code_Integration__c) == 'DHE') {
                            terrList.add('US'); terrList.add('CA');
                        }
                        else {
                            terrList.add(hpd.Territory__r.Territory_Code_Integration__c);
                        }
                    } 
                }

                Set<String> tempTerrSet = new Set<String>(terrList); 
                List<String> returnedTerrList = new List<String>(tempTerrSet); 
                ReleaseKeys rk = new ReleaseKeys(finProdID, returnedTerrList); 
                prkList.add(rk); 
            }
        }

        RestContext.response.addHeader('Content-Type', 'application/json');

        if (lastModifiedDate.addDays(30) >= DateTime.now()) {
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(prkList));
            objWrap.sResponse = JSON.serialize(prkList); 
        } 
        else {
            String res = HEP_Constants__c.getValues('HEP_RK_INBOUND_30DAY_ERROR').Value__c; 
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
            RestContext.response.statuscode = 404; 
            objWrap.sResponse = res; 
        }
    }

    public class ReleaseKeys {
        public String finTitleId; 
        public String territoryId; 

        public ReleaseKeys(String financialTitleID, List<String> territoryList) {
            this.finTitleId = financialTitleID; 
            this.territoryId = String.join(territoryList, ',');
        }
    }
}