/**
*HEP_PO_DetailsPage_Controller - To Process SKU Master Details to be shown on SKU_Details_SubTab Page
*@author : Ashutosh Arora
*/
public class HEP_SKU_Details_Controller {
    public static string sACTIVE;
    public static string sHEP_TERRITORY_DHE;
    public static string sHEP_TERRITORY_US;
    public static string sHEP_TERRITORY_CANADA;
    public static string sHEP_SKU_DETAILS_SUBTAB_PAGE;
    public static string sHEP_RECORD_STATUS_INACTIVE;
    public static string sCOMPILATION;
    public static string sPERMANENT_RANGE;
    public static string sCONFIGURATION;
    public static string sSKU_CUSTOMER;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'); 
        sHEP_TERRITORY_DHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
        sHEP_TERRITORY_US = HEP_Utility.getConstantValue('HEP_TERRITORY_US');
        sHEP_TERRITORY_CANADA = HEP_Utility.getConstantValue('HEP_TERRITORY_CANADA');
        sHEP_SKU_DETAILS_SUBTAB_PAGE = HEP_Utility.getConstantValue('HEP_SKU_DETAILS_SUBTAB_PAGE');
        sHEP_RECORD_STATUS_INACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE');
        sCOMPILATION = HEP_Utility.getConstantValue('HEP_SKU_COMPILATION_TYPE');
        sPERMANENT_RANGE = HEP_Utility.getConstantValue('HEP_SKU_PERMANENT_RANGE');
        sCONFIGURATION = HEP_Utility.getConstantValue('HEP_SKU_CONFIGURATION_TYPE');
        sSKU_CUSTOMER = HEP_Utility.getConstantValue('HEP_SKU_CUSTOMER');
    }
    /**
     *fetchData - To fetch the SKU Master Details to be shown on the page
     *@return SKUWrapper (SKUWrapper Wrapper with SKU Master Data)
     *@author Ashutosh Arora
     */
    @RemoteAction
    public static String fetchData(String skuMasterId, String sLocaleDateFormat) {
        System.debug('FETCH CALLED');
        System.debug('sACTIVE : ' +sACTIVE);
        //Fetch SKU Record based on Id received in url Parameters    
        List < HEP_SKU_Master__c > lstSKUData = [SELECT Id, Name, SKU_Number__c, Channel__c, Format__c, Barcode__c,
            INTL_Stock_in_Warehouse__c, Rental_Ready__c, SKU_Compilation__c,Record_Status__c,
            SKU_Permanent_Range__c, Comments__c, Current_Release_Date__c, Territory__r.Name, SKU_Configuration__c, Catalog_Master__r.UI_Catalog_Number__c, Catalog_Master__r.Catalog_Name__c, Catalog_Master__r.Licensor_Type__c,JDE_Item_Status__c,
            (SELECT Customer__r.Id, Customer__r.CustomerName__c FROM SKU_Customers__r WHERE SKU_Master__r.id =: skuMasterId)
            FROM HEP_SKU_Master__c 
            WHERE ID =: skuMasterId
            AND Record_Status__c =:sACTIVE
        ];
        System.debug('lstSKUData : ' + lstSKUData);
        //Data to be displayed on UI
        SKUWrapper objSKUData = new SKUWrapper();
        Date dtSKUReleaseDate;
        if (lstSKUData != null && !lstSKUData.isEmpty()) {
            if (lstSKUData[0].Id != null) objSKUData.sSKUId = lstSKUData[0].Id;
            if (lstSKUData[0].Name != null) objSKUData.sSKUName = lstSKUData[0].Name;
            if (lstSKUData[0].SKU_Number__c != null) objSKUData.sSKUNumber = lstSKUData[0].SKU_Number__c;
            if (lstSKUData[0].Channel__c != null) objSKUData.sChannel = lstSKUData[0].Channel__c;
            if (lstSKUData[0].Format__c != null) objSKUData.sFormat = lstSKUData[0].Format__c;
            //objSKUData.sBarcode = lstSKUData[0].Barcode__c;
            if (lstSKUData[0].INTL_Stock_in_Warehouse__c != null) objSKUData.sWareshouseStock = String.valueOf(Integer.valueOf(lstSKUData[0].INTL_Stock_in_Warehouse__c));
           //if(lstSKUData[0].JDE_Item_Status__c != null) objSKUData.sSKUStatus = lstSKUData[0].JDE_Item_Status__c;
            if(lstSKUData[0].Record_Status__c!= null) objSKUData.sSKUStatus = lstSKUData[0].Record_Status__c;
            if (lstSKUData[0].Catalog_Master__r.UI_Catalog_Number__c != null) objSKUData.sCatalogNumber = lstSKUData[0].Catalog_Master__r.UI_Catalog_Number__c;
            if (lstSKUData[0].Catalog_Master__r.Catalog_Name__c != null) objSKUData.sCatalogName = lstSKUData[0].Catalog_Master__r.Catalog_Name__c;
            if (lstSKUData[0].Catalog_Master__r.Licensor_Type__c != null) objSKUData.sLicensorType = lstSKUData[0].Catalog_Master__r.Licensor_Type__c;
            if (lstSKUData[0].Territory__r.Name != null) objSKUData.sRegion = lstSKUData[0].Territory__r.Name;  
            if (lstSKUData[0].Territory__c != null) objSKUData.sRegionId = lstSKUData[0].Territory__c;
            //Set boolean variable true for DHE, variable used to control display/edit for certain fields on UI 
            if(objSKUData.sRegion != null){
                if (objSKUData.sRegion.equalsIgnoreCase(sHEP_TERRITORY_DHE) || objSKUData.sRegion.equalsIgnoreCase(sHEP_TERRITORY_US) ||
                objSKUData.sRegion.equalsIgnoreCase(sHEP_TERRITORY_CANADA)) {
                    objSKUData.bIsDomestic = true;
                } else {
                    objSKUData.bIsDomestic = false;
                }
            }
            if (lstSKUData[0].Rental_Ready__c != null) objSKUData.sRentalReady = lstSKUData[0].Rental_Ready__c;
            if (lstSKUData[0].SKU_Compilation__c != null) objSKUData.sSKUCompilation = lstSKUData[0].SKU_Compilation__c;
            if (lstSKUData[0].SKU_Permanent_Range__c != null) objSKUData.sSKUPermanentRange = lstSKUData[0].SKU_Permanent_Range__c;
            if (lstSKUData[0].SKU_Configuration__c != null) objSKUData.sSKUConfiguration = lstSKUData[0].SKU_Configuration__c;
            if (lstSKUData[0].Comments__c != null) objSKUData.sComments = lstSKUData[0].Comments__c;
            if (lstSKUData[0].Current_Release_Date__c != null) dtSKUReleaseDate = lstSKUData[0].Current_Release_Date__c;
        }
        if (dtSKUReleaseDate != null) {
            DateTime dtSKUReleaseDateTime = DateTime.newInstance(dtSKUReleaseDate.year(), dtSKUReleaseDate.month(), dtSKUReleaseDate.day());
            String sFormattedDate = dtSKUReleaseDateTime.format(sLocaleDateFormat);
            objSKUData.sReleaseDate = sFormattedDate;
        }
        String sSearchText = objSKUData.sRegion;
        objSKUData.lstSKUCompilationPickVals.add(new HEP_Utility.PicklistWrapper(NULL, 'None'));
        objSKUData.lstPermanentRangePickVals.add(new HEP_Utility.PicklistWrapper(NULL, 'None'));
        objSKUData.lstSKUCongigurationPickVals.add(new HEP_Utility.PicklistWrapper(NULL, 'None'));
        List<String> lstTypes = new List<String>{sCOMPILATION,sPERMANENT_RANGE,sCONFIGURATION};
        //Get picklist values for SKU Cimpilation, Permanent Range
        for(HEP_List_Of_Values__c objLOV : [SELECT Id, Name, Parent_Value__c, Type__c, Values__c
                                                FROM HEP_List_Of_Values__c
                                                WHERE Parent_Value__c LIKE :sSearchText
                                                AND Type__c IN :lstTypes
                                                AND Record_Status__c =:sACTIVE
                                                ORDER BY Values__c ASC]){
        
        if(objLOV.Type__c.equals(sCOMPILATION)){
            objSKUData.lstSKUCompilationPickVals.add(new HEP_Utility.PicklistWrapper(objLOV.Name, objLOV.Values__c));
        } else if(objLOV.Type__c.equals(sPERMANENT_RANGE)){
            objSKUData.lstPermanentRangePickVals.add(new HEP_Utility.PicklistWrapper(objLOV.Name, objLOV.Values__c));
        } else if(objLOV.Type__c.equals(sCONFIGURATION)){
            objSKUData.lstSKUCongigurationPickVals.add(new HEP_Utility.PicklistWrapper(objLOV.Name, objLOV.Values__c));
        }
        }   
        //Code to check the button access       
        objSKUData.mapCustomPermissions = HEP_Utility.fetchCustomPermissions(objSKUData.sRegionId, sHEP_SKU_DETAILS_SUBTAB_PAGE);
        //Get All SKU Customer Records for the SKU which are Active
        objSKUData.lstSKUCustomers = [SELECT Id,Name, Customer__c, Customer__r.CustomerName__c, Legacy_Id__c,
            Record_Status__c, SKU_Master__c, Unique_Id__c
            FROM HEP_SKU_Customer__c
            WHERE SKU_Master__r.Id =: skuMasterId
            AND Record_Status__c =: sACTIVE
        ];
        //Get All HEP Customer Records with Status as Active                                
        objSKUData.lstHEPCustomers = [SELECT Id, CustomerName__c, Record_Status__c, Territory__r.Name, Unique_Id__c
            FROM HEP_Customer__c
            WHERE Record_Status__c =: sACTIVE
            AND Customer_Type__c =: sSKU_CUSTOMER
            AND Territory__r.Name =: sSearchText
            ORDER BY CustomerName__c ASC
        ];
        Map < String, HEP_SKU_Customer__c > mapCustomerIDToSKUCustomer = new Map < String, HEP_SKU_Customer__c > ();
        for (HEP_SKU_Customer__c objSKUCustomer: objSKUData.lstSKUCustomers) {
            if(!mapCustomerIDToSKUCustomer.containsKey(objSKUCustomer.Customer__r.Id)){
            mapCustomerIDToSKUCustomer.put(objSKUCustomer.Customer__r.Id, objSKUCustomer); // HEP Customer Id,  HEP SKU Customer
            }
        }

        // Iterate over List of All HEP Customer Records
        for (HEP_Customer__c objHEPCustomer: objSKUData.lstHEPCustomers) {
            //Check if the HEP Customer Record exists on SKU Customer for the given SKU Record, if yes Mark it checked for SKU
            if (mapCustomerIDToSKUCustomer.containsKey(objHEPCustomer.Id)) {
                HEP_Utility.MultiPicklistWrapper objMultiPicklist = new HEP_Utility.MultiPicklistWrapper();
                objMultiPicklist.sValue = objHEPCustomer.Id;
                objMultiPicklist.sKey = objHEPCustomer.CustomerName__c;
                objMultiPicklist.bSelected = true;
                objSKUData.lstCustomersSelected.add(objMultiPicklist);
            }
            //IF HEP Customer Record doesn't exist for SKU Customer for given SKU, mark it as unchecked
            else {
                HEP_Utility.MultiPicklistWrapper objMultiPicklist = new HEP_Utility.MultiPicklistWrapper();
                objMultiPicklist.sValue = objHEPCustomer.Id;
                objMultiPicklist.sKey = objHEPCustomer.CustomerName__c;
                objMultiPicklist.bSelected = false;
                objSKUData.lstCustomersSelected.add(objMultiPicklist);
            }
        }
        //Return Data to be displayed on Screen
        return JSON.serialize(objSKUData);
    }

    /**
     *saveData - To save the SKU Master data updated from the UI
     *@return nothing
     *@author Ashutosh Arora
     */
    @RemoteAction
    public static void saveData(String sSKUData) {
        SKUWrapper objSKUData = (SKUWrapper)JSON.deserialize(sSKUData,SKUWrapper.class);
        System.debug('Data Received from UI : ' + objSKUData);
        System.debug('objSKUData.sSKUCompilation : ' + objSKUData.sSKUCompilation);
        System.debug('objSKUData.sSKUPermanentRange : ' + objSKUData.sSKUPermanentRange);
        List < HEP_SKU_Master__c > lstSKURecord = new List < HEP_SKU_Master__c > ();
        List < HEP_Customer__c > lstCustomer = new List < HEP_Customer__c > ();
        HEP_SKU_Master__c objSKURecord = new HEP_SKU_Master__c();
        objSKURecord.SKU_Compilation__c = objSKUData.sSKUCompilation;
        objSKURecord.SKU_Permanent_Range__c = objSKUData.sSKUPermanentRange;
        objSKURecord.Id = objSKUData.sSKUId;
        objSKURecord.SKU_Configuration__c = objSKUData.sSKUConfiguration;
        lstSKURecord.add(objSKURecord);
        //Map to store response from options selected on Customer Multi Select Picklist
        Map < String, Boolean > mapCustomertoSelected = new Map < String, Boolean > ();
        for (HEP_Utility.MultiPicklistWrapper objSelected: objSKUData.lstCustomersSelected) {
            if(!mapCustomertoSelected.containsKey(objSelected.sValue)){
                mapCustomertoSelected.put(objSelected.sValue, objSelected.bSelected);
            }
        }
        //List of SKU Customer records filtered on SKU Master and HEP Customer Ids retrieved from Customer Multi Select Picklist Options
        List < HEP_SKU_Customer__c > lstSKUCustomer = [SELECT Id, Customer__c, Customer__r.CustomerName__c, Legacy_Id__c, Record_Status__c, SKU_Master__c, Unique_Id__c
            FROM HEP_SKU_Customer__c
            WHERE SKU_Master__r.Id =: objSKUData.sSKUId
            AND Customer__c IN: mapCustomertoSelected.keyset()
        ];
        // Map of HEP Customer Id, SKU Customer Id
        Map < String, Id > mapSKUIdHEPCustomerId = new Map < String, Id > ();
        for (HEP_SKU_Customer__c objSKUCustomer: lstSKUCustomer) {
            if(!mapSKUIdHEPCustomerId.containskey(objSKUCustomer.Customer__c)){
                mapSKUIdHEPCustomerId.put(objSKUCustomer.Customer__c, objSKUCustomer.Id);
            }
        }
        //List to Update SKU Customer Records
        List < HEP_SKU_Customer__c > lstOfSkuCustomers = new List < HEP_SKU_Customer__c > ();
        for (String sCustomerId: mapCustomertoSelected.keyset()) {
            HEP_SKU_Customer__c objCustomerSKU = new HEP_SKU_Customer__c();
            if (mapSKUIdHEPCustomerId.containsKey(sCustomerId)){
                objCustomerSKU.Id = mapSKUIdHEPCustomerId.get(sCustomerId);
                objCustomerSKU.SKU_Master__c = objSKUData.sSKUId;
                objCustomerSKU.Customer__c = sCustomerId;
                objCustomerSKU.Record_Status__c = mapCustomertoSelected.get(sCustomerId) ? sACTIVE : sHEP_RECORD_STATUS_INACTIVE;
                lstOfSkuCustomers.add(objCustomerSKU);
            }
            else if(mapCustomertoSelected.get(sCustomerId)){
                objCustomerSKU.SKU_Master__c = objSKUData.sSKUId;
                objCustomerSKU.Customer__c = sCustomerId;
                objCustomerSKU.Record_Status__c = mapCustomertoSelected.get(sCustomerId) ? sACTIVE : sHEP_RECORD_STATUS_INACTIVE;
                lstOfSkuCustomers.add(objCustomerSKU);
            }
            
        }
        System.debug('--------->' + lstOfSkuCustomers);
        for(HEP_SKU_Customer__c objSKU : lstOfSkuCustomers){
            System.debug('SKU Record : ' +objSKU);
        }
        //Check if List of SKU Customers to update or insert is not empty
        if (!lstOfSkuCustomers.isEmpty()) {
            upsert lstOfSkuCustomers; 
        }
        //Check if List of SKU record to update is not empty
        if (!lstSKURecord.isEmpty()) {
            update lstSKURecord;
        }

    }

    /**
     *SKUWrapper - Wrapper storing SKU Master and Related object values
     *@author : Ashutosh Arora
     */
    public class SKUWrapper {
        public string sSKUId;
        public string sSKUName;
        public string sSKUNumber;
        public string sChannel;
        public string sFormat;
        public string sBarcode;
        public string sWareshouseStock;
        public string sCatalogNumber;
        public string sCatalogName;
        public string sLicensorType;
        public string sRegion;
        public string sSKUStatus;
        public string sRegionId;
        public string sCustomerId;
        public string sCustomerName;
        public string sSKUConfiguration;
        public string sRentalReady;
        public string sSKUCompilation;
        public string sSKUPermanentRange;
        public string sComments;
        public String sReleaseDate;
        public Boolean bIsDomestic;
        public String sNumberOfDiscs;
        public List < String > lstCatalogNum;
        public List<HEP_Utility.PicklistWrapper> lstPermanentRangePickVals;
        public List<HEP_Utility.PicklistWrapper> lstSKUCompilationPickVals;
        public List<HEP_Utility.PicklistWrapper> lstSKUCongigurationPickVals;
        public List < HEP_Utility.MultiPicklistWrapper > lstCustomersSelected; //For Customer Multi-select Picklst on UI
        public List < HEP_Customer__c > lstHEPCustomers;
        public List < HEP_SKU_Customer__c > lstSKUCustomers;
        public Map < String, String > mapCustomPermissions;
        /**
         *SKUWrapper - Constructor to initialize values 
         *@author : Ashutosh Arora
         */
        public SKUWrapper() {
            this.lstSKUCompilationPickVals = new List <HEP_Utility.PicklistWrapper> ();
            this.lstPermanentRangePickVals = new List <HEP_Utility.PicklistWrapper> ();
            this.lstSKUCongigurationPickVals = new List <HEP_Utility.PicklistWrapper> ();
            this.lstSKUCustomers = new List < HEP_SKU_Customer__c > ();
            this.bIsDomestic = false;
            this.lstHEPCustomers = new List < HEP_Customer__c > ();
            this.lstCustomersSelected = new List < HEP_Utility.MultiPicklistWrapper > ();
        }
    }
}