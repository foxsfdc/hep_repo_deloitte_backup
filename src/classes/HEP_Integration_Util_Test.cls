/**
* HEP_RatingDetails_Test -- Test class for the HEP_RatingDetails for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Integration_Util_Test{
    /**
    * getAuthenticationToken_Test --  Test method for the Authentication Token.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void getAuthenticationToken_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH'));
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }

    /**
    * getAuthenticationTokenFailure_Test --  Test method for the Authentication Failure Token.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void getAuthenticationTokenFailure_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH'));
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }


    
    /**
    * getAuthenticationTokenFailure_Test --  Test method for the Authentication Token Status not equal to OK.
    * @return  :  status Not Ok
    * @author  :  Anshul Jain
    */
    @isTest static void testGetAuthenticationToken1() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name = 'Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Sandbox_URL__c = '/oauth/oauth20/tokenNotOk';
        objService.Service_URL__c = '/oauth/oauth20/tokenNotOk';
        objService.Client_Id__c = 'TestClientID';
        objService.Client_Secret__c = 'TestSecret';
        objService.Service_Account_ID__c = 'client_credentials';
        objService.Grant_Type__c = 'client_credentials';
        insert objService;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        String sStatus = HEP_Integration_Util.getAuthenticationToken('Foxipedia_OAuth1');
        System.assertEquals(null,sStatus);
        
        sStatus = HEP_Integration_Util.getAuthenticationToken('Foxipedia_OAuth');
        System.assertEquals('Error',sStatus);
        Test.stoptest();
    }
    


    /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri,Anshul Jain
    */
    @isTest static void updateInterfaceTransactionError_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objInterfaceTransactionError = new HEP_InterfaceTxnResponse();
        
        objInterfaceTransactionError.sStatus = 'Failure';
        objInterfaceTransactionError.sErrorMsg = 'Invalid Response';
        objInterfaceTransactionError.sResponse = '{"access_token": "GMSMSAvsNfSSUDFJdeZq7AXwl4jbu7Gd","token_type": "Bearer","expires_in": "43199"}';
        objInterfaceTransactionError.sRequest = 'https://ms-devapi.foxinc.com/oauth/oauth20/tokenNotOk';
        HEP_Interface_Transaction__c objTransaction =  new HEP_Interface_Transaction__c();
        insert objTransaction;
        objInterfaceTransactionError.sInterfaceTxnId = objTransaction.Id;
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content Ojbect';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         
        
        
        HEP_Interface_Transaction_Error__c objTxnError = new HEP_Interface_Transaction_Error__c();
        objTxnError.Error_Datetime__c =DateTime.newInstance(1996, 09, 23, 12, 56, 56);
        objTxnError.Error_Description__c = 'Test Error';
        objTxnError.HEP_Interface_Transaction__c= objTransaction.Id;
        objTxnError.Status__c = 'Failure';
        insert objTxnError;
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];


        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=objTransaction.id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=testcontent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.updateInterfaceTransactionError(objInterfaceTransactionError);
        HEP_Interface_Transaction_Error objErrorInterface = new HEP_Interface_Transaction_Error();
        objErrorInterface.iTERecId =objTxnError.Id;
        objErrorInterface.getHEP_Interface_Transaction_Error();
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }



    /**
    * createInterfaceTxn_Test --  Test method for the createInterfaceTxn.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void createInterfaceTxnSourceFalse_Test(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_InterfaceTxnResponse objInterfaceTransactionError = new HEP_InterfaceTxnResponse();
        //objInterfaceTransactionError.sStatus = 'Success';
        
        HEP_Interface_Transaction__c objTransaction =  new HEP_Interface_Transaction__c();
        
        insert objTransaction;
        HEP_InterfaceTxnResponse objIntTxnResponse = new HEP_InterfaceTxnResponse();
        objIntTxnResponse.sInterfaceTxnId = objTransaction.Id;
        objIntTxnResponse.bIsSourceAsAttachment = false;
        objIntTxnResponse.iMaxRetriesAllowed = 1;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createInterfaceTxn(objIntTxnResponse);
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    
    
    @isTest 
    static void testsaveFileDatainContent1() {
        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        insert objTerritory;
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        objHEPLineOfBusiness.Record_Status__c = 'Active';
        insert objHEPLineOfBusiness;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion', 'Global', '', objTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objPromotion.LocalPromotionCode__c = '123456789';
        insert objPromotion;
        Map<String,String> mapData = new Map<String,String>();
        mapData.put('TestFile','TEstData');
        Test.startTest();
        HEP_Integration_Util.saveFileDatainContent(objPromotion.Id,mapData,'txt');
        HEP_Integration_Util.loadCachedIntegrationData(objPromotion.Id,'TestFile','DATING_INTEG_REFRESH_INTERVAL');
        Test.stoptest();
    }
 
    
    /**
    * createInterfaceTxn_Test --  Test method for the createInterfaceTxn with SourceId not blank.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void testCreateInterfaceTxn(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_InterfaceTxnResponse objInterfaceTransactionError = new HEP_InterfaceTxnResponse();
        //objInterfaceTransactionError.sStatus = 'Success';
        HEP_Constants__c objConstant = new HEP_Constants__c();
        objConstant.Name = 'HEP_CONTENT_TYPE_PDF';
        objConstant.Value__c = 'PDF';
        insert objConstant;
        HEP_Constants__c objConstant1 = new HEP_Constants__c();
        objConstant1.Name = 'HEP_INTEGRATION_SOURCE';
        objConstant1.Value__c = 'Source';
        insert objConstant1;
        
        HEP_Interface_Transaction__c objTransaction =  new HEP_Interface_Transaction__c();
        objTransaction.Retry_Count__c = 1;
        insert objTransaction;
        HEP_InterfaceTxnResponse objIntTxnResponse = new HEP_InterfaceTxnResponse();
        objIntTxnResponse.sInterfaceTxnId = '';
        objIntTxnResponse.sSourceId = 'TestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestDataTestData';
        objIntTxnResponse.bIsSourceAsAttachment = false;
        objIntTxnResponse.iMaxRetriesAllowed = 2;
        objIntTxnResponse.sStatus = 'Failure';
        objIntTxnResponse.iRetryInterval = 0;
        objIntTxnResponse.bIsCreateContentFile = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createInterfaceTxn(objIntTxnResponse);
        HEP_Integration_Util.CatalogCoupleComponentWrapper objCatalog = new HEP_Integration_Util.CatalogCoupleComponentWrapper('Test','Test','Test','Test','Test');
        HEP_Integration_Util.Transactionresponse objTxnResp = new HEP_Integration_Util.Transactionresponse('Test','Test','Test');
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    
        /**
    * createInterfaceTxn_Test --  Test method for the source Id Blank.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void testCreateInterfaceTxn2(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_InterfaceTxnResponse objInterfaceTransactionError = new HEP_InterfaceTxnResponse();
        //objInterfaceTransactionError.sStatus = 'Success';
        
        HEP_Interface_Transaction__c objTransaction =  new HEP_Interface_Transaction__c();
        
        insert objTransaction;
        HEP_InterfaceTxnResponse objIntTxnResponse = new HEP_InterfaceTxnResponse();
        objIntTxnResponse.sInterfaceTxnId = '';
        objIntTxnResponse.bIsSourceAsAttachment = false;
        objIntTxnResponse.iMaxRetriesAllowed = 1;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createInterfaceTxn(objIntTxnResponse);
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    
    
    @isTest static void createInterfaceTxnSourceTrue_Test(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_InterfaceTxnResponse objInterfaceTransactionError = new HEP_InterfaceTxnResponse();
        //objInterfaceTransactionError.sStatus = 'Success';
        
        HEP_Interface_Transaction__c objTransaction =  new HEP_Interface_Transaction__c();
        
        insert objTransaction;
        HEP_InterfaceTxnResponse objIntTxnResponse = new HEP_InterfaceTxnResponse();
        objIntTxnResponse.sInterfaceTxnId = objTransaction.Id;
        objIntTxnResponse.bIsSourceAsAttachment = true;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createInterfaceTxn(objIntTxnResponse);
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }

    /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void createInterfaceRetryRecord_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createInterfaceRetryRecord('HEP_InvoiceDetails', 'integBody','integType','failureReason');
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }

     /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void createContentRecords_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createContentRecords('Request','Response', objTerritory.id,objTerritory.id,true,true);
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }

     /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void retriveDatafromContent_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.createNewFile(objTerritory.Id, 'Filedata', 'Request');
        HEP_Integration_Util.retriveDatafromContent(objTerritory.Id,'Request');
        HEP_Integration_Util.updateExistingContent(objTerritory.Id, 'Filedata','Request');
        HEP_Integration_Util.upDateExistingorAddNew(objTerritory.Id, 'Filedata','Request');
        //System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }

    //commenting out sendToEDF method tests since it is not being used in HEP
      /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
   /* @isTest static void sendToEDF_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Integration_Util.Transactionresponse res= new HEP_Integration_Util.Transactionresponse('123456','789101112','131415');
         HEP_Integration_Util.CatalogCoupleComponentWrapper reswrap = new HEP_Integration_Util.CatalogCoupleComponentWrapper('123456','789101112','131415','abc','def');
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        FOX_Org_Sandbox_Control__c objSandboxControl = new FOX_Org_Sandbox_Control__c();
        objSandboxControl.Name = 'LOC';
        objSandboxControl.Is_Sandbox__c = true;
        insert objSandboxControl;
        LOC_Authentication__c objLOCAuthentication = new LOC_Authentication__c();
        objLOCAuthentication.Name = 'HEP_InvoiceDetails_QA';
        objLOCAuthentication.Endpoint_URL__c = 'https://ms-devapi.foxinc.com?foxid=123456';
        objLOCAuthentication.OAuth_Endpoint_URL__c = 'https://ms-devapi.foxinc.com?foxid=123456';
        objLOCAuthentication.Service_Account_ID__c = '123456';
        objLOCAuthentication.Share_Key_ID__c = '891011';
        objLOCAuthentication.Shared_Secret__c = '121314';
        insert objLOCAuthentication;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.sendToEDF('sJSON', 'HEP_InvoiceDetails', true);
        Test.stoptest();
    }*/
      /**
    * updateInterfaceTransactionError_Test --  Test method for the updateInterfaceTransactionError.
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
  /*  @isTest static void sendToEDF_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        FOX_Org_Sandbox_Control__c objSandboxControl = new FOX_Org_Sandbox_Control__c();
        objSandboxControl.Name = 'LOC';
        objSandboxControl.Is_Sandbox__c = true;
        insert objSandboxControl;
        LOC_Authentication__c objLOCAuthentication = new LOC_Authentication__c();
        objLOCAuthentication.Name = 'HEP_Invoice_QA';
        objLOCAuthentication.Endpoint_URL__c = 'https://ms-devapi.foxinc.com?foxid=1234567';
        objLOCAuthentication.OAuth_Endpoint_URL__c = 'https://ms-devapi.foxinc.com?foxid=1234567';
        objLOCAuthentication.Service_Account_ID__c = '123456';
        objLOCAuthentication.Share_Key_ID__c = '891011';
        objLOCAuthentication.Shared_Secret__c = '121314';
        insert objLOCAuthentication;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Integration_Util.sendToEDF('sJSON', 'HEP_InvoiceDetails', true);
        Test.stoptest();
    } */

    
}