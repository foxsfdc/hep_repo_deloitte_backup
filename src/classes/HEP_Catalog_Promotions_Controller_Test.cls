/**
 * HEP_Catalog_Promotions_Controller_Test --- 
 * @author   -- Roshi
 */
@isTest
public class HEP_Catalog_Promotions_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static void createData() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        insert objTerritory;
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        //Inserting Territory Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        //All HEP Constants
        //LOB
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true);
        //SKUTEMP 
        HEP_SKU_Template__c objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKUTEMPUI', objTerritory.id, objLOB.id, true);
       
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, null, null, null, false);
        objPromotion.EndDate__c = System.today();
        objPromotion.StartDate__c = System.today();
        objPromotion.Record_Status__c = 'Active';
        objPromotion.LineOfBusiness__c = objLOB.id;
        objPromotion.FirstAvailableDate__c = System.today();
        objPromotion.LocalPromotionCode__c = '00098';
        objPromotion.Domestic_Marketing_Manager__c = u.id;
        objPromotion.Status__c = 'Draft';
        insert objPromotion;
        //Creating Catalog  Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TESTTAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objCatalog;
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, objSKUTemp.id, true);
    }
    public static testMethod void test1() {
        createData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Catalog__c objCat = [SELECT id, Catalog_Name__c FROM HEP_Catalog__c where Catalog_Name__c = 'TESTTAL'];
        System.runAs(u) {
            Test.startTest();
            HEP_Catalog_Promotions_Controller.CatalogPromotionsWrapper objcatWrapper = new HEP_Catalog_Promotions_Controller.CatalogPromotionsWrapper();
            HEP_Catalog_Promotions_Controller.loadData(objCat.id);
            HEP_Catalog_Promotions_Controller.MasterCatalogPromotionsRowWrapper objcatWrapperM = new HEP_Catalog_Promotions_Controller.MasterCatalogPromotionsRowWrapper();
            Test.Stoptest();
        }
    }
}