/**
 * HEP_Suisse_LiveCustomerPromoEmailBatch --- Class to get the data for Suisse integration
 * @author    Nidhin VK
 */
global class HEP_Suisse_LiveCustomerPromoEmailBatch implements Schedulable {

    global void execute(SchedulableContext SC){
        HEP_Suisse_EmailXLSAttachmentController objClass = new HEP_Suisse_EmailXLSAttachmentController();
        List<HEP_MDP_Promotion_Product_Detail__c> lstProducts = new List<HEP_MDP_Promotion_Product_Detail__c>(); 
		Integer iDateLimit = 0 - Integer.valueOf(System.Label.HEP_SUISSE_LIVE_PROMO_DATE_LIMIT);
		//Test Class
		if(Test.isRunningTest())
			iDateLimit = 0;
		SavePoint objSavePoint = Database.setSavepoint();
    	try{
			lstProducts = objClass.getProducts();
			Map<Id, HEP_Promotion__c> mapPromotions = new Map<Id, HEP_Promotion__c>();
            for(HEP_MDP_Promotion_Product_Detail__c objProduct : lstProducts){
            	if(objProduct.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.LastModifiedDate.date() <= Date.today().addDays(iDateLimit)){
	            	mapPromotions.put(objProduct.HEP_Promotion_MDP_Product__r.HEP_Promotion__c, 
	            		new HEP_Promotion__c(Id = objProduct.HEP_Promotion_MDP_Product__r.HEP_Promotion__c, Suisse_Updated__c = true));
            	}
            }
            System.debug('lstProducts>>' + lstProducts);
            System.debug('mapPromotions>>' + mapPromotions);
            if(mapPromotions.size() > 0){
            	if('False'.equalsIgnoreCase(Label.HEP_Emails_isSandbox))
	            	update mapPromotions.values();
	            List<String> lstEmailIds = new List<String>();
	            for(HEP_List_Of_Values__c objLOV : [SELECT 
	            										Id, Values__c 
	            									FROM 
	            										HEP_List_Of_Values__c 
	            									WHERE 
	            										Type__c = :HEP_Utility.getConstantValue('HEP_LOV_TYPE_EMAIL_TO_USERS')
	            									AND 
	            										Parent_Value__c = :HEP_Utility.getConstantValue('HEP_LOV_PARENT_SUISSE')
	            									AND
	            										Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')]){
	            	lstEmailIds.add(objLOV.Values__c);
	            }
	            lstEmailIds = HEP_Utility.getActualEmailIds(lstEmailIds);
			    PageReference pageRef = Page.HEP_Suisse_EmailXLSAttachment;
			    //Previous code-- Roshi
			    //Blob objBlob = pageRef.getContent();

			    //Change for Code Coverage--Roshi 
			    Blob objBlob;

			    if(Test.isRunningTest()){
			      String myString = 'StringToBlob';
				  objBlob = Blob.valueof(myString);}
			    else{
                  objBlob = pageRef.getContent();
			    }


			    Messaging.SingleEmailMessage objEmail = new Messaging.SingleEmailMessage();
			
			    Messaging.EmailFileAttachment objEmailAttachment = new Messaging.EmailFileAttachment();
			    String sFileName = HEP_Utility.getConstantValue('HEP_SUISSE_EMAIL_SUBJECT') + '_' + DateTime.now().format(HEP_Utility.getConstantValue('HEP_SUISSE_EMAIL_DATE_FORMAT')) +'.xls';
			    objEmailAttachment.setFileName(sFileName);
			    objEmailAttachment.setBody(objBlob);
			    objEmail.setSubject(Label.HEP_SUISSE_LIVE_PROMO_EMAIL_SUBJECT);
			    objEmail.setToAddresses(lstEmailIds);
			    objEmail.setPlainTextBody(sFileName);
			    // searching for HEP Org Wide Address to use
            	List<OrgWideEmailAddress> lstOrgWideId = [select id, Address,DisplayName from OrgWideEmailAddress where DisplayName like '%HEP%'];
			    if(lstOrgWideId.size()>0)
                	objEmail.setOrgWideEmailAddressId(lstOrgWideId[0].Id); 
			    objEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {
			        objEmailAttachment
			    });
			    Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
			        objEmail
			    });
            }
    	} catch(Exception ex) {
            Database.rollback(objSavePoint);
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_Suisse_EmailXLSAttachmentBatch','finish',null,null);
    	} 
    }
}