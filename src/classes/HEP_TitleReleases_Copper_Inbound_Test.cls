@isTest (seeAllData = false)
public class HEP_TitleReleases_Copper_Inbound_Test {
     @testSetup 
    Static void setup(){
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        List<HEP_Promotion_Dating__c> lstDating = new List<HEP_Promotion_Dating__c>();
        HEP_List_Of_Values__c objlistValueHD = new HEP_List_Of_Values__c(Values__c = 'EST',Order__c = '25',Type__c = 'COPPER_CHANNEL',Parent_Value__c='SD');
        insert objlistValueHD;
         EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;
        HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS1', 'AUS1', '', false);
            objTerritory1.Flag_Country_Code__c = 'AU';
            objTerritory1.Send_to_Prophet__c = True;
            insert objTerritory1;
            HEP_Territory__c objTerritory2 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Canada', 'EMEA', 'Subsidiary', null, null, 'ARS', 'CAN', 'CAN', '', false);
            objTerritory2.Flag_Country_Code__c = 'CA';
            objTerritory2.Send_to_Prophet__c = True;
            insert objTerritory2;
            HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'Domestic', 'Subsidiary', null, null, 'USD', 'US', 'US', '', false);
            objTerritory.Flag_Country_Code__c = 'US';
            objTerritory.Send_to_Prophet__c = True;
            insert objTerritory;
             HEP_Promotion__c objPromotionNational = HEP_Test_Data_Setup_Utility.createPromotion('Global National 1', 'National', null,objTerritory1.Id, null, null,null,false);
        objPromotionNational.Record_Status__c= 'Active'; 
        objPromotionNational.StartDate__c = date.today();
        objPromotionNational.EndDate__c = date.today()+20;
        // objPromotion.Customer__c = objCustomer.id;
        insert objPromotionNational;
             HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory1.Id, 'Approved', 'Request', null);
            objCatalog.Title_EDM__c = objTitle.id;
            objCatalog.Record_Status__c = 'Active';
            objCatalog.Catalog_Type__c = 'Bundle'; 
            insert objCatalog; 
            List<String> terrList = new List<String>();
            terrList.add(objTerritory1.Id);
            HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory1.Id, objTerritory1.Id, false);
        objDatingMatrix.Dating_Flag__c=Boolean.valueOf('true');
        // objDatingMatrix.channel__c ='EST';
        insert objDatingMatrix;
        HEP_Promotions_DatingMatrix__c testdata = [SELECT channel__c
from HEP_Promotions_DatingMatrix__c where id=:objDatingMatrix.Id];
        system.debug('testdata'+testdata);
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory1.id,objPromotionNational.Id,objDatingMatrix.id,False);
        objDatingRecord.Record_Status__c = 'Active';
        objDatingRecord.Status__c =  'Confirmed';
        objDatingRecord.Date_Type__c =  'EST Release Date';
        objDatingRecord.Customer__c = null;
        
        objDatingRecord.HEP_Catalog__c = objCatalog.id;
        objDatingRecord.Date__c =Date.today();
        // insert objDatingRecord;
        lstDating.add(objDatingRecord);
        HEP_Promotion_Dating__c objDatingRecord3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory2.id,objPromotionNational.Id,objDatingMatrix.id,False);
        objDatingRecord3.Record_Status__c = 'Active';
        objDatingRecord3.Status__c =  'Confirmed';
        objDatingRecord3.Date_Type__c =  'EST Release Date';
        objDatingRecord3.Customer__c = null;
        
        objDatingRecord3.HEP_Catalog__c = objCatalog.id;
        objDatingRecord3.Date__c =Date.today();
        // insert objDatingRecord3;
        lstDating.add(objDatingRecord3);
        HEP_Promotion_Dating__c objDatingRecord2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id,objPromotionNational.Id,objDatingMatrix.id,False);
        objDatingRecord2.Record_Status__c = 'Inactive';
        objDatingRecord2.Status__c =  'Confirmed';
        objDatingRecord2.Date_Type__c =  'EST Release Date';
        objDatingRecord2.Customer__c = null;
        
        objDatingRecord2.HEP_Catalog__c = objCatalog.id;
        objDatingRecord2.Date__c =Date.today();
        // insert objDatingRecord2;
        lstDating.add(objDatingRecord2);
        insert lstDating;
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory1.Id,'10', 768.00,False);
         objPriceGrade.Type__c ='MDP_EST';
         objPriceGrade.MM_RateCard_ID__c ='123';
         objPriceGrade.MDP_Duration_Start__c = '10';
         objPriceGrade.MDP_Duration_End__c = '20';
         objPriceGrade.MDP_PromoWSP__c = '111';
         insert objPriceGrade;
        HEP_SKU_Master__c objSKUMasterSD = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory1.Id, null, 'EST SKU', 'Master', 'Physical', 'EST', 'BD', null);
        objSKUMasterSD.Record_Status__c = 'Active';
        insert objSKUMasterSD;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','Catalog',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
         HEP_SKU_Template__c  objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU Template',objTerritory1.Id,objLOB.Id,false);
        objSKUTemp.Record_Status__c = 'Active';
        //insert objSKUTemp;
         HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotionNational.Id,objSKUTemp.Id,false);
         objPromoCatalog.Record_Status__c = 'Active';
         insert objPromoCatalog;
        HEP_Promotion_SKU__c promotionsku = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKUMasterSD.ID, 'Approved', 'Pending',false);
        promotionsku.Record_Status__c = 'Active';
        insert promotionsku;
        HEP_SKU_Price__c objSKUPriceSD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterSD.Id, objTerritory1.Id, null, null, 'Active', false);
        objSKUPriceSD.Start_Date__c = date.today()-30;
        objSKUPriceSD.Promotion_SKU__c = promotionsku.Id;
        objSKUPriceSD.Promotion_Dating__c = objDatingRecord.Id;
        insert objSKUPriceSD; 
        HEP_SKU_Price__c objSKUPriceSD1 = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterSD.Id, objTerritory.Id, null, null, 'Active', false);
        objSKUPriceSD1.Start_Date__c = date.today()-30;
        objSKUPriceSD1.Promotion_SKU__c = promotionsku.Id;
        objSKUPriceSD1.Promotion_Dating__c = objDatingRecord2.Id;
        insert objSKUPriceSD1; 
        system.debug('objDatingRecord-->'+objDatingRecord);
        system.debug('objSKUMasterSD-->'+objSKUMasterSD);
        system.debug('objSKUPriceSD-->'+objSKUPriceSD);
        system.debug('objCatalog-->'+objCatalog);
        system.debug('objTerritory1-->'+objTerritory1);
        // List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        
        
        
    }
    @isTest
    static void testPerformTransaction (){
         
        HEP_InterfaceTxnResponse objWrap = new HEP_InterfaceTxnResponse();
        objWrap.sRequest = '345';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        HEP_TitleReleases_Copper_Inbound copp = new HEP_TitleReleases_Copper_Inbound();
        copp.performTransaction(objWrap);
        
        // objWrap.sRequest = '2018-06-01T00:00:00Z';
        // HEP_TitleReleases_Copper_Inbound copp1 = new HEP_TitleReleases_Copper_Inbound();
        // copp1.performTransaction(objWrap);
        // HEP_ReleaseKeys_Foxipedia_Inbound.ReleaseKeys rk = new HEP_ReleaseKeys_Foxipedia_Inbound.ReleaseKeys('345',terrList);
        Test.stopTest();
        
    }
}