public class HEP_Search_Controller {

	public map<String, String> mapPageAccess{get;set;}
    public String sHeaderTabs{get;set;}
    public String sCurrencyFormat {get;set;}

	 /**
    * Class constructor to initialize the class variables
    * @return nothing
    * @author Arjun Narayanan
    */ 
	public HEP_Search_Controller() {
		// Calling the security framework to get the list of tabs to be shown on the UI 
        list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
        mapPageAccess = HEP_Security_Framework.getTerritoryAccess(null, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_SEARCH'), HEP_Utility.getConstantValue('HEP_FILTER_SEARCH'), lstHeaderTabs);
                
        sHeaderTabs = JSON.serialize(lstHeaderTabs); 
        
        sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(null);	
	}    
}