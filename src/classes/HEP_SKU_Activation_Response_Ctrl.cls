/**
 * HEP_SKU_Activation_Response_Ctrl --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_SKU_Activation_Response_Ctrl{
    
    public Id recID  {get; set;}
    public String sPageLink {get;set;}
     public String sHref {get; set;}
    //This variable is used to render table on Email template
    public List<EmailWrapper> lstEmailWrapper{
            get{
                if(lstEmailWrapper == null){
                    fetchDetails();
                }
                return lstEmailWrapper;
            }
            set;
    }
     
    
    /**
    * Helps to get details of Confirmed SKU Promotion records for the promotion Id passed from VF component.
    * @return null
    */ 
    public void fetchDetails(){
        
        try{
            String sSKUPromotionPageUrl='/apex/HEP_Promotion_Details?promoId=';
            this.lstEmailWrapper = new List<EmailWrapper>();   
            EmailWrapper objMail; 
            String sSKUPromotionId; 
            String sPromotionId; 
            String sApprovalRole;
            Set<Id> setRecordId  =new Set<Id>();
            Set<Id> setPromotionSkuIds  =new Set<Id>();
            //Helps to get the details of the Promotion SKU records where status is Approved.    
            
            List<HEP_Outbound_Email__c> lstOutBoundEmail = new List<HEP_Outbound_Email__c>([Select HEP_Promotion__c,
                                                                                                    Record_Id__c,
                                                                                                    Email_Sent__c 
                                                                                            from HEP_Outbound_Email__c
                                                                                            Where Email_Sent__c = False
                                                                                                    AND Email_Template_Name__c = 'HEP_SKU_Activation_Request'
                                                                                                    AND HEP_Promotion__c =:recID]);
            
            System.debug(lstOutBoundEmail.size());
            for(HEP_Outbound_Email__c objOutBoundEmail: lstOutBoundEmail){
                setRecordId.add(objOutBoundEmail.Record_Id__c);
            }                                                                                       
            if(recID != null){          
                List<HEP_Record_Approver__c> lstPrSKU  = [SELECT Id,
                                                    Name,
                                                    Approval_Record__r.CreatedDate,
                                                    Approval_Record__r.CreatedBy.Name,
                                                    Approver__r.Name, 
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,                                                
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Title__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Number__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Format__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Channel__c,
                                                    Approver_Role__r.Name,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Barcode__c                                                    
                                                    FROM HEP_Record_Approver__c
                                                    Where Id IN : setRecordId
                                                    ];              
                //Validate the List and Populate the Wrapper
                if(lstPrSKU != null && !lstPrSKU.isEmpty()){    
                    System.debug('+++++++++' + lstPrSKU );
                    for(HEP_Record_Approver__c objRecordApprover : lstPrSKU){
                        setPromotionSkuIds.add(objRecordApprover.Approval_Record__r.HEP_Promotion_SKU__c);
                    }
                    
                    sSKUPromotionId = lstPrSKU[0].Approval_Record__r.HEP_Promotion_SKU__c;
                    sPromotionId =  lstPrSKU[0].Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__c;
                    sApprovalRole = lstPrSKU[0].Approver_Role__r.Name;                  
                }
                 //Query to get all the Price Regions records for the promotion SKU record
                List<HEP_SKU_Price__c> lstSKUPriceRegion = [SELECT Id,
                                                    PriceGrade__c,
                                                    PriceGrade__r.Name,
                                                    PriceGrade__r.PriceGrade__c,
                                                    PriceGrade__r.Dealer_List_Price__c,
                                                    Region__c,
                                                    Region__r.Name,
                                                    Unit__c,
                                                    Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                                                    Promotion_Dating__r.Date__c,                                                   
                                                    SKU_Master__r.SKU_Number__c,
                                                    SKU_Master__r.SKU_Title__c,
                                                    SKU_Master__r.Format__c,
                                                    SKU_Master__r.Barcode__c,
                                                    SKU_Master__r.Channel__c
                                                    FROM HEP_SKU_Price__c
                                                    WHERE Promotion_SKU__c IN : setPromotionSkuIds
                                                    ORDER BY SKU_Master__r.SKU_Number__c];
                if(lstSKUPriceRegion != null && !lstSKUPriceRegion.isEmpty()){
                    for(HEP_SKU_Price__c objPrRegion : lstSKUPriceRegion){
                        objMail = new EmailWrapper();
                        objMail.sRegion = objPrRegion.Region__r.Name;
                        objMail.sPrCode = objPrRegion.Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c;
                        objMail.sSKUName = objPrRegion.SKU_Master__r.SKU_Title__c;
                        objMail.sReleaseDate = HEP_Utility.getFormattedDate(objPrRegion.Promotion_Dating__r.Date__c,'dd-MMM-yyyy');
                        objMail.sSKUNumber = objPrRegion.SKU_Master__r.SKU_Number__c;
                        objMail.sFormat = objPrRegion.SKU_Master__r.Format__c;
                        objMail.sChannel = objPrRegion.SKU_Master__r.Channel__c;
                        objMail.sBarcode = objPrRegion.SKU_Master__r.Barcode__c;
                        sHref  = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_Promotion_Details?promoId=' + sPromotionId +'&tab=HEP_Promotion_Products';
                        if(objPrRegion.Region__r.Name.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_TERRITORY_US'))){
                            objMail.sUSPrice = objPrRegion.PriceGrade__r.PriceGrade__c;
                        } else if(objPrRegion.Region__r.Name.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_TERRITORY_CANADA'))) {
                            objMail.sCANPrice = objPrRegion.PriceGrade__r.PriceGrade__c;
                        }
                        if(sApprovalRole.startsWith(HEP_Utility.getConstantValue('HEP_MARKETING_MANAGER'))){
                            objMail.sEmailTitle = 'The following SKUs have been confirmed by Customer Service and are ready for review:';
                        } else if(sApprovalRole.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATA_ADMIN'))){
                            objMail.sEmailTitle = 'The following SKUs have been submitted by Brand and are ready for review:';
                        }
                        lstEmailWrapper.add(objMail);            
                    }
                }
                
                
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

       
    /*
    **  EmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class EmailWrapper{
        
        public String sPrCode {get; set;}
        public String sSKUName {get; set;}
        public String sSKUNumber{get; set;}
        public String sBarcode {get; set;}      
        public String sRegion {get; set;}      
        public String sChannel {get; set;}      
        public String sFormat {get; set;}      
        public String sReleaseDate {get; set;}
        public String sUSPrice {get; set;}            
        public String sCANPrice {get; set;}  
        public String sHref {get;set;} 
        public String sEmailTitle {get;set;}    
    }

}