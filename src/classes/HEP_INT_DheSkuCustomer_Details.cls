/**
* HEP_INT_DheSkuCustomer_Details -- Customer details to store Customer details 
* @author    Avinash Harwani
*/
public class HEP_INT_DheSkuCustomer_Details implements HEP_IntegrationInterface{ 
    
    public static String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');

    /**
    * HEP_INT_DheSkuCustomer_Details -- Customer details to store Customer details 
    * @return no return value 
    * @author    Avinash Harwani
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_INT_DheSkuCustomerWrapper objWrapper = new HEP_INT_DheSkuCustomerWrapper();         
        if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sTimeStamp;
      String sLastPulledDate;
      String sNoOfRows;
      String sStartingFrom;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the TimeStamp, LastPulledDate, NoOfRows, StartingFrom details to get the sku box details through integration 
            HEP_INT_DheSkuInvoiceWrapperUtility objDheSkuInvoiceWrapperUtility = (HEP_INT_DheSkuInvoiceWrapperUtility)JSON.deserialize(sDetails,HEP_INT_DheSkuInvoiceWrapperUtility.class);
            if(objDheSkuInvoiceWrapperUtility != null){                            
            sTimeStamp = objDheSkuInvoiceWrapperUtility.sTimeStamp;
            sLastPulledDate = objDheSkuInvoiceWrapperUtility.sLastPulledDate;
            sNoOfRows = objDheSkuInvoiceWrapperUtility.sNoOfRows;
            sStartingFrom = objDheSkuInvoiceWrapperUtility.sStartingFrom;            
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_SKU_CUSTOMERDETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_JDE_SKU_CUSTOMERDETAILS'));                                   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?HHMMSS=' + sTimeStamp + '&YYYYMMDD=' + sLastPulledDate+ '&NO_OF_ROWS=' + sNoOfRows+ '&STARTING_FROM=' + sStartingFrom);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                system.debug('reponse body'+objTxnResponse.sResponse);
                if(/*HEP_Constants__c.getValues('HEP_SUCCESS') != null  && HEP_Utility.getConstantValue('HEP_SUCCESS') != null
                    && */objHttpResp.getStatus().equals('OK')){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                                       
                    String sResponse = objHttpResp.getBody();
                    //sResponse = sResponse.replace(HEP_Constants.HEP_SKUNo, HEP_Constants.HEP_REPLACE_SKUNo);  
                    sResponse = sResponse.replace(HEP_Utility.getConstantValue('HEP_SKUNo') , HEP_Utility.getConstantValue('HEP_REPLACE_SKUNo')); 
                    //sResponse = sResponse.replace(HEP_Constants.HEP_ECopy_Flag , HEP_Constants.HEP_REPLACE_ECopy_Flag);
                    //sResponse = sResponse.replace(HEP_Constants.HEP_BW_Color , HEP_Constants.HEP_REPLACE_BW_Color);
                    //sResponse = sResponse.replace(HEP_Constants.HEP_ThreeD_Flag , HEP_Constants.HEP_REPLACE_ThreeD_Flag);
                    
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    objWrapper = (HEP_INT_DheSkuCustomerWrapper)JSON.deserialize(sResponse, HEP_INT_DheSkuCustomerWrapper.class);  
                     if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        objTxnResponse.bRetry = false;
                        //Update customer details in SF
                        System.debug('objWrapper+++++   ' + objWrapper);
                        updateCustomerDetails(objWrapper);
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }      
    }  

    private static void updateCustomerDetails(HEP_INT_DheSkuCustomerWrapper objWrapper){
    	try{
	        if(objWrapper.dhe_sku_customer.isEmpty()) return;
	        
	        Set<String> skuNumberSet = new Set<String>();
	        Set<String> customerNumberSet = new Set<String>();
	        Map<String, String> skuCustomerMap = new Map<String, String>();
	        Map<String, String> map_skuNoToSkuMasterId = new Map<String, String>();
	        Map<String, String> map_CustomerNoToCustomerId = new Map<String, String>();
	        for(HEP_INT_DheSkuCustomerWrapper.DheSkuCustomer customerWrap : objWrapper.dhe_sku_customer){
	            skuNumberSet.add(customerWrap.SKU_No);
	            customerNumberSet.add(customerWrap.Customer_Number);
	        }
	        
	        for(HEP_SKU_Customer__c skuCustomer : [Select Id, Customer__r.Customer_Number__c, SKU_Master__r.SKU_Number__c from HEP_SKU_Customer__c where SKU_Master__r.SKU_Number__c IN :skuNumberSet AND Customer__r.Customer_Number__c IN :customerNumberSet]){
	            skuCustomerMap.put(skuCustomer.SKU_Master__r.SKU_Number__c + '-' + skuCustomer.Customer__r.Customer_Number__c, skuCustomer.Id);
	        }
	        
	        for(HEP_SKU_Master__c skuMaster : [Select Id, SKU_Number__c from HEP_SKU_Master__c where SKU_Number__c IN :skuNumberSet]){
	            map_skuNoToSkuMasterId.put(skuMaster.SKU_Number__c, skuMaster.Id);
	        }
	        
	        for(HEP_Customer__c customer : [Select Id, Customer_Number__c from HEP_Customer__c where Customer_Number__c IN :customerNumberSet]){
	            map_CustomerNoToCustomerId.put(customer.Customer_Number__c, customer.Id);
	        }
	        
	        List<HEP_SKU_Customer__c> skuCustomerListToUpsert = new List<HEP_SKU_Customer__c>();
	        for(HEP_INT_DheSkuCustomerWrapper.DheSkuCustomer customerWrap : objWrapper.dhe_sku_customer){
	            HEP_SKU_Customer__c skuCustomer = new HEP_SKU_Customer__c();
	            if(skuCustomerMap.containsKey(customerWrap.SKU_No + '-' + customerWrap.Customer_Number)) skuCustomer.id = skuCustomerMap.get(customerWrap.SKU_No + '-' + customerWrap.Customer_Number);
	            if(map_skuNoToSkuMasterId.containsKey(customerWrap.SKU_No)) skuCustomer.SKU_Master__c = map_skuNoToSkuMasterId.get(customerWrap.SKU_No);
	            else continue;
	            if(map_CustomerNoToCustomerId.containsKey(customerWrap.Customer_Number)) skuCustomer.Customer__c = map_CustomerNoToCustomerId.get(customerWrap.Customer_Number);
	            else continue;
	            skuCustomer.Record_Status__c = 'D'.equalsIgnoreCase(customerWrap.Action_Code) ? 'Inactive' : sACTIVE;
	            skuCustomerListToUpsert.add(skuCustomer);
	        }
	        
	        if(!skuCustomerListToUpsert.isEmpty()) upsert skuCustomerListToUpsert;
        
        } catch(Exception ex){
            //throw error
            System.debug('Exception on Class : HEP_INT_DheSkuCustomer_Details - updateCustomerDetails, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_INT_DheSkuCustomer_Details','updateCustomerDetails',null,null);
        }        
        
    }
    
}