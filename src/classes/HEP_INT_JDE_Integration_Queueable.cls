/**
 * HEP_INT_JDE_Integration --- Class contains Mapping information for JDE Outbound integrations 
 * @author    Ram Bairwa
 */
public class HEP_INT_JDE_Integration_Queueable implements Queueable, Database.AllowsCallouts {
    
    public Id promotionId;
    public List<Id> lstDatingIds;
    
    /**
    * Class constructor to initialize the class variables
    * @return nothing 
    * @author Avinash Harwani 
    */  
    public HEP_INT_JDE_Integration_Queueable(Id promoId){
        this.promotionId = promoId;
    }
    
    public HEP_INT_JDE_Integration_Queueable(Id promoId, List<Id> lstDatingIds){
        this.promotionId = promoId;
        this.lstDatingIds= lstDatingIds;
    }

    /**
    * Class constructor to initialize the class variables
    * @return nothing 
    * @author Avinash Harwani 
    */  
    public void execute(QueueableContext qContext){

        HEP_ExecuteIntegration.executeIntegMethod(promotionId,'','HEP_JDE_PromoSKU');
        
        // chaining Dating queueable
        if(!Test.isRunningTest() && lstDatingIds != null && lstDatingIds.size()>0){
            if(HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING') != null && HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c != null) 
                System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstDatingIds, ',') , HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c));

        }
    }

}