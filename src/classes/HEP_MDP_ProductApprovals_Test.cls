@isTest
    public class HEP_MDP_ProductApprovals_Test{
        public static testmethod void submitForApproval_Test(){
           
            //HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
            //Create Test User
            User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
            //Create HEP Territory Record
            HEP_Territory__c objTerritory1 = new HEP_Territory__c();
            objTerritory1.Name = 'US';
            objTerritory1.Region__c = 'APAC';
            objTerritory1.Type__c = 'Subsidiary';
            objTerritory1.CurrencyCode__c = 'USD';
            objTerritory1.ERMTerritoryCode__c = 'Test Code122';
            objTerritory1.Territory_Code_Integration__c = '001';
            objTerritory1.MM_Territory_Code__c = 'abc';
            insert objTerritory1;
            //Creating Product Type record for Title -> Season
            EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
            insert objProductType;
            //Creating Title Record
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
            objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'CNFDL';
            objTitle.FIN_PROD_ID__c = 'FIN_PROD_ID__c';
            insert objTitle;
            //Create HEP Customer 
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            //Create LOB Record_Status__c
            HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
            insert objLOB;
            //Create Promotion Record
            HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
            objPromotion1.PromotionName__c = 'Promo';
            objPromotion1.Promotion_Type__c  = 'Customer';
            objPromotion1.Record_Status__c  = 'Active';
            objPromotion1.Requestor__c = u.Id;
            objPromotion1.Customer__c = objCustomer.Id;
            objPromotion1.Record_Status__c = 'Active';
            objPromotion1.LineOfBusiness__c = objLOB.Id;
            objPromotion1.Territory__c = objTerritory1.Id;
            objPromotion1.Promotion_Type__c = 'Customer';
            insert objPromotion1;
            HEP_Promotion__c objPromotion2 = new HEP_Promotion__c();
            objPromotion2.PromotionName__c = 'Promo';
            objPromotion2.Record_Status__c  = 'Active';
            objPromotion2.Requestor__c = u.Id;
            objPromotion2.Customer__c = objCustomer.Id;
            objPromotion2.Record_Status__c = 'Active';
            objPromotion2.LineOfBusiness__c = objLOB.Id;
            objPromotion2.Territory__c = objTerritory1.Id;
            objPromotion2.Promotion_Type__c = 'National';
            insert objPromotion2;
            HEP_MDP_Promotion_Product__c objPromoProduct1 = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion2.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct1.Approval_Status__c = 'Approved';
            objPromoProduct1.Product_End_Date__c = System.today().addDays(14);
            objPromoProduct1.Product_Start_Date__c = System.today();
            objPromoProduct1.Duration__c = 12;
            objPromoProduct1.Record_Status__c = 'Active';
            insert objPromoProduct1;
            HEP_MDP_Promotion_Product__c objPromoProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion1.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct.Approval_Status__c = 'Draft';
            objPromoProduct.Product_End_Date__c = System.today().addDays(10);
            objPromoProduct.Product_Start_Date__c = System.today();
            objPromoProduct.Duration__c = 11;
            
            insert objPromoProduct;
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct.Id,'SD','EST',false);
            objPromoProductDetail.Record_Status__c = 'Active';
            objPromoProductDetail.HEP_Promotion_MDP_Product__c = objPromoProduct1.Id;
            objPromoProductDetail.Promo_WSP__c = 10.99;
            objPromoProductDetail.Everyday_SRP__c = 99.99;
            insert objPromoProductDetail;
            List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            lstPromotionMDPProducts.add(objPromoProduct);
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail1 = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct1.Id,'HD','EST',false);
            objPromoProductDetail1.Record_Status__c = 'Active';
            objPromoProductDetail1.HEP_Promotion_MDP_Product__c = objPromoProduct.Id;
            objPromoProductDetail1.Promo_WSP__c = 19.99;
            objPromoProductDetail1.Everyday_SRP__c = 199.99;
            insert objPromoProductDetail1;
            //List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            //lstPromotionMDPProducts.add(objPromoProduct);
            
            test.startTest();
            HEP_MDP_ProductApprovals.submitForApproval(lstPromotionMDPProducts,true);
            test.stopTest();
        }
        public static testmethod void submitForApproval_Test2(){
            //HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
            //Create Test User
            User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
            //Create HEP Territory Record
            HEP_Territory__c objTerritory1 = new HEP_Territory__c();
            objTerritory1.Name = 'US';
            objTerritory1.Region__c = 'APAC';
            objTerritory1.Type__c = 'Subsidiary';
            objTerritory1.CurrencyCode__c = 'USD';
            objTerritory1.ERMTerritoryCode__c = 'Test Code122';
            objTerritory1.Territory_Code_Integration__c = '001';
            objTerritory1.MM_Territory_Code__c = 'abc';
            insert objTerritory1;
            //Creating Product Type record for Title -> Season
            EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
            insert objProductType;
            //Creating Title Record
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
            objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'CNFDL';
            objTitle.FIN_PROD_ID__c = 'FIN_PROD_ID__c';
            insert objTitle;
            //Create HEP Customer 
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple',objTerritory1.Id,'4','MDP Customers','',false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            //Create LOB Record_Status__c
            HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
            insert objLOB;
            //Create Promotion Record
            HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
            objPromotion1.PromotionName__c = 'Promo';
            objPromotion1.Promotion_Type__c  = 'Customer';
            objPromotion1.Record_Status__c  = 'Active';
            objPromotion1.Requestor__c = u.Id;
            objPromotion1.Customer__c = objCustomer.Id;
            objPromotion1.Record_Status__c = 'Active';
            objPromotion1.LineOfBusiness__c = objLOB.Id;
            objPromotion1.Territory__c = objTerritory1.Id;
            objPromotion1.Promotion_Type__c = 'Customer';
            insert objPromotion1;
            HEP_Promotion__c objPromotion2 = new HEP_Promotion__c();
            objPromotion2.PromotionName__c = 'Promo';
            objPromotion2.Record_Status__c  = 'Active';
            objPromotion2.Requestor__c = u.Id;
            objPromotion2.Customer__c = objCustomer.Id;
            objPromotion2.Record_Status__c = 'Active';
            objPromotion2.LineOfBusiness__c = objLOB.Id;
            objPromotion2.Territory__c = objTerritory1.Id;
            objPromotion2.Promotion_Type__c = 'National';
            insert objPromotion2;
            HEP_MDP_Promotion_Product__c objPromoProduct1 = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion2.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct1.Approval_Status__c = 'Approved';
            objPromoProduct1.Product_End_Date__c = System.today().addDays(14);
            objPromoProduct1.Product_Start_Date__c = System.today();
            objPromoProduct1.Duration__c = 12;
            objPromoProduct1.Record_Status__c = 'Active';
            insert objPromoProduct1;
            HEP_MDP_Promotion_Product__c objPromoProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion1.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct.Approval_Status__c = 'Draft';
            objPromoProduct.Product_End_Date__c = System.today().addDays(10);
            objPromoProduct.Product_Start_Date__c = System.today();
            objPromoProduct.Duration__c = 11;
            
            insert objPromoProduct;
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct.Id,'HD','EST',false);
            objPromoProductDetail.Record_Status__c = 'Active';
            objPromoProductDetail.HEP_Promotion_MDP_Product__c = objPromoProduct1.Id;
            objPromoProductDetail.Promo_WSP__c = 19.99;
            objPromoProductDetail.Everyday_SRP__c = 99.99;
            insert objPromoProductDetail;
            List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            lstPromotionMDPProducts.add(objPromoProduct);
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail1 = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct1.Id,'HD','EST',false);
            objPromoProductDetail1.Record_Status__c = 'Active';
            objPromoProductDetail1.HEP_Promotion_MDP_Product__c = objPromoProduct.Id;
            objPromoProductDetail1.Promo_WSP__c = 19.99;
            objPromoProductDetail1.Everyday_SRP__c = 199.99;
            insert objPromoProductDetail1;
            //List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            //lstPromotionMDPProducts.add(objPromoProduct);
            
            test.startTest();
            HEP_MDP_ProductApprovals.submitForApproval(lstPromotionMDPProducts,false);
            test.stopTest();
        }
        public static testmethod void submitForApproval_Test3(){
            //HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
            //Create Test User
            User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
            //Create HEP Territory Record
            HEP_Territory__c objTerritory1 = new HEP_Territory__c();
            objTerritory1.Name = 'US';
            objTerritory1.Region__c = 'APAC';
            objTerritory1.Type__c = 'Subsidiary';
            objTerritory1.CurrencyCode__c = 'USD';
            objTerritory1.ERMTerritoryCode__c = 'Test Code122';
            objTerritory1.Territory_Code_Integration__c = '001';
            objTerritory1.MM_Territory_Code__c = 'abc';
            insert objTerritory1;
            //Creating Product Type record for Title -> Season
            EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
            insert objProductType;
            //Creating Title Record
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
            objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'CNFDL';
            objTitle.FIN_PROD_ID__c = 'FIN_PROD_ID__c';
            insert objTitle;
            //Create HEP Customer 
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple',objTerritory1.Id,'4','MDP Customers','',false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            //Create LOB Record_Status__c
            HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
            insert objLOB;
            //Create Promotion Record
            HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
            objPromotion1.PromotionName__c = 'Promo';
            objPromotion1.Promotion_Type__c  = 'Customer';
            objPromotion1.Record_Status__c  = 'Active';
            objPromotion1.Requestor__c = u.Id;
            objPromotion1.Customer__c = objCustomer.Id;
            objPromotion1.Record_Status__c = 'Active';
            objPromotion1.LineOfBusiness__c = objLOB.Id;
            objPromotion1.Territory__c = objTerritory1.Id;
            objPromotion1.Promotion_Type__c = 'Customer';
            insert objPromotion1;
            HEP_Promotion__c objPromotion2 = new HEP_Promotion__c();
            objPromotion2.PromotionName__c = 'Promo';
            objPromotion2.Record_Status__c  = 'Active';
            objPromotion2.Requestor__c = u.Id;
            objPromotion2.Customer__c = objCustomer.Id;
            objPromotion2.Record_Status__c = 'Active';
            objPromotion2.LineOfBusiness__c = objLOB.Id;
            objPromotion2.Territory__c = objTerritory1.Id;
            objPromotion2.Promotion_Type__c = 'National';
            insert objPromotion2;
            HEP_MDP_Promotion_Product__c objPromoProduct1 = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion2.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct1.Approval_Status__c = 'Approved';
            objPromoProduct1.Product_End_Date__c = System.today().addDays(14);
            objPromoProduct1.Product_Start_Date__c = System.today();
            objPromoProduct1.Duration__c = 12;
            objPromoProduct1.Record_Status__c = 'Active';
            insert objPromoProduct1;
            HEP_MDP_Promotion_Product__c objPromoProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion1.Id,objTitle.Id,objTerritory1.Id,false);
            objPromoProduct.Approval_Status__c = 'Draft';
            objPromoProduct.Product_End_Date__c = System.today().addDays(10);
            objPromoProduct.Product_Start_Date__c = System.today();
            objPromoProduct.Duration__c = 11;
            
            insert objPromoProduct;
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct.Id,'HD','EST',false);
            objPromoProductDetail.Record_Status__c = 'Active';
            objPromoProductDetail.HEP_Promotion_MDP_Product__c = objPromoProduct1.Id;
            objPromoProductDetail.Promo_WSP__c = 19.99;
            objPromoProductDetail.Everyday_SRP__c = 99.99;
            insert objPromoProductDetail;
            List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            lstPromotionMDPProducts.add(objPromoProduct);
            HEP_MDP_Promotion_Product_Detail__c objPromoProductDetail1 = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProduct1.Id,'HD','EST',false);
            objPromoProductDetail1.Record_Status__c = 'Active';
            objPromoProductDetail1.HEP_Promotion_MDP_Product__c = objPromoProduct.Id;
            objPromoProductDetail1.Promo_WSP__c = 19.99;
            objPromoProductDetail1.Everyday_SRP__c = 199.99;
            insert objPromoProductDetail1;
            //List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts = new List<HEP_MDP_Promotion_Product__c>();
            //lstPromotionMDPProducts.add(objPromoProduct);
            
            test.startTest();
            HEP_MDP_ProductApprovals.submitForApproval(lstPromotionMDPProducts,false);
            test.stopTest();
        }
        public static testmethod void getApprovalHierarchy_Test(){
            //HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
            //Create Test User
            User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
            //Create HEP Territory Record
            HEP_Territory__c objTerritory1 = new HEP_Territory__c();
            objTerritory1.Name = 'US';
            objTerritory1.Region__c = 'APAC';
            objTerritory1.Type__c = 'Subsidiary';
            objTerritory1.CurrencyCode__c = 'USD';
            objTerritory1.ERMTerritoryCode__c = 'Test Code122';
            objTerritory1.Territory_Code_Integration__c = '001';
            objTerritory1.MM_Territory_Code__c = 'abc';
            insert objTerritory1;
            //Create HEP Territory Record
            HEP_Territory__c objTerritory = new HEP_Territory__c();
            objTerritory.Name = 'canada';
            objTerritory.Region__c = 'APAC';
            objTerritory.Type__c = 'Subsidiary';
            objTerritory.CurrencyCode__c = 'AUD';
            objTerritory.ERMTerritoryCode__c = 'Test Code';
            objTerritory.Territory_Code_Integration__c = '000';
            objTerritory.MM_Territory_Code__c = 'a';
            insert objTerritory;
            //Create Promotion Dating Matrix record
            HEP_Promotions_DatingMatrix__c objPromoDatingMtrx = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
            objPromoDatingMtrx.Media_Type__c = 'Digital';
            insert objPromoDatingMtrx;
            //Create Promotion Dating Matrix record
            HEP_Promotions_DatingMatrix__c objPromoDatingMtrx1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
            objPromoDatingMtrx1.Media_Type__c = 'Physical';
            insert objPromoDatingMtrx1;
            //Create HEP Customer 
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            //Create Promotion Record
            HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
            objPromotion1.PromotionName__c = 'Promo';
            objPromotion1.Promotion_Type__c  = 'Customer';
            objPromotion1.Record_Status__c  = 'Active';
            objPromotion1.Requestor__c = u.Id;
            objPromotion1.Customer__c = objCustomer.Id;
            objPromotion1.Record_Status__c = 'Active';
            insert objPromotion1;
            //Create Catalog Record
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.Catalog_Name__c = 'Primary Catalog';
            objCatalog.Product_Type__c = 'TV';
            objCatalog.Catalog_Type__c = 'Bundle';
            objCatalog.Territory__c = objTerritory.Id;  
            objCatalog.Record_Status__c  = 'Active';
            objCatalog.Status__c = 'Draft';
            objCatalog.Type__c = 'Master';
            insert objCatalog;
            //Create SKU Master Record
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
            objSKUMaster.Record_Status__c  = 'Active'; 
            insert objSKUMaster;
            HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Tst',objTerritory.Id,null,false);
            insert objSKUTemplate;
            //Create Promotion Catalog Record
            HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemplate.Id,false);
            insert objPromoCatalog;
            //Create Promotion SKU Record
            HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Pending','Unlocked',false);
            insert objPromoSKU;
            //Create Approval Type record
            HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test Approval type','tst obj','Approval_Status__c','Approved','Rejected','MDP CUSTOMER','Hierarchical','Submitted',false);
            insert objApprovalType;
            // Create Approval record
            HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending',objApprovalType.Id,objPromoSKU.Id,false);
            objApproval.HEP_Promotion_SKU__c = objPromoSKU.Id;
            objApproval.Record_ID__c = '8989';
            insert objApproval;
            test.startTest();
            HEP_MDP_ProductApprovals.getApprovalHierarchy('8989');
            test.stopTest();
        }
        public static testmethod void getApprovalHierarchy_Test1(){
            //HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
            //Create Test User
            User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
            //Create HEP Territory Record
            HEP_Territory__c objTerritory1 = new HEP_Territory__c();
            objTerritory1.Name = 'US';
            objTerritory1.Region__c = 'APAC';
            objTerritory1.Type__c = 'Subsidiary';
            objTerritory1.CurrencyCode__c = 'USD';
            objTerritory1.ERMTerritoryCode__c = 'Test Code122';
            objTerritory1.Territory_Code_Integration__c = '001';
            objTerritory1.MM_Territory_Code__c = 'abc';
            insert objTerritory1;
            //Create HEP Territory Record
            HEP_Territory__c objTerritory = new HEP_Territory__c();
            objTerritory.Name = 'canada';
            objTerritory.Region__c = 'APAC';
            objTerritory.Type__c = 'Subsidiary';
            objTerritory.CurrencyCode__c = 'AUD';
            objTerritory.ERMTerritoryCode__c = 'Test Code';
            objTerritory.Territory_Code_Integration__c = '000';
            objTerritory.MM_Territory_Code__c = 'a';
            insert objTerritory;
            //Create Promotion Dating Matrix record
            HEP_Promotions_DatingMatrix__c objPromoDatingMtrx = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
            objPromoDatingMtrx.Media_Type__c = 'Digital';
            insert objPromoDatingMtrx;
            //Create Promotion Dating Matrix record
            HEP_Promotions_DatingMatrix__c objPromoDatingMtrx1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
            objPromoDatingMtrx1.Media_Type__c = 'Physical';
            insert objPromoDatingMtrx1;
            //Create HEP Customer 
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            //Create Promotion Record
            HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
            objPromotion1.PromotionName__c = 'Promo';
            objPromotion1.Promotion_Type__c  = 'Customer';
            objPromotion1.Record_Status__c  = 'Active';
            objPromotion1.Requestor__c = u.Id;
            objPromotion1.Customer__c = objCustomer.Id;
            objPromotion1.Record_Status__c = 'Active';
            insert objPromotion1;
            //Create Catalog Record
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.Catalog_Name__c = 'Primary Catalog';
            objCatalog.Product_Type__c = 'TV';
            objCatalog.Catalog_Type__c = 'Bundle';
            objCatalog.Territory__c = objTerritory.Id;  
            objCatalog.Record_Status__c  = 'Active';
            objCatalog.Status__c = 'Draft';
            objCatalog.Type__c = 'Master';
            insert objCatalog;
            //Create SKU Master Record
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
            objSKUMaster.Record_Status__c  = 'Active'; 
            insert objSKUMaster;
            HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Tst',objTerritory.Id,null,false);
            insert objSKUTemplate;
            //Create Promotion Catalog Record
            HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemplate.Id,false);
            insert objPromoCatalog;
            //Create Promotion SKU Record
            HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Pending','Unlocked',false);
            insert objPromoSKU;
            //Create Approval Type record
            HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test Approval type','tst obj','Approval_Status__c','Approved','Rejected','MDP CUSTOMER','Hierarchical','Submitted',false);
            insert objApprovalType;
            // Create Approval record
            HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending',objApprovalType.Id,objPromoSKU.Id,false);
            objApproval.HEP_Promotion_SKU__c = objPromoSKU.Id;
            objApproval.Record_ID__c = '8989';
            insert objApproval;
            //Role for approver
            HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('HEP_MARKETING_MANAGER','',false);
            insert objRole;
            //Create Record Approver 
            HEP_Record_Approver__c objRecApprover = HEP_Test_Data_Setup_Utility.createHEPRecordApprover(objApproval.Id,u.Id,false);
            objRecApprover.Approver_Role__c = objRole.Id;
            insert objRecApprover;
            test.startTest();
            HEP_MDP_ProductApprovals.getApprovalHierarchy('8989');
            test.stopTest();
        }
        
    }