/*------------------------------------Fox HEP----------------------------------
This class handles the business logic for inbound REST from Prophet for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
public class HEP_TitleReleases_Prophet_Inbound implements HEP_IntegrationInterface {
    public HEP_TitleReleases_Prophet_Inbound() {

    }
    
    /**
    * @param objWrap - Integration Transaction Response Object
    * @return void 
    */
    public void performTransaction(HEP_InterfaceTxnResponse objWrap) {
    	TitleReleasesProphetRequest req = (TitleReleasesProphetRequest) JSON.deserialize(objWrap.sRequest, TitleReleasesProphetRequest.class);

    	// Split territories into a set of unique territories
		List<String> terrList = req.territories.split(','); 
		Set<String> terrSet = new Set<String>(); 
		terrSet.addAll(terrList);  

		List<HEP_Promotion_Dating__c> pdList = [SELECT 	Id, Date__c, Language__r.Name, Language__r.Code__c, Date_Type__c, Record_Status__c,
														FAD_Approval_Status__c, Locked_Status__c, Language__r.Prophet_Language_Id__c, Channel__c,
														HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, 
														HEP_Catalog__r.Title_EDM__r.FOX_VERSION_ID__c,
														HEP_Catalog__r.Title_EDM__r.Name,
														HEP_Catalog__r.CatalogId__c,
														Territory__r.Territory_Code__c, Territory__r.Send_to_Prophet__c,
														Territory__r.Name, Territory__r.Territory_Code_Integration__c 

												FROM 	HEP_Promotion_Dating__c 
												WHERE 	HEP_Catalog__c != NULL AND 
														HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c = :req.finTitleId AND 
														Territory__r.Territory_Code_Integration__c IN :terrSet AND 
														//Status__c = 'Confirmed' AND 
														Territory__r.Send_to_Prophet__c = true AND 
														Customer__c = NULL AND 
														Date__c != NULL AND 
														DateType__c LIKE '%Release Date%'];

		List<TitleReleases> trList = new List<TitleReleases>(); 
		for (HEP_Promotion_Dating__c hpd : pdList) {
			TitleReleases tr = new TitleReleases(	hpd.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
													hpd.Id,
													hpd.HEP_Catalog__r.CatalogId__c,
													hpd.Territory__r.Name,
													hpd.Territory__r.Territory_Code_Integration__c, 
													hpd.Channel__c,
													hpd.Language__r.Name,
													hpd.Language__r.Prophet_Language_Id__c,
													hpd.Date__c,
													hpd.HEP_Catalog__r.Title_EDM__r.FOX_VERSION_ID__c,
													hpd.HEP_Catalog__r.Title_EDM__r.Name,
													hpd.Locked_Status__c,
													hpd.Record_Status__c,
													hpd.Language__r.Code__c);
													
			trList.add(tr); 
		}	

		RestContext.response.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(trList));
		objWrap.sResponse = JSON.serialize(trList); 
    }

 //   private static Map<String,String> channelMap = new Map<String,String> {
	//	'PVOD Release Date' 				=> 'PVOD',
	//	'EST Release Date' 					=> 'DHD',
	//	'VOD Release Date' 					=> 'VOD',
	//	'Physical Release Date (Retail)' 	=> 'RETAIL',
	//	'Physical Release Date (Kiosk)' 	=> 'KIOSK',
	//	'Physical Release Date (Rental)' 	=> 'RENTAL',
	//	'SPEST Release Date' 				=> 'SPDHD',
	//	'PEST Release Date' 				=> 'PDHD',
	//	'SPVOD Release Date' 				=> 'SPVOD'
	//}; 

	public class TitleReleases {
		public String finTitleId; 
		public String recordId;  
		public String territoryCode; 
		public String channel; 
		public String releaseDate; 
		public String deletedFlag; 
		public String versionId; 
		public String releaseDateLocked; 
		public String languageId; 
		public String languageCode;
		public String active; 
		
		public TitleReleases(String finTitleId, Id recordId, String ctlgId, String territoryName, String territoryCode, String channel, String languageName, String languageId,
							Date releaseDate, String versionId, String titleName, String releaseDateLocked, String active, String languageCode) {
			this.recordId = String.valueOf(recordId); 
			this.finTitleId = finTitleId; 
			//if (channelMap.get(channel) != null) {
			//	this.channel = channelMap.get(channel);
			//}
			//else {
			//	this.channel = channel; 
			//}
			this.channel = channel; 
			this.releaseDate = String.valueOf(releaseDate);
			this.releaseDateLocked = String.valueOf((releaseDateLocked == HEP_Constants__c.getValues('HEP_PROMODATING_STATUS_LOCKED').Value__c) ? true : false); 
			this.deletedFlag = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_DELETED').Value__c) ? true : false); 
			this.versionId = versionId; 
			this.territoryCode = territoryCode; 
			this.active = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c) ? true : false);

			if (this.territoryCode == 'BE' || this.territoryCode == 'CH') { // Belgium or Switz 
				this.languageCode = languageCode;
				this.languageId = languageId; 
			}
			else {
				this.languageCode = '';
				this.languageId = ''; 
			}
		}
	}

	public class TitleReleasesProphetRequest {
		public String finTitleId;
		public String territories; 

		public TitleReleasesProphetRequest(String finTitleId, String territories) {
			this.finTitleId = finTitleId; 
			this.territories = territories; 
		}
	}
}