@isTest
public class HEP_INT_DheSkuBom_Details_Test {

    @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_SKU_BOM__c objSKUBOM=New HEP_SKU_BOM__c();
        objSKUBOM.Components_SKU_Number__c='2341811';
        objSKUBOM.Parent_SKU_Number__c='2354996';
        objSKUBOM.Unique_SKU_Number__c=objSKUBOM.Parent_SKU_Number__c+ objSKUBOM.Components_SKU_Number__c;
        insert objSKUBOM;
    }
    
    @isTest 
    static void Success_Test(){           
        Test.startTest();      
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180413","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuBom_Details demo = new HEP_INT_DheSkuBom_Details();
        system.debug('endpoint1');
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    } 
    
 	 @isTest 
    static void Failure_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        HEP_INT_DheSkuInvoiceWrapperUtility objDheSkuInvoiceWrapperUtility=new HEP_INT_DheSkuInvoiceWrapperUtility();
        objDheSkuInvoiceWrapperUtility.sStartingFrom='1';
        objDheSkuInvoiceWrapperUtility.sTimeStamp='080000';
        objDheSkuInvoiceWrapperUtility.sNoOfRows='1000';
        objDheSkuInvoiceWrapperUtility.sLastPulledDate='20180413';
        objTxnResponse.sSourceId = '{"sTimeStamp":"080000","sLastPulledDate":"20180413","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuBom_Details demo = new HEP_INT_DheSkuBom_Details();
        system.debug('endpoint2');
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }  
    
    @isTest 
    static void Access_Token_Failure_Test(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_JDE_E1_OAUTH']; 
        delete objConstant;      
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTimeStamp":"090000","sLastPulledDate":"20180413","sNoOfRows":"1000","sStartingFrom":"1"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_INT_DheSkuBom_Details demo = new HEP_INT_DheSkuBom_Details();
        system.debug('endpoint3');
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }                  
}