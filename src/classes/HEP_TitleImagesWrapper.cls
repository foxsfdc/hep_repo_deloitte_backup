/**
* HEP_TitleImagesWrapper -- Wrapper to store Rating details 
* @author    Lakshman Jinnuri
*/
public class HEP_TitleImagesWrapper{

    public String EventName;    
    public String EventType;    
    public String ProducerId;   
    public Data Data;
    public String calloutStatus;
    public String TxnId;
    public List<Errors> errors;
    
    /**
    * Data -- Class to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Payload Payload;
    }
    
    /**
    * Payload -- Class to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class Payload{
    public Event event;
    }

    /**
    * Event -- Class to store Event details 
    * @author    Lakshman Jinnuri
    */
    public class Event{
        public String STATUS;   
        public String USERNAME; 
        public String sTIME; 
        public AssetInfo ASSET_INFO;
        public SecurityPolicies SECURITY_POLICIES;
        public Integer PAYLOAD_ID;  
    }       
    
    /**
    * AssetInfo -- Class to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class AssetInfo {
        public String ASSET_ID; 
        public String MEDIA_ID; 
        public String FILE_NAME;    
        public String FILE_SIZE;    
        public String CONTENT_TYPE; 
        public String VERSION;  
        public String WPR_ID;   
    }
        
    /**
    * SecurityPolicies -- Class to store SecurityPolicies details 
    * @author    Lakshman Jinnuri
    */
    public class SecurityPolicies {
        public List<String> POLICY;
        public String FILTER;   
    }
    
    /**
    * Errors -- Class to store Errors details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status; //400
    }

    /**
    * SessionResource -- Class to store SessionResource details 
    * @author    Lakshman Jinnuri
    */    
    public SessionResource session_resource;
    public class SessionResource {
        public Session session;
    }
    
    /**
    * Session -- Class to store Session details 
    * @author    Lakshman Jinnuri
    */
    public class Session {
        public String domain_name;  
        public Integer id;  
        public boolean local_session;
        public String login_name;   
        public String message_digest;   
        public String role_name;    
        public String user_full_name;   
        public String user_id;  
        public Integer user_role_id;  
        public Integer validation_key; 
    }
}