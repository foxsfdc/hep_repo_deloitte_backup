/**
 * HEP_PromotionProductTriggerHandler --- Handler class for HEP_PromotionProductTrigger
 * @author  Nidhin V K
 */
public with sharing class HEP_PromotionProductTriggerHandler extends TriggerHandler{
    

    public static String sAPPROVED, sPUBLISHED, sACTIVE, sSUBMITTED, sDELETED, sDRAFT, sCUSTOMER;
    public static String sNATIONAL, sREJECTED;
    public Static String sHEP_Test_Apple_ID, INCLUDED_TERRITORIES_ITUNES;

    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');      
        sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
        sDRAFT = HEP_Utility.getConstantValue('HEP_CATALOG_STATUS_DRAFT');
        sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sPUBLISHED = HEP_Utility.getConstantValue('HEP_PROMOTION_PUBLISHED');
        sSUBMITTED = HEP_Utility.getConstantValue('HEP_PROMOTION_SUBMITTED');
        sCUSTOMER = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
        sNATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        sREJECTED = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
        INCLUDED_TERRITORIES_ITUNES = HEP_Utility.getConstantValue('HEP_ITUNES_INCLUDED_TERRITORIES');
        sHEP_Test_Apple_ID = HEP_Utility.getConstantValue('HEP_Test_Apple_ID');
    }
   
    
    /**
    * afterInsert --- all after insert logic
    * @param nothing
    * @exception Any exception
    * @return void
    */
     public override void afterInsert(){
        List<HEP_Promotion__c> lstPromotionsToUpdate = new List<HEP_Promotion__c>();
        //updates promotion approval status
        lstPromotionsToUpdate.addAll(updatePromotionApprovalStatus(NULL, (Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.newMap));//updates report status
        //updates report status
        lstPromotionsToUpdate.addAll(updateReportStatus(NULL, (Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.newMap));
        if(lstPromotionsToUpdate.size() > 0)
            update lstPromotionsToUpdate;
  
     }
     
    /**
    * afterUpdate --- all after update logic
    * @param
    * @exception Any exception
    * @return void
    */ 
    public override void afterUpdate(){
        List<String> testAppleIdsList = new List<String>();
        String sCustomer_Apple = HEP_Utility.getConstantValue('Customer_iTunes');
        sCustomer_Apple = '%' + sCustomer_Apple + '%';
        List<HEP_Promotion__c> lstPromotionsToUpdate = new List<HEP_Promotion__c>();
        List<HEP_MDP_Promotion_Product_Detail__c> lstPromotions_Details = new List<HEP_MDP_Promotion_Product_Detail__c>();
        Map<id,HEP_MDP_Promotion_Product__c> mapUpdatedPromotionProd = new Map<id,HEP_MDP_Promotion_Product__c>();
        Map<id,list<HEP_MDP_Promotion_Product_Detail__c>>  mapPromotionsId_ProDetails = new Map<id,list<HEP_MDP_Promotion_Product_Detail__c>>();
        //updates promotion approval status
        lstPromotionsToUpdate.addAll(updatePromotionApprovalStatus((Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.oldMap, (Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.newMap));
        //updates report status
        lstPromotionsToUpdate.addAll(updateReportStatus((Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.oldMap, (Map<Id, HEP_MDP_Promotion_Product__c>) Trigger.newMap));
        if(lstPromotionsToUpdate.size() > 0)
            update lstPromotionsToUpdate;
        // check if Start/End date is changed       
        
       if (!String.isBlank(sHEP_Test_Apple_ID)) {
           testAppleIdsList = sHEP_Test_Apple_ID.Split(',');  
       }        
        
        Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_MDP_Promotion_Product__c', 'HEP_MDP_Date_Update');
        //sAPPLE = HEP_Utility.getConstantValue('Customer_iTunes');
        //sI_TUNES = HEP_Utility.getConstantValue('HEP_CUSTOMER_APPLE');

        System.debug('THE VALUE FOR THE FLAG FIELD------ ' + checkFieldUpdate);
       //Exclude territories based on territory names in constants
        List<String> includedTerritoriesStringList = new List<String>();
        List<String> includedTerritoryIdsString = new List<String>();
        includedTerritoriesStringList = (!String.isBlank(INCLUDED_TERRITORIES_ITUNES))?INCLUDED_TERRITORIES_ITUNES.Split(','):new List<String>();
        if(!includedTerritoriesStringList.isEmpty()){
            for(HEP_Territory__c territory:[SELECT id,Name FROM HEP_Territory__c WHERE Flag_Country_Code__c IN: includedTerritoriesStringList]){
                includedTerritoryIdsString.add(territory.Id);
            }
        }

        System.debug('includedTerritoryIdsString------ '+includedTerritoryIdsString);
        if(checkFieldUpdate){
            lstPromotions_Details =[SELECT id,Promo_SRP__c,Format__c,Channel__c,Record_Status__c,HEP_Promotion_MDP_Product__c,Promo_WSP__c,HEP_Promotion_MDP_Product__r.Approval_Status__c, HEP_Promotion_MDP_Product__r.HEP_Sent_to_iTunes__c, HEP_MDP_Product_Mapping__r.Adam_Id__c, HEP_Promotion_MDP_Product__r.Record_Status__c, HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c FROM HEP_MDP_Promotion_Product_Detail__c 
                                    WHERE Channel__c =: HEP_Utility.getConstantValue('HEP_CHANNEL_EST') AND (Format__c =: HEP_Utility.getConstantValue('HEP_HD') OR 
                                    Format__c =:HEP_Utility.getConstantValue('HEP_SD')) AND HEP_Promotion_MDP_Product__c =: Trigger.newMap.Keyset() AND ( HEP_Promotion_MDP_Product__r.Approval_Status__c =: HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED') OR (HEP_Promotion_MDP_Product__r.HEP_Sent_to_iTunes__c = true AND HEP_Promotion_MDP_Product__r.Record_Status__c = 'Deleted') )AND HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c LIKE :sCustomer_Apple
                                    AND HEP_Promotion_MDP_Product__r.Product_Type__c =: HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_FILM') AND HEP_Promotion_MDP_Product__r.Title_Type__c=: HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_FILM') AND (HEP_Promotion_MDP_Product__r.HEP_Territory__c IN: includedTerritoryIdsString OR HEP_Promotion_MDP_Product__r.HEP_Territory__r.Parent_Territory__c IN: includedTerritoryIdsString)];
                
            for(HEP_MDP_Promotion_Product_Detail__c obj:lstPromotions_Details){
                if(String.isNotBlank(obj.HEP_MDP_Product_Mapping__r.Adam_Id__c) ){
                    if(mapPromotionsId_ProDetails.Containskey(obj.HEP_Promotion_MDP_Product__c)){
                        mapPromotionsId_ProDetails.get(obj.HEP_Promotion_MDP_Product__c).add(obj);
                    }
                    else{
                        if (!testAppleIdsList.isEmpty()) {
                            //If there are test apple ids configured, only add apple ids which are listed in the test Ids to be sent to iTunes interface
                            system.debug('testAppleIdsList ' + testAppleIdsList);
                            if (testAppleIdsList.contains(obj.HEP_MDP_Product_Mapping__r.Adam_Id__c)) {
                                system.debug('valid test ID');

                                mapPromotionsId_ProDetails.put(obj.HEP_Promotion_MDP_Product__c, new List<HEP_MDP_Promotion_Product_Detail__c> {obj});
                            }

                        }else{
                            system.debug('Actual ADAM ID');
                            mapPromotionsId_ProDetails.put(obj.HEP_Promotion_MDP_Product__c, new List<HEP_MDP_Promotion_Product_Detail__c> {obj});
                        }

                        //mapPromotionsId_ProDetails.put(obj.HEP_Promotion_MDP_Product__c,new List<HEP_MDP_Promotion_Product_Detail__c>{obj});
                    }   
                }
            }   
            
            for(String Pro_id: mapPromotionsId_ProDetails.keyset()){
                //getting an exception if one of the values were missing- EST SD or EST HD
                if(mapPromotionsId_ProDetails.get(Pro_id).size()>1)
                    mapUpdatedPromotionProd.put(Pro_id,(HEP_MDP_Promotion_Product__c)Trigger.newMap.get(Pro_id));
            }
            System.debug('++++mapUpdatedPromotion++++++'+mapUpdatedPromotionProd);

           if(mapUpdatedPromotionProd !=NULL && !mapUpdatedPromotionProd.isEmpty()){
               if(HEP_CheckRecursive.runOnce()){
                   Database.executeBatch(new HEP_ItunesCallOut_Batch((Map<Id, HEP_MDP_Promotion_Product__c>)  mapUpdatedPromotionProd),1); 
               }  
            }    
        }
    }
    
    
    /**
    * updatePromotionApprovalStatus --- updates the Promotion Approval Status based on the product approval status
    * @param Map<Id, HEP_Market_Spend__c> newMap
    * @exception Any exception
    * @return List<HEP_Promotion__c>
    */
    public static List<HEP_Promotion__c> updatePromotionApprovalStatus(Map<Id, HEP_MDP_Promotion_Product__c> oldMap, Map<Id, HEP_MDP_Promotion_Product__c> newMap){
        try{
            System.debug('oldMap>>' + Trigger.oldMap);
            System.debug('newMap>>' + Trigger.newMap);
            Set<Id> setPromotionIds = new Set<Id>();
            Set<Id> setRejectedPromoIds = new Set<Id>();
            for(HEP_MDP_Promotion_Product__c objProduct : newMap.values()){
                if(oldMap == NULL || oldMap.size() == 0){
                    setPromotionIds.add(objProduct.HEP_Promotion__c);
                } else if(oldMap != NULL
                    && (objProduct.Record_Status__c != oldMap.get(objProduct.Id).Record_Status__c
                    || objProduct.Approval_Status__c != oldMap.get(objProduct.Id).Approval_Status__c))
                    setPromotionIds.add(objProduct.HEP_Promotion__c);
                if(objProduct.Approval_Status__c == sREJECTED)
                    setRejectedPromoIds.add(objProduct.HEP_Promotion__c);
            }
            System.debug('setPromotionIds>>' + setPromotionIds);
            if(setPromotionIds.isEmpty())
                return new List<HEP_Promotion__c>();
            List<HEP_Promotion__c> lstPromotionsToUpdate = new List<HEP_Promotion__c>();
            Set<String> setPromoChildRecords = new Set<String>();
            for(HEP_Promotion__c objPromotion : [SELECT
                                                    Id, Status__c, Promotion_Type__c,Submitted_Date__c,
                                                    (SELECT
                                                        Id, Approval_Status__c
                                                    FROM
                                                        HEP_Promotion_MDP_Products__r
                                                    WHERE
                                                        Record_Status__c = :sACTIVE)
                                                FROM
                                                    HEP_Promotion__c
                                                WHERE
                                                    Id IN :setPromotionIds
                                                AND
                                                    Record_Status__c = :sACTIVE]){
                Boolean bIsAllApproved = true, bIsSubmitted = false;
                for(HEP_MDP_Promotion_Product__c objProduct : objPromotion.HEP_Promotion_MDP_Products__r){
                    setPromoChildRecords.add(objPromotion.Id);
                    System.debug('objProduct.Approval_Status__c>>' + objProduct.Approval_Status__c);
                    if(objProduct.Approval_Status__c != sAPPROVED)
                        bIsAllApproved = false;
                    if(objProduct.Approval_Status__c == sSUBMITTED)
                        bIsSubmitted = true;
                }
                System.debug('bIsAllApproved>>' + bIsAllApproved);
                System.debug('objPromotion.Status__c before>>' + objPromotion.Status__c);
                System.debug('objPromotion>>' + objPromotion);
                
                if(!setPromoChildRecords.contains(objPromotion.Id)){
                    if(objPromotion.Status__c != sDRAFT){
                        objPromotion.Status__c = sDRAFT;
                        lstPromotionsToUpdate.add(objPromotion);
                    }
                } else if(!setRejectedPromoIds.contains(objPromotion.Id)){
                    if(bIsAllApproved 
                        && ((objPromotion.Status__c != sPUBLISHED && objPromotion.Promotion_Type__c == sNATIONAL)
                        || (objPromotion.Status__c != sAPPROVED && objPromotion.Promotion_Type__c == sCUSTOMER))){
                        
                        objPromotion.Status__c = (objPromotion.Promotion_Type__c == sCUSTOMER)? sAPPROVED : sPUBLISHED;
                        lstPromotionsToUpdate.add(objPromotion);
                    } else if(!bIsAllApproved 
                        && ((objPromotion.Status__c != sSUBMITTED && bIsSubmitted)
                        || (objPromotion.Status__c != sDRAFT && !bIsSubmitted))){
                            
                        objPromotion.Status__c = bIsSubmitted ? sSUBMITTED : sDRAFT;
                        if(objPromotion.Status__c == sSUBMITTED && objPromotion.Submitted_Date__c == null){
                            objPromotion.Submitted_Date__c = Date.Today();
                        }
                        lstPromotionsToUpdate.add(objPromotion);
                    }
                } else if(setRejectedPromoIds.contains(objPromotion.Id)){
                    objPromotion.Status__c = sREJECTED;
                    lstPromotionsToUpdate.add(objPromotion);
                }
                    
                System.debug('objPromotion.Status__c after>>' + objPromotion.Status__c);
            }
            System.debug('lstPromotionsToUpdate>>' + lstPromotionsToUpdate);
            return lstPromotionsToUpdate;
        } catch(Exception ex){
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_PromotionProductTriggerHandler','updatePromotionApprovalStatus',null,null); 
            
            System.debug('Exception on Class : HEP_PromotionProductTriggerHandler - updatePromotionApprovalStatus, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            throw ex;
        }
    }
    
    /**
    * updateReportStatus --- updates the customer promotion report Status
    * @param Map<Id, HEP_MDP_Promotion_Product__c> oldMap, Map<Id, HEP_MDP_Promotion_Product__c> newMap
    * @exception Any exception
    * @return List<HEP_Promotion__c>
    */
    public static List<HEP_Promotion__c> updateReportStatus(Map<Id, HEP_MDP_Promotion_Product__c> oldMap, Map<Id, HEP_MDP_Promotion_Product__c> newMap){
        try{
            System.debug('oldMap>>' + Trigger.oldMap);
            System.debug('newMap>>' + Trigger.newMap);
            Map<Id, HEP_Promotion__c> mapPromotions = new Map<Id, HEP_Promotion__c>();
            for(HEP_MDP_Promotion_Product__c objNewProduct : [SELECT 
                                                                        Id, HEP_Promotion__c, HEP_Promotion__r.Suisse_Updated__c,
                                                                        Product_Start_Date__c, Product_End_Date__c, Duration__c
                                                                    FROM 
                                                                        HEP_MDP_Promotion_Product__c 
                                                                    WHERE 
                                                                        Id IN :newMap.keySet()
                                                                    AND
                                                                        Record_Status__c IN (:sACTIVE, :sDELETED)]){
                HEP_MDP_Promotion_Product__c objOldProduct = (oldMap == NULL) ? NULL : oldMap.get(objNewProduct.Id);
                if((oldMap == NULL
                    || objOldProduct.Product_Start_Date__c != objNewProduct.Product_Start_Date__c
                    || objOldProduct.Product_End_Date__c != objNewProduct.Product_End_Date__c
                    || objOldProduct.Duration__c != objNewProduct.Duration__c)
                    && objNewProduct.HEP_Promotion__r.Suisse_Updated__c)
                    
                    mapPromotions.put(objNewProduct.HEP_Promotion__c, 
                        new HEP_Promotion__c(Id = objNewProduct.HEP_Promotion__c, Suisse_Updated__c = false));
            }
            return mapPromotions.values();
        } catch(Exception ex){
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_PromotionProductTriggerHandler','updateReportStatus',null,null); 
            
            System.debug('Exception on Class : HEP_PromotionProductTriggerHandler - updateReportStatus, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            throw ex;
        }
    }  
       
}