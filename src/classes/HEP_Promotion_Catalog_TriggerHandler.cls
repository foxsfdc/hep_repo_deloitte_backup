/**
 *HEP_Promotion_Catalog_TriggerHandler --- Promotion Catalog Object trigger Handler
                                This will run every time a promotion catalog record is inserted and updated.
 */
Public Class HEP_Promotion_Catalog_TriggerHandler extends TriggerHandler{
    public static Set < Id > setAdditionApprovers;
    public static List < Id > lstAdditionApprovers;
    public static Set < Id > setDeletionApprovers;
    public static List < Id > lstDeletionApprovers;
    public static Set < Id > setApprovers;
    public static List < Id > lstApprovers;
    public static List <HEP_Promotion_Catalog__c > lstPromotionCatalogAddition;
    public static List <HEP_Promotion_Catalog__c> lstPromotionCatalogDeletion;
    public static List <HEP_Promotion__c> lstLocalPromotionsForAddition;
    public static List <HEP_Promotion__c> lstLocalPromotionsForDeletion;
    public static String sAdditionTemplate;
    public static String sDeletionTemplate;
    public static String sPromotionCatalogObject;
    public static String sGlobal;
    public static String sApproved;
    public static String sActive;
    public static String sDeleted;
    public static Boolean run = true;
    

    
    
    static {
        sAdditionTemplate = HEP_Utility.getConstantValue('HEP_Global_Promotion_Catalog_Addition');
        sDeletionTemplate = HEP_Utility.getConstantValue('HEP_Global_Promotion_Catalog_Deletion');
        sPromotionCatalogObject = HEP_Utility.getConstantValue('HEP_Promotion_Catalog');
        sGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL'); 
        sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sDeleted = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');  
    }
    /**
     *Class Constructor to initialise class variables
     *@return nothing
     */

    public HEP_Promotion_Catalog_TriggerHandler() {
        setAdditionApprovers = new Set < Id > ();
        lstAdditionApprovers = new List < Id > ();
        setDeletionApprovers = new Set < Id > ();
        lstDeletionApprovers = new List < Id > ();
        lstApprovers = new List < Id > ();
        setApprovers = new Set < Id > ();
        lstPromotionCatalogAddition = new List < HEP_Promotion_Catalog__c > ();
        lstPromotionCatalogDeletion = new List < HEP_Promotion_Catalog__c > ();
        lstLocalPromotionsForAddition = new List < HEP_Promotion__c > ();
        lstLocalPromotionsForDeletion = new List < HEP_Promotion__c > ();
    }
    
    /**
     * afterUpdate-- Context specific beforeUpdate method
     * @return nothing
     */

    public override void afterUpdate() {
        if(Trigger.oldMap != null){
            System.debug('New Map ------> ' + Trigger.newMap);
            System.debug('Old Map ------> ' + Trigger.oldMap);
            AlertsAfterUpsert((List < HEP_Promotion_Catalog__c > ) Trigger.new);
        }
            
    }
    /**
     * AlertsAfterUpsert -- Create notification whenever a catalog is added or deleted from the global promotion
     * @param List of promotion catalog records after change and map of promotion catalog after change
     * @return nothing
     */

    public static void AlertsAfterUpsert(List < HEP_Promotion_Catalog__c > lstHepPromotionCatalog) {
        setAdditionApprovers = new Set < Id > ();
        lstAdditionApprovers = new List < Id > ();
        setDeletionApprovers = new Set < Id > ();
        lstDeletionApprovers = new List < Id > ();
        lstPromotionCatalogAddition = new List < HEP_Promotion_Catalog__c > ();
        lstPromotionCatalogDeletion = new List < HEP_Promotion_Catalog__c > ();
        lstLocalPromotionsForAddition = new List < HEP_Promotion__c > ();
        lstLocalPromotionsForDeletion = new List < HEP_Promotion__c > ();
        System.debug('Intrigger+++++++++++');
        //setPromotionCatalogIds:set to store promotion catalog record id's
        set < id > setPromotionCatalogIds = new set < id > ();
        //Populate the Values in Set with the promotion catalog record id
        setPromotionCatalogIds = new Map<Id, HEP_Promotion_Catalog__c>(lstHepPromotionCatalog).keySet();
        lstPromotionCatalogAddition = [SELECT Promotion__r.Promotion_Type__c,Catalog__r.Status__c,Promotion__c,Promotion__r.LocalPromotionCode__c,Promotion__r.PromotionName__c
                    FROM HEP_Promotion_Catalog__c
                    WHERE Id IN: setPromotionCatalogIds AND Record_Status__c =: sActive AND Promotion__r.Promotion_Type__c =: sGlobal AND Catalog__r.Status__c =: sApproved
        ];
        lstPromotionCatalogDeletion = [SELECT Promotion__r.Promotion_Type__c,Catalog__r.Status__c,Promotion__c,Promotion__r.LocalPromotionCode__c,Promotion__r.PromotionName__c
                    FROM HEP_Promotion_Catalog__c
                    WHERE Id IN: setPromotionCatalogIds AND Record_Status__c =: sDeleted AND Promotion__r.Promotion_Type__c =: sGlobal AND Catalog__r.Status__c =: sApproved
        ];
        system.debug('lstPromotionCatalogAddition----->' + lstPromotionCatalogAddition);
        system.debug('lstPromotionCatalogDeletion----->' + lstPromotionCatalogDeletion);
        if(!lstPromotionCatalogAddition.isEmpty()){
            lstLocalPromotionsForAddition = [SELECT Domestic_Marketing_Manager__c
                    FROM HEP_Promotion__c
                    WHERE Global_Promotion__c =: lstPromotionCatalogAddition[0].Promotion__c AND Record_Status__c =: sActive
            ];
            system.debug('lstLocalPromotionsForAddition----->' + lstLocalPromotionsForAddition);
        }
        if(!lstPromotionCatalogDeletion.isEmpty()){
            lstLocalPromotionsForDeletion = [SELECT Domestic_Marketing_Manager__c
                    FROM HEP_Promotion__c
                    WHERE Global_Promotion__c =: lstPromotionCatalogDeletion[0].Promotion__c AND Record_Status__c =: sActive
            ];
            system.debug('lstLocalPromotionsForDeletion----->' + lstLocalPromotionsForDeletion);
        }   
        if(!lstLocalPromotionsForAddition.isEmpty()){
            for (HEP_Promotion__c objPromotion : lstLocalPromotionsForAddition){
                setAdditionApprovers.add(objPromotion.Domestic_Marketing_Manager__c);
            }           
            system.debug('setAdditionApprovers-----' + setAdditionApprovers);
        }   
        lstAdditionApprovers.addAll(setAdditionApprovers);
        if(!lstLocalPromotionsForDeletion.isEmpty()){
            for (HEP_Promotion__c objPromotion : lstLocalPromotionsForDeletion){
                setDeletionApprovers.add(objPromotion.Domestic_Marketing_Manager__c);
            }           
            system.debug('setDeletionApprovers-----' + setDeletionApprovers);
        }   
        lstDeletionApprovers.addAll(setDeletionApprovers);
        if(!lstAdditionApprovers.isEmpty() && !lstPromotionCatalogAddition.isEmpty()){
            HEP_Notification_Utility.createNotificationAlerts(lstPromotionCatalogAddition,sAdditionTemplate,lstAdditionApprovers);
        }   
        if(!lstDeletionApprovers.isEmpty() && !lstPromotionCatalogDeletion.isEmpty()){
            HEP_Notification_Utility.createNotificationAlerts(lstPromotionCatalogDeletion,sDeletionTemplate,lstDeletionApprovers);
        }       
    }
    
    /**
     * afterInsert -- Context specific afterInsert method
     * @return nothing
     */
    public override void afterInsert() {
        System.debug('Old Map ------> ' + Trigger.newMap);
        System.debug('Old Map ------> ' + Trigger.oldMap);
        AlertsAfterUpsert((List < HEP_Promotion_Catalog__c > ) Trigger.new);
    }
    
    /**
     * runOnce -- Check Recursion
     * @return boolean
     */
    public static boolean runOnce(){
        if(run){
           run=false;
           return true;
        }else{
           return run;
        }
    }

    
}