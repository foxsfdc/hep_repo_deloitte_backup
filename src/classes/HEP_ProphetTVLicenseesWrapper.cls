/**
* HEP_ProphetTVLicenseesWrapper -- Wrapper to store TV Licensees from Prophet Interface
* @author    Lakshman Jinnuri
*/

public class HEP_ProphetTVLicenseesWrapper {
    //List to hold the TV Licensee details
    public list<TvLicensees> TvLicensees;
    public String calloutStatus;
    public String TxnId;
    public list<Errors> errors;
   
    /**
    * Attributes -- Class to hold Projected Date details 
    * @author    Lakshman Jinnuri
    */
    public class TvLicensees {
        public String customerName;
        public String customerId;
        public String territory;
        public String licenseStartDate;
        public String language;
        public String media;
        public String licenseEndDate;
    }
    
    /**
    * Errors -- Class to hold Error details 
    * @author  Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status; //400
    }

}