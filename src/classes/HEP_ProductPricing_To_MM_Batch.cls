/*
*   HEP_ProductPricing_To_MM_Batch -- Batch class to push MM data to HEP_MM_Outbound__c object
*   @author    Mayank Jaggi
*/
global class HEP_ProductPricing_To_MM_Batch implements Database.Batchable<sObject>, Database.Stateful
{
    global Integer recordsProcessed = 0;
    /*  
    *   start -- start method of the batch class, does the query
    *   @param Database.BatchableContext
    *   @return Database.QueryLocator
    *   @author Mayank Jaggi
    */
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String sPublished = HEP_Utility.getConstantValue('HEP_PROMOTION_PUBLISHED');
        String sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        String sCustomer = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
        String sNoOfDays = 'LAST_N_DAYS:'+ string.Valueof(HEP_Utility.getConstantValue('HEP_MM_7'));

        return Database.getQueryLocator(
            'select HEP_Promotion_MDP_Product__r.HEP_Catalog__c, HEP_Promotion_MDP_Product__r.HEP_Catalog__r.CatalogId__c, '+
            'Contract_ID__c, createdDate, CustomerId__c, SentToMMoutbound__c, Name, Record_Status__c, '+
            'HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c, LastModifiedDate, Channel__c,'+
            'HEP_Promotion_MDP_Product__r.HEP_Promotion__r.EndDate__c, HEP_Promotion_MDP_Product__r.HEP_Promotion__r.StartDate__c, '+
            'HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Name, Promo_WSP__c, Format__c, MM_Currency_Code__c, '+
            'HEP_Promotion_MDP_Product__r.Title_EDM__r.TITLE_SUB_TYPE_CD__c, HEP_Promotion_MDP_Product__r.HEP_Territory__r.MDP_CountryID__c, '+ 
            'HEP_Promotion_MDP_Product__r.WPR_Id__c from HEP_MDP_Promotion_Product_Detail__c ' +
            'Where HEP_Promotion_MDP_Product__r.Approval_Status__c = :sApproved  and ' +
            'HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Promotion_Type__c = :sCustomer and ' +
            'LastModifiedDate = '+ sNoOfDays
            );
    }

    /*  
    *   execute -- execute method of the batch class, does the assignment to the object fields
    *   @param Database.BatchableContext, list of HEP_MDP_Promotion_Product_Detail__c object as scope from query
    *   @return void
    *   @author Mayank Jaggi
    */
    global void execute(Database.BatchableContext bc, List<HEP_MDP_Promotion_Product_Detail__c> scope){
        try{
            List <HEP_MM_Outbound__c> lstMMOutbounds = new List <HEP_MM_Outbound__c>();
            list<HEP_MDP_Promotion_Product_Detail__c> lstUpdateSentProductDetails = new list<HEP_MDP_Promotion_Product_Detail__c>();
            System.debug('scope: ' + scope);
            System.debug('scope size ' + scope.size());
            for (HEP_MDP_Promotion_Product_Detail__c objPromoProdDetailInstance : scope)
            {
                HEP_MM_Outbound__c objMMOutboundInstance = new HEP_MM_Outbound__c();
                objMMOutboundInstance.Bundle_ID__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Catalog__r.CatalogId__c;
                objMMOutboundInstance.ContractTitleId__c = null;
                objMMOutboundInstance.Contract_Id__c = objPromoProdDetailInstance.Contract_ID__c ;
                objMMOutboundInstance.CreatedDate__c = objPromoProdDetailInstance.createdDate ;
                objMMOutboundInstance.Currency_Code__c = objPromoProdDetailInstance.MM_Currency_Code__c;
                objMMOutboundInstance.Customer_Id__c = objPromoProdDetailInstance.CustomerId__c; 
                objMMOutboundInstance.Customer_Name__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c ;
                objMMOutboundInstance.FOX_Version_ID__c = '' ;
                objMMOutboundInstance.LastUpdateDate__c = objPromoProdDetailInstance.LastModifiedDate ;
                
                if (objPromoProdDetailInstance.Channel__c == HEP_Utility.getConstantValue('HEP_CHANNEL_EST')) 
                {objMMOutboundInstance.Media_Type__c = Integer.valueOf(HEP_Utility.getConstantValue('HEP_MM_MEDIA_TYPE_36')) ;}
                else if (objPromoProdDetailInstance.Channel__c == HEP_Utility.getConstantValue('HEP_VOD'))
                {objMMOutboundInstance.Media_Type__c = Integer.valueOf(HEP_Utility.getConstantValue('HEP_MM_MEDIA_TYPE_30')) ;}
                String PromotionProductID = '';
                for(integer j = 0; j<objPromoProdDetailInstance.Name.length(); j++){
                    if(objPromoProdDetailInstance.Name.substring(j, j+1).isnumeric())
                    {PromotionProductID += objPromoProdDetailInstance.Name.substring(j, j+1);}}
                objMMOutboundInstance.PromotionProductID__c = PromotionProductID;
                objMMOutboundInstance.Product_ID__c = '';
                objMMOutboundInstance.Promo_End_Date__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.EndDate__c ;
                objMMOutboundInstance.Promo_Start_Date__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.StartDate__c ;
                objMMOutboundInstance.Promo_WSP__c = objPromoProdDetailInstance.Promo_WSP__c ;
                objMMOutboundInstance.Resolution__c = objPromoProdDetailInstance.Format__c ;
                if(objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.Title_EDM__r.TITLE_SUB_TYPE_CD__c == HEP_Utility.getConstantValue('HEP_SEASN'))
                {objMMOutboundInstance.Season_Pass__c = HEP_Utility.getConstantValue('HEP_Changed_Flag_Y');}
                else {objMMOutboundInstance.Season_Pass__c = HEP_Utility.getConstantValue('HEP_Changed_Flag_N');}

                objMMOutboundInstance.Territory_ID__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Territory__r.MDP_CountryID__c;
                objMMOutboundInstance.WPR_ID__c = objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.WPR_Id__c ;
                if (objPromoProdDetailInstance.HEP_Promotion_MDP_Product__r.HEP_Catalog__r.CatalogId__c == NULL)
                    { objMMOutboundInstance.Bundle__c = HEP_Utility.getConstantValue('HEP_Changed_Flag_N');}
                else {objMMOutboundInstance.Bundle__c = HEP_Utility.getConstantValue('HEP_Changed_Flag_Y');}
                if (objPromoProdDetailInstance.Record_Status__c == HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE') || objPromoProdDetailInstance.Record_Status__c == HEP_Utility.getConstantValue('HEP_RECORD_DELETED') )
                {
                    objMMOutboundInstance.Record_Type__c = 'D';
                }
                else if(objPromoProdDetailInstance.SentToMMoutbound__c == true){
                    objMMOutboundInstance.Record_Type__c = 'U';
                }else{
                    objMMOutboundInstance.Record_Type__c = 'I';
                }
                lstMMOutbounds.add(objMMOutboundInstance);

                objPromoProdDetailInstance.SentToMMoutbound__c = true;
                lstUpdateSentProductDetails.add(objPromoProdDetailInstance);
            }

            Schema.SObjectField assetExternId = HEP_MM_Outbound__c.PromotionProductID__c;     
            Database.UpsertResult[] assetResults =  Database.upsert(lstMMOutbounds, assetExternId, true); 
            System.debug('upserted record');
            recordsProcessed++;

            Database.update(lstUpdateSentProductDetails);
        
        }
        catch(Exception exc){
            System.debug('exception while entering data: ' + exc);
        }
    }

    /*  
    *   finish -- finish method of the batch class, does the query to know job status
    *   @param Database.BatchableContext,
    *   @return void
    *   @author Mayank Jaggi
    */
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed.');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM
        AsyncApexJob WHERE Id = :bc.getJobId()];
    }    
}