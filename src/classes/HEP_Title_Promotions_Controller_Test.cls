/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest(seeAllData = false)
public class HEP_Title_Promotions_Controller_Test {
    @testSetup
    public static void testSetup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_List_Of_Values__c> lstListOfValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
        User user1 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@gmail.com','Vishnu','Vishnr','V','',true);
        
    }
    
    @isTest
    public static void testTitlePromoFunc(){
        // list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        // list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        // //List<HEP_List_Of_Values__c> lstListOfValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
        User user1 = [SELECT Id from User Where Name LIKE '%Vishnu%' LIMIT 1];
        
        HEP_Role__c marketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
        HEP_Territory__c gGermany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary' , null,null, 'EUR', 'DE', '182', 'ESCO', true);
		HEP_User_Role__c VRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(gGermany.id, marketingManager.id, user1.id, true);
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        
        EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
        EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('X-Men', '32434', 'CNFDL', prodType.id, true);
        
        
        HEP_Promotion__c testPromo1 = HEP_Test_Data_Setup_Utility.createPromotion('X-Men 3', 'National', null, gGermany.id,FoxNewRelease.id ,'', '', false);
        testPromo1.Record_Status__c = 'Active';
        insert testPromo1;
        HEP_Promotion__c testPromo2 = HEP_Test_Data_Setup_Utility.createPromotion('X2', 'National', null, gGermany.id,FoxNewRelease.id ,'', '', false);
        testPromo2.Record_Status__c = 'Active';
        insert testPromo2;
        HEP_Promotion__c testPromo3 = HEP_Test_Data_Setup_Utility.createPromotion('X-Men', 'National', null, gGermany.id,FoxNewRelease.id ,'', '', false);
        testPromo3.Record_Status__c = 'Active';
        insert testPromo3;
        
        testPromo1.Title__c = Title.id;
        testPromo2.Title__c = Title.id;
        testPromo3.Title__c = Title.id;
        
        HEP_Catalog__c MasterCatalog = HEP_Test_Data_Setup_Utility.createCatalog('52449', 'Titanic(2012)', 'Single', null, gGermany.Id, 'Approved', 'Master', false);
        MasterCatalog.Title_EDM__c = Title.id;
        insert MasterCatalog;
        //create SKU template
        //HEP_SKU_Template__c skutemp1 = HEP_Test_Data_Setup_Utility.createSKUTemplate('Titanic', gGermany.id, FoxNewRelease.Id, true);
        
        //Create Promotion catalog
        HEP_Promotion_Catalog__c PromoCatalogforPromo1 = HEP_Test_Data_Setup_Utility.createPromotionCatalog(MasterCatalog.Id, testPromo1.id, null, true);
        HEP_Promotion_Catalog__c PromoCatalogforPromo2 = HEP_Test_Data_Setup_Utility.createPromotionCatalog(MasterCatalog.Id, testPromo2.id, null, true);        
        HEP_Promotion_Catalog__c PromoCatalogforPromo3 = HEP_Test_Data_Setup_Utility.createPromotionCatalog(MasterCatalog.Id, testPromo3.id, null, true);
        //EDM_GLOBAL_TITLE__c TitleDetail = [SELECT Id FROM EDM_GLOBAL_TITLE__c WHERE FIN_PROD_ID__c = '32434'];
        Test.startTest();
        HEP_Title_Promotions_Controller.TitlePromotionsWrapper wrap = HEP_Title_Promotions_Controller.loadData(Title.Id);
        Test.stopTest();
        
    }
}