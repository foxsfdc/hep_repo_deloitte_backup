/*
 CLASS NAME : HEP_Catalog_SKUs
 PURPOSE    : Controller for the page HEP_Catalog_SKUs , loads SKU records.
 @author    : Gaurav Mehrishi
*/
public class HEP_Catalog_SKUs_Controller {
    //get related SKUs on load for Catalog
    @remoteAction
    public static CatalogSKUWrapper getSKU(String sCatalogId) {

        List < SKUWrapper > wrapperToReturn = new List < SKUWrapper > ();
        map < String, List < HEP_SKU_Price__c >> mapPriceRegionSKU = new map < String, List < HEP_SKU_Price__c >> ();
        map < String, List < HEP_SKU_Customer__c >> mapSKUCustomers = new map < String, List < HEP_SKU_Customer__c >> ();
        map < String, List < HEP_SKU_Master__c >> mapSKUMasters = new map < String, List < HEP_SKU_Master__c >> ();

        //Users can only view SKUs for territories they have been given access for; meaning a territory filter must be applied
        //List<HEP_User_Role__c> lstTerritoryRolesAcccessible = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
        Set < Id > setTerritorysAccessible = new Set < Id > ();
        String sCurrentPageName = '';
        if (String.isNotBlank(HEP_Utility.getConstantValue('HEP_CATALOG_DETAILS'))) {
            sCurrentPageName = HEP_Utility.getConstantValue('HEP_CATALOG_DETAILS');
        }
        setTerritorysAccessible = HEP_Utility.fetchTerritorysForPage(sCurrentPageName);

        System.debug('setTerritorysAccessible--------->' + setTerritorysAccessible);
        String sActiveStatus = '';
        String sMaster = '';
        if (String.isNotBlank(HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'))) {
            sActiveStatus = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        }
        if (String.isNotBlank(HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_MASTER'))) {
            sMaster = HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_MASTER');
        }        
        String sSKUQuery = 'SELECT Id,Name,SKU_Title__c,SKU_Number__c,Barcode__c,Channel__c,Format__c,Record_Status__c,Territory__c FROM HEP_SKU_Master__c WHERE Catalog_Master__c = \'' + sCatalogId + '\' AND Type__c = \'' + sMaster + '\' AND Territory__c IN : setTerritorysAccessible AND Record_Status__c = \'' + sActiveStatus + '\'';
        sSKUQuery += ' order by Territory__r.Territory_Custom_Sort__c Asc, Territory__r.Name asc, SKU_Title__c asc ';
        System.debug('Dynamic Query---->' + sSKUQuery);
        List < HEP_SKU_Master__c > lstSKURecords = Database.Query(sSKUQuery);
        System.debug('lstSKURecords-->> ' + lstSKURecords);
        Set < Id > setPromotionSKUId = new Set < Id > ();
        if (lstSKURecords != null && !lstSKURecords.isEmpty()) {
            for (HEP_SKU_Master__c objSKUMaster: lstSKURecords) {
                setPromotionSKUId.add(objSKUMaster.id);
                if (mapSKUMasters.containsKey(objSKUMaster.id)) {
                    mapSKUMasters.get(objSKUMaster.id).add(objSKUMaster);
                } else {
                    mapSKUMasters.put(objSKUMaster.id, new List < HEP_SKU_Master__c > {
                        objSKUMaster
                    });
                }
            }
            system.debug('>>>>>>>>>>>>>>' + mapSKUMasters);
            system.debug('setPromotionSKUId' + setPromotionSKUId);
            //Query all the customer records for the SKUs
            List < HEP_SKU_Customer__c > lstSKUCustomers = [SELECT Id,
                Customer__c,
                SKU_Master__c,
                Customer__r.CustomerName__c
                FROM HEP_SKU_Customer__c
                WHERE SKU_Master__c IN: setPromotionSKUId and Record_Status__c =: sActiveStatus
            ];
            if (lstSKUCustomers != NULL && !lstSKUCustomers.isEmpty()) {
                for (HEP_SKU_Customer__c objSKUCustomer: lstSKUCustomers) {
                    if (mapSKUCustomers.containsKey(objSKUCustomer.SKU_Master__c)) {
                        mapSKUCustomers.get(objSKUCustomer.SKU_Master__c).add(objSKUCustomer);
                    } else {
                        mapSKUCustomers.put(objSKUCustomer.SKU_Master__c, new List < HEP_SKU_Customer__c > {
                            objSKUCustomer
                        });
                    }
                }
                system.debug('---------' + mapSKUCustomers);
            }

            //Query to get all the Price Regions records for the SKUs related to the Catalog
            List < HEP_SKU_Price__c > lstSKUPriceRegion = [SELECT Id,
                PriceGrade__c,
                PriceGrade__r.Name,
                PriceGrade__r.PriceGrade__c,
                Region__c,
                Region__r.Name,
                SKU_Master__c,
                SKU_Master__r.Name
                FROM HEP_SKU_Price__c
                WHERE SKU_Master__c IN: setPromotionSKUId and Record_Status__c =: sActiveStatus
            ];

            if (lstSKUPriceRegion != null && !lstSKUPriceRegion.isEmpty()) {
                for (HEP_SKU_Price__c objSKUPriceRegion: lstSKUPriceRegion) {
                    if (mapPriceRegionSKU.containsKey(objSKUPriceRegion.SKU_Master__c)) {
                        mapPriceRegionSKU.get(objSKUPriceRegion.SKU_Master__c).add(objSKUPriceRegion);
                    } else {
                        mapPriceRegionSKU.put(objSKUPriceRegion.SKU_Master__c, new List < HEP_SKU_Price__c > {
                            objSKUPriceRegion
                        });
                    }
                }
                system.debug('>>>>>>>>>>>>>>' + mapPriceRegionSKU);
            }
            SKUWrapper objSKUWrapper;
            for (String sOuterKey: mapSKUMasters.keyset()) {
                objSKUWrapper = new SKUWrapper();
                List < String > lstRegions = new List < String > ();
                Set < String > setRegions = new Set < String > ();
                List < String > lstPriceGrades = new List < String > ();
                List < String > lstCustomers = new List < String > ();
                for (HEP_SKU_Master__c objSKUMaster: mapSKUMasters.get(sOuterKey)) {
                    objSKUWrapper.sKey = objSKUMaster.id;
                    if (String.isNotBlank(objSKUMaster.Name))
                        objSKUWrapper.sName = objSKUMaster.SKU_Title__c;

                    if (String.isNotBlank(objSKUMaster.SKU_Number__c))
                        objSKUWrapper.sSKUNumber = objSKUMaster.SKU_Number__c;

                    if (String.isNotBlank(objSKUMaster.Barcode__c))
                        objSKUWrapper.sBarcode = objSKUMaster.Barcode__c;

                    if (String.isNotBlank(objSKUMaster.Channel__c))
                        objSKUWrapper.sChannel = objSKUMaster.Channel__c;

                    if (String.isNotBlank(objSKUMaster.Format__c))
                        objSKUWrapper.sFormat = objSKUMaster.Format__c;

                    if (String.isNotBlank(objSKUMaster.Record_Status__c))
                        objSKUWrapper.sStatus = objSKUMaster.Record_Status__c;

                }
                if (mapPriceRegionSKU.containsKey(sOuterKey)) {
                    for (HEP_SKU_Price__c objSKUPrice: mapPriceRegionSKU.get(sOuterKey)) {
                        //lstRegions.add(objSKUPrice.Region__r.Name);
                        setRegions.add(objSKUPrice.Region__r.Name);
                        lstPriceGrades.add(objSKUPrice.PriceGrade__r.PriceGrade__c);
                    }
                }
                /*if (lstRegions.size() > 0) {
                    objSKUWrapper.sRegion = listToString(lstRegions);
                }*/
                if (setRegions.size() > 0) {
                    objSKUWrapper.sRegion = setToString(setRegions);
                }
                if (lstPriceGrades.size() > 0) {
                    objSKUWrapper.sPriceGrade = listToString(lstPriceGrades);
                }
                if (mapSKUCustomers.containsKey(sOuterKey)) {
                    for (HEP_SKU_Customer__c objSKUCustomer: mapSKUCustomers.get(sOuterKey)) {
                        lstCustomers.add(objSKUCustomer.Customer__r.CustomerName__c);
                        system.debug('@@@' + lstCustomers);
                    }
                }
                if (lstCustomers.size() > 0) {
                    objSKUWrapper.sCustomer = listToString(lstCustomers);
                    system.debug('objSKUWrapper.sCustomer' + objSKUWrapper.sCustomer);
                }
                wrapperToReturn.add(objSKUWrapper);
                system.debug('>>>>>>>>>>>>>>>>' + wrapperToReturn);
            }
            CatalogSKUWrapper CatalogSKUData = new CatalogSKUWrapper(wrapperToReturn);
            return CatalogSKUData;
        }

        return new CatalogSKUWrapper();
    }

    //this method converts list to String with values separated by comma
    public static String listToString(List < String > lstTemp) {
        String sCommaSeparated = '';
        for (String str: lstTemp) {
            sCommaSeparated += str + ',';
        }
        sCommaSeparated = sCommaSeparated.trim().removeEnd(',');
        system.debug('sCommaSeparated' + sCommaSeparated);
        return sCommaSeparated;

    }
    //this method converts set to String with values separated by comma
    public static String setToString(Set < String > setTemp) {
        String sCommaSeparated = '';
        for (String str: setTemp) {
            sCommaSeparated += str + ',';
        }
        sCommaSeparated = sCommaSeparated.trim().removeEnd(',');
        system.debug('sCommaSeparated' + sCommaSeparated);
        return sCommaSeparated;

    }
    public class CatalogSKUWrapper {

        List < SKUWrapper > wrapperToReturn;

        public CatalogSKUWrapper() {
            wrapperToReturn = new List < SKUWrapper > ();
        }

        public CatalogSKUWrapper(List < SKUWrapper > wrapperToReturn) {
            this.wrapperToReturn = wrapperToReturn;
        }
    }
    public class SKUWrapper {
        public String sKey;
        public String sName;
        public String sSKUNumber;
        public String sChannel;
        public String sFormat;
        public String sBarcode;
        public String sStatus;
        public String sRegion;
        public String sCustomer;
        public String sPriceGrade;
        public SKUWrapper() {

        }

        /**
         * SKUWrapper Constructor --- Constructor to initialize the List variables
         * @return   N/A
         */
        public SKUWrapper(String sKey, String sName, String sSKUNumber, String sChannel, String sFormat, String sBarcode, String sStatus, String sRegion, String sCustomer, String sPriceGrade) {
            this.sKey = sKey;
            this.sName = sName;
            this.sSKUNumber = sSKUNumber;
            this.sChannel = sChannel;
            this.sFormat = sFormat;
            this.sBarcode = sBarcode;
            this.sStatus = sStatus;
            this.sRegion = sRegion;
            this.sCustomer = sCustomer;
            this.sPriceGrade = sPriceGrade;
        }
    }

}