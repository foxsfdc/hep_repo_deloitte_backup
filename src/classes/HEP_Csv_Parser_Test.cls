/**
* HEP_Csv_Parser_Test -- Test class for HEP_Csv_Parser and HEP_Import_Data_From_CSV_Using_Emaild
* 
* @author    Mayank Jaggi
**/ 

@isTest 
public class HEP_Csv_Parser_Test {
  
    @testSetup
        static void createUsers(){
            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'jaggi', 'abc_xyz@deloitte.com','majaggi','mayank','majaggi','', true);
            List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
            HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary', null, null,'EUR','DE','182', 'ESCO', false);
            insert objTerr;
            HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Financials_Ultimate', 'HEP_Financials_Ultimate', false, 0, 0, 'Inbound',false, false, true);
            list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        }
    

    @isTest
    public static void TEST_HEP_Csv_Parser_handleInboundEmail()
    {
      
        HEP_Constants__c HEP_Current_Corporate = new HEP_Constants__c();
        HEP_Current_Corporate.Name = 'HEP_SPHERES_CURRENT_CORPORATE';
        HEP_Current_Corporate.Value__c = 'Current Corporate Commitment';
        insert HEP_Current_Corporate;
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Deadpool', null, null, null, False);
            insert objTitle;

            HEP_Territory__c objTerr = [select id from HEP_Territory__c limit 1];

            HEP_Catalog__c objCatalog =  HEP_Test_Data_Setup_Utility.createCatalog('83292', 'Deadpool', 'Single', null, objTerr.Id, 'Approved', 'Master', False);
            objCatalog.Title_EDM__c = objTitle.Id;
            insert objCatalog;
          
          String Body ='"North America" "T83292" "USD" "HD" "VOD" "Ultimate Initial" "Locked Sep 2018" "Current Corporate Commitment" "Ult_TheatricalDate" "No Year" "Ultimate_Rptg" "NoLicensorType" "Periodic" "NoCustomer" 19000100';
        Body += '\r\n';
            Body += '"HEI" "T83292" "USD" "HD" "VOD" "Ultimate Initial" "Locked Sep 2018" "Current Corporate Commitment" "Ult_VideoDate" "No Year" "Ultimate_Rptg" "NoLicensorType" "Periodic" "NoCustomer" 19000100';
            Body += '\r\n';

        Messaging.InboundEmail.binaryAttachment battachment = new Messaging.InboundEmail.binaryAttachment();
        battachment.fileName = 'filename.csv';
        battachment.body = Blob.valueOf(Body);

            List<String> lstToAddresses = new List<String>();
            //lstToAddresses.add('hep_csv_parser@3-s624yce2856ten0gkqvogzmbhcpxfjqhrw29judqj92yug11g.3d-d78luac.cs70.apex.sandbox.salesforce.com');
            lstToAddresses.add('test@test.com');

            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            email.toAddresses = lstToAddresses;
            email.subject = 'Email Subject';
            email.plainTextBody = 'Email text body.';
            email.binaryAttachments = new List<Messaging.InboundEmail.binaryAttachment>{battachment};
            email.fromAddress = 'test.testing@hep.dev.com';
            envelope.fromAddress = 'test.testing@hep.dev.com';

            test.startTest();
            HEP_Csv_Parser objCSVParserInstance = new HEP_Csv_Parser();
            Messaging.InboundEmailResult result = objCSVParserInstance.handleInboundEmail (email, envelope);
            HEP_Inbound_Email__c insertedInboundEmailInstance = [select Id, Email_Address__c, Type__c from HEP_Inbound_Email__c limit 1];
            system.debug('insertedInboundEmailInstance------' + insertedInboundEmailInstance);
            system.assertEquals(envelope.fromAddress, insertedInboundEmailInstance.Email_Address__c);
            system.assertEquals(HEP_Utility.getConstantValue('HEP_ULTIMATE_CSV'), insertedInboundEmailInstance.Type__c);
            system.assertEquals('InboundEmailResult:[message=null, success=true]', string.Valueof(result));

            List<HEP_Ultimates__c> lstUltimateInstance = [select Territories__c, Products__c, Currency__c, Format__c, Channel__c, Period__c, Account_No__c, Data_Value__c from HEP_Ultimates__c];
            system.debug('lstUltimateInstance--- ' + lstUltimateInstance);
            system.assertEquals('North America', lstUltimateInstance[0].Territories__c);
            system.assertEquals('HEI', lstUltimateInstance[1].Territories__c);
            system.assertEquals('T83292', lstUltimateInstance[0].Products__c);
            system.assertEquals('T83292', lstUltimateInstance[1].Products__c);
            system.assertEquals('USD', lstUltimateInstance[0].Currency__c);
            system.assertEquals('USD', lstUltimateInstance[1].Currency__c);
            system.assertEquals('HD', lstUltimateInstance[0].Format__c);
            system.assertEquals('HD', lstUltimateInstance[1].Format__c);
            system.assertEquals('VOD', lstUltimateInstance[0].Channel__c);
            system.assertEquals('VOD', lstUltimateInstance[1].Channel__c);
            system.assertEquals('Ultimate Initial', lstUltimateInstance[0].Period__c);
            system.assertEquals('Ultimate Initial', lstUltimateInstance[1].Period__c);
            system.assertEquals('Ult_TheatricalDate', lstUltimateInstance[0].Account_No__c);
            system.assertEquals('Ult_VideoDate', lstUltimateInstance[1].Account_No__c);
            test.stopTest();
        }
    }
}