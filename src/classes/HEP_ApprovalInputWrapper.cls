/**
 * InputToApprovalApi ---  Wrapper class for Approval Input Api
 * @author    Nishit Kedia
 */
 
 public class HEP_ApprovalInputWrapper {

        public String sApprovalRecordId;
        public String sApprovalTypeName;
        public String sterritoryId;
        public String sRequesterComment;
        public String sPriorSubmittersUserIds;
        public String sLineOfBuisness;
    }