/*
*   HEP_Import_Data_From_CSV_Using_Email -- Class to retrieve data in stored file and update records
*   in HEP_Ultimates__c object
*   Invoked from HEP_Csv_Parser class
*   @author    Mayank Jaggi
*/

public class HEP_Import_Data_From_CSV_Using_Email
{
    public static integer processedRecordCounter = 0;
    public static integer validRecordCounter = 0;
    public static string productValue = '';
    public static string title;
    public static list<String> lstCsvDataLines = new list<String>();
    public static List <HEP_Ultimates__c> lstUltimates = new List <HEP_Ultimates__c>();   

    /*  
    *   getCSVBody -- method to get CSV body of the stored file
    *   @param file name as string and parent record id as string
    *   @return void
    *   @author Mayank Jaggi
    */

    public static void getCSVBody(String sEmailContent) {    
        lstCsvDataLines = new list<String>();

        // calling retriveDatafromContent() in HEP_Integration_Util to get stored file body in string
        //String sEmailContent = HEP_Integration_Util.retriveDatafromContent(ParentRecordId, csvFileName);

        lstUltimates = new List <HEP_Ultimates__c>();  // list for object that needs to be updated 
        set<string> setFoxipediaCatalogIds = new set<string>();
        string sFoxipediaCatalogId;
        list<HEP_Catalog__c> lstCatalogs = new list<HEP_Catalog__c>();
        map<string, Id> mapCatIdVsTitleId = new map<string, Id>();
        //system.debug('sEmailContent.length()  : ' + sEmailContent.length() ); 
        //system.debug('sEmailContent  : ' + sEmailContent);
        for(Integer counter = 0; counter < sEmailContent.length(); ){
            String rowString = sEmailContent.subString(counter , sEmailContent.indexOf('\r\n',counter));
            //System.debug(rowString);
            rowString = rowString.replace('" "', ',');
            rowString = rowString.replace('" ', ','); 
            rowString = rowString.replace(' "', ','); 
            rowString = rowString.replace('"', '');
            //System.debug(rowString);
            list<string> lstCsvRecordData = rowString.split(',');
            sFoxipediaCatalogId = lstCsvRecordData[1].mid(1, (lstCsvRecordData[1].length() - 1));
            setFoxipediaCatalogIds.add(sFoxipediaCatalogId);
            counter = sEmailContent.indexOf('\r\n',counter)+3;
            //system.debug('Counter : ' +counter);
        }
        //System.debug('setFoxipediaCatalogIds : ' + setFoxipediaCatalogIds);   
        if(setFoxipediaCatalogIds != null && !setFoxipediaCatalogIds.isEmpty()){
            lstCatalogs = [select CatalogId__c, Title_EDM__c from HEP_Catalog__c 
                                                where CatalogId__c in :setFoxipediaCatalogIds];
        }
        //System.debug('lstCatalogs : ' + lstCatalogs);   
        if(lstCatalogs != null && !lstCatalogs.isEmpty()){
            for(HEP_Catalog__c objCatalog : lstCatalogs){
                mapCatIdVsTitleId.put(objCatalog.CatalogId__c, objCatalog.Title_EDM__c);
            }
        } 
        //System.debug('mapCatIdVsTitleId : ' + mapCatIdVsTitleId);   
        
        //system.debug('sEmailContent : ' + sEmailContent);
        sEmailContent = sEmailContent.replace('Korea, South', 'South Korea');
        for(Integer counter = 0; counter < sEmailContent.length();){
            String rowString = sEmailContent.subString(counter , sEmailContent.indexOf('\r\n',counter));
            //System.debug(rowString);
            parseCSV(rowString, mapCatIdVsTitleId);
            counter = sEmailContent.indexOf('\r\n',counter)+3;
        }
        //System.debug('-------UPSERTING VALUES NOW-----');
        upsert lstUltimates CompositeKey__c;          // upserting based in this field    
        //System.debug('-------DONE UPSERTING------');
        //System.debug('list lstUltimates ---- '+lstUltimates);   
        
    }   

        /*  
        *   parseCSV -- method to parse the CSV body and update records
        *   @param string with body of the file
        *   @return void
        *   @author Mayank Jaggi
        */

        public static void parseCSV(String contents, map<string,id> mapCatIdVsTitleId) {
            
            HEP_Ultimates__c ultimateObj = new HEP_Ultimates__c();
            // replacing "" in CSV Body with , here
            contents = contents.replace('" "', ',');
            contents = contents.replace('" ', ','); 
            contents = contents.replace(' "', ','); 
            contents = contents.replace('"', '');
            list<string> lstCsvRecordData = contents.split(',');
            String catalogId = lstCsvRecordData[1].mid(1, (lstCsvRecordData[1].length() - 1));
            
            ultimateObj.Title__c = mapCatIdVsTitleId.get(catalogId);
            ultimateObj.Territories__c = lstCsvRecordData[0];   
            ultimateObj.Products__c = lstCsvRecordData[1];    
            ultimateObj.Currency__c = lstCsvRecordData[2];    
            ultimateObj.Format__c = lstCsvRecordData[3];
            ultimateObj.Channel__c = lstCsvRecordData[4];
            ultimateObj.Period__c = lstCsvRecordData[5];
            ultimateObj.Account_No__c = lstCsvRecordData[8];
            //system.debug('lstCsvRecordData[14].trim()--> '+lstCsvRecordData[14].trim());
            ultimateObj.Data_Value__c = Decimal.ValueOf(lstCsvRecordData[14].trim());
            
            //integer lengthOfCatalogId = lstCsvRecordData[1].length();

            // creating composite key
            ultimateObj.CompositeKey__c = lstCsvRecordData[0] + lstCsvRecordData[1] + lstCsvRecordData[2] + 
                lstCsvRecordData[3] + lstCsvRecordData[4] + lstCsvRecordData[5] + lstCsvRecordData[8];
            processedRecordCounter++;
                
            // filter criteria
            if (((lstCsvRecordData[0] == HEP_Utility.getConstantValue('HEP_NORTH_AMERICA') || 
                lstCsvRecordData[0] == HEP_Utility.getConstantValue('HEP_HEI')) && 
                    (lstCsvRecordData[7] == HEP_Utility.getConstantValue('HEP_SPHERES_CURRENT_CORPORATE')) && 
                    (lstCsvRecordData[2] == HEP_Utility.getConstantValue('HEP_USD'))))
            {
                lstUltimates.add(ultimateObj);   
                //System.debug('Filter condition satisfied. Adding record to list.');
                validRecordCounter++;
            }  
            //System.debug('total records processed: ' + processedRecordCounter);
            //system.debug('records satisfying criteria: ' + validRecordCounter);
            
        }
}