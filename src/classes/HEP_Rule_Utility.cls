/* Class Name   : HEP_Rule_Utility
 * Description  : All Utility methods.    
 * Created By   : Nidhin V K
 * Created On   : 07-29-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-29-2016        1000            Initial version
 *
 */
public without sharing class HEP_Rule_Utility {

    /* getActiveRules
     * Returns Map of Rule criteria applicable for the given sObject type
     * @params : sObjectType, ruleType
     * @return : Map <Id,HEP_Rule__c>
     *
     */
    public static Map <Id,HEP_Rule__c> getActiveRules(String sObjectType) {
        //get all active rules on the specified object                    
        Map <Id,HEP_Rule__c> activeRulesMap = new Map <Id,HEP_Rule__c> ([SELECT
            Id,
            Name,
            HEP_Criteria_Logic__c,
            HEP_IsActive__c,
            HEP_Priority__c,
            HEP_Target_SObject_API_Name__c,
            (SELECT Id,
                HEP_Operator__c,
                HEP_Serial_Number__c,
                HEP_Source_SObject_Field__c,
                HEP_Source_SObject_Field_Type__c,
                HEP_Target_Task_Field__c,
                HEP_Value_To_Compare__c,
                HEP_Value_to_Map__c,
                RecordTypeId FROM HEP_Rule_Criteria__r)
            FROM
                HEP_Rule__c
            WHERE
                HEP_Target_SObject_API_Name__c =: sObjectType
            AND
                HEP_IsActive__c = TRUE
        ]);

        return activeRulesMap;
    }
    
    


    /* evaluateRuleCriteria
     * Returns list of Rule criteria applicable for the given sObject type
     * @params : objRecord, activeRules
     * @return : List<HEP_Rule_Version__c>
     *
     */
    public static List < HEP_Rule__c > evaluateRuleCriteria(sObject objRecord, List < HEP_Rule__c > lstActiveRules) {

        Map < Id, HEP_Rule__c > mapEligibleRules = new Map < Id, HEP_Rule__c > ();
        Map < String, Boolean > mapExprValue = new Map < String, Boolean > ();

        try {
            System.debug('evaluateRuleCriteria : ');
            //get record type compare
            Map<String, Schema.RecordTypeInfo> mapRTbyName = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName();
            //evaluate each rule criteria
            for (HEP_Rule__c activeRule: lstActiveRules) {

                mapExprValue = new Map < String, Boolean > ();
                //if 1st criteria is false then no need to check in case of and  
                for (HEP_Rule_Criterion__c criterion: activeRule.HEP_Rule_Criteria__r) {
                    if (!String.isBlank(criterion.HEP_Source_SObject_Field__c) &&
                        !String.isBlank(criterion.HEP_Operator__c) &&
                        !String.isBlank(criterion.HEP_Value_To_Compare__c) &&
                        !String.isBlank(criterion.HEP_Source_SObject_Field_Type__c) &&
                        criterion.HEP_Serial_Number__c > 0 &&
                        mapRTbyName.get(HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE).getRecordTypeId() == criterion.RecordTypeId) {

                        System.debug('Inside Loop : ' + criterion);
                        Object objFieldValue = objRecord.get(criterion.HEP_Source_SObject_Field__c);
                        mapExprValue.put(String.valueOf(criterion.HEP_Serial_Number__c),
                            compareValues(
                                (objFieldValue == NULL) ? NULL : String.valueOf(objFieldValue),
                                criterion.HEP_Operator__c,
                                criterion.HEP_Value_To_Compare__c,
                                criterion.HEP_Source_SObject_Field_Type__c));

                    }
                }
                System.debug('mapExprValue : ' + mapExprValue);
                System.debug('HEP_Criteria_Logic__c : ' + activeRule.HEP_Criteria_Logic__c);
                Boolean bExpressionResult = evaluateExpression(activeRule.HEP_Criteria_Logic__c, mapExprValue);
                if (mapExprValue != NULL && bExpressionResult) {
                    mapEligibleRules.put(activeRule.Id, activeRule);
                }
            }
            System.debug('mapEligibleRules : ' + mapEligibleRules);

            //return false if the rule evaluates to false or there is no evaluation criteria    
            if (mapEligibleRules.size() == 0) {
                //no valid rules
                System.debug('HEP_Util : No Valid Eligible Rules');
                return null;
            }

            System.debug('<<Final:>>' + mapEligibleRules);
            return mapEligibleRules.values();

        } catch (Exception ex) {
            //throw error no such field exists
            System.debug('Exception on Class : HEP_Util, Error : ' +
                ex.getMessage() +
                ' Line Number : ' +
                ex.getLineNumber() +
                ' Cause : ' +
                ex.getCause() +
                ' Type : ' +
                ex.getTypeName());

            return null;
        }
    }


    /* evaluateExpression
     * This Method Process the given Expression and Return the Final Result(true/false)
     * @params : sExpresssion, mapExprValue
     * @return : Boolean
     *
     */
    public static Boolean evaluateExpression(String sExpresssion, Map < String, Boolean > mapExprValue) {
        String sFilter = sExpresssion; //This variable is used for getting the expression Value 
        
        List < Integer > lstExprValue = new List < Integer > ();
        for (String exprKey: mapExprValue.keyset()) {
            lstExprValue.add(Integer.valueof(exprKey));
        }
        lstExprValue.sort();

        for (Integer i = lstExprValue.size() - 1; i >= 0; i--) {
            sFilter = sFilter.replace(String.valueOf(lstExprValue.get(i)), String.valueOf(mapExprValue.get(String.valueOf(lstExprValue.get(i)))));
        }
        //Start:Akshay[25-Jan-18]- Added method for criteria logic processing
        return evaluateFilteredExpression(sFilter);
    }

    /* evaluateFilteredExpression
     * This Method Process the given Expression and Return the Final Result(true/false)
     * @params : sFilter, Expression of booleans with AND OR conditions
     * @return : Boolean
     *
     */
    public static Boolean evaluateFilteredExpression(String sFilter) {
        Boolean bRuleFlag; //This variable is used for returning the Final Filter value 

        if (sFilter.Contains('OR') && sFilter.Contains('AND')) {
            List < string > lstFilter = sFilter.split('\\('); //Splitting the Filter String and Creating the List
            //system.debug('FilterList'+FilterList.size());
            Integer iLastIndex, iFirstIndex;
            String sSubFilterValue, sSubFilterValue1;
            Boolean bSubFilterFlag;
            for (integer i = 0; i < lstFilter.size(); i++) {
                iLastIndex = sFilter.lastIndexOf('(');
                if (iLastIndex > -1) {
                    sSubFilterValue1 = sFilter.substring(iLastIndex, sFilter.length());
                    iFirstIndex = sSubFilterValue1.indexOf(')');
                    iFirstIndex = iFirstIndex + iLastIndex;
                    sSubFilterValue = sFilter.substring(iLastIndex, iFirstIndex + 1);
                    system.debug(sSubFilterValue);
                    bSubFilterFlag = returnFilterResult(sSubFilterValue); //Returns result extracted expression from sFilter  eg:(true And false)
                    sFilter = sFilter.replace(sSubFilterValue, string.valueOf(bSubFilterFlag));
                    system.debug('Filter' + sFilter);
                }
            }
            bRuleFlag = Boolean.valueof(sFilter);
        } else {
            bRuleFlag = returnFilterResult(sFilter);
        }

        return bRuleFlag;
    }
    //End:Akshay[25-Jan-18]- Added method for criteria logic processing
    /* returnFilterResult
     * This Method checks and process the Either "AND" OR  "OR" Filter String and Return the Final Value(true/false)
     * @params : sFilterValue
     * @return : Boolean
     *
     */
    public static Boolean returnFilterResult(String sFilterValue) {
        Boolean bRuleFlag;
        System.debug('SAu***: ' + sFilterValue);
        if (sFilterValue.Contains('AND') && !sFilterValue.Contains('OR')) {
            if (sFilterValue.Contains('false')) {
                bRuleFlag = false;
            } else {
                bRuleFlag = true;
            }
        } else if (sFilterValue.Contains('OR') && !sFilterValue.Contains('AND')) {
            if (sFilterValue.Contains('true')) {
                bRuleFlag = true;
            } else {
                bRuleFlag = false;
            }
        } else {
            sFilterValue = sFilterValue.replace('(', '');
            sFilterValue = sFilterValue.replace(')', '');
            bRuleFlag = Boolean.valueof(sFilterValue);
        }
        return bRuleFlag;
    }


    /* compareValues
     * This Method compare the expression and Return the Final Value(true/false)
     * @params : sFieldValue, sValue, fieldType, sOperator
     * @return : Boolean
     *
     */
    public static Boolean compareValues(String sFieldValue, String sOperator, String tValue, String fieldType) {
        System.debug('compareValues : ');
        System.debug('sOperator : ' + sOperator);
        System.debug('fieldType : ' + fieldType);
        if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_EQUALS_TO)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() || fieldType == Schema.DisplayType.Phone.name() ||  fieldType == Schema.DisplayType.Reference.name() || 
                    fieldType == Schema.DisplayType.Picklist.name() || fieldType == Schema.DisplayType.URL.name() || fieldType == Schema.DisplayType.Email.name() || fieldType == Schema.DisplayType.EncryptedString.name() ||fieldType == Schema.DisplayType.Id.name()) 
                    && String.valueOf(sFieldValue) == String.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() &&
                Integer.valueOf(sFieldValue) == Integer.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() &&
                Double.valueOf(sFieldValue) == Double.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Boolean.name() &&
                Boolean.valueOf(sFieldValue) == Boolean.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() &&
                DateTime.valueOf(sFieldValue) == DateTime.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() &&
                Date.valueOf(sFieldValue) == Date.valueOf(tValue)) {

                return true;

            } else {
                return false;
            }
        } else if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() || fieldType == Schema.DisplayType.Phone.name() || fieldType == Schema.DisplayType.Email.name() ||                    
                    fieldType == Schema.DisplayType.Picklist.name() || fieldType == Schema.DisplayType.URL.name() || fieldType == Schema.DisplayType.Reference.name() || fieldType == Schema.DisplayType.EncryptedString.name() ||  fieldType == Schema.DisplayType.Id.name())          
                   && String.valueOf(sFieldValue) != String.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() && Integer.valueOf(sFieldValue) != Integer.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() && Double.valueOf(sFieldValue) != Double.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Boolean.name() && Boolean.valueOf(sFieldValue) != Boolean.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() && DateTime.valueOf(sFieldValue) != DateTime.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() && Date.valueOf(sFieldValue) != Date.valueOf(tValue)) {
                return true;

            } else {
                return false;
            }
        } else if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_GREATER_THAN)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() || fieldType == Schema.DisplayType.Phone.name() || fieldType == Schema.DisplayType.Email.name() ||                  
                    fieldType == Schema.DisplayType.Picklist.name() || fieldType == Schema.DisplayType.URL.name() || fieldType == Schema.DisplayType.Reference.name() || fieldType == Schema.DisplayType.EncryptedString.name() ||  fieldType == Schema.DisplayType.Id.name())                   
                    && String.valueOf(sFieldValue) > String.valueOf(tValue)) {

                return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() && Integer.valueOf(sFieldValue) > Integer.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() && Double.valueOf(sFieldValue) > Double.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() && DateTime.valueOf(sFieldValue) > DateTime.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() && Date.valueOf(sFieldValue) > Date.valueOf(tValue)) {
                return true;

            } else {
                return false;
            }
        } else if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_GREATER_THAN_OR_EQUALS_TO)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() ||fieldType == Schema.DisplayType.Phone.name() || fieldType == Schema.DisplayType.URL.name() ||                 
                    fieldType == Schema.DisplayType.Picklist.name() ||  fieldType == Schema.DisplayType.Email.name() || fieldType == Schema.DisplayType.Reference.name() || fieldType == Schema.DisplayType.EncryptedString.name() || fieldType == Schema.DisplayType.Id.name())             
                     && String.valueOf(sFieldValue) >= String.valueOf(tValue)) {

               		return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() && Integer.valueOf(sFieldValue) >= Integer.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() && Double.valueOf(sFieldValue) >= Double.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() && DateTime.valueOf(sFieldValue) >= DateTime.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() && Date.valueOf(sFieldValue) >= Date.valueOf(tValue)) {
                return true;

            } else {
                return false;
            }
        } else if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_LESS_THAN)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() ||fieldType == Schema.DisplayType.Phone.name() || fieldType == Schema.DisplayType.URL.name() ||                 
                    fieldType == Schema.DisplayType.Picklist.name() ||  fieldType == Schema.DisplayType.Email.name() || fieldType == Schema.DisplayType.Reference.name() || fieldType == Schema.DisplayType.EncryptedString.name() || fieldType == Schema.DisplayType.Id.name())             
                     && String.valueOf(sFieldValue) < String.valueOf(tValue)) {

               		return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() && Integer.valueOf(sFieldValue) < Integer.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() &&  Double.valueOf(sFieldValue) < Double.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() && DateTime.valueOf(sFieldValue) < DateTime.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() &&  Date.valueOf(sFieldValue) < Date.valueOf(tValue)) {
                return true;

            } else {
                return false;
            }
        } else if (sOperator.equals(HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO)) {

            if ((fieldType == Schema.DisplayType.String.name() || fieldType == Schema.DisplayType.TextArea.name() ||fieldType == Schema.DisplayType.Phone.name() || fieldType == Schema.DisplayType.URL.name() ||                 
                    fieldType == Schema.DisplayType.Picklist.name() ||  fieldType == Schema.DisplayType.Email.name() || fieldType == Schema.DisplayType.Reference.name() || fieldType == Schema.DisplayType.EncryptedString.name() || fieldType == Schema.DisplayType.Id.name())             
                     && String.valueOf(sFieldValue) <= String.valueOf(tValue)) {

               		return true;

            } else if (fieldType == Schema.DisplayType.Integer.name() && Integer.valueOf(sFieldValue) <= Integer.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Double.name() && Double.valueOf(sFieldValue) <= Double.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.DateTime.name() && DateTime.valueOf(sFieldValue) <= DateTime.valueOf(tValue)) {
                return true;

            } else if (fieldType == Schema.DisplayType.Date.name() && Date.valueOf(sFieldValue) <= Date.valueOf(tValue)) {
                return true;

            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;
    }

    /* checkCriteriaLogic 
     * Check criteria logic in rule version has valid syntax
     * @params : objRuleVersion - List of Rule Versions
     * @return : Boolean
     */
    public static Boolean checkCriteriaLogic(HEP_Rule__c objRule) {

        if (validateExpression(objRule.HEP_Criteria_Logic__c)) {
            List < HEP_Rule_Criterion__c > lstRelatedCriteria = [SELECT
                Id, HEP_Serial_Number__c, HEP_Rule__c
                FROM
                HEP_Rule_Criterion__c
                WHERE
                ((RecordType.DeveloperName = 'Compare_Field_Value') OR (RecordType.DeveloperName = 'Map_Object_Fields'))
                AND
                HEP_Rule__c =: objRule.Id
            ];

            if (lstRelatedCriteria.size() > 0) {

                System.debug('Criteria is there');

                Set < Integer > setSerialNo = new Set < Integer > ();
                for (HEP_Rule_Criterion__c criteria: lstRelatedCriteria) {
                    System.debug(criteria.HEP_Serial_Number__c);
                    setSerialNo.add(Integer.valueOf(criteria.HEP_Serial_Number__c));
                }

                Integer iMaxSerialNo;
                for (Integer max: setSerialNo) {
                    if(iMaxSerialNo == null || iMaxSerialNo == 0)
                        iMaxSerialNo = max;
                    else if(iMaxSerialNo<max)
                        iMaxSerialNo = max;
                }

                System.debug('<<iMaxSerialNo>>' + iMaxSerialNo);

                //Extract serial Numbers from criteria logic
                List < String > lstSeialsEntered = extractSerialNumbers(objRule.HEP_Criteria_Logic__c);

                for (String serial: lstSeialsEntered) {
                    if (Integer.valueOf(serial) > iMaxSerialNo || Integer.valueOf(serial) == 0) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /* extractSerialNumbers 
     * Parse given Expression to extract serialNumbers
     * @params : sCriteriaLogic
     * @return : List of serial numbers
     */
    public static List < String > extractSerialNumbers(String sCriteriaLogic) {

        System.debug('<<extractSerialNumbers>>' + sCriteriaLogic);
        List < String > lstExtractedSerials = new List < String > ();
        sCriteriaLogic = sCriteriaLogic.replaceAll('\\(', '');
        sCriteriaLogic = sCriteriaLogic.replaceAll('\\)', '');
        sCriteriaLogic = sCriteriaLogic.replaceAll('AND', ',');
        sCriteriaLogic = sCriteriaLogic.replaceAll('OR', ',');
        sCriteriaLogic = sCriteriaLogic.replaceAll(' ', '');

        System.debug('<<Final sCriteriaLogic>>' + sCriteriaLogic);
        lstExtractedSerials = sCriteriaLogic.split(',');
        System.debug(lstExtractedSerials);

        return lstExtractedSerials;
    }


    /* validateExpression 
     * Validate given Expression using defined RegEx and return true if matching correctly otherwise false
     * @params : sInputExp
     * @return : Boolean
     */
    public static Boolean validateExpression(String sInputExp) {
        Boolean bIsValid = false;

        //START: Check expression starts and ends with '(' and ')' respectively
        String sInputExpTemp = sInputExp.replaceAll('AND', '');
        sInputExpTemp = sInputExpTemp.replaceAll('OR', '');
        sInputExpTemp = sInputExpTemp.replaceAll('[0-9]', '');
        sInputExpTemp = sInputExpTemp.replaceAll(' ', '');
        if (!((sInputExpTemp.startswith('((') && sInputExpTemp.endswith('))')) || (sInputExpTemp == '()'))) {
            System.debug('Doesnt start and end with');
            return false;
        }
        //END: Check expression starts and ends with '(' and ')' respectively

        //START:Check if bracket count is not equal
        Integer iCountStartBracket = sInputExp.length() - sInputExp.replaceAll('\\(', '').length();
        Integer iCountEndBracket = sInputExp.length() - sInputExp.replaceAll('\\)', '').length();
        if (iCountStartBracket != iCountEndBracket) {
            System.debug('Bracket Count unequal');
            return false;
        }
        //END:Check if bracket count is not equal

        try {
            Pattern myPattern1 = Pattern.compile('((.*(AND)\\s+[0-9]+\\s+(OR).*)|(.*(OR)\\s+[0-9]+\\s+(AND).*))');
            Matcher myMatcher1 = myPattern1.matcher(sInputExp);

            Pattern myPattern2 = Pattern.compile('([(]*[0-9]+[)]*(\\s+(AND|OR)\\s+[(]*[0-9]+[)]*)*$)');
            Matcher myMatcher2 = myPattern2.matcher(sInputExp);

            Pattern myPattern3 = Pattern.compile('\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\([^()]*\\))*\\))*\\))*\\))*\\))*\\))*\\))*\\))*\\))*\\)'); //For 10 level
            //Pattern myPattern3 = Pattern.compile('\\(([^()]*|\\(([^()]*|\\(([^()]*|\\(([^()]*|\\([^()]*\\))*\\))*\\))*\\))*\\)'); //For 4 level
            //Pattern myPattern3 = Pattern.compile('\\(([^()]*|\\(([^()]*|\\([^()]*\\))*\\))*\\)'); //For 3 level
            Matcher myMatcher3 = myPattern3.matcher(sInputExp);

            /*Pattern myPattern4 = Pattern.compile('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');
            Matcher myMatcher4 = myPattern4.matcher(sInputExp);*/

            /*For Spacial Character*/

            if (myMatcher1.Matches() || !myMatcher2.Matches() || !myMatcher3.Matches()) {
                bIsValid = false;
            } else {
                bIsValid = true;
            }
            System.debug('Expression Status:' + bIsValid);
        } catch (System.LimitException e1) {
            bIsValid = false;
            system.debug('Exception during LeadDistributionExpressionHelper::validateExpression()---->' + e1.getMessage());
        } catch (Exception e) {
            bIsValid = false;
            system.debug('Exception during LeadDistributionExpressionHelper::validateExpression()---->' + e.getMessage());
        }

        return bIsValid;
    }
}