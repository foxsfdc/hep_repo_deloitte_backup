/**
* HEP_Siebel_CatalogDetails -- Controller to make the callout to Siebel interface for updating/deleting/deleting the Catalog details
* @author    Lakshman Jinnuri
*/
public class HEP_Siebel_CatalogDetails implements HEP_IntegrationInterface{ 
    //Variables to hold the custom Settings data
    String sHEP_SIEBEL_OAUTH = HEP_Utility.getConstantValue('HEP_SIEBEL_OAUTH');
    String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
    String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
    String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
    String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
    String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
    String sHEP_SIEBEL_PRODUCERID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
    String sHEP_SIEBEL_EVENTTYPE = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
    String sHEP_SIEBEL_EVENTNAME = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
    String sHEP_SIEBEL_CATALOG_VALUE = HEP_Utility.getConstantValue('PROMOTION_LOB_CATALOG');
    String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
    String sHEP_SIEBEL_SERVICE = HEP_Utility.getConstantValue('HEP_SIEBEL_SERVICE');
    String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    String sLicensor = HEP_Utility.getConstantValue('HEP_FOX_LICENSOR');
    String sLicensorType = HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_TYPE');
    String sFox = HEP_Utility.getConstantValue('HEP_FOX');
    String sGlobal = HEP_Utility.getConstantValue('CATALOG_TYPE_GLOBAL');
      
    /**
    * performTransaction -- Method to store Catalog details 
    * @param  Takes HEP_InterfaceTxnResponse wrapper as input
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        String sCatalogJson;
        HEP_ProductCatalogWrapper objWrapper = new HEP_ProductCatalogWrapper();  
        if(sHEP_SIEBEL_OAUTH != null )   
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_SIEBEL_OAUTH);
        System.debug('sAccessToken :'+sAccessToken);
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sCatalogWrapper;
            if(objTxnResponse.sSourceId != null){
                sCatalogWrapper = getRequestBody((List<Id>)JSON.deserialize(objTxnResponse.sSourceId,list<Id>.class));
                System.debug('request body :' + sCatalogWrapper);
            }
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for Authentication
            if(sHEP_SIEBEL_SERVICE != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_SIEBEL_SERVICE);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c);
                objHttpRequest.setMethod('POST');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('Accept','application/json');
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objHttpRequest.setBody(sCatalogWrapper);
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                System.debug('objHttpResp.getStatus() :' + objHttpResp.getStatus());
                if(sHEP_SUCCESS != null && sHEP_STATUS_ACCEPTED != null){
                    if(objHttpResp.getStatus().equals(sHEP_SUCCESS) || objHttpResp.getStatus().equals(sHEP_STATUS_ACCEPTED) && sHEP_Int_Txn_Response_Status_Success != null){         
                        objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                    }
                    else{
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    }
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }     
    }

    /**
    * getRequestBody -- gets the details from Catalog Object based on input paramters
    * @param  Takes List of Id's
    * @return Serialised HEP_Siebel_CatalogWrapper wrapper   
    * @author Lakshman Jinnuri      
    */
    @TestVisible
    private string getRequestBody(list<ID> lstCatalogIds){
        HEP_Siebel_CatalogWrapper objCatalogWrapper = new HEP_Siebel_CatalogWrapper();
        list<HEP_Siebel_CatalogWrapper.Items> lstTempItems = new list<HEP_Siebel_CatalogWrapper.Items>();
        List<String> lstTypes = new List<String>{sLicensor,sLicensorType};
        map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(lstTypes, 'Name', 'Values__c');
        System.debug('mapLOVMapping :' + mapLOVMapping);
        if(sHEP_SIEBEL_PRODUCERID != null && sHEP_SIEBEL_EVENTTYPE != null && sHEP_SIEBEL_EVENTNAME != null){
            objCatalogWrapper.ProducerID = sHEP_SIEBEL_PRODUCERID;
            objCatalogWrapper.EventType = sHEP_SIEBEL_EVENTTYPE;
            objCatalogWrapper.EventName = sHEP_SIEBEL_EVENTNAME;
        }    
        for (HEP_Catalog__c objCatalog: [SELECT 
                      Id,CreatedDate,CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Record_Status__c,Territory__r.Id,
                      Territory__r.Name,CatalogId__c,Title_EDM__r.FOX_ID__c,Title_EDM__r.FIN_PROD_ID__c,Catalog_Name__c,
                      Catalog_Type__c,Licensor__c,Licensor_Type__c,Type__c,Local_Catalog_Name__c
                      FROM
                        HEP_Catalog__c
                      WHERE 
                        Id IN :lstCatalogIds]) {
            HEP_Siebel_CatalogWrapper.Items objCatalogItem = new HEP_Siebel_CatalogWrapper.Items();
            if(sHEP_SIEBEL_CATALOG_VALUE != null)  
                objCatalogItem.objectType = sHEP_SIEBEL_CATALOG_VALUE.toLowerCase();
            objCatalogItem.createdDate = String.valueOf(objCatalog.CreatedDate);  
            objCatalogItem.createdBy = objCatalog.CreatedBy.Name;    
            objCatalogItem.updatedDate = String.valueOf(objCatalog.LastModifiedDate);  
            objCatalogItem.updatedBy = objCatalog.LastModifiedBy.Name;    
            objCatalogItem.recordId = objCatalog.Id; 
            objCatalogItem.recordStatus = objCatalog.Record_Status__c; 
            objCatalogItem.territoryId = objCatalog.Territory__r.Id; 
            objCatalogItem.territoryName = objCatalog.Territory__r.Name;       
            objCatalogItem.catalogNumber = objCatalog.CatalogId__c;
            if(objCatalog.Territory__r.Name.equals(sGlobal))   
                objCatalogItem.catalogName = objCatalog.Catalog_Name__c;
            else 
                objCatalogItem.catalogName = objCatalog.Local_Catalog_Name__c;
            objCatalogItem.catalogType = objCatalog.Catalog_Type__c;  
            objCatalogItem.foxId = objCatalog.Title_EDM__r.FOX_ID__c;   
            objCatalogItem.titleNumber = objCatalog.Title_EDM__r.FIN_PROD_ID__c;
            if(objCatalog.Licensor__c != null){
                objCatalogItem.licensor = mapLOVMapping.containsKey(sLicensor + '*' + objCatalog.Licensor__c) ? mapLOVMapping.get(sLicensor + '*' + objCatalog.Licensor__c) : objCatalog.Licensor__c; 
            }else
                objCatalogItem.licensor = objCatalog.Licensor__c; 
            if(objCatalog.Licensor_Type__c != null){      
                String sLOVLicensorTypeExists = sLicensorType + '*' + objCatalog.Licensor_Type__c;
                objCatalogItem.licensorType = mapLOVMapping.containsKey(sLOVLicensorTypeExists) ? mapLOVMapping.get(sLOVLicensorTypeExists) : objCatalog.Licensor_Type__c; 
            }else
                objCatalogItem.licensorType = objCatalog.Licensor_Type__c;
            objCatalogItem.type = objCatalog.Type__c;
            lstTempItems.add(objCatalogItem);
        }
        objCatalogWrapper.Data.Payload.items = lstTempItems;
        return JSON.serialize(objCatalogWrapper);       
    }    
}