@isTest
public class HEP_Approval_Component_Controller_Test{
    
    @testSetup
  	static void createSetupComponent(){
  		//Create All test data;
  		HEP_ApprovalProcess_Test.createTestUserSetup();
  	}
  	
    static TestMethod void testComponentForDating() {
        //Fetch the created Test User:
	  	User objTestUser = [Select 	Id,
	  								Name 
	  								from User 
	  								where Email = 'ApprovalTestUser@hep.com'];
	  	
	  	//fetch promotion:
	  	HEP_Promotion__c objTestPromotion = [Select Id,
	  												Territory__c,
	  												Name 
	  												from HEP_Promotion__c 
	  												where Promotion_Type__c ='National'];
	  	
	  	HEP_Territory__c objGerTerritory = [Select Id,
	  											Name 
	  											from HEP_Territory__c
	  											where Name = 'Germany'];
	  	
	  	//Run in the context of User:
	  	System.runAs(objTestUser){
			HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objGerTerritory.Id, objGerTerritory.Id, false);
	        VODRelease.Projected_FAD_Logic__c = 'Monday';
	        VODRelease.Media_Type__c ='Digital';
	        insert VODRelease;
	        
	        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objGerTerritory.Id, objTestPromotion.Id, VODRelease.Id, false);
	        DatingRec1.Status__c = 'Confirmed';
	        DatingRec1.Locked_Status__c = 'Locked';
	        insert DatingRec1;
	        
			//Invoke Approval Process:
			String approvalRecordId = DatingRec1.Id;
			String approvalType='DATING';
			String territoryId = objGerTerritory.Id;
			HEP_ApprovalProcess pr= new HEP_ApprovalProcess();
			Test.startTest();
			Boolean status = pr.invokeApprovalProcess(approvalRecordId,approvalType,territoryId);
			
			Test.stopTest();
	  	}
    }
    
    static TestMethod void testComponentForProduct() {
        //Fetch the created Test User:
	  	User objTestUser = [Select 	Id,
	  								Name 
	  								from User 
	  								where Email = 'ApprovalTestUser@hep.com'];
	  	
	  	//fetch promotion:
	  	HEP_Promotion__c objTestPromotion = [Select Id,
	  												Territory__c,
	  												Name 
	  												from HEP_Promotion__c 
	  												where Promotion_Type__c ='National'];
	  	
	  	HEP_Territory__c objGerTerritory = [Select Id,
	  											Name 
	  											from HEP_Territory__c
	  											where Name = 'Germany'];
	  	
	  	//Run in the context of User:
	  	System.runAs(objTestUser){
			
		  //Create Catalog Record
	      HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
	      objCatalog.Licensor_Type__c = 'Lucas';
	      objCatalog.Licensor__c = 'ABLO';
	      insert objCatalog;
	      
	      //Create Promotion Catalog Record
	      HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
	        
	       //Create SKU Master Record
		    HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '456', 'New SKU', 'Master', 'Physical', 'DD', 'DVD', false);
		    objSKU.Barcode__c = 'Barcode';
		    objSKU.SKU_Compilation__c = 'SKUCompilation';
		    objSKU.SKU_Permanent_Range__c ='NL (NL)';
		    objSKU.Comments__c ='Test Comments';
		    objSKU.SKU_Configuration__c = '4K';
		    objSKU.Current_Release_Date__c = System.today();
		    objSKU.INTL_Stock_in_Warehouse__c = 100;
		    objSKU.Rental_Ready__c = 'RRD';
		    insert objSKU;
    
        	//Create Promotion SKU record
    		HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id, HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'), 'Locked', true);  
			
			//Invoke Approval Process:
			String approvalRecordId = objPromotionSKU.Id;
			String approvalType='PRODUCT UNLOCK';
			String territoryId = objGerTerritory.Id;
			HEP_ApprovalProcess pr= new HEP_ApprovalProcess();
			Test.startTest();
			Boolean status = pr.invokeApprovalProcess(approvalRecordId,approvalType,territoryId);
			
			Test.stopTest();
	  	}
    }
}