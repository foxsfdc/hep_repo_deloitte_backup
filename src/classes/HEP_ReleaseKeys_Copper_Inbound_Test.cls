/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest (seeAllData = false)
public class HEP_ReleaseKeys_Copper_Inbound_Test {
    @isTest
    static void testPerformTransaction (){
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_InterfaceTxnResponse objWrap = new HEP_InterfaceTxnResponse();
        objWrap.sRequest = '2018-07-20T00:00:00Z';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        HEP_ReleaseKeys_Copper_Inbound copp = new HEP_ReleaseKeys_Copper_Inbound();
        copp.performTransaction(objWrap);
        
        objWrap.sRequest = '2018-06-01T00:00:00Z';
        HEP_ReleaseKeys_Copper_Inbound copp1 = new HEP_ReleaseKeys_Copper_Inbound();
        copp1.performTransaction(objWrap);
        Test.stopTest();
        
    }
}