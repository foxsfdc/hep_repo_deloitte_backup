/**
* TEST_HEP_ProductPricing_To_MM_Batch -- Test class for HEP_ProductPricing_To_MM_Batch 
* 
* @author    Mayank Jaggi
**/ 
@isTest
public class HEP_ProductPricing_To_MM_Batch_Test {
    @testSetup
        static void createUsers(){
            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'jaggi', 'abc_xyz@deloitte.com','majaggi','mayank','majaggi','', true);
            List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
            string templateName = HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change');
            string objName = HEP_Utility.getConstantValue('HEP_PROMOTION');

            HEP_Notification_Template__c notifTemplate = HEP_Test_Data_Setup_Utility.createTemplate(templateName, objName,'has been Approved','Global promotion creation', 'Active', 'Pending','09as09as',false);
            notifTemplate.Unique__c  = 'unique';
            insert notifTemplate;

            HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
            objTerr.Flag_Country_Code__c = 'US';
            insert objTerr; 
            //  HEP_Notification_Template__c notifTemplate = [select id from HEP_Notification_Template__c];
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title', null, '', null, false);
            insert objTitle;
            system.debug('inserted objTitle---> '+ objTitle);

            // HEP_Territory__c objTerr = [select id from HEP_Territory__c where name = 'US' limit 1];
            // system.debug('inserted objTerr---> '+ objTerr);

            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple', objTerr.Id, '11', 'MDP Customers', '', false);
            insert objCustomer;
            system.debug('inserted objCustomer---> '+ objCustomer);

            HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null, objTerr.Id, null, objCustomer.Id, null, false);
            insert objGlobalPromotion;
            system.debug('inserted objGlobalPromotion---> '+ objGlobalPromotion);

            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Customer Promotion', 'Customer', objGlobalPromotion.Id, objTerr.Id, null,null,null,false);
            objPromotion.LocalPromotionCode__c = '0000000032'; 
            objPromotion.StartDate__c = date.today();
            objPromotion.EndDate__c = date.today();
            objPromotion.Status__c = 'Approved'; //HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'); // changed this 6/26
            system.debug('HEP_APPROVAL_APPROVED----------> '+ objPromotion.Status__c);
            objPromotion.Promotion_Type__c = 'Customer'; //HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
            system.debug('PROMOTION_TYPE_CUSTOMER----------> '+ objPromotion.Promotion_Type__c);
            insert objPromotion;
            system.debug('inserted objPromotion---> '+ objPromotion);

            HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
            insert objCat;
            system.debug('inserted objCat---> '+ objCat);

            HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
            insert objPromotionCatalog;
            system.debug('inserted objPromotionCatalog---> '+ objPromotionCatalog);

            HEP_MDP_Promotion_Product__c objPromoProd = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.Id, objTitle.Id, objTerr.Id, false);
            objPromoProd.HEP_Catalog__c = objCat.Id;
            objPromoProd.Approval_Status__c = 'Approved';
            insert objPromoProd;
            system.debug('inserted objPromoProd---> '+ objPromoProd);

            
            // update objPromoProd;

            HEP_MDP_Promotion_Product_Detail__c objpromoproddet = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProd.Id, 'SD', 'ESD', false);
            objpromoproddet.CustomerId__c = '121';
            objpromoproddet.Promo_WSP__c = 60.00;
            objpromoproddet.MM_Currency_Code__c = 'USD';
            objpromoproddet.Channel__c = HEP_Utility.getConstantValue('HEP_CHANNEL_EST');
            insert objpromoproddet;
            system.debug('inserted objpromoproddet---> '+ objpromoproddet);
             HEP_Constants__c MM7 = new HEP_Constants__c();
                MM7.Name = 'HEP_MM_7';
                MM7.Value__c = '7';
                insert MM7;
            
        }

    @isTest
    public static void TEST_HEP_ProductPricing_To_MM_Batch_testmethod(){
        // list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        // list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
       

        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {

            HEP_Promotion__c objPromotion = [Select id,Status__c from HEP_Promotion__c where Promotion_Type__c = 'Customer'];
            objPromotion.Status__c = 'Approved';
            update objPromotion;


            

            Test.startTest();
            HEP_ProductPricing_To_MM_Batch objBatch = new HEP_ProductPricing_To_MM_Batch();
            Database.executeBatch(objBatch);

            //Id batchJobId = Database.executeBatch(new HEP_ProductPricing_To_MM_Batch(), 200);
            
           
            test.stopTest();
        }   
    }
}