/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_Request_Catalog_Srt_Controller_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', true);
        HEP_Territory__c objAutoTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '183', 'JDE', true);
        objAutoTerritory.Auto_Catalog_Creation__c = true;
        update objAutoTerritory;
        HEP_Territory__c objLocalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        objLocalTerritory.CatalogTerritory__c = objAutoTerritory.id;
        insert objLocalTerritory;
        HEP_Territory__c objExtraTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('CANADA', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '188', 'JDE', true);

        //Line of business
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;

        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;

        //Promotion - 1
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestGlobaPromotion', 'Global', '', objGlobalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objGlobalPromotion.LocalPromotionCode__c = '123456789';
        objGlobalPromotion.Title__c = objTitle.id;
        insert objGlobalPromotion;

        //Promotion - 2
        HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestNationalPromotion', 'Global', '', objLocalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objNationalPromotion.LocalPromotionCode__c = '123456789';
        objNationalPromotion.Title__c = objTitle.id;
        insert objNationalPromotion;

        //Promotion - 3
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestNationalPromotion', 'Global', '', objLocalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objPromotion.LocalPromotionCode__c = '123456123';
        objPromotion.Title__c = objTitle.id;
        insert objPromotion;


        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Glocal Catalog', 'Single', null, string.valueof(objGlobalTerritory.id), 'Pending', 'Request', true);
        HEP_Catalog__c objLocalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Local Catalog', 'Single', string.valueof(objGlobalCatalog.id), string.valueof(objLocalTerritory.id), 'Pending', 'Request', true);

        //List of Values
        List < HEP_List_Of_Values__c > lstListofvalues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        //Catalog Genre
        HEP_Catalog_Genre__c objGlobalGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objGlobalCatalog.id, true);
        HEP_Catalog_Genre__c objLocalGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objLocalCatalog.id, true);

    }                                                   

    public static testMethod void test() {
        
        HEP_Catalog__c objGlobalCatalog;
        HEP_Catalog__c objLocalCatalog;

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Catalog__c> lstCatalogs = [SELECT Id, Catalog_Name__c FROM HEP_Catalog__c];
        
       
       	list<HEP_Promotion__c> lstPromotions = [SELECT Id, LocalPromotionCode__c, Territory__c FROM HEP_Promotion__c];

       	HEP_Territory__c objAutoTerritory = [select id from HEP_Territory__c where CatalogTerritory__c != null];
       	list<HEP_Territory__c> lstTerritory = [select id from HEP_Territory__c where CatalogTerritory__c = null];
       	HEP_Territory__c objExtraTerritory = [select id from HEP_Territory__c where name = 'CANADA'];

       	list<EDM_GLOBAL_TITLE__c> lstTitles = [select id from EDM_GLOBAL_TITLE__c];

        for(HEP_Catalog__c objCatalog : lstCatalogs){
            if(objCatalog.Catalog_Name__c == 'Glocal Catalog'){
                objGlobalCatalog = objCatalog;
            }
            if(objCatalog.Catalog_Name__c == 'Local Catalog'){
                objLocalCatalog = objCatalog;
            }
        }

        System.runAs(u) {
            Test.startTest();
	            HEP_Request_Catalog_Start_Controller.getInitialDetails(lstPromotions[0].id, objGlobalCatalog.id);
	            HEP_Request_Catalog_Start_Controller.getInitialDetails(lstPromotions[1].id, null);
	            HEP_Request_Catalog_Start_Controller.catalogFinalValuesWrapper objCatalogFinalValuesWrapper = new HEP_Request_Catalog_Start_Controller.catalogFinalValuesWrapper();
	            List<HEP_Utility.MultiPicklistOrderWrapper> lstGnre = new List<HEP_Utility.MultiPicklistOrderWrapper>();
	            HEP_Utility.MultiPicklistOrderWrapper objGnere = new HEP_Utility.MultiPicklistOrderWrapper('key', 'value', true, 4);
	            lstGnre.add(objGnere);
	            objCatalogFinalValuesWrapper.lstGenres = lstGnre;
	            objCatalogFinalValuesWrapper.sFinantialId = null;
	            objCatalogFinalValuesWrapper.sPromotionCatalogTerritoryId = objAutoTerritory.id;
	            objCatalogFinalValuesWrapper.scatalogType = 'Single';
	            objCatalogFinalValuesWrapper.sPromotionId = lstPromotions[0].id;
	            HEP_Request_Catalog_Start_Controller.saveCatatlog(objCatalogFinalValuesWrapper);
	            objCatalogFinalValuesWrapper.bIsDraft = true;
	            objCatalogFinalValuesWrapper.sPromotionCatalogTerritoryId = objExtraTerritory.id;
	            HEP_Request_Catalog_Start_Controller.saveCatatlog(objCatalogFinalValuesWrapper);
	            objCatalogFinalValuesWrapper.bIsDraft = false;
	            objCatalogFinalValuesWrapper.scatalogType = 'Box Set';
	            objCatalogFinalValuesWrapper.sPromotionCatalogTerritoryId = lstTerritory[0].id;
	            objCatalogFinalValuesWrapper.sCatalogId = objGlobalCatalog.id;
	            objCatalogFinalValuesWrapper.sPromotionId = lstPromotions[1].id;
	            objCatalogFinalValuesWrapper.lstSelectedCatalogs = '[{"sCatalog":"30180","sCatalogId":"a8P3D0000008VdtUAE","sCatalogName":"9 SONGS (COMPETITIVE)","sTerritory":"Germany","sType":"Single","$$hashKey":"object:398"},{"sCatalog":"24717","sCatalogId":"a8P3D0000008VfbUAE","sCatalogName":"ALL ABOUT THE BENJAMINS (COMPETITIVE)","sTerritory":"Germany","sType":"Single","$$hashKey":"object:810"}]';
	            HEP_Request_Catalog_Start_Controller.saveCatatlog(objCatalogFinalValuesWrapper);
	            objCatalogFinalValuesWrapper.scatalogType = 'Coupling';
	            objCatalogFinalValuesWrapper.sPromotionCatalogTerritoryId = lstTerritory[1].id;
	            objCatalogFinalValuesWrapper.sCatalogId = objLocalCatalog.id;
	            objCatalogFinalValuesWrapper.sPromotionId = lstPromotions[2].id;
	            objCatalogFinalValuesWrapper.lstSelectedTitles = '[{"sFinancialTitleId":"7ARG01","sFinancialTitleName":"Modern Family - 07 - Summer Lovin","sInitialUSAirDate":1522800000000,"sTitle":"a4b3D000000CnRgQAK","sTitleType":"Episode","$$hashKey":"object:667"},{"sFinancialTitleId":"7ARG02","sFinancialTitleName":"Modern Family - 07 - Day Alex Left for College, The","sTitle":"a4b3D000000CnRhQAK","sTitleType":"Episode","$$hashKey":"object:678"}]';
	            HEP_Request_Catalog_Start_Controller.saveCatatlog(objCatalogFinalValuesWrapper);

	            HEP_Request_Catalog_Start_Controller.CatalogBoxComponentWrapper objWrapper1 = new HEP_Request_Catalog_Start_Controller.CatalogBoxComponentWrapper('a', 'b', 'c', 'd', 'e');
	            HEP_Request_Catalog_Start_Controller.CatalogCoupleComponentWrapper objWrapper2 = new HEP_Request_Catalog_Start_Controller.CatalogCoupleComponentWrapper('a', 'b', 'c', 'd', 'e');

	            //Updating catalog Name
	            list<hep_catalog__c> lstCatalogsNameUpdate = new list<hep_catalog__c>();
	            for(hep_catalog__c objCatalog : [select id from hep_catalog__c]){
	            	objCatalog.Catalog_Name__c = 'Catalog';
	            	lstCatalogsNameUpdate.add(objCatalog);
	            }
	            update lstCatalogsNameUpdate;

	            HEP_Request_Catalog_Start_Controller.getFinacialTitleId('war', 'Single');
	            HEP_Request_Catalog_Start_Controller.getFinacialTitleId('war', '');
	            HEP_Request_Catalog_Start_Controller.getAllFinancialTitleIds('war', 'Episodes');
	            HEP_Request_Catalog_Start_Controller.getAllEpisodesOfParentTitle(lstTitles[0].id, 'Seasons');
	            HEP_Request_Catalog_Start_Controller.getAllEpisodesOfParentTitle(lstTitles[0].id, 'Episodes');
	            HEP_Request_Catalog_Start_Controller.getAllEpisodesOfSearchedKey('war');
	            HEP_Request_Catalog_Start_Controller.getAllEpisodesOfSearchedKey('war,catalog');
	            HEP_Request_Catalog_Start_Controller.searchCatalog('title', objAutoTerritory.id);
	            HEP_Request_Catalog_Start_Controller.searchCatalog('title,Catalog', null);
	            HEP_Request_Catalog_Start_Controller.getCatalogData('Catalog', null);
	            list<hep_catalog__c> lst1 = [select id,Catalog_Name__c from hep_catalog__c];
	            list<hep_catalog__c> lst2 = [select id from hep_catalog__c where Catalog_Name__c LIKE '%Catalog%'];
	            list<hep_catalog__c> lst3 = [select id from hep_catalog__c  WHERE (Catalog_Name__c LIKE '%Catalog%' OR CatalogId__c LIKE '%Catalog%' ) ORDER BY Catalog_Name__c ASC LIMIT 1001];
				list<hep_catalog__c> lst4 = [select id,Catalog_Name__c from hep_catalog__c WHERE Catalog_Name__c = 'Glocal Catalog'];           

	            system.debug('lst1 : ' + lst1);
	            system.debug('lst2 : ' + lst2);
	            system.debug('lst3 : ' + lst3);
	            system.debug('lst4 : ' + lst4);

	            HEP_Request_Catalog_Start_Controller.getCatalogData('Local Catalog', objAutoTerritory.id);
	            HEP_Request_Catalog_Start_Controller.getCatalogData('Local Catalog, Global Catalog', null);
	            HEP_Request_Catalog_Start_Controller.getCatalogData('Local Catalog/-/', null);
	            HEP_Request_Catalog_Start_Controller.TitleEpisodeWrapper lstEpisodes = new HEP_Request_Catalog_Start_Controller.TitleEpisodeWrapper('a', 'b', 'c', 'd', system.today());
	            HEP_Request_Catalog_Start_Controller.PicklistWrapper lstPicklist = new HEP_Request_Catalog_Start_Controller.PicklistWrapper('a', 'b', 'c');

	            HEP_Request_Catalog_Start_Controller objController = new HEP_Request_Catalog_Start_Controller();
	            PageReference pageRef = Page.HEP_Request_Catalog_Start; // Add your VF page Name here  
	            pageRef.getParameters().put('promoId', String.valueOf(lstPromotions[0].id));
	            Test.setCurrentPage(pageRef);
	            objController.HEP_Request_Catalog_Start_Controller();
	            pageRef.getParameters().put('promoId', String.valueOf(lstPromotions[1].id));
	            Test.setCurrentPage(pageRef);
	            objController.HEP_Request_Catalog_Start_Controller();
	            objController.checkPermissions(); 
            Test.stoptest();
        }
    }
}