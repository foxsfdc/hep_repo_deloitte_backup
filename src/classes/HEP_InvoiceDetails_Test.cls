/**
* HEP_InvoiceDetails_Test -- Test class for the HEP_InvoiceDetails for JDE And E1 Interfaces  
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_InvoiceDetails_Test{
    /**
    * HEP_InvoiceDetails_SuccessTest -- Test method to get Invoice details Successfully
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_InvoiceDetails_SuccessTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_InvoiceDetails','HEP_InvoiceDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objInvoiceWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objInvoiceWrapper.sPromoId = '40550';
        objInvoiceWrapper.sTerritory = 'AU';
        String sInvoiceJson = JSON.serialize(objInvoiceWrapper);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_InvoiceDetails');
        System.assertEquals('HEP_InvoiceDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_InvoiceDetails_FailureTest --  Test method to get Invoice details in case of failure condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_InvoiceDetails_FailureTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_InvoiceDetails','HEP_InvoiceDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objInvoiceWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objInvoiceWrapper.sPromoId = '123456';
        objInvoiceWrapper.sTerritory = 'AU';
        String sInvoiceJson = JSON.serialize(objInvoiceWrapper);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_InvoiceDetails');
        System.assertEquals('HEP_InvoiceDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_InvoiceDetails_InvalidAcessTokenTest -- Test method to get Invoice details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_InvoiceDetails_InvalidAcessTokenTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='JDE_E1_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_InvoiceDetails','HEP_InvoiceDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objInvoiceWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objInvoiceWrapper.sPromoId = '40550';
        objInvoiceWrapper.sTerritory = 'AU';
        String sInvoiceJson = JSON.serialize(objInvoiceWrapper);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_InvoiceDetails');
        System.assertEquals('HEP_InvoiceDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
}