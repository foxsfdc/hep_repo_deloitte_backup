/**
* HEP_BoxServices_Test-- Test class for the HEP_BoxServices. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_BoxServices_Test{
    /**
    * HEP_BoxServices_createData --  Test method for Creating the data
    * @return  :  Promotion records 
    * @author  :  Lakshman Jinnuri
    */
    @isTest static HEP_Promotion__c HEP_BoxServices_createData(){
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        objTerritory.Territory_Code__c = '12355';
        insert objTerritory;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;  
        System.assertEquals('Global Promotion',objGlobalPromotion.PromotionName__c);
        return objGlobalPromotion;
    }   
    /**
    * HEP_BoxServices_createPromoFolders --  Test method for Method createPromoFolders
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolders() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
        Test.stoptest();
    }
    /**
    * HEP_BoxServices_createFolder --  Test method for Method createFolder
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createFolder() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createFolder('json String','Accesstoken');
        Test.stoptest();
    }

    /**
    * HEP_BoxServices_createFolder --  Test method for Method createFolder
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_parseRequestFolder() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.parseRequestFolder('String Name Test','Parent Id');
        Test.stoptest();
    }
    /**
    * HEP_BoxServices_getFolderDetails --  Test method for the getFolderDetails method
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderDetails() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
         HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderDetails((String)objGlobalPromotion.Id);
        Test.stoptest();
    }
    /**
    * HEP_BoxServices_getFolderDetailsBoxId --  Test method for getFolderDetails with Box Id
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderDetailsBoxId() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Promotion_Box_Id__c = '67985';
        objGlobalPromotion.Finance_Box_Root_Id__c = '783684';
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderDetails((String)objGlobalPromotion.Id);
        System.assertEquals('783684',objGlobalPromotion.Finance_Box_Root_Id__c);
        Test.stoptest();
    }

    /**
    * HEP_BoxServices_getFolderDetailsBoxId --  Test method for getFolderDetails with Box Id
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderDetailsBoxId12() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Promotion_Box_Id__c = 'genFolder';
        objGlobalPromotion.Finance_Box_Root_Id__c = 'finFolder';
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderDetails((String)objGlobalPromotion.Id);
        Test.stoptest();
    }
     /**
    * HEP_BoxServices_getFolderDetailsBoxIdErrorTypeCondition --  Test method for the getFolderDetails with type error response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderDetailsBoxIdErrorTypeCondition() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Promotion_Box_Id__c = '679857';
        objGlobalPromotion.Finance_Box_Root_Id__c = '7836847';
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderDetails((String)objGlobalPromotion.Id);
        System.assertEquals('7836847',objGlobalPromotion.Finance_Box_Root_Id__c);
        Test.stoptest();
    }
     /**
    * HEP_BoxServices_getFolderDetailsBoxIdNational --  Test method for getFolderDetails with National Promotion
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderDetailsBoxIdNational() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        //HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        insert objLOB; 
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;  
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        objPromotion.Promotion_Type__c = 'National';
        objPromotion.Promotion_Box_Id__c = '67985';
        objPromotion.Finance_Box_Root_Id__c = '35787';
        objPromotion.Record_Status__c = 'Active';
        update objPromotion; 
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objPromotion.Id);
        HEP_FolderWrapper.RequestFolder promoGeneralRoot = new HEP_FolderWrapper.RequestFolder();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderDetails((String)objPromotion.Id);
        System.assertEquals('67985',objPromotion.Promotion_Box_Id__c);
        Test.stoptest();
    }
    /**
    * HEP_BoxServices_getFolderItems --  Test method for method getFolderItems
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderItems() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderItems('78546','4KmoTBFOG1i5vAawzDaikfAi0nrI');
        Test.stoptest();
    }
     /**
    * HEP_BoxServices_createShareLink --  Test method for method createShareLink
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createShareLink() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createShareLink('78546','4KmoTBFOG1i5vAawzDaikfAi0nrI');
        Test.stoptest();
    }
      /**
    * HEP_BoxServices_createPromoFolder --  Test method for createPromoFolder method
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolder() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Finance_Box_Root_Id__c = '35787';
        objGlobalPromotion.Promotion_Box_Id__c = null;//'67985';
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
       // System.assertEquals('67985',objGlobalPromotion.Promotion_Box_Id__c);
        Test.stoptest();
    }
     /**
    * HEP_BoxServices_createPromoFolder --  Test method for createPromoFolder method
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolder2() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Finance_Box_Root_Id__c = '35787';
        objGlobalPromotion.Promotion_Box_Id__c = null;//'67985';
        objGlobalPromotion.Record_Status__c = 'Active';
        objGlobalPromotion.Promotion_Type__c = 'National';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
       // System.assertEquals('67985',objGlobalPromotion.Promotion_Box_Id__c);
        Test.stoptest();
    }
     /**
    * HEP_BoxServices_createPromoFolder --  Test method for createPromoFolder method
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolder3() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Finance_Box_Root_Id__c = null;
        objGlobalPromotion.Promotion_Box_Id__c = null;//'67985';
        objGlobalPromotion.Record_Status__c = 'Active';
        objGlobalPromotion.Promotion_Type__c = 'Global';
        objGlobalPromotion.PromotionName__c = 'Global Promotion For Failure';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
       // System.assertEquals('67985',objGlobalPromotion.Promotion_Box_Id__c);
        Test.stoptest();
    }


    /**
    * HEP_BoxServices_createPromoFolderNational --  Test method for createPromoFolder method for Type National
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolderNational() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',false);
        objLineOfBusiness.Record_Status__c = 'Active';
        insert objLineOfBusiness;
        HEP_Promotion__c objGProm = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,objTerritory.Id,objLineOfBusiness.id, null,UserInfo.getUserId(), true);
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Finance_Box_Root_Id__c = '35787';
        objGlobalPromotion.Promotion_Box_Id__c = '67985';
        objGlobalPromotion.Record_Status__c = 'Active';
        objGlobalPromotion.Record_Status__c = 'Active';
        objGlobalPromotion.Promotion_Type__c = 'National';
        objGlobalPromotion.Global_Promotion__c = objGProm.Id;
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
       // System.assertEquals('67985',objGlobalPromotion.Promotion_Box_Id__c);
        Test.stoptest();
    }
       /**
    * HEP_BoxServices_createPromoFolderBoxIdEmpty --  Test method for createPromoFolder with empty Box Id
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_createPromoFolderBoxIdEmpty() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Promotion_Type__c = 'Global';
        objGlobalPromotion.Promotion_Box_Id__c = '67985';
        objGlobalPromotion.Finance_Box_Root_Id__c = null;
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.createPromoFolders((String)objGlobalPromotion.Id);
        System.assertEquals('Global',objGlobalPromotion.Promotion_Type__c);
        Test.stoptest();
    }
        /**
    * HEP_BoxServices_createPromoFolderBoxIdEmpty --  Test method for createPromoFolder with empty Box Id
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxServices_getFolderIdFromShareLink() {
        HEP_Promotion__c objGlobalPromotion = HEP_BoxServices_createData();
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        objGlobalPromotion.Promotion_Type__c = 'Global';
        objGlobalPromotion.Promotion_Box_Id__c = '67985';
        objGlobalPromotion.Finance_Box_Root_Id__c = null;
        objGlobalPromotion.Record_Status__c = 'Active';
        update objGlobalPromotion;
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('promoId',objGlobalPromotion.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxServices.getFolderIdFromShareLink('share link','acess token');
        //System.assertEquals('Global',objGlobalPromotion.Promotion_Type__c);
        Test.stoptest();
    }

}