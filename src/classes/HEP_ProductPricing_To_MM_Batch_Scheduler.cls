/*
*   HEP_ProductPricing_To_MM_Batch_Scheduler -- Scheduler for HEP_Product_Pricing_To_MM batch class
*   @author    Mayank Jaggi
*/
global class HEP_ProductPricing_To_MM_Batch_Scheduler implements Schedulable {
   	/*  
    *   execute -- execute method of the scheduler class
    *   @param SchedulableContext
    *   @return void
    *   @author Mayank Jaggi
    */
    global void execute(SchedulableContext sc) {
        HEP_ProductPricing_To_MM_Batch objMMBatch = new HEP_ProductPricing_To_MM_Batch();
        ID jobId = Database.executeBatch(objMMBatch , 200);           
    }
}