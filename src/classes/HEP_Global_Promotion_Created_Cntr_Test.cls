@isTest
public class HEP_Global_Promotion_Created_Cntr_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void test1() {
        //Inserting Territory Record
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
       //Inserting Promotion Record
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, null, null, null, false);
        objPromotion.FirstAvailableDate__c = System.today();
        objPromotion.Domestic_Marketing_Manager__c = u.id;
        objPromotion.International_Marketing_Manager__c = u.id;
        insert objPromotion;
        System.runAs(u) {
            test.startTest();
            HEP_Global_Promotion_Created_Controller obj = new HEP_Global_Promotion_Created_Controller();
            obj.promotionId = objPromotion.id;
            HEP_Global_Promotion_Created_Controller.ApprovalEmailWrapper newobj = new HEP_Global_Promotion_Created_Controller.ApprovalEmailWrapper();
            obj.fetchPromotionDatingDetails();
            test.stopTest();
        }
    }
}