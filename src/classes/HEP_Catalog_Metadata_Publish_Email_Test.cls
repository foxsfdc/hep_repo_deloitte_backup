/**
* HEP_Catalog_Metadata_Publish_Email_Ctrl -- Test class for the HEP_Catalog_Metadata_Publish_Email_Ctrl email alert. 
* @author    Ritesh Konduru
*/
@isTest
private class HEP_Catalog_Metadata_Publish_Email_Test {

    static testMethod void ValidateTest() {



    	User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.SKU_Interface__c = 'ESCO';
        insert objTerritory;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objTerritory.Id, 'Draft', 'Request', false);
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;
    	HEP_Catalog_MetaData__c objCatalogM = HEP_Test_Data_Setup_Utility.createCatalogMetadata(objCatalog.id,u.id, false);
    	system.debug('objCatalogM ' + objCatalogM);
    	objCatalogM.Publish_Comments__c = 'wprComments';
    	objCatalogM.Unique_Id__c ='unique';
        insert objCatalogM;
        System.runAs(u) {       
             HEP_Catalog_Metadata_Publish_Email_Ctrl tempClass = new HEP_Catalog_Metadata_Publish_Email_Ctrl();
            tempClass.recID = objCatalogM.id;
            tempClass.fetchDetails();
            System.assertEquals('1234',objCatalog.CatalogId__c);

          }

    }
}