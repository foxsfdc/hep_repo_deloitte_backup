/**
 * HEP_Catalog_Details_SubTab_Controller---   Class to fetch the catalog Details.
 * @author    Roshi Rai
 */
Public class HEP_Catalog_Details_SubTab_Controller {
    /** 
     **/
    Public class CatalogDetailWrapper {
        String sCatalogName;
        String sCatalogNumber;
        String sCatalogType;
        String sLocalCalatogName; //Editable
        String sTerritory;
        String sLicensorGroup; //Editable
        String sLicensor; //Editable
        String sGenre; //Editable
        String sLicensorType; //Editable
        String sFinancialTitleName;
        String sFinancialTitleID;
        String sProductionYear;
        Boolean bShowEditButton;
       
        public Picklists picklist = new Picklists();
        public HEP_Utility.PicklistWrapper objLicensor;
        public HEP_Utility.PicklistWrapper objLicensorType;
        public HEP_Utility.MultiPicklistOrderWrapper objGenre;
    }
    
    /**
     * Picklists  Class to hold all the picklist field values lookup Field need to be displayed as dropdown(id,value pair)
     * @author    Roshi Rai
     */
    public class Picklists {
        public list < HEP_Utility.PicklistWrapper > lstLicensor;
        public list < HEP_Utility.PicklistWrapper > lstLicensorType;
        public LIST < HEP_Utility.MultiPicklistOrderWrapper > lstGenre;
        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public Picklists() {
            lstLicensor = new list < HEP_Utility.PicklistWrapper > ();
            lstLicensorType = new list < HEP_Utility.PicklistWrapper > ();
            lstGenre = new LIST < HEP_Utility.MultiPicklistOrderWrapper > ();
        }
    }
    
    
    /**
     * Its used to extract details for seleted catalog from Title Master_Catalogs screeen
     * @param catId the Id of the promotion which needs to be updated
     *  
     * @return  object of CatalogDetailWrapper
     */
    @RemoteAction
    public Static CatalogDetailWrapper extractHepCatalogDetailsData(String sCatlogId) {
        map < string, string > mapGenre = new map < string, string > ();
        CatalogDetailWrapper objCatalogDetailWrapper = new CatalogDetailWrapper();
        string HEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        string HEP_RECORD_CANCELLED = HEP_Utility.getConstantValue('HEP_RECORD_CANCELLED');
        list < HEP_Catalog__c > lstHepCatalog = new list < HEP_Catalog__c > ();
        map<string, HEP_Catalog_Genre__c> mapCatalogGenre = new map<string, HEP_Catalog_Genre__c>();
        if (sCatlogId != NULL) {
            lstHepCatalog = [SELECT id, CatalogId__c, Catalog_Name__c, Catalog_Type__c, Digital_Physical__c, Record_Status__c,
                                    Status__c,Local_Catalog_Name__c, Territory__c, Title_EDM__c, Title_EDM__r.Name, Title_EDM__r.FIN_PROD_ID__c, Territory__r.Name,
                                    Title_EDM__r.PROD_CAL_YR__c, Licensor_Group__c, Licensor_Type__c, Licensor__c
                             FROM HEP_Catalog__c
                             WHERE id = : sCatlogId AND (Record_Status__c = :HEP_RECORD_ACTIVE OR Record_Status__c = :HEP_RECORD_CANCELLED) ];
        }

        if (!lstHepCatalog.isEmpty()) {
            
            for (HEP_Catalog__c objHepCatalog: lstHepCatalog) {


               
                //Definning Edit Permission If the User has edit Permission on Page 
                map < String, String > mapCustomPermissions = HEP_Utility.fetchCustomPermissions(objHepCatalog.Territory__c, 'HEP_Catalog_Details_SubTab');
                if (mapCustomPermissions!=null) {
                    if(mapCustomPermissions.get('WRITE').equalsIgnoreCase('FALSE')){
                    objCatalogDetailWrapper.bShowEditButton = false;
                } else {
                    objCatalogDetailWrapper.bShowEditButton = true;
                }
            }
                objCatalogDetailWrapper.sCatalogName = objHepCatalog.Catalog_Name__c;
                objCatalogDetailWrapper.sCatalogNumber = objHepCatalog.CatalogId__c;
                objCatalogDetailWrapper.sCatalogType = objHepCatalog.Catalog_Type__c;
                objCatalogDetailWrapper.sTerritory = objHepCatalog.Territory__r.Name;
                objCatalogDetailWrapper.sFinancialTitleID = objHepCatalog.Title_EDM__r.FIN_PROD_ID__c;
                objCatalogDetailWrapper.sFinancialTitleName = objHepCatalog.Title_EDM__r.Name;
                objCatalogDetailWrapper.sProductionYear = objHepCatalog.Title_EDM__r.PROD_CAL_YR__c;

                objCatalogDetailWrapper.sLocalCalatogName = String.isNotBlank(objHepCatalog.Local_Catalog_Name__c) ? objHepCatalog.Local_Catalog_Name__c :null;
                // Assign the selected  Licensor_Type__c 
                objCatalogDetailWrapper.sLicensorType = objHepCatalog.Licensor_Type__c;
                objCatalogDetailWrapper.objLicensorType = new HEP_Utility.PicklistWrapper(objCatalogDetailWrapper.sLicensorType, objCatalogDetailWrapper.sLicensorType);
                //Assign the selected  Licensor_Group__c
                objCatalogDetailWrapper.sLicensorGroup = objHepCatalog.Licensor_Group__c;
                //Assign the selected  Licensor__c
                objCatalogDetailWrapper.sLicensor = objHepCatalog.Licensor__c;
                objCatalogDetailWrapper.objLicensor = new HEP_Utility.PicklistWrapper(objCatalogDetailWrapper.sLicensor, objCatalogDetailWrapper.sLicensor);
                String newSearchText = '%' + objCatalogDetailWrapper.sTerritory + '%';
                list < HEP_List_Of_Values__c > lstListofValues =  [ SELECT Parent_Value__c, Type__c, Values__c, Order__c, Name,Record_Status__c
                                                                    FROM   HEP_List_Of_Values__c
                                                                    WHERE (Parent_Value__c like: newSearchText AND Type__c = : HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_TYPE') AND   Record_Status__c = :HEP_RECORD_ACTIVE)
                                                                    OR    (Type__c = : HEP_Utility.getConstantValue('HEP_FOX_LICENSOR') AND   Record_Status__c = :HEP_RECORD_ACTIVE)
                                                                    OR    (Type__c = : HEP_Utility.getConstantValue('HEP_FOX_GENRE')   )
                                                                    ORDER BY Values__c
                                                                  ];
             System.debug('*** lstListofValueslstListofValues****' + lstListofValues);   
                if (!lstListofValues.isEmpty()) {
                    for (HEP_List_Of_Values__c objlistval: lstListofValues) {
                        if (objlistval.Type__c == HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_TYPE')) objCatalogDetailWrapper.picklist.lstLicensorType.add(new HEP_Utility.PicklistWrapper(objlistval.Values__c, objlistval.Values__c));
                    }
                    for (HEP_List_Of_Values__c objlistval: lstListofValues) {
                        if (objlistval.Type__c == HEP_Utility.getConstantValue('HEP_FOX_LICENSOR')) objCatalogDetailWrapper.picklist.lstLicensor.add(new HEP_Utility.PicklistWrapper(objlistval.Values__c, objlistval.Values__c));
                    }
                    //Assign the selected  Genre__c
                    list < HEP_Catalog_Genre__c > lstCatGenre = [SELECT id, toLabel(Genre__c), Catalog__c, Record_Status__c,Order__c
                                                                 FROM HEP_Catalog_Genre__c
                                                                 WHERE Catalog__c = : sCatlogId and Record_Status__c = :HEP_RECORD_ACTIVE
                                                                 ORDER BY Order__c];

                    System.debug('*** lstCatGenre****' + lstCatGenre);
                    if (lstCatGenre != null) {
                        for (HEP_Catalog_Genre__c obj: lstCatGenre) {
                            if (obj.Record_Status__c == HEP_RECORD_ACTIVE) {
                            mapCatalogGenre.put(obj.Genre__c, obj);
                        }
                        }
                    }
                    System.debug('**mapCatalogGenre**'+mapCatalogGenre);
                    for (HEP_List_Of_Values__c objlistval: lstListofValues) {
                        if (objlistval.Type__c == HEP_Utility.getConstantValue('HEP_FOX_GENRE')) {
                            //if the filed is selected and set as need to pass the selected value as true
                            if (mapCatalogGenre.containskey(objlistval.Values__c)) {
                                 objCatalogDetailWrapper.picklist.lstGenre.add(new HEP_Utility.MultiPicklistOrderWrapper(objlistval.Values__c, objlistval.Id, true, integer.valueof(mapCatalogGenre.get(objlistval.Values__c).order__C)));
                                  System.debug('***EDIT objCatalogDetailWrapper.picklist.lstGenre**'+objCatalogDetailWrapper.picklist.lstGenre);
                                //objCatalogDetailWrapper.picklist.lstGenre.add(new HEP_Utility.MultiPicklistOrderWrapper(objlistval.Values__c, objlistval.Id, true));
                            } else {
                                  objCatalogDetailWrapper.picklist.lstGenre.add(new HEP_Utility.MultiPicklistOrderWrapper(objlistval.Values__c, objlistval.Id, false, 0));
                            }
                        }
                    }
                }
            }
        }
        System.debug('*****objCatalogDetailWrapper*****' + JSON.serializePretty(objCatalogDetailWrapper));
        return objCatalogDetailWrapper;
    }
    
    
    @RemoteAction
    public Static String extractLicensor(String sValue) {
        String sLicensorGroup;
        if (sValue != null) {
           
            list < HEP_List_Of_Values__c > lstListofValues = [SELECT Parent_Value__c, Type__c, Values__c, Order__c
                                                              FROM HEP_List_Of_Values__c
                                                              WHERE(Name = : sValue AND Type__c = : HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_GROUP') AND Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')) ];
            if (!lstListofValues.isEmpty()) {
                for (HEP_List_Of_Values__c objlstofval: lstListofValues) {
                    sLicensorGroup = objlstofval.Values__c;
                }
            }
        }
       
       return sLicensorGroup;
}
    
    
    /**
     * Its used to save the  details for seleted catalog from Title Master_Catalogs screeen
     * @param  sCatlogId the Id of the promotion which needs to be updated
     *  
     * @return  nothing
     */
    @RemoteAction
    public Static void saveCatalogDetails(CatalogDetailWrapper objPageData, String sCatlogId) {
        try {
            string HEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'); 
            string HEP_DELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED'); 

            //string HEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'); 
            //System.debug('selectedgenrelistselectedgenrelist'+objPageData.selectedgenrelist);

            Integer iOrder = 1;
            List<HEP_Catalog_Genre__c> lstSelectedGenreInsert = new List<HEP_Catalog_Genre__c>();
            List<HEP_Catalog_Genre__c> lstSelectedGenreDelete = new List<HEP_Catalog_Genre__c>();
            map<string, HEP_Catalog_Genre__c> mapCatalogGenre = new map<string, HEP_Catalog_Genre__c>();  

            //Extracting the Promotion that needs to be updated 
            list < HEP_Catalog__c > lstCatalog = [SELECT id, CatalogId__c, Catalog_Name__c, Catalog_Type__c, Licensor_Group__c, Licensor_Type__c, Licensor__c, Record_Status__c,
                                                        Status__c, Territory__c, Title_EDM__c, Title_EDM__r.Name, Title_EDM__r.FIN_PROD_ID__c, Territory__r.Name,
                                                        Title_EDM__r.PROD_CAL_YR__c,Local_Catalog_Name__c
                                                  FROM HEP_Catalog__c
                                                  WHERE id = : sCatlogId ];

            //MultiSelect Picklist which is Child to Catalog to make it active and inactive inser and update on basis of selection made on page
            //List of Geners if picklist value alredy Exsist                                       
            //list < HEP_Catalog_Genre__c > lstGenre = [SELECT id, toLabel(Genre__c), Catalog__c, Record_Status__c
                                                    //  FROM HEP_Catalog_Genre__c
                                                     // WHERE Catalog__c = : sCatlogId ];

             //Create Promotion Genre records or update the existing 
            //Fetch all Genre against the catalog
            list < HEP_Catalog_Genre__c > lstCatGenre = [SELECT id, Name, Catalog__r.Unique_Id__c, Catalog__c, Record_Status__c, toLabel(Genre__c),Order__c
                                                         FROM   HEP_Catalog_Genre__c
                                                         WHERE  Catalog__c = : sCatlogId and Record_Status__c = :HEP_RECORD_ACTIVE order by Order__c];                                         

            //Fetch Genres which are active
            if (lstCatGenre != null) {
                for (HEP_Catalog_Genre__c obj: lstCatGenre) {
                    if (obj.Record_Status__c == HEP_RECORD_ACTIVE) mapCatalogGenre.put(obj.Genre__c, obj);
                }    
            }


            System.debug('****mapCatalogGenre***Save'+mapCatalogGenre);
            System.debug('****mapCatalogGenre***Save'+objPageData.picklist.lstGenre);

            
            //Query catalog to get the unique ID
            list<Hep_Catalog__c> lstCatalogs = [select Unique_Id__c from Hep_Catalog__c where id = :sCatlogId];


            //Process Genre to identify New, deleted type
            for(HEP_Utility.MultiPicklistOrderWrapper objGenre :  objPageData.picklist.lstGenre){

                    HEP_Catalog_Genre__c objCatalogGenre = new HEP_Catalog_Genre__c();
                    objCatalogGenre.Catalog__c = sCatlogId;
                    objCatalogGenre.Genre__c = objGenre.sValue;
                    objCatalogGenre.Record_Status__c = HEP_RECORD_ACTIVE;
                    objCatalogGenre.Unique_Id__c = lstCatalogs[0].Unique_Id__c + ' / ' + objGenre.sValue; 
                    objCatalogGenre.Order__c = iOrder;
                    lstSelectedGenreInsert.add(objCatalogGenre);
                    iOrder += 1;
                    system.debug('objCatalogGenre : ' + objCatalogGenre);
                

               
               mapCatalogGenre.remove(objGenre.sValue);
            }

            system.debug('lstSelectedGenreInsert : ' + lstSelectedGenreInsert);
            system.debug('lstSelectedGenreDelete : ' + lstSelectedGenreDelete);
            //Insert the new Genres
            if(lstSelectedGenreInsert != null && !lstSelectedGenreInsert.isEmpty()){   
               upsert lstSelectedGenreInsert Unique_Id__c;
            }  

            for(HEP_Catalog_Genre__c objGenre : mapCatalogGenre.Values()){
                //HEP_Catalog_Genre__c objCatalogGenre = new HEP_Catalog_Genre__c(id = sGenre);
                objGenre.Record_Status__c = HEP_DELETED;
                lstSelectedGenreDelete.add(objGenre);
            }

            //Remove the deleted Genres
            if(lstSelectedGenreDelete != null && !lstSelectedGenreDelete.isEmpty()){
                update lstSelectedGenreDelete;
            }
            

            
            //Remove the deleted Genres
            if(lstSelectedGenreDelete != null && !lstSelectedGenreDelete.isEmpty()){
               // upsert lstSelectedGenreDelete Unique_Id__c;
            }




            if (lstCatalog != NULL && !lstCatalog.isEmpty()) {
                lstCatalog[0].Licensor_Type__c = objPageData.objLicensorType.sKey;
                lstCatalog[0].Licensor__c = objPageData.objLicensor.sKey;
                lstCatalog[0].Licensor_Group__c = objPageData.sLicensorGroup;

                lstCatalog[0].Local_Catalog_Name__c=objPageData.sLocalCalatogName;

            }
            System.debug('lstCataloglstCatalog' + lstCatalog);
            update lstCatalog;
        }
        // Catch the exception
        catch (DMLException dmlEx) {
            throw dmlEx;
        }
    }
}