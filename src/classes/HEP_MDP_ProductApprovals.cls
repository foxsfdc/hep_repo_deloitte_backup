/**
 * HEP_MDP_ProductApprovals --- Class to do the MDP Product approval submission 
 * @author    Nidhin
 */
public class HEP_MDP_ProductApprovals {
    
    public static String sCUSTOMER, sAPPROVED, sACTIVE, sNATIONAL, sEST, sVOD, s4K;
    public static String sPENDING, sSUBMITTED, sDRAFT, sAPPLE, sI_TUNES;
    static{
        sDRAFT = HEP_Utility.getConstantValue('HEP_CATALOG_STATUS_DRAFT');
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sCUSTOMER = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
        sNATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sEST = HEP_Utility.getConstantValue('HEP_CHANNEL_EST');
        sVOD = HEP_Utility.getConstantValue('HEP_CHANNEL_VOD');
        s4K = HEP_Utility.getConstantValue('HEP_FORMAT_4K');
        sPENDING = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        sSUBMITTED = HEP_Utility.getConstantValue('HEP_PROMOTION_SUBMITTED');
        sAPPLE = HEP_Utility.getConstantValue('Customer_iTunes');
        sI_TUNES = HEP_Utility.getConstantValue('HEP_CUSTOMER_APPLE');
    }

    /**
    * submitForApproval --- submit products for approval
    * params lstPromotionMDPProducts
    * return
    * @author   Nidhin
    */
    public static void submitForApproval(List<HEP_MDP_Promotion_Product__c> lstPromotionMDPProducts, Boolean bIsSubmit){
        Map<Id,Id> mapCustomerProductTerritory = new Map<Id,Id>();
        System.debug('submitForApproval : lstPromotionMDPProducts>>' + lstPromotionMDPProducts);
        //get all promotion products
        List<HEP_MDP_Promotion_Product__c> lstProducts = [SELECT
                                                                Id, Title_EDM__c, Product_Start_Date__c, Product_End_Date__c,HEP_Promotion__r.Id,
                                                                HEP_Promotion__r.Promotion_Type__c, HEP_Territory__c,
                                                                HEP_Promotion__r.LineOfBusiness__r.Type__c, HEP_Promotion__r.Customer__r.CustomerName__c,
                                                                HEP_Promotion__r.Territory__c, Product_Type__c, 
                                                                HEP_Promotion__r.TPR_Only__c, HEP_Promotion__r.Submitted_date__c,
                                                                (SELECT
                                                                    Id, Channel__c, Format__c, Promo_WSP__c, Promo_SRP__c
                                                                FROM
                                                                    HEP_Promotion_MDP_Product_Details__r
                                                                WHERE
                                                                    Record_Status__c = :sACTIVE)
                                                            FROM
                                                                HEP_MDP_Promotion_Product__c
                                                            WHERE
                                                                Id IN :lstPromotionMDPProducts
                                                            AND
                                                                Record_Status__c = :sACTIVE
                                                            AND
                                                                Approval_Status__c = :sDRAFT];
        if(lstProducts == NULL || lstProducts.isEmpty())
            return;
        HEP_Promotion__c objPromotion = lstProducts[0].HEP_Promotion__r;
        Map<Id, CustomerProduct> mapCustomerProduct = new Map<Id, CustomerProduct>();
        Map<Id,List<Id>> mapTitleToProducts = new Map<Id,List<Id>>();
        for(HEP_MDP_Promotion_Product__c objProduct : lstProducts){
            
            Boolean bIsESTOr4K = false, bIsVOD = false;
            CustomerProduct objCustomerProduct = new CustomerProduct();
            //filter customer products for pre approval check
            if(objProduct.Product_Start_Date__c != NULL
                && objProduct.Product_End_Date__c != NULL
                && objProduct.HEP_Promotion__r.Promotion_Type__c == sCUSTOMER){
                
                objCustomerProduct.iDaysBetween = objProduct.Product_Start_Date__c.daysBetween(objProduct.Product_End_Date__c) + 1;
                for(HEP_MDP_Promotion_Product_Detail__c objProductDetail : objProduct.HEP_Promotion_MDP_Product_Details__r){
                    if(objProductDetail.Channel__c == sEST 
                        || (objProductDetail.Format__c == s4K && objProductDetail.Channel__c != sVOD))
                        bIsESTOr4K = true;
                    else if(objProductDetail.Channel__c == sVOD)
                        bIsVOD = true;
                }
                //check for channel rule to be satisfied
                if(bIsESTOr4K && !bIsVOD){
                    if(mapTitleToProducts.containsKey(objProduct.Title_EDM__c))
                        mapTitleToProducts.get(objProduct.Title_EDM__c).add(objProduct.Id);
                    else
                        mapTitleToProducts.put(objProduct.Title_EDM__c, new List<Id>{objProduct.Id});
                        
                    objCustomerProduct.bChannelRuleSatisfied = true;
                    objCustomerProduct.objCustomerProduct = objProduct;
                    mapCustomerProduct.put(objProduct.Id, objCustomerProduct);
                }
                mapCustomerProductTerritory.put(objProduct.Id, objProduct.HEP_Territory__c);
            }
            System.debug('objCustomerProduct>>' + objCustomerProduct);
            System.debug('mapTitleToProducts>>' + mapTitleToProducts);
            System.debug('mapCustomerProductTerritory>>' + mapCustomerProductTerritory);
        }
        System.debug('mapCustomerProduct>>' + mapCustomerProduct);
        //get all national promo products which has the titles with customer promo
        for(HEP_MDP_Promotion_Product__c objProduct : [SELECT
                                                            Id, Title_EDM__c, Product_Start_Date__c, Product_End_Date__c,
                                                            HEP_Territory__c, Duration__c,
                                                            (SELECT
                                                                Id, Channel__c, Format__c, Promo_WSP__c
                                                            FROM
                                                                HEP_Promotion_MDP_Product_Details__r
                                                            WHERE
                                                                Record_Status__c = :sACTIVE)
                                                        FROM
                                                            HEP_MDP_Promotion_Product__c
                                                        WHERE
                                                            HEP_Promotion__r.Promotion_Type__c = :sNATIONAL
                                                        AND
                                                            Title_EDM__c IN :mapTitleToProducts.keySet()
                                                        AND
                                                            Record_Status__c = :sACTIVE
                                                        AND
                                                            Approval_Status__c = :sAPPROVED
                                                        AND
                                                            HEP_Territory__c IN :mapCustomerProductTerritory.values()]){
            for(Id sCustomerProductId : mapTitleToProducts.get(objProduct.Title_EDM__c)){
                if(objProduct.Product_Start_Date__c != NULL
                    && objProduct.Product_End_Date__c != NULL
                    && mapCustomerProductTerritory.get(sCustomerProductId) == objProduct.HEP_Territory__c){
                    
                    CustomerProduct objCustomerProduct = mapCustomerProduct.get(sCustomerProductId);
                    Integer iDaysBetween = Integer.valueOf(objProduct.Duration__c);
                    
                    //check for duration rule
                    System.debug('iDaysBetween>>' + iDaysBetween);
                    System.debug('objCustomerProduct.iDaysBetween>>' + objCustomerProduct.iDaysBetween);
                    if(objCustomerProduct.iDaysBetween <= iDaysBetween
                    	&& objCustomerProduct.objCustomerProduct.Product_Start_Date__c >= objProduct.Product_Start_Date__c
                    	&& objCustomerProduct.objCustomerProduct.Product_End_Date__c <= objProduct.Product_End_Date__c){
                    		
                        System.debug('Inside iDaysBetween>>' + iDaysBetween);
                        Integer iPromoWSPRuleSatisfied = 0;
                        for(HEP_MDP_Promotion_Product_Detail__c objCusProductDetail : objCustomerProduct.objCustomerProduct.HEP_Promotion_MDP_Product_Details__r){
                            for(HEP_MDP_Promotion_Product_Detail__c objNatProductDetail : objProduct.HEP_Promotion_MDP_Product_Details__r){
                                if((objCusProductDetail.Channel__c == sEST
                                    && objNatProductDetail.Channel__c == sEST
                                    && objCusProductDetail.Format__c == objNatProductDetail.Format__c)
                                    ||
                                    (objCusProductDetail.Channel__c != sVOD
                                    && objCusProductDetail.Format__c == s4K
                                    && objNatProductDetail.Format__c == s4K
                                    && objCusProductDetail.Channel__c == objNatProductDetail.Channel__c)){
                                    System.debug('Inside Channel Format objCusProductDetail>>' + objCusProductDetail.Promo_WSP__c);
                                    System.debug('Inside Channel Format objNatProductDetail>>' + objNatProductDetail.Promo_WSP__c);
                                    //check for promoWSP rule
                                    if(objCusProductDetail.Promo_WSP__c == objNatProductDetail.Promo_WSP__c)
                                        iPromoWSPRuleSatisfied += 1;
                                }
                            }
                        }
                        if(iPromoWSPRuleSatisfied != 0
                        	&& iPromoWSPRuleSatisfied == objCustomerProduct.objCustomerProduct.HEP_Promotion_MDP_Product_Details__r.size())
                        	objCustomerProduct.bPromoWSPRuleSatisfied = true;
                    }
                    System.debug('objCustomerProduct>>' + objCustomerProduct);
                }
            }
        }
        List<HEP_MDP_Promotion_Product__c> lstProductsToApprove = new List<HEP_MDP_Promotion_Product__c>();
        List<HEP_Approvals__c> lstApprovals = new List<HEP_Approvals__c>();
        Boolean bIsSubmittedDateUpdated = false;
        List<String> lstAsyncInput = new List<String>();
        //skip approval process if the rules are satisfied
        for(HEP_MDP_Promotion_Product__c objProduct : lstProducts){
            if(mapCustomerProduct.containsKey(objProduct.Id)){
                CustomerProduct objCustomerProduct = mapCustomerProduct.get(objProduct.Id);
                if(objCustomerProduct.bChannelRuleSatisfied == true
                    && objCustomerProduct.bPromoWSPRuleSatisfied == true){
                    
                    System.debug('objProduct>>' + objProduct);
                    System.debug('sAPPLE>>' + sAPPLE);
                    HEP_Approvals__c objApproval = new HEP_Approvals__c();
                    String sCustomerName = objProduct.HEP_Promotion__r.Customer__r.CustomerName__c;
                    System.debug('CustomerName__c>>' + sCustomerName);
                    if(!bIsSubmit 
                        && String.isNotBlank(sCustomerName)
                        && (sCustomerName.contains(sAPPLE)
                            || sCustomerName.contains(sI_TUNES))){
                            
                        System.debug('Inside auto approvals');
                        Boolean bFlag = false;
                        for(HEP_MDP_Promotion_Product_Detail__c objProdDetail : objProduct.HEP_Promotion_MDP_Product_Details__r){
                            if(objProdDetail.Promo_SRP__c == NULL){
                                bFlag = true;
                            }
                        }
                        System.debug('bFlag>>' + bFlag);
                        if(bFlag)
                            objProduct.Approval_Status__c = sDRAFT;
                        else{
                            objProduct.Approval_Status__c = sAPPROVED;
                            objApproval = new HEP_Approvals__c(Record_ID__c = objProduct.Id,
				                            Record_Status__c = sACTIVE,
				                            HEP_MDP_Promotion_Product__c = objProduct.Id,
				                            Approval_Status__c = sAPPROVED);
                            lstApprovals.add(objApproval);
                        }
                    } else{
                        System.debug('Approved');
                        objProduct.Approval_Status__c = sAPPROVED;
                        objApproval = new HEP_Approvals__c(Record_ID__c = objProduct.Id,
			                            Record_Status__c = sACTIVE,
			                            HEP_MDP_Promotion_Product__c = objProduct.Id,
			                            Approval_Status__c = sAPPROVED);
                        lstApprovals.add(objApproval);
                    }
                    if(objPromotion.Submitted_date__c == null
                        && !bIsSubmittedDateUpdated){
                        bIsSubmittedDateUpdated = true;
                        objPromotion.Submitted_date__c = Date.Today();
                    }
                    lstProductsToApprove.add(objProduct);
                } else{
                    if(bIsSubmit){
                        objProduct.Approval_Status__c = sSUBMITTED;
                        lstProductsToApprove.add(objProduct);
                        lstAsyncInput.add(JSON.serialize(objProduct));
                      //  invokeApprovalProcessAsync(JSON.serialize(objProduct));
                    }
                }
            } else{
                if(bIsSubmit){
                    objProduct.Approval_Status__c = sSUBMITTED;
                    lstProductsToApprove.add(objProduct);
                    lstAsyncInput.add(JSON.serialize(objProduct));
                 //   invokeApprovalProcessAsync(JSON.serialize(objProduct));
                }
            }
        }
        
        System.debug('lstProductsToApprove>>' + lstProductsToApprove);
        if(lstProductsToApprove.size() > 0)
            update lstProductsToApprove;
            
        if(lstApprovals.size() > 0)
            insert lstApprovals;
        	
        if(bIsSubmittedDateUpdated)
            update objPromotion;
        
        // logic to split into list of 10 records for processing for future handler
        if(lstAsyncInput.size()>0)
        {
            List <String> lstSendProducts = new List <String> ();
            integer i = 1;
            for(String input : lstAsyncInput)
            {
                lstSendProducts.add(input);
                if(i == 9)
                {
                    invokeApprovalProcessAsync(lstSendProducts);
                    i = 0;
                    lstSendProducts = new List <String>();
                }
                i = i +1;
            }
            // invocation needs to have list of 10 records
            if(lstSendProducts.Size()>0){
                invokeApprovalProcessAsync(lstSendProducts);
            }
            
        }
    }
    
    @future
    Public static void invokeApprovalProcessAsync(List <String> lstJSONProduct){      
        for(String input : lstJSONProduct)
        {
            invokeApprovalProcessSync(input);
        }
    }
    
    /**
    * Process the valid records for the approval process Asynchronously
    * @param  objPromotionSKU : promotion Sku record Id
    * @return 
    * @author Nishit Kedia
    */
  //  @future
    Public static void invokeApprovalProcessSync(String sJSONProduct){
 
        System.debug('sJSONProduct>>' + sJSONProduct);
        try{
            HEP_MDP_Promotion_Product__c objProduct = (HEP_MDP_Promotion_Product__c)JSON.deserialize(sJSONProduct,HEP_MDP_Promotion_Product__c.class);
            //Create Input Records for Approval
            HEP_ApprovalInputWrapper objInputApprovalApi  = new HEP_ApprovalInputWrapper();
            objInputApprovalApi.sApprovalRecordId = objProduct.Id;
            objInputApprovalApi.sApprovalTypeName = (objProduct.HEP_Promotion__r.Promotion_Type__c == sCUSTOMER)?HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_CUSTOMER'):HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_NATIONAL');
            objInputApprovalApi.sterritoryId  =  objProduct.HEP_Promotion__r.Territory__c;
            objInputApprovalApi.sRequesterComment = '';
            objInputApprovalApi.sPriorSubmittersUserIds ='';
            objInputApprovalApi.sLineOfBuisness = objProduct.Product_Type__c;
            // Invoke Approval Process
            HEP_ApprovalProcess objApprovalProcess = new HEP_ApprovalProcess();
            objApprovalProcess.invokeApprovalProcess(objInputApprovalApi);
            
        }catch(Exception objException){
            HEP_Error_Log.genericException('Exception occured when Trashing catalogs','DML Errors',objException,'HEP_TPR_Discounts_Controller','invokeApprovalProcessAsync',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            
        }
        
    }
    
    /**
    * CustomerProduct -- Wrapper to store the product details values
    * @author    Nidhin
    */
    class CustomerProduct{
        HEP_MDP_Promotion_Product__c objCustomerProduct;
        Integer iDaysBetween;
        Boolean bChannelRuleSatisfied, bPromoWSPRuleSatisfied;
        CustomerProduct(){
            objCustomerProduct = new HEP_MDP_Promotion_Product__c();
            bChannelRuleSatisfied = false;
            bPromoWSPRuleSatisfied = false;
            iDaysBetween = 0;
        }
    }
    
    /**
    * getApprovalHierarchy --- get Approval Hierarchy of a Product
    * @param String sProductId
    * @exception Any exception
    * @return ApprovalHierarchy
    */
    @RemoteAction
    public static ApprovalHierarchy getApprovalHierarchy(String sProductId){
        ApprovalHierarchy objApprovalHierarchy = new ApprovalHierarchy();
        Integer iOrderNumber = 0;
        try{
            //populate the list of Approvers of a MDP Product to the ApprovalHierarchy objApprovalHierarchy
            List<HEP_Approvals__c> lstApprovals = [SELECT
	                                                    Id, Record_ID__c , Comments__c , CreatedDate, Owner.Name, Approval_Status__c,
	                                                    CreatedBy.Name, Approval_Type__r.Hierarchy_Type__r.Territory__c,
	                                                    HEP_MDP_Promotion_Product__r.Approval_Status__c,
	                                                    (SELECT 
	                                                        Id, Approver__r.Name, Status__c, 
	                                                        LastModifiedDate, Comments__c, IsActive__c,
	                                                        Approver_Role__r.Name, Approver_Role__r.LOB__c
	                                                    FROM
	                                                        Record_Approvers__r
	                                                    ORDER BY
	                                                        Sequence__c DESC)
	                                                FROM
	                                                    HEP_Approvals__c
	                                                WHERE
	                                                    Record_ID__c = :sProductId
	                                                ORDER BY
	                                                    CreatedDate DESC
	                                                LIMIT 1];
            
            if(lstApprovals == NULL || lstApprovals.size() == 0)
            	return objApprovalHierarchy;   
            HEP_Approvals__c objApproval = lstApprovals[0];
        	System.debug('objApproval>>' + objApproval);
            //Code to create the first approver.
            ApproverWrapper objSubmitter = new ApproverWrapper();                
            //objSubmitter.sName = objApproval.Owner.Name;
            objSubmitter.sStatus = sSUBMITTED;
            objSubmitter.sComments = objApproval.Comments__c;
            objSubmitter.dtDate = objApproval.CreatedDate.date();
            objSubmitter.iOrderNo = iOrderNumber;
            
            List<HEP_User_Role__c> lstUserRoles = [SELECT 
                                                        Id, Role__r.Name, Role__r.LOB__c
                                                    FROM 
                                                        HEP_User_Role__c 
                                                    WHERE 
                                                        User__c =: UserInfo.getUserId() 
                                                    AND 
                                                        Territory__c =: objApproval.Approval_Type__r.Hierarchy_Type__r.Territory__c 
                                                    AND 
                                                        Record_Status__c =: sACTIVE];
            String sRole = '', sLOB = '';
            if(lstUserRoles != null && lstUserRoles.size()>0){
                sRole = lstUserRoles[0].Role__r.Name;
                sLOB = lstUserRoles[0].Role__r.LOB__c;
            }
            objSubmitter.objCompleted = new CompletedBy(objApproval.CreatedBy.Name, sRole, sLOB);
            objApprovalHierarchy.lstApprovers.add(objSubmitter);
            //Code to add the Submitted Status:
            if(objApproval.Record_Approvers__r.size() == 0
            	&& objApproval.HEP_MDP_Promotion_Product__r.Approval_Status__c != sSUBMITTED){
            	++iOrderNumber;
            	ApproverWrapper objApproverWrap = new ApproverWrapper(); 
	            objApproverWrap.sStatus = 'Auto ' + objApproval.Approval_Status__c;
	            objApproverWrap.dtDate = objApproval.CreatedDate.date();
	            objApproverWrap.iOrderNo = iOrderNumber;
            	objApprovalHierarchy.lstApprovers.add(objApproverWrap);
            } else{
	            for(HEP_Record_Approver__c objApprover : objApproval.Record_Approvers__r){
	            
	                ++iOrderNumber;
	                ApproverWrapper objApproverWrapper = new ApproverWrapper();
	                //objApproverWrapper.sName = objApprover.Approver__r.Name;
	                if(objApprover.Status__c.contains(sPENDING)){
	                    objApproverWrapper.sStatus = objApprover.Status__c + ' ' +objApprover.Approver_Role__r.Name;
	                }
	                else{
	                    objApproverWrapper.sStatus = objApprover.Status__c;
	                }
	                String sApproverName = String.isNotBlank(objApprover.Approver__r.Name) ? objApprover.Approver__r.Name : 'TBD';
	                objApproverWrapper.objCompleted = new CompletedBy(sApproverName, objApprover.Approver_Role__r.Name, objApprover.Approver_Role__r.LOB__c);
	                //if(objApprover.Approver_Role__r != NULL)
	                    //objApproverWrapper.sRole = objApprover.Approver_Role__r.Name;
	                objApproverWrapper.sComments = objApprover.Comments__c;
	                if(objApproverWrapper.sStatus.equals(sAPPROVED) 
	                || objApproverWrapper.sStatus.equals(HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED')) 
	                || objApproverWrapper.sStatus.equals(HEP_Utility.getConstantValue('HEP_APPROVAL_CANCELLED')))
	                    objApproverWrapper.dtDate = objApprover.LastModifiedDate.date() ;
	                objApproverWrapper.iOrderNo = iOrderNumber;
	                objApprovalHierarchy.lstApprovers.add(objApproverWrapper);
	            
	            }
            }
            System.debug('Approval Visibility Screen Data --> ' + JSON.serializePretty(objApprovalHierarchy));
            
        }catch(Exception ex){
            HEP_Error_Log.genericException('Error occured while cancelling old Approval Records',
                                             'VF Controller',
                                              ex,
                                             ' HEP_MDP_ProductApprovals',
                                             'getApprovalHierarchy',
                                             null,
                                             '');
            throw ex;
        }
        return objApprovalHierarchy;
    }
    
    /**
    * ApprovalHierarchy --- Wrapper for ApprovalHierarchy
    */
    public class ApprovalHierarchy{
        public List<ApproverWrapper> lstApprovers;
        public ApprovalHierarchy(){
            lstApprovers = new List<ApproverWrapper>();
        }
    }
    
    /**
    * ApproverWrapper --- Wrapper for Approver list
    */
    public class ApproverWrapper{
        public String sStatus, sComments;
        public CompletedBy objCompleted;
        public Date dtDate;
        public Integer iOrderNo;
    }

    public class CompletedBy{
        public String sName, sRole, sLOB;
        public CompletedBy(String sName, String sRole, String sLOB){
            this.sName = sName;
            this.sRole = sRole;
            this.sLOB = sLOB;
        }
    }
    
}