global class HEP_Promotion_SKU_AutoLock_Scheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
      HEP_Promotion_SKU_AutoLock_Batch autoLock = new HEP_Promotion_SKU_AutoLock_Batch(); 
      database.executebatch(autoLock,1);
   }
}