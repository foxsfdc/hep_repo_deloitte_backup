/**
 * HEP_Search_SKU_Controller_Test -- Test class for the HEP_Search_SKU_Controller.
 * @author    Roshi Rai
 */
@isTest
public class HEP_Search_SKU_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void extractAllCatalogData() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        //Inserting Territory Record
        //Inserting Territory Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, true);
        //Map to see the access for territory
        set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_SKU'));
        //Creating Catalog  Record
        //Creating Catalog  Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objCatalog;
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objTerritory.Id, 'SKUTEST', 'KINGSMAN BD', 'Master', 'Physical', 'FOX', 'HD', false);
        //
        objSKU.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        objSKU.SKU_Number__c = 'DV3677';
        objSKU.Barcode__c = '77677556';
        insert objSKU;
        System.runAs(u) {
            test.startTest();
            //Calling extractAllCatalog method
            HEP_Search_SKU_Controller.extractAllSKU(HEP_Utility.getConstantValue('HEP_SKU_NAME'), HEP_Utility.getConstantValue('HEP_CONTAINS'), 'TES');
            HEP_Search_SKU_Controller.extractAllSKU(HEP_Utility.getConstantValue('HEP_SKU_ID'), HEP_Utility.getConstantValue('HEP_CONTAINS'), '367');
            HEP_Search_SKU_Controller.extractAllSKU(HEP_Utility.getConstantValue('HEP_UPC_BARCODE'), HEP_Utility.getConstantValue('HEP_CONTAINS'), '677');
            HEP_Search_SKU_Controller.extractAllSKU(HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD'), HEP_Utility.getConstantValue('HEP_CONTAINS'), 'TES');
            test.stopTest();
        }
    }
}