/* 
 @HEP_Rule_Utility_Test ---Test Class for Rule
 @author : Deloitte
 */

@isTest
public class HEP_Rule_Utility_Test {
    
  @testSetup
  public static void createTestUserSetup(){
    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
    HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
          objInterface.Name = 'HEP_E1_SpendDetail';
          Update objInterface;
    //Create additonal LOVS
    List<HEP_List_Of_Values__c> additionalLov = new List<HEP_List_Of_Values__c>();
    HEP_List_Of_Values__c lov1 = new HEP_List_Of_Values__c();
    lov1.Type__c = 'EMAIL_TO_ROLES';
    lov1.Name = 'HEP_Spends_Email_Notification';
    lov1.Values__c = 'Marketing Manager - TV';
    lov1.Parent_Value__c = 'SPEND';
    lov1.Record_Status__c = 'Active';
    additionalLov.add(lov1);
    
    HEP_List_Of_Values__c lov2 = new HEP_List_Of_Values__c();
    lov2.Type__c = 'EMAIL_CC_USERS';
    lov2.Name = 'HEP_Dating_Approval_Request';
    lov2.Values__c = 'niskedia@deloitte.com';
    lov2.Parent_Value__c = 'Germany';
    lov2.Record_Status__c = 'Active';
    additionalLov.add(lov2);
    
    HEP_List_Of_Values__c lov3 = new HEP_List_Of_Values__c();
    lov3.Type__c = 'NOTIFICATION_TO_ROLES';
    lov3.Name = 'HEP_Spends_Email_Notification';
    lov3.Values__c = 'Marketing Executive - TV';
    lov3.Parent_Value__c = 'SPEND';
    lov3.Record_Status__c = 'Active';
    additionalLov.add(lov3);
    
    HEP_List_Of_Values__c lov4 = new HEP_List_Of_Values__c();
    lov4.Type__c = 'EMAIL_CC_USERS';
    lov4.Name = 'HEP_SKU_Approval_Request';
    lov4.Values__c = 'niskedia@deloitte.com';
    lov4.Parent_Value__c = 'Germany';
    lov4.Record_Status__c = 'Active';
    additionalLov.add(lov4);
    
    HEP_List_Of_Values__c lov5 = new HEP_List_Of_Values__c();
    lov5.Type__c = 'EMAIL_CC_USERS';
    lov5.Name = 'HEP_Dating_Approval_Response';
    lov5.Values__c = 'niskedia@deloitte.com';
    lov5.Parent_Value__c = 'Germany';
    lov5.Record_Status__c = 'Active';
    additionalLov.add(lov5);
    
    insert additionalLov;
    //User Setup
    User objTestUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
    User objTestUser2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest2', 'ApprovalTestUser2@hep.com','ApprovalTest2','App2','ApTest2','', true);
    
    System.runAs (objTestUser) {
    
    //Territory Set up
    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182','ESCO', false);
    insert objTerritory;
    //Global Territory
    HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
    
    //Insert Line of Business records
    HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV','TV',true);
    
    //Role Set Up FOR SPEND
    List<HEP_Role__c> lstRoles = new List<HEP_Role__c>();
    HEP_Role__c objRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPRole('Data Admin', 'Promotions' ,false);
    objRoleAdmin.Destination_User__c = objTestUser.Id;
    objRoleAdmin.Source_User__c = objTestUser.Id;  
    insert objRoleAdmin;

    //User role Set Up  
    List<HEP_User_Role__c > lstUserRole = new List<HEP_User_Role__c >();
    HEP_User_Role__c objUserRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleAdmin.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleAdmin);
    insert lstUserRole;
    
    //Rule and Criteria Setup
    List<HEP_Rule__c> lstRules = new List<HEP_Rule__c>();
    HEP_Rule__c objRule1 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 1','HEP_Market_Spend__c','(1 AND (2 OR 3))',false);
    lstRules.add(objRule1) ;
    
    HEP_Rule__c objRule2 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 2','HEP_Market_Spend__c','(1 AND 2)',false);
    lstRules.add(objRule2) ;
    
    HEP_Rule__c objRule3 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 3','HEP_Record_Approver__c','((1 OR 2) OR 3)',false);
    lstRules.add(objRule3) ;
    
    HEP_Rule__c objRule4 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 4','HEP_Record_Approver__c','((1 OR 2) AND 3)',false);
    lstRules.add(objRule4) ;
    
    HEP_Rule__c objRule5 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 5','HEP_Record_Approver__c','(1)',false);
    lstRules.add(objRule5) ;
    
    HEP_Rule__c objRule6 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 6','HEP_Record_Approver__c','(1 OR 2)',false);
    lstRules.add(objRule6) ;
    
    HEP_Rule__c objRule7 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 7','HEP_Record_Approver__c','((1 AND 2) OR 3)',false);
    lstRules.add(objRule7) ;
    
    HEP_Rule__c objRule8 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 8','HEP_Record_Approver__c','((1 OR 2) AND 3)',false);
    lstRules.add(objRule8) ; 
    
    HEP_Rule__c objRule9 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 9','HEP_Market_Spend__c','(1 OR (2 OR 3))',false);
    lstRules.add(objRule9) ; 
    
    HEP_Rule__c objRule10 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 10','HEP_Market_Spend__c','(1 OR (2 OR 3))',false);
    lstRules.add(objRule10) ;   
    
    HEP_Rule__c objRule11 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 11','HEP_Market_Spend__c','(1 OR (2 OR 3))',false);
    lstRules.add(objRule11) ;   
    insert lstRules;
    
    Id RecordTypeId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName().get('Compare Field Value').getRecordTypeId();
    
    //For SPEND
    List<HEP_Rule_Criterion__c> lstRuleCriterias = new List<HEP_Rule_Criterion__c>();
    HEP_Rule_Criterion__c objRuleCriteria1 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Approval_LOB__c','TV',HEP_Rule_Constants.HEP_STRING_EQUALS_TO,objRule1.Id,RecordTypeId, false);
    objRuleCriteria1.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria1.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria1);
    HEP_Rule_Criterion__c objRuleCriteria2 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','2000',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule1.Id,RecordTypeId, false);
    objRuleCriteria2.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria2.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria2);
    HEP_Rule_Criterion__c objRuleCriteria3 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Record_Status__c','Active',HEP_Rule_Constants.HEP_STRING_EQUALS_TO,objRule1.Id,RecordTypeId, false);
    objRuleCriteria3.HEP_Source_SObject_Field_Type__c ='STRING';
    objRuleCriteria3.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria3);
    
    Datetime todaysDateTime = System.now();
    String formatteddate = todaysDateTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
    
    HEP_Rule_Criterion__c objRuleCriteria4 = HEP_Test_Data_Setup_Utility.createRuleCriteria('RequestDate__c',formatteddate,HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule2.Id,RecordTypeId, false);
    objRuleCriteria4.HEP_Source_SObject_Field_Type__c ='DATE';
    objRuleCriteria4.HEP_Serial_Number__c=1;
    lstRuleCriterias.add(objRuleCriteria4);
    
    HEP_Rule_Criterion__c objRuleCriteria5 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CreatedDate',String.valueOfGmt(todaysDateTime),HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule2.Id,RecordTypeId, false);
    objRuleCriteria5.HEP_Source_SObject_Field_Type__c= 'DATETIME';
    objRuleCriteria5.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria5);
    
    
    HEP_Rule_Criterion__c objRuleCriteria6 = HEP_Test_Data_Setup_Utility.createRuleCriteria('IsActive__c','True',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
    objRuleCriteria6.HEP_Source_SObject_Field_Type__c= 'BOOLEAN';
    objRuleCriteria6.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria6);
    
    HEP_Rule_Criterion__c objRuleCriteria7 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Status__c','Approved',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
    objRuleCriteria7.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria7.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria7);
    HEP_Rule_Criterion__c objRuleCriteria8 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Sequence__c','2',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
    objRuleCriteria8.HEP_Source_SObject_Field_Type__c= 'INTEGER';
    objRuleCriteria8.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria8);
    
    HEP_Rule_Criterion__c objRuleCriteria9 = HEP_Test_Data_Setup_Utility.createRuleCriteria('IsActive__c','False',HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule4.Id,RecordTypeId, false);
    objRuleCriteria9.HEP_Source_SObject_Field_Type__c= 'BOOLEAN';
    objRuleCriteria9.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria9);
    
    HEP_Rule_Criterion__c objRuleCriteria10 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Status__c','Approved',HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule4.Id,RecordTypeId, false);
    objRuleCriteria10.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria10.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria10);
    
    HEP_Rule_Criterion__c objRuleCriteria11 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Sequence__c','4',HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule4.Id,RecordTypeId, false);
    objRuleCriteria11.HEP_Source_SObject_Field_Type__c= 'INTEGER';
    objRuleCriteria11.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria11);
    
    todaysDateTime = todaysDateTime.addDays(20);
    formatteddate = todaysDateTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
    HEP_Rule_Criterion__c objRuleCriteria24 = HEP_Test_Data_Setup_Utility.createRuleCriteria('RequestDate__c',formatteddate,HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule10.Id,RecordTypeId, false);
    objRuleCriteria24.HEP_Source_SObject_Field_Type__c ='DATE';
    objRuleCriteria24.HEP_Serial_Number__c=1;
    lstRuleCriterias.add(objRuleCriteria24);
    
    HEP_Rule_Criterion__c objRuleCriteria25 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CreatedDate',String.valueOfGmt(todaysDateTime),HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule10.Id,RecordTypeId, false);
    objRuleCriteria25.HEP_Source_SObject_Field_Type__c= 'DATETIME';
    objRuleCriteria25.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria25);
    
     HEP_Rule_Criterion__c objRuleCriteria26 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','200',HEP_Rule_Constants.HEP_STRING_NOT_EQUALS_TO ,objRule10.Id,RecordTypeId, false);
    objRuleCriteria26.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria26.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria26);
    
    
    HEP_Rule_Criterion__c objRuleCriteria12 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Sequence__c','1',HEP_Rule_Constants.HEP_STRING_GREATER_THAN ,objRule5.Id,RecordTypeId, false);
    objRuleCriteria12.HEP_Source_SObject_Field_Type__c= 'INTEGER';
    objRuleCriteria12.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria12);
    
    
    HEP_Rule_Criterion__c objRuleCriteria13 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CreatedDate',String.valueOfGmt(todaysDateTime.addDays(-20)),HEP_Rule_Constants.HEP_STRING_GREATER_THAN_OR_EQUALS_TO ,objRule6.Id,RecordTypeId, false);
    objRuleCriteria13.HEP_Source_SObject_Field_Type__c= 'DATETIME';
    objRuleCriteria13.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria13);
    
    HEP_Rule_Criterion__c objRuleCriteria14 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Status__c','Approved',HEP_Rule_Constants.HEP_STRING_GREATER_THAN_OR_EQUALS_TO ,objRule6.Id,RecordTypeId, false);
    objRuleCriteria14.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria14.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria14);
    
    HEP_Rule_Criterion__c objRuleCriteria15 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CreatedDate',String.valueOfGmt(todaysDateTime),HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO ,objRule7.Id,RecordTypeId, false);
    objRuleCriteria15.HEP_Source_SObject_Field_Type__c= 'DATETIME';
    objRuleCriteria15.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria15);
    
    HEP_Rule_Criterion__c objRuleCriteria16 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Status__c','Approved',HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO ,objRule7.Id,RecordTypeId, false);
    objRuleCriteria16.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria16.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria16);
    
    HEP_Rule_Criterion__c objRuleCriteria17 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Sequence__c','2',HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO ,objRule7.Id,RecordTypeId, false);
    objRuleCriteria17.HEP_Source_SObject_Field_Type__c= 'INTEGER';
    objRuleCriteria17.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria17);
    
    HEP_Rule_Criterion__c objRuleCriteria18 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CreatedDate',String.valueOfGmt(todaysDateTime.addDays(9)),HEP_Rule_Constants.HEP_STRING_LESS_THAN ,objRule8.Id,RecordTypeId, false);
    objRuleCriteria18.HEP_Source_SObject_Field_Type__c= 'DATETIME';
    objRuleCriteria18.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria18);
    
    HEP_Rule_Criterion__c objRuleCriteria19 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Status__c','Cancelled',HEP_Rule_Constants.HEP_STRING_LESS_THAN ,objRule8.Id,RecordTypeId, false);
    objRuleCriteria19.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria19.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria19);
    
    HEP_Rule_Criterion__c objRuleCriteria20 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Sequence__c','30',HEP_Rule_Constants.HEP_STRING_LESS_THAN ,objRule8.Id,RecordTypeId, false);
    objRuleCriteria20.HEP_Source_SObject_Field_Type__c= 'INTEGER';
    objRuleCriteria20.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria20);
    
    HEP_Rule_Criterion__c objRuleCriteria21 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Approval_LOB__c','TV',HEP_Rule_Constants.HEP_STRING_GREATER_THAN,objRule9.Id,RecordTypeId, false);
    objRuleCriteria21.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria21.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria21);
    
    HEP_Rule_Criterion__c objRuleCriteria22 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','1000',HEP_Rule_Constants.HEP_STRING_GREATER_THAN ,objRule9.Id,RecordTypeId, false);
    objRuleCriteria22.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria22.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria22);
    
    todaysDateTime = todaysDateTime.addDays(-20);
    formatteddate = todaysDateTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
    HEP_Rule_Criterion__c objRuleCriteria23 = HEP_Test_Data_Setup_Utility.createRuleCriteria('RequestDate__c',formatteddate,HEP_Rule_Constants.HEP_STRING_GREATER_THAN_OR_EQUALS_TO,objRule9.Id,RecordTypeId, false);
    objRuleCriteria23.HEP_Source_SObject_Field_Type__c ='DATE';
    objRuleCriteria23.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria23);
    
    HEP_Rule_Criterion__c objRuleCriteria27 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','1000',HEP_Rule_Constants.HEP_STRING_GREATER_THAN_OR_EQUALS_TO ,objRule11.Id,RecordTypeId, false);
    objRuleCriteria27.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria27.HEP_Serial_Number__c = 1;
     lstRuleCriterias.add(objRuleCriteria27);
     
    HEP_Rule_Criterion__c objRuleCriteria28 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','10000',HEP_Rule_Constants.HEP_STRING_LESS_THAN ,objRule11.Id,RecordTypeId, false);
    objRuleCriteria28.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria28.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria28);
    
    todaysDateTime = todaysDateTime.addDays(77);
    formatteddate = todaysDateTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
    HEP_Rule_Criterion__c objRuleCriteria29 = HEP_Test_Data_Setup_Utility.createRuleCriteria('RequestDate__c',formatteddate,HEP_Rule_Constants.HEP_STRING_LESS_THAN,objRule11.Id,RecordTypeId, false);
    objRuleCriteria29.HEP_Source_SObject_Field_Type__c ='DATE';
    objRuleCriteria29.HEP_Serial_Number__c = 3;
    lstRuleCriterias.add(objRuleCriteria29);
    
    insert lstRuleCriterias;
    
    //Create Global Promotion:
    HEP_Promotion__c objTestGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Global','Global','',objGlobalTerritory.Id,objLOB.Id,'','',true);
    //Create National Promotion
    HEP_Promotion__c objTestNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Germany NP','National',objTestGlobalPromotion.Id,objTerritory.Id,objLOB.Id,'','',true);
    
    //Notification Template Creation
    HEP_Notification_Template__c objGlobalPromoTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Creation','HEP_Promotion__c','A global promotion has been created','Global promotion creation','Active','-','Global_Promotion_Creation',true);
    HEP_Notification_Template__c objSpendNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Request_Notification','HEP_Market_Spend__c','A new total budget request of $<Request_Amount__c> has been submitted for approval.','Budget Approved','Active','-','Budget_Update_Request_Notification',true);
    HEP_Notification_Template__c objSpendAppNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Approved','HEP_Market_Spend__c','A Budget has been Approved','Budget Request','Active','Rejected','Budget_Update_Approved',true);
    HEP_Notification_Template__c objSpendRejNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Rejected','HEP_Market_Spend__c','A new total budget request on <Promotion__r.Name> is rejected','Budget Request','Active','Budget Approved','Budget_Update_Rejected',true);
    HEP_Notification_Template__c objfadNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','A new FAD is done','FAD Date Change','Active','-','Global_FAD_Date_Change',true);
    
    }
  }

    public static TestMethod void  tesRuleUtility(){
    
    //Fetch the created Test User:
    User objTestUser = [Select  Id,
                                Name 
                                from User 
                                where Email = 'ApprovalTestUser@hep.com'];
     
     //fetch promotion:
    HEP_Promotion__c objTestPromotion = [Select Id,
                          Territory__c,
                          Name 
                          from HEP_Promotion__c 
                          where Promotion_Type__c ='National'];
     
     HEP_Role__c role = [Select Id from HEP_Role__c];
     
     
     
     System.runAs(objTestUser){
      //Create Spend:
    HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',false);
    objTestMarketSpend.RequestDate__c = Date.today();
    insert objTestMarketSpend;
    
    //GL Account
    HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
    
    //create Spend Details
    HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
    objTestSpendDetails.RequestAmount__c = 2000;
    objTestSpendDetails.Budget__c =50000;
    objTestSpendDetails.Type__c ='Current';
    insert objTestSpendDetails;
    
     HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('SPEND Approval Type', 'HEP_Market_Spend__c', 'RecordStatus__c', 
                                                                                            'Budget Approved','Rejected','SPEND',
                                                                                            'Hierarchical','Pending',
                                                                                            true);
                                                                                            
    HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Approved',true);
    List <HEP_Record_Approver__c> rappInsertList = new List<HEP_Record_Approver__c>();
    
    HEP_Record_Approver__c objRecord1= HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, objTestUser.Id,role.Id,'Pending',false);
    objRecord1.Sequence__c = 1;
    objRecord1.IsActive__c = true;
    objRecord1.Record_Status__c = 'Active';
    rappInsertList.add(objRecord1);
    
    HEP_Record_Approver__c objRecord2= HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, objTestUser.Id,role.Id ,'Pending',false);
    objRecord2.Sequence__c = 2;
    objRecord2.IsActive__c = true;
    objRecord2.Record_Status__c = 'Inactive';
        rappInsertList.add(objRecord2);
        insert rappInsertList;
        
        Test.startTest();
        
        HEP_Market_Spend__c objMarketSpendRecord = [Select Id,Approval_LOB__c,Request_Amount__c,RequestDate__c,CreatedDate,Record_Status__c from HEP_Market_Spend__c];
        Map <Id,HEP_Rule__c> ruleMapforSpend = HEP_Rule_Utility.getActiveRules('HEP_Market_Spend__c');
        HEP_Rule_Utility.evaluateRuleCriteria(objMarketSpendRecord,ruleMapforSpend.values());
        
        List<HEP_Record_Approver__c> rappList = [Select Id,CreatedDate,Sequence__c,IsActive__c,Record_Status__c,Status__c,Search_Id__c from HEP_Record_Approver__c];
        Map <Id,HEP_Rule__c> ruleMapforRApprover = HEP_Rule_Utility.getActiveRules('HEP_Record_Approver__c');
        
        HEP_Rule_Utility.evaluateRuleCriteria(rappList.get(0),ruleMapforRApprover.values());
        HEP_Rule_Utility.evaluateRuleCriteria(rappList.get(1),ruleMapforRApprover.values());
        
        
        
        Test.stopTest();
     }
    }
    
    public static TestMethod void  tesRuleUtilityException(){
   
    //Fetch the created Test User:
    User objTestUser = [Select  Id,
                                Name 
                                from User 
                                where Email = 'ApprovalTestUser@hep.com'];
     
     //fetch promotion:
    HEP_Promotion__c objTestPromotion = [Select Id,
                          Territory__c,
                          Name 
                          from HEP_Promotion__c 
                          where Promotion_Type__c ='National'];
     
     HEP_Role__c role = [Select Id from HEP_Role__c];
     
     HEP_Rule__c objRule12 = HEP_Test_Data_Setup_Utility.createRule('Test Rule 12','HEP_Market_Spend__c','(1 OR (2 OR 3))',false);
     insert   objRule12;
    
    Id RecordTypeId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName().get('Compare Field Value').getRecordTypeId();
    
    HEP_Rule_Criterion__c objRuleCriteria = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','10000',HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO ,objRule12.Id,RecordTypeId, false);
    objRuleCriteria.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria.HEP_Serial_Number__c = 1;
    insert objRuleCriteria;
    
    DateTime todaysDateTime = System.now().addDays(5);
    String formatteddate = todaysDateTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
    HEP_Rule_Criterion__c objRuleCriteria1 = HEP_Test_Data_Setup_Utility.createRuleCriteria('RequestDate__c',formatteddate,HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO,objRule12.Id,RecordTypeId, false);
    objRuleCriteria1.HEP_Source_SObject_Field_Type__c ='DATE';
    objRuleCriteria1.HEP_Serial_Number__c = 2;
    insert objRuleCriteria1;
    
    HEP_Rule_Criterion__c objRuleCriteria2 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Approval_LOB__c','TV',HEP_Rule_Constants.HEP_STRING_LESS_THAN_OR_EQUALS_TO,objRule12.Id,RecordTypeId, false);
    objRuleCriteria2.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria2.HEP_Serial_Number__c = 3;
    insert objRuleCriteria2;
     
     System.runAs(objTestUser){
      //Create Spend:
    HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',false);
    objTestMarketSpend.RequestDate__c = Date.today();
    insert objTestMarketSpend;
    
    //GL Account
    HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
    
    //create Spend Details
    HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
    objTestSpendDetails.RequestAmount__c = 3000;
    objTestSpendDetails.Budget__c =50000;
    objTestSpendDetails.Type__c ='Current';
    insert objTestSpendDetails;
    
     HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('SPEND Approval Type', 'HEP_Market_Spend__c', 'RecordStatus__c', 
                                                                                            'Budget Approved','Rejected','SPEND',
                                                                                            'Hierarchical','Pending',
                                                                                            true);
                                                                                            
    HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Approved',true);
    List <HEP_Record_Approver__c> rappInsertList = new List<HEP_Record_Approver__c>();
    
    HEP_Record_Approver__c objRecord1= HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, objTestUser.Id,role.Id,'Pending',false);
    objRecord1.Sequence__c = 1;
    objRecord1.IsActive__c = false;
    objRecord1.Record_Status__c = 'Active';
    rappInsertList.add(objRecord1);
    
    HEP_Record_Approver__c objRecord2= HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, objTestUser.Id,role.Id ,'Pending',false);
    objRecord2.Sequence__c = 2;
    objRecord2.IsActive__c = true;
    objRecord2.Record_Status__c = 'Inactive';
        rappInsertList.add(objRecord2);
        insert rappInsertList;
        
        Test.startTest();
        
        HEP_Market_Spend__c objMarketSpendRecord = [Select Id,Approval_LOB__c,Request_Amount__c,RequestDate__c,CreatedDate,Record_Status__c from HEP_Market_Spend__c];
        Map <Id,HEP_Rule__c> ruleMapforSpend = HEP_Rule_Utility.getActiveRules('HEP_Market_Spend__c');
        HEP_Rule_Utility.evaluateRuleCriteria(objMarketSpendRecord,ruleMapforSpend.values());
        
        List<HEP_Record_Approver__c> rappList = [Select Id,CreatedDate,Sequence__c,IsActive__c,Record_Status__c,Status__c,Search_Id__c from HEP_Record_Approver__c];
        Map <Id,HEP_Rule__c> ruleMapforRApprover = HEP_Rule_Utility.getActiveRules('HEP_Record_Approver__c');
        
        HEP_Rule_Utility.evaluateRuleCriteria(rappList.get(0),ruleMapforRApprover.values());
        HEP_Rule_Utility.evaluateRuleCriteria(objRecord2,ruleMapforRApprover.values());    
        Test.stopTest();
     }
    }
     
}