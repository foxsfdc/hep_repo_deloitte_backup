/**
* HEP_Siebel_Catalog_GenreWrapper -- Wrapper to store Catalog Genre details for Siebel Integrations 
* @author    Lakshman Jinnuri
*/
public class HEP_Siebel_Catalog_GenreWrapper{
    public String ProducerID;   
    public String EventType;    
    public String EventName;    
    public Data Data;
    
    /**
    * constructor to initialize the class variables
    * @return nothing 
    * @author Lakshman Jinnuri 
    */    
    public HEP_Siebel_Catalog_GenreWrapper(){
        this.Data = new Data();
    }
    
    /**
    * Data -- Class to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Payload Payload;
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */
        public Data(){
            this.Payload = new Payload();
        }
    }
    
    /**
    * Payload -- Class to store Payload details 
    * @author    Lakshman Jinnuri
    */
    public class Payload {
        public list<Items> items;
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */
        public Payload(){
            items = new list<Items>();
        }
    }
    
    /**
    * Items -- Class to store Items details 
    * @author    Lakshman Jinnuri
    */
    public class Items {
        public String objectType;   
        public String createdDate;  
        public String createdBy;    
        public String updatedDate;  
        public String updatedBy;    
        public String recordId; 
        public String recordStatus; 
        public String territoryId;  
        public String territoryName;    
        public String catalogId;    
        public String catalogNumber;   
        public String genre;    
        public String orderBy;
        public String legacyId; 
    }   
}