public class hep_po_invoice_data_controller{
         
         map<Id,Department> mapDept;
         map<String,headerData> mapDeptPO;  //for key we will use ParentDept+'-'+PONum
         

         public static void getResponse(String POResponse){
         if(POResponse!= null){
         System.debug('****'+POResponse);
         HEP_purchaseOrderWrapper poInst = (HEP_purchaseOrderWrapper)JSON.deserialize(POResponse,HEP_purchaseOrderWrapper.class);
         system.debug('poInst' +poInst);
         system.debug('poInst.purchaseOrders***' +poInst.purchaseOrders);
         system.debug('poInst.purchaseOrders[0].FORMAT' + poInst.purchaseOrders[0].Format);
         List<HEP_purchaseOrderWrapper.purchaseOrders> lstPO = poInst.purchaseOrders;
         system.debug('lstPO ' +lstPO );
         system.debug('poInst.purchaseOrders[0].Country' +poInst.purchaseOrders[0].Country);
         //List<String> pNum = poInst.purchaseOrders.purchaseOrders[0].PROMO; 
         String promo = lstPo[0].PROMO;
         String territory = lstPo[0].Country;
         //system.debug('FORMAT****'+poFormat);
         //system.debug(pNum);
         /*for ( Schema.ChildRelationship scr : HEP_GL_Account__c.sObjectType.getDescribe().getchildRelationships() ){
        system.debug('');
        system.debug('**** Child SObject         **** ' + scr.getChildSObject());
        system.debug('     Field                 **** ' + scr.getField());
        system.debug('     Relationship Name     **** ' + scr.getRelationshipName());
        system.debug('     Cascade Delete        **** ' + scr.isCascadeDelete());
        system.debug('     Deprecated and Hidden **** ' + scr.isDeprecatedAndHidden());
        system.debug('     Restricted Delete     **** ' + scr.isRestrictedDelete());
}*/
         
         //String sPromotionTerr = [SELECT Territory__r.Name from HEP_Promotion__c WHERE LocalPromotionCode__c =:]
         
         map<Id,HEP_GL_Account__c> mapGLAccount = new map<Id,HEP_GL_Account__c>([SELECT id,Format__c,GLAccount__c,GLAccountDescription__c,
                                                   Parent_GL_Account__c,Record_Status__c
                                                   FROM HEP_GL_Account__c
                                                   WHERE GLAccountGroup__c='Marketing Spend']);     //could not find promotion or territory on GL_Account to filter on territory
        system.debug(mapGLAccount);
         getPODetails(poInst );
         
         }
         }
         
        //For PO details
        
         public static void getPODetails(HEP_purchaseOrderWrapper poData){
         List<HEP_purchaseOrderWrapper.purchaseOrders> lstPODetails = new  List<HEP_purchaseOrderWrapper.purchaseOrders>();//Loop through PO JSON // do mapping for format code to SFDC value // getPODetails
         List<HEP_purchaseOrderWrapper.purchaseOrders> lstPO = poData.purchaseOrders;
         system.debug('inside getPODetails' +lstPO);
         for(HEP_purchaseOrderWrapper.purchaseOrders pow : lstPO)
         {      system.debug('pow****' + pow);
                System.debug('FORMAT*************' +pow.FORMAT);
                if(pow.FORMAT == null)
                {
                    pow.FORMAT = '';
                }
                else
                {
                if(pow.FORMAT!= '980' && (pow.FORMAT == '910' || pow.FORMAT == '915' || pow.FORMAT == '925' || pow.FORMAT =='930' || pow.FORMAT == '935')){
                pow.FORMAT = 'PHY'; }
                else if(pow.FORMAT!= '980' && pow.FORMAT == '950'){
                pow.FORMAT = 'VOD'; }
                else if(pow.FORMAT!= '980' && pow.FORMAT == '960') {
                pow.FORMAT = 'DHD'; }
                else if(pow.FORMAT == '980'){
                pow.FORMAT = 'VR'; }
                }
                lstPODetails.add(pow);
         }
         system.debug('lstPODetails' +lstPODetails);
         
         } 

        public class lineData{
         public String sLineNo{get;set;}
         public String sGLAccount{get;set;}
         public String sFormat{get;set;}
         public String sLineDesc{get;set;}
         public String sLinestatus{get;set;}
         public String sAmount{get;set;}
         public String sActuals{get;set;}
         public String sOpenAmount{get;set;}
         public String sPoNum{get;set;}
         public String sCountry{get;set;}
        }

        public class headerData{            //ParentNode
         public String sPoNum{get;set;}
         public String sStatus{get;set;}
         public Date dtIssueData{get;set;}
         public String sAmount{get;set;}
         public String sCurrency{get;set;}
         public String sRequestor{get;set;}
         public String sGLAccount{get;set;}
         public String sFormat{get;set;}
         public String sComments{get;set;}
         public List<lineData> lstLineData{get;set;}
        }

        public class Department{
         public String depName{get;set;}
         public String sGLAccount{get;set;}
         public Decimal PaidActual{get;set;}
         public Decimal Commitments{get;set;}
         public Department(){
         PaidActual = 0;
         Commitments = 0;
         }
         
        }

        public class outerWrapper{

            public List<Department> lstDept;

        }

        }