/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_Promotion_Financials_Controller_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Line of business
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', false);
        insert objGlobalTerritory; 

        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;

        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;

        //Promotion
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion', 'Global', '', objGlobalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objPromotion.LocalPromotionCode__c = '123456789';
        objPromotion.Title__c = objTitle.id;
        insert objPromotion;

        //HEP Ultimates
        List<HEP_Ultimates__c> lstUltimates = HEP_Test_Data_Setup_Utility.createHEPUltimates();
        for(HEP_Ultimates__c objUltimate : lstUltimates){
            objUltimate.Title__c = objTitle.id;
        }
        update lstUltimates;

    }

    public static testMethod void test1() {
        //createData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Promotion__c> lstPromotions = [SELECT Id, LocalPromotionCode__c, Territory__c FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
       
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Financials_Controller.getFinancialDetails(lstPromotions[0].id);
            delete lstPromotions;
            HEP_Promotion_Financials_Controller.getFinancialDetails(lstPromotions[0].id);
            HEP_Promotion_Financials_Controller.PromotionFinancialWrapper objProFinWrapper = new HEP_Promotion_Financials_Controller.PromotionFinancialWrapper();
            HEP_Promotion_Financials_Controller.PromotionAllFinancialsWithErrorWrapper objProFinErrorWrapper = new HEP_Promotion_Financials_Controller.PromotionAllFinancialsWithErrorWrapper();
            Test.Stoptest();
        }
    }
}