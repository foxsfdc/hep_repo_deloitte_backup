@isTest
public class HEP_National_Promotion_Published_Test{
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'gaur', 'gmehrishi@deloitte.com', 'Meh', 'Gau', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void test1() {
        //Inserting Territory Record
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'gaur'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, null, null, null, false);
        objPromotion.EndDate__c = System.today();
        objPromotion.StartDate__c = System.today();
        insert objPromotion;
        //LOB
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true);
        HEP_SKU_Template__c objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU TEMP', objTerritory.id, objLOB.id, true);
        //Creating Catalog  Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', true);
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, objSKUTemp.id, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objTerritory.Id, 'SKUTEST', 'KINGSMAN BD', 'Master', 'Physical', 'FOX', 'HD', true);
        HEP_Promotion_SKU__c objPromotionSku = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat.id, objSKU.id, 'Pending', 'Locked', true);
        HEP_Approvals__c objHEPApprvals = HEP_Test_Data_Setup_Utility.createApproval('Pending', null, objPromotionSku.id, true);
        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objHEPApprvals.id, u.id, objRole.id, 'Pending', true);
        HEP_National_Promotion_Published_Cntrllr.NationalPromotionWrapper objcustWrapper = new HEP_National_Promotion_Published_Cntrllr.NationalPromotionWrapper();
        System.runAs(u) {
            test.startTest();
            HEP_National_Promotion_Published_Cntrllr obj = new HEP_National_Promotion_Published_Cntrllr();
            obj.nationalPromotionId = objPromotion.id;
            obj.sRecordApproverId = objRecordApprover.id;
            obj.fetchNationalPromotionDetials();
            test.stopTest();
        }
    }
}