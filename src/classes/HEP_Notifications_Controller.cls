/**
* HEP_Notifications_Controller --- Class to get the data for notifications
* @author    Sachin Agarwal
*/

public without sharing class HEP_Notifications_Controller {
    
    public HEP_Notifications_Controller(ApexPages.StandardSetController controller) {
    }
    public static String sNotApplicable;
    
    static{
        sNotApplicable = HEP_Utility.getConstantValue('HEP_NOTIFICATION_STATUS_NOT_APPLICABLE');
    }
    
     /**
    * Header --- Wrapper class for holding one row data
    * @author    Sachin Agarwal
    */
    public class Notification{
        DateTime dtReceived;
        String sTime;
        String sPromotion;
        String sPromotionId;
        String sRecordDetail;
        String sType;
        String sStatus;
    }
   
     /**
    * Header --- Wrapper class for holding all the notifications
    * @author    Sachin Agarwal
    */
    public class NotificationData{
        list<Notification> lstNotifications;
        
        /**
        * Default class constructor
        * @return nothing
        * @author Sachin Agarwal
        */
        public NotificationData(){
            lstNotifications = new list<Notification>(); 
        }
    }
    
    /**
    * Extracts all the notifications generated for the logged in user
    * @return an instance of NotificationData
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static NotificationData getNotificationDetails(){
        NotificationData objNotificationData = new NotificationData();
        
        String sLimit = HEP_Utility.getConstantValue('HEP_NOTIFICATION_RECORDS_LIMIT');
        Integer iLimit = 1000;
        if(!String.isEmpty(sLimit) && Integer.valueOf(sLimit) <= 50000){
            iLimit = Integer.valueOf(sLimit);
        }
        
        // Querying all the matching records for the logged in user
        list<HEP_Notification_User_Action__c> lstNotifications = new list<HEP_Notification_User_Action__c>();
        lstNotifications = [SELECT id, HEP_Notification__c, IsRead__c, Notification_User__c,
                                   HEP_Notification__r.HEP_Assignee_ID__c, HEP_Notification__r.HEP_Message__c,
                                   HEP_Notification__r.HEP_Object_API__c, HEP_Notification__r.HEP_Record_ID__c,
                                   HEP_Notification__r.HEP_Template_Name__c,
                                   HEP_Notification__r.Notification_Template__r.HEP_Body__c,
                                   HEP_Notification__r.Notification_Template__r.Status__c,
                                   HEP_Notification__r.Notification_Template__r.HEP_Object__c,
                                   HEP_Notification__r.Notification_Template__r.Notifying_Field__c,
                                   HEP_Notification__r.Notification_Template__r.Type__c, createddate  
                            FROM HEP_Notification_User_Action__c
                            WHERE Notification_User__c = :UserInfo.getUserId() order by createddate desc limit :iLimit];
        
        // If any matching records are found, do the further processing                         
        if(lstNotifications != null && !lstNotifications.isEmpty()){
            
            map<String, set<String>> mapObjectRecordIds = new map<String, set<String>>();
            map<String, set<String>> mapObjectFields = new map<String, set<String>>();
            map<String, list<HEP_Notification_User_Action__c>> mapObjectNotifications = new map<String, list<HEP_Notification_User_Action__c>>();
            map<String, Notification> mapIdNotification = new map<String, Notification>();
            
            String sObjectAPIName;
            
            // Going through all the notification user action records
            for(HEP_Notification_User_Action__c objSFDCNotification : lstNotifications){
                
                // if there's a record id
                if(String.isNotEmpty(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                    
                    id recordId = objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c;
                    sObjectAPIName = String.valueOf(recordId.getsobjecttype());
                    
                    // Creating a map between the object type and all the associated record ids
                    if(mapObjectRecordIds.containsKey(sObjectAPIName)){
                        mapObjectRecordIds.get(sObjectAPIName).add(recordId);
                    }
                    else{
                        mapObjectRecordIds.put(sObjectAPIName, new set<String>{recordId});
                    }
                    
                    // Creating a map between the object type and all the associated notifications
                    if(mapObjectNotifications.containsKey(sObjectAPIName)){
                        mapObjectNotifications.get(sObjectAPIName).add(objSFDCNotification);
                    }
                    else{
                        mapObjectNotifications.put(sObjectAPIName, new list<HEP_Notification_User_Action__c>{objSFDCNotification});
                    }
                    
                    // Creating a map between the object type and all the notifying fields in that object
                    if(String.isNotEmpty(objSFDCNotification.HEP_Notification__r.Notification_Template__r.Notifying_Field__c)){
                        if(mapObjectFields.containsKey(sObjectAPIName)){
                            mapObjectFields.get(sObjectAPIName).add(objSFDCNotification.HEP_Notification__r.Notification_Template__r.Notifying_Field__c);
                        }
                        else{
                            mapObjectFields.put(sObjectAPIName, new set<String>{objSFDCNotification.HEP_Notification__r.Notification_Template__r.Notifying_Field__c});
                        }
                    }                   
                }
            }
            
            String sQuerySelect;
            String sQueryFrom;
            String sQueryWhere;
            
            // Going through all the object types
            for(String sObjAPIName : mapObjectNotifications.keyset()){
                sQuerySelect = '';
                sQueryFrom = '';
                sQueryWhere = '';
                
                System.debug('sObjAPIName ' + sObjAPIName);
                // Creating the query string
                
                sQuerySelect = 'SELECT id';
                if(mapObjectFields.containsKey(sObjAPIName)){
                        
                    sQuerySelect += ', ' + String.join(new List<String>(mapObjectFields.get(sObjAPIName)), ',');                                      
                }
                sQueryFrom = ' FROM ' + sObjAPIName;
                    
                if(mapObjectRecordIds.containsKey(sObjAPIName)){
                    
                    set<String> setRecordIds = mapObjectRecordIds.get(sObjAPIName);
                    sQueryWhere += ' WHERE ID IN :setRecordIds';
                }  
                
                System.debug('sQuerySelect ' + sQuerySelect);
                
                // If its a valid query string
                if(String.isNotEmpty(sQuerySelect)){
                    
                    // Perform logic specific to promotion dating object
                    if(sObjAPIName == 'HEP_Promotion_Dating__c'){
                        
                        sQuerySelect += ', Promotion__r.PromotionName__c, Promotion__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_Promotion_Dating__c> mapPromoDating = new map<id, HEP_Promotion_Dating__c>((list<HEP_Promotion_Dating__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapPromoDating.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_Promotion_Dating__c objPromotionDating = mapPromoDating.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                objNotification.sPromotion = objPromotionDating.Promotion__r.PromotionName__c;
                                objNotification.sPromotionId = objPromotionDating.Promotion__c;                             
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }                                       
                    }
                    // Perform logic specific to market spend object
                    else if(sObjAPIName == 'HEP_Market_Spend__c'){
                        sQuerySelect += ', Promotion__r.PromotionName__c, Promotion__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_Market_Spend__c> mapMarketSpend = new map<id, HEP_Market_Spend__c>((list<HEP_Market_Spend__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapMarketSpend.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_Market_Spend__c objMarketSpend = mapMarketSpend.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                
                                objNotification.sPromotion = objMarketSpend.Promotion__r.PromotionName__c;
                                objNotification.sPromotionId = objMarketSpend.Promotion__c;                             
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    }
                    
                    // Perform logic specific to promotion SKU object
                    else if(sObjAPIName == 'HEP_Promotion_SKU__c'){
                        sQuerySelect += ', Promotion_Catalog__r.Promotion__r.PromotionName__c, Promotion_Catalog__r.Promotion__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_Promotion_SKU__c> mapPromotionSKU = new map<id, HEP_Promotion_SKU__c>((list<HEP_Promotion_SKU__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapPromotionSKU.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_Promotion_SKU__c objPromotionSKU = mapPromotionSKU.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                
                                objNotification.sPromotion = objPromotionSKU.Promotion_Catalog__r.Promotion__r.PromotionName__c;
                                objNotification.sPromotionId = objPromotionSKU.Promotion_Catalog__r.Promotion__c;                               
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    }
                    // Perform logic specific to promotion SKU object
                    else if(sObjAPIName == 'HEP_Promotion__c'){
                        sQuerySelect += ', PromotionName__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_Promotion__c> mapPromotion = new map<id, HEP_Promotion__c>((list<HEP_Promotion__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapPromotion.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_Promotion__c objPromotion = mapPromotion.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                
                                objNotification.sPromotion = objPromotion.PromotionName__c;
                                objNotification.sPromotionId = objPromotion.id;
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    }   
                    // Perform logic specific to Catalog object
                    else if(sObjAPIName == 'HEP_Catalog__c'){
                        //sQuerySelect += ', PromotionName__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;                        
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    }
                    // Perform logic specific to promotion product object
                    else if(sObjAPIName == 'HEP_MDP_Promotion_Product__c'){
                        sQuerySelect += ', HEP_Promotion__c, HEP_Promotion__r.PromotionName__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_MDP_Promotion_Product__c> mapPromotionProduct = new map<id, HEP_MDP_Promotion_Product__c>((list<HEP_MDP_Promotion_Product__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapPromotionProduct.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_MDP_Promotion_Product__c objPromotionProduct = mapPromotionProduct.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                
                                objNotification.sPromotion = objPromotionProduct.HEP_Promotion__r.PromotionName__c;
                                objNotification.sPromotionId = objPromotionProduct.HEP_Promotion__c;
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    } 
                    // Perform logic specific to promotion product object
                    else if(sObjAPIName == 'HEP_Promotion_Catalog__c'){
                        sQuerySelect += ', Promotion__c, Promotion__r.PromotionName__c';
                        sQuerySelect += sQueryFrom;
                        sQuerySelect += sQueryWhere;
                        
                        // Querying the records
                        map<id, HEP_Promotion_Catalog__c> mapPromotionCatalog = new map<id, HEP_Promotion_Catalog__c>((list<HEP_Promotion_Catalog__c>)Database.query(sQuerySelect));
                        
                        for(HEP_Notification_User_Action__c objSFDCNotification : mapObjectNotifications.get(sObjAPIName)){
                            
                            Notification objNotification = fillCommonFields(objSFDCNotification);
                            
                            if(mapPromotionCatalog.containsKey(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c)){
                                
                                HEP_Promotion_Catalog__c objPromotionCatalog = mapPromotionCatalog.get(objSFDCNotification.HEP_Notification__r.HEP_Record_ID__c);
                                
                                objNotification.sPromotion = objPromotionCatalog.Promotion__r.PromotionName__c;
                                objNotification.sPromotionId = objPromotionCatalog.Promotion__c;
                            }
                            
                            mapIdNotification.put(objSFDCNotification.id, objNotification);                         
                        }   
                    }              
                }               
            }
            
            
            // Sorting the data
            for(HEP_Notification_User_Action__c objSFDCNotification : lstNotifications){
                if(mapIdNotification.containsKey(objSFDCNotification.id)){
                    objNotificationData.lstNotifications.add(mapIdNotification.get(objSFDCNotification.id));
                }
            }
        }       
        // Returning all the notification records           
        return objNotificationData;
    }
    
    
    /**
    * Fills up the common fields
    * @param objSFDCNotification Notification user action record
    * @return an instance of Notification
    * @author Sachin Agarwal
    */
    public static Notification fillCommonFields(HEP_Notification_User_Action__c objSFDCNotification){
        Notification objNotification = new Notification();
        
        if(objSFDCNotification != null){
            
            objNotification.dtReceived = objSFDCNotification.createddate;
            objNotification.sTime = objSFDCNotification.createddate.format('h:mm a');
            objNotification.sRecordDetail = objSFDCNotification.HEP_Notification__r.HEP_Message__c;
            objNotification.sType = objSFDCNotification.HEP_Notification__r.Notification_Template__r.Type__c;
            objNotification.sStatus = objSFDCNotification.HEP_Notification__r.Notification_Template__r.Status__c;
            if(String.isEmpty(objNotification.sStatus) || objNotification.sStatus == '-'){
                objNotification.sStatus = sNotApplicable;
            }                           
        }
        // Returning the notification record        
        return objNotification;
    }    
}