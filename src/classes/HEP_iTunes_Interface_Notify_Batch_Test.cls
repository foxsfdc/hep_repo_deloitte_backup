@isTest private class HEP_iTunes_Interface_Notify_Batch_Test {
    @testSetup
     static void createUsers(){
       
        list < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Constants__c objConstant = new HEP_Constants__c();
        objConstant.Name = 'HEP_Itunes_Interface_Error_Notify';
        objConstant.Value__c = 'HEP_Itunes_Interface_Error_Notify';
        insert objConstant;
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        HEP_Interface__c objInt = new HEP_Interface__c();
        objInt.Name = 'HEP_iTunes_Price';
        objInt.Type__c = 'Outbound-Push';
        objInt.Class__c = 'HEP_Itunes_Price';
        objInt.Integration_Name__c = 'HEP_iTunes_Price';
        objInt.Record_Status__c = 'Active';
        insert objInt;
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'DH';
        objTerritory.MM_Territory_Code__c = 'DHE';
        insert objTerritory;
        
        HEP_Promotion__c objPromotionNational = HEP_Test_Data_Setup_Utility.createPromotion('Global National 1', 'National', null,objTerritory.Id, null, null,null,false);
        objPromotionNational.Record_Status__c= 'Active'; 
        objPromotionNational.StartDate__c = date.today();
        objPromotionNational.EndDate__c = date.today()+20;
        insert objPromotionNational;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;
        
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;      

        HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotionNational.id,objTitle.id,objTerritory.id,false);
        objPromotionProduct.Product_End_Date__c = date.today()+30;
        objPromotionProduct.Product_Start_Date__c = date.today(); 
        objPromotionProduct.Approval_Status__c = 'Approved';
        objPromotionProduct.Record_Status__c = 'Active';
        objPromotionProduct.HEP_Catalog__c = objCatalog.id;
        insert objPromotionProduct;
        
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInt.id;
        objTxn.Object_Id__c = objPromotionProduct.id;
        objTxn.Status__c = 'Failure';
        insert objTxn;
        
        HEP_Interface_Transaction_Error__c objTxnError1 = new HEP_Interface_Transaction_Error__c();
        objTxnError1.HEP_Interface_Transaction__c = objTxn.id;
        insert objTxnError1;
        
        //Create Outbound Email Record
        HEP_Outbound_Email__c objOutEmail = new HEP_Outbound_Email__c();
        objOutEmail.Record_Id__c = objTxnError1.Id;
        objOutEmail.Email_Sent__c = false;
        objOutEmail.Email_Template_Name__c = 'HEP_Itunes_Interface_Error_Notify';
        objOutEmail.Object_API__c = 'HEP_Interface_Transaction_Error__c'; //Adding any random Id to required field
        insert objOutEmail;
    }
    
    @isTest static void HEP_iTunes_Interface_Error_Notif() {
        Test.startTest();
        HEP_iTunes_Interface_Error_Notify_Batch objBatch = new HEP_iTunes_Interface_Error_Notify_Batch();
        DataBase.executeBatch(objBatch);
        Test.stoptest();
    }
}