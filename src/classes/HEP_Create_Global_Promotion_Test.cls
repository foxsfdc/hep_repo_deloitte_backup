@isTest(SeeAllData=false)
public class HEP_Create_Global_Promotion_Test {

	@testSetup
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','Dreamworks - New Release', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager NR','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;
	    HEP_Notification_Template__c objNotification = new HEP_Notification_Template__c(Name = 'Global_FAD_Date_Change',HEP_Body__c = '<old Global FAD> updated to <new Global FAD>',HEP_Object__c = 'HEP_Promotion__c',Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = 'Global_FAD_Date_Change',Status__c='-');
		//insert objNotification;

        HEP_Notification_Template__c objNotification1 = new HEP_Notification_Template__c(Name = HEP_Utility.getConstantValue('GLOBAL_PROMOTION_CREATION'),HEP_Body__c = 'A catalog has been added to <Promotion__r.PromotionName__c> - <Promotion__r.LocalPromotionCode__c>. Please review your local promotion and update as needed.',HEP_Object__c = 'HEP_Promotion__c',Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = HEP_Utility.getConstantValue('GLOBAL_PROMOTION_CREATION'),Status__c='-');
        //insert objNotification;

        insert new List<HEP_Notification_Template__c>{objNotification , objNotification1};
        new HEP_Notification_Utility();
        HEP_Notification_Utility.fetchFieldApisFromTemplate('A catalog has been added to <Promotion__r.PromotionName__c> - <Promotion__r.LocalPromotionCode__c>. Please review your local promotion and update as needed.');
  	}

  	static testMethod void testData(){
  		User u = [SELECT Id,Name, DateFormat__c, HEP_Line_Of_Business__c,Email FROM User WHERE LastName = 'nara'];

  		HEP_Create_Global_Promotion_Controller.GlobalPromotionPageData objWrapperReturned = new HEP_Create_Global_Promotion_Controller.GlobalPromotionPageData();
  		objWrapperReturned.sMediaType = 'Digital';
		HEP_Create_Global_Promotion_Controller.LineOfBusinessWrapper objLineOfBusinessWrapper = new HEP_Create_Global_Promotion_Controller.LineOfBusinessWrapper();
		HEP_Create_Global_Promotion_Controller.MarketingManagerWrapper objMarketingManagerWrapper = new HEP_Create_Global_Promotion_Controller.MarketingManagerWrapper();

    	HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager NR'];
    	HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];

    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
		HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
		objNationalTerritory.Flag_Country_Code__c = 'DE';
		insert objNationalTerritory;
		HEP_Territory__c objNationalTerritoryLicensee = HEP_Test_Data_Setup_Utility.createHEPTerritory('India','APAC','Licensee',null, null,'INR','IN','213',null, false);
		objNationalTerritoryLicensee.FoxipediaCode__c = 'IN';
		insert objNationalTerritoryLicensee;

		HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.id, u.id, false);
    	objUserRole.Search_Id__c = objRole.id+' / '+objTerritory.Id;
    	insert objUserRole;
    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRoleOperations.id, u.id, false);
    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objTerritory.Id;
    	insert objUserRoleOperation;

    	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release','New Release',true);

    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objNationalTerritoryLicensee.Id, false);
    	objDatingMatrix.Media_Type__c = 'Digital';
    	insert objDatingMatrix;

    	

    	objLineOfBusinessWrapper.sKey = objLOB.id;
    	objLineOfBusinessWrapper.sValue = objLOB.Name;
    	objLineOfBusinessWrapper.sLineOfBusinessType = objLOB.Type__c;

    	objWrapperReturned.objLineOfBusiness = objLineOfBusinessWrapper;
    	
    	objMarketingManagerWrapper.sKey = u.id;
        objMarketingManagerWrapper.sValue = u.Name;
    	objMarketingManagerWrapper.sLOB = u.HEP_Line_Of_Business__c;
    	objMarketingManagerWrapper.sEmail = u.Email;

    	objWrapperReturned.objDomesticMarketingManager = objMarketingManagerWrapper;
    	objWrapperReturned.objInternationalMarketingManager = objMarketingManagerWrapper;

	   	        
    	System.runAs(u){

    		Test.startTest();
    		
    		HEP_Notification_Template__c objNotificationTemplate = [SELECT id FROM HEP_Notification_Template__c LIMIT 1];   
			System.debug('----------template-----------'+objNotificationTemplate);  
    		HEP_Create_Global_Promotion_Controller.fetchGlobalPromotionDefaultValues();
    		HEP_Create_Global_Promotion_Controller.saveGlobalPromotion(objWrapperReturned);
    		HEP_Create_Global_Promotion_Controller.checkPermissions();
    		
    		Test.stopTest();

    	}
  	}



}