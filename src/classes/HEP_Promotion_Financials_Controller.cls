/**
* HEP_Promotion_Financials_Controller ---> Class for Promotion Financial Details from Utlimate object 
* @author    Abhishek Mishra
*/
public class HEP_Promotion_Financials_Controller{
    
   /**
    * Helps getting the Financial details from Utlimate object based on promotion's EDM title
    * @param sPromoId -- Promotion Id for which Financial details will be fetched
    * @return masterCatalogWrapper - This wrapper contains all Inital values and User input final values
    * @authod Abhishek Mishra
    */
    @RemoteAction
    public static PromotionAllFinancialsWithErrorWrapper getFinancialDetails(id sPromoId){
        try{
            //Local Variable declarations
            List<PromotionFinancialWrapper> lstPromotionFinancialWrapper = new List<PromotionFinancialWrapper>();
            PromotionFinancialWrapper objPromotionFinancialWrapperOneDomestic;
            PromotionFinancialWrapper objPromotionFinancialWrapperOneInternational;
            PromotionFinancialWrapper objPromotionFinancialWrapperSixDomestic;
            PromotionFinancialWrapper objPromotionFinancialWrapperSixInternational;
            PromotionAllFinancialsWithErrorWrapper objPromotionAllFinancialsWithErrorWrapper = new PromotionAllFinancialsWithErrorWrapper();

            //Fetch Constant Values
            string sHEP_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            string sHEP_Ultimate1Yr = HEP_Utility.getConstantValue('HEP_Ultimate1Yr');
            string sHEP_Ultimate6Yr = HEP_Utility.getConstantValue('HEP_Ultimate6Yr');
            string sHEP_ONE = HEP_Utility.getConstantValue('HEP_ONE');
            string sHEP_SIX = HEP_Utility.getConstantValue('HEP_SIX');
            string sREGION_DOMESTIC = HEP_Utility.getConstantValue('REGION_DOMESTIC');
            string sHEP_International = HEP_Utility.getConstantValue('HEP_International'); 
            string sHEP_NORTH_AMERICA = HEP_Utility.getConstantValue('HEP_NORTH_AMERICA'); 
            string sHEP_HEI = HEP_Utility.getConstantValue('HEP_HEI'); 
            string sHEP_Ult_ProjectedBO = HEP_Utility.getConstantValue('HEP_Ult_ProjectedBO');
            string sHEP_Ult_Contribution = HEP_Utility.getConstantValue('HEP_Ult_Contribution'); 
            string sHEP_SKU_CHANNEL_RETAIL = HEP_Utility.getConstantValue('HEP_SKU_CHANNEL_RETAIL'); 
            string sHEP_SKU_CHANNEL_RENTAL = HEP_Utility.getConstantValue('HEP_SKU_CHANNEL_RENTAL'); 
            string sHEP_GL_ACC_FORMAT_DHD = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_DHD'); 
            string sHEP_CHANNEL_VOD = HEP_Utility.getConstantValue('HEP_CHANNEL_VOD'); 
            list <string> lstgenericIntegrationErrors;

            //Query the promotion
            list<hep_promotion__c> lstPromotions = [select Title__c 
                                                    from hep_promotion__c 
                                                    where id= :sPromoId and Record_Status__c = :sHEP_ACTIVE];
            //Fetch the generic error
            if (string.isNotEmpty(System.Label.HEP_PROMOTION_FINANCIALS_ERROR))
                lstgenericIntegrationErrors = System.Label.HEP_PROMOTION_FINANCIALS_ERROR.split(',');

            //Check if Promotion is present in HEP or not
            if(lstPromotions != null && !lstPromotions.isEmpty()){
                //Check if promotion have Title
                if(lstPromotions[0].Title__c != null){
                    //Query Ultimates based on Promotion Title
                    list<HEP_Ultimates__c> lstUltimates = [select id, Period__c, Territories__c, Account_No__c, Channel__c, Data_Value__c 
                                                           from HEP_Ultimates__c 
                                                           where Title__c = :lstPromotions[0].Title__c and Record_Status__c = :sHEP_ACTIVE];
                    system.debug('lstUltimates : ' + lstUltimates);

                    if(lstUltimates != null && !lstUltimates.isEmpty()){
                        objPromotionFinancialWrapperOneDomestic = new PromotionFinancialWrapper();
                        objPromotionFinancialWrapperOneInternational = new PromotionFinancialWrapper();
                        objPromotionFinancialWrapperSixDomestic = new PromotionFinancialWrapper();
                        objPromotionFinancialWrapperSixInternational = new PromotionFinancialWrapper();

                        Decimal dOneDomBox = 0, dOneDomPHY = 0, dOneDomDHD = 0, dOneDomVOD = 0;
                        Decimal dOneIntlBox = 0, dOneIntlPHY = 0, dOneIntlDHD = 0, dOneIntlVOD = 0;
                        Decimal dSixDomBox = 0, dSixDomPHY = 0, dSixDomDHD = 0, dSixDomVOD = 0;
                        Decimal dSixIntlBox = 0, dSixIntlPHY = 0, dSixIntlDHD = 0, dSixIntlVOD = 0;

                        //Loop through all Ultimate records avaliable on promotion Title
                        for(HEP_Ultimates__c objUtimate : lstUltimates){
                            //Feching Finanacial Detail for OneYear Domestic 
                            if(objUtimate.Period__c == sHEP_Ultimate1Yr && objUtimate.Territories__c == sHEP_NORTH_AMERICA){
                                objPromotionFinancialWrapperOneDomestic.sYear = sHEP_ONE;
                                objPromotionFinancialWrapperOneDomestic.sType = sREGION_DOMESTIC;
                                if(objUtimate.Account_No__c == sHEP_Ult_ProjectedBO){
                                    dOneDomBox = objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && (objUtimate.Channel__c == sHEP_SKU_CHANNEL_RETAIL || objUtimate.Channel__c == sHEP_SKU_CHANNEL_RENTAL)){
                                    dOneDomPHY += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_GL_ACC_FORMAT_DHD){
                                    dOneDomDHD += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_CHANNEL_VOD){
                                    dOneDomVOD += objUtimate.Data_Value__c;
                                }
                            }
                            //Feching Finanacial Detail for OneYear International 
                            if(objUtimate.Period__c == sHEP_Ultimate1Yr && objUtimate.Territories__c == sHEP_HEI){
                                objPromotionFinancialWrapperOneInternational.sYear = sHEP_ONE;
                                objPromotionFinancialWrapperOneInternational.sType = sHEP_International;
                                if(objUtimate.Account_No__c == sHEP_Ult_ProjectedBO){
                                    dOneIntlBox = objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && (objUtimate.Channel__c == sHEP_SKU_CHANNEL_RETAIL || objUtimate.Channel__c == sHEP_SKU_CHANNEL_RENTAL)){
                                    dOneIntlPHY += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_GL_ACC_FORMAT_DHD){
                                    dOneIntlDHD += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_CHANNEL_VOD){
                                    dOneIntlVOD += objUtimate.Data_Value__c;
                                }
                            }
                            //Feching Finanacial Detail for SixYear Domestic 
                            if(objUtimate.Period__c == sHEP_Ultimate6Yr && objUtimate.Territories__c == sHEP_NORTH_AMERICA){
                                objPromotionFinancialWrapperSixDomestic.sYear = sHEP_SIX;
                                objPromotionFinancialWrapperSixDomestic.sType = sREGION_DOMESTIC;
                                if(objUtimate.Account_No__c == sHEP_Ult_ProjectedBO){
                                    dSixDomBox = objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && (objUtimate.Channel__c == sHEP_SKU_CHANNEL_RETAIL || objUtimate.Channel__c == sHEP_SKU_CHANNEL_RENTAL)){
                                    dSixDomPHY += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_GL_ACC_FORMAT_DHD){
                                    dSixDomDHD += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_CHANNEL_VOD){
                                    dSixDomVOD += objUtimate.Data_Value__c;
                                }
                            }
                            //Feching Finanacial Detail for SixYear International 
                            if(objUtimate.Period__c == sHEP_Ultimate6Yr && objUtimate.Territories__c == sHEP_HEI){
                                objPromotionFinancialWrapperSixInternational.sYear = sHEP_SIX;
                                objPromotionFinancialWrapperSixInternational.sType = sHEP_International;
                                if(objUtimate.Account_No__c == sHEP_Ult_ProjectedBO){
                                    dSixIntlBox = objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && (objUtimate.Channel__c == sHEP_SKU_CHANNEL_RETAIL || objUtimate.Channel__c == sHEP_SKU_CHANNEL_RENTAL)){
                                    dSixIntlPHY += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_GL_ACC_FORMAT_DHD){
                                    dSixIntlDHD += objUtimate.Data_Value__c;
                                }
                                if(objUtimate.Account_No__c == sHEP_Ult_Contribution && objUtimate.Channel__c == sHEP_CHANNEL_VOD){
                                    dSixIntlVOD += objUtimate.Data_Value__c;
                                }
                            }
                        }

                        //Converting Values to Millions
                        dOneDomBox = dOneDomBox / 1000000; dOneDomPHY = dOneDomPHY / 1000000; dOneDomDHD = dOneDomDHD / 1000000; dOneDomVOD = dOneDomVOD / 1000000;
                        dOneIntlBox = dOneIntlBox / 1000000; dOneIntlPHY = dOneIntlPHY / 1000000; dOneIntlDHD = dOneIntlDHD / 1000000; dOneIntlVOD = dOneIntlVOD / 1000000;
                        dSixDomBox = dSixDomBox / 1000000; dSixDomPHY = dSixDomPHY / 1000000; dSixDomDHD = dSixDomDHD / 1000000; dSixDomVOD = dSixDomVOD / 1000000;
                        dSixIntlBox = dSixIntlBox / 1000000; dSixIntlPHY = dSixIntlPHY / 1000000; dSixIntlDHD = dSixIntlDHD / 1000000; dSixIntlVOD = dSixIntlVOD / 1000000;

                        objPromotionFinancialWrapperOneDomestic.sBoxOffice = string.valueOf(dOneDomBox);
                        objPromotionFinancialWrapperOneDomestic.sPhysical = string.valueof(dOneDomPHY);
                        objPromotionFinancialWrapperOneDomestic.sDHD = string.valueof(dOneDomDHD);
                        objPromotionFinancialWrapperOneDomestic.sVOD = string.valueof(dOneDomVOD);
                        objPromotionFinancialWrapperOneInternational.sBoxOffice = string.valueOf(dOneIntlBox);
                        objPromotionFinancialWrapperOneInternational.sPhysical = string.valueof(dOneIntlPHY);
                        objPromotionFinancialWrapperOneInternational.sDHD = string.valueof(dOneIntlDHD);
                        objPromotionFinancialWrapperOneInternational.sVOD = string.valueof(dOneIntlVOD);
                        objPromotionFinancialWrapperSixDomestic.sBoxOffice = string.valueOf(dSixDomBox);
                        objPromotionFinancialWrapperSixDomestic.sPhysical = string.valueof(dSixDomPHY);
                        objPromotionFinancialWrapperSixDomestic.sDHD = string.valueof(dSixDomDHD);
                        objPromotionFinancialWrapperSixDomestic.sVOD = string.valueof(dSixDomVOD);
                        objPromotionFinancialWrapperSixInternational.sBoxOffice = string.valueOf(dSixIntlBox);
                        objPromotionFinancialWrapperSixInternational.sPhysical = string.valueof(dSixIntlPHY);
                        objPromotionFinancialWrapperSixInternational.sDHD = string.valueof(dSixIntlDHD);
                        objPromotionFinancialWrapperSixInternational.sVOD = string.valueof(dSixIntlVOD);

                        objPromotionFinancialWrapperOneDomestic.sTODDTG = string.valueof(dOneDomDHD + dOneDomVOD);
                        objPromotionFinancialWrapperOneDomestic.sPDU = string.valueof(dOneDomPHY + dOneDomDHD + dOneDomVOD);
                        if(dOneDomBox != null && dOneDomBox != 0){
                                objPromotionFinancialWrapperOneDomestic.sCMGBO = string.valueof(((dOneDomPHY + dOneDomDHD + dOneDomVOD) / dOneDomBox).setScale(2, RoundingMode.HALF_UP));   // check here
                        }        
                        objPromotionFinancialWrapperOneInternational.sTODDTG = string.valueof(dOneIntlDHD + dOneIntlVOD);
                        objPromotionFinancialWrapperOneInternational.sPDU = string.valueof(dOneIntlPHY + dOneIntlDHD + dOneIntlVOD);
                        if(dOneIntlBox != null && dOneIntlBox != 0){
                                objPromotionFinancialWrapperOneInternational.sCMGBO = string.valueof(((dOneIntlPHY + dOneIntlDHD + dOneIntlVOD) / dOneIntlBox).setScale(2, RoundingMode.HALF_UP));
                        }        
                        objPromotionFinancialWrapperSixDomestic.sTODDTG = string.valueof(dSixDomDHD + dSixDomVOD);
                        objPromotionFinancialWrapperSixDomestic.sPDU = string.valueof(dSixDomPHY + dSixDomDHD + dSixDomVOD);
                        if(dSixDomBox != null && dSixDomBox != 0){                        
                                objPromotionFinancialWrapperSixDomestic.sCMGBO = string.valueof(((dSixDomPHY + dSixDomDHD + dSixDomVOD) / dSixDomBox).setScale(2, RoundingMode.HALF_UP));
                        }              
                        objPromotionFinancialWrapperSixInternational.sTODDTG = string.valueof(dSixIntlDHD + dSixIntlVOD);
                        objPromotionFinancialWrapperSixInternational.sPDU = string.valueof(dSixIntlPHY + dSixIntlDHD + dSixIntlVOD);
                        if(dSixIntlBox != null && dSixIntlBox != 0){                        
                                objPromotionFinancialWrapperSixInternational.sCMGBO = string.valueof(((dSixIntlPHY + dSixIntlDHD + dSixIntlVOD) / dSixIntlBox).setScale(2, RoundingMode.HALF_UP));
                        }             
                        system.debug('objPromotionFinancialWrapperOneDomestic : ' + objPromotionFinancialWrapperOneDomestic);
                        system.debug('objPromotionFinancialWrapperOneInternational : ' + objPromotionFinancialWrapperOneInternational);
                        system.debug('objPromotionFinancialWrapperSixDomestic : ' + objPromotionFinancialWrapperSixDomestic);
                        system.debug('objPromotionFinancialWrapperSixInternational : ' + objPromotionFinancialWrapperSixInternational);
                        lstPromotionFinancialWrapper.add(objPromotionFinancialWrapperOneDomestic);
                        lstPromotionFinancialWrapper.add(objPromotionFinancialWrapperOneInternational);
                        lstPromotionFinancialWrapper.add(objPromotionFinancialWrapperSixDomestic);
                        lstPromotionFinancialWrapper.add(objPromotionFinancialWrapperSixInternational);
                        objPromotionAllFinancialsWithErrorWrapper.lstPromotionFinancialWrapper = lstPromotionFinancialWrapper;
                    }else{
                         objPromotionAllFinancialsWithErrorWrapper.lstErrors = lstgenericIntegrationErrors;
                    }                        
                }else{
                     objPromotionAllFinancialsWithErrorWrapper.lstErrors = lstgenericIntegrationErrors;
                }
            }else{
                objPromotionAllFinancialsWithErrorWrapper.lstErrors = lstgenericIntegrationErrors;
            }
            return objPromotionAllFinancialsWithErrorWrapper;
        }catch(exception ex){
            //Code to log error..
            HEP_Error_Log.genericException('Error occured while retriving Financial Details',
                                             'VF Controller',
                                              ex,
                                             'HEP_Promotion_Financials_Controller',
                                             'getFinancialDetails',
                                             null,
                                             '');
            throw ex;
        } 
    }  

    /**
    * DateType --- Wrapper class for holding all Financials Informations and Errors
    * @author    Abhishek Mishra
    */
    public class PromotionAllFinancialsWithErrorWrapper {
        public list<string> lstErrors = new list<string>();
        public list<PromotionFinancialWrapper> lstPromotionFinancialWrapper = new list<PromotionFinancialWrapper>();
    }    

    
   /**
    * DateType --- Wrapper class for holding Promotion Financials Information
    * @author    Abhishek Mishra
    */
    public class PromotionFinancialWrapper {
        public String sError;
        public String sYear;
        public String sType;
        public String sBoxOffice;
        public String sPhysical;
        public String sDHD;
        public String sVOD; 
        public String sTODDTG;
        public String sPDU;
        public String sCMGBO;
    }   
}