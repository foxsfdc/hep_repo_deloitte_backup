@isTest
private class HEP_CatalogTitleWrapper_Test {
    @isTest
    static void testCatalogTitleWrapper() {
        HEP_CatalogTitleWrapper.Attributes objAtt = new HEP_CatalogTitleWrapper.Attributes();
        objAtt.type = 'TITLE';
        objAtt.foxId = '4897679';
        objAtt.primaryClassificationCode = null;
        objAtt.productTypeCode = 'SNGL';
        objAtt.productTypeDescription = 'Single Pack';
        objAtt.productId = '88541';
        objAtt.productName = 'DEADPOOL 2';
        objAtt.productStatusCode = 'ACTV';
        objAtt.productStatusDescription = 'Active';
        objAtt.globalEpisodeConfigIndicator = null;
        objAtt.productStatusComment = null;
        objAtt.productComment = null;
        objAtt.primaryClassificationDescription = null;
        //Links Class in HEP_CatalogTitleWrapper class
        HEP_CatalogTitleWrapper.LinksOuter objLinksOuter = new HEP_CatalogTitleWrapper.LinksOuter();
        objLinksOuter.self = '/foxipedia/global/api/title/catalog?foxId\u003d4897679';
        //Links Class in HEP_CatalogTitleWrapper class
        HEP_CatalogTitleWrapper.Links objLink = new HEP_CatalogTitleWrapper.Links();
        objLink.related = '/foxipedia/global/api/title/details?foxId\u003d4897679';
        //Title Class in HEP_CatalogTitleWrapper class
        HEP_CatalogTitleWrapper.Title objTitle = new HEP_CatalogTitleWrapper.Title();
        objTitle.links = objLink;
        //Relationships class in HEP_CatalogTitleWrapper
        HEP_CatalogTitleWrapper.Relationships objRel = new HEP_CatalogTitleWrapper.Relationships();
        objRel.title = objTitle;
        //Error class in HEP_CatalogTitleWrapper
        HEP_CatalogTitleWrapper.Errors objError = new HEP_CatalogTitleWrapper.Errors();
        objError.title = 'Title';
        objError.detail = 'Error in Title';
        objError.status = 500;
        //Data Class in HEP_CatalogTitleWrapper
        HEP_CatalogTitleWrapper.Data objData = new HEP_CatalogTitleWrapper.Data();
        objData.attributes = objAtt;
        objData.id = '4897679';
        objData.type = 'Single';
        objData.relationships = objRel;
        //Variable Declaration 
        HEP_CatalogTitleWrapper objCatWrpr = new HEP_CatalogTitleWrapper();
        objCatWrpr.data = new List < HEP_CatalogTitleWrapper.Data > ();
        objCatWrpr.data.add(objData);
        objCatWrpr.calloutStatus = 'calloutStatus';
        objCatWrpr.TxnId = 'tax2345';
        objCatWrpr.links = objLinksOuter;
        objCatWrpr.errors = new List < HEP_CatalogTitleWrapper.Errors > ();
        objCatWrpr.errors.add(objError);
    }
}