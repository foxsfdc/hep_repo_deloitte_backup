/**
 * HEP_Suisse_EmailXLSAttachmentController
--- 
 * @author   -- Roshi
 */
@isTest
public class HEP_Suisse_EmailXLSAttachment_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'roshrai45', 'roshirai678@deloitte.com', 'Rai7', 'ros7', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List < HEP_List_Of_Values__c > lstHepLOVDATA = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        createData();
    }
    static void createData() {
        Integer iDateLimit = 0 - Integer.valueOf(System.Label.HEP_SUISSE_LIVE_PROMO_DATE_LIMIT);
        Date datenow = Date.newInstance(2018, 06, 12);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        insert objTerritory;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        HEP_Customer__c objCustPromotion = HEP_Test_Data_Setup_Utility.createHEPCustomers('RoshiRai', objTerritory.Id, '782933', 'Dating Customers', 'EST Release Date', true);
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion', HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER'), null, objTerritory.Id, null, null, null, false);
        objPromotion.Suisse_Updated__c = false;
        objPromotion.Customer__c = objCustPromotion.id;
        objPromotion.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objPromotion;
        //Create PRoduct Type Object.
        EDM_REF_PRODUCT_TYPE__c objEDMREFPRODUCT = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature', 'FEATR', true);
        //Create Title Object.
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('TESTFinancial', '908763', 'PUBLC', objEDMREFPRODUCT.Id, true);
        //objTitle.FOX_ID__c = '67728';
        update objTitle;
        HEP_MDP_Promotion_Product__c objProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id, objTitle.id, objTerritory.id, false);
        objProduct.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objProduct;
        HEP_MDP_Promotion_Product_Detail__c objProductDetails = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objProduct.id, 'HD', 'EST', false);
        objProductDetails.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objProductDetails;
        objPromotion.Customer_Promotion_Status__c = HEP_Utility.getConstantValue('HEP_PROMOTION_STATUS_LIVE');
        update objPromotion;
    }
    public static testMethod void test1() {
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'roshrai45'];
        System.runAs(u) {
            Test.startTest();
            HEP_Suisse_EmailXLSAttachmentController obj = new HEP_Suisse_EmailXLSAttachmentController();
            obj.getRecords();
            HEP_Suisse_LiveCustomerPromoEmailBatch schedule = new HEP_Suisse_LiveCustomerPromoEmailBatch();
            // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
            String sch = '40 17 22 20 4 ?';
            String jobID = System.schedule('HEP_MM_Batch', sch, schedule);
            Test.Stoptest();
        }
    }
}