/**
* HEP_Interface_Retry_Scheduler --- Scheduler class to  schedule the Batch class responsible for Interface retry mechanism
* @author    Nishit Kedia
*/ 
public class HEP_Interface_Retry_Scheduler implements Schedulable{
	
	/**
    * execute --- main method where scheduler class invokes 
    * @param 	objSchedulableContext is the context on which the batch is Scheduled to run
    * @return 	nothing
    * @author   Nishit Kedia
    */
    public void execute(SchedulableContext objSchedulableContext){
    	
        HEP_Interface_Retry_Batch objBatchInstance = new HEP_Interface_Retry_Batch();
        objBatchInstance.cronId = objSchedulableContext.getTriggerId(); // assign job Id 
        /*String sJobType = HEP_Constants.HEP_BATCH_JOBTYPE;
        String sApexClassName = HEP_Constants.HEP_BATCH_APEXCLASSNAME;
        String sHoldingStatus = HEP_Constants.HEP_BATCH_HOLDING;
        String sPreparingStatus = HEP_Constants.HEP_BATCH_PREPARING;
        String sQueuedStatus = HEP_Constants.HEP_BATCH_QUEUED;
        */
        
        String sJobType = HEP_Utility.getConstantValue('HEP_BATCH_JOBTYPE');
        String sApexClassName = HEP_Utility.getConstantValue('HEP_BATCH_APEXCLASSNAME');
        String sHoldingStatus = HEP_Utility.getConstantValue('HEP_BATCH_HOLDING');
        String sPreparingStatus = HEP_Utility.getConstantValue('HEP_BATCH_PREPARING');
        String sQueuedStatus = HEP_Utility.getConstantValue('HEP_BATCH_QUEUED');
        // Check if the Batch is already in progress
        if ([SELECT count() FROM AsyncApexJob WHERE JobType=:sJobType and ApexClass.Name=:sApexClassName 
                AND (Status =:sHoldingStatus OR Status =:sPreparingStatus OR Status =:sQueuedStatus) ] < 1){ 
            ID batchprocessid = Database.executeBatch(objBatchInstance, 1);
        }
    }
}