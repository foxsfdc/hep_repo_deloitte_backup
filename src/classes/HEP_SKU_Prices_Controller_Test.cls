/**
* HEP_SKU_Prices_Controller_Test --- Test class for HEP_SKU_Prices_Controller
* @author    Sachin Agarwal
*/
@isTest 
public with sharing class HEP_SKU_Prices_Controller_Test {
    /**
    * Its used to setting up the test data
    * @author Sachin Agarwal
    */
    @testSetup 
    public static void createData(){
        // List of HEP Constants
        list < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
        
        list<HEP_Territory__c> lstRegionTerritories = new list<HEP_Territory__c>(); 
        
        // Creating territories
        HEP_Territory__c objGermanyTerritory  = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        objGermanyTerritory.SKU_Territory__c = true;
        insert objGermanyTerritory;
        
        // Creating regional territories
        lstRegionTerritories.add(HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium', 'EMEA', 'Subsidiary', objGermanyTerritory.id, null, 'EUR', 'BEL', '49', '', false));
        lstRegionTerritories.add(HEP_Test_Data_Setup_Utility.createHEPTerritory('Netherlands', 'EMEA', 'Subsidiary', objGermanyTerritory.id, null, 'EUR', 'NL', '324', '', false));
        
        insert lstRegionTerritories;
        
        // Inserting role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
    
        // Creating user role records
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objGermanyTerritory.id, objRole.id, UserInfo.getUserId(), true);
        
        list<HEP_Line_Of_Business__c> lstLOBs = new list<HEP_Line_Of_Business__c>();
        
        //Create Line of Business
        HEP_Line_Of_Business__c objNewReleaseLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release', 'New Release', false);
        HEP_Line_Of_Business__c objTVLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV', 'TV', false);
        HEP_Line_Of_Business__c objCatalogLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Catalog', 'Catalog', false);
        
        lstLOBs.add(objNewReleaseLOB);
        lstLOBs.add(objTVLOB);
        lstLOBs.add(objCatalogLOB);
        
        insert lstLOBs;
        
        // Creating national promotion
        HEP_Promotion__c objGermanPromotion = HEP_Test_Data_Setup_Utility.createPromotion('German Promotion', 'National', null, objGermanyTerritory.id, lstLOBs[0].Id, null, UserInfo.getUserId(), false);
        
        insert objGermanPromotion;
        
        // Creating price grades
        list<HEP_Price_Grade__c> lstPriceGrades = new list<HEP_Price_Grade__c>(); 
        HEP_Price_Grade__c objMashPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGermanyTerritory.Id, '12-11', 11, false);
        objMashPriceGrade.Channel__c = 'RENTAL';
        objMashPriceGrade.Format__c = 'DVD';
        objMashPriceGrade.Type__c = 'MASH';
        
        lstPriceGrades.add(objMashPriceGrade);
        
        HEP_Price_Grade__c objMDP_ESTPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGermanyTerritory.Id, '12-11', 11, false);
        objMDP_ESTPriceGrade.Channel__c = 'EST';
        objMDP_ESTPriceGrade.Format__c = 'SD';
        objMDP_ESTPriceGrade.MM_RateCard_ID__c = '49';
        objMDP_ESTPriceGrade.Type__c = 'MDP_EST';
        
        lstPriceGrades.add(objMDP_ESTPriceGrade);
        
        insert lstPriceGrades; 
        
        // Creating the catalog
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('77777', 'SAMPLE', 'Bundle', NULL, 
                                    objGermanyTerritory.Id, 'Approved', 'Master', false);
        objCatalog.Licensor__c = 'FOX';
        insert objCatalog;
        
        // Creating SKU Masters
        list<HEP_SKU_Master__c> lstSKUMasters = new list<HEP_SKU_Master__c>(); 
        lstSKUMasters.add(HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGermanyTerritory.Id, 'SKU123456', 'Sample SKU', 
                                        'Master', NULL, 'RENTAL', 'DVD', false));
        lstSKUMasters.add(HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGermanyTerritory.Id, 'SKU123457', 'Sample SKUs', 
                                        'Master', NULL, 'EST', 'SD', false));
                                        
        insert lstSKUMasters;
        
        // Creating promotion catalog
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, 
                                                    objGermanPromotion.Id, NULL, true);
        
        list<HEP_Promotion_SKU__c> lstPromoSKUs = new list<HEP_Promotion_SKU__c>();
         
        lstPromoSKUs.add(HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, lstSKUMasters[0].Id, 
                                        'Approved', 'Unlocked', false));
        lstPromoSKUs.add(HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, lstSKUMasters[1].Id, 
                                        'Approved', 'Unlocked', false));
                                        
        insert lstPromoSKUs;
        
        // Creating SKU Prices
        list<HEP_SKU_Price__c> lstSKUPrices = new list<HEP_SKU_Price__c>();
         
        lstSKUPrices.add(HEP_Test_Data_Setup_Utility.createHEPSkuPrice(lstPriceGrades[0].id, lstSKUMasters[0].id, lstRegionTerritories[0].id, lstPromoSKUs[0].id, null, 'Active', false));
        lstSKUPrices.add(HEP_Test_Data_Setup_Utility.createHEPSkuPrice(lstPriceGrades[0].id, lstSKUMasters[0].id, lstRegionTerritories[0].id, null, null, 'Active', false));
        lstSKUPrices.add(HEP_Test_Data_Setup_Utility.createHEPSkuPrice(lstPriceGrades[1].id, lstSKUMasters[1].id, lstRegionTerritories[1].id, lstPromoSKUs[1].id, null, 'Active', false));
        lstSKUPrices.add(HEP_Test_Data_Setup_Utility.createHEPSkuPrice(lstPriceGrades[1].id, lstSKUMasters[1].id, lstRegionTerritories[1].id, null, null, 'Active', false));
        
        for(HEP_SKU_Price__c objSKUPrice : lstSKUPrices){
            objSKUPrice.Changed_Flag__c = true;
        }
        
        insert lstSKUPrices;
    }
    
    /**
    * Its used to test getPriceDetails method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testGetPriceDetails(){
        
        list<HEP_SKU_Master__c> lstSKUMasters = new list<HEP_SKU_Master__c>(); 
        lstSKUMasters = [SELECT ID
                         FROM HEP_SKU_Master__c];
                         
        HEP_SKU_Prices_Controller.SKUPriceDetails objSKUPriceDetails = HEP_SKU_Prices_Controller.getPriceDetails(lstSKUMasters[0].id);
        HEP_SKU_Prices_Controller.getPriceDetails(lstSKUMasters[1].id);
        
        // Extracting all the territories
        list<HEP_Territory__c> lstTerritories = new list<HEP_Territory__c>();
        lstTerritories = [SELECT ID
                          FROM HEP_Territory__c
                          WHERE Parent_Territory__c != null]; 
        
        HEP_SKU_Prices_Controller.SKUPrice objSKUPrice = new HEP_SKU_Prices_Controller.SKUPrice(); 
        objSKUPrice.sRegionId = lstTerritories[0].id;
        objSKUPrice.sPriceGradeId = null;
        objSKUPrice.dtEffectiveDate = Date.Today();
        
        objSKUPriceDetails.lstSKUPrices.add(objSKUPrice);
        list<HEP_Price_Grade__c> lstPriceGrades = new list<HEP_Price_Grade__c>();
        // Finding all the active price grade records associated with the given territory
                lstPriceGrades = [SELECT id, Record_Status__c, Price_Filter__c, Dealer_List_Price__c, PriceGrade__c, Territory__c, MM_RateCard_ID__c,
                                         Territory__r.Name, MDP_Price_Filter__c
                                  FROM HEP_Price_Grade__c];
        
        HEP_SKU_Prices_Controller.savePriceDetails(objSKUPriceDetails);
    }
}