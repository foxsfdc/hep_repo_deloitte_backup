/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Roshi
 */
@isTest
public class HEP_Title_Details_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'rosh123', 'roshirairosh123@deloitte.com', 'Rai', 'ros', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Talent', 'HEP_Foxipedia_Talent', true, 10, 10, 'Outbound-Pull', true, true, true);
        HEP_Interface__c objInterfaceFMC = HEP_Test_Data_Setup_Utility.createInterface('HEP_FMC_Apigee_Rendition', 'HEP_FMCApigeeRendition', true, 10, 10, 'Outbound-Pull', true, true, true);
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season', 'SEASN', false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('TitleTest1234576543', '', '', objProductType.Id, false);
        objTitle.FOX_ID__c = '2431818';
        insert objTitle;
        HEP_Title_Asset__c objTitleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('56670725', 'Name', false);
        objTitleAsset.Title_EDM__c = objTitle.id;
        insert objTitleAsset;
        TVD_Release_Dates__c objTVD = new TVD_Release_Dates__c();
        objTVD.Name = 'TEST';
        objTVD.Title_EDM__c = objTitle.id;
        insert objTVD;
    }
    public static testMethod void test1() {
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh123'];
        String sLocaleDateFormat = u.DateFormat__c;
        EDM_GLOBAL_TITLE__c objTitle = [SELECT id, Name FROM EDM_GLOBAL_TITLE__c where Name = 'TitleTest1234576543'];
        HEP_Title_Asset__c objAsset = [SELECT id, Asset_Name__c FROM HEP_Title_Asset__c where Asset_Name__c = 'Name'];
        System.runAs(u) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Title_Details_Controller objTitleDetail = new HEP_Title_Details_Controller();
            HEP_Title_Details_Controller.getHeaderDetails(objTitle.id, sLocaleDateFormat);
            HEP_Title_Details_Controller.getImage(objAsset.id);
            Test.Stoptest();
        }
    }
}