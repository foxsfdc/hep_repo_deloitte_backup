/**
* HEP_RatingWrapper -- Wrapper to store Rating details 
* @author    Lakshman Jinnuri
*/
public class HEP_RatingWrapper{
    public list < Data > data{get;set;}
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public LinksOuter links{get;set;}    
    public list<Errors> errors { get; set; }
     
    /**
    * Data -- Wrapper to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Attributes attributes{get;set;}
        public String type{get;set;}
        public Relationships relationships{get;set;}
    }

    /**
    * Attributes -- Wrapper to store Attributes details 
    * @author    Lakshman Jinnuri
    */
    public class Attributes {
        public String type{get;set;}
        public String foxId{get;set;}
        public String foxVersionId{get;set;}
        public String rowIdObject{get;set;}
        public String rowIdTitle{get;set;}
        public String rowIdTitleVersion{get;set;}
        public String countryDescription{get;set;}
        public String countryCode{get;set;}
        public String mediaDescription{get;set;}
        public String mediaCode{get;set;}
        public String ratingEntityDescription{get;set;}
        public String ratingEntityCode{get;set;}
        public String ratingShortDescription{get;set;}
        public String ratingCode{get;set;}
        public String ratingReason{get;set;}
        public String titleVersionTypeCode{get;set;}
        public String titleVersionTypeDescription{get;set;}
        public String titleVersionDescription{get;set;}
        public String ratingDescription{get;set;}
    }

    /**
    * Relationships -- Wrapper to store Relationships details 
    * @author    Lakshman Jinnuri
    */
    public class Relationships {
        public Title title{get;set;}
    }

    /**
    * Title -- Wrapper to store Title details 
    * @author    Lakshman Jinnuri
    */
    public class Title {
        public Links links{get;set;}
    }

    /**
    * Links -- Wrapper to store Links details 
    * @author    Lakshman Jinnuri
    */
    public class Links {
        public String related{get;set;}
    }
    
    /**
    * Links_Outer -- Wrapper to store Links details 
    * @author    Lakshman Jinnuri
    */
    public class LinksOuter {
        public String self {get;set;} 
    }    
    
    /**
    * Errors -- Class to hold related Error details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }
}