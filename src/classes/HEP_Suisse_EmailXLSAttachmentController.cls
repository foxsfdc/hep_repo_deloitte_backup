/**
 * HEP_Suisse_EmailXLSAttachmentController --- Class to get the data for Suisse integration
 * @author    Nidhin VK
 */
public class HEP_Suisse_EmailXLSAttachmentController {
    
    public static String sACTIVE, sDELETED, sCUSTOMER, sLIVE;
    public static Integer iDateLimit;
    static{
    	sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');        
		sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');      
		sCUSTOMER = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
		sLIVE = HEP_Utility.getConstantValue('HEP_PROMOTION_STATUS_LIVE');  
		iDateLimit = 0 - Integer.valueOf(System.Label.HEP_SUISSE_LIVE_PROMO_DATE_LIMIT);
    }
    
    public PageData objPageData{get;set;}
    public String sFileName{get;set;}
    
	public String xlsHeader {
	    get {
	        String strHeader = '';
	        strHeader += '<?xml version="1.0"?>';
	        strHeader += '<?mso-application progid="Excel.Sheet"?>';
	        return strHeader;
	    }
	}
	
	/**
    * formatDate --- format the date as per the sheet
    * @param Date dtDate
    * @exception Any exception
    * @return String date
    */
	public static String formatDate(Date dtDate){
		if(dtDate == NULL)
			return '';
		else
			return dtDate.month() + '/' + dtDate.day() + '/' + dtDate.year();
	}
	
	/**
    * getProducts --- get products
    * @param
    * @exception Any exception
    * @return List<HEP_MDP_Promotion_Product_Detail__c>
    */
	public List<HEP_MDP_Promotion_Product_Detail__c> getProducts(){

		if(Test.isRunningTest())
			iDateLimit = 0;
	
		return [SELECT 
					Id, Name, Channel__c, Promo_WSP__c, CurrencyIsoCode,
					HEP_Promotion_MDP_Product__r.Record_Status__c, Record_Status__c,
					HEP_Promotion_MDP_Product__r.Title_Name__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Record_Status__c, 
					HEP_Promotion_MDP_Product__r.WPR_Id__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Name,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.PromotionName__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.CustomerNotes__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c,
					HEP_Promotion_MDP_Product__r.HEP_Territory__r.Flag_Country_Code__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.StartDate__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.EndDate__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Promo_Type__c,
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.LastModifiedDate
				FROM 
					HEP_MDP_Promotion_Product_Detail__c
				WHERE
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.LastModifiedDate <= :Date.today().addDays(iDateLimit + 1)
				AND
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Suisse_Updated__c = false
				AND
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c != NULL
				AND
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Promotion_Type__c = :sCUSTOMER
				AND
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer_Promotion_Status__c = :sLIVE
				AND
					HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Record_Status__c IN (:sACTIVE, :sDELETED)
				AND
					HEP_Promotion_MDP_Product__r.Record_Status__c IN (:sACTIVE, :sDELETED)
				AND
					Record_Status__c IN (:sACTIVE, :sDELETED)];
	}
	
	/**
    * getProducts --- get getRecords
    * @param
    * @exception Any exception
    * @return 
    */
	public void getRecords(){
	    try{
	    	sFileName = HEP_Utility.getConstantValue('HEP_SUISSE_EMAIL_SUBJECT') + '_' + DateTime.now().format(HEP_Utility.getConstantValue('HEP_SUISSE_EMAIL_DATE_FORMAT')) +'.xls';
			objPageData = new PageData();
			objPageData.lstPromoProducts = new List<ProductWrapper>();
			objPageData.setPromoProductsUpdated = new Set<String>();
			for(HEP_MDP_Promotion_Product_Detail__c objProdDetail : getProducts()){
				System.debug('objProdDetail>>' + objProdDetail);
				if(objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.LastModifiedDate.date() <= Date.today().addDays(iDateLimit)){
			    	if(objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Record_Status__c == sACTIVE
			    		&& objProdDetail.HEP_Promotion_MDP_Product__r.Record_Status__c == sACTIVE
			    		&& objProdDetail.Record_Status__c == sACTIVE){
			    		ProductWrapper objProducts = new ProductWrapper();
			    		objProducts.sWPRId = objProdDetail.HEP_Promotion_MDP_Product__r.WPR_Id__c;
			    		objProducts.sTitle = objProdDetail.HEP_Promotion_MDP_Product__r.Title_Name__c;
			    		objProducts.sPromoDescription = objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.PromotionName__c + 
			    										' - ' + objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Name;
			    		objProducts.sLicensee = objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Customer__r.CustomerName__c;
			    		objProducts.sTerritory = objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Territory__r.Flag_Country_Code__c;
			    		objProducts.sCurrency = objProdDetail.CurrencyIsoCode;
			    		objProducts.sMedia = objProdDetail.Channel__c;
			    		objProducts.dPromoPrice = objProdDetail.Promo_WSP__c;
			    		objProducts.sPromoStartDate = formatDate(objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.StartDate__c);
			    		objProducts.sPromoEndDate = formatDate(objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.EndDate__c);
			    		objProducts.sPromoType = objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Promo_Type__c;
			    		objPageData.lstPromoProducts.add(objProducts);
			    	} else if(objProdDetail.HEP_Promotion_MDP_Product__r.Record_Status__c == sDELETED){
			    		objPageData.setPromoProductsUpdated.add(objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.PromotionName__c + 
			    		' - ' + objProdDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Name);
			    	}
				}
		    }
		} catch(Exception ex){
             
            System.debug('Exception on Class : HEP_Suisse_EmailXLSAttachmentController - constructor, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
        	HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_Suisse_EmailXLSAttachmentController','constructor',null,null);
        }
	}
	
	/**
    * PageData -- Wrapper to store the Page data
    * @author    Nidhin
    */
	public class PageData{
		public List<ProductWrapper> lstPromoProducts {
		    get;
		    set;
		}
		public Set<String> setPromoProductsUpdated {
		    get;
		    set;
		}
	}
	
	/**
    * PageData -- Wrapper to store the product details values
    * @author    Nidhin
    */
	public class ProductWrapper{
		public String sWPRId{get;set;}
		public String sTitle{get;set;}
		public String sPromoDescription{get;set;}
		public String sLicensee{get;set;}
		public String sTerritory{get;set;}
		public String sCurrency{get;set;}
		public String sMedia{get;set;}
		public String sPromoStartDate{get;set;}
		public String sPromoEndDate{get;set;}
		public String sPromoType{get;set;}
		public Decimal dPromoPrice{get;set;}
	}
}