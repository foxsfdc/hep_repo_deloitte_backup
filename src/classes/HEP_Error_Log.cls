/**
* HEP_Error_Log --- his is a generic class created to handle exception in case of Integration and application scenarios. This provides different methods to handle the error logging. Create error record/log attachments against it
* @author    Tejaswini Veliventi
*/

public with sharing class HEP_Error_Log {
    
    /**
    * This method is used to capture business exceptions or DML exceptions. It accepts the error and the error description as input
    * @param    
    *    Parameter 1 : Error Name - unique name for the record
    *    Parameter 2 : Error Type - type of the error (inbound,outbound,javascript,component etc.). This is a picklist field
    *    Parameter 3 : Error Message - Message details in short
    *    Parameter 4 : Error Description - Detailed stack trace of the message
    *    Parameter 5 : HEP Class Name - Class in which the error occured
    *    Parameter 6 : HEP Method Name - Method in which the error occured
    *    Parameter 7 : Object Id - Record id for which the error occured
    *    Parameter 8 : URL, for integrations
    * @return   error object id to further add attachments under it
    */

    public static String businessException(String sErrorName, String sErrorType,  String sErrorMessage, String sErrorDesc,
                                           String sClassName, String sClassMethod,
                                           String sObjectId, String sURL) {
        HEP_Error_Log__c objError = new HEP_Error_Log__c();
        objError.HEP_Class_Name__c = sClassName;
        objError.HEP_Class_Method__c = sClassMethod;

        objError.Error_Type__c = sErrorType;

        if (String.isNotBlank(sErrorMessage) && sErrorMessage.length() > 254)
            objError.HEP_Error_Message__c = sErrorMessage.substring(0, 254);
        else
            objError.HEP_Error_Message__c = sErrorMessage;

        //updates the object id. This can be a salesforce id or external id
        if (String.isNotBlank(sObjectId) &&  sObjectId.length() > 99)
            objError.HEP_Object_Id__c  = sObjectId.substring(0, 99);
        else
            objError.HEP_Object_Id__c  = sObjectId;

        if(String.isNotBlank(sErrorDesc) &&  sErrorDesc.length() > 32767)
            objError.Message__c = sErrorDesc.substring(0,32767);
        else 
            objError.Message__c = sErrorDesc;
        
        if (String.isNotBlank(sErrorType) &&  sErrorType.length() > 39)
            objError.Exception_Type__c = sErrorType.substring(0, 39);
        else
            objError.Exception_Type__c = sErrorType;
        
        //Stores the url. This is could be a callout endpoint url or the page url for javascript exceptions
        objError.Source_URL__c = sURL;

        if (String.isNotBlank(sErrorName) &&  sErrorName.length() > 79)
            objError.Name = sErrorName.substring(0, 79);
        else
            objError.Name = sErrorName;


        insert objError;
        System.debug('objError==' + objError);

        return objError.Id;
    }


    /**
    * This method is used to capture generic exceptions. It accepts the exception object as input
    * @param    
    *    Parameter 1 : Error Name - unique name for the record
    *    Parameter 2 : Error Type - type of the error (inbound,outbound,javascript,component etc.). This is a picklist field
    *    Parameter 3 : Exception - Exception object obtained 
    *    Parameter 4 : HEP Class Name - Class in which the error occured
    *    Parameter 5 : HEP Method Name - Method in which the error occured
    *    Parameter 6 : Object Id - Record id for which the error occured
    *    Parameter 7 : URL, for integrations
    * @return   error object id to further add attachments under it
    */
    public static String genericException(String sErrorName, String sErrorType,  Exception e,
                                          String sClassName, String sClassMethod,
                                          String sObjectId, String sURL) {
        HEP_Error_Log__c objError = new HEP_Error_Log__c();
        objError.HEP_Class_Name__c = sClassName;
        objError.HEP_Class_Method__c = sClassMethod;

        objError.Error_Type__c = sErrorType;

        if (e == null) {
            objError.HEP_Error_Message__c  = 'No Exception';
            objError.Message__c = 'No Exception';
            objError.Exception_Type__c = 'No Exception';
        } 
        else {
            //gets the exception details and puts it on the error log record
            String sErrMsg = e.getTypeName() + '++Message: ' + e.getMessage();
            if (sErrMsg.length() > 254)
                objError.HEP_Error_Message__c = sErrMsg.substring(0, 254);
            else
                objError.HEP_Error_Message__c = sErrMsg;
            objError.Message__c = e.getStackTraceString();
            if (e.getTypeName().length() > 39)
                objError.Exception_Type__c = e.getTypeName().substring(0, 39);
            else
                objError.Exception_Type__c = e.getTypeName();
        }
        //updates the object id. This can be a salesforce id or external id
        if (String.isNotBlank(sObjectId) && sObjectId.length() > 99)
            objError.HEP_Object_Id__c  = sObjectId.substring(0, 99);
        else
            objError.HEP_Object_Id__c  = sObjectId;
        objError.Source_URL__c = sURL;

        if (String.isNotBlank(sErrorName) && sErrorName.length() > 79)
            objError.Name = sErrorName.substring(0, 79);
        else
            objError.Name = sErrorName;
        insert objError;
        System.debug('objError==' + objError);
        return objError.Id;
    }


    /**
    * This method is used to capture business exceptions or DML exceptions. It accepts the error and the error description as input
    * @param    
    *    Parameter 1 : Error Id - id of the error log record created for the exception
    *    Parameter 2 : Database Result - list of Database Result records for DML operations
    * @return   no return value
    */  

    public static void logDMLerrors(String sErrorId, List<Database.SaveResult> lstSaveResult, List<Database.UpsertResult> lstUpsertResult) {
        string sErrorMsg = '';
        
        //looks out for any insert results and updates the DML Exception details on error log record
        if (lstSaveResult != null) {
            for (Integer i = 0; i < lstSaveResult.size(); i++) {
                Database.SaveResult objResult = lstSaveResult[i];
                system.debug('result===============' + objResult);
                system.debug('result===============' + objResult.getErrors());
                system.debug('result===============' + objResult.isSuccess());
                if (!objResult.isSuccess() ) {
                    for (Database.Error objError : objResult.getErrors()) {
                        sErrorMsg += sErrorMsg;
                        System.debug('The following error has occurred.');
                        System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                        System.debug('Fields that affected this error: ' + objError.getFields());
                        sErrorMsg = i + ' ' + objError.getMessage();
                        sErrorMsg = sErrorMsg + '++++++\n Error Fields that caused exception:\n' + objError.getFields() + '\n+++++++++';
                    }
                }
            }
        }

        //looks out for any upsert results and updates the DML Exception details on error log record

        if (lstUpsertResult != null) {

            for (Integer i = 0; i < lstUpsertResult.size(); i++) {

                Database.UpsertResult objResult = lstUpsertResult[i];
                system.debug('result===============' + objResult);
                system.debug('result===============' + objResult.getErrors());
                system.debug('result===============' + objResult.isSuccess());
                if (!objResult.isSuccess() ) {
                    for (Database.Error objError : objResult.getErrors()) {
                        sErrorMsg += sErrorMsg;
                        System.debug('The following error has occurred.');
                        System.debug(objError.getStatusCode() + ': ' + objError.getMessage());
                        System.debug('Fields that affected this error: ' + objError.getFields());
                        sErrorMsg = i + ' ' + objError.getMessage();
                        sErrorMsg = sErrorMsg + '++++++\n Error Fields that caused exception:\n' + objError.getFields() + '\n+++++++++';
                    }
                }

            }
            HEP_Error_Log__c objError = new HEP_Error_Log__c(id = sErrorId, Message__c = sErrorMsg);
            update objError;
            //Creates an attachment to hold the complete error information to avoid data loss
            InsertAttachment( sErrorId,  'LogDMLerrors', sErrorMsg);

        }
    }


    /**
    * This method is used to capture generic exceptions. It accepts the exception object as input
    * @param    
    *    Parameter 1 : Error Id - id of the error log record created for the exception
    *    Parameter 2 : FileName - file which contains the error details
    *    Parameter 3 : Error Text - short description of the error
    * @return   no return value
    */
    public static void insertAttachment(String sErrorId, String sFileName, String sErrorText) {
        if(String.isNotBlank(sErrorText) && String.isNotBlank(sErrorId)){
            Attachment objAtt = new Attachment();
            objAtt.Body = Blob.valueOf(sErrorText); 
            objAtt.Name = sFileName + '.txt';
            objAtt.parentId = sErrorId;
            insert objAtt;
        }
    }
}