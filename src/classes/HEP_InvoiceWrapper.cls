/**
* HEP_InvoiceWrapper -- Invoice Wrapper to store the invoice details from JDE And E1 Interfaces 
* @author    Lakshman Jinnuri
*/
public class HEP_InvoiceWrapper{
    public list<Invoices> Invoices;
    public String calloutStatus;
    public String TxnId;
    public list<Errors> errors;
    
    
    /**
    * Invoices -- Invoice Wrapper to store the invoice details
    * @author    Lakshman Jinnuri
    */
    public class Invoices{
       //common fields for both interfaces JDE and E1
        public String CURRENCY_TYPE;
        public String GL_LINE_NO;
        public String ACTUAL_TYPE;
        public String NAME;
        public String INVOICE_NO;
        public String COUNTRY;
        public String INVOICE_STATUS;
        public String OBJECT_NO;
        public String PROMO;
        public String ACTUAL;
        public String LINE_DESCRIPTION;
        public String INV_DATE;
        public String FORMAT;
        public String PO_NO;
        public String POTYPE;
        public String VENDOR;

        //Impana --- Added for NTS Integration
        public String TERRITORY_BU;
        public String CATALOG_NO;
        public String GLDGJ;
    }
    
    /**
    * Errors -- Class to hold related details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status; //400
    }  

    /**
    * HEP_TerritoryDetails  -- Class to hold related details to be sent to the JDE and E1
    * @author    Lakshman Jinnuri
    */
    public class HEP_TerritoryDetails{
        public String sPromoId;
        public String sTerritory;
    }   
}