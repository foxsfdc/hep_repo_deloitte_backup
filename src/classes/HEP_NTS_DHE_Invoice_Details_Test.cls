/**
* HEP_NTS_DHE_Invoice_Details_Test -- Test class for the HEP_NTS_DHE_Invoice_Details. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_NTS_DHE_Invoice_Details_Test{
    /**
    * HEP_NTS_DHE_Invoice_Details_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_NTS_DHE_Invoice_Details_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_NTS_DHE_InvoiceDetails','HEP_NTS_DHE_Invoice_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_NTS_DHE_Invoice_Details.HEP_NTSInvoiceParametersWrapper objInvoiceDetails = new  HEP_NTS_DHE_Invoice_Details.HEP_NTSInvoiceParametersWrapper();
        objInvoiceDetails.sFromDate = '90001';
        objInvoiceDetails.sToDate = '119157';
        objInvoiceDetails.sLegalTitle = '';
        objInvoiceDetails.sFinDivisionCode = '';
        objInvoiceDetails.sFoxId = '';
        String sJsonString = JSON.serialize(objInvoiceDetails);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJsonString,'','HEP_NTS_DHE_InvoiceDetails');
        System.assertEquals('HEP_NTS_DHE_InvoiceDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_NTS_DHE_Invoice_Details_FailureTest -- Test method for the failure response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_NTS_DHE_Invoice_Details_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_NTS_DHE_InvoiceDetails','HEP_NTS_DHE_Invoice_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_NTS_DHE_Invoice_Details.HEP_NTSInvoiceParametersWrapper objInvoiceDetails = new  HEP_NTS_DHE_Invoice_Details.HEP_NTSInvoiceParametersWrapper();
        objInvoiceDetails.sFromDate = '900011';
        objInvoiceDetails.sToDate = '119157';
        objInvoiceDetails.sLegalTitle = '';
        objInvoiceDetails.sFinDivisionCode = '';
        objInvoiceDetails.sFoxId = '';
        String sJsonString = JSON.serialize(objInvoiceDetails);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJsonString,'','HEP_NTS_DHE_InvoiceDetails');
        System.assertEquals('HEP_NTS_DHE_InvoiceDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_NTS_DHE_Invoice_Details_InvalidAcessTokenTest -- Test method to get Rating details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_NTS_DHE_Invoice_Details_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService =  [Select Id,Name from HEP_Services__c where Name =: 'HEP_NTS_E1_OAuth'];
        System.debug('objService'+objService);
        objService.Name='HEP_NTS_E1_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        update lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTS_DHE_Invoice_Details objNTSInvoiceDetails = new HEP_NTS_DHE_Invoice_Details();
        objNTSInvoiceDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
}