@isTest
public class HEP_CustomerMapping_Controller_Test{
    public static testmethod void testFetchData(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //User to run use for RunAs
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        //Create another Territory record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code1';
        objTerritory1.Territory_Code_Integration__c = '0001';
        objTerritory1.MM_Territory_Code__c = 'u';
        //objTerritory1.Record_Status__c = 'Active';
        insert objTerritory1;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Role Record
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create User Role Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create another User Role Record
        HEP_User_Role__c objUserRole1 = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        objUserRole1.Record_Status__c = 'Active';
        insert objUserRole1;
        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('xBox',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.Big_Id__c = 'b01';
        objMapping.Media_Id__c = 'm01';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'EST';
        objMapping.Format__c = 'HD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Title__c = objTitle.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        System.runAs(u){
            test.startTest();
            //Calling fetchData method
            HEP_CustomerMapping_Controller.fetchData(objTitle.Id);
            test.stopTest();
        }
    }
    static testmethod void testFetchDataAmazon(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //User to run use for RunAs
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        //Create another Territory record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code1';
        objTerritory1.Territory_Code_Integration__c = '0001';
        objTerritory1.MM_Territory_Code__c = 'u';
        //objTerritory1.Record_Status__c = 'Active';
        insert objTerritory1;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Role Record
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create User Role Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create another User Role Record
        HEP_User_Role__c objUserRole1 = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        objUserRole1.Record_Status__c = 'Active';
        insert objUserRole1;
        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.ASIN__c = 'id01';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'SD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Title__c = objTitle.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        System.runAs(u){
            test.startTest();
            //Calling fetchData method
            HEP_CustomerMapping_Controller.fetchData(objTitle.Id);
            test.stopTest();
        }   
    }
    static testmethod void testFetchDataApple(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //User to run use for RunAs
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        //Create another Territory record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code1';
        objTerritory1.Territory_Code_Integration__c = '0001';
        objTerritory1.MM_Territory_Code__c = 'u';
        //objTerritory1.Record_Status__c = 'Active';
        insert objTerritory1;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Role Record
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create User Role Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create another User Role Record
        HEP_User_Role__c objUserRole1 = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        objUserRole1.Record_Status__c = 'Active';
        insert objUserRole1;
        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','EPSD',false);
        insert objProductType;
        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple iTunes',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.Adam_Id__c = 'id001';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'HD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Title__c = objTitle.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        System.runAs(u){
            test.startTest();
            //Calling fetchData method
            HEP_CustomerMapping_Controller.fetchData(objTitle.Id);
            test.stopTest();
        }   
    }
    static testmethod void testFetchDataGoogle(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //User to run use for RunAs
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        //Create another Territory record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code1';
        objTerritory1.Territory_Code_Integration__c = '0001';
        objTerritory1.MM_Territory_Code__c = 'u';
        //objTerritory1.Record_Status__c = 'Active';
        insert objTerritory1;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Role Record
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create User Role Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create another User Role Record
        HEP_User_Role__c objUserRole1 = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        objUserRole1.Record_Status__c = 'Active';
        insert objUserRole1;
        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','FEATR',false);
        insert objProductType;
        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Google',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.Adam_Id__c = 'id001';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'HD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Title__c = objTitle.id;
        //objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        System.runAs(u){
            test.startTest();
            //Calling fetchData method
            HEP_CustomerMapping_Controller.fetchData(objTitle.Id);
            test.stopTest();
        }   
    }
    public static testmethod void testSaveProductData(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','FEATR',false);
        insert objProductType;
        //Create Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_CustomerMapping_Controller.ProductMappingWrapper objOuter = new HEP_CustomerMapping_Controller.ProductMappingWrapper();
        //Inner Wrapper record which holds mapping data fetched from Mapping object 
        HEP_CustomerMapping_Controller.ProductDataWrapper ObjInner = new HEP_CustomerMapping_Controller.ProductDataWrapper();
        objInner.sChannel = 'VOD';
        objInner.sFormat = 'HD';
        objOuter.lstProductData.add(objInner);
        objOuter.sSelectedRegion = 'Australia';
        objOuter.sSelectedCustomer = 'Amazon';
        objOuter.sIdTitle = objTitle.Id;
        objOuter.sSelectedRegionId = objTerritory.Id;
        map<String,String> tmpAccessMap = new map<String,String>();
        tmpAccessMap.put('WRITE','TRUE');
        objOuter.mapTerritoryAccess = tmpAccessMap;
        HEP_CustomerMapping_Controller.saveProductData(objOuter);
        
    }
    public static testmethod void testSaveProductData2(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','FEATR',false);
        insert objProductType;
        //Create Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.Adam_Id__c = 'id001';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'HD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Title__c = objTitle.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        HEP_CustomerMapping_Controller.ProductMappingWrapper objOuter = new HEP_CustomerMapping_Controller.ProductMappingWrapper();
        //Inner Wrapper record which holds mapping data fetched from Mapping object 
        HEP_CustomerMapping_Controller.ProductDataWrapper ObjInner = new HEP_CustomerMapping_Controller.ProductDataWrapper();
        objInner.sChannel = 'VOD';
        objInner.sFormat = 'HD';
        objInner.sRecordId = objMapping.Id;
        objOuter.lstProductData.add(objInner);
        objOuter.sSelectedRegion = 'Australia';
        objOuter.sSelectedCustomer = 'Amazon';
        objOuter.sIdTitle = objTitle.Id;
        objOuter.sSelectedRegionId = objTerritory.Id;
        map<String,String> tmpAccessMap = new map<String,String>();
        tmpAccessMap.put('WRITE','TRUE');
        objOuter.mapTerritoryAccess = tmpAccessMap;
        HEP_CustomerMapping_Controller.saveProductData(objOuter);
        
    }
}