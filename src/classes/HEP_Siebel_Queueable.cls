/**
* HEP_Siebel_Queueable --- Class to perform callouts to Insert/Update/Delete data at Siebel interface
* @author Lakshman Jinnuri
*/
public class HEP_Siebel_Queueable implements Queueable, Database.AllowsCallouts {
    public String sObjType; 
    public String sJson; 
    
    /**
    * Class constructor to initialize the class variables
    * @return nothing 
    * @author Lakshman Jinnuri 
    */  
    public HEP_Siebel_Queueable(String sJson, String sObjType){
        this.sObjType = sObjType;
        this.sJson = sJson;
    }
    
    /**
    * execute -- makes the callout and gets the JSessionId through response
    * @param  Takes no input 
    * @return nothing   
    * @author Lakshman Jinnuri      
    */
    public void execute(QueueableContext qContext) {
        system.debug('Inside execute Siebel Queueable job');
        system.debug('sJson in execute integrate method ' + sJson);
        system.debug('sObjType in execute integrate method ' + sObjType);
        HEP_ExecuteIntegration.executeIntegMethod(sJson,'',sObjType);
    }
}