@isTest
private class HEP_ExecuteIntegration_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        //List<HEP_Constants__c> lstHEP = Test.loadData(HEP_Constants__c.sobjectType, 'testHEPConstant');

        // User Created
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Jain', 'anshuljain787@deloitte.com', 'AJ', 'AJ', 'AJ', '', false);
        insert objUser;

        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'ARS', 'DHE', 'DHE', '', false);
        insert objTerritory;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion', 'Global', '', objTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        insert objPromotion;

    }
    
    
    
    @isTest
    static void testContentDocumentSourceId1(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        HEP_Interface_Transaction_Error__c objTxnError = new HEP_Interface_Transaction_Error__c();
        insert objTxnError;
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'Source';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];


        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=objTxnError.id;
        contentlink.ShareType= 'I';
        contentlink.ContentDocumentId=testcontent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        

        
        System.runAs(lstUser[0]) {
            insert contentlink;
            Test.startTest();
                HEP_ExecuteIntegration.contentDocumentSourceId(objTxnError.Id, false);
            Test.stopTest();
        }
        
    }
    
    @isTest
    static void testContentDocumentSourceId2(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        HEP_Interface_Transaction_Error__c objTxnError = new HEP_Interface_Transaction_Error__c();
        insert objTxnError;
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'Request';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];


        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=objTxnError.id;
        contentlink.ShareType= 'I';
        contentlink.ContentDocumentId=testcontent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        

        
        System.runAs(lstUser[0]) {
            insert contentlink;
            Test.startTest();
                HEP_ExecuteIntegration.contentDocumentSourceId(objTxnError.Id, true);
            Test.stopTest();
        }
        
    }
    
    
    @isTest
    static void testexecuteIntegMethod1(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        
        HEP_Interface__c objInterface= new HEP_Interface__c();
        objInterface.Name = 'HEP_PurchaseOrderDetails';
        objInterface.Source__c = 'JDE AND E1'; 
        objInterface.Destination__c = 'HEP'; 
        objInterface.Class__c = 'HEP_PurchaseOrder_Details'; 
        objInterface.Integration_Name__c = 'HEP_PurchaseOrderDetails'; 
        objInterface.Retry_Flag__c = true; 
        objInterface.Retry_Interval__c = 2; 
        objInterface.Retry_Limit__c = 1;
        objInterface.Type__c = 'Inbound';
        objInterface.Store_Source_As_Attachment__c = false;
        objInterface.Create_Content_File__c = true;
        objInterface.Record_Status__c = 'Active';
        insert objInterface;
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInterface.Id;
        objTxn.Retry_Count__c = 1;
        insert objTxn;

        System.runAs(lstUser[0]) {
            
            Test.startTest();
                HEP_ExecuteIntegration.executeIntegMethod(lstPromotion[0].Id,objTxn.Id, 'Test');
            Test.stopTest();
        }
    }
    
     @isTest
    static void testexecuteIntegMethod2(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        
        HEP_Interface__c objInterface= new HEP_Interface__c();
        objInterface.Name = 'HEP_PurchaseOrderDetails';
        objInterface.Source__c = 'JDE AND E1'; 
        objInterface.Destination__c = 'HEP'; 
        objInterface.Class__c = 'HEP_PurchaseOrder_Details'; 
        objInterface.Integration_Name__c = 'HEP_PurchaseOrderDetails'; 
        objInterface.Retry_Flag__c = true; 
        objInterface.Retry_Interval__c = 2; 
        objInterface.Retry_Limit__c = 1;
        objInterface.Type__c = 'Inbound';
        objInterface.Store_Source_As_Attachment__c = false;
        objInterface.Create_Content_File__c = true;
        objInterface.Record_Status__c = 'Active';
        insert objInterface;
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInterface.Id;
        objTxn.Retry_Count__c = 1;
        insert objTxn;

        System.runAs(lstUser[0]) {
            
            Test.startTest();
                HEP_ExecuteIntegration.executeIntegMethod(lstPromotion[0].Id,'', 'HEP_PurchaseOrderDetails');
            Test.stopTest();
        }
    }
    
    
    @isTest
    static void testexecuteIntegMethod3(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        
        HEP_Interface__c objInterface= new HEP_Interface__c();
        objInterface.Name = 'HEP_PurchaseOrderDetails';
        objInterface.Source__c = 'JDE AND E1'; 
        objInterface.Destination__c = 'HEP'; 
        objInterface.Class__c = 'HEP_PurchaseOrder_Details'; 
        objInterface.Integration_Name__c = 'HEP_PurchaseOrderDetails'; 
        objInterface.Retry_Flag__c = true; 
        objInterface.Retry_Interval__c = 2; 
        objInterface.Retry_Limit__c = 1;
        objInterface.Type__c = 'Outbound-Pull';
        objInterface.Store_Source_As_Attachment__c = true;
        objInterface.Create_Content_File__c = true;
        objInterface.Record_Status__c = 'Active';
        insert objInterface;
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInterface.Id;
        objTxn.Retry_Count__c = 1;
        insert objTxn;

        
        System.runAs(lstUser[0]) {
            
            Test.startTest();
                HEP_ExecuteIntegration.executeIntegMethod(lstPromotion[0].Id,objTxn.Id, 'Test');
            Test.stopTest();
        }
    }
    
    @isTest
    static void testexecuteIntegMethod4(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        
        System.runAs(lstUser[0]) {
            
            Test.startTest();
                HEP_ExecuteIntegration.executeIntegMethod(lstPromotion[0].Id,'', '');
            Test.stopTest();
        }
    }
 
    @isTest
    static void testexecuteIntegMethod5(){
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        
        HEP_Interface__c objInterface= new HEP_Interface__c();
        objInterface.Name = 'HEP_PurchaseOrderDetails';
        objInterface.Source__c = 'JDE AND E1'; 
        objInterface.Destination__c = 'HEP'; 
        objInterface.Class__c = 'HEP_PurchaseOrder_Details'; 
        objInterface.Integration_Name__c = 'HEP_PurchaseOrderDetails'; 
        objInterface.Retry_Flag__c = true; 
        objInterface.Retry_Interval__c = 2; 
        objInterface.Retry_Limit__c = 1;
        objInterface.Type__c = 'Outbound-Pull';
        objInterface.Store_Source_As_Attachment__c = true;
        objInterface.Create_Content_File__c = true;
        objInterface.Record_Status__c = 'Inactive';
        insert objInterface;
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInterface.Id;
        objTxn.Retry_Count__c = 1;
        insert objTxn;

        System.runAs(lstUser[0]) {
            
            Test.startTest();
                HEP_ExecuteIntegration.executeIntegMethod(lstPromotion[0].Id,objTxn.Id, 'Test');
            Test.stopTest();
        }
    }
    
    
    
}