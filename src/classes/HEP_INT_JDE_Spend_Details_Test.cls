/**
* HEP_INT_JDE_Spend_Details_Test-- Test class for the HEP_INT_JDE_Spend_Details for JDE Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_INT_JDE_Spend_Details_Test{
    /**
    * CreateTestData --  Test method to create the required data for test coverage
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static List<Id> CreateTestData() {
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate1';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'template1FAD';
        insert objTempGlobalFADnotification; 
        HEP_Interface__c objInterfacespend = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_Spend_Interface',true,10,10,'Outbound-Push',true,true,false);
        objInterfacespend.Name = 'HEP_E1_SpendDetail';
        objInterfacespend.Record_Status__c = 'Active';
        insert objInterfacespend;
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, true);
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_INT_JDE_SPEND_DETAILS','HEP_INT_JDE_Spend_Details',true,10,10,'Outbound-Push',true,true,true);
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion1','Global','',objTerritory.Id,objLOB.Id,null,null,false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion1', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('12345',objPromotion.Id,'Active', false);
        objSpend.RecordStatus__c ='Pending';
        objSpend.RequestDate__c = System.today();
        insert objSpend;
        objSpend.RecordStatus__c ='Budget Approved';
        Update objSpend;
        HEP_GL_Account__c objGlAccount = HEP_Test_Data_Setup_Utility.createGLAccount('Global / Online','51557','Online','Marketing Spend','Active',true);
        objGlAccount.Format__c = 'PHY';
        update objGlAccount;
        HEP_Market_Spend_Detail__c objSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objSpend.Id, objGlAccount.Id,'Active',false);
        objSpendDetail.Type__c = 'Current';
        objSpendDetail.Budget__c = 100;
        objSpendDetail.Unique_Id__c = '123456';
        insert objSpendDetail;
        HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('Product Unlock', 'HEP_Promotion_SKU__c', 'Locked_Status__c', 'Unlocked' , 'Locked', 'PRODUCT UNLOCK', 'Static', 'Approved', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', 'Products', true);
        HEP_Approval_Type_Role__c objApprvalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalType.Id, objRole.Id, 'ALWAYS', true);
        HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Approved', objApprovalType.Id, null, true);
        objApproval.HEP_Market_Spend__c = objSpend.Id;
        update objApproval;
        List<Id> lstSpendIds = new list<Id>();
        lstSpendIds.add(objSpend.Id);
        return lstSpendIds;
        
    }
    /**
    * HEP_INT_JDE_Spend_Details_Test --  Test method to send the details to the JDE interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Spend_Details_Test() {
        list<Id> lstIds = CreateTestData();
        String sSpendJson = JSON.serialize(lstIds);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSpendJson,'','HEP_INT_JDE_SPEND_DETAILS');
        System.assertEquals('HEP_INT_JDE_SPEND_DETAILS',objTxnResponse.sInterfaceName);
        objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSpendJson,'','HEP_INT_JDE_SPEND_DETAILS');
        HEP_INT_JDE_Spend_Details.HEP_JDE_SpendWrapper objspendWrapper = new  HEP_INT_JDE_Spend_Details.HEP_JDE_SpendWrapper();
        Test.stoptest();
    }     
    /**
    * HEP_INT_JDE_Spend_Details_Test --  Test method to send the details to the JDE interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_INT_JDE_Spend_Details_Test2() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, true);
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterfacespend = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_Spend_Interface',true,10,10,'Outbound-Push',true,true,false);
        objInterfacespend.Name = 'HEP_E1_SpendDetail';
        objInterfacespend.Record_Status__c = 'Active';
        insert objInterfacespend;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_INT_JDE_SPEND_DETAILS','HEP_INT_JDE_Spend_Details',true,10,10,'Outbound-Push',true,true,true);
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion1','Global','',objTerritory.Id,objLOB.Id,null,null,false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion1', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('12345',objPromotion.Id,'Active', false);
        objSpend.RecordStatus__c ='Pending';
        objSpend.RequestDate__c = System.today();
        insert objSpend;
        HEP_Market_Spend__c objSpend2 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('123454',objGlobalPromotion.Id,'Active', false);
        objSpend2.RecordStatus__c ='Pending';
        objSpend2.RequestDate__c = System.today();
        insert objSpend2;
        HEP_GL_Account__c objGlAccount = HEP_Test_Data_Setup_Utility.createGLAccount('Global / Online','51557','Online','Marketing Spend','Active',true);
        objGlAccount.Format__c = 'PHY';
        update objGlAccount;
        HEP_Market_Spend_Detail__c objSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objSpend.Id, objGlAccount.Id,'Active',false);
        objSpendDetail.Type__c = 'Current';
        objSpendDetail.Budget__c = 100;
        objSpendDetail.Unique_Id__c = '123456';
        insert objSpendDetail;
        Map<Id, HEP_Market_Spend__c> oldMap = new Map<Id, HEP_Market_Spend__c>();
        Map<Id, HEP_Market_Spend__c> newMap = new Map<Id, HEP_Market_Spend__c>();
        HEP_Market_Spend__c objSpend1 = [SELECT ID,RecordStatus__c FROM HEP_Market_Spend__c WHERE ID =: objSpend.Id];
        oldMap.put(objSpend1.Id,objSpend1);

        objSpend.RecordStatus__c ='Budget Approved';
        Update objSpend;
        newMap.put(objSpend.Id,objSpend);
        try{
        HEP_MarketSpendTriggerHandler.createSpendHistory(oldMap,newMap);
        }catch(Exception e){}
        
    }    
}