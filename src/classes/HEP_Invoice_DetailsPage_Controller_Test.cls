@isTest
public class HEP_Invoice_DetailsPage_Controller_Test{
    public static testMethod void getInvoiceDetailsTest(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Territory record
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        objTerritory.SKU_Interface__c = 'ESCO';
        insert objTerritory;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create Promotion Record - Global
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        insert objPromotion1;
        //GL Account
        HEP_GL_Account__c objGLAccount = HEP_Test_Data_Setup_Utility.createGLAccount('GL','GLA','Desc','group','Active',false);
        objGLAccount.GLAccount__c = '642100';
        objGLAccount.Account_Department__c = 'aa';
        insert objGLAccount;
        String POResponse = '{"purchaseOrders": [{"OPEN": "0.00","RUSHCODE": "","POLINE_NO": "50000","NAME": "IMAGINE FULFILLMENT SERVICES","PO_NO": "27933","COUNTRY": "DHE","OBJECT_NO": "53202","ITEM_AMT": "1333.33","LINE_DESC": "STORAGE & FULFILLMENT SVCS","PROMO": "27933","ACTUAL": "0.00","REQ_DATE": "02/19/2017","POSTATUS": "915","NETWORKID": "CHRISBES","RUSHGPCODE": "","FORMAT": "910","REQUESTOR": "CHRIS BESS","VENDOR": "116258"},{"OPEN": "300.00","RUSHCODE": "","POLINE_NO": "4.000","NAME": "RDP Limited","PO_NO": "27933","COUNTRY": "DHE","OBJECT_NO": "630100","ITEM_AMT": "300.00","LINE_DESC": "Other Promo Costs-Misc","PROMO": "40550","ACTUAL": "0.00","REQ_DATE": "11/22/2016","POSTATUS": "Open","NETWORKID": "SARIKAR","RUSHGPCODE": "","FORMAT": null,"REQUESTOR": null,"POTYPE": "OR","VENDOR": "71389"}]}';
        String invoiceResponse = '{"invoices": [{"GL_LINE#": "55.0","ACTUAL_TYPE": "Non-PO","NAME": "HMV L3","INVOICE#": "47685","COUNTRY": "","INVOICE_STATUS": "Journal Only","OBJECT": "642100","PROMO": "40550","ACTUAL": "21.49","LINE_DESCRIPTION": "","CURRENCY": "GBP","INV_DATE": "11/29/2017","FORMAT": "DVD","PO#": "","POTYPE": "","VENDOR": "354802"}]}'  ;
        HEP_Utility.processPOInvoice(POResponse,invoiceResponse,objPromotion1.Id);
        Attachment attch = new Attachment();
        attch.ParentId = objPromotion1.Id;
        attch.Name = 'POINVOICEDATA';
        attch.Body = Blob.valueOf(invoiceResponse);
        insert attch;
        //
        PageReference pageRef = Page.HEP_Invoice_Page;
        pageRef.getParameters().put('promoId',objPromotion1.Id);
        Test.setCurrentPage(pageRef);
        Test.startTest();
        HEP_Invoice_DetailsPage_Controller.getInvoiceDetails(objPromotion1.Id,'47685',objGLAccount.Id);
        HEP_Invoice_DetailsPage_Controller objInvoice = new HEP_Invoice_DetailsPage_Controller();
        objInvoice.checkPermissions();
        Test.stopTest();
    }

}