/**
* HEP_RatingDetails_Test -- Test class for the HEP_RatingDetails for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_RatingDetails_Test{
    /**
    * HEP_RatingDetails_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_RatingDetails_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Rating','HEP_RatingDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('67728','','HEP_Foxipedia_Rating');
        System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_RatingDetails_FailureTest -- Test method for the failure response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_RatingDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Rating','HEP_RatingDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('aer567854444','','HEP_Foxipedia_Rating');
        System.assertEquals('HEP_Foxipedia_Rating',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_RatingDetails_InvalidAcessTokenTest -- Test method to get Rating details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_RatingDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer54444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_RatingDetails objRatingDetails = new HEP_RatingDetails();
        objRatingDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
     /**
    * HEP_RatingDetails_WrapperTest -- Test method for the Rating Wrapper
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_RatingDetails_WrapperTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_RatingWrapper objRatingDetails = new HEP_RatingWrapper();
        Test.starttest ();
        objRatingDetails.TxnId=null;
        objRatingDetails.calloutStatus=null; 
        HEP_RatingWrapper.Attributes objAttributes = new HEP_RatingWrapper.Attributes();
        objAttributes.type = 'TITLE';
        objAttributes.foxId = '363029';
        objAttributes.foxVersionId = '363029';
        objAttributes.rowIdObject = '690061';
        objAttributes.rowIdTitle = '67728';
        objAttributes.rowIdTitleVersion = '363029';
        objAttributes.countryDescription = 'New Zealand';
        objAttributes.countryCode = 'NZ';
        objAttributes.mediaDescription = 'Home Entertainment';
        objAttributes.mediaCode = 'VID';
        objAttributes.ratingEntityDescription = 'OFLC - Office of Film and Literature Classification';
        objAttributes.ratingEntityCode = 'OFLC';
        objAttributes.ratingShortDescription = 'R16';
        objAttributes.ratingCode = 'OFLC-R16';
        objAttributes.ratingReason = null;
        objAttributes.titleVersionTypeCode = 'UNR';
        objAttributes.titleVersionTypeDescription = 'Unrated';
        objAttributes.titleVersionDescription = 'INTERNATIONAL/UNRATED/EXTENDED VERSION';
        objAttributes.ratingDescription = 'Restricted to persons 16 Years and over';
        list<HEP_RatingWrapper.Data> lstData = new list<HEP_RatingWrapper.Data>();
        HEP_RatingWrapper.Data objData = new HEP_RatingWrapper.Data();
        objData.attributes = objAttributes;
        objData.type = 'RATING';
        objRatingDetails.errors=null;
        HEP_RatingWrapper.Links objLinks = new HEP_RatingWrapper.Links();
        objLinks.related = '/foxipedia/global/api/title/details?foxId=67728';
        HEP_RatingWrapper.Title objTtle = new HEP_RatingWrapper.Title();
        objTtle.links = objLinks;
        HEP_RatingWrapper.Errors objErrors = new HEP_RatingWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_RatingWrapper.Relationships objRelationships = new HEP_RatingWrapper.Relationships();
        objRelationships.title = null;
        Test.stoptest();
    }
}