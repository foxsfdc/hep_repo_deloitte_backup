/**
* HEP_Promotion_Rights_Extractor -- Fetches real time right details from ERM 
* @author    Abhishek Mishra
*/
public without sharing class HEP_Promotion_Rights_Extractor implements HEP_IntegrationInterface{ 
    public HEP_Promotion_Rights_Extractor(){}
    /**
    * HEP_Promotion_Rights_Extractor -- Fetches real time right details from ERM 
    * @author  :  Abhishek Mishra
    * @return no return value
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);
        Map<String, String> mapSourceId = (Map<String,String>) JSON.deserialize(objTxnResponse.sSourceId, Map<String,String>.class);
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_ERM'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_ERM_Service');
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                httpRequest.setBody(createRequestBody(mapSourceId.get('sTitleId'), mapSourceId.get('sPromoId'), mapSourceId.get('scatId')));
                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                system.debug('objTxnResponse.sResponse : ' + objTxnResponse.sResponse);
                system.debug('httpResp.getStatus() : ' + httpResp.getStatus());
                system.debug('HEP_STATUS_OK :  ' + HEP_Utility.getConstantValue('HEP_STATUS_OK'));
                if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK')){
                //if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_SUCCESS')){
                    objWrapper = (ErrorResponseWrapper)JSON.deserialize(httpResp.getBody(),ErrorResponseWrapper.class);    
                    System.debug('objWrapper +++++++++++'+objWrapper );   
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                    if(objWrapper.status != 'SUCCESS'){
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        objTxnResponse.sErrorMsg = objWrapper.errorMessage;
                        objTxnResponse.bRetry = true;
                    }
                }
                else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
                
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }

    }
    
    string createRequestBody(Id titleId, String spromoId, String scatId){
        system.debug('I am here inside createRequestBody');
        system.debug('titleId : ' + titleId);
        system.debug('spromoId : ' + spromoId);
        system.debug('scatId : ' + scatId);
        set<id> setTerritoryIds = new set<id>();
        string sRequest;
        //For Catalog Master Page
        if(scatId != null && !string.isEmpty(scatId)){
            system.debug('Inside If Catalog : ' + scatId);
            //get Catalog Title and Catalog Territory
            HEP_Catalog__c objCatalog = [select id, Title_EDM__c, Territory__c, Territory__r.name from HEP_Catalog__c where id = :scatId limit 1];
            system.debug('objCatalog : ' + objCatalog);
            //Check if catalog is present or not
            if(objCatalog != null){
                //Check if title for the catalog is present or not
                if(objCatalog.Title_EDM__c != null){
                    if(objCatalog.Territory__r.name != HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME')){
                        /*
                        list<HEP_Territory__c> lstChildTerritories = [select id from HEP_Territory__c where CatalogTerritory__c = :objCatalog.Territory__c];
                        if(lstChildTerritories != null && !lstChildTerritories.isEmpty()){
                            for(HEP_Territory__c objTerritory : lstChildTerritories){  
                                setTerritoryIds.add(objTerritory.id);
                            }    
                        }else{
                                setTerritoryIds.add(objCatalog.Territory__c);
                        }   
                        */
                    }    
                }
            }   

            sRequest = createRequestBodyforRingts(titleId, setTerritoryIds); 
            return  sRequest;           
        }else{
            //Fetching all records for global and selected regions records for national promotion based on territory field in HEP territory rights object 
             HEP_Promotion__c objPromotionRecord = [select id, Promotion_Type__c from HEP_Promotion__c where  Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND  id =: sPromoId limit 1];
             //Checking for Promotion Type
             if (objPromotionRecord.Promotion_Type__c == HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL')) {

                    for (HEP_Promotion_Region__c objPromotionRegion : [SELECT Territory__c 
                                                             FROM   HEP_Promotion_Region__c 
                                                             WHERE  Parent_Promotion__c =: objPromotionRecord.id]) {
                        //setTerritoryIds.add(objPromotionRegion.Territory__c);
                    }
             }  
             system.debug('titleId : ' + titleId); 
             system.debug('setTerritoryIds : ' + setTerritoryIds);
             sRequest = createRequestBodyforRingts(titleId, setTerritoryIds); 
             return  sRequest;           
        }
    }
    
    string createRequestBodyforRingts(Id titleId, set<id> setTerritoryIds){
        //All Territory rights need to be processed 
        List<HEP_Territory_Rights__c> listAllTerrRights = new List<HEP_Territory_Rights__c>();
        //creating  StrandsOut list with all Territories
        List<StrandsOut> listStrands = new List<StrandsOut> ();
        String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        map<String, RowData> mapMediaRowData = new map<String, RowData>();
        string HEP_TITLE_CNFDL = HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL'); 
        
        Media objEST = new Media('');
        Media objVOD = new Media('');
        Media objPHY = new Media('');
        
        RightsWrapper objRights = new RightsWrapper();
        List<RightsWrapper> listRights = new List<RightsWrapper>();

        
        // Query to fetch Title as per the selected Title from Picklist on page
        List <EDM_GLOBAL_TITLE__c> listTitles = [SELECT id, FOX_ID__c
                                                 FROM EDM_GLOBAL_TITLE__c
                                                 WHERE id = : titleId and LIFE_CYCL_STAT_GRP_CD__c != :HEP_TITLE_CNFDL LIMIT 1];
       if(setTerritoryIds != null && !setTerritoryIds.isEmpty()){
                system.debug('I am here calculating : listAllTerrRights ' + setTerritoryIds);   
                listAllTerrRights = [SELECT id, Name, MltGroupId__c, TerritoryRegion__c, TerritoryAlias__c, ERMMedia__c, ERMLanguage__c, ERMTerritory__c, Media__c 
                                    FROM   HEP_Territory_Rights__c 
                                    WHERE  ERMTerritory__c != null 
                                    and Territory__c in: setTerritoryIds
                                    and record_status__c = :sACTIVE];
            } else {
                listAllTerrRights = [SELECT id, Name, MltGroupId__c, TerritoryRegion__c, TerritoryAlias__c, ERMMedia__c, ERMLanguage__c, ERMTerritory__c, Media__c 
                                    FROM   HEP_Territory_Rights__c
                                    WHERE  ERMTerritory__c != null
                                    and record_status__c = :sACTIVE];
        }
        system.debug('listAllTerrRights : ' + listAllTerrRights);   
        for (HEP_Territory_Rights__c objTerritotyRights: listAllTerrRights) {
            StrandsOut objStr = new StrandsOut(objTerritotyRights.MltGroupId__c, objTerritotyRights.ERMMedia__c, objTerritotyRights.ERMTerritory__c, objTerritotyRights.ERMLanguage__c, HEP_Utility.getConstantValue('HEP_ERM_Default_N'), HEP_Utility.getConstantValue('HEP_ERM_Default_N'), HEP_Utility.getConstantValue('HEP_ERM_Default_N'), String.valueOf(Date.today()), String.valueOf(Date.today().addYears(80)));
            listStrands.add(objStr);
            //Assigning the value in Wrapper Class RightsWrapper RowData 
            RowData objRowData = new RowData();
            objRowData.sTerritoryAlias = objTerritotyRights.TerritoryAlias__c;
            objRowData.sMltGroupId = objTerritotyRights.MltGroupId__c;
            if (objTerritotyRights.TerritoryRegion__c == 'Domestic') {
                objRowData.sGroup = 'DHE';
            } else {
                objRowData.sGroup = objTerritotyRights.TerritoryRegion__c;
            }
            objRowData.sMediaName = objTerritotyRights.Media__c;
            objRowData.EST = objEST;
            objRowData.PHY = objPHY;
            objRowData.VOD = objVOD;
            objRights.listRows.add(objRowData);
            listRights.add(objRights);    
        }
        
        //map of sMltGroupId and RowData to capture all updated Rowdata in ID and Value pair
        for (RightsWrapper each: listRights) {
            if (!(each.listRows).isEmpty()) {
                for (RowData eachRowData: each.listRows) {
                    mapMediaRowData.put(eachRowData.sMltGroupId, eachRowData);
                }
            }
        }
        
        // Request to be sent to ERM 
        Titles objTitle = new Titles(listTitles[0].FOX_ID__c, listStrands);
        List <Titles> titlList = new List <Titles>{objTitle};
        
        // Request to be sent to ERM 
        Contract objContract = new Contract(HEP_Utility.getConstantValue('HEP_ERM_Default_W'), titlList);
        // Request to be sent to ERM //listTitles[0].FOX_ID__c
        ReleaseRequestWrapper relWrap = new ReleaseRequestWrapper(String.valueOf(Math.abs(Crypto.getRandomInteger())), HEP_Utility.getConstantValue('HEP_ERM_Default_APPNAMEMASH'), objContract);
        String JSONRequest = JSON.serialize(relWrap);
        system.debug('JSONRequest : ' + JSONRequest);
        return JSONRequest;
    } 
    
    /**
    * Header --- Wrapper class for holding the strands out
    * @author    Abhishek Mishra
    */    
    public class StrandsOut {
        public String mltGroupId; //AutoNumberUnique
        public String mediaId;
        public String territoryId;
        public String languageId;
        public String mobileCellWirelessIndicator;
        public String internetOpenIndicator;
        public String internetClosedIndicator;
        public String startDate;
        public String endDate;
        public StrandsOut(String mltGroupId, String mediaId, String territoryId, String languageId, String mobileCellWirelessIndicator, String internetOpenIndicator, String internetClosedIndicator, String startDate, String endDate) {
            this.mltGroupId = mltGroupId;
            this.mediaId = mediaId;
            this.territoryId = territoryId;
            this.languageId = languageId;
            this.mobileCellWirelessIndicator = mobileCellWirelessIndicator;
            this.internetOpenIndicator = internetOpenIndicator;
            this.internetClosedIndicator = internetClosedIndicator;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
     /**
     * RowData Class to hold the row data to be displayed on AG Grid
     * @author    Abhishek Mishra
     */
    public class RowData {
        Public String sGroup;
        Public string sMediaName;
        Public String sTerritoryAlias;
        Public String sMltGroupId;
        public Media EST, VOD, PHY;
    }
        
    /**
     * Media      Class to hold Status based the Flag value from response and Status Message to be displayed when Status is warning.
     * @author    Abhishek Mishra
     */
    public class Media {
        Public String sStatus;
        list <String> lstStatusMessage;
        Public Media(String sStatus) {
            this.sStatus = sStatus;
        }
        Public Media() {}
    }
    
    /**
     * RightsWrapper  Wrapper class to be hold data for Rights Detail to be displayed on AG Grid On UI
     * @author    Abhishek Mishra
     */
    public class RightsWrapper {
        list<string> errorMessages;
        list <RowData> listRows;
        public RightsWrapper() {
            listRows = new list <RowData> ();
            errorMessages = new List<string>();
        }
    }
    
    /**
    * Header --- Wrapper class for holding the titles
    * @author    Abhishek Mishra
    */    
    public class Titles {
        public String foxId;
        public List<StrandsOut> strands;
        public Titles(String foxId, List <StrandsOut> strands) {
            this.foxId = foxId;
            this.strands = strands;
        }
    }
    
    /**
    * Header --- Wrapper class for holding the contracts
    * @author    Abhishek Mishra
    */
    public class Contract {
        public String withinThroughoutFlag;
        public List <Titles> titles;
        public Contract(String withinThroughoutFlag, List <Titles> titles) {
            this.withinThroughoutFlag = withinThroughoutFlag;
            this.titles = titles;
        }
    }
    
    /**
    * Header --- Wrapper class for holding the Release Requests
    * @author    Abhishek Mishra
    */
    public class ReleaseRequestWrapper {
        public String requestId;
        public String consumingApplicationName;
        public Contract contract;
        public ReleaseRequestWrapper(String requestId, String consumingApplicationName, Contract contract) {
            this.requestId = requestId;
            this.consumingApplicationName = consumingApplicationName;
            this.contract = contract;
        }
    }
    
    /**
    * Header --- Wrapper class for response parse
    * @author    Abhishek Mishra
    */
    public class ErrorResponseWrapper {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage) {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }
}