/**
* HEP_BoxAuth_Test -- Test class for the HEP_RatingDetails for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_BoxAuth_Test{
    /**
    * HEP_BoxAuth_Test --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxAuth_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        PageReference objVfPage = Page.HEP_BoxConnect;
        Test.setCurrentPage(objVfPage);
        ApexPages.currentPage().getParameters().put('code','53465');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxAuth objBox = new HEP_BoxAuth();
        HEP_BoxAuth.tokenWrapper objTokenWrapper = new HEP_BoxAuth.tokenWrapper();
        objTokenWrapper.access_token = '4KmoTBFOG1i5vAawzDaikfAi0nrI';
        objTokenWrapper.refresh_token = '43199';
        objTokenWrapper.expires_in = '43199';
        System.assertEquals(objTokenWrapper.access_token,'4KmoTBFOG1i5vAawzDaikfAi0nrI');
        Test.stoptest();
    }
     /**
    * HEP_BoxAuth_parseAuthJSONTest --  Test method for HEP_BoxAuth_parseAuthJSON Method
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_BoxAuth_parseAuthJSONTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        String sJson = '{"access_token": "4KmoTBFOG1i5vAawzDaikfAi0nrI","token_type": "Bearer","refresh_token":"43199","expires_in": "43199"}';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxAuth.parseAuthJSON(sJson);
        System.assertEquals(sJson,'{"access_token": "4KmoTBFOG1i5vAawzDaikfAi0nrI","token_type": "Bearer","refresh_token":"43199","expires_in": "43199"}');
        Test.stoptest();
    }
}