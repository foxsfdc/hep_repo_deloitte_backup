/**
 *HEP_Notification_Utility --- Class which houses the utility method to create application alerts
 *@author  Gaurav Mehrishi
 */
public with sharing class HEP_Notification_Utility {
    //Class Variable Declaration
    public static list < HEP_Notification__c > lstNotificationsToInsert;
    public static List < HEP_User_Favourites__c > lstUserFavourites;
    public static List < HEP_Notification_User_Action__c > lstNotificationsActionToInsert;
    public static String sNotifName;
    public static String sNotifType;
    public static String sNotifBody;

    /**
     *Class Constructor to initialise class variables
     *@return nothing
     */

    public HEP_Notification_Utility() {
        lstNotificationsToInsert = new List < HEP_Notification__c > ();
        lstNotificationsActionToInsert = new List < HEP_Notification_User_Action__c > ();
        lstUserFavourites = new List < HEP_User_Favourites__c > ();
        sNotifName = '';
        sNotifType = '';
        sNotifBody = '';
    }

    /**
     *getTemplate -- To get the Notification template.
     *@param sTempName name of the template stored in the backend.
     *@param sObjectType API name of the object to which template is tagged.
     *@return notification template object
     */

    public static HEP_Notification_Template__c getTemplate(String sTempName, String sObjectType) {
        //Fetch The Notification Template to Create Record Notifications
        System.debug('Temp Name--->'+sTempName +' and Object--->'+ sObjectType);
        HEP_Notification_Template__c objTemplate = [SELECT id, Name, HEP_Body__c, HEP_Object__c
            FROM HEP_Notification_Template__c
            WHERE Name =: sTempName AND HEP_Object__c =: sObjectType AND Record_Status__c =: HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').value__c
        ];
        if (objTemplate != null) {
            return objTemplate;
        } else {
            return null;
        }

    }
    /**
     *createNotifications -- To create Notification for dating record.
     *@param lstHepDating list of dating records.
     *@param sNotifName name of the template stored in the object.
     *@param sNotifType API name of the object to which template is tagged.
     *return nothing
     */

    public static void createNotifications(List < HEP_Promotion_Dating__c > lstHepDating, String sNotifName, String sNotifType) {
        try {
            sNotifBody = '';
            lstNotificationsToInsert = new List < HEP_Notification__c > ();
            lstNotificationsActionToInsert = new List < HEP_Notification_User_Action__c > ();
            lstUserFavourites = new List < HEP_User_Favourites__c > ();
            //fetch template
            HEP_Notification_Template__c objHepNotifTemplate = getTemplate(sNotifName, sNotifType);
            //setIds:set to store dating record id's
            set < id > setIds = new set < id > ();
            set < id > setPromotionIds = new set < id > ();
            //Populate the Values in Set with the Dating record id
            setIds = new Map<Id, HEP_Promotion_Dating__c>(lstHepDating).keySet();
            //Map to get lookup values for the record.
            Map < Id, HEP_Promotion_Dating__c > mapDatingLookup = new Map < Id, HEP_Promotion_Dating__c > ([SELECT Id, Promotion__r.Global_Promotion__r.PromotionName__c, Promotion__r.FirstAvailableDate__c, Territory__r.Name, Territory__r.Region__c, Promotion__r.PromotionName__c, Promotion__r.LocalPromotionCode__c, HEP_Catalog__r.Name, Language__r.Name, Date_Type__c, Date__c
                FROM HEP_Promotion_Dating__c
                WHERE   Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND ID IN: setIds
            ]);
            //Iterate the dating list and build the Dynamic Message Body for Notification as per the message body in the stored templates.
            for (HEP_Promotion_Dating__c objHPD: lstHepDating) {
                if (mapDatingLookup.containsKey(objHPD.id)) {
                    sNotifBody = objHepNotifTemplate.HEP_Body__c;
                    setPromotionIds.add(objHPD.Promotion__c);
                    system.debug('--------------------><><><>' + setPromotionIds);
                    if (!String.isBlank(sNotifBody) && sNotifBody.contains('<Promotion__r.PromotionName__c> : <Promotion__r.LocalPromotionCode__c>')) {
                        //sNotifBody = sNotifBody.replace('<Promotion__r.LocalPromotionCode__c>', mapDatingLookup.get(objHPD.id).Promotion__r.LocalPromotionCode__c);
                        //sNotifBody = sNotifBody.replace('<Promotion__r.PromotionName__c>', mapDatingLookup.get(objHPD.id).Promotion__r.PromotionName__c);
                        //sNotifBody = sNotifBody.replace('<Date_Type__c>', objHPD.Date_Type__c);
                        //sNotifBody = sNotifBody.replace('<HEP_Catalog__r.Name>', mapDatingLookup.get(objHPD.id).HEP_Catalog__r.Name);
                        //sNotifBody = sNotifBody.replace('<Territory__r.Region__c>', mapDatingLookup.get(objHPD.id).Territory__r.Region__c);
                        //sNotifBody = sNotifBody.replace('<Language__r.Name>', mapDatingLookup.get(objHPD.id).Language__r.Name);
                    } else if (!String.isBlank(sNotifBody) && sNotifBody.contains('<Date_Type__c>')) {
                        sNotifBody = sNotifBody.replace('<Date_Type__c>', mapDatingLookup.get(objHPD.id).Date_Type__c);
                        //sNotifBody = sNotifBody.replace('<Promotion__r.PromotionName__c>', mapDatingLookup.get(objHPD.id).Promotion__r.PromotionName__c);
                    } else if (!String.isBlank(sNotifBody) && sNotifBody.contains('<Promotion__r.PromotionName__c> <Territory__r.Name>')) {
                        /*sNotifBody = sNotifBody.replace('<Territory__r.Name>', mapDatingLookup.get(objHPD.id).Territory__r.Name);
                        sNotifBody = sNotifBody.replace('<Promotion__r.PromotionName__c>', mapDatingLookup.get(objHPD.id).Promotion__r.PromotionName__c);
                        sNotifBody = sNotifBody.replace('<Date_Type__c>', mapDatingLookup.get(objHPD.id).Date_Type__c);
                        if (mapDatingLookup.get(objHPD.id).Date__c != NULL) {
                            sNotifBody = sNotifBody.replace('<Date__c>', string.valueof(mapDatingLookup.get(objHPD.id).Date__c));
                        }
                        sNotifBody = sNotifBody.replace('<Promotion__r.Global_Promotion__r.PromotionName__c>', mapDatingLookup.get(objHPD.id).Promotion__r.Global_Promotion__r.PromotionName__c);
                        if (mapDatingLookup.get(objHPD.id).Promotion__r.FirstAvailableDate__c != NULL) {
                            sNotifBody = sNotifBody.replace('<Promotion__r.FirstAvailableDate__c>', string.valueof(mapDatingLookup.get(objHPD.id).Promotion__r.FirstAvailableDate__c));
                        }
                        system.debug('------------------->>>>>>>' + sNotifBody);*/
                    }
                    // Create Notification Records              
                    HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
                    objHepNewNotif.HEP_Message__c = sNotifBody;
                    objHepNewNotif.HEP_Template_Name__c = sNotifName;
                    objHepNewNotif.HEP_Object_API__c = sNotifType;
                    objHepNewNotif.Notification_Template__c = objHepNotifTemplate.id;
                    objHepNewNotif.HEP_Record_ID__c = objHPD.id;
                    lstNotificationsToInsert.add(objHepNewNotif);
                }
            }
            if (lstNotificationsToInsert != NULL && lstNotificationsToInsert.size() > 0) {
                Database.SaveResult[] lstSaveResult = Database.insert(lstNotificationsToInsert, false);
                system.debug('lstNotificationsToInsert ' + lstNotificationsToInsert);
                lstUserFavourites = [SELECT user__c
                    FROM HEP_User_Favourites__c
                    WHERE record_id__c IN: setPromotionIds
                ];
                system.debug('-----' + lstUserFavourites);
                if (lstUserFavourites != NULL && lstUserFavourites.size() > 0) {
                    for (Database.SaveResult objSaveResult: lstSaveResult) {
                        for (HEP_User_Favourites__c objUserFav: lstUserFavourites) {
                            if (objSaveResult.isSuccess() && (sNotifName == 'Promotion_Dating_Confirmed' || sNotifName == 'Promotion_Dating_ReConfirmed')) {
                                HEP_Notification_User_Action__c objHepNua = new HEP_Notification_User_Action__c();
                                objHepNua.HEP_Notification__c = objSaveResult.getId();
                                objHepNua.Notification_User__c = objUserFav.user__c;
                                lstNotificationsActionToInsert.add(objHepNua);
                            }
                        }
                    }
                }
                if (lstNotificationsActionToInsert != NULL && lstNotificationsActionToInsert.size() > 0) {
                    insert lstNotificationsActionToInsert;
                    system.debug('lstNotificationsActionToInsert ' + lstNotificationsActionToInsert);
                }
            }
        } catch (Exception e) {
            system.debug('Exception due to here 141 ======>' + e.getMessage() + e.getLineNumber());
        }
    }

    /**
     *createGenericNotifications -- To create generic Notifications for any object.
     *@param lstNotifyingRecords list of SObject records for which notification records need to be created.
     *@param sNotifName name of the template stored in the object.
     *return nothing
     */

    /*public static void createGenericNotifications(List < Id > lstNotifyingRecords, String sNotifName) {
        List < HEP_Notification__c > lstNotificationsToInsert = new List < HEP_Notification__c > ();
        try {
            if (lstNotifyingRecords != null && !lstNotifyingRecords.isEmpty()) {
                Id notifyingrecordId = lstNotifyingRecords.get(0);
                String sObjApiName = notifyingrecordId.getSObjectType().getDescribe().getName();
                HEP_Notification_Template__c objHepNotifTemplate = getTemplate(sNotifName, sObjApiName);
                String sNotifBody = objHepNotifTemplate.HEP_Body__c;
                String sFieldApiCommaSeparated = '';
                if (!String.isBlank(sNotifBody)) {
                    List < String > lstFieldApi = fetchFieldApisFromTemplate(sNotifBody);
                    if (lstFieldApi != null && !lstFieldApi.isEmpty()) {
                        for (String sField: lstFieldApi) {
                            sFieldApiCommaSeparated += ',' + sField;
                        }
                        String sBuildDynamicQuery = 'Select id' + sFieldApiCommaSeparated + ' from ' + sObjApiName;
                        List < Sobject > lstnotificationSobject = Database.Query(sBuildDynamicQuery);
                        if (lstnotificationSobject != null && !lstnotificationSobject.isEmpty()) {
                            for (Sobject objHPD: lstnotificationSobject) {
                                // Create Notification Records              
                                HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
                                objHepNewNotif.HEP_Message__c = CreateNotificationBody(objHPD, sNotifBody);
                                objHepNewNotif.HEP_Template_Name__c = sNotifName;
                                objHepNewNotif.HEP_Object_API__c = objHepNotifTemplate.HEP_Object__c;
                                //objHepNewNotif.HEP_Assignee_ID__c = (String)objHPD.get('LastModifiedById');
                                objHepNewNotif.HEP_Record_ID__c = objHPD.id;
                                lstNotificationsToInsert.add(objHepNewNotif);
                            }
                            if (lstNotificationsToInsert != NULL && lstNotificationsToInsert.size() > 0) {
                                insert lstNotificationsToInsert;
                                system.debug('lstNotificationsToInsert ' + lstNotificationsToInsert);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            system.debug('Exception due to ======>' + e.getMessage() + e.getLineNumber());
        }
    }*/
    /**
     *createNotificationAlerts -- To create generic Notifications for any object.
     *@param lstSobject list of SObject records for which notification records need to be created.
     *@param sNotifName name of the template stored in the object.
     *@param lstApprovers list of userID's to which notification alert needs to be sent.
     *return nothing
     */

    public static void createNotificationAlerts(List < Sobject > lstSobject, String sNotifName, List < Id > lstApprovers) {
        List < HEP_Notification__c > lstNotificationsToInsert = new List < HEP_Notification__c > ();
        try {
            sNotifType = '';
            sNotifBody = '';
            lstNotificationsActionToInsert = new List < HEP_Notification_User_Action__c > ();
            ID nType = lstSobject[0].id;
            String sNotifType = nType.getSObjectType().getDescribe().getName();
            system.debug('>>>>>>>>>' + sNotifType);
            HEP_Notification_Template__c objHepNotifTemplate = getTemplate(sNotifName, sNotifType);
            String sNotifBody = objHepNotifTemplate.HEP_Body__c;
            system.debug('?????????????' + sNotifBody);
            for (Sobject objHPD: lstSobject) {
                // Create Notification Records              
                HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
                objHepNewNotif.HEP_Message__c = CreateNotificationBody(objHPD, sNotifBody);
                objHepNewNotif.HEP_Template_Name__c = sNotifName;
                objHepNewNotif.HEP_Object_API__c = objHepNotifTemplate.HEP_Object__c;
                objHepNewNotif.Notification_Template__c = objHepNotifTemplate.id;
                objHepNewNotif.HEP_Record_ID__c = objHPD.id;
                lstNotificationsToInsert.add(objHepNewNotif);
            }
            if (lstNotificationsToInsert != NULL && lstNotificationsToInsert.size() > 0) {
                Database.SaveResult[] lstSaveResult = Database.insert(lstNotificationsToInsert, false);
                system.debug('lstNotificationsToInsert ' + lstNotificationsToInsert);
                if (lstApprovers != NULL && lstApprovers.size() > 0) {
                    for (Database.SaveResult objSaveResult: lstSaveResult) {
                        for (Id objUser: lstApprovers) {
                            if (objSaveResult.isSuccess()) {
                                HEP_Notification_User_Action__c objHepNua = new HEP_Notification_User_Action__c();
                                objHepNua.HEP_Notification__c = objSaveResult.getId();
                                objHepNua.Notification_User__c = objUser;
                                lstNotificationsActionToInsert.add(objHepNua);
                            }
                        }
                    }
                }
                if (lstNotificationsActionToInsert != NULL && lstNotificationsActionToInsert.size() > 0) {
                    insert lstNotificationsActionToInsert;
                    system.debug('lstNotificationsActionToInsert ' + lstNotificationsActionToInsert);
                }
            }
        } catch (Exception e) {
            system.debug('Exception due to ======>' + e.getMessage() + e.getLineNumber());
        }
    }
    /**
     *createNotificationBody -- To create notification body for generic Notification method.
     *@param objRecordDetails list of SObject records for which notification records need to be created.
     *@param sNotificationBody body of notification template records which consists field API names.
     *return notification body string
     */

    public static String createNotificationBody(Sobject objRecordDetails, String sNotificationBody) {
        String sBodyString = '';
        try {
            if (!String.isBlank(sNotificationBody)) {
                //Split the string based on the Separator Format in the Message Body
                String[] splitString = sNotificationBody.split('<');
                System.debug(splitString);
                if (splitString.size() > 0) {
                    //Iterate the String and build the Dynamic Message Body for Notification
                    for (String sStr: splitString) {
                        if (String.isNotBlank(sStr)) {
                            //For Merge Fields in message the format is <Field Api>
                            if (sStr.contains('>')) {
                                String sFieldApi = sStr.substringBefore('>');
                                String sFieldValue = '';
                                //For lookup Field Api's
                                if (sFieldApi.contains('.')) {
                                    String sParent = sFieldApi.substringBefore('.');
                                    String sChild = sFieldApi.substringAfter('.');
                                    //To extract relationship fields
                                    sFieldValue = (String) objRecordDetails.getSObject(sParent).get(sChild);
                                } else {
                                    //For Simple field Api's
                                    sFieldValue = (String) objRecordDetails.get(sFieldApi);
                                }
                                sBodyString += sFieldValue + '';
                                sBodyString += sStr.substringAfter('>') + '';
                            } else {
                                //If the String doesn't contains field Api's to merge
                                System.debug(sStr);
                                sBodyString += sStr + '';
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
        return sBodyString;
    }
    /**
     *fetchFieldApisFromTemplate -- To fetch fieldApilist from Sent notification template.
     *@param sNotificationBody body of notification template records which consists field API names.
     *return list of field API's.
     */

    public static List < String > fetchFieldApisFromTemplate(String sNotificationBody) {
        List < String > lstfieldApi = new List < String > ();
        //Split the string based on the Separator Format in the Message Body
        if (String.isNotBlank(sNotificationBody)) {
            String[] splitString = sNotificationBody.split('<');
            System.debug(splitString);
            if (splitString.size() > 0) {
                //Iterate the String and build the Dynamic Message Body for Notification
                for (String sStr: splitString) {
                    if (String.isNotBlank(sStr)) {
                        //For Merge Fields in message the format is <Field Api>
                        if (sStr.contains('>')) {
                            String sFieldApi = sStr.substringBefore('>');
                            lstfieldApi.add(sFieldApi);
                        }
                    }
                }
            }
        }
        return lstfieldApi;
    }

}