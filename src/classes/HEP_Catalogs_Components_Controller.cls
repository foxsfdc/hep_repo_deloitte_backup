/**
 * HEP_Catalogs_Components_Controller --- This class will make the callout to the JDE system to retrieve catalog components for Global catalogs and for local catalog we will be refering the HEP database.
 * @author Abhishek Mishra
 */
public without sharing class HEP_Catalogs_Components_Controller {

    public HEP_Catalogs_Components_Controller(){}


    //Declare Constants
    static string HEP_TITLE = HEP_Utility.getConstantValue('HEP_HEADER_PAGE_TITLE');
    static string HEP_CATALOG = HEP_Utility.getConstantValue('HEP_CATALOG');
    static string HEP_TITLE_CNFDL = HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL');
   /**
	* getCatalogTitle  : to get the list of all Title Associated with the Catalog.
	* @param           : 
	* @return          : CatalogRightsWrapper 
	*/
	@RemoteAction
	public static catalogComponentsWrapper getInitialDetails(id sCatId){
		//Get catalog details for default values 
		try{
			HEP_Foxipedia_CatalogWrapper objCatalogResponse = new HEP_Foxipedia_CatalogWrapper();
			HEP_InterfaceTxnResponse objIntTxnRsp = new HEP_InterfaceTxnResponse();
			catalogComponentsWrapper objcatalogComponentsWrapper = new catalogComponentsWrapper();
			string HEP_CATALOGTYPE_CPLNG = HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_COUPLING');
			string HEP_CATALOGTYPE_BXSET = HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BXSET');
			string HEP_CATALOGTYPE_BNDL = HEP_Utility.getConstantValue('HEP_CATALOGTYPE_BNDL');
			set<string> setCatalogIds = new set<string>();
			set<string> setTitleIds = new set<string>();
			map<string,string> mapCatalogIdvsName = new map<string,string>();
			map<string,string> mapTitleIdvsName = new map<string,string>();
	        HEP_Catalog__c objCatalog = [select id, Catalog_Name__c, CatalogId__c, Catalog_Type__c, Title_EDM__r.Name, Status__C, Foxipedia_Status__c
										 from HEP_Catalog__c
										 where id = :sCatId
										 limit 1];
			system.debug('objCatalog : ' + objCatalog);  							 
			if(objCatalog != null && objCatalog.CatalogId__c != null &&  !string.isBlank(objCatalog.CatalogId__c)){	
				objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(objCatalog.CatalogId__c, '', HEP_Utility.getConstantValue('HEP_CATALOG_COMPONENTS'));		 
		    }

		    if(objIntTxnRsp != null){
		    	if(objIntTxnRsp.sErrorMsg != null){
			    	errorWrapper objErrorWrapper = new errorWrapper('Error', objIntTxnRsp.sErrorMsg);
			    	objcatalogComponentsWrapper.objErrorWrapper = objErrorWrapper;
		    	}else{
			    	if(objIntTxnRsp.sResponse != null){
			    		objCatalogResponse = (HEP_Foxipedia_CatalogWrapper) JSON.deserialize(objIntTxnRsp.sResponse, HEP_Foxipedia_CatalogWrapper.class);
			    	}
			    }	
		    }	
		    system.debug('objCatalogResponse : ' + objCatalogResponse);
		    //system.debug('objCatalogResponse.data[0].attributes.productTypeCode : ' + objCatalogResponse.data[0].attributes.productTypeCode);

		    if(objCatalogResponse != null){
		    	if(objCatalogResponse.data != null && !objCatalogResponse.data.isEmpty()){
				    if(objCatalogResponse.data[0].attributes.productTypeCode == HEP_CATALOGTYPE_CPLNG){
				    	if(objCatalogResponse.data[0].attributes.productTitleList != null){
				    		for(HEP_Foxipedia_CatalogWrapper.productTitleWrapper objproductTitleWrapper : objCatalogResponse.data[0].attributes.productTitleList){
				    			setTitleIds.add(objproductTitleWrapper.financialProductId);
				    		}
				    		list<EDM_GLOBAL_TITLE__c> lstTitles = [Select Id, FIN_PROD_ID__c, name FROM EDM_GLOBAL_TITLE__c where FIN_PROD_ID__c in :setTitleIds and LIFE_CYCL_STAT_GRP_CD__c != :HEP_TITLE_CNFDL];	
				    		if(lstTitles != null && !lstTitles.isEmpty()){
				    			for(EDM_GLOBAL_TITLE__c objTitle : lstTitles){
				    				mapCatalogIdvsName.put(objTitle.FIN_PROD_ID__c, objTitle.Name);
				    			}
				    		}
				    		for(HEP_Foxipedia_CatalogWrapper.productTitleWrapper objproductTitleWrapper : objCatalogResponse.data[0].attributes.productTitleList){
				    			catalogComponentWrapper objcatalogComponentWrapper = new catalogComponentWrapper(objproductTitleWrapper.titleName,
				    																							 objproductTitleWrapper.financialProductId,
				    																							 objproductTitleWrapper.productTypeDescription,
				    																							 mapCatalogIdvsName.get(objproductTitleWrapper.financialProductId) == null ? '' : mapCatalogIdvsName.get(objproductTitleWrapper.financialProductId),
				    																							 objproductTitleWrapper.lifeCycleStatusDescription,
				    																							 HEP_TITLE);
				    			objcatalogComponentsWrapper.lstObjcatalogComponentsWrapper.add(objcatalogComponentWrapper);
				    		}
				    	}
				    } 
				    
				    if(objCatalogResponse.data[0].attributes.productTypeCode == HEP_CATALOGTYPE_BXSET){
						if(objCatalogResponse.data[0].attributes.productCatalogList != null){
				    		for(HEP_Foxipedia_CatalogWrapper.productCatalogWrapper objproductCatalogWrapper : objCatalogResponse.data[0].attributes.productCatalogList){
				    			setCatalogIds.add(objproductCatalogWrapper.productId);
				    		}	
				    		list<HEP_Catalog__c> lstCatalogs= [Select Id, Catalog_Name__c, CatalogId__c FROM HEP_Catalog__c where CatalogId__c in :setCatalogIds];
				    		if(lstCatalogs != null && !lstCatalogs.isEmpty()){
				    			for(HEP_Catalog__c objCatlg : lstCatalogs){
				    				mapCatalogIdvsName.put(objCatlg.CatalogId__c, objCatlg.Catalog_Name__c);
				    			}
				    		}

				    		for(HEP_Foxipedia_CatalogWrapper.productCatalogWrapper objproductCatalogWrapper : objCatalogResponse.data[0].attributes.productCatalogList){
				    			catalogComponentWrapper objcatalogComponentWrapper = new catalogComponentWrapper(objproductCatalogWrapper.productName,
				    																							 objproductCatalogWrapper.productId,
				    																							 objproductCatalogWrapper.productCatalogTypeDescription,
				    																							 mapCatalogIdvsName.get(objproductCatalogWrapper.productId) == null ? '' : mapCatalogIdvsName.get(objproductCatalogWrapper.productId),
				    																							 objproductCatalogWrapper.productStatusDescription,
				    																							 HEP_CATALOG);
				    			objcatalogComponentsWrapper.lstObjcatalogComponentsWrapper.add(objcatalogComponentWrapper);
				    		}
				    	}	
				    }
					
				    if(objCatalogResponse.data[0].attributes.productTypeCode == HEP_CATALOGTYPE_BNDL){
				    	if(objCatalogResponse.data[0].attributes.productTitleList != null){
				    		for(HEP_Foxipedia_CatalogWrapper.productTitleWrapper objproductTitleWrapper : objCatalogResponse.data[0].attributes.productTitleList){
				    			setTitleIds.add(objproductTitleWrapper.financialProductId);
				    		}
				    		list<EDM_GLOBAL_TITLE__c> lstTitles = [Select Id, FIN_PROD_ID__c, name FROM EDM_GLOBAL_TITLE__c where FIN_PROD_ID__c in :setTitleIds and LIFE_CYCL_STAT_GRP_CD__c != :HEP_TITLE_CNFDL];		
				    		if(lstTitles != null && !lstTitles.isEmpty()){
				    			for(EDM_GLOBAL_TITLE__c objTitle : lstTitles){
				    				mapCatalogIdvsName.put(objTitle.FIN_PROD_ID__c, objTitle.Name);
				    			}
				    		}
				    		for(HEP_Foxipedia_CatalogWrapper.productTitleWrapper objproductTitleWrapper : objCatalogResponse.data[0].attributes.productTitleList){
				    			catalogComponentWrapper objcatalogComponentWrapper = new catalogComponentWrapper(objproductTitleWrapper.titleName,
				    																							 objproductTitleWrapper.financialProductId,
				    																							 objproductTitleWrapper.productTypeDescription,
				    																							 mapCatalogIdvsName.get(objproductTitleWrapper.financialProductId) == null ? '' : mapCatalogIdvsName.get(objproductTitleWrapper.financialProductId),
				    																							 objproductTitleWrapper.lifeCycleStatusDescription,
				    																							 HEP_TITLE);
				    			objcatalogComponentsWrapper.lstObjcatalogComponentsWrapper.add(objcatalogComponentWrapper);
				    		}
				    	}	
				    }
				}    
			}    
		    system.debug('objcatalogComponentsWrapper inside Controller : ' + objcatalogComponentsWrapper);
		    return objcatalogComponentsWrapper;
		}catch(exception ex){
             //Code to log error..
            HEP_Error_Log.genericException('Error occured while fetching catalog components',
                                            'VF Controller',
                                            ex,
                                            'HEP_Catalogs_Components_Controller',
                                            'getInitialDetails',
                                            null,
                                            '');
            throw ex;
        }
	}	

   /**
	* getCatalogTitle  : to get the list of all Title Associated with the Catalog.
	* @param           : 
	* @return          : CatalogRightsWrapper 
	*/
	@RemoteAction
	public static string getTitleOrCatalogLink(string sProductId, string sObjectType){
		try{
			string sURL = '';
			string sQuery = '';
			system.debug('sProductId : ' + sProductId);
			system.debug('sObjectType : ' + sObjectType);
			if(sObjectType == HEP_TITLE){
				sQuery = 'Select Id, FIN_PROD_ID__c FROM EDM_GLOBAL_TITLE__c where FIN_PROD_ID__c = :sProductId and LIFE_CYCL_STAT_GRP_CD__c != :HEP_TITLE_CNFDL limit 1';
				LIST<EDM_GLOBAL_TITLE__c> lstTitles = Database.Query(sQuery);
				system.debug('lstTitles : ' + lstTitles);
				if(lstTitles != null && !lstTitles.isEmpty()){
					sURL = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/HEP_Title_Details?titleId='+lstTitles[0].id; 
					return sURL;
				}else{
					return null;
				}	
			}
			if(sObjectType == HEP_CATALOG){
				sQuery = 'Select Id, CatalogId__c FROM HEP_Catalog__c where CatalogId__c = :sProductId limit 1';
				LIST<HEP_Catalog__c> lstCatalogs = Database.Query(sQuery);
				if(lstCatalogs != null && !lstCatalogs.isEmpty()){
					sURL = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/HEP_Catalog_Details?catId='+lstCatalogs[0].id; 
					return sURL;
				}else{
					return null;
				}
			}
			return null;
		}catch(exception ex){
             //Code to log error..
            HEP_Error_Log.genericException('Error occured while opening Title or Catalog Detail page',
                                            'VF Controller',
                                            ex,
                                            'HEP_Catalogs_Components_Controller',
                                            'getTitleOrCatalogLink',
                                            null,
                                            '');
            throw ex;
        }

	}
	/**
    * catalogComponentWrappers --- Wrapper class list of components to be shown on the page
    * @author    Abhishek Mishra
    */
    public class catalogComponentsWrapper{
    	public list<catalogComponentWrapper> lstObjcatalogComponentsWrapper = new list<catalogComponentWrapper>();
    	public errorWrapper  objErrorWrapper = new errorWrapper();
    }

   /**
    * catalogComponentWrapper --- Wrapper class for catalog Component wrapper
    * @author    Abhishek Mishra
    */
    public class catalogComponentWrapper{
        public string sComponentName;
        public string sComponentID;
        public string sType;
        public string sLocalTitleName;
        public string sStatus;
        public string sComponentTypeRef;
        public catalogComponentWrapper(){
            this.sComponentName = '';
            this.sComponentID = '';
            this.sType = '';
            this.sLocalTitleName = '';
            this.sStatus = '';
            this.sComponentTypeRef = '';
        }
        public catalogComponentWrapper(string sComponentName, string sComponentID, string sType, string sLocalTitleName, string sStatus, string sComponentTypeRef){
            this.sComponentName = sComponentName;
            this.sComponentID = sComponentID;
            this.sType = sType;
            this.sLocalTitleName = sLocalTitleName;
            this.sStatus = sStatus;
            this.sComponentTypeRef = sComponentTypeRef;
        } 
    }  

    /**
    * DateType --- Wrapper class for holding Initial Loading response
    * @author    Abhishek Mishra
    */
    public class errorWrapper{
        public string sStatus;
        public string sErrorMessage;

        public errorWrapper(){
        	this.sStatus = '';
            this.sErrorMessage = '';
        }
        public errorWrapper(string sStatus, string sErrorMessage){
            this.sStatus = sStatus;
            this.sErrorMessage = sErrorMessage;
        }
    }  
}