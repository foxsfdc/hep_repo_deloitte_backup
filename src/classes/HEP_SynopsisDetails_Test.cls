/**
* TEST_HEP_SynopsisDetails -- Test class for the HEP_SynopsisDetails for Foxipedia Interface.
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_SynopsisDetails_Test{
    /**
    * HEP_SynopsisDetails_SuccessTest --  Test method for successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_SynopsisDetails_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Synopsis','HEP_SynopsisDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('12355','','HEP_Foxipedia_Synopsis');
        System.debug('objTxnResponse'+objTxnResponse);
        System.assertEquals('HEP_Foxipedia_Synopsis',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_SynopsisDetails_FailureTest -- Test method for the failure response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_SynopsisDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Synopsis','HEP_SynopsisDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('aer567854444','','HEP_Foxipedia_Synopsis');
        System.assertEquals('HEP_Foxipedia_Synopsis',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_SynopsisDetails_InvalidAcessTokenTest -- Test method for invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_SynopsisDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer54444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_SynopsisDetails objSynopsisDetails = new HEP_SynopsisDetails();
        objSynopsisDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    /**
    * HEP_SynopsisDetails_WrapperTest -- Test method for Synopsis wrapper
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_SynopsisDetails_WrapperTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_SynopsisWrapper objSynopsisDetails = new HEP_SynopsisWrapper();
        Test.starttest ();
        objSynopsisDetails.TxnId=null;
        objSynopsisDetails.calloutStatus=null; 
        HEP_SynopsisWrapper.Attributes objAttributes = new HEP_SynopsisWrapper.Attributes();
        objAttributes.type = 'TITLE';
        objAttributes.foxId = '12355';
        objAttributes.foxVersionId = '12355';
        objAttributes.synopsisId = '46500';
        objAttributes.rowIdObject = '46500';
        objAttributes.languageCode = 'ENG';
        objAttributes.languageDescription = 'English';
        objAttributes.countryDescription = 'USA';
        objAttributes.countryCode = 'US';
        objAttributes.mediaDescription = 'All Media';
        objAttributes.mediaCode = 'ALL';
        objAttributes.synopsisTypeCode = 'SHRT';
        objAttributes.synopsisTypeDescription = 'Long';
        objAttributes.synopsisText = 'A landlord tenant dispute.';
        objAttributes.titleVersionTypeCode = 'DFLT';
        objAttributes.titleVersionTypeDescription = 'Default / Non-Version-Specific';
        objAttributes.titleVersionDescription = 'NOT VERSION SPECIFIC';
        list<HEP_SynopsisWrapper.Data> lstData = new list<HEP_SynopsisWrapper.Data>();
        HEP_SynopsisWrapper.Data objData = new HEP_SynopsisWrapper.Data();
        objData.attributes = objAttributes;
        objData.type = 'SYNOPSIS';
        objSynopsisDetails.errors=null;
        HEP_SynopsisWrapper.Links objLinks = new HEP_SynopsisWrapper.Links();
        objLinks.related = '/foxipedia/global/api/title/details?foxId\u003d12355';
        HEP_SynopsisWrapper.Title objTtle = new HEP_SynopsisWrapper.Title();
        objTtle.links = objLinks;
        HEP_SynopsisWrapper.Errors objErrors = new HEP_SynopsisWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_SynopsisWrapper.Relationships objRelationships = new HEP_SynopsisWrapper.Relationships();
        objRelationships.title = null;
        HEP_SynopsisWrapper.LinksOuter objLnkOuter = new HEP_SynopsisWrapper.LinksOuter();
        objLnkOuter.self = '/foxipedia/global/api/title/synopsis?foxId\u003d12355';
        Test.stoptest();
    }
}