/**
* HEP_SKU_Price_TriggerHandler_Test -- Test class for HEP_SKU_Price_TriggerHandler
* 
* @author    Himanshi Mayal
**/ 

@istest 
public class HEP_SKU_Price_TriggerHandler_Test {

  @testSetup
        static void createUsers(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Mayal', 'hMayal@deloitte.com','Himanshi','Himanshi','maMaya','', true);
            //List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
        }

  @istest
  public static void TEST_HEP_SKU_Price_TriggerHandler_insert()
  {
   
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
          objInterface.Name = 'HEP_E1_SpendDetail';
          Update objInterface;
        HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary', null, null,'EUR','DE','182', 'ESCO', false);
        objTerr.SKU_Interface__c = 'ESCO';
        insert objTerr;
        HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
        insert objCat;
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerr.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
        objSKUMaster.Revenue_Share__c = False;
        insert objSKUMaster;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerr.Id,null,'','',null);
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerr.Id, null,'','',null);
        insert objPromotion;

        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
        insert objPromotionCatalog;

        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', null, null);
        insert objPromotionSKU;
        
        HEP_Promotions_DatingMatrix__c objPromotionDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog', 'Promotion', 'VOD Release Date', objTerr.id, objTerr.id, null);
        insert objPromotionDatingMatrix;

        HEP_Promotion_Dating__c objPromotionDating = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerr.Id, objPromotion.Id, objPromotionDatingMatrix.id, null);
        insert objPromotionDating;

        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerr.id, '345', 768.00, null);
        insert objPriceGrade;

        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMaster.Id, objTerr.Id, objPromotionSKU.Id, objPromotionDating.Id, 'Active', null);
        objSKUPrice.Unit__c = 30;
        insert objSKUPrice;



        HEP_SKU_Price__c objQueryRecord = [select Id, Unit__c from HEP_SKU_Price__c where Id = :objSKUPrice.Id limit 1];
        test.startTest();
        system.assertEquals(objSKUPrice.Unit__c, objQueryRecord.Unit__c);
        test.stopTest();
    }

    @istest
  public static void TEST_HEP_SKU_Price_TriggerHandler_update()
  {     
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
        objInterface.Name = 'HEP_E1_SpendDetail';
        Update objInterface;
        HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary', null, null,'EUR','DE','182', 'ESCO', false);
        objTerr.SKU_Interface__c = 'ESCO';
        objTerr.E1_Code__c = '410';
        insert objTerr;
        HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
        insert objCat;
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerr.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
        objSKUMaster.Revenue_Share__c = False;
        insert objSKUMaster;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerr.Id,null,'','',null);
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerr.Id, null,'','',null);
        insert objPromotion;

        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
        insert objPromotionCatalog;

        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', null, null);
        insert objPromotionSKU;
        
        HEP_Promotions_DatingMatrix__c objPromotionDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog', 'Promotion', 'VOD Release Date', objTerr.id, objTerr.id, null);
        insert objPromotionDatingMatrix;

        HEP_Promotion_Dating__c objPromotionDating = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerr.Id, objPromotion.Id, objPromotionDatingMatrix.id, null);
        insert objPromotionDating;

        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerr.id, '345', 768.00, null);
        insert objPriceGrade;

        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMaster.Id, objTerr.Id, objPromotionSKU.Id, objPromotionDating.Id, 'Active', null);
        objSKUPrice.Unit__c = 30;
        insert objSKUPrice;
        
        
        HEP_CheckRecursive.clearSetIds();
        
        HEP_SKU_Price__c objQueryRecord = [select Id, Unit__c from HEP_SKU_Price__c where Id = :objSKUPrice.Id limit 1];
        objQueryRecord.Unit__c = 50;
        update objQueryRecord;
        HEP_SKU_Price__c objQueryRecordOnUpdate = [select Id, Unit__c from HEP_SKU_Price__c where Id = :objSKUPrice.Id limit 1];
        test.startTest();
        system.assertEquals(objQueryRecord.Unit__c, objQueryRecordOnUpdate.Unit__c);
        test.stopTest();
    }
}