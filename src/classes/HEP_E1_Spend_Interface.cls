/*
 * E1_Spend_Interface --- Class for to send HEP Market Spend Details for a particular promotion to E1
 * @author  Himanshi Mayal
 */
public class HEP_E1_Spend_Interface implements HEP_IntegrationInterface
{
    /** 
   * SpendHeader --- wrapper to hold the constant details as spend header
     * @author  Himanshi Mayal
     */ 
  public class SpendHeader
  {
    public String Batch;
    public String SequenceNumber;
    public String Processed_Flag;
    public String Interface_Code;
    public String E1_Program_Id;
    public String Program_Id;
    public String User_Id;
    public String Updated_Date;
    public String Updated_Time;
    public String Transaction_Date;
    public Integer Record_Count;
    public String Group_Id;
    public SpendHeader()
    {
      this.Batch = '';
      this.SequenceNumber = '';
      this.Processed_Flag = '';
      this.E1_Program_Id = '';
      this.Program_Id = '';
      this.User_Id = '';
      this.Group_Id = '';
      this.Updated_Date = '';
      this.Updated_Time = '';
      this.Transaction_Date = '';
      this.Interface_Code = '';
      this.Record_Count = 0;                  
    }
    public SpendHeader(String sInterfaceCode, Integer recordCount, String sBatch, String sSequenceNumber)
    {
      this.Batch = sBatch;
      this.SequenceNumber = sSequenceNumber;
      this.Processed_Flag = HEP_Utility.getConstantValue('HEP_INVOICE_STATUS_CODE_A');
      this.E1_Program_Id = HEP_Utility.getConstantValue('HEP_E1_PROGRAM_ID');
      this.Program_Id = HEP_Utility.getConstantValue('HEP_SPEND');
      this.User_Id = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
      this.Group_Id = HEP_Utility.getConstantValue('HEP_E1_GROUP_ID');
      this.Updated_Date = getJulianDate(System.today());
      this.Updated_Time =  timeInHHMMSS((System.now()).time());
      this.Transaction_Date = getJulianDate(System.today());
      this.Interface_Code = sInterfaceCode;
      this.Record_Count = recordCount;            
    }
    List<SpendDetails> SpendDetails;        
  }
    /**
     * SpendDetails --- record level wrapper to hold the record details
     * @author  Himanshi Mayal
     */ 
  public class SpendDetails
  {
    public Integer Line_Number;
    public String Forecast_Type;
    public String E1_Company;
    public String Ledger_Type;
    public String Map_Id;
    public String Release_Id;
    public String E1_Territory;
    public String GL_Number;
    public String Subsidiary;
    public Integer GL_Amount;
    public Integer GL_Units;
    public String Release_Date;
    public String Catalog;
    public String WPR;
    public SpendDetails()
    {
      this.Line_Number = 0;
      this.Forecast_Type = '';
      this.E1_Company = '';
      this.Ledger_Type = '';
      this.Map_Id = '';
      this.Release_Id = '';
      this.E1_Territory = '';
      this.GL_Number = '';
      this.Subsidiary = '';
      this.GL_Amount = 0;
      this.GL_Units = 0;
      this.Release_Date = '';
      this.Catalog = '';
      this.WPR = '';
    }
  }
  public class Payload
  {
    public SpendHeader SpendHeader;
    
  }
  public class Data
  {
    public Payload Payload;
        public Data(Payload Payload){
          this.Payload = Payload;
      }
  }
    /**
     * E1Wrapper --- main wrapper which holds the entire request
     * @author  Himanshi Mayal
     */ 
  public class E1Wrapper
  {
    public String ProducerID;
    public String EventType;
    public String EventName;
    public Data Data;
    public E1Wrapper(Data Data)
    {
      this.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID'); 
      this.EventType = HEP_Utility.getConstantValue('HEP_E1_EVENT_TYPE');
      this.EventName = HEP_Utility.getConstantValue('HEP_JDE_EVENTNAME');
      this.Data = Data;  
      }    
  }
    public class ErrorResponseWrapper {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage){
            this.status = status;
            this.errorMessage = errorMessage;
          }
    }
    /**
     *Calculates the Julian Date for the given date
   *@param Date 
   *@return Julian Date as String
   *@author Himanshi Mayal
     */ 
    public static String getJulianDate(Date sysDate)
    {
        String sYear = String.valueOf(sysDate.year());
        String sCentury = sYear.substring(0, 2);
        String sYYForJulian = sYear.substring(2, 4);
        Integer cForJulian = Integer.valueOf(sCentury) - 19;
        String sTempDDDForJulian = String.valueOf(sysDate.dayOfYear()); 
        String sPadding = '000';
        String sDDDForJulian = sTempDDDForJulian.leftPad(3,sPadding);
        String sjJulianDate = String.valueOf(cForJulian) + sYYForJulian + sDDDForJulian;
        return sjJulianDate;
    } 
    /**
     *Calculates the time in HHMMSS Format for the given time
   *@param Time 
   *@return TIME IN HHMMSS as String
   *@author Himanshi Mayal
     */ 
    public static String timeInHHMMSS(Time sysTime)
    {
        String hour = ((String.valueOf((sysTime.hour()))).length() == 2) ? (String.valueOf((sysTime.hour()))) : 0+(String.valueOf((sysTime.hour())));
        String minute = ((String.valueOf((sysTime.minute()))).length() == 2) ? (String.valueOf((sysTime.minute()))) : 0+(String.valueOf((sysTime.minute())));
        String second = ((String.valueOf((sysTime.second()))).length() == 2) ? (String.valueOf((sysTime.second()))) : 0+(String.valueOf((sysTime.second())));
        String timeInHHMMSS = hour + minute + second;
        return timeInHHMMSS;  
    }
     /**
     * Perform the API call.
     * @param empty response
     * @return 
     * @author  Himanshi Mayal
     */ 
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        list<String> lstRecordIds = new list<string>();
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_E1_OAuth'); 

        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_E1_Service');

            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                //List<HEP_E1_Queueable.E1Wrapper> lstwrapperForSource;
                if((objTxnResponse.sSourceId != Null) && (String.isNotBlank(objTxnResponse.sSourceId)))
                {
                  //lstwrapperForSource = (List<HEP_E1_Queueable.E1Wrapper>)Json.deserialize(objTxnResponse.sSourceId,HEP_E1_Queueable.E1Wrapper.class);
                  lstRecordIds = objTxnResponse.sSourceId.split(',');
                }
                String sRequestBody = getSpendInformation(lstRecordIds,objTxnResponse);
                system.debug('Request ---- ' + sRequestBody);

                //httpRequest.setBody(sRequestBody);

                if((sRequestBody != Null) && ((sRequestBody) != ''))
                {
                  httpRequest.setBody(sRequestBody);
                  HTTP http = new HTTP();
                  system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                  system.debug('sAccessToken : ' + sAccessToken);
                  system.debug('httpRequest : ' + httpRequest);
                  HTTPResponse httpResp = http.send(httpRequest);
                  system.debug('httpResp : ' + httpResp);
                  objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                  objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                  if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK') || httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED'))
                  {  
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                  }
                  else
                  {
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                  }
                }
                else
                {
                  HTTPResponse res = new HTTPResponse();
                  System.debug('------- EMPTY JSON RETURNED ------');
                  objTxnResponse.sRequest = ''; //request details
                  //Kunal - updated to mark Transaction successful when nothing is to be sent (scenario - Promo has Spend but no Revenue OR vice-versa)
                  objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                  objTxnResponse.bRetry = false;
                  objTxnResponse.sErrorMsg = 'No records to be sent';
                  /*res.setStatus(HEP_Utility.getConstantValue('HEP_FAILURE'));
                  res.setStatusCode(500);
                  objTxnResponse.sResponse = res.getBody(); //Response from callout
                  objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                  objTxnResponse.sErrorMsg = res.getStatus();
                  objTxnResponse.bRetry = true;*/

                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }
    /**
     * Get the serialized json of the request to be sent
     * @param list of record ids (Promotion ids)
     * @return
     * @author  Himanshi Mayal
     */
  public static String getSpendInformation(List<String> lstRecordIds,HEP_InterfaceTxnResponse objTxnResponse)
  {
    List<String> lstPromoSkuId = new List<String>();
    List<String> lstMarketSpendId = new List<String>();
    Integer lineNumber = 0;
    String sFinalJSON = '';
    String sInterfaceCode = '';
    String sE1Company = HEP_Utility.getConstantValue('HEP_E1_COMPANY');
    String sLedgerTypeMP = HEP_Utility.getConstantValue('HEP_E1LEDGER_TYPE_MP');
    String sLedgerTypeMU = HEP_Utility.getConstantValue('HEP_E1LEDGER_TYPE_MU');
    String sReleaseId = HEP_Utility.getConstantValue('HEP_E1_RELEASEID');
    String sRecordActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    String sMapIdM = HEP_Utility.getConstantValue('HEP_E1_MAP_ID');
    String sWPRW = HEP_Utility.getConstantValue('HEP_ERM_Default_W');
    String sCatalogC = HEP_Utility.getConstantValue('HEP_JDE_TERRITORY_CODE_C');
    String sPEST = HEP_Utility.getConstantValue('HEP_CHANNEL_PEST');
    String sSPEST = HEP_Utility.getConstantValue('HEP_CHANNEL_SPEST');
    String sPVOD = HEP_Utility.getConstantValue('HEP_CHANNEL_PVOD');
    String sSPVOD = HEP_Utility.getConstantValue('HEP_CHANNEL_SPVOD');
    String sPhysical = HEP_Utility.getConstantValue('HEP_PHYSICAL');
    String sDigital = HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_DIGITAL');
    String sBoxset = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BXSET');
    Integer recordCount = 0;
    String sBatch = '';
    String sEarliestTransactionDate = '';
    Boolean bSpend = false;
    Boolean bRevenue = false;
    String sMarketSpendDetailCurrent = HEP_Utility.getConstantValue('HEP_SPEND_DETAIL_STATUS_CURRENT');
    List<HEP_Market_Spend_Detail__c> lstMarketSpendDetail = new List<HEP_Market_Spend_Detail__c>();
    List<SpendDetails> lstSpendDetails = new List<SpendDetails>();
    Map<Id,HEP_Approvals__c> mapApproval = new  Map<Id,HEP_Approvals__c>();
    Map<id,List<HEP_SKU_Price__c>> mapSkuPrice = new Map<id,List<HEP_SKU_Price__c>>();
    List<HEP_Promotion_SKU__c> lstPromotionSku = new List<HEP_Promotion_SKU__c>();
    List<HEP_Interface_Transaction__c> lstInterfaceTransaction = new List<HEP_Interface_Transaction__c>();
    List<String> lstChannel =  new List<String>();
    lstChannel.add(sPEST);
    lstChannel.add(sSPEST);
    lstChannel.add(sPVOD);
    lstChannel.add(sSPVOD);
    String sSequenceNumber = '';

    if(lstRecordIds != Null)
    {
      lstMarketSpendDetail = new List<HEP_Market_Spend_Detail__c>();
      List<HEP_Promotion__c> lstPromo = [SELECT id, Name from HEP_Promotion__c WHERE id in :lstRecordIds
                                        AND Territory__r.E1_Code__c != NULL];
      lstPromotionSku = [SELECT id, Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c, Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                         Release_Id__c, Promotion_Catalog__r.Catalog__r.CatalogId__c, Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c, Promotion_Catalog__r.Promotion__c
                         FROM HEP_Promotion_SKU__c WHERE Promotion_Catalog__r.Promotion__c in :lstPromo 
                                                          AND SKU_Master__r.Channel__c not in :lstChannel
                                                          AND Record_Status__c = :sRecordActive
                                                          AND ((Digital_Physical__c =: sDigital AND Promotion_Catalog__r.Catalog__r.Catalog_Type__c !=: sBoxset) OR Digital_Physical__c =: sPhysical)];      
      lstMarketSpendDetail = [SELECT HEP_Market_Spend__r.RecordStatus__c, HEP_Market_Spend__r.Promotion__r.Territory__c, Budget__c,
                              HEP_Market_Spend__r.Promotion__c, HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Interface_Code__c,
                              HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Code__c, HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c,
                              GL_Account__r.E1_Subsidiary__c, GL_Account__r.GLAccount__c, Type__c FROM HEP_Market_Spend_Detail__c 
                              WHERE HEP_Market_Spend__r.Promotion__c in :lstRecordIds 
                              AND Type__c = :sMarketSpendDetailCurrent
                              AND HEP_Market_Spend__r.RecordStatus__c = 'Budget Approved'];
      lstInterfaceTransaction = [SELECT Name FROM HEP_Interface_Transaction__c WHERE ID =: objTxnResponse.sInterfaceTxnId]; 
      if(lstInterfaceTransaction.size()>0)
        sSequenceNumber = lstInterfaceTransaction[0].Name.Substring(3,lstInterfaceTransaction[0].Name.length());                  
      System.debug('sSequenceNumber transaction number'+sSequenceNumber);
      for(HEP_Promotion_SKU__c objPromoSku : lstPromotionSku)
      {
        lstPromoSkuId.add(objPromoSku.Id);
      }
    }
    if((lstPromotionSku != Null) && (!lstPromotionSku.isEmpty()))
    {
      recordCount = 0;
      lineNumber = 0;
      for(HEP_SKU_Price__c objSkuPrice : [SELECT Unit__c, PriceGrade__r.Dealer_List_Price__c, 
                                          Promotion_Dating__r.Date__c, Promotion_SKU__c 
                                          FROM HEP_SKU_Price__c
                                          WHERE Promotion_SKU__c in : lstPromoSkuId
                                          AND PriceGrade__r.Dealer_List_Price__c != 0
                                          AND PriceGrade__r.Dealer_List_Price__c != NULL
                                          AND Promotion_Dating__r.Date__c != NULL
                                          AND Promotion_Dating__c != NULL
                                          AND Unit__c != NULL
                                          AND Unit__c != 0
                                          AND Record_Status__c = :sRecordActive])
      {
        if(mapSkuPrice.containsKey(objSkuPrice.Promotion_SKU__c))
        {
          mapSkuPrice.get(objSkuPrice.Promotion_SKU__c).add(objSkuPrice);
        }
        else
          mapSkuPrice.put(objSkuPrice.Promotion_SKU__c, new List<HEP_SKU_Price__c>{objSkuPrice});
      }
      System.debug('MAP OF SKU PRICE RECORDS TO PROMOTION SKUS ----- '+mapSkuPrice);
        


      if((lstMarketSpendDetail != Null) && (!lstMarketSpendDetail.isEmpty()))
      {
        System.debug('ENTERING MARKET SPEND DETAIL RECORDS-------- ');
        mapApproval = new  Map<Id,HEP_Approvals__c>();

        for(HEP_Market_Spend_Detail__c objMarketSpendD : lstMarketSpendDetail)
        {
          lstMarketSpendId.add(objMarketSpendD.HEP_Market_Spend__c);
        }
        for(HEP_Approvals__c objApproval : [SELECT Id,Name,HEP_Market_Spend__c,LastModifiedDate FROM HEP_Approvals__c WHERE HEP_Market_Spend__c in : lstMarketSpendId])
        {
          if((objApproval != Null) && (objApproval.HEP_Market_Spend__c != Null))
          {
            if(mapApproval.containsKey(objApproval.HEP_Market_Spend__c) && objApproval.LastModifiedDate > mapApproval.get(objApproval.HEP_Market_Spend__c).LastModifiedDate)
              mapApproval.put(objApproval.HEP_Market_Spend__c,objApproval);
            else
            {
              if(mapApproval.containsKey(objApproval.HEP_Market_Spend__c))
                continue;    
              mapApproval.put(objApproval.HEP_Market_Spend__c,objApproval);
            }
          }
        }
        System.debug('Map of Approvals --------- '+mapApproval);
        for(HEP_Market_Spend_Detail__c objMarketSpendDetail : lstMarketSpendDetail)
        {
          SpendDetails objSpendDetails = new SpendDetails();  
          bSpend = true; 
          if(mapApproval.containsKey(objMarketSpendDetail.HEP_Market_Spend__c))
            sBatch = mapApproval.get(objMarketSpendDetail.HEP_Market_Spend__c).Name.substring(4,12);                      
          ++lineNumber;
          objSpendDetails.Line_Number = lineNumber;
          objSpendDetails.Forecast_Type = HEP_Utility.getConstantValue('HEP_SPEND_TYPE');
          if((objMarketSpendDetail.HEP_Market_Spend__r != Null) && (objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r != Null))
          {
            objSpendDetails.Map_Id = (objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c != Null) ? sMapIdM + objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c : '';
            if(objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r != Null)
            {
              objSpendDetails.E1_Company = (objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Code__c != Null) ? sE1Company + objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Code__c : '';
              objSpendDetails.Ledger_Type = sLedgerTypeMP;
              
              sInterfaceCode = (objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Interface_Code__c != Null) ? objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Interface_Code__c : '';
              objSpendDetails.E1_Territory = (objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Code__c != Null) ? objMarketSpendDetail.HEP_Market_Spend__r.Promotion__r.Territory__r.E1_Code__c : '';
            }
            objSpendDetails.Release_Id = '';
            if(objMarketSpendDetail.GL_Account__r != Null)
            {
              objSpendDetails.GL_Number = (objMarketSpendDetail.GL_Account__r.GLAccount__c != Null) ? objMarketSpendDetail.GL_Account__r.GLAccount__c : '';
              objSpendDetails.Subsidiary = (objMarketSpendDetail.GL_Account__r.E1_Subsidiary__c != Null) ? objMarketSpendDetail.GL_Account__r.E1_Subsidiary__c : '';   
            }

            //Only for Japan, don't multiply amount by 100
            if(objSpendDetails.E1_Territory == '420')
                objSpendDetails.GL_Amount = Integer.valueOf(objMarketSpendDetail.Budget__c);
            else
                objSpendDetails.GL_Amount = Integer.valueOf(objMarketSpendDetail.Budget__c*100);

            objSpendDetails.GL_Units = 0;
            objSpendDetails.Release_Date = '';
            objSpendDetails.Catalog = '';
            objSpendDetails.WPR = '';
            lstSpendDetails.add(objSpendDetails);
          }
          System.debug('LIST OF SPEND ROWS---- '+lstSpendDetails);
        } //end of market spend detail
      }
      System.debug('ENTERING PROMOTION SKU RECORDS-------- ');
        for(HEP_Promotion_SKU__c objPromoSku : lstPromotionSku)
        {
          //Integer tempRecordCount = 0;
          SpendDetails objSpendDetails = new SpendDetails();
          //bRevenue = true;
          //COMMON REVENUE RECORDS
          objSpendDetails.Forecast_Type = HEP_Utility.getConstantValue('HEP_REVENUE_TYPE');
          if(objPromoSku.Promotion_Catalog__r != Null)
          {
            if(objPromoSku.Promotion_Catalog__r.Promotion__r != Null)
            {
              objSpendDetails.Map_Id = (objPromoSku.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c != Null) ? sMapIdM + objPromoSku.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c : '';
              if(objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r != Null)
              {
                objSpendDetails.E1_Company = (objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != Null) ? sE1Company + objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c : '';
                            
                objSpendDetails.E1_Territory = (objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != Null) ? objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c : '';
              }
            }
            if(objPromoSku.Promotion_Catalog__r.Catalog__r != Null)
            {
              objSpendDetails.Catalog = (objPromoSku.Promotion_Catalog__r.Catalog__r.CatalogId__c != Null) ? sCatalogC + objPromoSku.Promotion_Catalog__r.Catalog__r.CatalogId__c : '';
              objSpendDetails.WPR = (objPromoSku.Promotion_Catalog__r.Catalog__r.Title_EDM__r != Null) ? sWPRW + objPromoSku.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c : '';
            }
                                
          }

          objSpendDetails.Release_Id = (objPromoSku.Release_Id__c != Null) ? sReleaseId + objPromoSku.Release_Id__c : '';
          objSpendDetails.GL_Number = HEP_Utility.getConstantValue('HEP_E1_GL_NUMBER');
          objSpendDetails.Subsidiary = '';
          //Map SKU Price Records from map to the json
          Decimal sumMP = 0.0;
          Decimal sumMU = 0.0;
          List<Date> lstDate = new List<Date>();
          System.debug('MAP OF SKU PRICE RECORDS TO PROMOTION SKUS ----- '+mapSkuPrice);
          System.debug('PROMOTION SKU ID ---- '+objPromoSku.Id);
          if(mapSkuPrice.containsKey(objPromoSku.Id))
          {
            System.debug('ENTERING SKU PRICE REGION ---- ');
            for(HEP_SKU_Price__c objSkuPrice : mapSkuPrice.get(objPromoSku.Id))
            {
              System.debug('objSkuPrice.PriceGrade__r ---- '+objSkuPrice.PriceGrade__r);
              if (objSkuPrice.PriceGrade__r != Null)
              {
                System.debug('PRICE GRADE PRESENT-----');
                sumMP = sumMP + objSkuPrice.Unit__c * objSkuPrice.PriceGrade__r.Dealer_List_Price__c;
              }
              if(objSkuPrice.Unit__c!=null)
                sumMU = sumMU + objSkuPrice.Unit__c;
              System.debug('objSkuPrice.Promotion_Dating__r ----- '+objSkuPrice.Promotion_Dating__r);
              if(objSkuPrice.Promotion_Dating__r != Null)
              {
                System.debug('PROMO DATING PRESENT --- ');
                lstDate.add(objSkuPrice.Promotion_Dating__r.Date__c);
              }
            }
          }
          //Kunal - if there are no valid Proces for a Promo SKU, then that Release does not need to be added in request
          else{
            continue;
          }
          System.debug('LIST OF ALL THE DATES ----- '+lstDate);
          if((lstDate != Null) && (!lstDate.isEmpty()))
          {
            lstDate.sort();
            System.debug('LIST OF ALL THE DATES SORTED IN ASCENDING ORDER ----- '+lstDate);
            sEarliestTransactionDate = getJulianDate(lstDate[0]);
            System.debug('EARLIEST TRANSACTION DATE ----- '+sEarliestTransactionDate);
          }
          objSpendDetails.Release_Date = sEarliestTransactionDate;
          //MP TYPE REVENUE RECORDS
          System.debug('MP RECORDS BEING MAPPED----');
          ++lineNumber;
          objSpendDetails.Line_Number = lineNumber;
          objSpendDetails.Ledger_Type = sLedgerTypeMP;

          if(objSpendDetails.E1_Territory == '420')
              objSpendDetails.GL_Amount = Integer.valueOf((sumMP));
          else
              objSpendDetails.GL_Amount = Integer.valueOf((sumMP*100));

          objSpendDetails.GL_Amount = objSpendDetails.GL_Amount*(-1);
          objSpendDetails.GL_Units = 0;
          lstSpendDetails.add(objSpendDetails);

          //putting bRevenue = true after the first Revenue record is written, if there are no valid Revenue record then nothing will be sent to E1
          bRevenue = true;
          System.debug('LIST OF SPEND AND REVENUE MP ROWS --- '+lstSpendDetails);

          
          objSpendDetails = new SpendDetails();

          objSpendDetails.Forecast_Type = HEP_Utility.getConstantValue('HEP_REVENUE_TYPE');
          if(objPromoSku.Promotion_Catalog__r != Null)
          {
            if(objPromoSku.Promotion_Catalog__r.Promotion__r != Null)
            {
              objSpendDetails.Map_Id = (objPromoSku.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c != Null) ? sMapIdM + objPromoSku.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c : '';
              if(objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r != Null)
              {
                objSpendDetails.E1_Company = (objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != Null) ? sE1Company + objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c : '';
                            
                objSpendDetails.E1_Territory = (objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != Null) ? objPromoSku.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c : '';
              }
            }
            if(objPromoSku.Promotion_Catalog__r.Catalog__r != Null)
            {
              objSpendDetails.Catalog = (objPromoSku.Promotion_Catalog__r.Catalog__r.CatalogId__c != Null) ? sCatalogC + objPromoSku.Promotion_Catalog__r.Catalog__r.CatalogId__c : '';
              objSpendDetails.WPR = (objPromoSku.Promotion_Catalog__r.Catalog__r.Title_EDM__c != Null) ? sWPRW + objPromoSku.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c : '';
            }
                                
          }

          objSpendDetails.Release_Id = (objPromoSku.Release_Id__c != Null) ? sReleaseId + objPromoSku.Release_Id__c : '';
          objSpendDetails.GL_Number = HEP_Utility.getConstantValue('HEP_E1_GL_NUMBER');
          objSpendDetails.Subsidiary = '';
          objSpendDetails.Release_Date = sEarliestTransactionDate;

          //MU TYPE REVENUE RECORDS
          System.debug('MU RECORDS BEING MAPPED----');
          ++lineNumber;
          objSpendDetails.Line_Number = lineNumber;
          objSpendDetails.Ledger_Type = sLedgerTypeMU;
          objSpendDetails.GL_Amount = 0;

          if(objSpendDetails.E1_Territory == '420')
              objSpendDetails.GL_Units = Integer.ValueOf((sumMU));
          else
              objSpendDetails.GL_Units = Integer.ValueOf((sumMU*100));

          objSpendDetails.GL_Units = objSpendDetails.GL_Units*(-1);
          lstSpendDetails.add(objSpendDetails);
          System.debug('LIST OF SPEND AND REVENUE ROWS  ---- '+lstSpendDetails);
        }
    }//extraaa

    System.debug('LIST OF DETAILSS -- '+lstSpendDetails);
    if((bSpend == true) && (bRevenue == true))
    {
      SpendHeader spendHead = new SpendHeader(sInterfaceCode, lineNumber, sBatch ,sSequenceNumber);
      spendHead.SpendDetails = lstSpendDetails;
      System.debug('SPENDHEADER ---- '+spendHead);
      Payload payl = new Payload();            
      payl.SpendHeader = spendHead;
      System.debug('PAYLOAD --- '+payl);
      Data dat = new Data(payl);
      System.debug('DATA -- '+dat);
      E1Wrapper objE1Wrapper = new E1Wrapper(dat);
      System.debug('WRAPPERRR --- '+objE1Wrapper);
      sFinalJSON = JSON.serialize(objE1Wrapper);
      System.debug(' RETURNED JSON :::: '+sFinalJSON);        
    }
    else
    {
      sFinalJSON = '';
      System.debug(' RETURNED JSON :::: '+sFinalJSON);                
    }
    return sFinalJSON;

  }//end of function
}//end of class