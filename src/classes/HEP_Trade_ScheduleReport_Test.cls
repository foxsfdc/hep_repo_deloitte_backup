/**
* HEP_Trade_ScheduleReport -- Test class for the HEP_Trade_ScheduleReport for Reports. 
* @author    Ritesh Konduru
*/
@isTest
private class HEP_Trade_ScheduleReport_Test {

    static testMethod void ValidateTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
         User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
       
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.SKU_Interface__c = 'ESCO';
        insert objTerritory;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objTerritory.Id, 'Draft', 'Request', true);
        HEP_Catalog_Genre__c objCatGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
      	objCatGenre.Order__c = 1;
      	objCatGenre.Genre__c ='Adventure';
      	insert objCatGenre;
        
        HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objPromotion.Id, null, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '456', 'New SKU', 'Master', 'Physical', 'DD', 'DVD', true);
        objSKU.Current_Release_Date__c = date.today();//string.valueof(date.today());
        objSKU.Territory__c = objTerritory.id;
        update objSKU;
        
        HEP_SKU_Master__c objSKU2 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, 'UKFNWO', 'New SKU-2', 'Master', 'Physical', 'DD', 'DVD', true);
        objSKU2.Current_Release_Date__c = date.today();//string.valueof(date.today());
        objSKU2.Territory__c = objTerritory.id;
        update objSKU2;
        
        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,  'Approved', 'Unlocked', true);
        objPromotionSKU.Record_Status__c = 'Active';
        update objPromotionSKU;
        
        HEP_Promotion_SKU__c objPromotionSKU1 = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU2.Id,  'Approved', 'Unlocked', true);
        objPromotionSKU1.Record_Status__c = 'Active';
        update objPromotionSKU1;

        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.id, '10', 768.00, null);
        insert objPriceGrade;
        
        HEP_SKU_Price__c objSKUPriceSD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKU.Id, objTerritory.Id, objPromotionSKU.id, null, 'Active', false);
        insert objSKUPriceSD;       

        string lstOfTerritories = HEP_Trade_ScheduleReport.territoryLoad();

        HEP_Trade_ScheduleReport.HEP_TradeScheduleWrap objInteanalSchWrap = (HEP_Trade_ScheduleReport.HEP_TradeScheduleWrap) JSON.deserialize((lstOfTerritories), HEP_Trade_ScheduleReport.HEP_TradeScheduleWrap.class);

        List < HEP_Utility.MultiPicklistWrapper > listTerriotires = objInteanalSchWrap.lstTerrioryWrap; 
        if(listTerriotires[0] != null && listTerriotires.size() >0){
            listTerriotires[0].bSelected = true;
        }
        system.debug('listTerriotires ' + listTerriotires);
         System.runAs(u) {
         Test.startTest();  
         HEP_Trade_ScheduleReport.initialLoad(objInteanalSchWrap.dtStartDate , objInteanalSchWrap.dtStartDate , listTerriotires);
         Test.stopTest();
         System.assertEquals('1234',objCatalog.CatalogId__c);
    }
      }
}