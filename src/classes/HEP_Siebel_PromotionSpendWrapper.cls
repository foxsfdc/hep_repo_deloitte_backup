/**
* HEP_Siebel_Catalog -- Wrapper to store Promotion Market Spend details for interface Siebel 
* @author    Ashutosh Arora
*/

public class HEP_Siebel_PromotionSpendWrapper{

    
    public String ProducerID;
    public String EventType;    
    public String EventName;    
    public Data Data;
    
    /**
    * constructor to initialize the class variables
    * @return nothing 
    * @author Ashutosh Arora
    */
    public HEP_Siebel_PromotionSpendWrapper(){
    this.Data = new Data();
    }
    
    /**
    * Data -- Class to store Spend details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Payload Payload;
      
      /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Ashutosh Arora
        */   
      public Data(){
        this.Payload = new Payload();
      }
    }
    
    /**
    * Payload -- Class to store Spend details 
    * @author    Ashutosh Arora
    */    
    public class Payload {
        public list<Items> items;
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Ashutosh Arora 
        */
        public Payload(){
            items = new list<Items>();
        }
    }
    
     /**
    * Items -- Class to store Rating details 
    * @author    Ashutosh Arora
    */
    public class Items{
        public String objectType;
        public String createdDate;    
        public String createdBy;
        public String updatedDate;    
        public String updatedBy;
        public String recordId;
        public String recordStatus;
        public String territoryId;
        public String territoryName;
        public String promotionId;
        public String promotionName;
        public String promotionCode;
        public String department;
        public Decimal amount;
        public String sphereGroup;
        public String e1Group;
        public String jdeGroup;
    }
    

}