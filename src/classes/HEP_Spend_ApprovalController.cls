public with sharing class HEP_Spend_ApprovalController {
    
    public String sApproverId {get;set;}
    public String sRecordStatus {get;set;}
    public String sApprovalPageLink {get;set;}
    public String sPromotionLink{get;set;}
    public HEP_Record_Approver__c objApproverRecord; 
    public HEP_Market_Spend__c objSpendRecord;
    public String sSpendRecordId = '';
    public Hep_SpendEmailWrapper objTemplateWrapper{
        get{
            if(objTemplateWrapper == null){
                objTemplateWrapper = new Hep_SpendEmailWrapper();
                fetchSpendInformation();
            }
            return objTemplateWrapper;
        }
        set;
    
    }
    public void fetchSpendInformation(){
        //Variables to hold the custom Settings data
        String sHEP_DATETIME_FORMAT = HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT');
        String sSPEND_TAB_EXTENSION = HEP_Utility.getConstantValue('SPEND_TAB_EXTENSION');
        //sApproverId = 'a8a3D0000000doV';
        String sApprovalUrl = '/apex/HEP_Approvals?tab=Budgets&RecId=';
        String sPromotionUrl = '/apex/HEP_Promotion_Details?promoId=';
        String sPromoId = '';
        String sApprovalRecordId = '';
        try{
            List<HEP_Record_Approver__c> lstApprovers = [select Id,
                                                                Approver__r.Firstname,
                                                                Approver__r.Lastname,
                                                                Comments__c,
                                                                Approval_Record__r.HEP_Market_Spend__c,
                                                                Approval_Record__r.Name,
                                                                Approval_Record__r.Approval_Status__c,
                                                                Approval_Record__r.CreatedDate
                                                                from HEP_Record_Approver__c
                                                                where id =:sApproverId];
            system.debug('!@#@#Approver Record' +lstApprovers);  
                                           
            if(lstApprovers != NULL && !lstApprovers.isEmpty()){
                
                objApproverRecord = lstApprovers.get(0);
                sApprovalRecordId = objApproverRecord.Approval_Record__r.Name;          
                sSpendRecordId = String.valueOf(objApproverRecord.Approval_Record__r.HEP_Market_Spend__c);
                List<HEP_Market_Spend__c> lstSpendRecords = [select Id,
                                                                    Comments__c,
                                                                    RequestDate__c,
                                                                    RequestType__c,
                                                                    RecordStatus__c,
                                                                    Promotion__c,
                                                                    Promotion__r.PromotionName__c,
                                                                    Promotion__r.LocalPromotionCode__c,
                                                                    Promotion__r.MarketSpendStatus__c,
                                                                    Promotion__r.FirstAvailableDateFormula__c,
                                                                    Promotion__r.Global_Promotion__r.FirstAvailableDateFormula__c,
                                                                    Promotion__r.Domestic_Marketing_Manager__c,
                                                                    Promotion__r.Domestic_Marketing_Manager__r.Name
                                                                    from HEP_Market_Spend__c
                                                                    where id =: sSpendRecordId];
                                                                    
               system.debug('Spend Record Returned :'+lstSpendRecords);   
                                                                
               if(lstSpendRecords != null && !lstSpendRecords.isEmpty()){
                   
                   objSpendRecord = lstSpendRecords.get(0);
                   //sRecordStatus = objSpendRecord.RecordStatus__c;
                   sRecordStatus = objApproverRecord.Approval_Record__r.Approval_Status__c;
                   system.debug('Record Status :'+sRecordStatus);
                   
                   /*if(objSpendRecord.RecordStatus__c == 'Pending'){
                        objTemplateWrapper.sEmailTitle = 'A new total Budget Update has been submitted for your approval:';
                   }
                   else if(objSpendRecord.RecordStatus__c == 'Budget Approved'){
                        objTemplateWrapper.sEmailTitle = 'The following Budget Update has been Approved';
                   }
                   else if(objSpendRecord.RecordStatus__c == 'Rejected'){
                        objTemplateWrapper.sEmailTitle = 'The following Budget Update has been Rejected';
                   }*/
                   
                   if(sRecordStatus == 'Pending'){
                        objTemplateWrapper.sEmailTitle = 'A new total Budget Update has been submitted for your approval:';
                   }
                   else if(sRecordStatus == 'Approved'){
                        objTemplateWrapper.sEmailTitle = 'The following Budget Update has been Approved';
                   }
                   else if(sRecordStatus  == 'Rejected'){
                        objTemplateWrapper.sEmailTitle = 'The following Budget Update has been Rejected';
                   }
                   
                   
                   
                   
                   objTemplateWrapper.sPromoName = objSpendRecord.Promotion__r.PromotionName__c;
                   objTemplateWrapper.sPromoCode = objSpendRecord.Promotion__r.LocalPromotionCode__c;
                   Datetime requestedDate = objApproverRecord.Approval_Record__r.CreatedDate;
                   //objTemplateWrapper.sReqDate = HEP_Utility.getFormattedDate(requestedDate,'dd-MMM-yyyy');
                   if(sHEP_DATETIME_FORMAT != null)
                        objTemplateWrapper.sReqDate = requestedDate.format(sHEP_DATETIME_FORMAT);
                   if(objSpendRecord.Promotion__r.FirstAvailableDateFormula__c!=null){
                        objTemplateWrapper.sPromoFAD = HEP_Utility.getFormattedDate(objSpendRecord.Promotion__r.FirstAvailableDateFormula__c,'dd-MMM-yyyy'); 
                   }
                   if(objSpendRecord.Promotion__r.Global_Promotion__r.FirstAvailableDateFormula__c!=null){
                        objTemplateWrapper.sPromoFAD = HEP_Utility.getFormattedDate(objSpendRecord.Promotion__r.Global_Promotion__r.FirstAvailableDateFormula__c,'dd-MMM-yyyy');
                   }
                   
                   objTemplateWrapper.sBrandManager = objSpendRecord.Promotion__r.Domestic_Marketing_Manager__r.Name;
                   objTemplateWrapper.sComments = objSpendRecord.Comments__c;
                   objTemplateWrapper.sRejReason = objApproverRecord.Comments__c; //Rejection Reason yet to be finalised
                   sPromoId = objSpendRecord.Promotion__c;
               }
               
               String sBaseUrl =  System.URL.getSalesforceBaseUrl().toExternalForm();
               sApprovalPageLink = sBaseUrl + sApprovalUrl + sApprovalRecordId;
               sPromotionLink = sBaseUrl + sPromotionUrl + sPromoId;
               if(sSPEND_TAB_EXTENSION != null)
                   sPromotionLink = sPromotionLink + sSPEND_TAB_EXTENSION;
               //'&tab=HEP_Promotion_Spend';
               system.debug('Approval URL :'+sApprovalPageLink);
               system.debug('Promotion URL :'+sPromotionLink); 
            }
        }catch(Exception objException){
             HEP_Error_Log.genericException('Exception occured In Spend template','DML Errors',objException,'HEP_Spend_ApprovalController','fetchSpendInformation',null,null); 
        }
            
    }
    
}