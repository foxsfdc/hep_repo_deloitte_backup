@isTest (seeAllData = false)
public class HEP_TitleReleases_Test {
    
     @testSetup 
    Static void setup(){
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
            objTerritory.Flag_Country_Code__c = 'US';
            objTerritory.MM_Territory_Code__c = 'US';
            insert objTerritory;
            HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
            objCatalog.Title_EDM__c = objTitle.id;
            objCatalog.Record_Status__c = 'Active';
            objCatalog.Catalog_Type__c = 'Bundle'; 
            insert objCatalog; 
        
        
        
        
    }
    @isTest
    static void testPost(){
         
            
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/HEP_TitleReleases/'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HEP_TitleReleases.getFoxipediaTitleReleases();
        
        Test.stopTest();

    }
     @isTest
    static void testPost1(){
         
            
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/HEP_TitleReleases/'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
       
        HEP_TitleReleases.getTitleReleases('123','US','Test','345');
        Test.stopTest();

    }
    
    // @isTest
    // static void testGet(){
    //     List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            
    //     RestRequest req = new RestRequest();
    //     RestResponse res = new RestResponse();
    //     req.requestURI = '/services/apexrest/v1/HEP_ReleaseKeys/'; 
    //     req.httpMethod = 'GET';
    //     RestContext.request = req;
    //     RestContext.response = res;
        
    //     Test.startTest();
    //     HEP_ReleaseKeys.getFoxipediaReleaseKeys();
    //     Test.stopTest();
        
    // }
}