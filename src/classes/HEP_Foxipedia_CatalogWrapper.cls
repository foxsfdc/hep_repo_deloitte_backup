/**
* HEP_Foxipedia_CatalogWrapper -- Wrapper to store Catalog details for foxipedia integration
* @author    Gaurav Mehrishi
*/

public with sharing class HEP_Foxipedia_CatalogWrapper {

    public List<DataWrapper> data{get;set;}
    public String calloutStatus {get;set;}
    public String txnId {get;set;}
    public Links links{get;set;}
    public List<Errors> errors {get;set;}
    
    public HEP_Foxipedia_CatalogWrapper(){
        data = new List<DataWrapper>();
    }
    
    public class DataWrapper {
        public Attributes attributes{get;set;}
        public String id{get;set;}  
        public String type{get;set;}  
        public Relationships relationships{get;set;}
        
    }
    /**
    * Attributes -- Class to hold Attribute details 
    * @author    Gaurav Mehrishi
    */
    public class Attributes {
        public String type { get; set; } 
        public String productId { get; set; }    
        public String rowIdObject { get; set; }   
        public String productName { get; set; } 
        public String productTypeCode { get; set; }
        public String productTypeDescription { get; set; } 
        public String productGroupCode { get; set; }
        public String productGroupDescription { get; set; }
        public String productStatusCode { get; set; }
        public String productStatusDescription { get; set; } 
        public String primaryClassificationCode { get; set; }   
        public String primaryClassificationDescription { get; set; }  
        public String primaryCatalogId { get; set; } 
        public String statusDate { get; set; }    
        public String statusComments { get; set; } 
        public String globalEpisodeConfigIndicator { get; set; }
        public String productComment { get; set; }  
        public List<productCatalogWrapper> productCatalogList { get; set; }
        public List<productTitleWrapper> productTitleList { get; set; }
        
        public Attributes(){
           productTitleList = new List<productTitleWrapper>();
        }
        
    }
    /**
    * productCatalogWrapper -- Class to hold product catalog details 
    * @author    Gaurav Mehrishi
    */
    public class productCatalogWrapper {
        public String rowIdObject { get; set; } 
        public String productId { get; set; }    
        public String productName { get; set; }   
        public String productCatalogTypeCode { get; set; } 
        public String productCatalogTypeDescription { get; set; }
        public String productStatusCode { get; set; } 
        public String productStatusDescription { get; set; }    
        public String productGlobalEpisodeConfigIndicator { get; set; }   
        public String productPrimaryClassificationCode { get; set; } 
        public String productPrimaryClassificationDesc { get; set; } 
        public String statusDate { get; set; }    
        public String productComment { get; set; }   
        public String statusComments { get; set; }
        public String rowidProductGroup { get; set; }
    
    }
    /**
    * productTitleWrapper -- Class to hold product title details 
    * @author    Gaurav Mehrishi
    */
    public class productTitleWrapper {
        public String productTypeCode { get; set; } 
        public String productTypeDescription { get; set; }    
        public String jdeVaultId { get; set; }   
        public String financialProductId { get; set; } 
        public String titleVersionTypeCode { get; set; }
        public String titleVersionTypeDescription { get; set; } 
        public String titleVersionDescription { get; set; }    
        public String titleName { get; set; }   
        public String foxVersionId { get; set; } 
        public String releaseDate { get; set; }
        public String lifeCycleStatusGroup { get; set; }
        public String lifeCycleStatusGroupDescription { get; set; }    
        public String productCalendarYear { get; set; }   
        public String languageCode { get; set; } 
        public String languageDescription { get; set; }
        public String productFinancialCode { get; set; }
        public String foxId { get; set; } 
        public String lifeCycleStatusCode { get; set; }
        public String lifeCycleStatusDescription { get; set; }
        public String productCalendarFiscalYear { get; set; }    
        public String releaseFiscalYear { get; set; }   
        public String lifeCycleEffectiveDate { get; set; } 
        public String directorName { get; set; }
        public String mediaCode { get; set; }
        public String mediaDescription { get; set; }    
        public String rowIdTitleVersion { get; set; }   
        public String countryCode { get; set; } 
        public String countryDescription { get; set; }
        public String releaseCalendarYear { get; set; }
        public String financialDivisionCode { get; set; }
        public String financialDivisionDescription { get; set; }
        public String rowIdObject { get; set; }
        public String rowIdTitle { get; set; }
        
    }
    /**
    * Relationships -- Class to hold Relationships details 
    * @author    Gaurav Mehrishi
    */
    public class Relationships {
        public Title title{get;set;}
    }
    /**
    * Title -- Class to hold links details 
    * @author    Gaurav Mehrishi
    */
    public class Title {
        public Links links{get;set;}
    }
    /**
    * Links -- Class to hold related details 
    * @author    Gaurav Mehrishi
    */
    public class Links {
        public String related{get;set;} 
    }
    
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }

    
}