/**
 * HEP_Itunes_Price --- Class for Calling the API to update Price in iTunes
 * @author  Balaji S
 */
Public class HEP_Itunes_Price implements HEP_IntegrationInterface{

    public Static String sHEP_Test_Apple_ID = HEP_Utility.getConstantValue('HEP_Test_Apple_ID');
    Boolean dateUpdated = false;

    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Balaji S 
    */

    Public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){        
        String sAccessToken;
        String RecordId; 
        system.debug('HEP_InterfaceTxnResponse in interface : ' + objTxnResponse);
        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_NTS_E1_OAuth');
        HEP_Response responseWrapper = new HEP_Response();
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_iTunes_Service');
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type','application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                if(String.isNotBlank(objTxnResponse.sSourceId)){
                    system.debug('objTxnResponse.sSourceId'+objTxnResponse.sSourceId);
                    RecordId = getObjectId(objTxnResponse.sSourceId);
                    httpRequest.setBody(createRequestBody(RecordId));
                    HTTP http = new HTTP();
                    HTTPResponse httpResp = http.send(httpRequest);
                    system.debug('httpResp : ' + httpResp);
                    objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                    objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                    String sStatus = httpResp.getStatus();
                    if(sStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){  
                        responseWrapper = (HEP_Response)JSON.deserialize(httpResp.getBody(),HEP_Response.class);
                        System.debug('aaqqq'+responseWrapper);
                        if(String.valueOf(responseWrapper.IsErrorOccured).equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE'))){
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                            //objTxnResponse.sErrorMsg = responseWrapper.Messages[0].messageDetail;
                            for(Messages msg : responseWrapper.Messages){
                                if(msg.messagelevel == 'Error' && String.isBlank(objTxnResponse.sErrorMsg)){
                                    objTxnResponse.sErrorMsg = msg.messageDetail;
                                }
                            } 
                            objTxnResponse.bRetry =  true;
                        }
                        else{
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                            objTxnResponse.bRetry = false;
                            updateProduct(responseWrapper.AppleId, RecordId);

                        }
                    }    
                    else{
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        objTxnResponse.sErrorMsg = httpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    }                  
                }
            }    
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }       
    }   
    

      /**
    * Perform the API call.
    * @param  Ids
    * @return Stringified
    * @author Balaji S
    */
    public String createRequestBody(String objectId){
    
        System.debug('createRequestBody--------'+objectId);
        list<HEP_MDP_Promotion_Product_Detail__c> lstPromotionDetail = new list<HEP_MDP_Promotion_Product_Detail__c>();
        list<HEP_MDP_Promotion_Product__c> lstPromotionProduct = new list<HEP_MDP_Promotion_Product__c>();
        map<String,HEP_MDP_Promotion_Product_Detail__c> mapPromtionDetail = new map<String,HEP_MDP_Promotion_Product_Detail__c>();
        HEP_MDP_Promotion_Product__c objPromotion_Product = new HEP_MDP_Promotion_Product__c();
        HEP_MDP_Promotion_Product_Detail__c objPromotion_Product_Details = new HEP_MDP_Promotion_Product_Detail__c();
        list<HEP_List_Of_Values__c>  lstValues = new list<HEP_List_Of_Values__c>();
        Map<String,String> mapHDValues = new  Map<String,String>();
        Map<String,String> mapSDValues = new  Map<String,String>();
        HEP_Itune_Wrapper iTuneWrap = new HEP_Itune_Wrapper();
        Map<String,Decimal> mapEveryDayValue = new Map<String,Decimal>();
        String HDkey;
        String SDkey;
        String HDValue;
        String SDValue;
        if(objectId != NULL){    
            lstPromotionProduct = [Select id,HEP_Territory__r.Name, HEP_Catalog__c,Title_EDM__c,HEP_Territory__r.Flag_Country_Code__c,Record_Status__c,Product_Start_Date__c,Product_End_Date__c,HEP_Promotion__c,HEP_Previous_Start_Date__c,HEP_Previous_End_Date__c  from HEP_MDP_Promotion_Product__c where id =: objectId AND ( Approval_Status__c =: HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED') OR (HEP_Sent_to_iTunes__c = true AND Record_Status__c = 'Deleted') ) ];   
            
            if(lstPromotionProduct != NULL && !lstPromotionProduct.IsEmpty() ){
                objPromotion_Product  = lstPromotionProduct[0];
            }
            
            lstPromotionDetail =  [SELECT id,Promo_SRP__c,Format__c,Channel__c,Record_Status__c,Promo_WSP__c, HEP_MDP_Product_Mapping__r.Adam_Id__c FROM HEP_MDP_Promotion_Product_Detail__c WHERE Channel__c =: HEP_Utility.getConstantValue('HEP_CHANNEL_EST') AND (Format__c =: HEP_Utility.getConstantValue('HEP_HD') OR Format__c =: HEP_Utility.getConstantValue('HEP_SD')) AND HEP_Promotion_MDP_Product__c =: objPromotion_Product.id AND HEP_MDP_Product_Mapping__c != NULL ];
            System.debug('objPromotion_Product'+objPromotion_Product);
            System.debug('lstPromotionDetail'+lstPromotionDetail);

            if(lstPromotionDetail!=NULL && !lstPromotionDetail.IsEmpty() ){
                for(HEP_MDP_Promotion_Product_Detail__c  obj: lstPromotionDetail){
                    mapPromtionDetail.put(obj.Format__c,obj);
                }            
                System.debug('mapPromtionDetail'+mapPromtionDetail);

                
                for(HEP_List_Of_Values__c values : [select id,Name_And_Parent__c,Parent_Value__c,Record_Status__c,Type__c, Values__c,Order__c from HEP_List_Of_Values__c 
                            where (Type__c =: HEP_Utility.getConstantValue('HEP_MDP_EST_TIER')) AND 
                            (Parent_Value__c =: HEP_Utility.getConstantValue('HEP_HD') OR Parent_Value__c =: HEP_Utility.getConstantValue('HEP_SD') ) AND 
                            Name =: objPromotion_Product.HEP_Territory__r.Name ] ){
                                
                    //put all the HD tier values into a map.
                    if(values.Parent_Value__c == HEP_Utility.getConstantValue('HEP_HD')){
                        mapHDValues.put(values.Order__c,values.Values__c);
                    }
                    else{
                        mapSDValues.put(values.Order__c,values.Values__c);
                    }
                }                

                // Getting the Key 
                if(objPromotion_Product.Title_EDM__c !=NULL){
                    if(mapPromtionDetail != NULL && !mapPromtionDetail.IsEmpty() && mapPromtionDetail.containsKey(HEP_Utility.getConstantValue('HEP_HD')) ){
                        //objPromotion_Product_Details = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD'));
                        HDkey = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Channel__c+objPromotion_Product.Title_EDM__c+objPromotion_Product.HEP_Territory__c+mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Format__c+objPromotion_Product.Product_Start_Date__c;
                    }
                    if(mapPromtionDetail != NULL && !mapPromtionDetail.IsEmpty() && mapPromtionDetail.containsKey(HEP_Utility.getConstantValue('HEP_SD')) ){
                        //objPromotion_Product_Details = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD'));
                        SDkey = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Channel__c+objPromotion_Product.Title_EDM__c+objPromotion_Product.HEP_Territory__c+mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Format__c+objPromotion_Product.Product_Start_Date__c;
                    }
                }
                //else{
                //    if(mapPromtionDetail != NULL && !mapPromtionDetail.IsEmpty() && mapPromtionDetail.containsKey(HEP_Utility.getConstantValue('HEP_HD')) ){
                //        objPromotion_Product_Details = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD'));
                //        HDkey = objPromotion_Product_Details.Channel__c+objPromotion_Product.HEP_Catalog__c+objPromotion_Product.HEP_Territory__c+objPromotion_Product_Details.Format__c+objPromotion_Product.Product_Start_Date__c;
                //    }
                //    if(mapPromtionDetail != NULL && !mapPromtionDetail.IsEmpty() && mapPromtionDetail.containsKey(HEP_Utility.getConstantValue('HEP_SD')) ){
                //        objPromotion_Product_Details = mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD'));
                //        SDkey = objPromotion_Product_Details.Channel__c+objPromotion_Product.HEP_Catalog__c+objPromotion_Product.HEP_Territory__c+objPromotion_Product_Details.Format__c+objPromotion_Product.Product_Start_Date__c;
                //    }

                //}

                System.debug('mapHDValues'+mapHDValues);
                System.debug('mapSDValues'+mapSDValues);
                System.debug('HD KEY'+HDkey);
                System.debug('SD KEY'+SDkey);


                Datetime start_date = (Datetime)objPromotion_Product.Product_Start_Date__c;
                Datetime end_date = (Datetime)objPromotion_Product.Product_End_Date__c;
                System.debug('mapPromtionDetail'+mapPromtionDetail);

                //Creating the Request
                iTuneWrap.start_date = start_date.format('yyyy-MM-dd');
                iTuneWrap.end_date = end_date.format('yyyy-MM-dd');
                
                //if(sHEP_Test_Apple_ID != '' && sHEP_Test_Apple_ID != NULL)
                //    iTuneWrap.appleID = sHEP_Test_Apple_ID;
                //else{
                if(lstPromotionDetail[0].HEP_MDP_Product_Mapping__c != NULL && lstPromotionDetail[0].HEP_MDP_Product_Mapping__r.Adam_Id__c != NULL)
                    iTuneWrap.appleID = lstPromotionDetail[0].HEP_MDP_Product_Mapping__r.Adam_Id__c;
                //}

                iTuneWrap.previousStartDate = iTuneWrap.previousEndDate = ((objPromotion_Product.Record_Status__c == HEP_Utility.getConstantValue('HEP_MDP_Active') && dateUpdated == true)? String.Valueof(objPromotion_Product.HEP_Previous_Start_Date__c) :'');
                iTuneWrap.previousEndDate = iTuneWrap.previousEndDate = ((objPromotion_Product.Record_Status__c == HEP_Utility.getConstantValue('HEP_MDP_Active')  && dateUpdated == true)? String.Valueof(objPromotion_Product.HEP_Previous_End_Date__c) :'');   
                iTuneWrap.territory=objPromotion_Product.HEP_Territory__r.Flag_Country_Code__c;

                //Populating EST_HD_WSP and EST_SD_WSP VALUE

                if((mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active') 
                    || mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active')
                    || objPromotion_Product.Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active'))){
                 
                    mapEveryDayValue = HEP_Utility.calculateEverydaySRP(objPromotion_Product.HEP_Promotion__c);

                    for(String Str:mapEveryDayValue.keySet()){
                        if(Str.contains(HDkey)){
                            HDValue = mapHDValues.get(String.valueof(mapEveryDayValue.get(Str)));
                        }
                        if(Str.contains(SDkey)){
                            SDValue = mapSDValues.get(String.valueof(mapEveryDayValue.get(Str)));
                        }
                    }

                    system.debug('HDValue'+HDValue);
                    system.debug('SDValue'+SDValue);
                    // Promtion is Inactive 
                    if(objPromotion_Product.Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active') || 
                       ((mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active') 
                        && mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active')))){
                         iTuneWrap.EST_HD_WSP = HDValue;
                         iTuneWrap.EST_SD_WSP = SDValue;
                    }

                    else if(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Record_Status__c != HEP_Utility.getConstantValue('HEP_MDP_Active'))
                    {
                        iTuneWrap.EST_HD_WSP = HDValue;
                        iTuneWrap.EST_SD_WSP = mapSDValues.get(String.valueof(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Promo_SRP__c));
                    }  
                    else{
                        system.debug('mapHDValues.get(HDValue)'+Integer.valueOf(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Promo_SRP__c));
                        iTuneWrap.EST_HD_WSP = mapHDValues.get(String.valueof(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Promo_SRP__c));
                        iTuneWrap.EST_SD_WSP = SDValue;
                    }                            
                }
                else{                     
                        iTuneWrap.EST_SD_WSP = mapSDValues.get(String.valueof(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_SD')).Promo_SRP__c));
                        iTuneWrap.EST_HD_WSP = mapHDValues.get(String.valueof(mapPromtionDetail.get(HEP_Utility.getConstantValue('HEP_HD')).Promo_SRP__c));
                    }
            }
    
        System.debug('iTuneWrap'+iTuneWrap);        
        return JSON.serialize(iTuneWrap);
        }
        return NULL;
    }   



    /**
    * Check to which object ID belong's to and return Promotion ID
    * @param  Ids
    * @return Stringified
    * @author Balaji S
    */

    public String getObjectId(String ObjId){
        System.debug('getObjectId--------'+ObjId);
        ID objectId;
        objectId = ObjId;
        //HEP_MDP_Promotion_Product_Detail__c PromotionDetail = new HEP_MDP_Promotion_Product_Detail__c();
        //System.debug('objectId.getSObjectType().getDescribe().getName()'+objectId.getSObjectType().getDescribe().getName());
        //if(objectId.getSObjectType().getDescribe().getName() == HEP_Utility.getConstantValue('HEP_MDP_Promotion_Product_Detail'))
        //{
        //    PromotionDetail = [select id,Everyday_WSP__c,HEP_Promotion_MDP_Product__c from HEP_MDP_Promotion_Product_Detail__c where ID =: objId];
        //    System.debug('PromotionDetail'+PromotionDetail);
        //    return PromotionDetail.HEP_Promotion_MDP_Product__c;
        //}
        //else{
            System.debug('In else part');
            dateUpdated = true;
            return objectId;
        //}
    }

    /**
    * Is the Response the Sucess then Update the Promotion_product Previous Start date and End Date 
    * @param  Ids
    * @return Stringified
    * @author Balaji S
    */

    Public void updateProduct(String AppleId, String RecordId){
        list<HEP_MDP_Promotion_Product__c> lstObjPromotion = new list<HEP_MDP_Promotion_Product__c>();
        if(!String.isBlank(AppleId)){
            System.debug('updating the Record');
            lstObjPromotion = [Select id,HEP_Previous_End_Date__c,HEP_Previous_Start_Date__c,HEP_Sent_to_iTunes__c,Product_Start_Date__c,Product_End_Date__c from HEP_MDP_Promotion_Product__c where id =: RecordId];
            if(lstObjPromotion != NULL && !lstObjPromotion.IsEmpty() ){
                lstObjPromotion[0].HEP_Previous_Start_Date__c = lstObjPromotion[0].Product_Start_Date__c;
                lstObjPromotion[0].HEP_Previous_End_Date__c = lstObjPromotion[0].Product_End_Date__c;
                lstObjPromotion[0].HEP_Sent_to_iTunes__c = true;
                 System.debug('ObjPromotion'+lstObjPromotion[0]);
                update lstObjPromotion[0];
            }
        }    
    }

    //Response Wrapper 
    public class HEP_Itune_Wrapper{
        public String start_date{get;set;}
        public String EST_HD_WSP{get;set;}
        public String end_date{get;set;}
        public String EST_SD_WSP{get;set;}
        public String previousStartDate{get;set;}
        public String territory{get;set;}
        public String previousEndDate{get;set;}
        public String appleID{get;set;}
    }
    
    public class HEP_Response{
            public Boolean IsErrorOccured{get;set;}
            public list<Messages> Messages{get;set;}
            public String AppleId{get;set;}
            
            public HEP_Response(){
                Messages = new list<Messages>();
            }
    }   
        
        public class Messages{
            public String messageDetail{get;set;}
            public String messagelevel{get;set;}
        }
}