/**
 * HEP_SKU_ApprovalResponse --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_SKU_ApprovalResponse{
    
    //SKU Promotion record id is passed in this variable
    public Id recordApproverIdForSKUPromotion  {get; set;}
    public String sPageLink {get;set;}
    
    //This variable is used to render table on Email template
    public List<ApprovalEmailWrapper> lstApprovalwrapper{ 
        get{
            if(lstApprovalwrapper == null){
                fetchSKUPromotionDetails();
            }
            return lstApprovalwrapper;
        }
        set;
    }
    public ApprovalEmailWrapper objApprovalwrapper{ 
        get{
            if(objApprovalWrapper== null){
                fetchSKUPromotionDetails();
            }
            return objApprovalwrapper;
        }
        set;
    }
     
    
    /**
    * fetchSKUPromotionDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author  Gaurav Mehrishi
    */  
    public void fetchSKUPromotionDetails(){
        
        try{
            String sSKUPromotionPageUrl='/apex/HEP_Promotion_Details?promoId=';
            //this.lstApprovalwrapper = new List<ApprovalEmailWrapper>();   
            this.objApprovalwrapper= new ApprovalEmailWrapper(); 
            //ApprovalEmailWrapper objApprovalwrapper;           
            Integer iCount = 0;   
            String sSKUPromotionId;   
            if(recordApproverIdForSKUPromotion != null){          
                List<HEP_Record_Approver__c> lstRecordApproverForSKUPr = [SELECT Id,
                                                    Name,
                                                    Approval_Record__r.CreatedDate,
                                                    Approval_Record__r.CreatedBy.Name,
                                                    Approver__r.Name,
                                                    Comments__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.FirstAvailableDateFormula__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Title__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Number__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Format__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Channel__c,                                                                                   
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Unlock_Comments__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Unlock_Reason__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Locked_Status__c
                                                    FROM HEP_Record_Approver__c
                                                    Where Id =: recordApproverIdForSKUPromotion
                                                    LIMIT 1];              
                //Validate the List and Populate the Wrapper
                if(lstRecordApproverForSKUPr != null && !lstRecordApproverForSKUPr.isEmpty()){    
                    System.debug('+++++++++' + lstRecordApproverForSKUPr);
                    sSKUPromotionId = lstRecordApproverForSKUPr[0].Approval_Record__r.HEP_Promotion_SKU__c;
                    HEP_Record_Approver__c objSKURecordApprover = lstRecordApproverForSKUPr.get(0);
                    objApprovalwrapper.sActionReqdBy = objSKURecordApprover.Approver__r.Name;
                    objApprovalwrapper.sPromotionCode = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c;
                    objApprovalwrapper.sPromotionName = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c;
                    objApprovalwrapper.sSKUName = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Title__c;
                    objApprovalwrapper.sSKUNumber = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.SKU_Number__c;
                    objApprovalwrapper.sFormat = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Format__c;
                    objApprovalwrapper.sChannel = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Channel__c;
                    //objApprovalwrapper.sComment = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Unlock_Reason__c;
                    objApprovalwrapper.sComment = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Unlock_Comments__c;
                    objApprovalwrapper.sStatus = HEP_Utility.getConstantValue('HEP_PROMOTION_SKU_APPROVAL_MSG');
                    //objApprovalWrapper.sRejectionText = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Unlock_Comments__c;
                    objApprovalWrapper.sRejectionText = objSKURecordApprover.Comments__c;
                    objApprovalwrapper.sRequestor = objSKURecordApprover.Approval_Record__r.CreatedBy.Name;
                    objApprovalwrapper.sPromotionFAD = HEP_Utility.getFormattedDate(objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.FirstAvailableDateFormula__c,'dd-MMM-yyyy'); 
                    //On Basis Of Approval Send the status in Body                 
                    if(objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Locked_Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED'))){             
                        objApprovalwrapper.sStatus += ' ' + HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED');
                        objApprovalwrapper.bRejectionstatus = false;
                    }else{              
                        objApprovalwrapper.sStatus += ' ' + HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
                        objApprovalwrapper.bRejectionstatus = true;                       
                    }                    
                       
                    
                }
                 //Query to get all the Price Regions records for the promotion SKU record
                List<HEP_SKU_Price__c> lstSKUPriceRegion = [SELECT Id,
                                                    PriceGrade__c,
                                                    PriceGrade__r.Name,
                                                    PriceGrade__r.PriceGrade__c,
                                                    PriceGrade__r.Dealer_List_Price__c,
                                                    Region__c,
                                                    Region__r.Name,
                                                    Unit__c
                                                    FROM HEP_SKU_Price__c
                                                    WHERE Promotion_SKU__c =: sSKUPromotionId];
                objApprovalwrapper.lstPriceRegion = new List<PriceRegionWrapper>();                                    
                if(lstSKUPriceRegion != null && !lstSKUPriceRegion.isEmpty()){
                    for(HEP_SKU_Price__c objPrRegion : lstSKUPriceRegion){
                        PriceRegionWrapper objPriceRegion = new PriceRegionWrapper();
                        objPriceRegion.sRegion = objPrRegion.Region__r.Name;
                        objPriceRegion.sPriceGrade = objPrRegion.PriceGrade__r.PriceGrade__c;
                        objApprovalwrapper.lstPriceRegion.add(objPriceRegion);                 
                    }
                }
                
                
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

       
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class ApprovalEmailWrapper{
        
        public String   sActionReqdBy     {get; set;}
        public String   sPromotionCode    {get; set;}
        public String   sPromotionName    {get; set;}
        public String   sReleaseDate      {get; set;}
        public String   sSKUName          {get; set;}
        public String   sSKUNumber        {get; set;}
        public String   sRegion           {get; set;}
        public String   sChannel          {get; set;}
        public String   sFormat           {get; set;}
        public String   sPriceGrade       {get; set;}
        public String   sComment          {get; set;}
        public String   sRequestor        {get; set;}
        public String   sStatus           {get; set;}
        public String   sRejectionText    {get; set;}
        public Boolean  bRejectionstatus  {get; set;} 
        public String   sPromotionFAD  {get; set;}  
        public Integer  iIndex            {get; set;}
        public List<PriceRegionWrapper> lstPriceRegion {get; set;}
    }
    public class PriceRegionWrapper{
            public String sRegion     {get; set;} 
            public String sPriceGrade {get; set;}
    }

}