/**
* HEP_Siebel_CatalogDetails -- Controller to smake the callout to Siebel interface for updating/deleting/deleting the Catalog Genre details
* @author    Lakshman Jinnuri
*/
public class HEP_Siebel_CatalogGenreDetails implements HEP_IntegrationInterface{ 
    //Variables to hold the custom Settings data
    String sHEP_SIEBEL_OAUTH = HEP_Utility.getConstantValue('HEP_SIEBEL_OAUTH');
    String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
    String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
    String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
    String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
    String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
    String sHEP_SIEBEL_PRODUCERID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
    String sHEP_SIEBEL_EVENTTYPE = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
    String sHEP_SIEBEL_EVENTNAME = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
    String sHEP_SIEBEL_CATALOG_GENRE_VALUE = HEP_Utility.getConstantValue('HEP_SIEBEL_CATALOG_GENRE_VALUE');
    String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
    String sHEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    String sHEP_SIEBEL_SERVICE = HEP_Utility.getConstantValue('HEP_SIEBEL_SERVICE');
    String sGENRE = HEP_Utility.getConstantValue('HEP_FOX_GENRE');
    /**
    * performTransaction -- Method to store Catalog details 
    * @param  Takes HEP_InterfaceTxnResponse wrapper as input
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        String sCatalogJson;
        HEP_Siebel_Catalog_GenreWrapper objWrapper = new HEP_Siebel_Catalog_GenreWrapper();  
        if(sHEP_SIEBEL_OAUTH != null)   
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_SIEBEL_OAUTH);
        System.debug('sAccessToken :'+sAccessToken);

        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sCatalogGenreWrapper;
            if(objTxnResponse.sSourceId != null){
                sCatalogGenreWrapper = getRequestBody((List<Id>)JSON.deserialize(objTxnResponse.sSourceId,list<Id>.class));
                System.debug('request body :' + sCatalogGenreWrapper);
            }
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for Authentication
            if(sHEP_SIEBEL_SERVICE != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_SIEBEL_SERVICE);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c);
                objHttpRequest.setMethod('POST');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('Accept','application/json');
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objHttpRequest.setBody(sCatalogGenreWrapper);
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                System.debug('objHttpResp.getStatus() :' + objHttpResp.getStatus()); 
                if(sHEP_SUCCESS != null && sHEP_STATUS_ACCEPTED != null){
                    if(objHttpResp.getStatus().equals(sHEP_SUCCESS) || objHttpResp.getStatus().equals(sHEP_STATUS_ACCEPTED) && sHEP_Int_Txn_Response_Status_Success != null){         
                        objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                    }
                    else{
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    }
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null  && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }     
    }

    /**
    * getRequestBody -- gets the details from Catalog Genre Object based on input paramters
    * @param  Takes List of Id's
    * @return Serialised HEP_Siebel_Catalog_GenreWrapper wrapper   
    * @author Lakshman Jinnuri      
    */
    @TestVisible
    private string getRequestBody(list<ID> lstCatalogGenreIds){
        HEP_Siebel_Catalog_GenreWrapper objCatalogWrapper = new HEP_Siebel_Catalog_GenreWrapper();
        list<HEP_Siebel_Catalog_GenreWrapper.Items> lstTempItems = new list<HEP_Siebel_Catalog_GenreWrapper.Items>();
        
        List<String> lstGenreTypes = new List<String>{sGENRE};
        map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(lstGenreTypes, 'Name', 'Values__c');
        System.debug('mapLOVMapping>>' + mapLOVMapping + 'lstCatalogGenreIds: '+ lstCatalogGenreIds);
        if(sHEP_SIEBEL_PRODUCERID != null && sHEP_SIEBEL_EVENTNAME != null && sHEP_SIEBEL_EVENTTYPE != null){
            objCatalogWrapper.ProducerID = sHEP_SIEBEL_PRODUCERID;
            objCatalogWrapper.EventType = sHEP_SIEBEL_EVENTTYPE;
            objCatalogWrapper.EventName = sHEP_SIEBEL_EVENTNAME;
        }    
        for(HEP_Catalog_Genre__c objCatalogGenre: [SELECT 
                      Id,CreatedDate,CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Record_Status__c,Catalog__r.Territory__r.Id,
                      Catalog__r.Territory__r.Name,Catalog__r.CatalogId__c,Catalog__r.Id,Order__c,Genre__c,Legacy_Id__c
                      FROM
                        HEP_Catalog_Genre__c
                      WHERE 
                        Id IN :lstCatalogGenreIds]) {
            HEP_Siebel_Catalog_GenreWrapper.Items objCatalogGenreItem = new HEP_Siebel_Catalog_GenreWrapper.Items();
            if(sHEP_SIEBEL_CATALOG_GENRE_VALUE != null)  
                objCatalogGenreItem.objectType = sHEP_SIEBEL_CATALOG_GENRE_VALUE;
            objCatalogGenreItem.createdDate = String.ValueOf(objCatalogGenre.CreatedDate);  
            objCatalogGenreItem.createdBy = objCatalogGenre.CreatedBy.Name;    
            objCatalogGenreItem.updatedDate = String.ValueOf(objCatalogGenre.LastModifiedDate);  
            objCatalogGenreItem.updatedBy = objCatalogGenre.LastModifiedBy.Name;    
            objCatalogGenreItem.recordId = objCatalogGenre.Id; 
            objCatalogGenreItem.recordStatus = objCatalogGenre.Record_Status__c; 
            objCatalogGenreItem.territoryId = objCatalogGenre.Catalog__r.Territory__r.Id; 
            objCatalogGenreItem.territoryName = objCatalogGenre.Catalog__r.Territory__r.Name;  
            objCatalogGenreItem.catalogId = objCatalogGenre.Catalog__r.Id;      
            objCatalogGenreItem.catalogNumber = objCatalogGenre.Catalog__r.CatalogId__c;   
            if(objCatalogGenre.Genre__c != null){
                String sLOVExists = sGENRE + '*' + objCatalogGenre.Genre__c;
                System.debug('sLOVExists'+sLOVExists);
                objCatalogGenreItem.genre = mapLOVMapping.containsKey(sLOVExists) ? mapLOVMapping.get(sLOVExists) : objCatalogGenre.Genre__c;
            }else
                objCatalogGenreItem.genre =  objCatalogGenre.Genre__c;      
            objCatalogGenreItem.orderBy = String.valueOf(objCatalogGenre.Order__c); 
            objCatalogGenreItem.legacyId = objCatalogGenre.Legacy_Id__c;
            lstTempItems.add(objCatalogGenreItem);
        }
        objCatalogWrapper.Data.Payload.items.addAll(lstTempItems);  
        return JSON.serialize(objCatalogWrapper);       
    }    
}