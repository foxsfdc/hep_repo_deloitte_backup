@istest(SeeAllData=false)
public class HEP_Promotion_SKU_Siebel_Wrapper_Test {
   @testSetup
        static void createUsers(){
            list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
            list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Jaggi', 'abc_xyz@deloitte.com','majaggi','Mayank','majaggi','', true);
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
            HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary', null, null,'EUR','DE','182', 'ESCO', false);
            // createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'ESCO', false)
            // createHEPTerritory(String sTerritoryName, String sRegion, string sType, String sParentId, String sCatalogTerritoryId, String sCurrencyCode, String sTerritoryCodeIntg, String sERMTerrCode, string sSKUInterface, boolean bInsert)
            insert objTerr;
            string templateName = HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change');
            string objName = HEP_Utility.getConstantValue('HEP_PROMOTION');
            HEP_Notification_Template__c notifTemplate = HEP_Test_Data_Setup_Utility.createTemplate(templateName, objName,'has been Approved','Global promotion creation', 'Active', 'Pending', 'BAC', false);
            insert notifTemplate;
        }
        

    @isTest
    static void TEST_HEP_Promotion_SKU_Siebel_Wrapper_SuccessTest(){
        String promotionTypeGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        String promotionTypeNational = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            
            HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',null);
            insert objLOB;
            HEP_Territory__c objTerr = [select id from HEP_Territory__c limit 1];
            HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerr.Id,objLOB.Id,'','',null);
            insert objGlobalPromotion;
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerr.Id, objLOB.Id,'','',null);
            objPromotion.LocalPromotionCode__c = '0000000032a'; 
            insert objPromotion;
            HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
            insert objCat;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerr.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
            objSKUMaster.Revenue_Share__c = False;
            insert objSKUMaster;
            HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
            insert objPromotionCatalog;
            HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', '', false);
            objPromotionSKU.Approval_Status__c = 'Approved';
            objPromotionSKU.Legacy_Id__c = 'ABC123';
            objPromotionSKU.Release_Id__c = '1234';
            insert objPromotionSKU;

            HEP_Promotion_SKU__c objRecord = [Select CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, LastModifiedBy.FirstName,
                                             LastModifiedBy.LastName, Record_Status__c, SKU_Master__r.Territory__r.Id, SKU_Master__r.Territory__r.Name,
                                    Promotion_Catalog__r.Promotion__r.Id, Promotion_Catalog__r.Promotion__r.PromotionName__c, Release_Id__c,
                                    Promotion_Catalog__r.Catalog__r.CatalogId__c, Name,                            
                                    Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c, SKU_Master__r.SKU_Title__c,
                                    Promotion_Catalog__c, SKU_Master__r.Channel__c, Approval_Status__c,
                                    Promotion_Catalog__r.Catalog__c, SKU_Master__r.Format__c, SKU_Master__r.Revenue_Share__c,
                                    Promotion_Catalog__r.Catalog__r.Name,
                                    SKU_Master__r.Name, SKU_Master__r.SKU_Number__c,
                                    Promotion_Catalog__r.Record_Status__c, Legacy_Id__c from HEP_Promotion_SKU__c where 
                                    (Promotion_Catalog__r.Promotion__r.Promotion_Type__c = :promotionTypeGlobal
                                     or Promotion_Catalog__r.Promotion__r.Promotion_Type__c = :promotionTypeNational)
                                    and HEP_Promotion_SKU__c.Id = :objPromotionSKU.Id];
            
            
            String revenueShareFlag = '';
            if(objPromotionSKU.SKU_Master__r.Revenue_Share__c == true){revenueShareFlag = 'Y';}
            else{revenueShareFlag = 'N';}
            List<String> lstStrId = new List<String>();
            lstStrId.add(String.valueOf(objRecord.Id));

            Test.startTest();
            String sresultJson = HEP_Promotion_SKU_Siebel_Wrapper.getPromotionSKUFields(lstStrId);
            String updatedDate = String.ValueOf(objRecord.lastModifiedDate);
            String sUserName = (String.valueOf(objRecord.LastModifiedBy.Name)).toUpperCase();// + ' ' + uncapitalize(string.valueOf(objRecord.LastModifiedBy.LastName));
            String sActualResult = '{"ProducerID":"HEP","EventType":"HEPPromoOutbound","EventName":"HEPBroadcastEvent","Data":{"Payload":{"items":[{"updatedDate":"'+ updatedDate +'","updatedBy":"' + (string.valueOf(objRecord.LastModifiedBy.Name)) + '","territoryName":"' + String.valueOf(objRecord.SKU_Master__r.Territory__r.Name) +'","territoryId":"'+ String.valueOf(objRecord.SKU_Master__r.Territory__c) +'","skuNumber":"'+ String.valueOf(objRecord.SKU_Master__r.SKU_Number__c) +'","skuId":"'+ String.valueOf(objRecord.SKU_Master__c) +'","revenueShareFlag":"'+ String.valueOf(revenueShareFlag) +'","releaseName":"'+ objRecord.SKU_Master__r.SKU_Title__c +'","releaseId":'+ String.ValueOf(objRecord.Release_Id__c) +',"recordStatus":"Active","recordId":"'+ String.ValueOf(objRecord.Id) +'","promotionSKUStatus":"'+ objRecord.Approval_Status__c +'","promotionName":"'+ objRecord.Promotion_Catalog__r.Promotion__r.PromotionName__c +'","promotionId":"'+ String.valueOf(objRecord.Promotion_Catalog__r.Promotion__c) +'","promotionCode":"'+ String.valueOf(objRecord.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c) +'","objectType":"'+ HEP_Utility.getConstantValue('HEP_SKU_Promotion_objecttype') +'","legacyId":"'+ objRecord.Legacy_Id__c +'","format":"'+ objRecord.SKU_Master__r.Format__c +'","createdDate":"'+ String.ValueOf(objRecord.createdDate) +'","createdBy":"'+ (String.ValueOf(objRecord.createdBy.Name)) +'","channel":"' +objRecord.SKU_Master__r.Channel__c + '","catalogNumber":' +String.ValueOf(objRecord.Promotion_Catalog__r.Catalog__r.CatalogId__c) +',"catalogId":"'+String.ValueOf(objRecord.Promotion_Catalog__r.Catalog__c) +'"}]}}}';
            System.assertEquals(sresultJson, sActualResult);
            HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
            objTxnResponse.sSourceId = objPromotionSKU.Id;       
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Promotion_SKU_Siebel_Wrapper demo = new HEP_Promotion_SKU_Siebel_Wrapper();
            System.debug('RESPONSE BEFORE -- '+objTxnResponse);
            demo.performTransaction(objTxnResponse);     
            System.debug('RESPONSE AFTER -- '+objTxnResponse);
            Test.stoptest();
        }
    }

    @isTest static void TEST_HEP_Promotion_SKU_Siebel_Wrapper_FailureTest(){
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Siebel_Service';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/edf/events/failure';
        lstServices.add(objService);
        insert lstServices;
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
            objTxnResponse.sSourceId = 'aer54444'; 
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Promotion_SKU_Siebel_Wrapper demo = new HEP_Promotion_SKU_Siebel_Wrapper();
            System.debug('RESPONSE BEFORE -- '+objTxnResponse);
            demo.performTransaction(objTxnResponse);
            System.debug('RESPONSE AFTER -- '+objTxnResponse);
            Test.stoptest();
        }
    }
    @isTest static void TEST_HEP_Promotion_SKU_Siebel_Wrapper_InvalidAcessTokenTest()
    {
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Siebel_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token/tokenfail';
        lstServices.add(objService);
        insert lstServices;
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Promotion_SKU_Siebel_Wrapper demo = new HEP_Promotion_SKU_Siebel_Wrapper();
            System.debug('RESPONSE BEFORE -- '+objTxnResponse);
            demo.performTransaction(objTxnResponse);
            System.debug('RESPONSE AFTER -- '+objTxnResponse);
            Test.stoptest();        
            System.assertEquals('Invalid Access Token Or Invalid txnResponse data sent', objTxnResponse.sErrorMsg); 
        }       
    }  
}