/**
* HEP_Expand_UserAccess_Batch --- Batch class to Expand the Access in User Role for all Parent Tewrritories
* @author    Nishit Kedia
*/
public class HEP_Expand_UserAccess_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    //Global Variables to be used in Batch processing
    String sActive =HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    String sRoleToBeExpanded  = HEP_Utility.getConstantValue('HEP_ROLE_NAME_TO_EXPAND');// This will be Default Access Role
    String sGlobalTerritory =  HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME');
    
    /**
    * start --- Batch start method which is invoked when batch is scheduled
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @return   Query Locator which will return query processed on batch size 10
    * @author   Nishit Kedia
    */ 
    public Database.QueryLocator start(Database.BatchableContext objBatchableContext){
        
        
        //Get all the User Role Elligible for Expansion in Parent Territories  
        String sbuildQuery='';
        
        //User Role obj fields
        sbuildQuery += 'Select Id,Read__c,Record_Status__c,Role__c,Role__r.Name,Territory__c,Territory__r.Name,User__c,User__r.UserName,Write__c,Unique_Id__c';
        
        //Object: Pickup records from User Role object
        sbuildQuery += ' From HEP_User_Role__c where ';
        
        //Filter Criteria
        sbuildQuery += 'Record_Status__c=:sActive And Role__r.Name = : sRoleToBeExpanded and Territory__r.Name = :sGlobalTerritory';
        
        System.debug('Query Built-->' + sbuildQuery);
        return Database.getQueryLocator(sbuildQuery);   
    }
    
    
    /**
    * execute --- Batch execute method which is kept in thread after start finishes
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @param    lstfailedTransactions which was picked by batch to process and retry
    * @return   nothing
    * @author   Nishit Kedia
    */ 
    public void execute(Database.BatchableContext objBatchableContext, List<HEP_User_Role__c> lstUserRolesToExpand){
        
        try{
            //Check whether any records are picked in Batch
            if(lstUserRolesToExpand != null && !lstUserRolesToExpand.isEmpty() ){
                List<HEP_User_Role__c> lstUserRolesToInsert = new List<HEP_User_Role__c>();
                List<HEP_Territory__c> lstTerritory = [Select  Id,
                                                            Name 
                                                            from HEP_Territory__c 
                                                            where Parent_Territory__c = null
                                                            And Name != :sGlobalTerritory];
                for(HEP_User_Role__c objUserRole : lstUserRolesToExpand){
                    //Iterate over Evry territory in HEP and clone user roles based on it
                    for(HEP_Territory__c objTerritory: lstTerritory){
                                                                
                        HEP_User_Role__c objuserToInsert =  objUserRole.clone();
                        objuserToInsert.Territory__c = objTerritory.Id;
                        objuserToInsert.Read__c = true;
                        objuserToInsert.Write__c = false;                       
                        objuserToInsert.Unique_Id__c = objUserRole.User__r.UserName +' / '+ objUserRole.Role__r.Name + ' / ' + objTerritory.Name;
                        lstUserRolesToInsert.add(objuserToInsert);
                    }
                }
                System.debug('List of User roles-->'+lstUserRolesToInsert);
                if(lstUserRolesToInsert.size()>0){
                  //Upsert roles using Unique Id
                    Schema.SObjectField UniqueId =  HEP_User_Role__c.Fields.Unique_Id__c;
                    Database.upsert(lstUserRolesToInsert, UniqueId, false); 

                }                               
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            HEP_Error_Log.genericException('Error occured during Retry','Batch Class',e,'HEP_Expand_UserAccess_Batch','execute',null,'');
        }        
    }
    
    
    /**
    * finish --- Batch finish method which will schedule the batch after every 10 mins
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @return   nothing
    * @author   Nishit Kedia
    */  
    public void finish(Database.BatchableContext objBatchableContext){
      //Empty Code Block
    }
    
    
    /*
    @Code Block To Query The User Set 
    ---->SELECT Id, Name, Read__c, Record_Status__c, Role__c, Role__r.Name, Search_Id__c, Territory__c, Territory__r.Name, Unique_Id__c, User__c, Write__c FROM HEP_User_Role__c WHERE Role__r.Name = 'Default Access'
    @Code Block To Run the Batch
     ---->Database.executeBatch(new HEP_Expand_UserAccess_Batch(), 10);   
    */
}