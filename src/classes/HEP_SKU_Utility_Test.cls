/* 
 @HEP_SKU_Utility_Test---Test Class for SKU Utility
 @author : Deloitte
 */

@isTest
public class HEP_SKU_Utility_Test{
    
  @testSetup
  public static void createTestUserSetup(){
    //constant Data
    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
    
    //User Setup
    User objTestUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
    User objTestUser2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest2', 'ApprovalTestUser2@hep.com','ApprovalTest2','App2','ApTest2','', true);
    
    System.runAs (objTestUser) {
    
    //Territory Set up
    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182','ESCO', false);
    insert objTerritory;
    
    HEP_Territory__c objDHETerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '183','JDE', false);
    insert objDHETerritory;
     
    //Global Territory
    HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
    
    //Insert Line of Business records
    HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV','TV',true);
    
    //Role Set Up FOR SPEND
    List<HEP_Role__c> lstRoles = new List<HEP_Role__c>();
    HEP_Role__c objRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPRole('Data Admin', 'Promotions' ,false);
    objRoleAdmin.Destination_User__c = objTestUser.Id;
    objRoleAdmin.Source_User__c = objTestUser.Id;  
    lstRoles.add(objRoleAdmin) ;

    insert lstRoles;
    
    //User role Set Up  
    List<HEP_User_Role__c > lstUserRole = new List<HEP_User_Role__c >();
    HEP_User_Role__c objUserRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleAdmin.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleAdmin);
    insert lstUserRole;
    
    
    //Create Global Promotion:
    HEP_Promotion__c objTestGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Global','Global','',objGlobalTerritory.Id,objLOB.Id,'','',true);
    //Create National Promotion
    HEP_Promotion__c objTestNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Germany NP','National',objTestGlobalPromotion.Id,objTerritory.Id,objLOB.Id,'','',true);
    HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-DHE NP','National',objTestGlobalPromotion.Id,objDHETerritory.Id,objLOB.Id,'','',true);
    
    //Notification Template Creation
    HEP_Notification_Template__c objGlobalPromoTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Creation','HEP_Promotion__c','A global promotion has been created','Global promotion creation','Active','-','Global_Promotion_Creation',true);
    HEP_Notification_Template__c objfadNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','A new FAD is done','FAD Date Change','Active','-','Global_FAD_Date_Change',true);
    // Catalog Records
    HEP_Catalog__c objCatalog1 = HEP_Test_Data_Setup_Utility.createCatalog('52560', 'ARTHUR', 'Single', null, objTerritory.Id, 'Approved', 'Master', false);
    insert objCatalog1;

    HEP_Catalog__c objCatalog2 = HEP_Test_Data_Setup_Utility.createCatalog('52460', 'ARTHUR1', 'Single', null, objTerritory.Id, 'Approved', 'Master', false);
    insert objCatalog2;
    HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog1.Id, objTerritory.Id, '12345', 'The Age of empire', 'Master', 'Physical', 'RETAIL', 'BD', false);
    insert objSKU;
    HEP_SKU_Master__c objSKU1 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog2.Id, objTerritory.Id, '123457', 'The Age of empire1', 'Master', 'Physical', 'VOD', 'HD', false);
    insert objSKU1;
    HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('AJ', objTerritory.Id, '8163888', 'SKU Customers', '', false);
    insert objCustomer;
    HEP_Customer__c objCustomer1 = HEP_Test_Data_Setup_Utility.createHEPCustomers('AJ1', objTerritory.Id, '8163889', 'SKU Customers', '', false);
    insert objCustomer1;
    HEP_SKU_Customer__c objSkuCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id, objSKU.Id, false);
    insert objSkuCustomer;

    HEP_SKU_Customer__c objSkuCustomer1 = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer1.Id, objSKU.Id, false);
    insert objSkuCustomer1;

    HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id, '$44.95 - $31.50', 44.95, false);
    insert objPriceGrade;

    HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKU.Id, objTerritory.Id, null, null, 'Active', false);
    objSKUPrice.Changed_Flag__c = true;
    objSKUPrice.Start_Date__c = Date.newInstance(1990, 09, 20);
    insert objSKUPrice;
    }
  }
  
  static TestMethod void testautoApproval() {
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
          objInterface.Name = 'HEP_E1_SpendDetail';
          Update objInterface;
        //Fetch the created Test User:
        User objTestUser = [Select  Id,
                                    Name 
                                    from User 
                                    where Email = 'ApprovalTestUser@hep.com'];
                                   
        HEP_Territory__c objGerTerritory = [Select Id,
                                                    Name 
                                                    from HEP_Territory__c
                                                    where Name = 'Germany'];
                                                    
        HEP_Territory__c objDHETerritory = [Select Id,
                                                    Name 
                                                    from HEP_Territory__c
                                                    where Name = 'DHE'];
        
        //fetch promotion:
        HEP_Promotion__c objTestPromotion = [Select Id,
                                                    Territory__c,
                                                    Name 
                                                    from HEP_Promotion__c 
                                                    where Territory__c =: objGerTerritory.Id];
        
         HEP_Promotion__c objDHEPromotion = [Select Id,
                                                    Territory__c,
                                                    Name 
                                                    from HEP_Promotion__c 
                                                    where Territory__c =: objDHETerritory.Id];
        
        HEP_Territory__c objTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium','EMEA','Subsidiary',objGerTerritory.Id, null, 'EUR', 'BE' , '49','ESCO', false);
        insert objTerritoryRegion;
        
        //Run in the context of User:
        System.runAs(objTestUser){
            
          //Create Catalog Record
          HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
          objCatalog.Licensor_Type__c = 'Lucas';
          objCatalog.Licensor__c = 'ABLO';
          insert objCatalog;
          
          HEP_Catalog_Genre__c objCatGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
          objCatGenre.Order__c = 1;
          objCatGenre.Genre__c ='Adventure';
          insert objCatGenre;
          
          HEP_Catalog_Genre__c objCatGenre1 = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
          objCatGenre1.Order__c = 1;
          objCatGenre1.Genre__c = 'Action';
          insert objCatGenre1;
          
          //Create Promotion Catalog Record
          HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
          HEP_Promotion_Catalog__c objDHEPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objDHEPromotion.Id, null, true);
          
            List<HEP_SKU_Master__c> lstSkuMaster = new List<HEP_SKU_Master__c>();
           //Create SKU Request Record
            HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '45623', 'New SKU', 'Master', 'Digital', 'Rental', 'DVD', false);
            objSKU.Barcode__c = 'Barcode';
            objSKU.SKU_Compilation__c = 'SKUCompilation';
            objSKU.SKU_Permanent_Range__c ='NL (NL)';
            objSKU.Comments__c ='Test Comments';
            objSKU.SKU_Configuration__c = '4K';
            objSKU.Current_Release_Date__c = System.today();
            objSKU.INTL_Stock_in_Warehouse__c = 100;
            objSKU.Rental_Ready__c = 'RRD';
            lstSkuMaster.add(objSKU);
            
            HEP_SKU_Master__c objSKU1 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objDHETerritory.Id, '20973', 'DHE SKU', 'Request', 'Digital', 'Rental', 'DVD', false);
            objSKU1.Barcode__c = 'Barcode';
            objSKU1.SKU_Compilation__c = 'SKUCompilation';
            objSKU1.SKU_Permanent_Range__c ='NL (NL)';
            objSKU1.Comments__c ='Test Comments';
            objSKU1.SKU_Configuration__c = '4K';
            objSKU1.Current_Release_Date__c = System.today();
            objSKU1.INTL_Stock_in_Warehouse__c = 100;
            objSKU1.Rental_Ready__c = 'RRD';
            lstSkuMaster.add(objSKU1);
            
            HEP_SKU_Master__c objSKU2 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '2334', 'New SKU Ger', 'Request', 'Digital', 'Rental', 'DVD', false);
            objSKU2.Barcode__c = '';
            objSKU2.SKU_Compilation__c = '';
            objSKU2.SKU_Permanent_Range__c ='';
            objSKU2.Comments__c ='Test Comments';
            objSKU2.SKU_Configuration__c = '4k+DD+3D';
            objSKU2.Current_Release_Date__c = System.today();
            objSKU2.INTL_Stock_in_Warehouse__c = 100;
            objSKU2.Rental_Ready__c = 'RRD';
            lstSkuMaster.add(objSKU2);
            
            HEP_SKU_Master__c objSKU3 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, 'D0134JKFI', 'New SKU Ger', 'Request', 'Digital', 'Rental', 'DVD', false);
            objSKU3.Barcode__c = '';
            objSKU3.SKU_Compilation__c = '';
            objSKU3.SKU_Permanent_Range__c ='';
            objSKU3.Comments__c ='Test Comments';
            objSKU3.SKU_Configuration__c = '4k+DD+3D';
            objSKU3.Current_Release_Date__c = System.today();
            objSKU3.INTL_Stock_in_Warehouse__c = 100;
            objSKU3.Rental_Ready__c = 'RRD';
            lstSkuMaster.add(objSKU3);
            
            insert lstSkuMaster;
                
            //Create Promotion SKU record
            HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,'Draft',HEP_Utility.getConstantValue('HEP_STATUS_SEND_FOR_APPROVAL'), true);  
            HEP_Promotion_SKU__c objDHEPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objDHEPromoCatalog.Id, objSKU1.Id,'Draft',HEP_Utility.getConstantValue('HEP_STATUS_SEND_FOR_APPROVAL'), true);
            HEP_Promotion_SKU__c objGerPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU2.Id,'Draft',HEP_Utility.getConstantValue('HEP_STATUS_SEND_FOR_APPROVAL'), true);
            HEP_Promotion_SKU__c objReqPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU3.Id,'Draft',HEP_Utility.getConstantValue('HEP_STATUS_SEND_FOR_APPROVAL'), true);
            //Create Price Grade
            HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGerTerritory.Id,'$20-$30', 54, true);
            //SKU price 
            HEP_SKU_Price__c objSKUPrice = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                                SKU_Master__c = objSKU.Id,
                                                                Region__c = objGerTerritory.Id,
                                                                Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                                Record_Status__c ='Active');
            insert objSKUPrice;
            
             HEP_SKU_Price__c objSKUPrice1 = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                                SKU_Master__c = objSKU2.Id,
                                                                Region__c = objGerTerritory.Id,
                                                                Promotion_SKU__c = objGerPromotionSKU.Id,                                                        
                                                                Record_Status__c ='Active');
            insert objSKUPrice1;
            
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objGerTerritory.Id, '123', 'SKU Customers', '',true);
            HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id, objSKU.Id, true);
            
            Test.startTest();  
            HEP_SKU_Utility.autoApprovalSubmissionInLockPeriod(new Set<Id>{objDHEPromotionSKU.Id});
            HEP_CheckRecursive.clearSetIds();
            HEP_SKU_Utility.autoApprovalSubmissionInLockPeriod(new Set<Id>{objPromotionSKU.Id}); 
            HEP_CheckRecursive.clearSetIds();
            HEP_SKU_Utility.autoApprovalSubmissionInLockPeriod(new Set<Id>{objGerPromotionSKU.Id});
            HEP_SKU_Utility.updatePriceRegionsJDEFlag(new Set<Id>{objPromotionSKU.Id}); 
            HEP_CheckRecursive.clearSetIds();
            HEP_SKU_Utility.swapRequestedSkuToMaster(new Map<Id,Id>{objSKU.Id => objSKU3.Id});
            Test.stopTest();
        }
    }
    
    static TestMethod void testSaveSkuData() {
    
      HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
      objInterface.Name = 'HEP_E1_SpendDetail';
      Update objInterface;
      List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'AppTest'];
      List < HEP_Promotion__c > lstPromotion = [SELECT Id, LocalPromotionCode__c, Unique_Id__c, Territory__c FROM HEP_Promotion__c WHERE PromotionName__c = 'The Darkest Mind-Global'];
      List < HEP_Territory__c > lstTerritory = [SELECT Id,Name FROM HEP_Territory__c Order By Name];
        List < HEP_Catalog__c > lstCatalog = [SELECT Id, Unique_Id__c FROM HEP_Catalog__c WHERE CatalogId__c = '52560'
            OR CatalogId__c = '52460'
            ORDER BY CatalogId__c DESC
        ];
        String sDayOfStartDate = HEP_SKU_Utility.readableDay(System.Today());
        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', lstPromotion[0].Territory__c, lstPromotion[0].Territory__c, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
         VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Release Date', lstPromotion[0].Territory__c, lstPromotion[0].Territory__c, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
         ESTRelease.Media_Type__c = 'Digital';
        insert ESTRelease;
        HEP_Promotions_DatingMatrix__c PhysicalRetailRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'Physical Release Date (Retail)', lstPromotion[0].Territory__c, lstPromotion[0].Territory__c, false);
         PhysicalRetailRelease.Media_Type__c = 'Physical';
        PhysicalRetailRelease.Projected_FAD_Logic__c = 'Monday';
        insert PhysicalRetailRelease;
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'VOD Avail Date', lstPromotion[0].Territory__c, lstPromotion[0].Territory__c, false);
         VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset__c = 30;
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
        HEP_Promotions_DatingMatrix__c ESTAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'EST Avail Date', lstPromotion[0].Territory__c, lstPromotion[0].Territory__c, false);
         ESTAvail.Media_Type__c = 'Digital';
        ESTAvail.Date_Offset__c = 30;
        ESTAvail.Date_Offset_Source__c = 'EST Release Date';
        insert ESTAvail;
        
         
        //Create dating records 
        Set<Id> setPromotionDatingIds = new Set<Id>();
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(lstPromotion[0].Territory__c, lstPromotion[0].Id, VODRelease.id, false);
        DatingRec1.HEP_Catalog__c = lstCatalog[0].Id;
        DatingRec1.Status__c = 'Tentative';
        insert DatingRec1;
        setPromotionDatingIds.add(DatingRec1.id);
        HEP_Promotion_Dating__c DatingRec2 = HEP_Test_Data_Setup_Utility.createDatingRecord(lstPromotion[0].Territory__c, lstPromotion[0].Id, ESTRelease.id, false);
       
        DatingRec2.Status__c = 'Projected';
        insert DatingRec2;
        setPromotionDatingIds.add(DatingRec2.id);
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(lstPromotion[0].Territory__c, lstPromotion[0].Id, PhysicalRetailRelease.id, false);
        DatingRec3.Status__c = 'Projected';
        insert DatingRec3;
        setPromotionDatingIds.add(DatingRec3.id);
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(lstPromotion[0].Territory__c, lstPromotion[0].Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Projected';
        insert DatingRec4;
        setPromotionDatingIds.add(DatingRec4.id);
        HEP_Promotion_Dating__c DatingRec5 = HEP_Test_Data_Setup_Utility.createDatingRecord(lstPromotion[0].Territory__c, lstPromotion[0].Id, ESTAvail.id, false);
        DatingRec5.Status__c = 'Projected';
        insert DatingRec5;
        setPromotionDatingIds.add(DatingRec5.id);
        List < HEP_SKU_Master__c > lstSku = [SELECT Id, Unique_Id__c,Media_Type__c,Catalog_Master__c FROM HEP_SKU_Master__c WHERE SKU_Number__c = '12345'
            OR SKU_Number__c = '123457'
            ORDER BY SKU_Number__c DESC
        ];
        
        String catId = lstSku[0].Catalog_Master__c;
        lstSku[0].Catalog_Master__c = lstSku[1].Catalog_Master__c;
        lstSku[1].Catalog_Master__c = catId;
        update lstSku;
        List < HEP_SKU_Price__c > lstPrice = [SELECT Id, PriceGrade__c, Region__c FROM HEP_SKU_Price__c WHERE SKU_Master__r.SKU_Number__c = '12345'];
        HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
        objPromotionCatalog.Promotion__c = lstPromotion[0].Id;
        objPromotionCatalog.Catalog__c = lstCatalog[1].Id;
        objPromotionCatalog.Unique_Id__c = lstPromotion[0].LocalPromotionCode__c + ' / ' + lstCatalog[1].Unique_Id__c;
        objPromotionCatalog.Record_Status__c = 'Active';
        insert objPromotionCatalog;
        
        HEP_List_Of_Values__c objlov= new HEP_List_Of_Values__c();
        objlov.Name = 'Dreamworks - New Release';
        objlov.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov.Parent_Value__c = 'DHE';
        objlov.Values__c = '0';
        objlov.Record_Status__c = 'Active';
        insert objlov;
        
        HEP_List_Of_Values__c objlov1= new HEP_List_Of_Values__c();
        objlov1.Name = 'Dreamworks - New Release';
        objlov1.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov1.Parent_Value__c = 'Canada';
        objlov1.Values__c = '0';
        objlov1.Record_Status__c = 'Active';
        insert objlov1;
        HEP_List_Of_Values__c objlov2= new HEP_List_Of_Values__c();
        objlov2.Name = 'Dreamworks - New Release';
        objlov2.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov2.Parent_Value__c = 'Australia1';
        objlov2.Values__c = '0';
        objlov2.Record_Status__c = 'Active';
        insert objlov2;
        
        List<HEP_List_Of_Values__c> lstValues = [select Parent_Value__c,
                Type__c,
                Values__c,
                Name
                FROM HEP_List_Of_Values__c
                WHERE
                Record_Status__c ='Active' and
                Type__c = 'SKU_PRICE_OFFSET_PHYSICAL' and
                Name =  'Dreamworks - New Release'];
                
                System.debug('-------------------->' + lstTerritory);
        System.debug('-------------------->' + lstSku);
        System.debug('-------------------->' + lstValues);
         List < HEP_SKU_Utility.SKUWrapper > lstPriceGradeUpdate = new List < HEP_SKU_Utility.SKUWrapper > ();  
         for (HEP_SKU_Price__c objSkuPriceRecord: [SELECT Id, PriceGrade__c, Promotion_SKU__c,Promotion_Dating__c, Promotion_SKU__r.SKU_Master__c, Promotion_SKU__r.Promotion_Catalog__r.Catalog__r.CatalogId__c,Promotion_SKU__r.HEP_Promotion__c FROM HEP_SKU_Price__c WHERE Promotion_Dating__c IN: setPromotionDatingIds]) {
                HEP_SKU_Utility.SKUWrapper objSkuWrapper = new HEP_SKU_Utility.SKUWrapper();
                objSkuWrapper.sPromoId = objSkuPriceRecord.Promotion_SKU__r.HEP_Promotion__c;
                objSkuWrapper.sCatalogId = objSkuPriceRecord.Promotion_SKU__r.Promotion_Catalog__r.Catalog__r.CatalogId__c;
                objSkuWrapper.sSKUId = objSkuPriceRecord.Promotion_SKU__r.SKU_Master__c;
                objSkuWrapper.sPromotionSKUID = objSkuPriceRecord.Promotion_SKU__c;
                objSkuWrapper.sSKUPriceId = objSkuPriceRecord.Id;
                lstPriceGradeUpdate.add(objSkuWrapper);
            }
        if(!lstPriceGradeUpdate.isEmpty()){
                System.debug('lstPriceGrade Records Tagged to Updated Dating Records ---->' + lstPriceGradeUpdate);         
                HEP_SKU_Utility.defaultSKUPriceRules(lstPriceGradeUpdate, HEP_Utility.getConstantValue('HEP_EDIT_DATING'));
            }
        String sSaveJson = '[{"bActive":true,"lstPriceRegion":[{"sDLP":"31.50","sPriceGrade":"$44.95 - $31.50","sPriceGradeId":"' + lstPrice[0].PriceGrade__c + '","sRegion":"Australia","sSkuId":"' + lstSku[1].Id + '","sTerritoryId":"' + lstPrice[0].Region__c + '","sUnits":"314511"}],"sCatalogId":"' + lstCatalog[0].Id + '","sCatalogName":"ARTHUR","sCatalogNumber":"52560","sCatalogProductType":"FILM","sCatalogUniqueId":"52560 / Australia","sChannel":"RETAIL","sFormat":"BD","sLicensor":"FOX","sPromotionName":"","sRegion":"Australia","sRevenueShare":"No","sSameBaseUPC":"No","sSku":"12345","sSkuConfiguration":"3D_DD","sSkuId":"' + lstSku[1].Id + '","sSkuName":"Retail Blu-ray Disc 3D + DD","sSkuUniqueId":"12345 / Australia","sType":"Physical"},{"bActive":true,"lstPriceRegion":[{"sDLP":"31.50","sPriceGrade":"$44.95 - $31.50","sPriceGradeId":"' + lstPrice[0].PriceGrade__c + '","sRegion":"Australia","sSkuId":"' + lstSku[0].Id + '","sTerritoryId":"' + lstTerritory[2].Id + '","sUnits":"314511"}],"sBarcode":"12345","sCatalogId":"' + lstCatalog[1].Id + '","sCatalogName":"ARTHUR1","sCatalogNumber":"52460","sCatalogUniqueId":"52460 / Australia","sChannel":"VOD","sCustomers":"Flipkart","sFormat":"HD","sPromotionName":"","sRegion":"Australia","sRevenueShare":"No","sSameBaseUPC":"No","sSku":"123457","sSkuCompilation":"TV","sSkuConfiguration":"2D","sSkuId":"' + lstSku[0].Id + '","sSkuName":"The Age of empire1","sSkuPermanentRange":"4K","sSkuUniqueId":"123457 / Australia","sType":"Digital"}]';
        System.runAs(lstUser[0]) {
            Test.startTest();
            HEP_SKU_Utility.SKUWrapper objSKuWrapper = new HEP_SKU_Utility.SKUWrapper(lstPromotion[0].Id,lstCatalog[0].Id,'Test','Test','Test');
            HEP_SKU_Utility.SKUPriceWrapper objSkuPriceWrap = new HEP_SKU_Utility.SKUPriceWrapper(Date.newInstance(1990, 09, 20), 'TestData',Date.newInstance(1990, 09, 20), 'TestData', 'TestData', 'TestData', 'TestData', false,'TestData', false, 'TestData','TestApproval');
            
            List < String > lstResults = HEP_Add_Existing_SKU_Controller.saveSkuData(sSaveJson, lstPromotion[0].Id, false, lstPromotion[0].Unique_Id__c);
            System.assertEquals(0, lstResults.size());
            Test.stopTest();

        }
    }
    
    
    static TestMethod void testSaveSkuData1() {
        
      List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'AppTest'];
      List < HEP_Promotion__c > lstPromotion = [SELECT Id, LocalPromotionCode__c, Unique_Id__c, Territory__c FROM HEP_Promotion__c WHERE PromotionName__c = 'The Darkest Mind-Global'];
      List < HEP_Territory__c > lstTerritory = [SELECT Id,Name FROM HEP_Territory__c Order By Name];
        List < HEP_Catalog__c > lstCatalog = [SELECT Id, Unique_Id__c FROM HEP_Catalog__c WHERE CatalogId__c = '52560'
            OR CatalogId__c = '52460'
            ORDER BY CatalogId__c DESC
        ];
        List < HEP_SKU_Master__c > lstSku = [SELECT Id, Unique_Id__c,Media_Type__c,Catalog_Master__c FROM HEP_SKU_Master__c WHERE SKU_Number__c = '12345'
            OR SKU_Number__c = '123457'
            ORDER BY SKU_Number__c DESC
        ];
        
        String catId = lstSku[0].Catalog_Master__c;
        lstSku[0].Catalog_Master__c = lstSku[1].Catalog_Master__c;
        lstSku[1].Catalog_Master__c = catId;
        update lstSku;
        List < HEP_SKU_Price__c > lstPrice = [SELECT Id, PriceGrade__c, Region__c FROM HEP_SKU_Price__c WHERE SKU_Master__r.SKU_Number__c = '12345'];
        HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
        objPromotionCatalog.Promotion__c = lstPromotion[0].Id;
        objPromotionCatalog.Catalog__c = lstCatalog[1].Id;
        objPromotionCatalog.Unique_Id__c = lstPromotion[0].LocalPromotionCode__c + ' / ' + lstCatalog[1].Unique_Id__c;
        objPromotionCatalog.Record_Status__c = 'Active';
        insert objPromotionCatalog;
        
        HEP_List_Of_Values__c objlov= new HEP_List_Of_Values__c();
        objlov.Name = 'Dreamworks - New Release';
        objlov.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov.Parent_Value__c = 'Australia';
        objlov.Values__c = '0';
        objlov.Record_Status__c = 'Active';
        insert objlov;
        
        HEP_List_Of_Values__c objlov1= new HEP_List_Of_Values__c();
        objlov1.Name = 'Dreamworks - New Release';
        objlov1.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov1.Parent_Value__c = 'Canada';
        objlov1.Values__c = '0';
        objlov1.Record_Status__c = 'Active';
        insert objlov1;
        HEP_List_Of_Values__c objlov2= new HEP_List_Of_Values__c();
        objlov2.Name = 'Dreamworks - New Release';
        objlov2.Type__c = 'SKU_PRICE_OFFSET_PHYSICAL';
        objlov2.Parent_Value__c = 'Australia1';
        objlov2.Values__c = '0';
        objlov2.Record_Status__c = 'Active';
        insert objlov2;
        
        List<HEP_List_Of_Values__c> lstValues = [select Parent_Value__c,
                Type__c,
                Values__c,
                Name
                FROM HEP_List_Of_Values__c
                WHERE
                Record_Status__c ='Active' and
                Type__c = 'SKU_PRICE_OFFSET_PHYSICAL' and
                Name =  'Dreamworks - New Release'];
                
                System.debug('-------------------->' + lstTerritory);
        System.debug('-------------------->' + lstSku);
        System.debug('-------------------->' + lstValues);
         List < HEP_SKU_Utility.SKUWrapper > lstPriceGradeUpdate = new List < HEP_SKU_Utility.SKUWrapper > ();  

        String sSaveJson = '[{"bActive":true,"lstPriceRegion":[{"sDLP":"31.50","sPriceGrade":"$44.95 - $31.50","sPriceGradeId":"' + lstPrice[0].PriceGrade__c + '","sRegion":"Australia","sSkuId":"' + lstSku[1].Id + '","sTerritoryId":"' + lstPrice[0].Region__c + '","sUnits":"314511"}],"sCatalogId":"' + lstCatalog[0].Id + '","sCatalogName":"ARTHUR","sCatalogNumber":"52560","sCatalogProductType":"FILM","sCatalogUniqueId":"52560 / Australia","sChannel":"RETAIL","sFormat":"BD","sLicensor":"FOX","sPromotionName":"","sRegion":"Australia","sRevenueShare":"No","sSameBaseUPC":"No","sSku":"12345","sSkuConfiguration":"3D_DD","sSkuId":"' + lstSku[1].Id + '","sSkuName":"Retail Blu-ray Disc 3D + DD","sSkuUniqueId":"12345 / Australia","sType":"Physical"},{"bActive":true,"lstPriceRegion":[{"sDLP":"31.50","sPriceGrade":"$44.95 - $31.50","sPriceGradeId":"' + lstPrice[0].PriceGrade__c + '","sRegion":"Australia","sSkuId":"' + lstSku[0].Id + '","sTerritoryId":"' + lstTerritory[2].Id + '","sUnits":"314511"}],"sBarcode":"12345","sCatalogId":"' + lstCatalog[1].Id + '","sCatalogName":"ARTHUR1","sCatalogNumber":"52460","sCatalogUniqueId":"52460 / Australia","sChannel":"VOD","sCustomers":"Flipkart","sFormat":"HD","sPromotionName":"","sRegion":"Australia","sRevenueShare":"No","sSameBaseUPC":"No","sSku":"123457","sSkuCompilation":"TV","sSkuConfiguration":"2D","sSkuId":"' + lstSku[0].Id + '","sSkuName":"The Age of empire1","sSkuPermanentRange":"4K","sSkuUniqueId":"123457 / Australia","sType":"Digital"}]';
        System.runAs(lstUser[0]) {
            Test.startTest();
            HEP_SKU_Utility.SKUWrapper objSKuWrapper = new HEP_SKU_Utility.SKUWrapper(lstPromotion[0].Id,lstCatalog[0].Id,'Test','Test','Test');
            HEP_SKU_Utility.SKUPriceWrapper objSkuPriceWrap = new HEP_SKU_Utility.SKUPriceWrapper(Date.newInstance(1990, 09, 20), 'TestData',Date.newInstance(1990, 09, 20), 'TestData', 'TestData', 'TestData', 'TestData', false,'TestData', false, 'TestData','TestApproval');
            
            List < String > lstResults = HEP_Add_Existing_SKU_Controller.saveSkuData(sSaveJson, lstPromotion[0].Id, false, lstPromotion[0].Unique_Id__c);
            System.assertEquals(0, lstResults.size());
            Test.stopTest();

        }
    }
    
    /*static TestMethod void testgenerateSmartSKUUnique() {
        //Fetch the created Test User:
        User objTestUser = [Select  Id,
                                    Name 
                                    from User 
                                    where Email = 'ApprovalTestUser@hep.com'];
        
        //fetch promotion:
        HEP_Promotion__c objTestPromotion = [Select Id,
                                                    Territory__c,
                                                    Name 
                                                    from HEP_Promotion__c 
                                                    where Promotion_Type__c ='National'];
        
        HEP_Territory__c objGerTerritory = [Select Id,
                                                Name 
                                                from HEP_Territory__c
                                                where Name = 'Germany'];
        
        //Run in the context of User:
        System.runAs(objTestUser){
            
          //Create Catalog Record
          HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
          objCatalog.Licensor_Type__c = 'Lucas';
          objCatalog.Licensor__c = 'ABLO';
          insert objCatalog;
          
          HEP_Catalog_Genre__c objCatGenre1 = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
          objCatGenre1.Genre__c = 'Action';
          insert objCatGenre1;
          
          HEP_Catalog_Genre__c objCatGenre2 = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
          objCatGenre2.Genre__c = 'Drama';
          insert objCatGenre2;
          
          //Create Promotion Catalog Record
          HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
           
           //Create SKU Request Record
            HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '456', 'New SKU', 'Request', 'Digital', 'Rental', 'DVD', false);
            objSKU.Barcode__c = 'Barcode';
            objSKU.SKU_Compilation__c = 'SKUCompilation';
            objSKU.SKU_Permanent_Range__c ='NL (NL)';
            objSKU.Comments__c ='Test Comments';
            objSKU.SKU_Configuration__c = 'Dig_Ins';
            objSKU.Current_Release_Date__c = System.today();
            objSKU.INTL_Stock_in_Warehouse__c = 100;
            objSKU.Rental_Ready__c = 'RRD';
            insert objSKU;
            
            
    
            //Create Promotion SKU record
            HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,'Draft','Locked', true);  
            //Create Price Grade
            HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGerTerritory.Id,'$20-$30', 54, true);
            //SKU price 
            HEP_SKU_Price__c objSKUPrice = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                                SKU_Master__c = objSKU.Id,
                                                                Region__c = objGerTerritory.Id,
                                                                Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                                Record_Status__c ='Active');
            insert objSKUPrice;
            //Invoke Approval Process
            Test.startTest();
            HEP_Generate_SKU_Number.generateUniqueSKUNumber(new Set<Id>{objSKU.Id});
            Test.stopTest();
        }
    }*/

}

//D001234URP