/**
 *HEP_Invoice_DetailsPage_Controller - to process the Invoices to be displayed on Invoice Details Page
 *@author : Ashutosh Arora
 */

public class HEP_Invoice_DetailsPage_Controller {

    public String sCurrencyFormat {get;set;}
    public String sTerritoryId {get;set;}
    /**
     *HEP_Invoice_DetailsPage_Controller - Constructor to initialize values 
     *@author : Ashutosh Arora
     */
    public HEP_Invoice_DetailsPage_Controller() {
        // Extracting all the matching promotions
        list < HEP_Promotion__c > lstPromotion = new list < HEP_Promotion__c > ();
        //bToastMessage=true;
        lstPromotion = [SELECT id, createdDate, lastModifiedDate, Territory__c, Promotion_Type__c, Territory__r.CurrencyCode__c
            FROM HEP_Promotion__c
            WHERE   Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND id =: apexpages.currentpage().getparameters().get('promoId')
        ];
        //Currency Format
        if (lstPromotion != null && lstPromotion.size() > 0 && lstPromotion[0].Territory__r!=null) {
            sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(lstPromotion[0].Territory__r.CurrencyCode__c);
            sTerritoryId = lstPromotion[0].Territory__c;
        }
    }
    /**
     *InvoiceDetails - To process the Invoice data to be displayed on Invoice Details Page
     *@return InvoiceDetails (InvoiceDetails Wrapper with Invoice and respective Line Data)
     *@author Ashutosh Arora
     */
    
    public PageReference checkPermissions(){
        map<String,String> mapCustomPermissions = new map<String,String>();
        mapCustomPermissions = HEP_Utility.fetchCustomPermissions(sTerritoryId, HEP_Utility.getConstantValue('HEP_PO_Page'));
        if(mapCustomPermissions.get('READ').equalsIgnoreCase('FALSE')){
            return HEP_Utility.redirectToErrorPage();
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    public static InvoiceDetails getInvoiceDetails(String sPromotionId, String sInvoiceNumber, string sDepartmentId) {
        //if (sInvoiceNumber != null && sDepartmentID != null) {
            InvoiceDetails objInvoiData = new InvoiceDetails();
            List < HEP_Promotion__c > lstCurrencies = new List<HEP_Promotion__c>();
            String sNameDepartment;
            List<HEP_GL_Account__c> lstGLAccountId = new List<HEP_GL_Account__c>();
            if(sDepartmentId != null){
                lstGLAccountId = [SELECT Id, Account_Department__c 
                                    FROM HEP_GL_Account__c 
                                    WHERE ID =: sDepartmentId];
                sNameDepartment = lstGLAccountId[0].Account_Department__c;
            }
            String sCurrencyCode;
            //To get String response stored on the Promotion 
            String sResponseonPromotion;
            String sIdPromotion = sPromotionId.trim();
            //TO get the currency code from the Territory of Promotion
            lstCurrencies = [SELECT Territory__r.CurrencyCode__c FROM HEP_Promotion__c where   Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND id =: sIdPromotion];
            if (lstCurrencies != null && lstCurrencies.size() > 0 && lstCurrencies[0].Territory__r != NULL) {
                sCurrencyCode = lstCurrencies[0].Territory__r.CurrencyCode__c;
            }
            if (sIdPromotion != null) {
                sResponseonPromotion = HEP_Integration_Util.retriveDatafromContent(sIdPromotion, HEP_Constants__c.getValues('HEP_PO_INVOICE_RESPONSE').Value__c);
            }
            HEP_InvoicePurchaseOrderData objInvoiceData = new HEP_InvoicePurchaseOrderData();
            if (sResponseonPromotion != null) {
                //Deserialize the String response into HEP_InvoicePurchaseOrderData format
                objInvoiceData = (HEP_InvoicePurchaseOrderData) JSON.deserialize(sResponseonPromotion, HEP_InvoicePurchaseOrderData.class);
                System.debug('Serialized Data : ' +objInvoiceData);
            }
            if (sNameDepartment != null && objInvoiceData != null) {
                for (HEP_InvoicePurchaseOrderData.Department objDepart: objInvoiceData.lstDepartments) {
                    if (objDepart.sDepartmentName.equals(sNameDepartment)) {
                        if(objDepart.lstInvoices!=null){
                            for (HEP_InvoicePurchaseOrderData.ParentNode invData: objDepart.lstInvoices) {
                                //To filter out on the invoices to be displayed
                                if (invData.sRequestNumber == sInvoiceNumber) {
                                    Decimal dAmount = 0;
                                    System.debug('*****'+invData.lstLines);
                                    system.debug('objDepart.lstInvoices' +objDepart.lstInvoices);
                                    objInvoiData.sVendorName = invData.sVendorName;
                                    objInvoiData.sInvoiceNumber = invData.sRequestNumber;
                                    objInvoiData.sRequestNumber = invData.sRequestNumber;
                                    objInvoiData.sStatus = invData.sStatus;
                                    objInvoiData.dtIssuedDate = invData.dtIssueDate;
                                    //objInvoiData.dAmount = invData.dAmount;
                                    objInvoiData.sCurrency = sCurrencyCode;
                                    for (HEP_InvoicePurchaseOrderData.Line objLineWrapper: invData.lstLines) {
                                        LineData objLine = new LineData();
                                        objLine.sLineNumber = objLineWrapper.sLineNumber;
                                        objLine.sGLAccount = objLineWrapper.sGLAccount;
                                        objLine.sFormat = objLineWrapper.sFormat;
                                        objLine.dActuals = objLineWrapper.dActuals;
                                        objLine.sCountry = objLineWrapper.sCountry;
                                        objLine.sPONumber = invData.sPONumber;
                                        objLine.sInvoiceLineDesription = objLineWrapper.sLineDescription;
                                        objInvoiData.lstLines.add(objLine);
                                        dAmount+=objLineWrapper.dActuals; 
                                    }
                                    objInvoiData.dAmount =dAmount; 
                                    break;
                                }
                            }
                        }
                        
                    }
                }
            } else {
                return null;
            }
            system.debug('objInvoiData' + objInvoiData);
            return objInvoiData;
        /*} else {
            return null;
        }*/
    }
    /**
     *InvoiceDetails - Wrapper for the top level header values
     *@author : Ashutosh Arora
     */
    Public Class InvoiceDetails {
        String sInvoiceNumber;
        String sVendorName;
        String sStatus;
        String sRequestNumber;
        Date dtIssuedDate;
        Decimal dAmount;
        String sCurrency;
        list < LineData > lstLines;
        /**
         *LineData - Constructor to initialize values
         *@author : Ashutosh Arora
         */
        public InvoiceDetails() {
            lstLines = new list < LineData > ();
        }

    }
    /**
     *LineData - Wrapper for Line data relating to a Invoice
     *@author : Ashutosh Arora
     */
    Public class LineData {
        String sLineNumber;
        String sGLAccount;
        String sFormat;
        String sInvoiceLineDesription;
        String sPONumber;
        Decimal dAmount;
        Decimal dActuals;
        String sCountry;

    }

}