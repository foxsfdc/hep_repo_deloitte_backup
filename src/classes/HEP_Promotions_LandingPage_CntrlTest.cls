@isTest(SeeAllData=false)
private class HEP_Promotions_LandingPage_CntrlTest{


    static testmethod void loadPromotionstest()
    {
        //setupData();
        Test.startTest();
        String sSelectedTab = 'My Promotions';
        HEP_Promotions_LandingPage_Controller.promotionDashboardFilterWrapper promotionsfilterWrap  =new HEP_Promotions_LandingPage_Controller.promotionDashboardFilterWrapper();
        Integer imaxRecCount = 25;
        String sLocaleDateFormat = 'dd-MMM-yyyy';
        HEP_Promotions_LandingPage_Controller.loadPromotions('',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat, true);
        Test.stopTest();
    }
    static testmethod void fetchFilteredCustomerPromotionsTest()
    {
        //setupData();
        Test.startTest();
        String sSelectedTab = 'My Promotions';
        HEP_Promotions_LandingPage_Controller.promotionDashboardFilterWrapper promotionsfilterWrap  =new HEP_Promotions_LandingPage_Controller.promotionDashboardFilterWrapper();
        List<HEP_Utility.MultiPicklistWrapper> test11 = new List<HEP_Utility.MultiPicklistWrapper> ();
        HEP_Utility.MultiPicklistWrapper test1 = new HEP_Utility.MultiPicklistWrapper('Russia','Russia',true);
        test11.add(test1);
        promotionsfilterWrap.digitalAccountManagerMultiPicklist = test11;
        promotionsfilterWrap.customersMultiPicklist = test11;
        promotionsfilterWrap.customerPromotionStatusMultiPicklist = test11;
        promotionsfilterWrap.territoriesMultiPicklist = test11;
        promotionsfilterWrap.localMarketingManagersMultiPicklist = test11;
        promotionsfilterWrap.showAllFutureFads = true;
        promotionsfilterWrap.showAllFadsPast365 = true;
        promotionsfilterWrap.showGlobalPromotionLinked = true;
        promotionsfilterWrap.showFavoritedPromotions = true;
        Integer imaxRecCount = 25;
        String sLocaleDateFormat = 'dd-MMM-yyyy';
        HEP_Promotions_LandingPage_Controller.FetchFilteredCustomerPromotions('my promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat);
        HEP_Promotions_LandingPage_Controller.FetchFilteredCustomerPromotions('upcoming customer promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat);
        HEP_Promotions_LandingPage_Controller.FetchFilteredCustomerPromotions('live customer promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat);
        HEP_Promotions_LandingPage_Controller.FetchFilteredPromotions('live customer promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat, true);
        HEP_Promotions_LandingPage_Controller.FetchFilteredPromotions('my promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat, true);
        promotionsfilterWrap.showAllFutureFads = false;
        HEP_Promotions_LandingPage_Controller.FetchFilteredPromotions('my promotions',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat, true);
        HEP_Promotions_LandingPage_Controller.FetchFilteredPromotions('new release',promotionsfilterWrap,imaxRecCount,sLocaleDateFormat, true);
        
       
        Test.stopTest();
        
    }
    static testmethod void getJwtTokenTest()
    {
        //setupData();
        Test.startTest();
        HEP_Promotions_LandingPage_Controller.getJwtToken();
        Test.stopTest();
    }
    static testmethod void setUserFavoriteTest()
    {
        //setupData();
        Test.startTest();
        List<HEP_Promotion__c> promotionList = [SELECT Id,Promotion_Type__c,FirstAvailableDateFormula__c,MinPromotionDt_Date__c,
                                                Territory__r.Name,Territory__c,LineOfBusiness__r.Type__c,Customer__r.CustomerName__c,Submitted_Date__c,Requestor__r.Name,Status__c
                                                ,Priority__c,Territory__r.Flag_Country_Code__c,Domestic_Marketing_Manager__r.Name,International_Marketing_Manager__r.Name,PromotionName__c
                                                ,Promotion_Image_URL__c,Title__c,Customer_Promotion_Status__c,
                                                LocalPromotionCode__c,StartDate__c,EndDate__c FROM HEP_Promotion__c];
        set<String> promoid = new set<String>();
        if(promotionList != null && promotionList.Size()>0){
            for(HEP_Promotion__c promo:promotionList){
                promoid.add(promo.ID);
            }
        }
        map<id,HEP_Title_Asset__c> newAssettitle= new map<id,HEP_Title_Asset__c>();
        HEP_Promotions_LandingPage_Controller.setUserFavorite(promotionList.get(0).id);
        HEP_Promotions_LandingPage_Controller.setUserFavorite(promotionList.get(0).id);
        HEP_Promotions_LandingPage_Controller.BuildPromotionsWrapper(promotionList,newAssettitle);
        HEP_Promotions_LandingPage_Controller.FetchCustomersPicklistValues(promoid);
        HEP_Promotions_LandingPage_Controller.FetchlocalMarketingManagersPicklistValues(promoid);
        Test.stopTest();
        
    }
    static testmethod void compareToTest()
    {
        //setupData();
        Test.startTest();
        HEP_Promotions_LandingPage_Controller.promotionsWrapper promoWrapper = new HEP_Promotions_LandingPage_Controller.promotionsWrapper();
        promoWrapper.compareTo(promoWrapper);
        promoWrapper.SortByApprovalStatusCustomerPromotions(promoWrapper);
        Test.stopTest();
        
    }
        
    @testSetup
    static void setupData()
    {
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
            insert objTitle;
        String sTerritoryName = 'Russia';
        String sRegion = 'APAC';
        String sType = 'Global';
        String sParentId = null;
        String sCatalogTerritoryId = null;
        String sCurrencyCode = 'INR';
        String sTerritoryCodeIntg = '10';
        String sERMTerrCode = '10';
        HEP_Territory__c objHEP_Territory = HEP_Test_Data_Setup_Utility.createHEPTerritory(sTerritoryName, sRegion,  sType,  sParentId,  sCatalogTerritoryId,  sCurrencyCode,  sTerritoryCodeIntg,  sERMTerrCode, '', true);
        
        objHEP_Territory.LegacyId__c = '1000';
        update objHEP_Territory;
        
        String sName = 'Dreamworks - TV';
        sType = 'New Release';
        HEP_Line_Of_Business__c LOBrec = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness(sName,sType,true);
        
        String sCustomerName = 'Test Customer';
        String sTerritoryId = objHEP_Territory.id;
        String sCustomerNumber = '10';
        String sCustomerType = 'MDP Customers';
        String sDateType = 'YYYY-MM-DD';
        String uID = '10';
        
        
        HEP_Customer__c  customerRec = HEP_Test_Data_Setup_Utility.createHEPCustomers( sCustomerName,  sTerritoryId,  sCustomerNumber,  sCustomerType,  sDateType, true);
        
        String sProfileName = 'System Administrator';
        String sLastName = 'User';
        String sEmail = 'test@testuser.com';
        String sUserName = 'test@testuser.com.hep.test';
        String sNickName = 'tuser';
        String sAliasName = 'tuser';
        String sLOB = 'Dreamworks - TV';
        User u = HEP_Test_Data_Setup_Utility.createUser(sProfileName, sLastName,  sEmail,  sUserName,  sNickName, sAliasName,   sLOB, true);
        
        String sPromoName = 'Test Promotion';
        String sPromoType = 'Global';
        sTerritoryId = objHEP_Territory.id;
        sLOB = LOBrec.id;
        String sCustId = customerRec.id;
        String sRequestorId = u.id;
        String sGlobalPromoId = null;
        
        HEP_Title_Asset__c titleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('Test','Asset',true);
        HEP_Promotion__c promotionRec = HEP_Test_Data_Setup_Utility.createPromotion( sPromoName,  sPromoType,  sGlobalPromoId,  sTerritoryId,  sLOB,sCustId,  sRequestorId, true);
        HEP_Promotion__c promotionRec1 = HEP_Test_Data_Setup_Utility.createPromotion( sPromoName,  'National',  sGlobalPromoId,  sTerritoryId,  sLOB,sCustId,  sRequestorId, true);
        HEP_Promotion__c promotionRec2 = HEP_Test_Data_Setup_Utility.createPromotion( sPromoName,  'Customer',  sGlobalPromoId,  sTerritoryId,  sLOB,sCustId,  sRequestorId, false);
        promotionRec2.Title__c = objTitle.ID;
        promotionRec2.Domestic_Marketing_Manager__c = userinfo.getUserId();
        insert promotionRec2;
    }



}