/**
 * HEP_MarketSpendTriggerHandler --- Handler class for HEP_MarketSpendTrigger
 * @author  Nidhin V K
 */
public class HEP_MarketSpendTriggerHandler extends TriggerHandler{
    
    /**
    * afterUpdate --- all after update logic
    * @param Map<Id, HEP_Market_Spend__c> oldMap, Map<Id, HEP_Market_Spend__c> newMap
    * @exception Any exception
    * @return void
    */ 
    public override void afterUpdate(){
        System.debug('in after update++++++++++++++');
        callQueueableForE1((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);
        //if(HEP_CheckRecursive.runOnce()){
            //creates the spend history records and update the current spend details
            createSpendHistory((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap, (Map<Id, HEP_Market_Spend__c>) Trigger.newMap);
        if(HEP_CheckRecursive.runOnce()){    
            //calls the call out class to send the details to JDE When the Budget request is approved and territory is DHE/US/Canada
            //triggerEnqueueJob((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);     
        }
        //Kunal - moving JDE integration outside the recursive check
        triggerEnqueueJob((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);     
    }

    /**
    * afterInsert --- all after insert logic
    * @param nothing
    * @exception Any exception
    * @return void
    */
     public override void afterInsert(){
        callQueueableForE1((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);         
        //calls the call out class to send the details to JDE When the Budget request is approved and territory is DHE/US/Canada
        if(HEP_CheckRecursive.runOnce()){
            //triggerEnqueueJob((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);
        }  
        //Kunal - moving JDE integration outside the recursive check
        triggerEnqueueJob((Map<Id, HEP_Market_Spend__c>) Trigger.oldMap);
        //HEP_CheckRecursive.clearSetIds(); 
     }
    
    /**
    * createSpendHistory --- creates the spend history records and update the current spend details
    * @param Map<Id, HEP_Market_Spend__c> oldMap, Map<Id, HEP_Market_Spend__c> newMap
    * @exception Any exception
    * @return void
    */
    public static void createSpendHistory(Map<Id, HEP_Market_Spend__c> oldMap, Map<Id, HEP_Market_Spend__c> newMap){
        if(oldMap == NULL || newMap == NULL)
            return;
        //select approved spend records
        Set<Id> setApprovedSpendIds = getValidSpendIds(oldMap, newMap, HEP_Constants__c.getValues('HEP_SPEND_BUDGET_APPROVED').Value__c);
        Set<Id> setRejectedSpendIds = getValidSpendIds(oldMap, newMap, HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c);
        if(setApprovedSpendIds.size() == 0 && setRejectedSpendIds.size() == 0 )
            return;
        System.debug('setApprovedSpendIds>>' + setApprovedSpendIds);    
        System.debug('setRejectedSpendIds>>' + setRejectedSpendIds);
        Set<Id> setAllSpendIds = new Set<Id>();
        setAllSpendIds.addAll(setApprovedSpendIds);
        setAllSpendIds.addAll(setRejectedSpendIds);
        
        String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        Savepoint objSavePoint = Database.setSavepoint();
        try{
            List<HEP_Market_Spend_Detail__c> lstHistorySpendDetails = new List<HEP_Market_Spend_Detail__c>();
            List<HEP_Market_Spend__c> lstSpends = new List<HEP_Market_Spend__c>();
            Map<Id, HEP_Promotion__c> mapPromotionsToUpdate = new Map<Id, HEP_Promotion__c>();
            //query all approved spend detail records
            //here we will create initial budget and history records for spend details
            for(HEP_Market_Spend__c objSpend : [SELECT
                                                    Id, Promotion__c,
                                                    (SELECT 
                                                        Id, RequestAmount__c, HEP_Market_Spend__c,
                                                        Budget__c, GL_Account__c, Type__c,
                                                        Comments__c, Requestor__c, Department__c
                                                    FROM
                                                        HEP_Market_Spend_Details__r
                                                    WHERE
                                                        Record_Status__c = :sACTIVE
                                                    ORDER BY
                                                        CreatedDate DESC)
                                                FROM
                                                    HEP_Market_Spend__c
                                                WHERE
                                                    Id IN :setAllSpendIds
                                                AND
                                                    Record_Status__c = :sACTIVE]){
                
                String sRequestType = '';
                if(objSpend.HEP_Market_Spend_Details__r.size() > 0){
                    //if its the first request, then on record approval
                    //store the record with type as Initial Budget
                    //else type will be Budget Update
                    HEP_Market_Spend_Detail__c objLatestSpendDetail = objSpend.HEP_Market_Spend_Details__r[0];    
                    System.debug('objLatestSpendDetail>>' + objLatestSpendDetail);
                    if(objLatestSpendDetail.Type__c.equals(HEP_Constants__c.getValues('HEP_SPEND_DETAIL_STATUS_CURRENT').Value__c))
                        sRequestType = HEP_Constants__c.getValues('HEP_SPEND_DETAIL_TYPE_INITIAL_BUDGET').Value__c;
                    else
                        sRequestType = HEP_Constants__c.getValues('HEP_SPEND_DETAIL_TYPE_BUDGET_UPDATE').Value__c;
                    
                    Boolean bIsFirstRejection = true;
                    for(HEP_Market_Spend_Detail__c objSpendDetail : objSpend.HEP_Market_Spend_Details__r){    
                        System.debug('objSpendDetail>>' + objSpendDetail);
                        //populate the history records from current spend detail and update the current spend detail 
                        if(objSpendDetail.Type__c.equals(HEP_Constants__c.getValues('HEP_SPEND_DETAIL_STATUS_CURRENT').Value__c)
                            && objSpendDetail.RequestAmount__c != objSpendDetail.Budget__c){
                            //if approved
                            if(setApprovedSpendIds.contains(objSpend.Id)){
                                HEP_Market_Spend_Detail__c objSpendDetailHistory = new HEP_Market_Spend_Detail__c();
                                objSpendDetailHistory.Type__c = sRequestType;
                                objSpendDetailHistory.Budget_Difference__c = objSpendDetail.RequestAmount__c - objSpendDetail.Budget__c;
                                objSpendDetailHistory.Comments__c = objSpendDetail.Comments__c;
                                objSpendDetailHistory.Requestor__c = objSpendDetail.Requestor__c;
                                objSpendDetailHistory.GL_Account__c = objSpendDetail.GL_Account__c;
                                objSpendDetailHistory.HEP_Market_Spend__c = objSpendDetail.HEP_Market_Spend__c;
                                lstHistorySpendDetails.add(objSpendDetailHistory);
                                //update the current spend detail Budget__c value with RequestAmount__c if it is approved
                                objSpendDetail.Budget__c = objSpendDetail.RequestAmount__c;
                                lstHistorySpendDetails.add(objSpendDetail);
                            } else{
                                //revert the current spend detail RequestAmount__c value with Budget__c on rejection
                                objSpendDetail.RequestAmount__c = objSpendDetail.Budget__c;
                                lstHistorySpendDetails.add(objSpendDetail);
                            }
                        } else if(!objSpendDetail.Type__c.Equals(HEP_Constants__c.getValues('HEP_SPEND_DETAIL_STATUS_CURRENT').Value__c)){
                            bIsFirstRejection = false;
                        }
                    }
                    System.debug('bIsFirstRejection>>' + bIsFirstRejection);
                    String sStatus;
                    if(bIsFirstRejection
                        && setRejectedSpendIds.contains(objSpend.Id))
                        sStatus = HEP_Constants__c.getValues('HEP_APPROVAL_REJECTED').Value__c;
                    else
                        sStatus = HEP_Constants__c.getValues('HEP_SPEND_BUDGET_APPROVED').Value__c;
                    //update promotion status and spend status                          
                    mapPromotionsToUpdate.put(objSpend.Id, new HEP_Promotion__c(Id = objSpend.Promotion__c, MarketSpendStatus__c = sStatus));
                    objSpend.RecordStatus__c = sStatus;
                    lstSpends.add(objSpend);
                }
            }
            System.debug('lstHistorySpendDetails>>' + lstHistorySpendDetails);
            if(lstHistorySpendDetails.size() > 0)
                upsert lstHistorySpendDetails;
            System.debug('lstSpends>>' + lstSpends);
            if(lstSpends.size() > 0)
                update lstSpends;
            System.debug('mapPromotionsToUpdate>>' + mapPromotionsToUpdate.values());
            if(mapPromotionsToUpdate.size() > 0)
                update mapPromotionsToUpdate.values();
        }catch(Exception ex){
            Database.rollback(objSavePoint);
            system.debug('Exception in Class : HEP_MarketSpendTriggerHandler - createSpendHistory, Error : ' +
                ex.getMessage() +
                ' Line Number : ' +
                ex.getLineNumber() +
                ' Cause : ' +
                ex.getCause() +
                ' Type : ' +
                ex.getTypeName());
            throw ex;
        }
    }
    
    /**
    * getValidSpendIds --- get spend ids with given RecordStatus__c
    * @param Map<Id, HEP_Market_Spend__c> oldMap, Map<Id, HEP_Market_Spend__c> newMap, String sRecordStatus
    * @exception Any exception
    * @return Set<Id> setSpendIds
    */
    public static Set<Id> getValidSpendIds(Map<Id, HEP_Market_Spend__c> oldMap, Map<Id, HEP_Market_Spend__c> newMap, String sRecordStatus){
        Set<Id> setSpendIds = new Set<Id>();
        for(Id sKey : newMap.keySet()){
            HEP_Market_Spend__c objNewSpend = newMap.get(sKey);
            if(oldMap.get(sKey).RecordStatus__c != objNewSpend.RecordStatus__c
                && String.isNotBlank(objNewSpend.RecordStatus__c)
                && objNewSpend.RecordStatus__c.equals(sRecordStatus)){
                
                setSpendIds.add(sKey);
            }
        }
        return setSpendIds;
    }

    /**
    * triggerEnqueueJob -- send the details to make the call out to Queueable class
    * @param  nothing
    * @return nothing  
    * @author Lakshman Jinnuri      
    */
    public void triggerEnqueueJob(Map<Id, HEP_Market_Spend__c> oldMap) {
        list<Id> lstIds = new list<Id>();
        String sHEP_TERRITORY_CANADA = HEP_Utility.getConstantValue('HEP_TERRITORY_CANADA');
        String sHEP_TERRITORY_US = HEP_Utility.getConstantValue('HEP_TERRITORY_US');
        String sHEP_TERRITORY_DHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
        String sHEP_SPEND_BUDGET_APPROVED = HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED');
        String sHEP_INT_JDE_SPEND_DETAILS = HEP_Utility.getConstantValue('HEP_INT_JDE_SPEND_DETAILS');
        for(HEP_Market_Spend__c objSpend : [SELECT Id,Promotion__r.Territory__c,RecordStatus__c,Promotion__r.Territory__r.Name
                      FROM
                        HEP_Market_Spend__c
                      WHERE 
                        Id IN : Trigger.newMap.keySet() AND RecordStatus__c =: sHEP_SPEND_BUDGET_APPROVED])
        {
        if(oldMap.get(objSpend.Id).RecordStatus__c != sHEP_SPEND_BUDGET_APPROVED)
        {
            if(sHEP_TERRITORY_CANADA != null && sHEP_TERRITORY_US != null && sHEP_TERRITORY_DHE != null && sHEP_SPEND_BUDGET_APPROVED != null
                && objSpend.RecordStatus__c.equals(sHEP_SPEND_BUDGET_APPROVED) && (objSpend.Promotion__r.Territory__r.Name.equalsIgnoreCase(sHEP_TERRITORY_DHE) || objSpend.Promotion__r.Territory__r.Name.equalsIgnoreCase(sHEP_TERRITORY_US) 
                || objSpend.Promotion__r.Territory__r.Name.equalsIgnoreCase(sHEP_TERRITORY_CANADA)))
                lstIds.add(objSpend.Id);
            }
        }
        if(sHEP_INT_JDE_SPEND_DETAILS != null && !lstIds.isEmpty())  
            if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
                System.enqueueJob(new HEP_Siebel_Queueable(JSON.serialize(lstIds),sHEP_INT_JDE_SPEND_DETAILS));
            }
    }
    /**
    * callQueueableForE1 -- send the details to make the call out to Queueable class
    * @param  
    * @return 
    * @author Himanshi Mayal     
    */
    public void callQueueableForE1(Map<Id, HEP_Market_Spend__c> oldMap)
    {
        //System.debug('callQueueableForE1++++++++++'+Trigger.newMap.keySet());
        //HEP_E1_Queueable.E1Wrapper wrapperE1;
        //List<HEP_E1_Queueable.E1Wrapper> lstWrapper = new List<HEP_E1_Queueable.E1Wrapper>();  
        Boolean approvedflag = false;
        String sTerritoryGlobal = HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME');
        String sTerritoryAustralia = HEP_Utility.getConstantValue('HEP_TERRITORY_AUSTRALIA');
        String sTerritoryDenmark = HEP_Utility.getConstantValue('HEP_TERRITORY_DENMARK');
        String sTerritoryFinland = HEP_Utility.getConstantValue('HEP_TERRITORY_FINLAND');
        String sTerritoryFrance = HEP_Utility.getConstantValue('HEP_TERRITORY_FRANCE');
        String sTerritoryGermany = HEP_Utility.getConstantValue('HEP_TERRITORY_GERMANY');
        String sTerritoryJapan = HEP_Utility.getConstantValue('HEP_TERRITORY_JAPAN');
        String sTerritoryNorway = HEP_Utility.getConstantValue('HEP_TERRITORY_NORWAY');
        String sTerritorySpain = HEP_Utility.getConstantValue('HEP_TERRITORY_SPAIN');
        String sTerritorySweden = HEP_Utility.getConstantValue('HEP_TERRITORY_SWEDEN');
        String sTerritoryUK = HEP_Utility.getConstantValue('HEP_Territory_UK');
        String sBudgetApproved = HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        String sHEP_E1_SpendDetail = HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'); 
        map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRecord = new map<String,HEP_Interface_Transaction__c>();
        Set<id> setPromoId = new Set<id>();  
        HEP_Interface__c objInterfaceRec = [SELECT id FROM HEP_Interface__c WHERE Name =: sHEP_E1_SpendDetail];                                     
        for(HEP_Market_Spend__c objMarketSpend : [SELECT id, RecordStatus__c, Promotion__r.Territory__r.Name, Promotion__c 
                                                  FROM HEP_Market_Spend__c WHERE id IN :Trigger.newMap.keySet()
                                                  AND Promotion__r.Territory__r.E1_Code__c != NULL
                                                  AND RecordStatus__c = :sBudgetApproved])
        {   
            if(((oldMap.get(objMarketSpend.Id)).RecordStatus__c != objMarketSpend.RecordStatus__c))
            {
                /*if((objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryGlobal) || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryAustralia)
                   || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryDenmark) || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryFinland)
                   || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryFrance) || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryGermany)
                   || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryJapan) || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryNorway)
                   || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritorySpain) || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritorySweden)
                   || (objMarketSpend.Promotion__r.Territory__r.Name == sTerritoryUK))
                {*/
                    approvedflag = true;
                    setPromoId.add(objMarketSpend.Promotion__c);
                    //wrapperE1  = new HEP_E1_Queueable.E1Wrapper();
                    //wrapperE1.spendId = objMarketSpend.Id;
                    //System.debug('WrapperE1 --- '+wrapperE1);   
                    //lstWrapper.add(wrapperE1);
                    //System.debug('List Of WrapperE1 ----- '+lstWrapper);
                //}
            }
        }
        if(setPromoId != NULL && !setPromoId.isEmpty() && objInterfaceRec.Id != NULL){
            //Creation of interface transaction records are being done as the autonumber is being used as the unique number 
            for(String promoId : setPromoId)
            {
                HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
                objTxn.Object_Id__c = promoId;
                objTxn.HEP_Interface__c = objInterfaceRec.Id;
                objTxn.Transaction_Datetime__c = System.now();
                objTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
                mapPromoIdInterfaceRecord.put(promoId,objTxn);
            }
            insert mapPromoIdInterfaceRecord.Values();
            for(String promoId : setPromoId)
            {
                //String sjson = json.serialize(lstPromoId);
                System.debug('STRING --- '+promoId);
                E1Wrapper objE1Wrapper = new E1Wrapper();
                if(mapPromoIdInterfaceRecord.containsKey(promoId)){
                    objE1Wrapper.sPromoId = promoId;
                    objE1Wrapper.sTransactionId = mapPromoIdInterfaceRecord.get(promoId).Id;
                }    
                String sSerlizedWrap = JSON.serialize(objE1Wrapper);
                if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
                    if(approvedflag == true && objE1Wrapper.sPromoId != NULL && objE1Wrapper.sTransactionId != NULL)
                    {
                        System.debug('GOING FOR QUEUEABLE-----');
                        System.enqueueJob(new HEP_E1_Queueable(sSerlizedWrap , HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL')));
                    }

                }
            }
        }            
    }

    public class E1Wrapper{
        public String sPromoId;
        public String sTransactionId;
    }
}