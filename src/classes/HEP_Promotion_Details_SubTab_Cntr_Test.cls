@isTest
public class HEP_Promotion_Details_SubTab_Cntr_Test {
    @testSetup
    static void createAllData() {
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        //User 
        //User objuser = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'lakshman', 'lakshman@deloitte.com', 'lakshman', 'lakshman', 'N', '', true);
        String sLocaleDateFormat = u.DateFormat__c;
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        objTerritory.FoxipediaCode__c = 'US';
        insert objTerritory;
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        //List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season', 'SEASN', false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id, false);
        objTitle.FOX_ID__c = '677282';
        insert objTitle;
        List < HEP_Line_Of_Business__c > lstLOB = new List < HEP_Line_Of_Business__c > {
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true),
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Catalog', 'Catalog', true),
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('TV', 'TV', true)
        };
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB', 'New Release', true);
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion', 'Global', '', objTerritory.Id, objLOB.Id, '', '', false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion1', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id, '', '', false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        //National
        List < HEP_Promotion__c > lstPromotion = new List < HEP_Promotion__c > {
            HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion1', 'National', null, objTerritory.Id, lstLOB[0].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion2', 'National', null, objTerritory.Id, lstLOB[1].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion3', 'National', null, objTerritory.Id, lstLOB[2].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotion4', 'Customer', null, objTerritory.Id, lstLOB[2].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('GlobalPromotion1', 'Global', null, objTerritory.Id, lstLOB[0].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('GlobalPromotion2', 'Global', null, objTerritory.Id, lstLOB[1].Id, null, null, true),
            HEP_Test_Data_Setup_Utility.createPromotion('GlobalPromotion3', 'Global', null, objTerritory.Id, lstLOB[2].Id, null, null, true)
        };
        //TRP
        HEP_Promotion__c objPromotionTRP = HEP_Test_Data_Setup_Utility.createPromotion('NationalPromotionTRP', 'National', null, objTerritory.Id, lstLOB[2].Id, null, null, false);
        objPromotionTRP.StartDate__c = System.today();
        objPromotionTRP.EndDate__c = System.today();
        objPromotionTRP.TPR_Only__c = true;
        objPromotionTRP.Title__c = objTitle.Id;
        objPromotionTRP.LineOfBusiness__c = lstLOB[0].Id;
        insert objPromotionTRP;
        //Create HEP_Customer__c Record
        HEP_Customer__c objCustomerPromotion = HEP_Test_Data_Setup_Utility.createHEPCustomers('CustomerPromotion', objTerritory.id, 'Cust567', 'MDP Child Customers', 'VOD Release Date', true);
        //Inserting Region
        List < HEP_Territory__c > lstjRegion = new List < HEP_Territory__c > {
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '890', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Netherlands', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'CAD', 'CA', '891', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('New Zealand', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '892', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Norway', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '893', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Poland', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '894', 'JDE', true)
        };
        //Create Dating Matrix Record...
        List < HEP_Promotions_DatingMatrix__c > lstDatingMatrix = new List < HEP_Promotions_DatingMatrix__c > {
            HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE'), objTerritory.Id, lstjRegion[0].Id, true),
            HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RENTAL'), objTerritory.Id, lstjRegion[2].Id, true),
            HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RETAIL'), objTerritory.Id, lstjRegion[2].Id, true),
            HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', HEP_Utility.getConstantValue('HEP_DATETYPE_EST_RELEASE_DATE'), objTerritory.Id, lstjRegion[4].Id, true)
        };
        //Insert Promotion Dating Record
        HEP_Promotion_Dating__c objDatingVOD = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id, lstPromotion[0].id, lstDatingMatrix[0].id, false);
        objDatingVOD.Date_Type__c = HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE');
        insert objDatingVOD;
        HEP_Promotion_Dating__c objDatingPHYRN = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id, lstPromotion[0].id, lstDatingMatrix[1].id, false);
        objDatingPHYRN.Date_Type__c = HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RENTAL');
        insert objDatingPHYRN;
        HEP_Promotion_Dating__c objDatingPHYRT = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id, lstPromotion[0].id, lstDatingMatrix[2].id, false);
        objDatingPHYRT.Date_Type__c = HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RETAIL');
        insert objDatingPHYRT;
        HEP_Promotion_Dating__c objDatingEST = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id, lstPromotion[0].id, lstDatingMatrix[3].id, false);
        objDatingEST.Date_Type__c = HEP_Utility.getConstantValue('HEP_DATETYPE_EST_RELEASE_DATE');
        insert objDatingEST;
    }
    static testMethod void CreateData() {
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Rating', 'HEP_RatingDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
    }
    static testMethod void test2() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677281';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test3() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677282';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test4() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677283';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test5() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677284';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test6() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677285';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test7() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677286';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test8() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677287';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test9() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677288';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test10() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '677289';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test11() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '6772891';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test12() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '6772892';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test13() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '6772893';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test14() {
        CreateData();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '6772894';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test15() {
        CreateData();
        HEP_Line_Of_Business__c objLOB = [SELECT Type__c FROM HEP_Line_Of_Business__c WHERE Name = : 'LOB'];
        objLOB.Type__c = 'TV';
        Update objLOB;
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        EDM_GLOBAL_TITLE__c objTitle = [SELECT FOX_ID__c FROM EDM_GLOBAL_TITLE__c WHERE Name = : 'Title Test'];
        objTitle.FOX_ID__c = '6772895';
        update objTitle;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRatingForTitle(objPromotion.id);
            Test.Stoptest();
        }
    }
    static testMethod void test16() {
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        String sLocaleDateFormat = u.DateFormat__c;
        List < HEP_Territory__c > lstjRegion = [SELECT id from HEP_Territory__c where Name = : 'Austria'];
        List < String > lstRgionid = new List < String > ();
        lstRgionid.add(lstjRegion[0].id);
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.getRegionAssociatedCustomers(lstRgionid);
            Test.Stoptest();
        }
    }
    static testMethod void test17() {
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        String sLocaleDateFormat = u.DateFormat__c;
        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotion1'];
        HEP_Promotion__c objPromotionGL = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'GlobalPromotion1'];
        HEP_Promotion_Details_SubTab_Controller.PromotionDetailWrapper objWrapperReturned = new HEP_Promotion_Details_SubTab_Controller.PromotionDetailWrapper();
        objWrapperReturned = HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion.id, sLocaleDateFormat);
        objWrapperReturned = HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotionGL.id, sLocaleDateFormat);
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion_Details_SubTab_Controller.updatePromotionDetails(objWrapperReturned, objPromotion.id);
            HEP_Promotion_Details_SubTab_Controller.updatePromotionDetails(objWrapperReturned, objPromotionGL.id);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion.id, sLocaleDateFormat);
            Test.Stoptest();
        }
    }
    static testMethod void test18() {
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        String sLocaleDateFormat = u.DateFormat__c;
        HEP_Promotion__c objPromotion1 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotion1'];
        HEP_Promotion__c objPromotion2 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotion2'];
        HEP_Promotion__c objPromotion3 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotion3'];
        HEP_Promotion__c objPromotion4 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'GlobalPromotion1'];
        HEP_Promotion__c objPromotion5 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'GlobalPromotion2'];
        HEP_Promotion__c objPromotion6 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'GlobalPromotion3'];
        HEP_Promotion__c objPromotion7 = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotion4'];
        HEP_Promotion__c objPromotion8 = [SELECT Id, StartDate__c, EndDate__c FROM HEP_Promotion__c WHERE PromotionName__c = : 'NationalPromotionTRP'];
        System.runAs(u) {
            Test.startTest();
            HEP_Promotion__c obj = new HEP_Promotion__c();
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion1.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion2.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion3.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion4.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion5.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion6.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion7.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.extractHepPromotionDetailPageData(objPromotion8.id, sLocaleDateFormat);
            HEP_Promotion_Details_SubTab_Controller.getGlobalPromotions('GlobalPromotion2');
            HEP_Promotion_Details_SubTab_Controller.isRangeCorrect(objPromotion8.id, objPromotion8.StartDate__c, objPromotion8.EndDate__c);
            Test.Stoptest();
        }
    }
    static testMethod void test19() {
        User u1 = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'roshiRai', 'roshirai777@deloitte.com', 'Rai7', 'ros7', 'N', '', true);
        HEP_Promotion__c objPromotionGL = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion1'];
        System.runAs(u1) {
            Test.startTest();
            ApexPages.currentPage().getParameters().put('promoId', objPromotionGL.Id);
            HEP_Promotion_Details_SubTab_Controller objPromotion = new HEP_Promotion_Details_SubTab_Controller();
            objPromotion.sCurrencyFormat = '$';
            objPromotion.sTerritoryId = '345yyry';
            objPromotion.lStDateMdDate = 10;
            objPromotion.lStDateSysDate = 9;
            objPromotion.dtSysTime = System.Now();
            objPromotion.sCustomerPromotionStatus = 'Pending';
            objPromotion.bChangeCustomer = true;
            Test.Stoptest();
        }
    }
}