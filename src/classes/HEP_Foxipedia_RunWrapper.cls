/**
* HEP_Foxipedia_RunWrapper-- Wrapper to store details for foxipedia integration
* @author    Balaji
*/

public class HEP_Foxipedia_RunWrapper{
    public List<Data> data;
    public Links links;
    public String calloutStatus;
    public String TxnId;
    public list<Errors> errors; 
    
    
    public class Data {
        public Attributes attributes;
        public String id;
        public String type;
        public Relationships relationships;
    }
    
    
    
    public class attributes {
        public String type;  
        public String foxId; 
        public String rowIdObject; 
        public String rowIdVersion;    
        public String titleName;   
        public String financialProductId;  
        public String publicCode;  
        public String publicCodeDescription;   
        public String scopeIndicator;
        public Version version;
        public String seriesName;
        public String seasonNumber;
        public String productEpisodeNumber;
        public String productTypeCode; 
        public String productTypeDescription;  
        public String episodeName;
        public String lifecycleStatusDescription;  
        public String lifecycleStatusCode; 
        public String lifecycleStatusGroup;    
        public String lifecycleEffectiveDate;
        public String lifecycleNotes;
        public String productEpisodeCount;
        public String productionCalendarYear;  
        public String productionFiscalYear;    
        public String releaseCalendarYear; 
        public String releaseFiscalYear;   
        public String primaryProductionCountryCode;    
        public String primaryProductionCountryDescription; 
        public String financeDivisionCode; 
        public String financeDivisionDescription;  
        public String internationalProductTypeCode;    
        public String internationalProductTypeDescription; 
        public String publishIndicatorCode;    
        public String publishIndicatorDescription; 
        public String mergedTitle; 
        public String unsortedTitle;   
        public String sortedTitle; 
        public String originalMediaCode;   
        public String originalMediaDescription;    
        public String originalLanguageCode;    
        public String originalLanguageDescription; 
        public String directorName;    
        public String productAcqInd;   
        public String productFinanceCode;  
        public list<Location> location;
        public list<ProdCompany> prodCompany;
        public list<Extensions> extensions;
    }

    Public class Version {
        public String titleSubTypCode; 
        public String programRunTime;
        public String actualRunTime;   
    }
    
    Public class ProdCompany {
        public String foxId;   
        public String rowIdObject; 
        public String rowIdTitle;  
        public String productionCompanySortOrder;  
        public String productionCompanyCode;
        public String productionCompanyDescription;    
    }
    
    
    Public class Extensions {
        public String rowIdObject; 
        public String attributeValueCode;  
        public String attributeTypeCode;   
        public String attributeTypeDescription;    
        public String attributeValueDescription;   
    }
    
    Public class Location{
        public String primaryLocationIndicator;
        public String countryCode;
        public String shootingLocation;
        public String locationSortOrder;
        public String rowIdObject;
        public String countryDescription;
        public String foxId;
    }
    
    
    public class Relationships {
        public Title title;
    }
    
    
    public class Title {
        public Links links;
    }
    
    
    public class Links {
        public String related; 
        public String self;
    }

    public class Errors{
        public String title ;
        public String detail; 
        public Integer status;
    }
    
 }