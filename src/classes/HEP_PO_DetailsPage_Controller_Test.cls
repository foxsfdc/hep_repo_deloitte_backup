@isTest
public class HEP_PO_DetailsPage_Controller_Test{
    public static testMethod void getPODetailsTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //Create User
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
                                    'test@deloitte.com','testL','testL','Fox - New Release',true);

        //Create Line of Business
        HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);
        System.debug('objLineOfBusiness --> ' + objLineOfBusiness);
        //Create EDM Product Type.
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','FEATR',true);

        //Create EDM Title Record.
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);
        //Create Territory
        List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false),
                                          HEP_Test_Data_Setup_Utility.createHEPTerritory('India','EMEA','Subsidiary',null,null,'USD','IN','999','JDE',false)};

        lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
        lstTerritory[0].Flag_Country_Code__c = '1';
        lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
        lstTerritory[1].Flag_Country_Code__c = '2';
        lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
        lstTerritory[2].Flag_Country_Code__c = '3';
        insert lstTerritory; 
        //Create Promotion
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                      HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                      HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)};
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[0].Digital_Physical__c = 'Both';
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[1].Digital_Physical__c = 'Both';
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;
        lstPromotions[2].Digital_Physical__c = 'Both';
        insert lstPromotions;
        list<HEP_GL_Account__c> lstGLAccounts = new list<HEP_GL_Account__c>(); 
        HEP_GL_Account__c objGLAccount1 = HEP_Test_Data_Setup_Utility.createGLAccount('Germany / Global Marketing Partnership / 629701','629701','3RD Party Promo Satz/Rein', null,'Active', false);        
            objGLAccount1.Account_Department__c = 'Global Marketing Partnership';
        lstGLAccounts.add(objGLAccount1);
        
        HEP_GL_Account__c objGLAccount2 = HEP_Test_Data_Setup_Utility.createGLAccount('Germany / Creative','611000',null, null,'Active', false);        
        objGLAccount2.Account_Department__c = 'Creative';
        objGLAccount2.Territory__c = lstTerritory[1].id;
        lstGLAccounts.add(objGLAccount2);
        
        HEP_GL_Account__c objGLAccount4 = HEP_Test_Data_Setup_Utility.createGLAccount('Germany / Global Content Development','630122',null, null,'Active', false);        
        objGLAccount4.Account_Department__c = 'Global Content Development';
        objGLAccount4.Territory__c = lstTerritory[1].id;
        lstGLAccounts.add(objGLAccount4);
        
        insert lstGLAccounts;
        
        lstGLAccounts.clear();
        HEP_GL_Account__c objGLAccount3 = HEP_Test_Data_Setup_Utility.createGLAccount('Germany / Creative / 611700','611700', 'TV Spots-Creative', null,'Active', false);        
        objGLAccount3.Account_Department__c = 'Creative';
        objGLAccount3.Territory__c = lstTerritory[1].id;
        objGLAccount3.Parent_GL_Account__c = objGLAccount2.id;
        lstGLAccounts.add(objGLAccount3);
        insert lstGLAccounts;
        String sPO1 = '{\"purchaseOrders\":[{\"OPEN\":\"1000.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019781\",\"COUNTRY\":\"\",\"OBJECT\":\"629701\",\"ITEM_AMT\":\"1000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"0.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Open\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"},{\"OPEN\":\"0.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019782\",\"COUNTRY\":\"\",\"OBJECT\":\"611700\",\"ITEM_AMT\":\"20000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"20000.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Closed\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"},{\"OPEN\":\"30000.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019783\",\"COUNTRY\":\"\",\"OBJECT\":\"630122\",\"ITEM_AMT\":\"30000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"0.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Open\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"}]}';
        String sInv1 = '{\"invoices\":[{\"GL_LINE#\":\"1.0\",\"ACTUAL_TYPE\":\"PO\",\"NAME\":\"ABCreativ Service\",\"INVOICE#\":\"TEST5637\",\"COUNTRY\":\"\",\"INVOICE_STATUS\":\"Approved for Payment\",\"OBJECT\":\"611700\",\"PROMO\":\"90722\",\"ACTUAL\":\"20000.00\",\"LINE_DESCRIPTION\":\"3RD Party Promo Satz/Rein\",\"CURRENCY\":\"EUR\",\"INV_DATE\":\"07/19/2018\",\"FORMAT\":\"DVD\",\"PO#\":\"1019782\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"}]}';
        HEP_Utility.processPOInvoice(sPO1 , sinv1 , lstPromotions[0].Id);
        test.startTest();
        HEP_PO_DetailsPage_Controller objPODetails = new HEP_PO_DetailsPage_Controller();
        HEP_PO_DetailsPage_Controller.getPODetails(lstPromotions[0].Id,'1019782',lstGLAccounts[0].Id);
        objPODetails.checkPermissions();
        test.stopTest();
        
    }
}