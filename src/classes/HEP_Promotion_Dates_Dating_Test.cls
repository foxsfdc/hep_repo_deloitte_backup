@isTest
public class HEP_Promotion_Dates_Dating_Test{
    
    @testSetup
	static void createData(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Elango', 'elango@deloitte.com', 'K', 'Elan', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Dating', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date', 'HEP_ProphetProjectedDateDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
	}
	
	@isTest
	public static void testGetNationalPromotionDatesNewRelease(){
	    
	    User u = [SELECT Id, DateFormat__c from User where LastName = 'Elango'];
	    String sLocaleDateFormat =  u.DateFormat__c;
	    HEP_Promotion_Dates_Dating_Controller.HEP_National_Dates_Wrapper objWrapperReturned = new HEP_Promotion_Dates_Dating_Controller.HEP_National_Dates_Wrapper();
	    
	    //Create Territory Records
        HEP_Territory__c Germany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'BGN', 'BG', '102', null, false);
        Germany.FoxipediaCode__c = 'DE';
        insert Germany;
         HEP_Territory__c GlobalTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', null, true);
        
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations(International)', 'Promotions' ,true);
        
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        //User role setup 
        HEP_User_Role__c objUserRoleGermany = HEP_Test_Data_Setup_Utility.createHEPUserRole(Germany.Id,objRoleOperations.Id,u.Id,true);
        
        //Create LOB
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        
        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', Germany.Id, Germany.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
         VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Release Date', Germany.Id, Germany.Id, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
         ESTRelease.Media_Type__c = 'Digital';
        insert ESTRelease;
        HEP_Promotions_DatingMatrix__c PhysicalRetailRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'Physical Release Date (Retail)', Germany.Id, Germany.Id, false);
         PhysicalRetailRelease.Media_Type__c = 'Physical';
        PhysicalRetailRelease.Projected_FAD_Logic__c = 'Monday';
        insert PhysicalRetailRelease;
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'VOD Avail Date', Germany.Id, Germany.Id, false);
         VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset__c = 30;
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
        HEP_Promotions_DatingMatrix__c ESTAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'EST Avail Date', Germany.Id, Germany.Id, false);
         ESTAvail.Media_Type__c = 'Digital';
        ESTAvail.Date_Offset__c = 30;
        ESTAvail.Date_Offset_Source__c = 'EST Release Date';
        insert ESTAvail;
        
        //Creating a title ID
        EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
        EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadettge', '197429', 'PUBLC', prodType.id, true);
        EDM_GLOBAL_TITLE__c Title1 = HEP_Test_Data_Setup_Utility.createTitleEDM('Deadpool 3', '20004', 'PUBLC', prodType.id, true);
        //EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadette', '197429', '', prodType.id, true);
        //EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', prodType.Id,false);
        //insert Title;
        
        HEP_Promotion__c testPromoGlobal = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', null, GlobalTerr.id, FoxNewRelease.Id, null, null, false);
        //testPromo.Title__c = Title.Id;
        String FADDate = '2018-07-12';
        testPromoGlobal.FirstAvailableDate__c = Date.valueOf(FADDate); 
        System.debug('testPromo.FirstAvailableDate__c *&** '+ testPromoGlobal.FirstAvailableDate__c);
        insert testPromoGlobal;

        HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', testPromoGlobal.Id, Germany.id, FoxNewRelease.Id, null, null, false);
        testPromo.Title__c = Title.Id;
        /*String FADDate = '2018-07-12';
        testPromo.FirstAvailableDate__c = Date.valueOf(FADDate); 
        System.debug('testPromo.FirstAvailableDate__c *&** '+ testPromo.FirstAvailableDate__c);*/
        insert testPromo;
        
        HEP_Promotion_Region__c objPromotionReg1 = HEP_Test_Data_Setup_Utility.createPromotionRegion(testPromo.Id, Germany.Id, true);
        
        //TVD_Release_Dates__c objTVDReleaseDates = HEP_Test_Data_Setup_Utility.createTVDReleaseDate(Germany.FoxipediaCode__c, Title.Id, Date.today(),true);
        GTS_Title__c objGTSTitle = HEP_Test_Data_Setup_Utility.createGTSTitle('Title Name','123','Title','197429', true);
        GTS_Territory__c objGTSTerritory = HEP_Test_Data_Setup_Utility.createGTSTerritory('Germany','Germany','102',true);
        GTS_Release__c objGTSRelease = HEP_Test_Data_Setup_Utility.createGTSRelease('ReleaseId',Date.today(),objGTSTitle.Id,objGTSTerritory.Id, true);
        
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Germany Customers',Germany.Id, '124', 'Dating Customers', 'VOD Release Date', true);
        //Create Catalog Record :Germany
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,Germany.Id, 'Draft', 'Request', false);
        objCatalog.Licensor_Type__c = 'Lucas';
        objCatalog.Licensor__c = 'ABLO';
        insert objCatalog;

        //Create Promotion Catalog Record:Germany
        HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, testPromo.Id, null, true);
            
        //Create dating records 
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODRelease.id, false);
        DatingRec1.HEP_Catalog__c = objCatalog.Id;
        DatingRec1.Customer__c = objCustomer.Id;
        DatingRec1.Status__c = 'Tentative';
        insert DatingRec1;
        
        HEP_Promotion_Dating__c DatingRec2 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, ESTRelease.id, false);
       
        DatingRec2.Status__c = 'Projected';
        insert DatingRec2;
        
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, PhysicalRetailRelease.id, false);
        DatingRec3.Status__c = 'Projected';
        insert DatingRec3;
        
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Projected';
        insert DatingRec4;
        
        HEP_Promotion_Dating__c DatingRec5 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, ESTAvail.id, false);
        DatingRec5.Status__c = 'Projected';
        insert DatingRec5;
       
        system.runAs(u){
            Test.startTest();
              Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
              
              objWrapperReturned = HEP_Promotion_Dates_Dating_Controller.getNationalPromotionDates(testPromo.Id,sLocaleDateFormat);
              objWrapperReturned.bIsEdited = true;
              HEP_Promotion_Dates_Dating_Controller.getNationalPromotionDates(testPromo.Id,sLocaleDateFormat);
              HEP_Promotion_Dates_Dating_Controller.saveNationalPromotionDates(objWrapperReturned);
              HEP_Promotion_Dates_Dating_Controller.callFADApprovalProcess(DatingRec1.Id, testPromo.Id);
              //system.assertEquals()
            Test.stopTest();
        }
	}
	
	
	/*@isTest
	public static void testGetNationalPromotionDatesTV(){
	    
	    User u = [SELECT Id, DateFormat__c from User where LastName = 'Elango'];
	    String sLocaleDateFormat =  u.DateFormat__c;
	    HEP_Promotion_Dates_Dating_Controller.HEP_National_Dates_Wrapper objWrapperReturned = new HEP_Promotion_Dates_Dating_Controller.HEP_National_Dates_Wrapper();
	    
	    //Create Territory Records
        HEP_Territory__c Germany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'BGN', 'BG', '102', null, false);
        Germany.FoxipediaCode__c = 'DE';
        insert Germany;
        
        //ROLE setup for UNLOCK
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations(International)', 'Promotions' ,true);
      
        //User role setup 
        HEP_User_Role__c objUserRoleGermany = HEP_Test_Data_Setup_Utility.createHEPUserRole(Germany.Id,objRoleOperations.Id,u.Id,true);
        
        //Create LOB
        HEP_Line_Of_Business__c FoxTV = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-TV', HEP_Utility.getConstantValue('PROMOTION_LOB_TV'), true);
	   
	    //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', 'VOD Release Date', Germany.Id, Germany.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
        VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', 'EST Release Date', Germany.Id, Germany.Id, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
        insert ESTRelease;
        HEP_Promotions_DatingMatrix__c PhysicalRetailRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Release Dates', 'Physical Release Date (Retail)', Germany.Id, Germany.Id, false);
        PhysicalRetailRelease.Projected_FAD_Logic__c = 'Monday';
        insert PhysicalRetailRelease;
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('TV', 'Avail and Announce Dates', 'VOD Avail Date', Germany.Id, Germany.Id, false);
        VODAvail.Date_Offset__c = 30;
        VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
        HEP_Promotions_DatingMatrix__c ESTAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'EST Avail Date', Germany.Id, Germany.Id, false);
        ESTAvail.Date_Offset__c = 30;
        ESTAvail.Date_Offset_Source__c = 'EST Release Date';
        insert ESTAvail;
        
        //Creating a title ID
        EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
        EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadette', '197429', 'PUBLC', prodType.id, true);
        
        
        //Setting up Promotion and dating records
        HEP_CheckRecursive.clearSetIds();
        
        TVD_Release_Dates__c objTVDReleaseDates = HEP_Test_Data_Setup_Utility.createTVDReleaseDate(Germany.FoxipediaCode__c, Title.Id, Date.today(),true);
       
        HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', null, Germany.id, FoxTV.Id, null, null, false);
        testPromo.Title__c = Title.Id;
        insert testPromo;
        
        //Create Catalog Record :Germany
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,Germany.Id, 'Draft', 'Request', false);
        objCatalog.Licensor_Type__c = 'Lucas';
        objCatalog.Licensor__c = 'ABLO';
        objCatalog.Title_EDM__c = Title.Id;
        insert objCatalog;

        //Create Promotion Catalog Record:Germany
        HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, testPromo.Id, null, true);
            
        HEP_Promotion_Region__c objPromotionReg1 = HEP_Test_Data_Setup_Utility.createPromotionRegion(testPromo.Id, Germany.Id, true);
       
        //Create dating records 
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODRelease.id, false);
        DatingRec1.Status__c = 'Tentative';
        DatingRec1.HEP_Catalog__c = objCatalog.Id;
        insert DatingRec1;
        
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, PhysicalRetailRelease.id, false);
        DatingRec3.Status__c = 'Projected';
        insert DatingRec3;
        
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Projected';
        insert DatingRec4;
       
        system.runAs(u){
            Test.startTest();
              Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
              objWrapperReturned = HEP_Promotion_Dates_Dating_Controller.getNationalPromotionDates(testPromo.Id,sLocaleDateFormat);
            Test.stopTest();
        }
	}*/
	
	@isTest
	static void testWrapperFields(){
	    
	    User u = [SELECT Id, DateFormat__c from User where LastName = 'Elango'];
	    String sLocaleDateFormat =  u.DateFormat__c;
	    
	    System.runAs(u){
    	    Test.startTest();
    	    HEP_Promotion_Dates_Dating_Controller.Languages objLang = new HEP_Promotion_Dates_Dating_Controller.Languages('sKey','sValue','Germany');
    	    HEP_Promotion_Dates_Dating_Controller.DateType objDateType = new HEP_Promotion_Dates_Dating_Controller.DateType('sKey','sValue','Release Dates','Germany','VOD Release Date',10,false);
    	    HEP_Promotion_Dates_Dating_Controller.CustomerPickList objCustPicklist = new HEP_Promotion_Dates_Dating_Controller.CustomerPickList('sKey','sValue','Germany','VOD Release Date');
    	    HEP_Promotion_Dates_Dating_Controller.PickLists objPicklist = new HEP_Promotion_Dates_Dating_Controller.PickLists('sKey','sValue');
    	    
    	    HEP_Promotion_Dates_Dating_Controller.Languages objLangEmpty = new HEP_Promotion_Dates_Dating_Controller.Languages();
    	    HEP_Promotion_Dates_Dating_Controller.CustomerPickList objCustPicklistEmpty = new HEP_Promotion_Dates_Dating_Controller.CustomerPickList();
    	    HEP_Promotion_Dates_Dating_Controller.PickLists objPicklistEmpty = new HEP_Promotion_Dates_Dating_Controller.PickLists();
    	    HEP_Promotion_Dates_Dating_Controller.CatalogPickList objCatalogPicklistEmpty = new HEP_Promotion_Dates_Dating_Controller.CatalogPickList();
    	    
    	    Test.stopTest();
	    }
	    
	}
	
	@isTest
	static void testDatingUnlock(){
	    
	    User u = [SELECT Id, DateFormat__c from User where LastName = 'Elango'];
	    String sLocaleDateFormat =  u.DateFormat__c;
	   
	    //Create Territory Records
        HEP_Territory__c Germany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'BGN', 'BG', '102', null, true);
        HEP_Territory__c GlobalTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', null, true);
        
        //ROLE setup for UNLOCK
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations(International)', 'Promotions' ,true);
        
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        //User role setup 
        HEP_User_Role__c objUserRoleGermany = HEP_Test_Data_Setup_Utility.createHEPUserRole(Germany.Id,objRoleOperations.Id,u.Id,true);
        
        //Create LOB
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        
        
        //Create DAting Approval Type
        HEP_Approval_Type__c objApprovalTypeDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_Dating__c', 'Locked_Status__c', 
                                                                                                'Unlocked','Locked','DATING',
                                                                                                'Static','Pending',                                                                 
                                                                                                false);
        objApprovalTypeDating.Notif_App_Template__c = 'Dating_Unlock_Request_Approved';
        objApprovalTypeDating.Notif_Rej_Template__c = 'Dating_Unlock_Request_Rejected';
        objApprovalTypeDating.Email_App_Template__c = 'HEP_Dating_Approval_Response';
        objApprovalTypeDating.Email_Rej_Template__c = 'HEP_Dating_Approval_Response';
        objApprovalTypeDating.Email_Req_Template__c = 'HEP_Dating_Approval_Request';
        insert objApprovalTypeDating;
        
        HEP_Rule__c objRule3 = HEP_Test_Data_Setup_Utility.createRule('Dating Unlock Rule International','HEP_Promotion_Dating__c','(1 AND 2)',true);
        Id RecordTypeId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName().get('Compare Field Value').getRecordTypeId();
        List<HEP_Rule_Criterion__c> lstRuleCriterias = new List<HEP_Rule_Criterion__c>();
        
        HEP_Rule_Criterion__c objRuleCriteria5 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Digital_Physical__c','Digital',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
        objRuleCriteria5.HEP_Source_SObject_Field_Type__c= 'STRING';
        objRuleCriteria5.HEP_Serial_Number__c = 1;
        lstRuleCriterias.add(objRuleCriteria5);
        HEP_Rule_Criterion__c objRuleCriteria6 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CC_Email_Filter__c','Germany',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
        objRuleCriteria6.HEP_Source_SObject_Field_Type__c= 'STRING';
        objRuleCriteria6.HEP_Serial_Number__c = 2;
        lstRuleCriterias.add(objRuleCriteria6);
        
        insert lstRuleCriterias;
        
        //Approval Type Role Setup
        List<HEP_Approval_Type_Role__c> lstApprovalTypeRole = new List<HEP_Approval_Type_Role__c>();
        
        //Approver role For Dating
        HEP_Approval_Type_Role__c objApprovalTypeRole3 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeDating.Id, objRoleOperations.Id, 'ALWAYS', false);
        objApprovalTypeRole3.Rule__c = objRule3.Id;
        objApprovalTypeRole3.Sequence__c = 1;
        lstApprovalTypeRole.add(objApprovalTypeRole3);
       
        insert lstApprovalTypeRole;
    
        
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', Germany.Id, Germany.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
        VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'VOD Avail Date', Germany.Id, Germany.Id, false);
        VODAvail.Date_Offset__c = 30;
        VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
            
        HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', null,Germany.id, FoxNewRelease.Id, null, null, true);
        
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODRelease.id, false);
        DatingRec1.Status__c = 'Tentative';
        DatingRec1.Locked_Status__c = 'Locked';
        insert DatingRec1;
        
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Tentative';
        insert DatingRec4;
        
        
        System.runAs(u){
            Test.startTest();
            HEP_Promotion_Dates_Dating_Controller.saveUnlockComments('Comments', DatingRec1.Id, Germany.Id, sLocaleDateFormat);
            Test.stopTest();
        }
        
	}
	
}