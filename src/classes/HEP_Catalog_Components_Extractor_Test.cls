/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_Catalog_Components_Extractor_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', true);
        HEP_Territory__c objLocalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Territory__c objAutoTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '183', 'JDE', true);
        objAutoTerritory.Auto_Catalog_Creation__c = true;
        update objAutoTerritory;

        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Glocal Catalog', 'Single', null, string.valueof(objGlobalTerritory.id), 'Pending', 'Request', true);
        HEP_Catalog__c objLocalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Local Catalog', 'Single', string.valueof(objGlobalCatalog.id), string.valueof(objLocalTerritory.id), 'Pending', 'Request', true);

        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Catalog_Components','HEP_Catalog_Components_Extractor',
                                        false,30,1,'Outbound-Pull', true, true, true);

    }                                                   

    public static testMethod void test() {

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Catalog__c> lstCatalogs = [SELECT Id, Catalog_Name__c FROM HEP_Catalog__c];
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '01234';


        System.runAs(u) {
            Test.startTest();
                HEP_Catalog_Components_Extractor objCatalogComExtractor = new HEP_Catalog_Components_Extractor();
                Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
                objCatalogComExtractor.performTransaction(objTxnResponse);
                objTxnResponse.sSourceId = '12345';
                objCatalogComExtractor.performTransaction(objTxnResponse);
                objTxnResponse.sSourceId = '23456';
                objCatalogComExtractor.performTransaction(objTxnResponse);
                objTxnResponse.sSourceId = '34567';
                objCatalogComExtractor.performTransaction(objTxnResponse);
            Test.stoptest();
        }
    }
}