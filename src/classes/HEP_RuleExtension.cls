/* Class Name   : HEP_RuleExtension
 * Description  :    
 * Created By   : Nidhin V K
 * Created On   : 07-28-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-28-2016        1000            Initial version
 *
 */
public with sharing class HEP_RuleExtension {
    
    public List<SelectOption> lstTargetObjects{get; set;}
    
    public HEP_Rule__c objRule;
    
    public Boolean bShowError{get; set;}
    public Boolean bError{get; set;}
    public Boolean bAllowEditSobj{get; set;}
    
    
    /* HEP_RuleExtension constructor does the basic functionalities like 
     * initialization of objects and get some needed data
     * Enable/Disable Buttons
     * Show/Hide fields
     * @params : controller - Apex Standard Controller
     * @return : null
     */
    public HEP_RuleExtension(ApexPages.StandardController controller) {
        
        //Get the fields which are not used in the Page
        if(!Test.isRunningTest())
            controller.addFields(new List<String>{System.Label.HEP_FIELD_IS_ACTIVE});
        this.objRule = (HEP_Rule__c)controller.getRecord();
        //If active then show message while editing
        if(objRule.HEP_IsActive__c){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.HEP_LABEL_DEACTIVATE_RULE_EDIT));
            bShowError = true;
            return;
        }
        
        //If rule is new allow some fields editable
        if(objRule.Id == NULL){
            bAllowEditSobj = true;
        }
        lstTargetObjects = populateSObjects();
    }
    
    
    /* populateSObjects 
     * Populate the sobject based in the Org
     * @params : null
     * @return : List<SelectOption> - lstOptions to populate list of sObjects
     */
    public List<SelectOption> populateSObjects(){
     
        List<String> lstOptionsLabel = new List<String>();
        List<SelectOption> lstOptions = new List<SelectOption>();
        Map<String, SelectOption> mapOptions = new Map<String, SelectOption>();
        //get all the sobjects
        List<Schema.SObjectType> lstGblDescribe = Schema.getGlobalDescribe().values();

        //Filter the Objects
        for(Schema.SObjectType sobjType : lstGblDescribe){
            Schema.DescribeSobjectResult sObj = sobjType.getDescribe();
            if(sObj.isAccessible() && sObj.isCreateable() && sObj.isUpdateable()){
                lstOptionsLabel.add(sObj.getLabel() + sObj.getName());
                mapOptions.put(sObj.getLabel() + sObj.getName(), new SelectOption(sObj.getName(), sObj.getLabel() + ' (' + sObj.getName() + ')'));
            }
        }
        lstOptionsLabel.sort();
        for(String key : lstOptionsLabel){
            lstOptions.add(mapOptions.get(key));
        }
        return lstOptions;
    }
    
    
    /* cancel 
     * Override cancel method to Redirect to Rule list if creating new record
     * else redirect to newly created record
     * @params :
     * @return :
     */
    public PageReference cancel(){
        //Redirect to Rule list if creating new record
        if(objRule != NULL && objRule.Id == NULL){
            return new PageReference(System.Label.HEP_URL_VIEW_RULE_LIST);
        }
        //else redirect to newly created record
        return new PageReference('/' + objRule.Id);
    }
    
    
    /* toggleActivate 
     * Activate/Deactivate the Rule
     * @params :
     * @return :
     */    
    public PageReference toggleActivate(){
        try{
            bError = false;
            if(objRule != NULL){
                Integer iCount = [SELECT
                                        Count()                                         
                                    FROM
                                        HEP_Rule_Criterion__c
                                    WHERE
                                        HEP_Rule__c = :objRule.Id];
                System.debug('iCount>>' + iCount);
                if(iCount < 1){ // Check if Rule Version has atleast one criteria
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.HEP_LABEL_NO_CRITERIA_FOUND));
                    bShowError = true;
                    bError = true;
                    return null;
                }                                         
            }
            System.debug('bError>>' + bError);
            if(bError == false){
                objRule.HEP_IsActive__c = objRule.HEP_IsActive__c ? false : true;
                System.debug('objRule.HEP_IsActive__c>>' + objRule.HEP_IsActive__c);
                if(String.isBlank(objRule.HEP_Criteria_Logic__c)){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide the Criteria Logic.'));
                    bShowError = true;
                    objRule.HEP_IsActive__c = false;
                } else{
                    if(!HEP_Rule_Utility.checkCriteriaLogic(objRule)){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Wrong Criteria Logic.'));
                        bShowError = true;
                        objRule.HEP_IsActive__c = false;
                    }
                }
                update objRule;
                return NULL;
            }
            return NULL;
        } catch(Exception ex){
        	HEP_Error_Log.genericException('Rule Erros','DML Errors',ex,'HEP_RuleExtension','toggleActivate',null,null); 
            //throw error
            System.debug('Exception on Class : HEP_RuleExtension - toggleActivate, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.HEP_LABEL_CONTACT_ADMIN));
            bShowError = true;                
            
            return NULL;
        }
        return NULL;
    }
    
    
    /* deleteRule 
     * Delete Rule and redirect to Rule List View VF Page
     * @params :
     * @return : PageReference
     */
    public PageReference deleteRule(){
        try{
            //delete if not null
            if(objRule != NULL){
                delete objRule;
            }
            return new PageReference(System.Label.HEP_URL_VIEW_RULE_LIST);
        }catch(Exception ex){
            return null;
        }
    }
}