/**
 * HEP_SKUDetailsController_Test --- Test class for HEP_SKUDetailsController
 * @author  Nidhin V K
 */
@isTest
private class HEP_SKUDetailsController_Test {

    @testSetup static void setup() { 
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Acquisitions', 'TV', true);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', NULL, NULL, 
                                        'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c objChildTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium', 'EMEA', 'Subsidiary', 
                                            objTerritory.Id, objTerritory.Id, 'EUR', 'BE', '49', NULL, true);
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo', 'Global', NULL, 
                                        objTerritory.Id, objLOB.Id, NULL, NULL, true);
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, 
                                                    objChildTerritory.Id, true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('SAMPLE123456', 'SAMPLE', 'Single', NULL, 
                                    objTerritory.Id, 'Approved', 'Master', false);
        objCatalog.Licensor__c = 'FOX';
        insert objCatalog;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, 
                                                    objPromotion.Id, NULL, true);
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '21708SDO', 'Sample SKU', 
                                        'Master', NULL, 'VOD', 'HD', true);
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKU.Id, 
                                        'Approved', 'Unlocked', true);
		
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_FoxRetail_SKUImage','HEP_Fetch_SKU_ImageDetails',
        								false,30,1,'Outbound-Pull', true, true, true);
       
    }
    
    @isTest static void testBasicMethods() {
        HEP_SKU_Master__c objSKU = [SELECT Id FROM HEP_SKU_Master__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_SKU_Details')); 
        System.currentPageReference().getParameters().put('skuId', objSKU.Id);
        Test.startTest();
        HEP_SKUDetailsController objCTRL = new HEP_SKUDetailsController();
        if(objCTRL.bHasPageAccess)
            System.assertEquals(NULL, objCTRL.checkPermissions());
        else{
            PageReference retURL = new PageReference('/apex/HEP_AccessDenied');
            retURL.setRedirect(true);
            System.assertEquals(retURL.getUrl(), objCTRL.checkPermissions().getUrl());
        }
        Test.stopTest();
    }
    
    @isTest static void test_getHeaderDetails() {
        HEP_SKU_Master__c objSKU = [SELECT Id, SKU_Number__c FROM HEP_SKU_Master__c LIMIT 1];

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_SKUDetailsController obj = new HEP_SKUDetailsController();
        obj.sSKUId = 'test';obj.sBinaryData = 'test';obj.sFileName = 'test';obj.sDocumentDescription = 'test';obj.sFileType = 'test';
        HEP_SKUDetailsController.Header objHeader = HEP_SKUDetailsController.getHeaderDetails(objSKU.Id,'');
        System.assertEquals(objSKU.Id,objHeader.sSKUId);
        HEP_SKUDetailsController.getImageUrl(objSKU.SKU_Number__c,'au');
        HEP_SKUDetailsController.getImageUrl(objSKU.Id,'au');
        Test.stopTest();
    }
}