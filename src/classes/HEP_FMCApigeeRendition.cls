/**
* HEP_FMCApigeeRendition -- Controller to get the Image details based on Thumbnail on Asset object 
* @author    Lakshman Jinnuri
*/
public class HEP_FMCApigeeRendition implements HEP_IntegrationInterface{ 
    /**
    * performTransaction -- Method to get Image details
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sImageResult;
        String sThumbnailId;
        String sAssetId;
        //Variables to hold the custom Settings data
        String sFMC_APIGEE_RENDITION = HEP_Utility.getConstantValue('FMC_APIGEE_RENDITION');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_SUCCESS_CODE = HEP_Utility.getConstantValue('HEP_SUCCESS_CODE');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_SOURCE_NAME = HEP_Utility.getConstantValue('HEP_SOURCE_NAME');
        String sHEP_SESSION_ID = HEP_Utility.getConstantValue('HEP_SESSION_ID');
        String sHEP_ACCESS_TOKEN = HEP_Utility.getConstantValue('HEP_ACCESS_TOKEN');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        if(objTxnResponse.sSourceId != null)
            sAssetId = objTxnResponse.sSourceId;
        list<HEP_Title_Asset__c> lstTitleAsset = new list<HEP_Title_Asset__c>();
        lstTitleAsset = [SELECT Id,Asset_Id__c,Asset_Name__c,Media_Id__c,Preview_Id__c,Thumbnail_Id__c,Title_EDM__c 
                         FROM HEP_Title_Asset__c 
                         WHERE Id =: sAssetId]; 
        //Gives the JSessionId and Accesstoken
        Map<string,string> sessionIdTokenId = HEP_FMC_TitleImages.HEP_getSessionIdAuthId();
   
        if(sHEP_SESSION_ID != null && sHEP_ACCESS_TOKEN != null && sHEP_TOKEN_BEARER != null && sessionIdTokenId.get(sHEP_SESSION_ID) != null 
            && sessionIdTokenId.get(sHEP_ACCESS_TOKEN) != null && sessionIdTokenId.get(sHEP_ACCESS_TOKEN).startsWith(sHEP_TOKEN_BEARER) 
            && objTxnResponse != null){
            HEP_Services__c objApigeeRedention;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Details from custom setting for Authentication
            if(sFMC_APIGEE_RENDITION != null)
                objApigeeRedention = HEP_Services__c.getInstance(sFMC_APIGEE_RENDITION);            
            //Acess token and JSessionId is passed to get the Preview Id and Thumbnail Id    
            if(objApigeeRedention != null && !lstTitleAsset.isEmpty()){
                if(lstTitleAsset[0] != null && lstTitleAsset[0].Preview_Id__c != null)
                objHttpRequest.setEndpoint(objApigeeRedention.Endpoint_URL__c + objApigeeRedention.Service_URL__c + '/' + lstTitleAsset[0].Preview_Id__c);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sessionIdTokenId.get(sHEP_ACCESS_TOKEN));
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setHeader('JSESSIONID',sessionIdTokenId.get(sHEP_SESSION_ID));
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                if(sHEP_SUCCESS_CODE != null){
                    if(objHttpResp.getStatusCode() == integer.ValueOf(sHEP_SUCCESS_CODE)){
                        //Gets the details from Foxipedia interface and stores in the wrapper 
                        Blob image = objHttpResp.getBodyAsBlob();
                        sImageResult = 'data:'+objHttpResp.getHeader('Content-Type')+';base64,'+EncodingUtil.base64Encode(image);
                        objTxnResponse.sResponse = sImageResult; //Response from callout
                        if(sHEP_Int_Txn_Response_Status_Success != null)
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                        objTxnResponse.bRetry = false;
                    }
                    else{
                        objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                        if(sHEP_FAILURE != null)
                            objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                        else    
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    }
                }           
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }
        }        
    }
}