/* Class Name   : HEP_RuleCriterionTriggerHandler
 * Description  :    
 * Created By   : Nidhin V K
 * Created On   : 07-26-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-26-2016        1000            Initial version
 *
 */
public class HEP_RuleCriterionTriggerHandler{
    
    /* beforeDelete
     * Block users from doing delete rule criteria operation id rule or rule version is active
     * @params : mapRuleCriteria
     * @return : 
     *
    */
    public static void beforeDelete(Map<Id, HEP_Rule_Criterion__c> mapRuleCriteria){
        
        //Get the Rule Details
        for(HEP_Rule_Criterion__c criterion : [SELECT 
                                                Id, 
                                                HEP_Rule__r.HEP_IsActive__c
                                              FROM
                                                HEP_Rule_Criterion__c
                                              WHERE
                                                Id IN :mapRuleCriteria.keySet()]){
            
            //Check for active rule
            if(criterion.HEP_Rule__r.HEP_IsActive__c){
                mapRuleCriteria.get(criterion.Id).addError(System.Label.HEP_LABEL_DEACTIVATE_RULE);
            }
        }
    }
}