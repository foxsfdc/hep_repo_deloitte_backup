/**
* HEP_FMCApigeeRendition_Test -- Test class for the HEP_FMCApigeeRendition for Foxipedia Interface. 
* @author    Balaji
*/

@isTest
Public class HEP_FMCApigeeRendition_Test{

    @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Title_Asset__c objAsset =  HEP_Test_Data_Setup_Utility.createTitleAsset('0c01d16451a8c1fbd9ccfd9a88aaa7ec1605dda6','AR_BQ_20184102061942.mxf',false);
        objAsset.Media_Id__c = '100618895';
        objAsset.Preview_Id__c = '0025d5c880119b96c1d9f853c416a2c8e5081e74';
        objAsset.Thumbnail_Id__c = 'c1860cd4c282b49df8447894eb564c6a693e5b0e';
        insert objAsset;
    }
     /**
    * Success_Test--  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Balaji
    */
    @isTest 
    static void Success_Test(){   
        HEP_Services__c objService = new HEP_Services__c();  
        HEP_Title_Asset__c objAsset = [select id from HEP_Title_Asset__c limit 1];
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objAsset.id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMCApigeeRendition demo = new HEP_FMCApigeeRendition();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
     /**
    * Failure_Test--  Test method for the failure response
    * @return  :  no return value
    * @author  :  Balaji
    */    
     @isTest 
    static void Failure_Test(){   
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Title_Asset__c objAsset = [select id from HEP_Title_Asset__c limit 1];
        objAsset.Media_Id__c = '100618895';
        objAsset.Preview_Id__c = '0025d5c880119b96c1d9f853c416a2c8e5081e74';
        objAsset.Thumbnail_Id__c = 'c1860cd4c282b49df844';
        update objAsset;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse(); 
        objTxnResponse.sSourceId = objAsset.id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMCApigeeRendition demo = new HEP_FMCApigeeRendition();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
     /**
    * Failure_Access_Token--  Access token Failure
    * @return  :  no return value
    * @author  :  Balaji
    */ 
    @isTest 
    static void Failure_Access_Token(){   
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Constants__c objconstant = new HEP_Constants__c();
        objconstant = [select id,Name from HEP_Constants__c where Name = 'HEP_SESSION_ID'];
        delete objconstant;
        Test.startTest();
        HEP_Title_Asset__c objAsset = [select id from HEP_Title_Asset__c limit 1];
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse(); 
        objTxnResponse.sSourceId = objAsset.id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMCApigeeRendition demo = new HEP_FMCApigeeRendition();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
}