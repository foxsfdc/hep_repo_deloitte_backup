/**
* HEP_Foxipedia_GenreWrapper -- Wrapper to store Genre details for foxipedia integration
* @author    Tejaswini Veliventi
*/

public with sharing class HEP_Foxipedia_GenreWrapper {

    public List< Data > data{get;set;}
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public Links links{get;set;}
    public List<Errors> errors { get; set; }
    
    
    public class Data {
        public Attributes attributes{get;set;}
        public String id{get;set;}  
        public String type{get;set;}  
        public Relationships relationships{get;set;}
    }
    
    
    /**
    * Attributes -- Class to hold Attribute details 
    * @author    Tejaswini Veliventi
    */
    public class Attributes {
        public String type { get; set; } 
        public String foxId { get; set; }    
        public String categoryId { get; set; }   
        public String categoryTypeCode { get; set; } 
        public String categoryTypeDescription { get; set; }
        public String categoryCode { get; set; } 
        public String categoryDescription { get; set; }
        public String categorySortOrder { get; set; }
        public String primaryTitleIndicator { get; set; }
        public String foxVersionId { get; set; } 
        public String countryDescription { get; set; }   
        public String countryCode { get; set; }  
        public String mediaDescription { get; set; } 
        public String mediaCode { get; set; }    
        public String titleVersionTypeCode { get; set; } 
        public String titleVersionTypeDescription { get; set; }
        public String titleVersionDescription { get; set; }  
        public String jdeVersionId { get; set; }
        public String rowIdObject {get;set;}
        public String rowIdTitleVersion{get;set;}
    }
    
    
    /**
    * Relationships -- Class to hold Relationships details 
    * @author    Tejaswini Veliventi
    */
    public class Relationships {
        public Title title{get;set;}
    }
    
    
    /**
    * Title -- Class to hold links details 
    * @author    Tejaswini Veliventi
    */
    public class Title {
        public Links links{get;set;}
    }
    
    
    /**
    * Links -- Class to hold related details 
    * @author    Tejaswini Veliventi
    */
    public class Links {
        public String related{get;set;} 
    }
    
    
    /**
    * Links -- Class to hold related details 
    * @author    Tejaswini Veliventi
    */
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }

}