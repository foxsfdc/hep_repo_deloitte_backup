public class HEP_PromotionSpend_TriggerHander extends TriggerHandler{

List<ID> lstIds = new List<ID>();

    public override void afterInsert(){
        for(SObject objSObj : Trigger.new){
        HEP_Market_Spend_Detail__c objSpendDetail = (HEP_Market_Spend_Detail__c)objSObj;
        lstIds.add(objSpendDetail.id);
        }
        System.debug('Inside After Insert Handler****');
        callQueuableMethod(Trigger.newMap.keyset());
        //if(HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND') != null) 
        //System.enqueueJob(new HEP_Siebel_Queueable(JSON.serialize(lstIds),HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND')));

    }

    public override void afterUpdate(){
        for(SObject objSObj : Trigger.new){
        HEP_Market_Spend_Detail__c objSpendDetail = (HEP_Market_Spend_Detail__c)objSObj;
        lstIds.add(objSpendDetail.id);
        }
        System.debug('Inside After Update Handler****');
        if(HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND') != null) 
        {
            Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Market_Spend_Detail__c', 'Market_Spend_Field_Set');
            System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate); 
            if(checkFieldUpdate)
             callQueuableMethod(Trigger.newMap.keyset());   
            //{System.enqueueJob(new HEP_Siebel_Queueable(JSON.serialize(lstIds),HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND')));}
        }    
    }

    public static void callQueuableMethod(set<Id> setPromotionIds){
        System.debug('inside callQueuableMethod');    
        Boolean valueFalse = false;    
        String sPromotionTypeGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        String sPromotionTypeNational = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        List<String> lstGlobalNationalPromo = new List<String>();
        lstGlobalNationalPromo.add(sPromotionTypeGlobal);
        lstGlobalNationalPromo.add(sPromotionTypeNational);
        String sMarketSpendDetailType = HEP_Utility.getConstantValue('HEP_SPEND_DETAIL_STATUS_CURRENT');
        List<Id> lstPromotionSpendIds = new List<Id>();
        for(HEP_Market_Spend_Detail__c objMarketSpend : [Select Id,HEP_Market_Spend__r.Promotion__r.Promotion_Type__c FROM HEP_Market_Spend_Detail__c WHERE HEP_Market_Spend__r.Promotion__r.Promotion_Type__c IN: lstGlobalNationalPromo and Type__c =: sMarketSpendDetailType AND Id IN :setPromotionIds AND Budget__c != NULL]){
            lstPromotionSpendIds.add(objMarketSpend.Id);
        }   
        System.debug('lstPromotionSpendIds'+lstPromotionSpendIds);
        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable()) && !lstPromotionSpendIds.isEmpty()){
            System.enqueueJob(new HEP_Siebel_Queueable(JSON.serialize(lstPromotionSpendIds) , HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND')));
        }
    }


}