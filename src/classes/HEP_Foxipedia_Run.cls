/**
* HEP_Foxipedia_Run -- class implements the HEP_IntegrationInterface and contains all the logic for performing foxipedia integration to fetch Data.
* @author    Balaji
*/
public with sharing class HEP_Foxipedia_Run implements HEP_IntegrationInterface{

    /**
    * performTransaction -- makes the callout and updates the response object
    * @param HEP_InterfaceTxnResponse object 
    * @return no return value
    * @author Balaji  
    */

    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sAccessToken;
        HEP_Services__c objServiceDetails;
        HEP_Foxipedia_RunWrapper objWrapper = new HEP_Foxipedia_RunWrapper();

        String sHEP_FOXIPEDIA_OAUTH = HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_FOXIPEDIA_RUN= HEP_Utility.getConstantValue('HEP_FOXIPEDIA_RUN'); 
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 

        if(sHEP_FOXIPEDIA_OAUTH != null )
        sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_FOXIPEDIA_OAUTH);
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sFoxId = objTxnResponse.sSourceId;
            HTTPRequest objHttpRequest = new HTTPRequest();
            if(sHEP_FOXIPEDIA_RUN!= null)
            objServiceDetails = HEP_Services__c.getInstance(sHEP_FOXIPEDIA_RUN);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?foxId=' + sFoxId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('system',System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString()+'\n Body : \t'+objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                system.debug('objHttpResp '+objHttpResp );
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(sHEP_SUCCESS != null && objHttpResp.getStatus().equals(sHEP_SUCCESS)){                   
                    System.debug('objHttpResp.getBody() JSON :' + objHttpResp.getBody() );
                    objWrapper = (HEP_Foxipedia_RunWrapper)JSON.deserialize(objHttpResp.getBody(),HEP_Foxipedia_RunWrapper.class);    
                    if(sHEP_FAILURE != null && objWrapper.data.size() == 0 ){
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        objTxnResponse.bRetry = true;                        

                    }else{
                    if(sHEP_Int_Txn_Response_Status_Success != null)         
                        objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                        objTxnResponse.bRetry = false;                   
                    }    
                }
                else{
                    if(sHEP_FAILURE != null)
                        objTxnResponse.sStatus = sHEP_FAILURE;                       
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    }  
                }
            }
        else{
            system.debug('inside else');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;          
            }    
        }   
    }    
}