/**
 * HEP_InboundEmailHandler --- class to process Inbound emails in HEP
 * @author    Nishit Kedia
 */
 global class HEP_InboundEmailHandler implements Messaging.InboundEmailHandler {

	
	//Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
	
    /**
    * handleInboundEmail --- To ovverride the interface method for processing Inbound email
    * @param email is the Inbound email coming to Salesforce
    * @param envelope is the Inbound envelope to Salesforce 
    * @return nothing
    * @author    Nishit Kedia
    */ 
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email , Messaging.Inboundenvelope envelope){  
     
        //----------------------------------//
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        try{
            //------------------------------//
            //Invoke the Dating handler Class to process records on Dating.
            Boolean bResponse  =  false;
            bResponse = invokeEmailHandler(email , envelope);
            if(bResponse){
                result.success = bResponse;
                
                } else{
                    result.success = bResponse;
                    result.message = HEP_Utility.getConstantValue('HEP_FAILURE'); 
                    result.message+= HEP_Utility.getConstantValue('HEP_EMAIL_ERROR'); 
                }
            //-----------------------------//
            } catch(Exception e){
                result.success = false;
                result.message = HEP_Utility.getConstantValue('HEP_FAILURE'); 
                result.message+= HEP_Utility.getConstantValue('HEP_EMAIL_ERROR'); 
            }
            
            return result;
        }
    //-------------------------------------//


    global static Boolean invokeEmailHandler(Messaging.InboundEmail email,  Messaging.Inboundenvelope envelope){
        Boolean bResponse = false;
        String sQuery = '';
        String sApproverId = '';
        String sRejectedReasonComments = '';
        
        Boolean bApprovalStatus = false;
        
        if(email != null){
            sQuery = 'SELECT id,Status__c,IsActive__c,LastModifiedById,Comments__c,Approver__c,LastModifiedDate,Approver_Role__c,Search_Id__c FROM HEP_Record_Approver__c WHERE IsActive__c = true AND Record_Status__c =:sACTIVE ';
            
                //if(email.subject.contains(sSpend))
                sApproverId = email.subject.substringBetween('(' , ')').trim();
                
                if(sApproverId!=null){
                    sQuery += ' AND Id=\''+sApproverId+'\'';
                }
                sQuery += ' LIMIT 1';
                
                if((email.plainTextBody.contains(HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'))||email.plainTextBody.contains(HEP_Utility.getConstantValue('HEP_APPROVE_TEXT'))) && email.subject.contains(HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'))){
                    bApprovalStatus = true;
                    }else{
                        sRejectedReasonComments= email.plainTextBody.substringAfter(HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED'));
                    }
                    
                    List<HEP_Record_Approver__c> lstRecordApprover = Database.Query(sQuery);
                    
                    List<HEP_Inbound_Email__c> lstofInboundEmailData = new List<HEP_Inbound_Email__c>(); 
                    
                    
                    if(lstRecordApprover != null && !lstRecordApprover.isEmpty()){
                        
                        HEP_Record_Approver__c ObjRecordApprover=lstRecordApprover.get(0);
                        String sRolesSearchId = ObjRecordApprover.Search_Id__c;
                        
                        if(String.isNotBlank(sRolesSearchId)){
                            
                            String sReceivedFromEmailId=email.fromAddress ;
                            for (HEP_User_Role__c objUserRole: [Select Id,
                                Role__c,                                                    
                                Search_Id__c,
                                Territory__c,
                                User__c,
                                User__r.Email
                                FROM
                                HEP_User_Role__c
                                WHERE
                                Search_Id__c =: sRolesSearchId
                                AND Record_Status__c =:sACTIVE 
                                ]){ 
                                if(objUserRole.User__r.Email.equalsIgnoreCase(sReceivedFromEmailId)){
                                    
                                    if(ObjRecordApprover.Status__c.equals(HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING'))){
                                        if(bApprovalStatus){
                                            ObjRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                                        }
                                        else{
                                            ObjRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
                                            ObjRecordApprover.Comments__c = sRejectedReasonComments;
                                        }
                                        ObjRecordApprover.Approver__c = objUserRole.User__c;
                                    }
                                    break;
                                }   
                            }
                            
                            Database.SaveResult objSaveResult = Database.update(ObjRecordApprover, false);
                            
                            if(objSaveResult.isSuccess()){
                                System.debug('objSaveResult--->' + objSaveResult);    
                                HEP_Inbound_Email__c inboundEmailrecord = new HEP_Inbound_Email__c();
                                if(ObjRecordApprover != null){
                                    inboundEmailrecord.Email_Address__c = email.fromAddress;
                                    inboundEmailrecord.Email_Body__c = email.plainTextBody;
                                    inboundEmailrecord.Record_Id__c = String.valueOf(objSaveResult.getId());
                                    inboundEmailrecord.Object_Name__c = objSaveResult.getId().getSObjectType().getDescribe().getName();
                                    inboundEmailrecord.Email_Subject__c = email.subject;
                                    if(ObjRecordApprover.Status__c.equals(HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED')) || ObjRecordApprover.Status__c.equals(HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED'))){
                                        inboundEmailrecord.Processing_Status__c = HEP_Utility.getConstantValue('HEP_InboudEmail_Processed');
                                        }else{
                                            inboundEmailrecord.Processing_Status__c = HEP_Utility.getConstantValue('HEP_InboundEmail_NotProcessed');
                                        }
                                    }   
                                    system.debug('@@@@@@@@@@@@'+inboundEmailrecord);
                                    insert inboundEmailrecord;  
                                }
                                bResponse = true;
                            }
                        }
                        
                    }
                    return bResponse;
                }
            }