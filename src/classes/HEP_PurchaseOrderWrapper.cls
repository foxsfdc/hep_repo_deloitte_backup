/**
* HEP_PurchaseOrderWrapper -- Purchase Order Wrapper to store the Purchase order details from JDE And E1 Interfaces 
* @author    Lakshman Jinnuri
*/
public class HEP_PurchaseOrderWrapper{
    public List<purchaseOrders> purchaseOrders;
    public String calloutStatus;
    public String TxnId;
    public List<Errors> errors;
    
    /**
    * purchaseOrders -- Class to hold purchaseOrders details 
    * @author    Lakshman Jinnuri
    */
    public class PurchaseOrders{
       //common fields for both interfaces JDE and E1
        public String OPEN;
        public String RUSHCODE;
        public String POLINE_NO;
        public String NAME;   
        public String PO_NO;
        public String COUNTRY;    
        public String OBJECT_NO;   
        public String ITEM_AMT;    
        public String LINE_DESC; 
        public String PROMO;     
        public String ACTUAL; 
        public String REQ_DATE;
        public String POSTATUS;
        public String NETWORKID;  
        public String RUSHGPCODE;  
        public String FORMAT;
        public String REQUESTOR;  
        public String POTYPE;   
        public String VENDOR; 
        public String CURRENCY_TYPE; 
    }
    
    /**
    * Errors -- Class to hold related details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }   
}