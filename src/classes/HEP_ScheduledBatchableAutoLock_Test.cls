/**
* HEP_ScheduledBatchableAutoLock_Test -- Test class for the HEP_ScheduledBatchableAutoLock Batch
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_ScheduledBatchableAutoLock_Test{
    /**
    * ScheduleTest --  Test method to Scheduke the Auto Lock Module 
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    public static testmethod void ScheduleTest(){
        Test.StartTest();
        HEP_ScheduledBatchableAutoLock schedule = new HEP_ScheduledBatchableAutoLock();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '30 49 07 08 04 ?';
        String jobID = System.schedule('AutoLockBatch', sch, schedule);
        CronTrigger objCronTrigger = [SELECT Id,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id =: jobId];
        System.assertEquals(sch,objCronTrigger.CronExpression);
        Test.stopTest();
    }
}