/**
 * HEP_Generate_SKU_Number ---  Apex class to Generate SKU number for ESCO
 * @author    Nishit Kedia
 */
public class HEP_Generate_SKU_Number{
  
  public static String sACTIVE;
    
    static{
       sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * Algorithm to Generate the SKU Number for INT'L territories for ESCO Integrations
    * @param  setSkuIds : Sku record Id
    * @return String
    * @author Nishit Kedia
    */
   
    public static Boolean generateUniqueSKUNumber(Set<Id> setSkuRecordId ){
        Boolean bIsSuccessfull = false;
        try{
            //Blank check
            if(setSkuRecordId != null && !setSkuRecordId.isEmpty()){
              //Query SKU master with the Sku Record id in parameter
                List<HEP_SKU_Master__c> lstSKUs = [Select    Id,
                                                            Channel__c,
                                                            Format__c,
                                                            SKU_Number__c,
                                                            Media_Type__c,
                                                            SKU_Configuration__c,
                                                            ESCO_Sku_Config__c,
                                                            SKU_Type__c,
                                                            Type__c,
                                                            Territory__c,
                                                            Territory__r.Name,
                                                            Territory__r.SKU_Interface__c,
                                                            Catalog_Master__c,
                                                            Catalog_Master__r.CatalogId__c,
                                                            Catalog_Master__r.Licensor_Type__c,
                                                            Catalog_Master__r.Licensor__c,
                                                            Record_Status__c,
                              (Select id,
                                  Region__c,
                                  Region__r.Name,
                                  SKU_Master__c
                                  from
                                  SKU_Prices__r 
                                  Where
                                  Record_Status__c =:sACTIVE
                              )
                                                            from
                                                            HEP_SKU_Master__c
                                                            where Id  In: setSkuRecordId
                                                            AND Record_Status__c =:sACTIVE];
                              
        if(lstSKUs != null&& !lstSKUs.isEmpty()){
          //Set to store the Catalog ids of the SKu(s) Linked
          Set<Id> setCatalogId = new Set<Id>();
          
          //Set to store the territory ids of the SKu(s) Linked
          Set<Id> setSkuTerritoryIds = new Set<Id>();
          
          //Set to store the UNIQUE SKU# Generated from Below Algo
          Set<String> setSkuUniqueNumbers = new Set<String>();        
          
          // Map to store GenreCount for Sku Linked to the Catalog
          Map<Id,Integer> mapLinkedCatalogGenre = new Map<Id,Integer>();
          
          // Map to store Territory for SKU
          Map<Id,Integer> mapSkuTerritories= new Map<Id,Integer>();
          
          //Generate the Map from HEP LOV.
          Map<String, Map<String,String>> mapESCOSKUListOfValues = generateESCOSKUMapFromLOV();
          
          //Map which contains Unique SKU number generated and respective record reference.
          Map<String,List<HEP_SKU_Master__c>> mapToBeUpdatedWithUniqueSKU = new Map<String,List<HEP_SKU_Master__c>>();
          
          //populate set
          for(HEP_SKU_Master__c objSkuRequestRecord : lstSKUs){
            setCatalogId.add(objSkuRequestRecord.Catalog_Master__c);
            setSkuTerritoryIds.add(objSkuRequestRecord.Territory__c);
          }             
                           
          
          //Query the Genre Object linked to Catalog
          if(setCatalogId != null && !setCatalogId.isEmpty()){
            
            //Store the count of Genre in Map for every Linked Catalog                        
            for(HEP_Catalog_Genre__c objGenre :[Select   Id,
                                  Catalog__c
                                  from 
                                  HEP_Catalog_Genre__c 
                                  where 
                                  Catalog__c IN:setCatalogId 
                                  AND Record_Status__c =:sACTIVE
                              ]){
              
              if(mapLinkedCatalogGenre.containsKey(objGenre.Catalog__c)){
                Integer iCount = mapLinkedCatalogGenre.get(objGenre.Catalog__c);
                mapLinkedCatalogGenre.put(objGenre.Catalog__c,iCount+1);
              }else{
                mapLinkedCatalogGenre.put(objGenre.Catalog__c,1);
              }
            }
            System.debug('Genre Map--->'+ mapLinkedCatalogGenre);
          }
                    
          
          //Query the Territory linked to SKU
          if(setSkuTerritoryIds != null && !setSkuTerritoryIds.isEmpty()){
            List<HEP_Territory__c> lstTerritoryCount = [Select   Id,
                                      Parent_Territory__c
                                      from HEP_Territory__c
                                      where
                                      Parent_Territory__c = :setSkuTerritoryIds
                                      OR
                                      Id = :setSkuTerritoryIds 
                                      
                                  ];
            //Store the count of Regions in Map for every Linked Territory                          
            for(HEP_Territory__c objTerritory :lstTerritoryCount){
              String sterritory ='';
              if(objTerritory.Parent_Territory__c != null){
                sterritory = objTerritory.Parent_Territory__c;
              } else{
                sterritory = objTerritory.Id;
              }
              
              if(mapSkuTerritories.containsKey(sterritory)){
                Integer iCount = mapSkuTerritories.get(sterritory);
                mapSkuTerritories.put(sterritory,iCount+1);
              }else{
                mapSkuTerritories.put(sterritory,1);
              }
                                
            
            }
            System.debug('Territories Map--->'+ mapSkuTerritories);
          }
          

          //Main processing
          for(HEP_SKU_Master__c objSkuRequestRecord : lstSKUs){
           
            // Buisness Validation - Enabled for Physical SKU Requests only, ignore rules if Digital 
            String sSkuRequestType = objSkuRequestRecord.Type__c;
            if(objSkuRequestRecord.Media_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PHYSICAL_MEDIA_TYPE')) && sSkuRequestType.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_REQUEST'))){
              //Enabled for subset of International Territories only, ignore rules if ESCO SKU Request interface is not supported for the International Territory
              String sTerritoryName = objSkuRequestRecord.Territory__r != null ? objSkuRequestRecord.Territory__r.Name :'';
              String sSkuInterfaceName = objSkuRequestRecord.Territory__r != null ? objSkuRequestRecord.Territory__r.SKU_Interface__c :'';
              String sSkuTerritoryId = objSkuRequestRecord.Territory__c;
              if(sSkuInterfaceName.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_ESCO'))){
                //SKU Request's Catalog Number, Channel, Format, and Price Grade must be populated
                //Territory Catalog must have Licensor Type populated (not null)
                String sCatalogNumber =  objSkuRequestRecord.Catalog_Master__r.CatalogId__c;
                String sSkuChannel = objSkuRequestRecord.Channel__c;
                String sSkuFormat =  objSkuRequestRecord.Format__c;
                String sSkuConfiguration = objSkuRequestRecord.ESCO_Sku_Config__c;
                String sLicensorType = objSkuRequestRecord.Catalog_Master__r.Licensor_Type__c;
                String sCatalogId = objSkuRequestRecord.Catalog_Master__c;
                if(String.isNotBlank(sCatalogNumber) && String.isNotBlank(sSkuChannel) && String.isNotBlank(sSkuFormat)  
					//&& String.isNotBlank(sLicensorType)
				&& String.isNotBlank(sCatalogId)){
                  
                  //Fetch the Number of Genres Linked to Catalog
                  Integer iCatalogGenreCount = 0;
                  if(mapLinkedCatalogGenre != null && mapLinkedCatalogGenre.containsKey(sCatalogId)){
                    iCatalogGenreCount = mapLinkedCatalogGenre.get(sCatalogId);
                  }
                  
                  
                  //Territory Catalog must have at least one Genre
                  //if(iCatalogGenreCount >= 1 ){
					if(iCatalogGenreCount >= 0 ){
                    List<HEP_SKU_Price__c> lstSkuPriceregions = new List<HEP_SKU_Price__c>();
                    Set<String> setUniqueSkuRegions = new Set<String>();
                    //Calculate Total No. of Price Region records Created
                    if(objSkuRequestRecord.SKU_Prices__r != null){
                      //lstSkuPriceregions = objSkuRequestRecord.SKU_Prices__r;
                      for(HEP_SKU_Price__c objPriceRegion : objSkuRequestRecord.SKU_Prices__r){
                        if(!setUniqueSkuRegions.contains(objPriceRegion.Region__r.Name)){
                          lstSkuPriceregions.add(objPriceRegion);
                          setUniqueSkuRegions.add(objPriceRegion.Region__r.Name);
                        }                        
                      }
                    }
                    
                    Integer iRegionsCount = 0;
                    //Calculate regions under territory if any
                    if(mapSkuTerritories != null && mapSkuTerritories.containsKey(sSkuTerritoryId)){
                       iRegionsCount = mapSkuTerritories.get(sSkuTerritoryId);
                    }
                                      
                     //---------------- [Unique SKU NUMBER GENERATION STARTS]----------------------//
                     /*
                     @ The SKU number, made up of 12 alphanumeric characters, e.g. N056901BSP01 will be constituted of the following
                     */
                     String sUniqueSkuNumber = '';
                     
                     // --------------- The Territory (1 letter):[Part 1] [STARTS]----------//
                     String sType_TerritoryCODE = HEP_Utility.getConstantValue('HEP_ESCO_SKU_TERRITORY');                                 
                     if(mapESCOSKUListOfValues.containsKey(sType_TerritoryCODE) && mapESCOSKUListOfValues.get(sType_TerritoryCODE) != null){
                      Map<String,String> mapEscoTerritory = mapESCOSKUListOfValues.get(sType_TerritoryCODE);
                      if(mapEscoTerritory != null && mapEscoTerritory.containsKey(sTerritoryName)){
                        sUniqueSkuNumber += mapEscoTerritory.get(sTerritoryName);
                      }
                     }
                     //--------------- The Territory (1 letter):[Part 1] [ENDS]------------//
                     
                     //--------------- The Catalog Number (6 digits) [Part 2] [STARTS] ----//
                     if(String.isNotBlank(sCatalogNumber)){
                      //If less than 6 characters in the Catalog number, add as many leading 0's as required to make the number up to 6 characters
                      if(sCatalogNumber.length() < 6){
                        for(Integer i=1 ; i <= 6 - sCatalogNumber.length(); i++){
                          sUniqueSkuNumber += '0' ;
                        }
                        
                      }
                      sUniqueSkuNumber += sCatalogNumber;
                     }
                     //---------------The Catalog Number (6 digits) [Part 2] [STARTS] ---- //
                     
                     // --------------- The Format (1 character):[Part 3] [STARTS]----------//
                     String sType_FormatCODE = HEP_Utility.getConstantValue('HEP_ESCO_SKU_FORMAT'); 
                     String sType_CONFIGCODE = HEP_Utility.getConstantValue('HEP_ESCO_SKU_CONFIG');
                                                    
                      if (mapESCOSKUListOfValues.containsKey(sType_CONFIGCODE) && mapESCOSKUListOfValues.get(sType_CONFIGCODE) != null){
	                      Map<String,String> mapEscoSKUConfig = mapESCOSKUListOfValues.get(sType_CONFIGCODE);
	                      if(String.isNotBlank(sSkuConfiguration) && mapEscoSKUConfig != null && mapEscoSKUConfig.containsKey(sSkuConfiguration)){
	                        sUniqueSkuNumber += mapEscoSKUConfig.get(sSkuConfiguration);
	                      }else{
		                     if (mapESCOSKUListOfValues.containsKey(sType_FormatCODE) && mapESCOSKUListOfValues.get(sType_FormatCODE) != null){
		                      Map<String,String> mapEscoFormat = mapESCOSKUListOfValues.get(sType_FormatCODE);
		                      if(String.isNotBlank(sSkuFormat) && mapEscoFormat != null && mapEscoFormat.containsKey(sSkuFormat)){
		                        sUniqueSkuNumber += mapEscoFormat.get(sSkuFormat);
		                      }
		                     }
	                     }
                     }
                     //--------------- The Format (1 character):[Part 3] [ENDS]------------//
                     
                     // --------------- The Division Code (1 letter):[Part 4] [STARTS]----------//
                     String sType_DivisionCODE = HEP_Utility.getConstantValue('HEP_ESCO_SKU_DIVISION');                                
                     if(mapESCOSKUListOfValues.containsKey(sType_DivisionCODE) && mapESCOSKUListOfValues.get(sType_DivisionCODE) != null){
                      Map<String,String> mapEscoDivision = mapESCOSKUListOfValues.get(sType_DivisionCODE);
                      if(String.isNotBlank(sSkuChannel) && mapEscoDivision != null && mapEscoDivision.containsKey(sSkuChannel)){
                        sUniqueSkuNumber += mapEscoDivision.get(sSkuChannel);
                      }
                     }
                     //--------------- The Division Code (1 letter):[Part 4] [ENDS]------------//
                     
                     // --------------- The Region Code (1 letter):[Part 5] [STARTS]----------//
                     if(lstSkuPriceregions != null && !lstSkuPriceregions.isEmpty() && iRegionsCount != null){
                    String sType_RegionCODE = HEP_Utility.getConstantValue('HEP_ESCO_SKU_REGION'); 
                      if(lstSkuPriceregions.size() == iRegionsCount){
                        sUniqueSkuNumber += 'P'; //All Regions (Pan)
                      } else if(lstSkuPriceregions.size() > 1 && lstSkuPriceregions.size() < iRegionsCount){
                        String sType_GroupRegion = HEP_Utility.getConstantValue('HEP_ESCO_SKU_GROUP_REGION');
                        if(mapESCOSKUListOfValues.containsKey(sType_GroupRegion) && mapESCOSKUListOfValues.get(sType_GroupRegion) != null){
                          Map<String,String> mapGroupedEscoRegions = mapESCOSKUListOfValues.get(sType_GroupRegion);
                          //check if there is an Common Region which is Grouped as per Mapping
                          Boolean bIsCommonRegionSet = false;
                          for(HEP_SKU_Price__c objSkuPriceregion : lstSkuPriceregions){
                            if(objSkuPriceregion.Region__r != null){
                              if(mapGroupedEscoRegions.containsKey(objSkuPriceregion.Region__r.Name)){
                                bIsCommonRegionSet = true;
                                sUniqueSkuNumber += mapGroupedEscoRegions.get(objSkuPriceregion.Region__r.Name);
                                break;
                              }
                            }
                          }
                          
                          //Set the Value to Multi Region only if bIsCommonRegionSet is set to false
                          if(!bIsCommonRegionSet){
                            sUniqueSkuNumber += 'M'; // Multi-regions (M)
                          }
                        }
                        
                      } else if(lstSkuPriceregions.size() == 1){
                         HEP_SKU_Price__c objSkuPriceregion = lstSkuPriceregions.get(0);
                         String sRegionName = objSkuPriceregion.Region__r.Name;                                                   
                         if(mapESCOSKUListOfValues.containsKey(sType_RegionCODE) && mapESCOSKUListOfValues.get(sType_RegionCODE) != null){
                          Map<String,String> mapEscoRegion = mapESCOSKUListOfValues.get(sType_RegionCODE);
                          if(String.isNotBlank(sRegionName) && mapEscoRegion != null && mapEscoRegion.containsKey(sRegionName)){
                            sUniqueSkuNumber += mapEscoRegion.get(sRegionName);
                          }
                         }
                      }
                     }
                     // --------------- The Region Code (1 letter):[Part 5] [ENDS]----------//
                     System.debug('SKU Number------------>'+ sUniqueSkuNumber );
                     //---------------- [Unique SKU NUMBER GENERATION ENDS]------------------------//                                                   
                     setSkuUniqueNumbers.add(sUniqueSkuNumber +'%' );
                     
                     //Put the Same Sku formats generated and List of SKu masters linked in map 
                     if(mapToBeUpdatedWithUniqueSKU.containsKey(sUniqueSkuNumber)){
                      mapToBeUpdatedWithUniqueSKU.get(sUniqueSkuNumber).add(objSkuRequestRecord);
                     }else{
                       mapToBeUpdatedWithUniqueSKU.put(sUniqueSkuNumber,new List<HEP_SKU_Master__c>{objSkuRequestRecord});
                     }
                     
                     System.debug('Map of Sku-->'+mapToBeUpdatedWithUniqueSKU);
                  }                                                                              
                }                       
              }
            }
           }
           
          //UPDATE SKU MASTER WITH UNIQUE SKU#:
           
          //The SKU number, made up of 12 alphanumeric characters
          if(setSkuUniqueNumbers != null && !setSkuUniqueNumbers.isEmpty()){
            //Query the SKU database to check whether a similar SKU Number generated before or not
            Map<String,Integer> mapExistingSimilarSKUCount = generateSmartSKUCountMap(setSkuUniqueNumbers);
            
            if(mapExistingSimilarSKUCount != null && !mapToBeUpdatedWithUniqueSKU.isEmpty()){
              for(String sUniqueSkuNumber : mapToBeUpdatedWithUniqueSKU.keySet() ){
                
                //copy the Sku number in a variable
                String sUniqueSkuFormatCopy = sUniqueSkuNumber;
                Integer iCount = 0;
                
                //get the Last SKU suffix from DB and update the count to be reflected for next Incoming SKU
                if(mapExistingSimilarSKUCount.containsKey(sUniqueSkuNumber)){
                  iCount = mapExistingSimilarSKUCount.get(sUniqueSkuNumber);
                  iCount += 1;
                }else{                
                  iCount = 1;
                }
                
                //update the Linked Sku object records from the aove suffix counter
                if(mapToBeUpdatedWithUniqueSKU.containsKey(sUniqueSkuNumber)){
                  for(HEP_SKU_Master__c objSKURecord :mapToBeUpdatedWithUniqueSKU.get(sUniqueSkuNumber)){
                    
                    /*if(iCount < 9){
                      sUniqueSkuNumber +='0';
                    }
                    objSKURecord.SKU_Number__c = sUniqueSkuNumber + iCount;
                    */
                    
                    if(iCount < 10){
                      //sUniqueSkuNumber +='0';
					  objSKURecord.SKU_Number__c = sUniqueSkuNumber +'0'+iCount;
                    }else{
						objSKURecord.SKU_Number__c = sUniqueSkuNumber + iCount;
					}
                    //Updating the counter as there may be same SKU formats generated for multiple SKUs
                    iCount++;
                  }
                }
                //Re-Updation of Map is Required to reflect the most recent counter for SKU format
                mapExistingSimilarSKUCount.put(sUniqueSkuFormatCopy,iCount);
              }
            }
            
            //UPDATE SKU REQUEST WITH UNIQUE SKU#:
            if(!mapToBeUpdatedWithUniqueSKU.isEmpty()){
              
              List<HEP_SKU_Master__c> lstSkuRequestRecord = new List<HEP_SKU_Master__c>();
              for(String sKey :mapToBeUpdatedWithUniqueSKU.keySet()){
                lstSkuRequestRecord.addAll(mapToBeUpdatedWithUniqueSKU.get(sKey));
              }
              System.debug('List of SKU master -->'+ lstSkuRequestRecord);
              update lstSkuRequestRecord;
            }
            
            System.debug('Final SKU number  generated ---->' + mapToBeUpdatedWithUniqueSKU.keySet());
            bIsSuccessfull = true;          
          }           
        }
            } 
        } catch(Exception objException){
            HEP_Error_Log.genericException('Exception occured when Updating Promotion SKu','DML Errors',objException,'HEP_Generate_SKU_Number','generateUniqueSKUNumber',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }   
        return bIsSuccessfull;   
    }
    
    /**
    * generateSmartSKUCountMap: Build Map from HEP list of Values
  * @params  setSkuUniqueNumbers : generated SKu like number formats
    * @return Map<String,Integer> with Sku number as key and count in database
    * @author Nishit Kedia
    */
    public static Map<String,Integer>  generateSmartSKUCountMap(Set<String> setSkuUniqueNumbers){
      //Map to hold existing count of Sku that matches the smart skus generated
      Map<String,Integer> mapExistingSimilarSKUCount = new Map<String,Integer>();
      if(!setSkuUniqueNumbers.isEmpty()){      
       
      List<HEP_SKU_Master__c> lstExistingSameSkuNumber = [Select   Id,
                                    SKU_Number__c
                                    from
                                    HEP_SKU_Master__c
                                    where SKU_Number__c LIKE: setSkuUniqueNumbers
                                    //AND Type__c =: HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_MASTER')
                                ];
         
        if(lstExistingSameSkuNumber != null && !lstExistingSameSkuNumber.isEmpty()){
          System.debug('List lstExistingSameSkuNumber size--->'+lstExistingSameSkuNumber.size());
           for(HEP_SKU_Master__c objSKU : lstExistingSameSkuNumber){
	          if(String.isNotBlank(objSKU.SKU_Number__c)){
	            //split the first 10 characters
	            //[Code change: Extract the Sku number excluding the Last 2 chaarcters as They will conatin Suffix.]
	            String sUniqueSkuNumberExisting = objSKU.SKU_Number__c;
	            String sSmartSkunumber = '';
	            Integer iPresentSkuSuffix = 1;
	            if(sUniqueSkuNumberExisting.length() <= 12){
	            	sSmartSkunumber = sUniqueSkuNumberExisting.substring(0,sUniqueSkuNumberExisting.length() - 2);
	            	iPresentSkuSuffix = Integer.valueOf(sUniqueSkuNumberExisting.substring(sUniqueSkuNumberExisting.length() - 2));
	            }else{
	            	sSmartSkunumber = sUniqueSkuNumberExisting.substring(0,10);
	            	iPresentSkuSuffix = Integer.valueOf(sUniqueSkuNumberExisting.substring(sUniqueSkuNumberExisting.length() - 3));
	            }
	            System.debug('Smart SKU#------->'+sSmartSkunumber);
	            if(mapExistingSimilarSKUCount.containsKey(sSmartSkunumber)){
	              Integer iExistingSkuSuffix = mapExistingSimilarSKUCount.get(sSmartSkunumber);
	              if(iExistingSkuSuffix < iPresentSkuSuffix){
	              	mapExistingSimilarSKUCount.put(sSmartSkunumber,iPresentSkuSuffix);
	              }	              
	            }else{
	              mapExistingSimilarSKUCount.put(sSmartSkunumber,iPresentSkuSuffix);
	            }          
	          }          
           }
        }
      }
      System.debug('Existing SKU map  built--->'+mapExistingSimilarSKUCount);
      return mapExistingSimilarSKUCount;                                    
    }
  
  /**
    *generateESCOSKUMapFromLOV: Build Map from HEP list of Values
    * @param Nothing
  * @return 2d map with Lov mapping and of Type and map (parentvalue as key and value as value)
    * @author Nishit Kedia
    */
    public static Map<String,Map<String,string>>  generateESCOSKUMapFromLOV(){
      
       Map<String,Map<String,String>> mapESCOSKUListOfValues = new Map<String,Map<String,String>>();
       List<HEP_List_Of_Values__c> lstSkuMappingValues = [Select Id,
                                     Name,
                                     Name_And_Parent__c,
                                     Order__c,
                                     Parent_Value__c,
                                     Record_Status__c,
                                     Type__c,
                                     Values__c
                                     from HEP_List_Of_Values__c
                                     Where 
                                     Name =: HEP_Utility.getConstantValue('HEP_ESCO_SKU_LOV_MAPPING')
                                     And Record_Status__c =: sACTIVE
                                     order by Type__c Asc];
       
       if(lstSkuMappingValues != null && !lstSkuMappingValues.isEmpty()){
         for(HEP_List_Of_Values__c objValue : lstSkuMappingValues){
           if(mapESCOSKUListOfValues.containsKey(objValue.Type__c)){
             mapESCOSKUListOfValues.get(objValue.Type__c).put(objValue.Parent_Value__c,objValue.Values__c);
           } else{
             mapESCOSKUListOfValues.put(objValue.Type__c, new Map<String,String>{objValue.Parent_Value__c => objValue.Values__c});
           }
         }
       }
       System.debug('ESCO map  built--->'+mapESCOSKUListOfValues);
       return mapESCOSKUListOfValues;
                                     
    }

}