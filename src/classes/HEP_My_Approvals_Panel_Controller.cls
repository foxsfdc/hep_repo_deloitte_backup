/**
* HEP_My_Approvals_Panel_Controller --- Class to get the data for my approvals panel
* @author    Sachin Agarwal
*/

public without sharing class HEP_My_Approvals_Panel_Controller {
    
    public static String sACTIVE;
    public static String sFADOverride;
    public static String sUnlockDate;
    public static String sBudget;
    public static String sBudgets;
    public static String sUnlockProduct;
    public static String sUnlockProducts;
    public static String sProductRequest;
    public static String sMDPProduct;
    public static String sNationalPromoTPR;
    public static String sNationalPromotionTPR;
    public static String sCustomerPromotionTPR;
    public static String sNationalPromosTPR;
    public static String sCustomerPromoTPR;
    public static String sCustomerPromosTPR;
    public static String sApprovalStatusPending;
    public static String sSingleProductRequest;
    public static String sSingleUnlockDate;
    public static String sUnlockDates;
    public static String sSingleUnlockProduct;
    public static String sFADOverrides;
    public static String sProductRequests;
    public static String sSingleBudget;
    public static String sApprovalCommentsNo;
    
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sFADOverride = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_FAD_OVERRIDE');
        sBudgets = HEP_Utility.getConstantValue('HEP_APPROVAL_REQUESTS_BUDGETS');
        sUnlockDate = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_DATING');
        sBudget = HEP_Utility.getConstantValue('HEP_SPEND_TYPE');
        sUnlockProduct = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_UNLOCK');
        sUnlockProducts = HEP_Utility.getConstantValue('HEP_APPROVAL_REQUESTS_UNLOCK_PRODUCTS');
        sProductRequest = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_REQUESTS');
        sMDPProduct = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_PRODUCT');
        sNationalPromoTPR = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_NATIONAL');
        sNationalPromosTPR = HEP_Utility.getConstantValue('HEP_NATIONAL_PROMOTIONS_TPR');
        sCustomerPromoTPR = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_CUSTOMER');
        sCustomerPromosTPR = HEP_Utility.getConstantValue('HEP_CUSTOMER_PROMOTIONS_TPR');
        sApprovalStatusPending = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        sSingleProductRequest = HEP_Utility.getConstantValue('HEP_APPROVAL_REQUESTS_PRODUCTREQUEST');
        sSingleUnlockDate = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_UNLOCK_DATE');
        sUnlockDates = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_UNLOCK_DATES');
        sSingleUnlockProduct = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_UNLOCK_PRODUCT');
        sFADOverrides = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_FAD_OVERRIDES');
        sProductRequests = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_PRODUCT_REQUESTS');
        sSingleBudget = HEP_Utility.getConstantValue('HEP_REQUEST_TYPE_BUDGET');
        sApprovalCommentsNo = HEP_Utility.getConstantValue('HEP_APPROVAL_COMMENTS_NO');
        sNationalPromotionTPR = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_NATIONAL_PROMO_TPR');
    	sCustomerPromotionTPR = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_CUSTOMER_PROMO_TPR');
    }
    
    /**
	* ApprovalRecord --- Wrapper class for holding one approval record
	* @author    Sachin Agarwal
	*/
    public class ApprovalRecord{
    	public String sRequestType;
    	public String sRequestId;
    	public String sSpendId;
    	public String sApprovalId;
    	public String sRequestorName;
    	public boolean bSelected;
    	public String sPromotionName;
    	public String sPromotionId;
    	public DateTime dtRequestDate;
    	public String sTime;
    	public String sComments;
    	public String sCommentsYesNo;
    	public String sTabName;
    	
    	/**
	    * Default class constructor
	    * @return nothing
	    * @author Sachin Agarwal
	    */
    	public ApprovalRecord(){
    		bSelected = false;
    		sCommentsYesNo = sApprovalCommentsNo;
    	}
    }
    
    /**
	* RequestTypeCount --- Wrapper class for holding request type and the associated count
	* @author    Sachin Agarwal
	*/
    public class RequestTypeCount{
    	String sRequestType;
    	Integer iCount;
    } 
    
    /**
	* MyApprovalsData --- Wrapper class for holding My Approvals data
	* @author    Sachin Agarwal
	*/
    public class MyApprovalsData{
    	public list<ApprovalRecord> lstApprovalRecords;
    	public list<RequestTypeCount> lstRequestTypeCounts;
    	
    	/**
	    * Default class constructor
	    * @return nothing
	    * @author Sachin Agarwal
	    */
    	public MyApprovalsData(){
    		lstApprovalRecords = new list<ApprovalRecord>();
    		lstRequestTypeCounts = new list<RequestTypeCount>();
    	}
    }
    
    
    /**
    * Its used to get the page data
    * @param sRequestType to identify the type of approval
    * @return wrapper
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static MyApprovalsData getPendingApprovalRecordsData(){
    	
    	MyApprovalsData objMyApprovalsData = new MyApprovalsData();
    	try{
    		// Adding all the request types
	    	list<String> lstApprovalTypes = new list<String>();
	    	
	    	lstApprovalTypes.add(sBudget);
	    	lstApprovalTypes.add(sUnlockDate);
	    	lstApprovalTypes.add(sFADOverride);
	    	lstApprovalTypes.add(sProductRequest);
	    	lstApprovalTypes.add(sUnlockProduct);
	    	lstApprovalTypes.add(sNationalPromoTPR.toLowerCase());
	    	lstApprovalTypes.add(sCustomerPromoTPR.toLowerCase());	
	    	
	    	//lstApprovalTypes.add(sMDPProduct);
	    	
	    	// Creating a map between the request type in the system and what is to be shown on the UI
	    	map<String, String> mapRequestTypes = new map<String, String>();
	    	mapRequestTypes.put(sFADOverride.toLowerCase(), sFADOverride);
	    	mapRequestTypes.put(sUnlockDate.toLowerCase(), sSingleUnlockDate);
	    	mapRequestTypes.put(sBudget.toLowerCase(), sSingleBudget);
	    	mapRequestTypes.put(sUnlockProduct.toLowerCase(), sSingleUnlockProduct);
	    	mapRequestTypes.put(sProductRequest.toLowerCase(), sSingleProductRequest);
	    	mapRequestTypes.put(sNationalPromoTPR.toLowerCase(), sNationalPromotionTPR);
	    	mapRequestTypes.put(sCustomerPromoTPR.toLowerCase(), sCustomerPromotionTPR);
	    	
	    	// Creating a map between the request type in the system and what is to be shown on the UI in header
	    	map<String, String> mapRequestHeader = new map<String, String>();
	    	mapRequestHeader.put(sBudget.toLowerCase(), sBudgets);
	    	mapRequestHeader.put(sUnlockDate.toLowerCase(), sUnlockDates);
	    	mapRequestHeader.put(sFADOverride.toLowerCase(), sFADOverrides);
	    	mapRequestHeader.put(sProductRequest.toLowerCase(), sProductRequests);
	    	mapRequestHeader.put(sUnlockProduct.toLowerCase(), sUnlockProducts);
	    	mapRequestHeader.put(sNationalPromoTPR.toLowerCase(), sNationalPromosTPR);
	    	mapRequestHeader.put(sCustomerPromoTPR.toLowerCase(), sCustomerPromosTPR);    	
	    	    	
	    	list<HEP_User_Role__c> lstUserRoles = new list<HEP_User_Role__c>();
	
		    //Roles of the user
		    lstUserRoles = [SELECT Id, Role__c ,Role__r.LOB__c, Search_Id__c, Territory__c, User__c 
	      					FROM HEP_User_Role__c 
	      					WHERE User__c =: UserInfo.getUserId() AND Record_Status__c =:sACTIVE ];
	
		    //map to store the territories which each role has access
		    map<String, set<String>> mapOfRoleWithTerritory = new map<String, set<String>>();
		    for(HEP_User_Role__c objUserRole : lstUserRoles){
		      	
		      	if(mapOfRoleWithTerritory.containsKey(objUserRole.Role__c)){
		          mapOfRoleWithTerritory.get(objUserRole.Role__c).add(objUserRole.Territory__c);
		        }
		    	else{
		          mapOfRoleWithTerritory.put(objUserRole.Role__c,new set<string>{objUserRole.Territory__c});
		       	}
		    }
	    	
	    	set<String> setMapIds = new set<String>();
	     	setMapIds = mapOfRoleWithTerritory.keySet();
	     	
	     	list<String> lstOfRoleIDfromMap = new list<String>(setMapIds);
	     	Set<String> setSearchIds = new Set<String>();
	      	
	      	//Instantiate Role Hierarchy
	      	HEP_Role_Hierarchy objroleHierarchy = new HEP_Role_Hierarchy();
	
	        //Generate Hierachy for the Requestor's Managers
			//fetch All manager Roles for Requestor
	        objroleHierarchy.GetRoleHierarchyList(lstOfRoleIDfromMap, HEP_Utility.getConstantValue('HEP_REPORTEE_HIERARCHY'));    
	        
	        system.debug('objroleHierarchy-------->' + objroleHierarchy);
	        
	        for(String sObjUserRoleId : lstOfRoleIDfromMap){
	        	Set<Id> setChildRoleIds = objroleHierarchy.mapRolesInHierarchy.get(sObjUserRoleId);
	            Set<String> setTerritoryIds = mapOfRoleWithTerritory.get(sObjUserRoleId);
	
	            //1st Iteriate over territories
	            for(String sTerritoryId : setTerritoryIds){
	            	setSearchIds.add(sObjUserRoleId + ' / ' + sTerritoryId);
	            	
	                for(Id childRoleId : setChildRoleIds){
	                	setSearchIds.add(childRoleId + ' / ' + sTerritoryId);
					}
	            }
	        }
	    	// Querying all the matching data
	    	list<HEP_Record_Approver__c> lstRecordApprovers = new list<HEP_Record_Approver__c>();
	    	lstRecordApprovers = [SELECT ID, Approval_Record__r.Approval_Type__r.Type__c, Name, CreatedBy.Name, CreatedDate,
	    								 Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c,
	    								 Approval_Record__r.HEP_Market_Spend__r.Promotion__r.PromotionName__c,
	    								 Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c,
	    								 Approval_Record__r.HEP_Market_Spend__r.Promotion__c,
	    								 Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__c,
	    								 Approval_Record__r.HEP_Promotion_Dating__r.Promotion__c,
	    								 Approval_Record__r.HEP_Market_Spend__c,
	    								 Approval_Record__c,
	    								 Approval_Record__r.HEP_Market_Spend__r.Comments__c, Approval_Record__r.Request_Id__c,
	    								 Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Comments__c,
	    								 Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c,
	    								 Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.Dating_Comments__c,
	    								 Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__c,
	    								 Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.PromotionName__c,
	    								 Approval_Record__r.HEP_MDP_Promotion_Product__r.Promotion_Type__c,
	    								 Approval_Record__r.HEP_MDP_Promotion_Product__r.Note__c
	            					FROM HEP_Record_Approver__c
							        WHERE Status__c = :sApprovalStatusPending and IsActive__c = true  // is active condition needs to be added
							          AND Approval_Record__r.Approval_Status__c = :sApprovalStatusPending
							          AND Approval_Record__r.Approval_Type__r.Type__c IN :lstApprovalTypes
							          AND Search_Id__c in : setSearchIds
							          AND Record_Status__c =:sACTIVE
							          AND ((Approval_Record__r.HEP_Market_Spend__r.Record_Status__c = :sACTIVE) OR 
							          (Approval_Record__r.HEP_Promotion_Dating__r.Record_Status__c = :sACTIVE) OR							          
							          (Approval_Record__r.HEP_MDP_Promotion_Product__r.Record_Status__c = :sACTIVE) OR
							          (Approval_Record__r.HEP_Promotion_SKU__r.Record_Status__c = :sACTIVE))
							          ORDER BY CreatedDate DESC];
	    	
	    	map<String, Integer> mapRequestTypeCount = new map<String, Integer>(); 
	    	
	    	// Going through all the extracted records 
	    	for(HEP_Record_Approver__c objRecordApprover : lstRecordApprovers){
	    		
	    		ApprovalRecord objApprovalRecord = new ApprovalRecord();    		
	    		
	    		objApprovalRecord.sRequestType = objRecordApprover.Approval_Record__r.Approval_Type__r.Type__c;
	    		objApprovalRecord.sRequestId = objRecordApprover.Approval_Record__r.Request_Id__c;
	    		objApprovalRecord.sRequestorName = objRecordApprover.CreatedBy.Name;
	    		
	    		objApprovalRecord.dtRequestDate = objRecordApprover.CreatedDate;
	    		objApprovalRecord.sTime = objRecordApprover.createddate.format('h:mm a');
	    		/*
	    		// If its a MDP Product request type, figure out whether its related to national or customer promotion
	    		if(sMDPProduct.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    			objApprovalRecord.sTabName = HEP_Utility.getConstantValue('HEP_TPR_DISCOUNTS');
	    			// If its related to customer promotion
					if(objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.Promotion_Type__c == HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER')){
						
						objApprovalRecord.sRequestType = sCustomerPromoTPR;					
					}
					// If its related to national promotion
					else if(objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.Promotion_Type__c == HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL')){
						
						objApprovalRecord.sRequestType = sNationalPromoTPR;
					}
	    		}
	    		*/
	    		objApprovalRecord.sRequestType = objApprovalRecord.sRequestType.toLowerCase();
	    		// Counting for each request type
	    		if(mapRequestTypeCount.containsKey(objApprovalRecord.sRequestType)){
	    			
	    			mapRequestTypeCount.put(objApprovalRecord.sRequestType, mapRequestTypeCount.get(objApprovalRecord.sRequestType) + 1);
	    		}
	    		else{
	    			mapRequestTypeCount.put(objApprovalRecord.sRequestType, 1);
	    		}
	    		
	    		// if the request type is of type Unlock Date	
	    		if(sUnlockDate.equalsIgnoreCase(objApprovalRecord.sRequestType) || 
	    			sFADOverride.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    			
	    			objApprovalRecord.sPromotionName = objRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c;
	                objApprovalRecord.sPromotionId = objRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__c;
	    		}
	    		// if the request type is of type Budget/Spend
	    		else if(sBudget.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    			// Make change here for Budget
	    			objApprovalRecord.sTabName = HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_PROMOTION_SPEND');
	                objApprovalRecord.sPromotionName = objRecordApprover.Approval_Record__r.HEP_Market_Spend__r.Promotion__r.PromotionName__c;
	                objApprovalRecord.sPromotionId = objRecordApprover.Approval_Record__r.HEP_Market_Spend__r.Promotion__c;
	                objApprovalRecord.sComments = objRecordApprover.Approval_Record__r.HEP_Market_Spend__r.Comments__c;
	                objApprovalRecord.sSpendId = objRecordApprover.Approval_Record__r.HEP_Market_Spend__c;
    				objApprovalRecord.sApprovalId = objRecordApprover.Approval_Record__c;
	            }
	            // if the request type is of type Unlock Product
	    		else if(sUnlockProduct.equalsIgnoreCase(objApprovalRecord.sRequestType) ||
	    				sProductRequest.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    			if(sUnlockProduct.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    				objApprovalRecord.sTabName = sUnlockProducts;
	    			}
	    			else{
	    				objApprovalRecord.sTabName = sSingleProductRequest;
	    			}
	                
	                objApprovalRecord.sPromotionId = objRecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__c;
	                objApprovalRecord.sPromotionName = objRecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c;
	                objApprovalRecord.sComments = objRecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.SKU_Master__r.Comments__c;
	    		}
	    		// if the request type is of type Customer Promotion (TPR)/National Promotion (TPR)
	    		else if(objApprovalRecord.sRequestType == sCustomerPromoTPR || objApprovalRecord.sRequestType == sNationalPromoTPR){
	    			System.debug('Inside national/customer promotion');
	    			 			
	    			//objApprovalRecord.sTabName = HEP_Utility.getConstantValue('HEP_TPR_DISCOUNTS');
	    			if(sCustomerPromoTPR.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    				objApprovalRecord.sTabName = sCustomerPromosTPR;
	    			}
	    			else{
	    				objApprovalRecord.sTabName = sNationalPromosTPR;
	    			}
	    			objApprovalRecord.sRequestType = mapRequestTypes.get(objApprovalRecord.sRequestType.toLowerCase()); 
	    			objApprovalRecord.sPromotionId = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__c;
	                objApprovalRecord.sPromotionName = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.PromotionName__c;
	                objApprovalRecord.sComments = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.Note__c;
	    		}
	    		// if the request type is of type Unlock Date
	    		if(sUnlockDate.equalsIgnoreCase(objApprovalRecord.sRequestType)){
	    			objApprovalRecord.sTabName = sUnlockDates;
	                objApprovalRecord.sComments = objRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c;
	
	            }
	            // if the request type is of type FAD Override
	            else if(sFADOverride.equalsIgnoreCase(objApprovalRecord.sRequestType)){
					objApprovalRecord.sTabName = 'FAD Dates';
	                objApprovalRecord.sComments = objRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.Dating_Comments__c;
	            }
	    		
	    		// If there are comments, mark it Yes otherwise No    		
	    		if(String.isNotEmpty(objApprovalRecord.sComments)){
	    			objApprovalRecord.sCommentsYesNo = HEP_Utility.getConstantValue('HEP_APPROVAL_COMMENTS_YES');
	    		}
	    		if(objApprovalRecord.sRequestType != null && 
	    			mapRequestTypes.containsKey(objApprovalRecord.sRequestType.toLowerCase())){
	    			
	    			objApprovalRecord.sRequestType = mapRequestTypes.get(objApprovalRecord.sRequestType.toLowerCase()); 
	    		}
	    		
	    		objMyApprovalsData.lstApprovalRecords.add(objApprovalRecord);
	    	}
	    	// Creating the header portion on the my approvals panel
	    	for(String sRequestType : lstApprovalTypes){
	    		if(mapRequestTypeCount.containsKey(sRequestType.toLowerCase())){
	    			
	    			RequestTypeCount objRequestTypeCount = new RequestTypeCount();
	    			if(mapRequestHeader.containsKey(sRequestType.toLowerCase())){
	    				objRequestTypeCount.sRequestType = mapRequestHeader.get(sRequestType.toLowerCase());
	    			}
		    		
		    		objRequestTypeCount.iCount = mapRequestTypeCount.get(sRequestType.toLowerCase());
		    
		    		objMyApprovalsData.lstRequestTypeCounts.add(objRequestTypeCount);
	    			
	    		}
	    	}
    	}
    	catch(Exception ex){
    		throw ex;
    	}
    	// Returning the data
    	return objMyApprovalsData;
    }
}