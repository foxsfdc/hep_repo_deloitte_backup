/**
* HEP_INT_DheSkuMasterWrapper --JDE SkuBom Wrapper to store the DheSkuBom details from JDE And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuBomWrapper{
    public List<DheSkuBom> dhe_sku_bom;
    public String calloutStatus;
    public String TxnId;
    public List<HEP_INT_DheSkuInvoiceWrapperUtility.Errors> errors;
    
    /**
    * DheSkuBom -- Class to hold Sku Bom details 
    * @author    Ram Bairwa
    */
    public class DheSkuBom{
       //common fields for both interfaces JDE and E1
        	public String Quantity;
            public String Action_Code;
            public String Time_Last_Updated;
            public String Date_Updated;
            public String Component_SKU_Cd;
            public String Parent_SKU_Cd;
    }
       /**
    * Errors -- Class to hold related details 
    * @author    Ram Bairwa
    
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }*/
}