@isTest
private class HEP_ProphetTVLicenseesDetails_Test {

  private static testMethod void testHEP_ProphetTVLicenseesDetails_Success() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_TV_Licensees','HEP_ProphetTVLicenseesDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('164278');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('23');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_TV_Licensees');
        System.assertEquals('HEP_Prophet_TV_Licensees',objTxnResponse.sInterfaceName);
        Test.stoptest();
  }
  
  private static testMethod void testHEP_ProphetTVLicenseesDetails_Failure() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_TV_Licensees','HEP_ProphetTVLicenseesDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('1642780');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('23');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_TV_Licensees');
        System.assertEquals('HEP_Prophet_TV_Licensees',objTxnResponse.sInterfaceName);
        Test.stoptest();
  }
  
    /**
    * HEP_ProphetProjectedDateDetails_InvalidAcessTokenTest -- Test method in case of Invalid Access Token from Prophet Interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    private static testMethod void HEP_ProphetProjectedDateDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Services__c objService = new HEP_Services__c();
         list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        objService.Name='Prophet_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_TV_Licensees','HEP_ProphetTVLicenseesDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('164278');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('23');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_TV_Licensees');
        System.assertEquals('HEP_Prophet_TV_Licensees',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
}