/**
* HEP_INT_DheSkuBom_Details -- DheSkuBom Order details will be stored from NTS And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuBom_Details implements HEP_IntegrationInterface{ 
    public static String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    /**
    * HEP_INT_DheSkuBom_Details -- dhe_sku_Bom details to store sku bom details
    * @return  :  no return value
    * @author  :  Ram Bairwa
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_INT_DheSkuBomWrapper objWrapper = new HEP_INT_DheSkuBomWrapper();         
        if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sTimeStamp;
            String sLastPulledDate;
            String sNoOfRows;
            String sStartingFrom;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the TimeStamp, LastPulledDate, NoOfRows, StartingFrom details to get the sku box details through integration 
            HEP_INT_DheSkuInvoiceWrapperUtility objDheSkuInvoiceWrapperUtility = (HEP_INT_DheSkuInvoiceWrapperUtility)JSON.deserialize(sDetails,HEP_INT_DheSkuInvoiceWrapperUtility.class);
            if(objDheSkuInvoiceWrapperUtility != null){                             
            sTimeStamp = objDheSkuInvoiceWrapperUtility.sTimeStamp;
            sLastPulledDate = objDheSkuInvoiceWrapperUtility.sLastPulledDate;
            sNoOfRows = objDheSkuInvoiceWrapperUtility.sNoOfRows;
            sStartingFrom = objDheSkuInvoiceWrapperUtility.sStartingFrom;            
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_SKU_BOMDETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_JDE_SKU_BOMDETAILS'));                                   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?HHMMSS=' + sTimeStamp + '&YYYYMMDD=' + sLastPulledDate+ '&NO_OF_ROWS=' + sNoOfRows+ '&STARTING_FROM=' + sStartingFrom);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                system.debug('response body'+objTxnResponse.sResponse);
                if(/*HEP_Constants__c.getValues('HEP_STATUS_OK') != null  && HEP_Utility.getConstantValue('HEP_STATUS_OK') != null
                    &&*/ objHttpResp.getStatus().equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){
                                       
                    String sResponse = objHttpResp.getBody();
                        System.debug('sResponse'+sResponse);
                    objWrapper = (HEP_INT_DheSkuBomWrapper)JSON.deserialize(sResponse, HEP_INT_DheSkuBomWrapper.class); 
                     System.debug('objWrapper'+objWrapper);
                        if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        objTxnResponse.bRetry = false;
                        //calling helper method to perform mapping and upsert
                        System.debug('objWrapper+++++   ' + objWrapper);
                        updateMapping(objWrapper);
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }        
    }    
       
    private static void updateMapping(HEP_INT_DheSkuBomWrapper bomWrapper){
        try{
	        List<HEP_SKU_BOM__c> lstDBObjectUpsert=New List<HEP_SKU_BOM__c>();
	       // Map<String,HEP_SKU_BOM__c> mapSkuBom=New Map<String,HEP_SKU_BOM__c>();
	        Map<String,String> mapParentSKU = new Map<String,String>();
            Map<String,String> mapChildSKU = new Map<String,String>();
            Map<String,HEP_INT_DheSkuBomWrapper.DheSkuBom> mapSkuWrapper=New Map<String,HEP_INT_DheSkuBomWrapper.DheSkuBom>();
	        
            for(HEP_INT_DheSkuBomWrapper.DheSkuBom var:bomWrapper.dhe_sku_bom){
	           mapSkuWrapper.put(var.Parent_SKU_Cd+'/'+var.Component_SKU_Cd,var);  
               mapParentSKU.put(var.Parent_SKU_Cd, null);      
               mapChildSKU.put(var.Component_SKU_Cd, null); 
	        }

            List<HEP_SKU_Master__c> listSKUMasters = [Select Id, SKU_Number__c from HEP_SKU_Master__c
                                                    where SKU_Number__c IN :mapParentSKU.keySet() OR
                                                    SKU_Number__c IN :mapChildSKU.keySet()];

            for(HEP_SKU_Master__c SKUMasterRec : listSKUMasters){
                if(mapParentSKU.containsKey(SKUMasterRec.SKU_Number__c)){
                    mapParentSKU.put(SKUMasterRec.SKU_Number__c, SKUMasterRec.Id);
                }
                if(mapChildSKU.containsKey(SKUMasterRec.SKU_Number__c)){
                    mapChildSKU.put(SKUMasterRec.SKU_Number__c, SKUMasterRec.Id);
                }
            }

	        //iterate over unique key and update the value with JDE parameter
	        for(String uniqueKey: mapSkuWrapper.keySet()){
                
                HEP_INT_DheSkuBomWrapper.DheSkuBom skuBom= mapSkuWrapper.get(uniqueKey);

                HEP_SKU_BOM__c skuBomRec = new HEP_SKU_BOM__c();
                skuBomRec.Components_SKU_Number__c=skuBom.Component_SKU_Cd;
                skuBomRec.Component_SKU__c = mapChildSKU.get(skuBom.Component_SKU_Cd);
                skuBomRec.Parent_SKU_Number__c=skuBom.Parent_SKU_Cd;
                skuBomRec.Parent_SKU__c = mapParentSKU.get(skuBom.Parent_SKU_Cd);
                skuBomRec.Quantity__c=Decimal.ValueOf(skuBom.Quantity); 
                skuBomRec.Unique_SKU_Number__c = uniqueKey;                                  

                skuBomRec.Record_Status__c = 'D'.equalsIgnoreCase(skuBom.Action_Code) ? 'Inactive' : sACTIVE;  

                lstDBObjectUpsert.add(skuBomRec);                             
	        }
	        
            if(!lstDBObjectUpsert.isEmpty()) 
                Database.upsert(lstDBObjectUpsert, HEP_SKU_BOM__c.Fields.Unique_SKU_Number__c); 
        
        } catch(Exception ex){
            //throw error
            System.debug('Exception on Class : HEP_INT_DheSkuBom_Details - updateMapping, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_INT_DheSkuBom_Details','updateMapping',null,null);
        }          
    }
}