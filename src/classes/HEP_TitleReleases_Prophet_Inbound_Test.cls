@isTest (seeAllData = false)
public class HEP_TitleReleases_Prophet_Inbound_Test {
    @testSetup
    static void createData() {
        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

       //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', false);
        objGlobalTerritory.Send_to_Prophet__c = true;
        objGlobalTerritory.Territory_Code_Integration__c = '123';
        insert objGlobalTerritory;

        //Line of business
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;

        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        objTitle.FIN_PROD_ID__c = '12345';
        insert objTitle;

        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Glocal Catalog', 'Single', null, string.valueof(objGlobalTerritory.id), 'Pending', 'Request', false);
        objGlobalCatalog.Title_EDM__c = objTitle.id;
        insert objGlobalCatalog;

        //Promotion - 1
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestGlobaPromotion', 'Global', '', objGlobalTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objGlobalPromotion.LocalPromotionCode__c = '123456789';
        objGlobalPromotion.Title__c = objTitle.id;
        insert objGlobalPromotion;

         //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c objDatingMatrixGlobal = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objGlobalTerritory.Id, objGlobalTerritory.Id, true);

        //Create dating records 
        HEP_Promotion_Dating__c DatingGlobalRec = HEP_Test_Data_Setup_Utility.createDatingRecord(objGlobalTerritory.Id, objGlobalPromotion.Id, objDatingMatrixGlobal.id, false);
        DatingGlobalRec.HEP_Catalog__c = objGlobalCatalog.id;
        DatingGlobalRec.Date__c = system.today();
        insert DatingGlobalRec;

    }    

    public static testMethod void test() {

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<EDM_GLOBAL_TITLE__c> objTitle = [SELECT FIN_PROD_ID__c FROM EDM_GLOBAL_TITLE__c];
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        HEP_TitleReleases_Prophet_Inbound.TitleReleasesProphetRequest objRequest = new HEP_TitleReleases_Prophet_Inbound.TitleReleasesProphetRequest(objTitle[0].FIN_PROD_ID__c, '123,124');
        objTxnResponse.sRequest = JSON.serialize(objRequest);

        list<HEP_Promotion_Dating__c> lst = [select HEP_Catalog__c, 
                                                        HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, 
                                                        Territory__r.Territory_Code_Integration__c,
                                                        Territory__r.Send_to_Prophet__c,
                                                        Customer__c,
                                                        Date__c,
                                                        DateType__c from HEP_Promotion_Dating__c];

        System.runAs(u) {
            Test.startTest();
                HEP_TitleReleases_Prophet_Inbound objTitleClass = new HEP_TitleReleases_Prophet_Inbound();
                Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
                RestResponse res = new RestResponse();
                RestContext.response = res;
                objTitleClass.performTransaction(objTxnResponse);
            Test.stoptest();
        }    
    }
}