/**
* HEP_CatalogDetails_Test -- Test class for the HEP_CatalogDetails for Foxipedia Interface.
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_CatalogDetails_Test{
    /**
    * HEP_CatalogDetails_SuccessTest -- Test method to get Catalog details Successfully
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog_Title','HEP_CatalogDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('108544','','HEP_Foxipedia_Catalog_Title');
        System.assertEquals('HEP_Foxipedia_Catalog_Title',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_CatalogDetails_FailureTest -- Test method to get Catalog details in case of failure condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog_Title','HEP_CatalogDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('aer567854444','','HEP_Foxipedia_Catalog_Title');
        System.assertEquals('HEP_Foxipedia_Catalog_Title',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_CatalogDetails_InvalidAcessTokenTest -- Test method to get Catalog details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer54444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_CatalogDetails objCatalogDetails = new HEP_CatalogDetails();
        objCatalogDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
}