/**
* HEP_INT_DheSkuPriceWrapper --JDE SkuPrice Wrapper to store the DheSkuPrice details from JDE And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuPriceWrapper{
    public List<DheSkuPrice> dheSkuPrice;
    public String calloutStatus;
    public String TxnId;
    public List<HEP_INT_DheSkuInvoiceWrapperUtility.Errors> errors;
    
    /**
    * DheSkuPrice -- Class to hold Sku Price details 
    * @author    Ram Bairwa
    */
    public class DheSkuPrice{
       //common fields for both interfaces JDE and E1
        	public String Local_PromoCode;
            public String Region;
            public String Action_Code;
            public String Time_Last_Updated;
            public String Effective_Start_Date;
            public String Date_Updated;
            public String Price;
            public String UserID;
            public String SKU_No;//SKU#
    }
    

}