@isTest
private class HEP_Catalogs_Components_Controller_Test {

    @testSetup static void setup() { 

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','006053','',null,null);        
        insert objTitle;
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', NULL, NULL, 
                                        'EUR', 'DE', '182', 'ESCO', true);
                                        
        HEP_Territory__c objChildTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium', 'EMEA', 'Subsidiary', 
                                            objTerritory.Id, objTerritory.Id, 'EUR', 'BE', '49', NULL, true);
                                            
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo', 'Global', NULL, 
                                        objTerritory.Id, NULL, NULL, NULL, true);
                                        
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        objCatalog.Catalog_Type__c = 'Bundle'; 
        objCatalog.CatalogId__c= '99999';
        insert objCatalog;     
        
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Catalog_Components','HEP_Catalog_Components_Extractor',true,3,1,'Outbound-Pull',false,true,false);     
        objInterface.Record_Status__c = 'Active';      
        insert  objInterface;                           

        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
       
    }
    
    @isTest static void test1() {
        HEP_Catalog__c objCatalog = [SELECT Id FROM HEP_Catalog__c LIMIT 1];
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Catalogs_Components_Controller  objCTRL = new HEP_Catalogs_Components_Controller ();
        HEP_Catalogs_Components_Controller.getInitialDetails(objCatalog.id);
        Test.stopTest();
    }

      @isTest static void test2() {
        HEP_Catalog__c objCatalog = [SELECT Id,CatalogId__c FROM HEP_Catalog__c LIMIT 1];
        objCatalog.CatalogId__c = '88888';
        update objCatalog;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Catalogs_Components_Controller  objCTRL = new HEP_Catalogs_Components_Controller ();
        HEP_Catalogs_Components_Controller.getInitialDetails(objCatalog.id);
        HEP_Catalogs_Components_Controller.getTitleOrCatalogLink('006053','Catalog');
        Test.stopTest();
    }
        @isTest static void test3() {
        HEP_Catalog__c objCatalog = [SELECT Id,CatalogId__c FROM HEP_Catalog__c LIMIT 1];
        objCatalog.CatalogId__c = '77777';
        update objCatalog;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Catalogs_Components_Controller  objCTRL = new HEP_Catalogs_Components_Controller ();
        HEP_Catalogs_Components_Controller.getInitialDetails(objCatalog.id);
        HEP_Catalogs_Components_Controller.getTitleOrCatalogLink('006053','Title');
        Test.stopTest();
    }

   
      
    
}