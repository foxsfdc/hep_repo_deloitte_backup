/* 
 @HEP_ApprovalProcess_Test ---Test Class for Approval process.
 @author : Deloitte
 */

@isTest
public class HEP_Approval_Utility_Test{
    
  @testSetup
  public static void createTestUserSetup(){
    HEP_ApprovalProcess_Test.createTestUserSetup();
  }

  static TestMethod void  testUtilityMethodsForStatic(){
  	
  	HEP_ApprovalProcess_Test.testDatingUnlockRequest();    
  }
  
  static TestMethod void  testUtilityMethodsForHierarchical(){
  	
    HEP_ApprovalProcess_Test.testBudgetRequestApproved();
  }
  
  static TestMethod void  testUtilityMethodsForAutoUpdate(){
  	
    HEP_ApprovalProcess_Test.testBudgetRequestAutoApproved();
  }
  
   static TestMethod void  testforExceptionScenario(){
	
	HEP_Approval_Utility obj = new HEP_Approval_Utility();
	
   	HEP_Approval_Type__c objApprovalType = [Select Id,Approval_Field__c from HEP_Approval_Type__c where Type__c ='SPEND'];
   	objApprovalType.Approval_Field__c = 'TestToCatchField';
	update objApprovalType;
	
    //Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',false);
		objTestMarketSpend.Record_Status__c = 'Inactive';
		insert objTestMarketSpend;
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = -10;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Budget Update';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = null;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
        System.debug('Auto Approval Method status-->'+bApproval);
        Test.stopTest();        
        	
  	}	
  }
  
  static TestMethod void  testExceptionWhenHierarchical(){
	
	HEP_Approval_Utility obj = new HEP_Approval_Utility();
	
   	HEP_Approval_Type__c objApprovalType = [Select Id,Approval_Field__c from HEP_Approval_Type__c where Type__c ='SPEND'];
   	objApprovalType.Approval_Field__c = 'TestToCatchField';
	update objApprovalType;
	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',true);
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = 2000;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Current';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = null;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
        
        Test.stopTest();
  	}
  } 
  
   static TestMethod void  testExceptionWhenStatic(){
	
	HEP_Approval_Utility obj = new HEP_Approval_Utility();
	
   	HEP_Approval_Type__c objApprovalType = [Select Id,Approval_Field__c from HEP_Approval_Type__c where Type__c ='DATING'];
   	objApprovalType.Approval_Field__c = 'TestToCatchField';
	update objApprovalType;
	
	HEP_ApprovalProcess_Test.testDatingUnlockRequest();
  }       
}