@isTest
public class HEP_Dating_ApprovalRequest_Test{
    
    @testSetup
    static void createSetupComponent(){
      //Create All test data;
      HEP_ApprovalProcess_Test.createTestUserSetup();
    }
    
    static TestMethod void testComponentForDating() {
        //Fetch the created Test User:
      User objTestUser = [Select   Id,
                    Name 
                    from User 
                    where Email = 'ApprovalTestUser@hep.com'];
      
      //fetch promotion:
      HEP_Promotion__c objTestPromotion = [Select Id,
                            Territory__c,
                            Name 
                            from HEP_Promotion__c 
                            where Promotion_Type__c ='National'];
      
      HEP_Territory__c objGerTerritory = [Select Id,
                          Name 
                          from HEP_Territory__c
                          where Name = 'Germany'];
      
      //Run in the context of User:
      System.runAs(objTestUser){
      HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objGerTerritory.Id, objGerTerritory.Id, false);
          VODRelease.Projected_FAD_Logic__c = 'Monday';
          VODRelease.Media_Type__c ='Digital';
          insert VODRelease;
          
          HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objGerTerritory.Id, objTestPromotion.Id, VODRelease.Id, false);
          DatingRec1.Status__c = 'Confirmed';
          DatingRec1.Locked_Status__c = 'Locked';
          insert DatingRec1;
          
      //Invoke Approval Process:
      String approvalRecordId = DatingRec1.Id;
      String approvalType='DATING';
      String territoryId = objGerTerritory.Id;
      HEP_ApprovalProcess pr= new HEP_ApprovalProcess();
      Test.startTest();
      Boolean status = pr.invokeApprovalProcess(approvalRecordId,approvalType,territoryId);
      
      Test.stopTest();
      }
    }
}