/**
 * HEP_Catalog_Details_SubTab_Controller_Test -- Test class for the HEP_Catalog_Details_SubTab_Controller.
 * @author    Roshi Rai
 */
@isTest
public class HEP_Catalog_Details_SubTab_Cntr_Test {
    //user Setup 
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List < HEP_List_Of_Values__c > lstListofvalues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void testOne() {
        //Inserting Territory Record
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('National', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        //All HEP Constants
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        //Inserting Catalog Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', true);
        //Catalog wrapper on page
        HEP_Catalog_Details_SubTab_Controller.CatalogDetailWrapper objCatalogDetailWrapper = new HEP_Catalog_Details_SubTab_Controller.CatalogDetailWrapper();
        //List of Values
        //Catalog Genre
        HEP_Catalog_Genre__c objGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.id, true);
        System.runAs(u) {
            HEP_Catalog_Details_SubTab_Controller.extractHepCatalogDetailsData(objCatalog.id);
            HEP_Catalog_Details_SubTab_Controller.extractLicensor('Competitive');
            objCatalogDetailWrapper = HEP_Catalog_Details_SubTab_Controller.extractHepCatalogDetailsData(objCatalog.id);
            HEP_Catalog_Details_SubTab_Controller.saveCatalogDetails(objCatalogDetailWrapper, objCatalog.id);
        }
    }
}