/**
* HEP_INT_DheSkuPrice_Details -- Price details to store Price details 
* @author    Avinash Harwani
*/
public class HEP_INT_DheSkuPrice_Details implements HEP_IntegrationInterface{ 
    
    
    /**
    * HEP_INT_DheSkuPrice_Details -- Price details to store Price details 
    * @return no return value 
    * @author    Avinash Harwani
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_INT_DheSkuPriceWrapper objWrapper = new HEP_INT_DheSkuPriceWrapper();         
        if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sTimeStamp;
      String sLastPulledDate;
      String sNoOfRows;
      String sStartingFrom;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the TimeStamp, LastPulledDate, NoOfRows, StartingFrom details to get the sku box details through integration 
            HEP_INT_DheSkuInvoiceWrapperUtility objDheSkuInvoiceWrapperUtility = (HEP_INT_DheSkuInvoiceWrapperUtility)JSON.deserialize(sDetails,HEP_INT_DheSkuInvoiceWrapperUtility.class);
            if(objDheSkuInvoiceWrapperUtility != null){                            
            sTimeStamp = objDheSkuInvoiceWrapperUtility.sTimeStamp;
            sLastPulledDate = objDheSkuInvoiceWrapperUtility.sLastPulledDate;
            sNoOfRows = objDheSkuInvoiceWrapperUtility.sNoOfRows;
            sStartingFrom = objDheSkuInvoiceWrapperUtility.sStartingFrom;            
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_SKU_PRICEDETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_JDE_SKU_PRICEDETAILS'));                                   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?HHMMSS=' + sTimeStamp + '&YYYYMMDD=' + sLastPulledDate+ '&NO_OF_ROWS=' + sNoOfRows+ '&STARTING_FROM=' + sStartingFrom);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                system.debug('reponse body'+objTxnResponse.sResponse);
                if(/*HEP_Constants__c.getValues('HEP_SUCCESS') != null  && HEP_Utility.getConstantValue('HEP_SUCCESS') != null
                    && */objHttpResp.getStatus().equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                                       
                    String sResponse = objHttpResp.getBody();
                    //sResponse = sResponse.replace(HEP_Constants.HEP_SKUNo, HEP_Constants.HEP_REPLACE_SKUNo); 
                    //sResponse = sResponse.replace(HEP_Constants.HEP_ECopy_Flag , HEP_Constants.HEP_REPLACE_ECopy_Flag);
                    //sResponse = sResponse.replace(HEP_Constants.HEP_BW_Color , HEP_Constants.HEP_REPLACE_BW_Color);
                    //sResponse = sResponse.replace(HEP_Constants.HEP_ThreeD_Flag , HEP_Constants.HEP_REPLACE_ThreeD_Flag);
                    
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    sResponse = sResponse.replace('SKU#','SKU_No');
                    objWrapper = (HEP_INT_DheSkuPriceWrapper)JSON.deserialize(sResponse, HEP_INT_DheSkuPriceWrapper.class);  
                     if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        objTxnResponse.bRetry = false;
                        System.debug('objWrapper+++++   ' + objWrapper);
                        updatePriceDetails(objWrapper);
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }
    }
    //start Nidhin 6-20
    public static String sDHE, sUS, sCANADA, sACTIVE, sINACTIVE, sFORMAT;
    static{
		sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
		sINACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE');
        sDHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
        sUS = HEP_Utility.getConstantValue('HEP_TERRITORY_US');
        sCANADA = HEP_Utility.getConstantValue('HEP_TERRITORY_CANADA');
        sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
    }
    
    private static void updatePriceDetails(HEP_INT_DheSkuPriceWrapper objWrapper){
    	try{
	        if(objWrapper.dheSkuPrice.isEmpty()) 
	        	return;
	        Set<String> setLocalPromoCodes = new Set<String>(); 
	        Set<String> setSKUNumbers = new Set<String>(); 
	        Set<String> setPromoCodeSKUNos = new Set<String>();
            Map<String, Integer> mapSKUNumWithTime = new Map<String, Integer>();
	        System.debug('objWrapper.dheSkuPrice>>' + objWrapper.dheSkuPrice);
	        for(HEP_INT_DheSkuPriceWrapper.DheSkuPrice objPriceWrap : objWrapper.dheSkuPrice){
	       		System.debug('objPriceWrap>>' + objPriceWrap);
	        	if(String.isNotBlank(objPriceWrap.Region)
	        		&& String.isNotBlank(objPriceWrap.Local_PromoCode)
	        		&& String.isNotBlank(objPriceWrap.SKU_No)
	        		&& String.isNotBlank(objPriceWrap.Price)){
	        			
	        		String sPromoCode = objPriceWrap.Local_PromoCode.removeStart('U');
	        		setLocalPromoCodes.add(sPromoCode);
	        		setSKUNumbers.add(objPriceWrap.SKU_No);
	        		setPromoCodeSKUNos.add(sPromoCode + '*' + objPriceWrap.SKU_No);

                    //Kunal - logic to track last update for a SKU #
                    if(mapSKUNumWithTime.containsKey(objPriceWrap.SKU_No+'*'+objPriceWrap.Region)){
                        if(mapSKUNumWithTime.get(objPriceWrap.SKU_No+'*'+objPriceWrap.Region) < Integer.valueOf(objPriceWrap.Time_Last_Updated) )
                            mapSKUNumWithTime.put(objPriceWrap.SKU_No+'*'+objPriceWrap.Region, Integer.valueOf(objPriceWrap.Time_Last_Updated));
                    }
                    else{
                        mapSKUNumWithTime.put(objPriceWrap.SKU_No+'*'+objPriceWrap.Region, Integer.valueOf(objPriceWrap.Time_Last_Updated));
                    }
	        	}
	        }
	        System.debug('setLocalPromoCodes>>' + setLocalPromoCodes);
	        System.debug('setSKUNumbers>>' + setSKUNumbers);
	        System.debug('setPromoCodeSKUNos>>' + setPromoCodeSKUNos);
            System.debug('mapSKUNumWithTime>>' + mapSKUNumWithTime);
    
		    List<String> lstLOVTypes = new List<String>{sFORMAT};
		    Map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(
    										lstLOVTypes, 
    										HEP_Utility.getConstantValue('HEP_NAME_FIELD'), 
    										HEP_Utility.getConstantValue('HEP_LOV_FIELD_VALUES'));
    										
	        Map<String,HEP_Promotion_SKU__c> mapPromoCodePromoSKUs = new Map<String,HEP_Promotion_SKU__c>();
	        Set<String> setFilters = new Set<String>(); 
	        Set<String> setFormats = new Set<String>(); 
	        for(HEP_Promotion_SKU__c objPromoSKU : [SELECT
	        											Id, Unique_Id__c, SKU_Master__r.Id, SKU_Master__r.SKU_Number__c,
	        											SKU_Master__r.JDE_Wholesale_Price_US__c,
	        											SKU_Master__r.JDE_Wholesale_Price_CAN__c,
	        											SKU_Master__r.Channel__c, SKU_Master__r.Format__c,
	        											Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
	        											Promotion_Catalog__r.Catalog__r.Catalog_Type__c
	        										FROM
	        											HEP_Promotion_SKU__c
	        										WHERE
	        											SKU_Master__r.SKU_Number__c IN :setSKUNumbers
	        										AND
	        											Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c IN :setLocalPromoCodes
	        										AND
	        											SKU_Master__r.Territory__r.Name = :sDHE]){
	        	
	        	String sPromoCodeKey = objPromoSKU.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c + '*' + objPromoSKU.SKU_Master__r.SKU_Number__c;
	        	System.debug('localcode*number>>' + sPromoCodeKey);
	        	if(setPromoCodeSKUNos.contains(sPromoCodeKey)){
	        		mapPromoCodePromoSKUs.put(sPromoCodeKey, objPromoSKU);
	        		setFormats.add(objPromoSKU.SKU_Master__r.Format__c);
	        		setFormats.add(mapLOVMapping.get(sFORMAT + '*' + objPromoSKU.SKU_Master__r.Format__c));
	        	}
	        }
	        System.debug('mapPromoCodePromoSKUs>>' + mapPromoCodePromoSKUs);
	        System.debug('setFormats>>' + setFormats);
	        
	        Map<String,List<HEP_Price_Grade__c>> mapPriceGradesUS = new Map<String,List<HEP_Price_Grade__c>>();
	        Map<String,List<HEP_Price_Grade__c>> mapPriceGradesCAN = new Map<String,List<HEP_Price_Grade__c>>();
	        for(HEP_Price_Grade__c objPriceGrade : [SELECT
	        											Id, PriceGrade__c, Territory__r.Name, Format__c
	        										FROM
	        											HEP_Price_Grade__c
	        										WHERE
	        											Territory__r.Name IN (:sUS, :sCANADA)
	        										AND
	        											Format__c IN :setFormats
	        										AND
	        											PriceGrade__c != NULL
	        										AND
	        											Record_Status__c = :sACTIVE]){
	       		String sMasterPriceGrade = objPriceGrade.PriceGrade__c.split('-')[0].trim().removeStart('$') + '-' + objPriceGrade.PriceGrade__c.split('-')[1].trim().removeStart('$');
	       		if(objPriceGrade.Territory__r.Name.equalsIgnoreCase(sUS)){
		       		if(mapPriceGradesUS.containsKey(sMasterPriceGrade))
		       			mapPriceGradesUS.get(sMasterPriceGrade).add(objPriceGrade);
		       		else
		       			mapPriceGradesUS.put(sMasterPriceGrade, new List<HEP_Price_Grade__c>{objPriceGrade});
	       		} else{
	       			if(mapPriceGradesCAN.containsKey(sMasterPriceGrade))
		       			mapPriceGradesCAN.get(sMasterPriceGrade).add(objPriceGrade);
		       		else
		       			mapPriceGradesCAN.put(sMasterPriceGrade, new List<HEP_Price_Grade__c>{objPriceGrade});
	       		}
	       	}
	        System.debug('mapPriceGradesUS>>' + mapPriceGradesUS);
	        System.debug('mapPriceGradesCAN>>' + mapPriceGradesCAN);
	       	
	        List<HEP_SKU_Price__c> lstSKUPrice = new List<HEP_SKU_Price__c>();
	    	for(HEP_INT_DheSkuPriceWrapper.DheSkuPrice objPriceWrap : objWrapper.dheSkuPrice){

                if(mapSKUNumWithTime.get(objPriceWrap.SKU_No+'*'+objPriceWrap.Region)!=Integer.valueOf(objPriceWrap.Time_Last_Updated)){
                    continue;
                }

	        	if(String.isNotBlank(objPriceWrap.Region)
	        		&& String.isNotBlank(objPriceWrap.Local_PromoCode)
	        		&& String.isNotBlank(objPriceWrap.SKU_No)
	        		&& String.isNotBlank(objPriceWrap.Price)){
	        		
	        		String sPromoCode = objPriceWrap.Local_PromoCode.removeStart('U');
	        		String sPromoCodeKey = sPromoCode + '*' + objPriceWrap.SKU_No;
	        		if(mapPromoCodePromoSKUs.containsKey(sPromoCodeKey)){
	        			HEP_Promotion_SKU__c objPromoSKU = mapPromoCodePromoSKUs.get(sPromoCodeKey);
	        			HEP_SKU_Price__c objSKUPrice = new HEP_SKU_Price__c();
	        			String sUniqueId = objPriceWrap.Region + ' / ' + objPromoSKU.Unique_Id__c;
	        			String sStatus = 'D'.equalsIgnoreCase(objPriceWrap.Action_Code) ? sINACTIVE : sACTIVE;
	        			Decimal dPriceGrade = Decimal.valueOf(objPriceWrap.Price).divide(10000, 2);
						String sReceivedPriceGrade = '', sFormatKey = '';
						if(objPriceWrap.Region.equalsIgnoreCase(sUS)){
							sReceivedPriceGrade = String.valueOf(dPriceGrade) + '-' + String.valueOf(objPromoSKU.SKU_Master__r.JDE_Wholesale_Price_US__c);
							System.debug('sReceivedPriceGrade US>>' + sReceivedPriceGrade);
							if(mapPriceGradesUS.containsKey(sReceivedPriceGrade)){
								System.debug('containsKey US>> containsKey US');
								for(HEP_Price_Grade__c objPriceGrade : mapPriceGradesUS.get(sReceivedPriceGrade)){
									System.debug('objPriceGrade.Format__c US>>' + objPriceGrade.Format__c);
									System.debug('objPromoSKU.SKU_Master__r.Format__c US>>' + objPromoSKU.SKU_Master__r.Format__c);
									sFormatKey = mapLOVMapping.get(sFORMAT + '*' + objPromoSKU.SKU_Master__r.Format__c);
	    							if(objPriceGrade.Format__c.equalsIgnoreCase(objPromoSKU.SKU_Master__r.Format__c)
	    								|| objPriceGrade.Format__c.equalsIgnoreCase(sFormatKey)){
				        				objSKUPrice.Unique_Id__c = sUniqueId;
				        				objSKUPrice.PriceGrade__c = objPriceGrade.Id;
				    					objSKUPrice.Start_Date__c = Date.valueOf(objPriceWrap.Effective_Start_Date);
				    					objSKUPrice.Promotion_SKU__c = objPromoSKU.Id;
				    					objSKUPrice.SKU_Master__c = objPromoSKU.SKU_Master__r.Id;
										objSKUPrice.Record_Status__c = sStatus;
										objSKUPrice.JDE_Pending_Flag__c = false;
				        				lstSKUPrice.add(objSKUPrice);
				        				break;
		    						}
								}
	        				}
						}else{
	    					sReceivedPriceGrade = String.valueOf(dPriceGrade) + '-' + String.valueOf(objPromoSKU.SKU_Master__r.JDE_Wholesale_Price_CAN__c);
							System.debug('sReceivedPriceGrade CAN>>' + sReceivedPriceGrade);
							if(mapPriceGradesCAN.containsKey(sReceivedPriceGrade)){
								System.debug('containsKey CAN>> containsKey US');
								for(HEP_Price_Grade__c objPriceGrade : mapPriceGradesCAN.get(sReceivedPriceGrade)){
									System.debug('objPriceGrade.Format__c CAN>>' + objPriceGrade.Format__c);
									System.debug('objPromoSKU.SKU_Master__r.Format__c CAN>>' + objPromoSKU.SKU_Master__r.Format__c);
									sFormatKey = mapLOVMapping.get(sFORMAT + '*' + objPromoSKU.SKU_Master__r.Format__c);
	    							if(objPriceGrade.Format__c.equalsIgnoreCase(objPromoSKU.SKU_Master__r.Format__c)
	    								|| objPriceGrade.Format__c.equalsIgnoreCase(sFormatKey)){
				        				objSKUPrice.Unique_Id__c = sCANADA + ' / ' + objPromoSKU.Unique_Id__c;//sUniqueId;
				        				objSKUPrice.PriceGrade__c = objPriceGrade.Id;
				    					objSKUPrice.Start_Date__c = Date.valueOf(objPriceWrap.Effective_Start_Date);
				    					objSKUPrice.Promotion_SKU__c = objPromoSKU.Id;
				    					objSKUPrice.SKU_Master__c = objPromoSKU.SKU_Master__r.Id;
				    					objSKUPrice.Record_Status__c = sStatus;
				        				lstSKUPrice.add(objSKUPrice);
				        				break;
		    						}
								}
	        				}
						}
	        		}
	        	}
	        }

            System.debug('lstSKUPrice>>' + lstSKUPrice);
	        if(lstSKUPrice.size() > 0)
	        	upsert lstSKUPrice Unique_Id__c;

    	} catch(Exception ex){
            //throw error
            System.debug('Exception on Class : HEP_INT_DheSkuPrice_Details - updatePriceDetails, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            HEP_Error_Log.genericException('Query Erros','Query Errors',ex,'HEP_INT_DheSkuPrice_Details','updatePriceDetails',null,null);
        }
    }
    //end Nidhin 6-20
}