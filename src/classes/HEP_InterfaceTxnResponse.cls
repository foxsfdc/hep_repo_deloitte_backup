/**
* HEP_InterfaceTxnResponse --- Class to store integration request, response
* @author    Sachin Agarwal
*/
public with sharing class HEP_InterfaceTxnResponse {
    public String sRequest;
    public String sResponse;
    public Boolean bRetry; // If the bad response is received or the response body contains errors, it should be true otherwise false
    public String sStatus; // Success, Failure
    public Boolean bInboundTxn;
    public String sInterfaceName;
    public String sInterfaceId;
    public String sInterfaceTxnId;
    public String sSourceId;
    public String sErrorMsg;
    public Integer iCurrentNoOfRetries;
    public Integer iMaxRetriesAllowed;
    public Integer iRetryInterval;
    public Boolean bIsSourceAsAttachment;
    public Boolean bIsCreateContentFile;
    public Integer iNoOfRecords;
    
    /**
     * Class Constructor
     * @return nothing
     */
    public HEP_InterfaceTxnResponse(){
      bRetry = false;
      bInboundTxn = false;
      bIsSourceAsAttachment = false;
      bIsCreateContentFile  = false;
    }
}