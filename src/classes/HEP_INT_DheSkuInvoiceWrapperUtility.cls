/**
* HEP_INT_DheSkuInvoiceWrapperUtility --HEP_INT_DheSkuInvoiceWrapperUtility Wrapper to store the Input parameter and Error details for JDE And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuInvoiceWrapperUtility {
    
    public String sTimeStamp;
	public String sLastPulledDate;
	public String sNoOfRows;
	public String sStartingFrom;


   /**
    * Errors -- Class to hold related details 
    * @author    Ram Bairwa
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }
 }