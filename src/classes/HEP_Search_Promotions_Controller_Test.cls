/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest(seeAllData = false)
public class HEP_Search_Promotions_Controller_Test {
    
    @testSetup
    public static void createPromoData(){
        //HEP_Notification_Template__c globalTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Creation','HEP_Promotion__c','A global promotion has been created. Please create your territory promotion and link to the global promotion','Global promotion creation','Active','-','',true);
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
    }
    @isTest
    public static void testsearch(){
        
        HEP_Territory__c Australia = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'APAC', 'Subsidiary', null, null, 'AUD', 'AU', '31', null, true);
        HEP_Territory__c GlobalTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '', null, true);
        
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_PROMOTIONS'));
        System.debug('%^^&& setOfUserTerritory :: ' + setOfUserTerritory);
        //List<HEP_List_Of_Values__c> lstListOfValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
        
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        User MarketingUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@gmail.com', 'vishnur', 'vishnr','v','', true);
        
        HEP_Role__c marketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', '', true);
        HEP_User_Role__c userRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(Australia.id, marketingManager.id,MarketingUser.id , true);
        

        HEP_Promotion__c testPromo1 = HEP_Test_Data_Setup_Utility.createPromotion('Deadpool 2','Global', null, Globalterr.id, FoxNewRelease.id, null, null, false);
        testPromo1.FirstAvailableDate__c = Date.today();
        insert testPromo1;
        
        HEP_Promotion__c testPromo2 = HEP_Test_Data_Setup_Utility.createPromotion('Baaghi 2','National', null, Australia.id, FoxNewRelease.id, null, null, true);
                
        HEP_Promotion__c testPromo3 = HEP_Test_Data_Setup_Utility.createPromotion('Baaghi 3','Customer', null, Australia.id, FoxNewRelease.id, null, null, false);
        testPromo3.Status__c = 'Submitted';
        insert testPromo3;
        HEP_Promotion__c testPromo4 = HEP_Test_Data_Setup_Utility.createPromotion('Baaghi 4','National', null, Australia.id, FoxNewRelease.id, null, null, true);
        HEP_Promotion__c testPromo5 = HEP_Test_Data_Setup_Utility.createPromotion('Baaghi 5','National', null, Australia.id, FoxNewRelease.id, null, null, true);
        HEP_Promotion__c testPromo6 = HEP_Test_Data_Setup_Utility.createPromotion('Baaghi 6','National', null, Australia.id, FoxNewRelease.id, null, null, true);
        
        /*HEP_Market_Spend__c spend1 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1000002', testPromo2.id, 'Pending', false);
        spend1.
        HEP_Market_Spend__c spend2 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1000003', testPromo3.id, 'Pending', false);
        HEP_Market_Spend__c spend3 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1000004', testPromo4.id, 'Pending', false);
        HEP_Market_Spend__c spend4 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1000005', testPromo5.id, 'Pending', false);
        HEP_Market_Spend__c spend5 = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1000006', testPromo6.id, 'Pending', false);
        */
        System.runAs(MarketingUser){
        //Promotion Name search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Promotion Name', 'Starts With', 'Dead', null, null);
        System.debug('setOfUserTerritory :: '+setOfUserTerritory);
        //Local promo search
        HEP_Promotion__c localpromo = [SELECT LocalPromotionCode__c FROM HEP_Promotion__c WHERE PromotionName__c LIKE 'Deadpool%' LIMIT 1];
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Local Promo Code (Map ID)', 'Starts With', localpromo.LocalPromotionCode__c, null, null);
        
        //Territory search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Territory', 'Contains','aus', null, null);
        
        //Status Search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Status', 'Contains','', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Status', 'Starts With','Sub', null, null);
        
        
        //Line of Business search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Line of Business', 'Contains','Fox', null, null);
        
        //Title search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Title Name', 'Starts With','Fox', null, null);
        
        //FTID search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Financial Title ID', 'Contains','Red', null, null);
        
        //Region Search
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Region', 'Contains','aus', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Region', 'Starts With','aus', null, null);
        
        // Total market spend
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Total Market Spend', 'Greater Than','10000', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Total Market Spend', 'Lesser Than','10000', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Total Market Spend', 'Equals','10000', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Total Market Spend', 'Greater Than Or Equals','10000', null, null);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Total Market Spend', 'Lesser Than Or Equals','10000', null, null);
        
        //Keyword
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'Keyword', 'Starts With','Baa', null, null);
        
        //FAD 
        Date startDate = Date.today();
        startDate.addDays(-5);
        Date endDate = Date.Today();
        endDate.addDays(2);
        HEP_Search_Promotions_Controller.getPromotionData('DD-MMM-YYYY', 'FAD (Date Range)', 'Starts With','', startDate, endDate);
        }
    }
}