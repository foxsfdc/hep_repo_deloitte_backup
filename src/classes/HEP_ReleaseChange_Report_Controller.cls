/**
* HEP_ReleaseSchedule_Report_Controller --- Class to get the data for releaseschedule reports
* @author    Arjun Narayanan
*/
public class HEP_ReleaseChange_Report_Controller {

	public static String sACTIVE;
	static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        }

    public class PicklistWrapper{
		LIST<HEP_Utility.PicklistWrapper> lstTerritories;

		public PicklistWrapper(){
			lstTerritories = new LIST<HEP_Utility.PicklistWrapper>();
		}
	}    

/**
* pageData --- Wrapper class to hold all the page data
* @author    Arjun Narayanan
*/
    public class PageData{
    	list<ReportRowData> lstPromotionReportData;

/**
* Class Constructor
* @return nothing
* @author Arjun Narayanan
*/
    	public PageData(){
    		lstPromotionReportData = new list<reportRowData>();
    	}
    }

/**
* reportRowData --- Wrapper class to hold one record data
* @author    Arjun Narayanan
*/
    public class ReportRowData{
    	Date dtChangeDate;
    	String sTerritory;
    	String sLicensor;
    	String sLOB;
    	String sPromotionName;
    	String sPromocode;
    	Date dtFAD;
    	String sMarketingManager;
    	String sChangeType;
    	String sPromotionOldValue;
    	String sPromotionNewValue;

    	
    }

/**
     * Helps to picklist values on page load.
     * @return PicklistWrapper wrapper object 
     */
    @RemoteAction
    public static PicklistWrapper getPicklistData(){

    	PicklistWrapper objPicklistWrapper = new PicklistWrapper();

    	for(HEP_territory__c objTerritory : [SELECT id,Name FROM HEP_territory__c WHERE Name != :HEP_Utility.getConstantValue('REGION_HOME_OFFICE') AND Parent_Territory__c = null ORDER BY Name]){
			objPicklistWrapper.lstTerritories.add(new HEP_Utility.PicklistWrapper(objTerritory.id, objTerritory.Name));
		}

		return objPicklistWrapper;
    }

/**
     * Helps to display data when report is run.
     * @param  sTerritoryValue value of Territory for the report
     * @param  sLocalDateFormat user locale format
     * @return PageData wrapper object 
     */
    @RemoteAction
    public static PageData getPageData(String sTerritoryValue, String sLocalDateFormat){
    	PageData objPageData = new PageData();
    	Set<String> setTerritory = new Set<String>();
    	List<HEP_Promotion__c> lstPromotion = new List<HEP_Promotion__c>();
    	List<HEP_Promotion_Dating__History> lstPromotionDating = new List<HEP_Promotion_Dating__History>();
    	Map<String,String> mapOfLicensorForPromotion = new Map<String,String>();

    	//To get the territory for which report is run
    /*	for(HEP_Utility.PicklistWrapper objPicklistTerritory : objPicklistValue.lstTerritories){
    		setTerritory.add(objPicklistTerritory.sValue);
    	} */

    	//Query to get the promotion dating changes
    	lstPromotionDating = [SELECT OldValue, NewValue,Field,Parent.Id, Parent.Name,Parent.Promotion__r.LineOfBusiness__r.Type__c,
    								 Parent.Date__c, Parent.Date_type__c,Parent.HEP_Catalog__r.Licensor__c,
    								 CreatedDate,Parent.Promotion__r.Territory__r.Name,Parent.Promotion__r.PromotionName__c,
    								 Parent.Promotion__r.LocalPromotionCode__c,Parent.Promotion__r.FirstAvailableDate__c,
    								 Parent.Promotion__r.Domestic_Marketing_Manager__r.Name,Parent.Territory__r.Name,
    								 Parent.HEP_Promotions_Dating_Matrix__r.Date_Type__c
									 FROM HEP_Promotion_Dating__History
									 WHERE Parent.Promotion__r.Territory__c =: sTerritoryValue
									 AND Field =: HEP_Utility.getConstantValue('DATE__C')];

		integer icount = Limits.getQueryRows();
		integer inewLimit = integer.valueof(HEP_Utility.getConstantValue('HEP_SOQL_LIMIT')) - icount;
		system.debug('----count---->'+icount);
		system.debug('----newLimitcount---->'+inewLimit);
		//start loop through dating history records 
		for(HEP_Promotion_Dating__History objPromotionDatingHistory : lstPromotionDating){
			if(Date.valueOf(objPromotionDatingHistory.CreatedDate).daysBetween(System.today()) < integer.valueOf(HEP_Utility.getConstantValue('HEP_DAYS_28')) && HEP_Utility.getConstantValue('DATE__C').equalsIgnoreCase(objPromotionDatingHistory.Field)){
				ReportRowData objRowData = new ReportRowData();
				System.debug('no of days between for dating--->'+Date.valueOf(objPromotionDatingHistory.CreatedDate).daysBetween(System.today()));
				objRowData.dtChangeDate = Date.valueOf(objPromotionDatingHistory.CreatedDate);
				objRowData.sTerritory = objPromotionDatingHistory.Parent.Territory__r.Name;
				objRowData.sLicensor = objPromotionDatingHistory.Parent.HEP_Catalog__r.Licensor__c;
				objRowData.sLOB = objPromotionDatingHistory.Parent.Promotion__r.LineOfBusiness__r.Type__c;
				objRowData.sPromotionName = objPromotionDatingHistory.Parent.Promotion__r.PromotionName__c;
				objRowData.sPromocode = objPromotionDatingHistory.Parent.Promotion__r.LocalPromotionCode__c;
				objRowData.dtFAD = objPromotionDatingHistory.Parent.Promotion__r.FirstAvailableDate__c;
				objRowData.sMarketingManager = objPromotionDatingHistory.Parent.Promotion__r.Domestic_Marketing_Manager__r.Name;
				objRowData.sChangeType = objPromotionDatingHistory.Parent.HEP_Promotions_Dating_Matrix__r.Date_Type__c+' '+HEP_Utility.getConstantValue('HEP_CHANGE');
				objRowData.sPromotionOldValue = HEP_Utility.getFormattedDate(Date.valueOf(objPromotionDatingHistory.Oldvalue),sLocalDateFormat);
				objRowData.sPromotionNewValue = HEP_Utility.getFormattedDate(Date.valueOf(objPromotionDatingHistory.NewValue),sLocalDateFormat);

				objPageData.lstPromotionReportData.add(objRowData);
			}

		}
		//end loop through dating history records
			
		//Query promotions and history records to get changes on promotion records							 
    	lstPromotion = [SELECT Id,Name, title__r.Name, Title__c,CreatedDate,
    							Territory__r.Name,PromotionName__c,LocalPromotionCode__c,
    							FirstAvailableDate__c,Domestic_Marketing_Manager__r.Name,
		    					(SELECT NewValue, Oldvalue, Field, Parent.id,CreatedDate,
		    							Parent.PromotionName__c,Parent.title__r.Name,
		    							Parent.CreatedDate,Parent.Territory__r.Name,Parent.LineOfBusiness__r.Type__c,
		    							Parent.LocalPromotionCode__c,Parent.FirstAvailableDate__c,
		    							Parent.Domestic_Marketing_Manager__r.Name
		    							FROM Histories
		    							WHERE (Field =: HEP_Utility.getConstantValue('HEP_TITLE__C') OR Field =: HEP_Utility.getConstantValue('CREATED'))),
		    					(SELECT Catalog__r.Licensor__c,Promotion__r.id 
		    					FROM HEP_Promotion_Catalogs__r)
								FROM hep_promotion__c
								WHERE Territory__r.id =:sTerritoryValue
								AND Record_Status__c =: sACTIVE
								LIMIT :inewLimit];

		//start Looping to get map of catalog licensor values
		for(HEP_Promotion__c objPromotionToCatalog : lstPromotion){
			for(HEP_Promotion_Catalog__c objPromotionCatalog : objPromotionToCatalog.HEP_Promotion_Catalogs__r){
				mapOfLicensorForPromotion.put(objPromotionCatalog.Promotion__r.id, objPromotionCatalog.Catalog__r.Licensor__c);
			}
		}
		//end Looping to get map of catalog licensor values

		//Start looping over promotion record
		for(HEP_Promotion__c objPromotion : lstPromotion){
			//Start looping over promotion history records
			for(HEP_Promotion__History objHistoryRecord : objPromotion.Histories){
				//Condition to get only changes for last 28 days and only if the changed field is a field we require
				if(Date.valueOf(objHistoryRecord.CreatedDate).daysBetween(System.today()) < integer.valueOf(HEP_Utility.getConstantValue('HEP_DAYS_28')) && (HEP_Utility.getConstantValue('HEP_TITLE__C').equalsIgnoreCase(objHistoryRecord.Field) || HEP_Utility.getConstantValue('CREATED').equalsIgnoreCase(objHistoryRecord.Field))){
					ReportRowData objRowData = new ReportRowData();
					System.debug('no of days between for promotion--->'+Date.valueOf(objHistoryRecord.CreatedDate).daysBetween(System.today()));

					try{
						id titleid = String.valueof(objHistoryRecord.newValue);
						if(objHistoryRecord.newValue == null){
							titleid = string.valueof(objHistoryRecord.Oldvalue);
						}
						continue;
					}
					catch(exception e){
						system.debug('id');
					}

					objRowData.dtChangeDate = Date.valueOf(objHistoryRecord.CreatedDate);
					objRowData.sTerritory = objHistoryRecord.Parent.Territory__r.Name;
					objRowData.sLicensor = mapOfLicensorForPromotion.get(objHistoryRecord.Parent.id);
					objRowData.sLOB = objHistoryRecord.Parent.LineOfBusiness__r.Type__c;
					objRowData.sPromotionName = objHistoryRecord.Parent.PromotionName__c;
					objRowData.sPromocode = objHistoryRecord.Parent.LocalPromotionCode__c;
					objRowData.dtFAD = objHistoryRecord.Parent.FirstAvailableDate__c;
					objRowData.sMarketingManager = objHistoryRecord.Parent.Domestic_Marketing_Manager__r.Name;

					if(HEP_Utility.getConstantValue('HEP_TITLE__C').equalsIgnoreCase(objHistoryRecord.Field)){
						if(objHistoryRecord.Oldvalue == null && objHistoryRecord.NewValue != null){
							objRowData.sChangeType = HEP_Utility.getConstantValue('HEP_ADDED_TITLE');
						}
						else if(objHistoryRecord.Oldvalue != null && objHistoryRecord.NewValue == null){
							objRowData.sChangeType = HEP_Utility.getConstantValue('HEP_REMOVED_TITLE');
						}
					}
					else if(HEP_Utility.getConstantValue('CREATED').equalsIgnoreCase(objHistoryRecord.Field)){
						objRowData.sChangeType = HEP_Utility.getConstantValue('HEP_NEW_PROMOTION');
					}

					objRowData.sPromotionOldValue = String.valueOf(objHistoryRecord.Oldvalue);
					objRowData.sPromotionNewValue = String.valueOf(objHistoryRecord.NewValue);

					objPageData.lstPromotionReportData.add(objRowData);
				}	
				
			}
			//End looping over promotion history records
		}
		//End looping over promotion record

		return objPageData;
    }
}