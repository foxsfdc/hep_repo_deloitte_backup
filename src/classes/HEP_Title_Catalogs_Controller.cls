/**
 * HEP_Title_Master_Catalog_Controller ---   Class to display in grid all the Global Calatog related to selected Title.
 * @author    Roshi Rai
 */
Public without sharing  class HEP_Title_Catalogs_Controller {
  
    /**
     *  RightsWrapper  Wrapper class to be hold data Global Catalog Related to Selected Title
     *  @author    Roshi Rai
     **/
    Public class CatalogWrapper {
        list < string > errorMessages;
        list < RowData > lstRowData;
        public CatalogWrapper() {
            lstRowData = new list < RowData > ();
            errorMessages = new list < string > ();
        }
    }
    /**
     * RowData Class to hold the row data to be displayed on AG Grid
     * @author    Roshi Rai
     **/
    public class RowData {
        Public String sCatalogName;
        Public string sCatalogId;
        Public String sType;
        Public String sStatus;
        Public String sCatalogRecordID;
    }
    /**
     * Retrieves Primary method which will be called from the Title Master Catalogs page to retrieve the Catalog data.
     * @param    String sTitleID - Id of the title
     * @return   CatalogWrapper
     */
    @RemoteAction
    public Static CatalogWrapper extractAllCatalogForTitleCatalog(String sTitleId) {
        
        list < string > genericIntegrationErrors;
        //get the generic integraion error from custom label
        if (string.isNotEmpty(System.Label.HEP_INTEGRATION_CATALOG_ERROR)) genericIntegrationErrors = System.Label.HEP_INTEGRATION_CATALOG_ERROR.split(',');
        try {
            CatalogWrapper objCatalogWrap = new CatalogWrapper();
            List < HEP_CatalogTitleWrapper > lstCatalogTitleWrapper = new List < HEP_CatalogTitleWrapper > ();
            // cheking whether Use has Access to the Territory, Applying Territory Filter
            set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_TITLE_CATALOGS'));
            map < id, id > mapOfUserTerritory = new map < id, id > ();
            String sTerritoyId = [SELECT id 
                                  FROM   HEP_Territory__c 
                                  WHERE  Name = : HEP_Utility.getConstantValue('CATALOG_TYPE_GLOBAL')].id;
            if (setOfUserTerritory != NULL) {
                for (String obj: setOfUserTerritory) {
                    mapOfUserTerritory.put(obj, obj);
                }
            }
            // FoxId of the related title
            String sFOXId = [SELECT id, FOX_ID__c 
                             FROM   EDM_GLOBAL_TITLE__c 
                             WHERE  id = : sTitleId 
                             AND    LIFE_CYCL_STAT_GRP_CD__c != : HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL')].FOX_ID__c;

            System.debug('sFOXId --------->' + sFOXId);
            // Calling the API and getting response
            if (sFOXId != null && sFOXId != '') {
                HEP_InterfaceTxnResponse objIntTxnRspProduct = HEP_ExecuteIntegration.executeIntegMethod(sFOXId, '', HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG_TITLE'));
                if (!objIntTxnRspProduct.sStatus.equals('Class Not found')) {
                    HEP_CatalogTitleWrapper wrapObj = (HEP_CatalogTitleWrapper) JSON.deserialize((objIntTxnRspProduct.sResponse), HEP_CatalogTitleWrapper.class);
                    // if user has access to the territory will assign record from response to display on page 
                    if (wrapObj != NULL && !mapOfUserTerritory.isEmpty() && mapOfUserTerritory.containsKey(sTerritoyId)) {
                        for (HEP_CatalogTitleWrapper.Data objwraptemp: wrapObj.Data) {
                            System.debug('objwraptemp*****' + objwraptemp);
                            RowData objRowData = new RowData();
                            objRowData.sCatalogName = objwraptemp.attributes.productName;
                            objRowData.sCatalogId = objwraptemp.attributes.productId;
                            objRowData.sType = objwraptemp.attributes.productTypeDescription;
                            objRowData.sStatus = objwraptemp.attributes.productStatusDescription;
                            objCatalogWrap.lstRowData.add(objRowData);
                        }
                    }
                }
            }
            System.debug('*****objCatalogWrap******' + json.serialize(objCatalogWrap));
            return objCatalogWrap;
        } catch (Exception ex) {
            system.debug('Error while fetching data from Foxipedia : ' + ex + ' Line Number : ' + ex.getLineNumber());
            //Load error message to page
            CatalogWrapper objCatalogWrapper = new CatalogWrapper();
            objCatalogWrapper.errorMessages = genericIntegrationErrors;
            System.debug('objHEPCatalogTitleWrapper second : ' + objCatalogWrapper);
            return objCatalogWrapper;
        }
    }
    /**
     * Retrieves Primary method which will be called from the Title Master Catalogs page to retrieve the Catalog data.
     * @param    String sTitleID - Id of the title
     * @return   CatalogWrapper
     */
    @RemoteAction
    public Static CatalogWrapper extractAllCatalogForTitleProduct(String sTitleId) {
        list < string > genericIntegrationErrors;
        //get the generic integraion error from custom label
        if (string.isNotEmpty(System.Label.HEP_INTEGRATION_CATALOG_ERROR)) genericIntegrationErrors = System.Label.HEP_INTEGRATION_CATALOG_ERROR.split(',');
        try {
            CatalogWrapper objCatalogWrap = new CatalogWrapper();
            List < HEP_CatalogTitleWrapper > lstCatalogTitleWrapper = new List < HEP_CatalogTitleWrapper > ();
            
            // cheking whether Use has Access to the Territory, Applying Territory Filter
            set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_TITLE_CATALOGS'));
            map < id, id > mapOfUserTerritory = new map < id, id > ();
            String sTerritoyId = [SELECT id 
                                  FROM   HEP_Territory__c 
                                  WHERE  Name = : HEP_Utility.getConstantValue('CATALOG_TYPE_GLOBAL')].id;
            if (setOfUserTerritory != NULL) {
                for (String obj: setOfUserTerritory) {
                    mapOfUserTerritory.put(obj, obj);
                }
            }
            // FoxId of the related title
            String sFOXId = [SELECT id, FOX_ID__c 
                             FROM   EDM_GLOBAL_TITLE__c
                             WHERE  id = : sTitleId 
                             AND    LIFE_CYCL_STAT_GRP_CD__c != : HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL')].FOX_ID__c;
            // Calling the API and getting response
            if (sFOXId != null && sFOXId != '') { //2431818  --108544
                HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(sFOXId, '', HEP_Utility.getConstantValue('HEP_FOXIPEDIA_PRODUCT'));
                if (!objIntTxnRsp.sStatus.equals('Class Not found')) {
                    //Checks the conditions for New Release if country code is US,Media type is THE and Rating entity code is MPAA  
                    HEP_CatalogTitleWrapper wrapObj = (HEP_CatalogTitleWrapper) JSON.deserialize((objIntTxnRsp.sResponse), HEP_CatalogTitleWrapper.class);
                    // if user has access to the territory will assign record from response to display on page 
                    if (wrapObj != NULL && !mapOfUserTerritory.isEmpty() && mapOfUserTerritory.containsKey(sTerritoyId)) {
                        for (HEP_CatalogTitleWrapper.Data objwraptemp: wrapObj.Data) {
                            System.debug('objwraptemp*****' + objwraptemp);
                            RowData objRowData = new RowData();
                            objRowData.sCatalogName = objwraptemp.attributes.productName;
                            objRowData.sCatalogId = objwraptemp.attributes.productId;
                            objRowData.sType = objwraptemp.attributes.productTypeDescription;
                            objRowData.sStatus = objwraptemp.attributes.productStatusDescription;
                            objCatalogWrap.lstRowData.add(objRowData);
                        }
                    }
                }
            }
            System.debug('*****objCatalogWrap******' + json.serialize(objCatalogWrap));
            return objCatalogWrap;
        } catch (Exception ex) {
            system.debug('Error while fetching data from Foxipedia : ' + ex + ' Line Number : ' + ex.getLineNumber());
            //Load error message to page
            CatalogWrapper objCatalogWrapper = new CatalogWrapper();
            //genericIntegrationErrors.add(errorValue.errorMessage);
            objCatalogWrapper.errorMessages = genericIntegrationErrors;
            System.debug('objHEPCatalogTitleWrapper second : ' + objCatalogWrapper);
            return objCatalogWrapper;
        }
    }
    /**
     * Retrieves Primary method which will be called from the Title Master Catalogs page to retrieve the Catalog ID of selected Hyperlink.
     * @param    String sTitleID - Id of the Catalog
     * @return   Catalog ID
     */
    @RemoteAction
    public Static String getCatalogRecordId(String CatID) {
        try {
            string HEP_RECORD_ACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            string HEP_RECORD_CANCELLED = HEP_Utility.getConstantValue('HEP_RECORD_CANCELLED');
            //if catalog is Active and Contains the selected Catalog Id from Foxipedia return Catalog ID
            String sCatrecordId = [SELECT id, CatalogId__c, Unique_Id__c 
                                   FROM   HEP_Catalog__c 
                                   WHERE  Unique_Id__c LIKE: '%' + CatID + '%'
                                   AND Territory_Name__c = : HEP_Utility.getConstantValue('CATALOG_TYPE_GLOBAL')
                                   AND(Record_Status__c = : HEP_RECORD_ACTIVE OR Record_Status__c = : HEP_RECORD_CANCELLED) ].id;
            return sCatrecordId;
        } catch (Exception ex) {
            return null;
        }
    }
}