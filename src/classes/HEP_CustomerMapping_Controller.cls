/**
     *HEP_CustomerMapping_Controller - To show Customer mapping data for Global customers 
     *@author : Ashutosh Arora
     */
    public without sharing class HEP_CustomerMapping_Controller {
        public static String sACTIVE;
        public static String sAmazon;
        public static String sMicrosoft;
        public static String sMicrosoftXbox;
        public static String sApple;
        public static String sGoogle;
        public static String sFILM;
        public static String sEPISODECODE;
        public static String sACCELCODE;
        public static String sSEASN;
        public static String sSEASON;
        public static String sEPISODE;
        public static String sTV;
        public static String sDELETED;
        public static String sTRUE;
        public static string sALLGLOBALCUSTOMERS;
        public static string sMDPCustomers;
        static{
            sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            sMicrosoftXbox = HEP_Utility.getConstantValue('HEP_CUSTOMER_MICROSOFT_XBOX');
            sAmazon = HEP_Utility.getConstantValue('HEP_CUSTOMER_AMAZON');
            sMicrosoft = HEP_Utility.getConstantValue('HEP_CUSTOMER_MICROSOFT');
            sApple = HEP_Utility.getConstantValue('HEP_CUSTOMER_APPLE');
            sGoogle = HEP_Utility.getConstantValue('HEP_CUSTOMER_GOOGLE');
            sMDPCustomers = HEP_Utility.getConstantValue('HEP_CUSTOMER_TYPE_MDP');
            sFILM = HEP_Utility.getConstantValue('HEP_PRODUCT_TYPE_FILM');
            sSEASN = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SEASN');
            sEPISODECODE = HEP_Utility.getConstantValue('HEP_TITLE_CODE_EPSD');
            sACCELCODE = HEP_Utility.getConstantValue('HEP_TITLE_CODE_ACCEL');
            sSEASON = HEP_Utility.getConstantValue('HEP_SEASON');
            sEPISODE = HEP_Utility.getConstantValue('HEP_EPISODE');
            sTV = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_TV');
            sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
            sTRUE = HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE');
            sALLGLOBALCUSTOMERS = HEP_Utility.getConstantValue('HEP_GLOBAL_CUSTOMERS');
        }
        
        /**
         *fetchData - To fetch the Mapping data to be displayed on UI called on Page Load
         *@return ProductMappingWrapper (ProductMappingWrapper Wrapper with Customer Mapping Data)
         *@author Ashutosh Arora
         */
        @RemoteAction
        public static ProductMappingWrapper fetchData(String sTitleId) {
            System.debug('HEP_CUSTOMER_MICROSOFT_XBOX : ' + sMicrosoft);
            System.debug('TRUEEEEE  FETCH : ' + sTRUE);
            List<HEP_User_Role__c> tmpLstUserRole = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
            System.debug('USER ROLE DATA : ' + tmpLstUserRole);
            List<String> lstGlobalCustomers = sALLGLOBALCUSTOMERS.split(',');
            System.debug('lstGlobalCustomers :' + lstGlobalCustomers);
            Set<Id> setTerritoryIds = new Set<Id>();
            if(tmpLstUserRole != null){
                for(HEP_User_Role__c objUserRole : tmpLstUserRole){
                    if(objUserRole.read__c==true || objUserRole.write__c==true)
                        setTerritoryIds.add(objUserRole.Territory__c);
                }
            }
            System.debug('TERRITORY IDS THAT USER HAS ACCESS TO : ' + setTerritoryIds);
            //Get List of all Customers and Regions to display in dropdown for filter
            List < HEP_Customer__c > tmpLstCustomer = [SELECT Id, CustomerName__c FROM HEP_Customer__c
                                                        WHERE Record_Status__c =: sACTIVE
                                                        AND Customer_Type__c =: sMDPCustomers
                                                        AND CustomerName__c IN: lstGlobalCustomers
                                                        ORDER BY CustomerName__c ASC
                                                       ];
            System.debug('tmpLstCustomer : ' + tmpLstCustomer);
            List < HEP_Territory__c > tmpLstRegion = [SELECT Id, Name, MM_Territory_Code__c,Parent_Territory__c
                                                        FROM HEP_Territory__c
                                                        WHERE ID IN: setTerritoryIds
                                                        ORDER BY Name ASC
                                                     ];
            for(HEP_Territory__c objTerr : [SELECT Id, Name, MM_Territory_Code__c, Parent_Territory__c
                                            FROM HEP_Territory__c
                                            WHERE MM_Territory_Code__c != null AND
                                            Parent_Territory__c IN: tmpLstRegion]){
                                tmpLstRegion.add(objTerr);
            
            }                       
            List<HEP_Territory__c> lstDropRegions = new List<HEP_Territory__c> ();
            for(HEP_Territory__c objRegions : tmpLstRegion){
                if(objRegions.MM_Territory_Code__c != null){
                    lstDropRegions.add(objRegions);
                }
            }   
            System.debug('tmpLstRegion : ' + tmpLstRegion);                                      
            // Getting first Global Customer Name and Territory from the list to display data initially on page load
            String sInitialRegion;
            String sInitialCustomer;
            if(String.isNotBlank(tmpLstCustomer[0].CustomerName__c))
                sInitialCustomer = tmpLstCustomer[0].CustomerName__c;
            if(lstDropRegions.size() > 0)
                sInitialRegion = lstDropRegions[0].Name;
            System.debug('sInitialCustomer : ' + sInitialCustomer);
            System.debug('sInitialRegion : ' + sInitialRegion);
            ProductMappingWrapper objMappingData = getMappingData(sTitleId, sInitialCustomer, sInitialRegion);
            //Return the mapping data to display on UI
            return objMappingData;
        }
        /**
         *fetchData - To fetch the Mapping data to be displayed on UI
         *@return ProductMappingWrapper (ProductMappingWrapper Wrapper with Customer Mapping Data)
         *@author Ashutosh Arora
         */
        @RemoteAction
        public static ProductMappingWrapper getMappingData(String sTitleId, String sCustomerName, String sRegionName) {
            String sProductType;
            System.debug('TRUEEEEE  MAPPING DATA : ' + sTRUE);
            List<HEP_User_Role__c> tmpLstUserRole = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
            List<String> lstGlobalCustomers = sALLGLOBALCUSTOMERS.split(',');
            System.debug('lstGlobalCustomers SPLIT ' + lstGlobalCustomers);
            Set<Id> setTerritoryIds = new Set<Id>();
            if(tmpLstUserRole != null){
                System.debug('AND THIS--->' + tmpLstUserRole);
                for(HEP_User_Role__c objUserRole : tmpLstUserRole){
                    if(objUserRole.read__c==true || objUserRole.write__c==true){
                        setTerritoryIds.add(objUserRole.Territory__c);
                    }
                }
            }
            List<HEP_Territory__c> lstTerritory = [SELECT Id, Name,Parent_Territory__c
                                                    FROM HEP_Territory__c
                                                    WHERE Name =:sRegionName];
            Map<String,String> mapChildParentTerrIds = new Map<String,String>();
            List<HEP_Territory__c> lstChildParent = new List<HEP_Territory__c>();
            Set < String > setCustomers = new Set < String > ();
            Set < String > setRegions = new Set < String > ();
            List < string > lstFilmSubTypes = new List < String > ();
            //Get All Product Types and Product Type Codes from Custom Setting
            List < HEP_EMD_Prod_Type_Sub_Type__c > lstProductTypes = [SELECT Product_Sub_Type__c, Product_Type_Code__c FROM HEP_EMD_Prod_Type_Sub_Type__c];
            for (HEP_EMD_Prod_Type_Sub_Type__c objProductType: lstProductTypes) {
                if (objProductType.Product_Sub_Type__c.equalsIgnoreCase(sFILM)) {
                    lstFilmSubTypes.add(objProductType.Product_Type_Code__c);
                }
            }
            List < String > lstEpisodeType = new List < String > ();
           List < HEP_EMD_Prod_Type_Sub_Type__c > lstAllEpisodeCodes = [Select Product_Type_Code__c, Product_Sub_Type__c from HEP_EMD_Prod_Type_Sub_Type__c where Product_Sub_Type__c =: sEPISODECODE AND Product_Type_Code__c !=: sACCELCODE];
            for (HEP_EMD_Prod_Type_Sub_Type__c objEpisodeTypes: lstAllEpisodeCodes) {
                lstEpisodeType.add(objEpisodeTypes.Product_Type_Code__c);
            }
            //Getting Product Type on Title Record
            List < EDM_GLOBAL_TITLE__c > lstTitleProductType = [SELECT PROD_TYP_CD__r.PROD_TYP_CD__c FROM EDM_GLOBAL_TITLE__c WHERE ID =: sTitleId
            AND LIFE_CYCL_STAT_GRP_CD__c != :HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL')];
            String titleProductType;
            if (lstTitleProductType != null && !lstTitleProductType.isEmpty()) {
                titleProductType = lstTitleProductType[0].PROD_TYP_CD__r.PROD_TYP_CD__c;
            }
            //Checking the Product type on title
            if (lstFilmSubTypes.contains(titleProductType)) {
                sProductType = sFILM;
            } else if (titleProductType.equalsIgnoreCase(sSEASN)) {
                sProductType = sSEASON;
            } else if (lstEpisodeType.contains(titleProductType)) {
                sProductType = sEPISODE;
            }
            System.debug('sProductType : ' + sProductType);
            List < HEP_Customer__c > tmpLstCustomer = [SELECT Id, CustomerName__c
                                                        FROM HEP_Customer__c
                                                        WHERE Record_Status__c =: sACTIVE
                                                        AND Customer_Type__c =: sMDPCustomers
                                                        AND CustomerName__c IN: lstGlobalCustomers
                                                        ORDER BY CustomerName__c ASC
                                                      ];
            System.debug('^^^^^^tmpLstCustomer' + tmpLstCustomer);
            List < HEP_Territory__c > tmpLstRegion = [SELECT Id, Name,MM_Territory_Code__c
                                                        FROM HEP_Territory__c
                                                        WHERE Id IN: setTerritoryIds
                                                        ORDER BY Name ASC
                                                     ];
            for(HEP_Territory__c objTerr : [SELECT Id, Name, MM_Territory_Code__c, Parent_Territory__c
                                            FROM HEP_Territory__c
                                            WHERE MM_Territory_Code__c != null AND
                                            Parent_Territory__c IN: tmpLstRegion]){
                                tmpLstRegion.add(objTerr);
            
            
            }
            
            if(tmpLstRegion != null){
            lstChildParent = [SELECT Id, Parent_Territory__c
                                                     FROM HEP_Territory__c
                                                     WHERE Id IN: tmpLstRegion];
            }
            //Add child and it's parent territory Id to the map 
            for(HEP_Territory__c objchidParent : lstChildParent){
                mapChildParentTerrIds.put(objchidParent.Id,objchidParent.Parent_Territory__c);
            }
            System.debug('Parent Child Map : ' + mapChildParentTerrIds);
            List<HEP_Territory__c> lstDropRegions = new List<HEP_Territory__c>();
            for(HEP_Territory__c objRegions : tmpLstRegion){
                if(objRegions.MM_Territory_Code__c != null){
                    lstDropRegions.add(objRegions);
                }
            }       
            ProductMappingWrapper objProductMappingWrapper = new ProductMappingWrapper();
            List < HEP_MDP_Product_Mapping__c > lstMappingRecords;
            Map < String, HEP_MDP_Product_Mapping__c > mapIdMappingRecord = new Map < String, HEP_MDP_Product_Mapping__c > ();
            if (sCustomerName != null && sRegionName != null) {
                //Query the mapping records based on TitleId, Customer Name, Region Name and Record Status as 'Active'
                lstMappingRecords = [SELECT ASIN__c,
                    Eidr__c,
                    ETAG__c,
                    Title__c,
                    Big_Id__c,
                    Format__c,
                    Channel__c,
                    Adam_Id__c,
                    Media_Id__c,
                    Series_Id__c,
                    Google_Id__c,
                    Title__r.Name,
                    Season_ASIN__c,
                    Series_Title__c,
                    HEP_Customer__c,
                    Component_Id__c,
                    Series_Big_Id__c,
                    Season_Number__c,
                    HEP_Territory__c,
                    Record_Status__c,
                    Episode_Number__c,
                    Series_Media_Id__c,
                    Local_Description__c,
                    X1st_Episode_ASIN__c,
                    Title__r.PROD_TYP_CD__c,
                    HEP_Customer__r.CustomerName__c,
                    (SELECT ParentId, OldValue, NewValue, Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)
                    FROM HEP_MDP_Product_Mapping__c
                    WHERE Title__c =: sTitleId
                    AND HEP_Territory__r.Name =: sRegionName
                    AND HEP_Customer__r.CustomerName__c =: sCustomerName
                    AND Record_Status__c =: sACTIVE
                ];
            }
            System.debug('lstMappingRecords : ' + lstMappingRecords);
            //If the list of Mapping records returned from query is not null and not empty                  
            if (lstMappingRecords != null && !lstMappingRecords.isEmpty()) {
                for (HEP_MDP_Product_Mapping__c objTitleCustomerMapping: lstMappingRecords) {
                    ProductDataWrapper objProductDataWrapper = new ProductDataWrapper();
                    mapIdMappingRecord.put(objTitleCustomerMapping.Id, objTitleCustomerMapping);
                    objProductDataWrapper.sRecordId = objTitleCustomerMapping.Id;
                    if (objTitleCustomerMapping.Channel__c != null) objProductDataWrapper.sChannel = objTitleCustomerMapping.Channel__c;
                    if (objTitleCustomerMapping.Format__c != null) objProductDataWrapper.sFormat = objTitleCustomerMapping.Format__c;
                    if (objTitleCustomerMapping.Eidr__c != null) objProductDataWrapper.sEidr = objTitleCustomerMapping.Eidr__c;
                    if (objTitleCustomerMapping.ETAG__c != null) objProductDataWrapper.sEtag = objTitleCustomerMapping.ETAG__c;
                    if (objTitleCustomerMapping.ETAG__c != null) objProductDataWrapper.sEtag = objTitleCustomerMapping.ETAG__c;
                    if (objTitleCustomerMapping.Big_Id__c != null) objProductDataWrapper.sBigId = objTitleCustomerMapping.Big_Id__c;
                    if (objTitleCustomerMapping.Media_Id__c != null) objProductDataWrapper.sMediaId = objTitleCustomerMapping.Media_Id__c;
                    if (objTitleCustomerMapping.Series_Id__c != null) objProductDataWrapper.sSeriesId = objTitleCustomerMapping.Series_Id__c;
                    if (objTitleCustomerMapping.Google_Id__c != null) objProductDataWrapper.sVideoId = objTitleCustomerMapping.Google_Id__c;
                    if (objTitleCustomerMapping.Season_ASIN__c != null) objProductDataWrapper.sSeasonASIN = objTitleCustomerMapping.Season_ASIN__c;
                    if (objTitleCustomerMapping.Series_Title__c != null) objProductDataWrapper.sSeriesTitle = objTitleCustomerMapping.Series_Title__c;
                    if (objTitleCustomerMapping.Component_Id__c != null) objProductDataWrapper.sComponentId = objTitleCustomerMapping.Component_Id__c;
                    if (objTitleCustomerMapping.Series_Big_Id__c != null) objProductDataWrapper.sSeriesBigId = objTitleCustomerMapping.Series_Big_Id__c;
                    if (objTitleCustomerMapping.Season_Number__c != null) objProductDataWrapper.sSeasonNumber = objTitleCustomerMapping.Season_Number__c;
                    if (objTitleCustomerMapping.Episode_Number__c != null) objProductDataWrapper.sEpisodeNumber = objTitleCustomerMapping.Episode_Number__c;
                    if (objTitleCustomerMapping.Series_Media_Id__c != null) objProductDataWrapper.sSeriesMediaId = objTitleCustomerMapping.Series_Media_Id__c;
                    if (objTitleCustomerMapping.X1st_Episode_ASIN__c != null) objProductDataWrapper.s1stEpisodeASIN = objTitleCustomerMapping.X1st_Episode_ASIN__c;
                    if (objTitleCustomerMapping.ASIN__c != null) objProductDataWrapper.sASIN = objTitleCustomerMapping.ASIN__c;
                    if (objTitleCustomerMapping.Local_Description__c != null) objProductDataWrapper.sLocalDescription = objTitleCustomerMapping.Local_Description__c;
                    if (objTitleCustomerMapping.Adam_Id__c != null) objProductDataWrapper.sADAMId = objTitleCustomerMapping.Adam_Id__c;

                    if (objTitleCustomerMapping.Record_Status__c.equalsignorecase(sACTIVE)) {
                        objProductDataWrapper.bIsDeleted = false;
                    }
                    //Iterating over field Histories for the record
                    for (HEP_MDP_Product_Mapping__History objHistory: objTitleCustomerMapping.Histories) {
                        LastModifiedField objHistoryRecord = new LastModifiedField();
                        //Getting history data only for the required fields 
                        objHistoryRecord.sLastModifiedBy = objHistory.CreatedBy.Name;
                        dateTime tempDt = objHistory.CreatedDate;
                        String dateFormatte = tempDT.format('dd-MMM-yyyy') + ' ' + tempDT.format('hh:mm a');
                        objHistoryRecord.sRecordId = objHistory.ParentId;
                        objHistoryRecord.sFieldName = objHistory.Field;
                        objHistoryRecord.sLastModifiedDate = dateFormatte;
                        objHistoryRecord.sLastModifiedData = 'Updated : ' + objHistoryRecord.sLastModifiedBy + ' ' + objHistoryRecord.sLastModifiedDate;
                        if (!objProductDataWrapper.mapFieldHistory.containsKey(objHistoryRecord.sFieldName)) {
                            objProductDataWrapper.mapFieldHistory.put(objHistory.Field, objHistoryRecord);
                        }
                    }
                    objProductDataWrapper.bUniqueASIN = true;
                    objProductDataWrapper.bLocalDescUnique = true;
                    objProductDataWrapper.bIdADAMUnique = true;
                    objProductDataWrapper.bLocalDescUnique = true;
                    objProductDataWrapper.bUniqueVideoId = true;
                    objProductDataWrapper.bMediaIdUnique = true;
                    objProductDataWrapper.bBigIdUnique = true;
                    objProductDataWrapper.bUniqueASIN = true;
                    objProductDataWrapper.bUnique1stEpASIN = true;
                    objProductDataWrapper.bUniqueSeasonASIN = true;
                    objProductMappingWrapper.sSelectedRegion = sRegionName;
                    objProductMappingWrapper.sSelectedCustomer = sCustomerName;
                    objProductMappingWrapper.sIdTitle = sTitleId;
                    objProductMappingWrapper.sSelectedRegionId = lstTerritory[0].Id;
                    objProductMappingWrapper.lstProductData.add(objProductDataWrapper);
                    System.debug('objProductMappingWrapper.lstProductData.   :  ' + objProductMappingWrapper.lstProductData);
                    System.debug('objProductMappingWrapper.sIdTitle : ' + objProductMappingWrapper.sIdTitle);   
                }
            }
            //If the customer name or region name value is null    
            else {
                ProductDataWrapper objProductDataWrapper = new ProductDataWrapper();
                objProductMappingWrapper.sSelectedRegion = sRegionName;
                objProductMappingWrapper.sSelectedCustomer = sCustomerName;
                if(lstTerritory.size() > 0 )
                objProductMappingWrapper.sSelectedRegionId = lstTerritory[0].Id;
                objProductMappingWrapper.sIdTitle = sTitleId;
                if(objProductMappingWrapper.sSelectedRegionId != null){
                        objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(objProductMappingWrapper.sSelectedRegionId,'');
                    }
                objProductMappingWrapper.lstProductData = null;
            }
            if(objProductMappingWrapper.sSelectedRegionId != null && lstTerritory[0].Parent_Territory__c != null){
                objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(objProductMappingWrapper.sSelectedRegionId,'');
                if(mapChildParentTerrIds.get(objProductMappingWrapper.sSelectedRegionId) != null){
                    objProductMappingWrapper.parentTerritoryAccess = HEP_Utility.fetchCustomPermissions(mapChildParentTerrIds.get(objProductMappingWrapper.sSelectedRegionId),'');
                }
                system.debug('This------>' + objProductMappingWrapper.mapTerritoryAccess);
            }else if(objProductMappingWrapper.sSelectedRegionId != null){
                objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(lstTerritory[0].Id,'');
            }
            System.debug('sSelectedCustomer : ' + objProductMappingWrapper.sSelectedCustomer);
            System.debug('sMicrosoft : ' + sMicrosoft);
            //Set the Mapping Type based on Selected Customer and Product Type on Title
            if (objProductMappingWrapper.sSelectedCustomer.containsIgnoreCase(sAmazon)) {
                if (sProductType.equalsIgnoreCase(sSEASON)) {
                    objProductMappingWrapper.sMappingType = sSEASON;
                } else if (sProductType.equalsIgnoreCase(sFILM)) {
                    objProductMappingWrapper.sMappingType = sFILM;
                } else if (sProductType.equalsIgnoreCase(sEPISODE)) {
                    objProductMappingWrapper.sMappingType = sEPISODE;
                }
            } else if (sMicrosoftXbox.containsIgnoreCase(objProductMappingWrapper.sSelectedCustomer)) {
                System.debug('Entered Microsoft if');
                SYstem.debug('sProductType : ' + sProductType);
                if (sProductType.equalsIgnoreCase(sFILM)) {
                    objProductMappingWrapper.sMappingType = sFILM;
                } else if (sProductType.equalsIgnoreCase(sEPISODE) || sProductType.equalsIgnoreCase(sSEASON)) {
                    objProductMappingWrapper.sMappingType = sTV;
                }
                System.debug('xBOX Mapping Type : ' + objProductMappingWrapper.sMappingType);
            } else if (objProductMappingWrapper.sSelectedCustomer.containsIgnoreCase(sApple)) {
                objProductMappingWrapper.sMappingType = '';
            } else if (objProductMappingWrapper.sSelectedCustomer.containsIgnoreCase(sGoogle)) {
                objProductMappingWrapper.sMappingType = '';
            }
            for (HEP_Customer__c objCust: tmpLstCustomer) {
                setCustomers.add(objCust.CustomerName__c);
            }
            objProductMappingWrapper.lstCustomers.addAll(setCustomers);
            System.debug('objProductMappingWrapper.lstCustomers *****' + objProductMappingWrapper.lstCustomers);
            for (HEP_Territory__c objRegion: lstDropRegions) {
                setRegions.add(objRegion.Name);
            }
            objProductMappingWrapper.lstRegions.addAll(setRegions);
            System.debug('Returned records to UI : ' + objProductMappingWrapper);
            System.debug('MAPPING TYPE : ' + objProductMappingWrapper.sMappingType);
            return objProductMappingWrapper;
        }
         /**
         *saveProductData - To save the data updated from UI to back-end
         *@return nothing
         *@author Ashutosh Arora
         */
        @RemoteAction
        public static void saveProductData(ProductMappingWrapper objMappingRecord) {
           if((objMappingRecord.mapTerritoryAccess != null && objMappingRecord.mapTerritoryAccess.get('WRITE').equalsIgnoreCase(sTRUE)) || (objMappingRecord.parentTerritoryAccess != null && objMappingRecord.parentTerritoryAccess.get('WRITE').equalsIgnoreCase(sTRUE))){
                System.debug('USER HAS WRITE ACCESS');
                String customerName = objMappingRecord.sSelectedCustomer;
                String regionName = objMappingRecord.sSelectedRegion;
                List < HEP_Customer__c > lstCustomerId = [SELECT Id from HEP_Customer__c
                    WHERE CustomerName__c =: customerName
                    AND Customer_Type__c =: sMDPCustomers
                    AND Record_Status__c =: sACTIVE
                ];
                String IdCustomer = lstCustomerId[0].Id;
                List < HEP_Territory__c > lstTerritoryId = [SELECT Id from HEP_Territory__c WHERE Name =: regionName];
                String IdRegion = lstTerritoryId[0].Id;
                System.debug('SELECTED REGION : ' +IdRegion);
                String sTitleId = objMappingRecord.sIdTitle;
                System.debug('SELECTED CUSTOMER : ' +IdCustomer);
                System.debug('sTitleId ' + sTitleId);
                List < HEP_MDP_Product_Mapping__c > lstRecordsToUpsert = new List < HEP_MDP_Product_Mapping__c > ();
                if(objMappingRecord.lstProductData!=null && !objMappingRecord.lstProductData.isEmpty()){
                for (ProductDataWrapper objRecord: objMappingRecord.lstProductData) {
                    //System.debug();
                    //When the record already exists in Salesforce
                    if (objRecord.sRecordId != null && objRecord.sRecordId != '') {
                        System.debug('ID IN UPDATE : ' + objRecord.sRecordId);
                        HEP_MDP_Product_Mapping__c objMappingsData = new HEP_MDP_Product_Mapping__c();
                        objMappingsData.Id = objRecord.sRecordId;
                        if (objRecord.sChannel != null) objMappingsData.Channel__c = objRecord.sChannel;
                        if (objRecord.sFormat != null && objRecord.sFormat == 'formatK') objMappingsData.Format__c = '4K';
                        if (objRecord.sFormat != null && objRecord.sFormat != 'formatK') objMappingsData.Format__c = objRecord.sFormat;
                        if (objRecord.sASIN != null) objMappingsData.ASIN__c = objRecord.sASIN;
                        if (objRecord.sLocalDescription != null) objMappingsData.Local_Description__c = objRecord.sLocalDescription;
                        if (objRecord.sADAMId != null) objMappingsData.Adam_Id__c = objRecord.sADAMId;
                        if (objRecord.sBigId != null) objMappingsData.Big_Id__c = objRecord.sBigId;
                        if (objRecord.sComponentId != null) objMappingsData.Component_Id__c = objRecord.sComponentId;
                        if (objRecord.sEidr != null) objMappingsData.Eidr__c = objRecord.sEidr;
                        if (objRecord.sEtag != null) objMappingsData.ETAG__c = objRecord.sEtag;
                        if (objRecord.sMediaId != null) objMappingsData.Media_Id__c = objRecord.sMediaId;
                        if (objRecord.sName != null) objMappingsData.Name__c = objRecord.sName;
                        if (objRecord.bIsDeleted == true) {
                            objMappingsData.Record_Status__c = sDELETED; 
                        } else {
                            objMappingsData.Record_Status__c = sACTIVE;   
                        }
                        if (objRecord.sVideoId != null) objMappingsData.Google_Id__c = objRecord.sVideoId;
                        if (objRecord.sSeasonNumber != null) objMappingsData.Season_Number__c = objRecord.sSeasonNumber;
                        if (objRecord.sSeriesTitle != null) objMappingsData.Series_Title__c = objRecord.sSeriesTitle;
                        if (objRecord.sSeriesId != null) objMappingsData.Series_Id__c = objRecord.sSeriesId;
                        if (objRecord.sSeriesMediaId != null) objMappingsData.Series_Media_Id__c = objRecord.sSeriesMediaId;
                        if (objRecord.sSeriesBigId != null) objMappingsData.Series_Big_Id__c = objRecord.sSeriesBigId;
                        if (objRecord.s1stEpisodeASIN != null) objMappingsData.X1st_Episode_ASIN__c = objRecord.s1stEpisodeASIN;
                        if (objRecord.sSeasonASIN != null) objMappingsData.Season_ASIN__c = objRecord.sSeasonASIN;
                        if (objRecord.sEpisodeNumber != null) objMappingsData.Episode_Number__c = objRecord.sEpisodeNumber;
                        lstRecordsToUpsert.add(objMappingsData);
                    }
                    //When it is a new record
                    else {
                        System.debug('INSERT RECORDS : ' + objRecord);
                        System.debug('ID FOR TITLE MAPPING NEW RECORD : ' + sTitleId);
                        HEP_MDP_Product_Mapping__c objMappingsData = new HEP_MDP_Product_Mapping__c();
                        if (objRecord.sChannel != null) objMappingsData.Channel__c = objRecord.sChannel;
                        if (objRecord.sFormat != null && objRecord.sFormat == 'formatK') objMappingsData.Format__c = '4K';
                        if (objRecord.sFormat != null && objRecord.sFormat != 'formatK') objMappingsData.Format__c = objRecord.sFormat;
                        if (objRecord.sASIN != null) objMappingsData.ASIN__c = objRecord.sASIN;
                        if (objRecord.sLocalDescription != null) objMappingsData.Local_Description__c = objRecord.sLocalDescription;
                        if (objRecord.sADAMId != null) objMappingsData.Adam_Id__c = objRecord.sADAMId;
                        if (objRecord.sBigId != null) objMappingsData.Big_Id__c = objRecord.sBigId;
                        if (objRecord.sComponentId != null) objMappingsData.Component_Id__c = objRecord.sComponentId;
                        if (objRecord.sEidr != null) objMappingsData.Eidr__c = objRecord.sEidr;
                        if (objRecord.sEtag != null) objMappingsData.ETAG__c = objRecord.sEtag;
                        if (objRecord.sMediaId != null) objMappingsData.Media_Id__c = objRecord.sMediaId;
                        if (objRecord.sName != null) objMappingsData.Name__c = objRecord.sName;
                        if (IdRegion != null) objMappingsData.HEP_Territory__c = IdRegion;
                        if (IdCustomer != null) objMappingsData.HEP_Customer__c = IdCustomer;
                        if (sTitleId != null) objMappingsData.Title__c = sTitleId;
                        if (objRecord.bIsDeleted == true) {
                            objMappingsData.Record_Status__c = sDELETED; 
                        } else {
                            objMappingsData.Record_Status__c = sACTIVE;
                        }
                        if (objRecord.sVideoId != null) objMappingsData.Google_Id__c = objRecord.sVideoId;
                        if (objRecord.sSeasonNumber != null) objMappingsData.Season_Number__c = objRecord.sSeasonNumber;
                        if (objRecord.sSeriesTitle != null) objMappingsData.Series_Title__c = objRecord.sSeriesTitle;
                        if (objRecord.sSeriesId != null) objMappingsData.Series_Id__c = objRecord.sSeriesId;
                        if (objRecord.sSeriesMediaId != null) objMappingsData.Series_Media_Id__c = objRecord.sSeriesMediaId;
                        if (objRecord.sSeriesBigId != null) objMappingsData.Series_Big_Id__c = objRecord.sSeriesBigId;
                        if (objRecord.s1stEpisodeASIN != null) objMappingsData.X1st_Episode_ASIN__c = objRecord.s1stEpisodeASIN;
                        if (objRecord.sSeasonASIN != null) objMappingsData.Season_ASIN__c = objRecord.sSeasonASIN;
                        if (objRecord.sEpisodeNumber != null) objMappingsData.Episode_Number__c = objRecord.sEpisodeNumber;
                        lstRecordsToUpsert.add(objMappingsData);
                    }
                
                }
            System.debug('DATA TO UPSERT');
                //Update / Inert the data to Salesforce
                upsert lstRecordsToUpsert;
                
            }
        }
        }
        /**
         *ProductMappingWrapper - Outer Wrapper that holds list of records to display + other data common to records
         *@author : Ashutosh Arora
        */
        public class ProductMappingWrapper {
            public List < ProductDataWrapper > lstProductData;
            public List < String > lstCustomers;
            public List < String > lstRegions;
            public String sMappingType;
            public string sSelectedRegion;
            public string sSelectedRegionId;
            public string sSelectedCustomer;
            public string sIdTitle;
            public map <String,String> mapTerritoryAccess;
            public map<String,String> parentTerritoryAccess;
            public ProductMappingWrapper() {
                this.lstProductData = new List < ProductDataWrapper > ();
                this.lstCustomers = new List < String > ();
                this.lstRegions = new List < String > ();
            }
        }
        
        /**
         *ProductDataWrapper - Wrapper class for storing individual records
         *@author : Ashutosh Arora
        */
        public class ProductDataWrapper {
            public string sRecordId;
            public string sChannel;
            public string sFormat;
            public string sName;
            public string sASIN;
            public string sADAMId;
            public string sComponentId;
            public string sMediaId;
            public string sCustomerId;
            public string sBigId;
            public string sEidr;
            public string sEtag;
            public string sVideoId;
            public string sSeasonNumber;
            public string sSeriesTitle;
            public string sSeriesId;
            public string sSeriesMediaId;
            public string sSeriesBigId;
            public string s1stEpisodeASIN;
            public string sSeasonASIN;
            public string sEpisodeNumber;
            public string sLocalDescription;
            public map < String, LastModifiedField > mapFieldHistory;
            public boolean bIsDeleted;
            public boolean bIdADAMUnique;
            public boolean bLocalDescUnique;
            public boolean bUniqueVideoId;
            public boolean bMediaIdUnique;
            public boolean bBigIdUnique;
            public boolean bUniqueASIN;
            public boolean bUnique1stEpASIN;
            public boolean bUniqueSeasonASIN;

            public ProductDataWrapper() {
                mapFieldHistory = new map < String, LastModifiedField > ();
            }
        }
        /**
         *LastModifiedField - Wrapper class for storing Field History Data
         *@author : Ashutosh Arora
         */
        public class LastModifiedField {
            public date dtLastModifiedDate;
            public string sLastModifiedBy;
            public string sFieldName;
            public string sRecordId;
            public string sLastModifiedDate;
            public string sLastModifiedData;
        }

    }