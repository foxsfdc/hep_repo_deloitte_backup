/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_Production_Company_Interface_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', true);
        HEP_Territory__c objLocalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Territory__c objAutoTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '183', 'JDE', true);
        objAutoTerritory.Auto_Catalog_Creation__c = true;
        update objAutoTerritory;

        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Glocal Catalog', 'Single', null, string.valueof(objGlobalTerritory.id), 'Pending', 'Request', true);
        HEP_Catalog__c objLocalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Local Catalog', 'Single', string.valueof(objGlobalCatalog.id), string.valueof(objLocalTerritory.id), 'Pending', 'Request', true);

        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;

        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        objTitle.FOX_ID__c ='4897679';
        insert objTitle;

        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Production_Company_Interface','HEP_Production_Company_Interface',true,10,10,'Inbound',true,true,true);

    }                                                   

    public static testMethod void test() {

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Catalog__c> lstCatalogs = [SELECT Id, Catalog_Name__c FROM HEP_Catalog__c];
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        HEP_Production_Company_Interface.ProductionWrapper objProductionWrapper = new HEP_Production_Company_Interface.ProductionWrapper();
        objProductionWrapper.Data.Payload.Change.foxId = '4897679';
        objTxnResponse.sRequest = JSON.serialize(objProductionWrapper);

        System.runAs(u) {
            Test.startTest();
                HEP_Production_Company_Interface objPromotionCompany = new HEP_Production_Company_Interface();
                Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
                objPromotionCompany.performTransaction(objTxnResponse);
            Test.stoptest();
        }
    }
}