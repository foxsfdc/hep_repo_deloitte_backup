@isTest
private class HEP_Add_Existing_Catalog_Controller_Test {
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        // User Created
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Jain', 'anshuljain787@deloitte.com', 'AJ', 'AJ', 'AJ', '', false);
        insert objUser;

        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        insert objTerritory;

        HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia1', 'EMEA', 'Subsidiary', null, objTerritory.Id, 'ARS', 'AUS1', 'AUS1', '', false);
        insert objTerritory1;


        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion', 'Global', '', objTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        objPromotion.LocalPromotionCode__c = '123456789';
        insert objPromotion;


        HEP_Promotion__c objPromotion1 = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion1', 'Global', '', objTerritory1.Id, objHEPLineOfBusiness.Id, '', '', false);
        insert objPromotion1;


        HEP_Catalog__c objCatalog1 = HEP_Test_Data_Setup_Utility.createCatalog('52560', 'ARTHUR', 'Single', null, objTerritory.Id, 'Approved', 'Master', false);
        insert objCatalog1;

        HEP_Catalog__c objCatalog2 = HEP_Test_Data_Setup_Utility.createCatalog('52460', 'ARTHUR1', 'Single', null, objTerritory.Id, 'Approved', 'Master', false);
        insert objCatalog2;

    }

    // Method to test searchCatalog method where search value is a single value
    @isTest
    static void testSearchCatalog1() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        LIST < HEP_Utility.PicklistWrapper > lstSearchCatalog = HEP_Add_Existing_Catalog_Controller.searchCatalog('525', lstTerritory[0].Id);
        System.assertEquals(1, lstSearchCatalog.size());
        test.stopTest();
    }

    // Method to test searchCatalog method where search value are multiple values comma separated
    @isTest
    static void testSearchCatalog2() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        LIST < HEP_Utility.PicklistWrapper > lstSearchCatalog = HEP_Add_Existing_Catalog_Controller.searchCatalog('52560,52460', lstTerritory[0].Id);
        System.assertEquals(2, lstSearchCatalog.size());
        test.stopTest();
    }

    // Method to test searchCatalog method where search results are null
    @isTest
    static void testSearchCatalog3() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        LIST < HEP_Utility.PicklistWrapper > lstSearchCatalog = HEP_Add_Existing_Catalog_Controller.searchCatalog('5250', lstTerritory[0].Id);
        System.assertEquals(null, null);
        test.stopTest();
    }

    // Method to test searchCatalog method to catch Exception Block
    @isTest
    static void testSearchCatalog4() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        try {
            LIST < HEP_Utility.PicklistWrapper > lstSearchCatalog = HEP_Add_Existing_Catalog_Controller.searchCatalog('5250', null);
        } catch (Exception ex) {
            System.assertEquals(true, ex.getMessage().containsIgnoreCase('Invalid id'));
            test.stopTest();
        }
    }

// Method to test getCatalogData method where search value Catalog Number and Name
    @isTest
    static void testGetCatalogData1() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        HEP_Add_Existing_Catalog_Controller.CatalogResults objGetCatalog = HEP_Add_Existing_Catalog_Controller.getCatalogData('52560/--/Aurthor', lstTerritory[0].Id);
        System.assertNotEquals(null,objGetCatalog);
        test.stopTest();
    }

// Method to test getCatalogData method where search value are multiple values
    @isTest
    static void testGetCatalogData2() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        HEP_Add_Existing_Catalog_Controller.CatalogResults objGetCatalog = HEP_Add_Existing_Catalog_Controller.getCatalogData('52560,ARTHUR,535,524', lstTerritory[0].Id);
        System.assertNotEquals(null,objGetCatalog);
        test.stopTest();
    }

// Method to test getCatalogData method where search value are Catalog Number
    @isTest
    static void testGetCatalogData3() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        HEP_Add_Existing_Catalog_Controller.CatalogResults objGetCatalog = HEP_Add_Existing_Catalog_Controller.getCatalogData('52560', lstTerritory[0].Id);
        System.assertNotEquals(null,objGetCatalog);
        test.stopTest();
    }

// Method to test getCatalogData method to catch Exception
    @isTest
    static void testGetCatalogData4() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        test.startTest();
        try {
            HEP_Add_Existing_Catalog_Controller.CatalogResults objGetCatalog = HEP_Add_Existing_Catalog_Controller.getCatalogData('52560', null);
        } catch (Exception ex) {
            System.assertEquals(true, ex.getMessage().containsIgnoreCase('Invalid id'));
            test.stopTest();
        }
    }

// Method to test saveCatalogData method When promotion Catalog Already Exists
    @isTest
    static void testSaveCatalogData1() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        List < HEP_Promotion__c > lstPromotion = [Select Id, LocalPromotionCode__c FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_Catalog__c > lstCatalog = [SELECT Id, Unique_Id__c FROM HEP_Catalog__c WHERE CatalogId__c = '52460' OR CatalogId__c = '52560' ORDER BY CatalogId__c  ];
        HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
        objPromotionCatalog.Promotion__c = lstPromotion[0].Id;
        objPromotionCatalog.Catalog__c = lstCatalog[0].Id;
        objPromotionCatalog.Unique_Id__c = lstPromotion[0].LocalPromotionCode__c + ' / ' + lstCatalog[0].Unique_Id__c;
        objPromotionCatalog.Record_Status__c = 'Active';
        insert objPromotionCatalog;
        String sCatalog = '[{"sCatalog":"52560","sCatalogId":"'+lstCatalog[1].Id+'","sCatalogName":"ARTHUR (2011) (COMPETITIVE)","sRegion":"Australia","sType":"Single","sUniqueId":"52560 / Australia","sWPRId":"119370"},{"sCatalog":"52460","sCatalogId":"' + lstCatalog[0].Id + '","sCatalogName":"ARTHUR","sRegion":"Australia","sType":"Single","sUniqueId":"52460 / Australia","sWPRId":"119370"}]';
        test.startTest();


        List < HEP_Add_Existing_Catalog_Controller.CatalogWrapper > lstSaveCatalog = HEP_Add_Existing_Catalog_Controller.saveCatalogData(sCatalog, lstPromotion[0].Id, lstTerritory[0].Id);
        System.assertEquals(1,lstSaveCatalog.size());
        test.stopTest();
    }


// Method to test saveCatalogData method When promotion Catalog Already Exists
    @isTest
    static void testSaveCatalogData3() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        List < HEP_Promotion__c > lstPromotion = [Select Id, LocalPromotionCode__c FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_Catalog__c > lstCatalog = [SELECT Id, Unique_Id__c FROM HEP_Catalog__c WHERE CatalogId__c = '52460' OR CatalogId__c = '52560' ORDER BY CatalogId__c  ];
        HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
        objPromotionCatalog.Promotion__c = lstPromotion[0].Id;
        objPromotionCatalog.Catalog__c = lstCatalog[0].Id;
        objPromotionCatalog.Unique_Id__c = lstPromotion[0].LocalPromotionCode__c + ' / ' + lstCatalog[0].Unique_Id__c;
        objPromotionCatalog.Record_Status__c = 'Active';
        insert objPromotionCatalog;
        objPromotionCatalog.Record_Status__c = 'Deleted';
        System.debug('checking Trigger after Insert'+HEP_Promotion_Catalog_TriggerHandler.run);
        // HEP_Promotion_Catalog_TriggerHandler.run = true;
         HEP_Promotion_Catalog_TriggerHandler.runOnce();
         HEP_Promotion_Catalog_TriggerHandler.runOnce();
         HEP_CheckRecursive.clearSetIds();
         
        System.debug('checking Trigger Before Update'+HEP_Promotion_Catalog_TriggerHandler.run);
        update objPromotionCatalog;
        System.debug('checking Trigger after Update'+HEP_Promotion_Catalog_TriggerHandler.run);
        String sCatalog = '[{"sCatalog":"52560","sCatalogId":"'+lstCatalog[1].Id+'","sCatalogName":"ARTHUR (2011) (COMPETITIVE)","sRegion":"Australia","sType":"Single","sUniqueId":"52560 / Australia","sWPRId":"119370"},{"sCatalog":"52460","sCatalogId":"' + lstCatalog[0].Id + '","sCatalogName":"ARTHUR","sRegion":"Australia","sType":"Single","sUniqueId":"52460 / Australia","sWPRId":"119370"}]';
        test.startTest();


        List < HEP_Add_Existing_Catalog_Controller.CatalogWrapper > lstSaveCatalog = HEP_Add_Existing_Catalog_Controller.saveCatalogData(sCatalog, lstPromotion[0].Id, lstTerritory[0].Id);
        System.assertEquals(0,lstSaveCatalog.size());
        test.stopTest();
    }

// Method to test saveCatalogData method to catch Exception
    @isTest
    static void testSaveCatalogData2() {
        List < HEP_Territory__c > lstTerritory = [Select Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        List < HEP_Promotion__c > lstPromotion = [Select Id, LocalPromotionCode__c FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion1'];
        List < HEP_Catalog__c > lstCatalog = [SELECT Id, Unique_Id__c FROM HEP_Catalog__c WHERE CatalogId__c = '52460'];
        HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
        objPromotionCatalog.Promotion__c = lstPromotion[0].Id;
        objPromotionCatalog.Catalog__c = lstCatalog[0].Id;
        objPromotionCatalog.Unique_Id__c = lstPromotion[0].LocalPromotionCode__c + ' / ' + lstCatalog[0].Unique_Id__c;
        objPromotionCatalog.Record_Status__c = 'Active';
        insert objPromotionCatalog;
        String sCatalog = '[{"sCatalog":"52560","sCatalogId":12345,"sCatalogName":"ARTHUR (2011) (COMPETITIVE)","sRegion":"Australia","sType":"Single","sUniqueId":"52560 / Australia","sWPRId":"119370"},{"sCatalog":"52460","sCatalogId":"' + lstCatalog[0].Id + '","sCatalogName":"ARTHUR","sRegion":"Australia","sType":"Single","sUniqueId":"52460 / Australia","sWPRId":"119370"}]';
        test.startTest();
        try {
            List < HEP_Add_Existing_Catalog_Controller.CatalogWrapper > lstSaveCatalog = HEP_Add_Existing_Catalog_Controller.saveCatalogData(sCatalog, lstPromotion[0].Id, null);
        } catch (Exception ex) {
            System.assertEquals(true, ex.getMessage().containsIgnoreCase('Invalid id'));
            test.stopTest();

        }

    }

// Test checkPermissions Method when user doesn't have access to Territory
    @isTest
    static void testCheckPermissions1() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion1'];
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Add_Existing_Catalog_Controller objCatalog = new HEP_Add_Existing_Catalog_Controller();
            PageReference objPageReference = objCatalog.checkPermissions();
            System.assertEquals('/apex/HEP_AccessDenied', objPageReference.getUrl());
            test.stopTest();
        }
    }

// Test checkPermissions Method when user have access to Territory
    @isTest
    static void testCheckPermissions2() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_Territory__c > lstTerritory = [SELECT Id FROM HEP_Territory__c WHERE Name = 'Australia'];
        List < HEP_Role__c > lstRole = [SELECT Id FROM HEP_Role__c WHERE Name = 'Brand Manager'];
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id, lstRole[0].Id, lstUser[0].Id, false);
        objUserRole.Record_Status__c = 'Active';
        insert objUserRole;
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Add_Existing_Catalog_Controller objCatalog = new HEP_Add_Existing_Catalog_Controller();
            PageReference objPageReference = objCatalog.checkPermissions();
            System.assertEquals(null, null);
            test.stopTest();
        }
    }

    @isTest
    static void testClassesObject1() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];

        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Add_Existing_Catalog_Controller objCatalog = new HEP_Add_Existing_Catalog_Controller();
            test.stopTest();
        }
    }

    @isTest
    static void testClassesObject2() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion1'];

        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Add_Existing_Catalog_Controller objCatalog = new HEP_Add_Existing_Catalog_Controller();
            test.stopTest();
        }
    }
}