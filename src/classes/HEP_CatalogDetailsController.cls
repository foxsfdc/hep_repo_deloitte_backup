/**
 * HEP_CatalogDetailsController --- Class for Catalog Master page
 * @author  Nidhin VK
 */
public with sharing class HEP_CatalogDetailsController {
    
    public map<String, String> mapPageAccess{get;set;}
    public Boolean bHasPageAccess{get;set;}
    public String sHeaderTabs{get;set;}
    public String sCatalogId {get;set;}
    public String sBinaryData {get;set;}
    public String sFileName {get;set;}
    public String sDocumentDescription {get;set;}
    public String sFileType {get;set;}
    public String sCurrencyFormat {get;set;}
    public String sTerritoryId;
    public static String sCATALOG;
    public static String sCATALOG_PAGE;
    public static String sACTIVE;
    public static String sCANCELLED;
    static{
        sCATALOG = HEP_Constants__c.getValues('HEP_CATALOG').Value__c;
        sCATALOG_PAGE = HEP_Constants__c.getValues('HEP_CATALOG_DETAILS').Value__c;
        sACTIVE = HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c;
        sCANCELLED = HEP_Constants__c.getValues('HEP_RECORD_CANCELLED').Value__c;
    }
    
    /**
    * Class constructor to initialize the class variables
    * @return nothing
    * @author Sachin Agarwal
    */ 
    String sRecordActive = HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c;
    public HEP_CatalogDetailsController (){
      
      bHasPageAccess = false;
        Integer iTabCount;
      list<HEP_Catalog__c> lstCatalogs = [SELECT Id, Territory__c, Territory__r.CurrencyCode__c, Title_EDM__c, Catalog_Type__c
                            FROM HEP_Catalog__c 
                            WHERE Id = :ApexPages.CurrentPage().getParameters().get('catId') AND Record_Status__c =: sRecordActive];
        // If a matching catalog is found 
      if(lstCatalogs != null && !lstCatalogs.isEmpty()){
        
            sTerritoryId = lstCatalogs[0].Territory__c;
            // Getting the currency format applicable for the territory
            sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(lstCatalogs[0].Territory__r.CurrencyCode__c);
                      
          // Calling the security framework to get the list of tabs to be shown on the UI 
          list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
          mapPageAccess = HEP_Security_Framework.getTerritoryAccess(sTerritoryId, sCATALOG_PAGE, sCATALOG, lstHeaderTabs);
          
            system.debug('mapPageAccess : ' + mapPageAccess);
            system.debug('lstHeaderTabs : ' + lstHeaderTabs);
            set<HEP_Security_Framework.Tab> setHeaderTabs = new set<HEP_Security_Framework.Tab>(lstHeaderTabs);
            //Conditional Tab Remove
            //1. No Title - No Rights Check Title_EDM__c   2. If Catalog type = single -- > No component tab
            if(lstCatalogs != null && !lstCatalogs.isEmpty() ){
                if(lstCatalogs[0].Title_EDM__c == null)
                    if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_CATALOG_RIGHTSCHECK')))
                        mapPageAccess.put(HEP_Utility.getConstantValue('HEP_CATALOG_RIGHTSCHECK'), 'FALSE');

                if(lstCatalogs[0].Catalog_Type__c == HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_SINGLE'))
                    if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_CATALOG_COMPONENTS')))
                        mapPageAccess.put(HEP_Utility.getConstantValue('HEP_CATALOG_COMPONENTS'), 'FALSE');
                //Adding condition to show Product Mapping page only if it is Bundle
                if(lstCatalogs[0].Catalog_Type__c != HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BNDL'))
                    if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_CATALOG_MAPPING')))
                        mapPageAccess.put(HEP_Utility.getConstantValue('HEP_CATALOG_MAPPING'), 'FALSE');                
                for(HEP_Security_Framework.Tab objTab : lstHeaderTabs){
                    if((objTab.sPageName == HEP_Utility.getConstantValue('HEP_CATALOG_RIGHTSCHECK') && (lstCatalogs[0].Title_EDM__c == null)))
                        setHeaderTabs.remove(objTab);

                    if(objTab.sPageName == HEP_Utility.getConstantValue('HEP_CATALOG_COMPONENTS') && (lstCatalogs[0].Catalog_Type__c == HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_SINGLE')))
                        setHeaderTabs.remove(objTab);
                    
                    if(objTab.sPageName == HEP_Utility.getConstantValue('HEP_CATALOG_MAPPING') && (lstCatalogs[0].Catalog_Type__c != HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BNDL')))
                        setHeaderTabs.remove(objTab);
                }   
            }
            lstHeaderTabs = new list<HEP_Security_Framework.Tab>(setHeaderTabs);
          // If the user has read access to the page, then only load the page otherwise redirect the user to access denied page.          
          if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) && mapPageAccess.get(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) == 'TRUE'){
            bHasPageAccess = true;
          }
          
            system.debug('mapPageAccess 2 : ' + mapPageAccess);
            system.debug('lstHeaderTabs 2 : ' + lstHeaderTabs);
          sHeaderTabs = JSON.serialize(lstHeaderTabs); 
      }       
    }
    
    
     /**
    * CheckPermissions --- To check if the current user have access to the page is in.
    * @author    Sachin Agarwal
    */
    public PageReference checkPermissions(){
        return new HEP_Security_Framework().redirectToAccessDenied(bHasPageAccess);
    }
    
    /**
    * Extracts all the data required on the header.
    * @param  sCatalogId id of the catalog of which details are to be loaded
    * @return an instance of header class containing all the data required for the header portion
    * @author Nidhin VK
    */
    @RemoteAction 
    public static Header getHeaderDetails(String sCatalogId, String sLocaleDateFormat){
        // Creating an instance of Header class
        Header objHeader = new Header();
        List<String> lstStatus = new List<String>{sACTIVE, sCANCELLED};
        try{
            List<HEP_Catalog__c> lstCatalog = new List<HEP_Catalog__c>();
            lstCatalog = [SELECT 
                                Id, Catalog_Name__c, Catalog_Type__c, Image_URL__c,
                                Territory__c, Territory__r.Name, Territory__r.Type__c,
                                UI_Catalog_Number__c, Production_Year__c,
                                Licensor__c, Licensor_Group__c, LICENSOR_TYPE__c, 
                                Title_EDM__r.FIN_PROD_ID__c, Title_EDM__r.Name
                            FROM 
                                HEP_Catalog__c
                            WHERE 
                                Id = :sCatalogId
                            AND
                                Record_Status__c IN :lstStatus];
            if(lstCatalog == NULL || lstCatalog.isEmpty())
                return objHeader;
            HEP_Catalog__c objCatalog = lstCatalog[0];
            objHeader.sCatalogId = objCatalog.Id;
            objHeader.sCatalogName = objCatalog.Catalog_Name__c;
            objHeader.sCatalogNumber = objCatalog.UI_Catalog_Number__c;
            objHeader.sCatalogType = objCatalog.Catalog_Type__c;
            if(objCatalog.Territory__r != NULL)
            objHeader.sTerritoryName = objCatalog.Territory__r.Name;
            if(objCatalog.Title_EDM__r != NULL){
                objHeader.sFinancialTitleId = objCatalog.Title_EDM__r.FIN_PROD_ID__c;
                objHeader.sFinancialTitleName = objCatalog.Title_EDM__r.Name;
            }
            objHeader.sProductionYear = objCatalog.Production_Year__c;
            /*objHeader.sGenre = '';
            for(HEP_Catalog_Genre__c objGenre : objCatalog.Genres__r){
                if(String.isNotBlank(objGenre.Name))
                    objHeader.sGenre += objGenre.Name + ', ';
            }
            objHeader.sGenre = objHeader.sGenre.removeEnd(', ');*/
            objHeader.sLicensor = objCatalog.Licensor__c;
            objHeader.sLicensorType = objCatalog.LICENSOR_TYPE__c;
            objHeader.sLicensorGroup = objCatalog.Licensor_Group__c;
            objHeader.bIsFavourite = false;
            if(String.isBlank(objCatalog.Image_URL__c))
                objHeader.sImageUrl = HEP_Utility.getPromoDefaultImageUrl();
            else
                objHeader.sImageUrl = objCatalog.Image_URL__c;
            return objHeader;
        } catch(Exception ex){
            HEP_Error_Log.genericException('Query Erros','Query Errors',ex,'HEP_CatalogDetailsController','getHeaderDetails',null,null); 
            //throw error
            System.debug('Exception on Class : HEP_CatalogDetailsController - getHeaderDetails, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            return NULL;
        }
    }

    
    /**
    * Its used to get the url of the latest promotion image uploaded by the user
    * @param sCatalogId id of the catalog of which the image url is to be extracted
    * @return the url of the promotion image
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static String getImageUrl(String sCatalogId){
        String sRecordActive = HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c;
        String sImageUrl = '';
        list<HEP_Catalog__c> lstCatalogs = new list<HEP_Catalog__c>();
        lstCatalogs = [SELECT Id
                         FROM HEP_Catalog__c
                         WHERE ID = :sCatalogId
                         AND Record_Status__c =: sRecordActive];
        // If a matching record is found
        if(!lstCatalogs.isEmpty()){               
            sImageUrl = '';//lstPromotions[0].Promotion_Image_URL__c;
            
            if(String.isEmpty(sImageUrl)){
                sImageUrl = HEP_Utility.getPromoDefaultImageUrl();
            }
        }
        return sImageUrl;
    }
    
    /**
    * Used to save the image uploaded by the user in backend
    * @return nothing
    * @author Sachin Agarwal
    */
    public void uploadFile(){     
        
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();     
        try{
            // Deleting all the content documents associated with the promotion
            list<ContentDocumentLink> lstContentDocLink = new list<ContentDocumentLink>(); 
            lstContentDocLink =  [SELECT ContentDocumentId 
                                  FROM ContentDocumentLink 
                                  WHERE LinkedEntityId = :sCatalogId];
                                  
            if(!lstContentDocLink.isEmpty()){ 
                list<ContentDocument> lstContentDocument = new list<ContentDocument>();
                for(ContentDocumentLink objContentDocLink : lstContentDocLink){
                    lstContentDocument.add(new ContentDocument(id = objContentDocLink.ContentDocumentId));
                }                
                delete lstContentDocument;
            }
            
            // Creating a content version
            ContentVersion objContentVersion = new ContentVersion();
            
            objContentVersion.VersionData = EncodingUtil.base64Decode(sBinaryData);
            objContentVersion.Title = sFileName;
            
            // FileType FileExtension  fields not writable
            objContentVersion.PathOnClient = sFileName;
            
            objContentVersion.Description = sDocumentDescription;            
            objContentVersion.Origin = 'H';
            insert objContentVersion;
            
            objContentVersion = [SELECT id, ContentDocumentId, ContentUrl 
                                 FROM ContentVersion 
                                 WHERE id = :objContentVersion.Id][0];
            
            objContentDocumentLink.ContentDocumentId = objContentVersion.ContentDocumentId;
            objContentDocumentLink.LinkedEntityId = sCatalogId;
            objContentDocumentLink.ShareType = 'V';
            insert objContentDocumentLink;
            
            // Updating the link to the new content document
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.id = sCatalogId;        
            objCatalog.Image_URL__c = '/sfc/servlet.shepherd/version/download/' + objContentVersion.Id;
            update objCatalog;            
        }
        // Catch the exception
        catch(DMLException dme){  
            HEP_Error_Log.genericException('DML Erros','DML Errors',dme,'HEP_CatalogDetailsController','uploadFile',null,null);          
            throw dme;
        }   
        finally{
            sBinaryData = null;
            sFileName = null;
            
            sDocumentDescription = null;
            sFileType = null;
        }  
    }
    
    /**
    * Header --- Wrapper class for holding the data for the entire header
    * @author    Nidhin VK
    */
    public class Header{
        public String sImageUrl, sVideoUrl, sCatalogId, sCatalogName, sCatalogNumber; 
        public String sCatalogType, sTerritoryName, sFinancialTitleId, sFinancialTitleName;
        public String sGenre, sProductionYear, sLicensor, sLicensorType, sLicensorGroup;
        public Boolean bIsFavourite;
    }
}