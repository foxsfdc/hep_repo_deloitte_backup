/**
* HEP_AssetWrapper -- Wrapper to store Asset details 
* @author    Lakshman Jinnuri
*/
public class HEP_AssetWrapper{
    public AssetResource asset_resource;
    public String calloutStatus;
    public String TxnId;
    public list<Errors> errors;

    /**
    * AssetResource -- Class to hold related AssetResource details 
    * @author    Lakshman Jinnuri
    */
    public class AssetResource {
        public Asset asset;
    }

    /**
    * Asset -- Class to hold related Asset details 
    * @author    Lakshman Jinnuri
    */
    public class Asset {
        public AccessControlDescriptor access_control_descriptor;
        public AssetContentInfo asset_content_info;
        public String asset_id; 
        public String asset_state;  
        public String asset_state_last_update_date; 
        public String asset_state_user_id;  
        public boolean checked_out;
        public boolean content_editable;
        public Integer content_size;    
        public String content_state;    
        public String content_state_last_update_date;   
        public String content_state_user_id;    
        public String content_state_user_name;  
        public String content_type; 
        public String creator_id;   
        public String date_imported;    
        public String date_last_updated;    
        public boolean deleted;
        public Integer import_job_id;   
        public String import_user_name; 
        public boolean latest_version;
        public Integer legacy_model_id; 
        public boolean locked;
        public MasterContentInfo master_content_info;
        public String metadata_model_id;    
        public String metadata_state_user_name; 
        public String mime_type;    
        public String name; 
        public String original_asset_id;    
        public RenditionContent rendition_content;
        public boolean subscribed_to;
        public String thumbnail_content_id; 
        public Integer version; 
    }

    /**
    * AccessControlDescriptor -- Class to hold related AccessControlDescriptor details 
    * @author    Lakshman Jinnuri
    */
    public class AccessControlDescriptor {
        public PermissionsMap permissions_map;
    }

    /**
    * PermissionsMap -- Class to hold related PermissionsMap details 
    * @author    Lakshman Jinnuri
    */
    public class PermissionsMap {
        public list<Entry> entry;
    }

    /**
    * Entry -- Class to hold related Entry details 
    * @author    Lakshman Jinnuri
    */
    public class Entry {
        public String key;  
        public boolean value;
    }

    /**
    * AssetContentInfo -- Class to hold related AssetContentInfo details 
    * @author    Lakshman Jinnuri
    */
    public class AssetContentInfo {
        public MasterContent master_content;
    }

    /**
    * MasterContent -- Class to hold related MasterContent details 
    * @author    Lakshman Jinnuri
    */
    public class MasterContent {
        public String content_checksum; 
        public ContentData content_data;
        public String content_kind; 
        public String content_manager_id;   
        public Integer content_size;    
        public String id;   
        public String mime_type;    
        public String name; 
        public String unit_of_size; 
        public String url;  
    }

    /**
    * ContentData -- Class to hold related ContentData details 
    * @author    Lakshman Jinnuri
    */
    public class ContentData {
        public String data_source;  
        public boolean temp_file;
    }

    /**
    * MasterContentInfo -- Class to hold related MasterContentInfo details 
    * @author    Lakshman Jinnuri
    */
    public class MasterContentInfo {
        public String content_checksum; 
        public ContentData content_data;
        public String content_kind; 
        public String content_manager_id;   
        public Integer content_size;    
        public String id;   
        public String mime_type;    
        public String name; 
        public String unit_of_size; 
        public String url;  
    }

    /**
    * RenditionContent -- Class to hold related RenditionContent details 
    * @author    Lakshman Jinnuri
    */
    public class RenditionContent {
        public PreviewContent preview_content;
        public ThumbnailContent thumbnail_content;
    }

    /**
    * PreviewContent -- Class to hold related PreviewContent details 
    * @author    Lakshman Jinnuri
    */
    public class PreviewContent {
        public ContentData content_data;
        public String content_kind; 
        public String content_manager_id;   
        public Integer content_size;    
        public String id;   
        public String mime_type;    
        public String name; 
        public String unit_of_size; 
        public String url;  
    }

    /**
    * ThumbnailContent -- Class to hold related ThumbnailContent details 
    * @author    Lakshman Jinnuri
    */
    public class ThumbnailContent {
        public ContentData content_data;
        public String content_kind; 
        public String content_manager_id;   
        public Integer content_size;    
        public String id;   
        public String mime_type;    
        public String name; 
        public String unit_of_size; 
        public String url;  
    }
    
    /**
    * Errors -- Class to hold related Error details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }

}