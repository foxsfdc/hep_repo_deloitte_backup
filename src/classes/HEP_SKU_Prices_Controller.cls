/**
* HEP_SKU_Prices_Controller --- Class to get the data for SKU Prices
* @author    Sachin Agarwal
*/

public class HEP_SKU_Prices_Controller {
    
    public static String sWritePermission;
    public static String sBooleanValueFalse;
    public static String sRegionToRateCard;
    public static String sActive;
    
    static {
        sWritePermission = HEP_Utility.getConstantValue('HEP_PERMISSION_WRITE');
        sBooleanValueFalse = HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_FALSE');
        sRegionToRateCard = HEP_Utility.getConstantValue('MDP_REGION_TO_RATECARD');
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * RegionWrapper --- Wrapper class for holding region id and region name
    * @author    Sachin Agarwal
    */
    public class RegionWrapper{
        String sRegionId;
        String sRegionName; 
    }
    
    
    /**
    * SKUPrice --- Wrapper class for holding SKU Price data for one row
    * @author    Sachin Agarwal
    */
    public class SKUPrice{
        public String sSKUPriceId;
        public String sRegion;
        public String sRegionId;        
        public Date dtEffectiveDate;
        public String sPriceGradeId;
        public String sPriceGradeValue;
        public decimal dDealerListPrice;
        public String sPromotionName;
        public String sPromoId;
        public Boolean bDeleted;
        public Boolean bIsDuplicate;
        
        /**
        * Class constructor to initialize the class variables
        * @return nothing
        * @author Sachin Agarwal
        */ 
        public SKUPrice(){
            bDeleted = false;
            bIsDuplicate = false;
        }
    }
    
    
    /**
    * PriceGradeWrapper --- Wrapper class for holding SKU Price data for one row
    * @author    Sachin Agarwal
    */
    public class PriceGradeWrapper{
        public String sRegionId;
        public String sRegion;
        public String sPriceGradeId;
        public String sPriceGrade;
        public decimal dDealerListPrice;
    }
    
    /**
    * SKUPriceDetails --- Wrapper class for holding SKU Price data
    * @author    Sachin Agarwal
    */
    public class SKUPriceDetails{
        public String sSKUId;
        public String sSKUPriceType;
        public String sSKUMasterUniqueId;
        public String sRegionId;
        public String sRegion;
        public Boolean bEditAllowed;
        public list<SKUPrice> lstSKUPrices;
        public list<PriceGradeWrapper> lstPriceGradeWrapper;
        public list<RegionWrapper> lstRegionWrapper;
            
        /**
        * Class constructor to initialize the class variables
        * @return nothing
        * @author Sachin Agarwal
        */ 
        public SKUPriceDetails(){
            bEditAllowed = true;
            lstSKUPrices = new list<SKUPrice>();
            lstPriceGradeWrapper = new list<PriceGradeWrapper>();
            lstRegionWrapper = new list<RegionWrapper>();
        }
    }
    
    
    /**
    * Extracts all the data required on the page.
    * @param  sSKUId id of the SKU of which details are to be loaded
    * @return an instance of SKUPriceDetails class containing all the data required for the SKU price
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static SKUPriceDetails getPriceDetails(String sSKUId){
        SKUPriceDetails objSKUPriceDetails = new SKUPriceDetails();
        
        // Querying the SKUs
        list<HEP_SKU_Master__c> lstSKUs = new list<HEP_SKU_Master__c>();
        lstSKUs = [SELECT id, Catalog_Master__c, Catalog_Master__r.Territory__c, Catalog_Master__r.Catalog_Type__c,
                          Type__c, Territory__c, Territory__r.Name, Unique_Id__c, Channel__c, Format__c, Catalog_Master__r.Product_Type__c,
                          Catalog_Master__r.Product_Sub_Type__c,
                          (SELECT id, Promotion_Catalog__r.Record_Status__c, Promotion_Catalog__r.Promotion__c,
                                  Promotion_Catalog__r.Promotion__r.PromotionName__c 
                           FROM Promotion_SKUs__r 
                           WHERE Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND
                           Promotion_Catalog__r.Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND
                           Promotion_Catalog__r.Catalog__r.Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND
                           Promotion_Catalog__r.Catalog__r.Type__c = :HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER')),
                           (SELECT ID, PriceGrade__c, PriceGrade__r.Dealer_List_Price__c, Start_Date__c, Region__c, Region__r.Name,
                                   Promotion_SKU__c, Promotion_SKU__r.Promotion_Catalog__r.Promotion__c, PriceGrade__r.PriceGrade_Calc__c,
                                   Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c, PriceGrade__r.PriceGrade__c 
                            FROM SKU_Prices__r
                            WHERE Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND Changed_Flag__c = true order by Start_Date__c desc)                           
                   FROM HEP_SKU_Master__c
                   WHERE ID = :sSKUId AND Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') limit 1];
        
        // Checking if there's a matching SKU. If so, get all the related SKU price records
        if(lstSKUs != null && !lstSKUs.isEmpty()){
            
            String sSKUPriceGradeType;
            set<String> setFilter = new set<String>();
            // Changes for MP-1667 : 07/31 : Sachin Agarwal
            if((!(lstSKUs[0].Channel__c != null && lstSKUs[0].Channel__c.containsIgnoreCase('EST') && lstSKUs[0].Channel__c.containsIgnoreCase('VOD'))) && lstSKUs[0].Format__c != 'SD' && lstSKUs[0].Format__c != 'HD' && 
                lstSKUs[0].Format__c != '4K'){
                    
                sSKUPriceGradeType = 'MASH';
            }
            // Changes for MP-1667 : 07/31 : Sachin Agarwal
            else if(lstSKUs[0].Channel__c != null && lstSKUs[0].Channel__c.containsIgnoreCase('EST') && (lstSKUs[0].Format__c == 'SD' || lstSKUs[0].Format__c == 'HD' || lstSKUs[0].Format__c != '4K')){
                sSKUPriceGradeType = 'MDP_EST';
            } 
            // Changes for MP-1667 : 07/31 : Sachin Agarwal
            else if(lstSKUs[0].Channel__c != null && lstSKUs[0].Channel__c.containsIgnoreCase('VOD') && (lstSKUs[0].Format__c == 'SD' || lstSKUs[0].Format__c == 'HD' || lstSKUs[0].Format__c == '4K')){
                        
                sSKUPriceGradeType = 'MDP_VOD_ESRP';
            }
            else if(lstSKUs[0].Catalog_Master__r.Catalog_Type__c == 'Bundle'){
                sSKUPriceGradeType = 'MDP_BUNDLES';
            }
            objSKUPriceDetails.sSKUPriceType = sSKUPriceGradeType;
            objSKUPriceDetails.lstRegionWrapper = getChildRegions(lstSKUs[0].Territory__c);
            System.debug('sSKUPriceGradeType ' + sSKUPriceGradeType);
            
            if(sSKUPriceGradeType == 'MASH'){
                
                for(RegionWrapper objRegionWrapper : objSKUPriceDetails.lstRegionWrapper){
                    setFilter.add(sSKUPriceGradeType + '/' + objRegionWrapper.sRegionName + '/' + lstSKUs[0].Format__c);
                }               
            }
            
            else if(sSKUPriceGradeType == 'MDP_VOD_ESRP'){
                
                for(RegionWrapper objRegionWrapper : objSKUPriceDetails.lstRegionWrapper){
                    setFilter.add(sSKUPriceGradeType + '/' + objRegionWrapper.sRegionName + '/' + lstSKUs[0].Format__c + '/');
                }               
            }
            else if(sSKUPriceGradeType == 'MDP_EST'){
                if(String.isnotBlank(lstSKUs[0].Catalog_Master__r.Product_Type__c) && String.isnotBlank(lstSKUs[0].Catalog_Master__r.Product_Sub_Type__c))
                    setFilter.add(sSKUPriceGradeType + '/' + lstSKUs[0].Catalog_Master__r.Product_Type__c + '/' + lstSKUs[0].Catalog_Master__r.Product_Sub_Type__c);
                else
                    setFilter.add(sSKUPriceGradeType + '/' +  '/' );
            }
            else if(sSKUPriceGradeType == 'MDP_BUNDLES'){
                for(RegionWrapper objRegionWrapper : objSKUPriceDetails.lstRegionWrapper){
                    setFilter.add(sSKUPriceGradeType + '/' + objRegionWrapper.sRegionName);
                } 
            }
            
            System.debug('setFilter ' + setFilter);
            objSKUPriceDetails.lstPriceGradeWrapper =  getPriceGradeDetails(objSKUPriceDetails.lstRegionWrapper, setFilter, sSKUPriceGradeType);

            objSKUPriceDetails.sSKUId = lstSKUs[0].id;
            objSKUPriceDetails.sSKUMasterUniqueId = lstSKUs[0].Unique_Id__c;
            objSKUPriceDetails.sRegion = lstSKUs[0].Territory__r.Name;
            objSKUPriceDetails.sRegionId = lstSKUs[0].Territory__c;
            
            map <String, String> mapCustomPermissions = HEP_Utility.fetchCustomPermissions(lstSKUs[0].Territory__c, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_SKU_PRICES'));
            
            // If the user doesn't have edit access on the territory, then he won't be able to edit
            if(mapCustomPermissions != null && mapCustomPermissions.containsKey(sWritePermission) && mapCustomPermissions.get(sWritePermission) == sBooleanValueFalse){
                objSKUPriceDetails.bEditAllowed = false;                
            }
            else{
                // If the territory is an International one other than DHE, then the editing is allowed
                if(lstSKUs[0].Territory__r.Name != HEP_Utility.getConstantValue('HEP_TERRITORY_DHE')){
                    objSKUPriceDetails.bEditAllowed = true;
                }
                else{
                    objSKUPriceDetails.bEditAllowed = false;
                }
            } 
                                  
            // Going through all the SKU price records
            for(HEP_SKU_Price__c objSFDCSKUPrice : lstSKUs[0].SKU_Prices__r){
                
                SKUPrice objSKUPrice = new SKUPrice();
                objSKUPrice.sSKUPriceId = objSFDCSKUPrice.id;
                objSKUPrice.sRegion = objSFDCSKUPrice.Region__r.Name;
                objSKUPrice.sRegionId = objSFDCSKUPrice.Region__c;
                objSKUPrice.dtEffectiveDate = objSFDCSKUPrice.Start_Date__c;
                objSKUPrice.sPriceGradeId = objSFDCSKUPrice.PriceGrade__c;
                objSKUPrice.sPriceGradeValue = objSFDCSKUPrice.PriceGrade__r.PriceGrade_Calc__c;
                objSKUPrice.dDealerListPrice = objSFDCSKUPrice.PriceGrade__r.Dealer_List_Price__c != null ? objSFDCSKUPrice.PriceGrade__r.Dealer_List_Price__c : null;
                objSKUPrice.sPromotionName = objSFDCSKUPrice.Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c;
                objSKUPrice.sPromoId = objSFDCSKUPrice.Promotion_SKU__r.Promotion_Catalog__r.Promotion__c;
                
                objSKUPriceDetails.lstSKUPrices.add(objSKUPrice);
            } 
        }
        System.debug('objSKUPriceDetails ' + objSKUPriceDetails);
        return objSKUPriceDetails;
    }
    
    
    /**
    * Stores the details of SKU Prices
    * @param  objSKUPriceDetails parameter representing SKU price details
    * @return nothing
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static void savePriceDetails(SKUPriceDetails objSKUPriceDetails){
        Boolean bEditAllowed = false;
        map <String, String> mapCustomPermissions = HEP_Utility.fetchCustomPermissions(objSKUPriceDetails.sRegionId, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_SKU_PRICES'));
        
        // If the user doesn't have edit access on the territory, then he won't be able to edit
        if(mapCustomPermissions != null && mapCustomPermissions.containsKey(sWritePermission) && mapCustomPermissions.get(sWritePermission) == sBooleanValueFalse){
            bEditAllowed = false;                
        }
        else{
            // If the territory is an International one other than DHE, then the editing is allowed
            if(objSKUPriceDetails.sRegion != HEP_Utility.getConstantValue('HEP_TERRITORY_DHE')){
                bEditAllowed = true;
            }
            else{
                bEditAllowed = false;
            }
        }
        
        // Save only if edit permission is assigned to the user otherwise not
        if(bEditAllowed){
            
            map<id, HEP_Territory__c> mapRegion = new map<id, HEP_Territory__c>([SELECT id, name
                                                                                 FROM HEP_Territory__c]); 
                                                                                 
            map <String, String> mapDeletedSKUPrice = new map<String, String>();                                        
            for(HEP_SKU_Price__c tempSKUPrice : [SELECT ID, Unique_Id__c 
                                         FROM HEP_SKU_Price__c
                                         WHERE SKU_Master__c = :objSKUPriceDetails.sSKUId AND Promotion_SKU__c = null
                                         and Record_Status__c != :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')]){
                 
                 mapDeletedSKUPrice.put(tempSKUPrice.Unique_Id__c, tempSKUPrice.Id);
             }
                                              
              set<id> setSKUPriceToBeDeleted = new set<id>();
              list<HEP_SKU_Price__c> lstSKUPricesToUpdate = new list<HEP_SKU_Price__c>();  
              list<HEP_SKU_Price__c> lstSKUPricesToUpsert = new list<HEP_SKU_Price__c>();  
              set<String> setRegionIds = new set<String>();
              //set<String> setSKUPriceIds = new set<String>();
              // Going through all the records of SKU prices
              String sUniqueKey = '';
              
              for(SKUPrice objSKUPrice : objSKUPriceDetails.lstSKUPrices){
                  sUniqueKey = '';
                  // If a record is deleted which was earlier present in SFDC, delete it from SFDC
                  HEP_SKU_Price__c objSFDCSKUPrice = new HEP_SKU_Price__c();
                  
                  if(mapRegion.containsKey(objSKUPrice.sRegionId))
                        sUniqueKey += String.valueOf(mapRegion.get(objSKUPrice.sRegionId).name);
                    
                    sUniqueKey += ' / ';
                    sUniqueKey += objSKUPriceDetails.sSKUMasterUniqueId;
                    
                    sUniqueKey += ' /';
                    if(objSKUPrice.dtEffectiveDate != null){
                        sUniqueKey += ' ';
                        sUniqueKey += String.valueOf(objSKUPrice.dtEffectiveDate);
                    }
                    
                  if(String.isNotEmpty(objSKUPrice.sSKUPriceId))
                  {
                    
                    objSFDCSKUPrice.id = objSKUPrice.sSKUPriceId;
                    if( objSKUPrice.bDeleted){      
                      objSFDCSKUPrice.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
                    }
                    else{
                        if(mapDeletedSKUPrice.containsKey(sUniqueKey))
                        {
                            objSFDCSKUPrice.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
                            lstSKUPricesToUpdate.add(objSFDCSKUPrice);
                            objSFDCSKUPrice = new HEP_SKU_Price__c();
                            objSFDCSKUPrice.id = mapDeletedSKUPrice.get(sUniqueKey);
                        }
                        objSFDCSKUPrice.Start_Date__c = objSKUPrice.dtEffectiveDate;
                        objSFDCSKUPrice.PriceGrade__c = objSKUPrice.sPriceGradeId;
                        objSFDCSKUPrice.SKU_Master__c = objSKUPriceDetails.sSKUId;
                        objSFDCSKUPrice.Region__c = objSKUPrice.sRegionId;
                        objSFDCSKUPrice.Changed_Flag__c = true;
                        objSFDCSKUPrice.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
                    }
                    //setSKUPriceIds.add(objSKUPrice.sSKUPriceId);
                    lstSKUPricesToUpdate.add(objSFDCSKUPrice);
                  }
                  else{                
                    //objSFDCSKUPrice.id = null;
                    objSFDCSKUPrice.Start_Date__c = objSKUPrice.dtEffectiveDate;
                    objSFDCSKUPrice.PriceGrade__c = objSKUPrice.sPriceGradeId;
                    objSFDCSKUPrice.SKU_Master__c = objSKUPriceDetails.sSKUId;
                    objSFDCSKUPrice.Region__c = objSKUPrice.sRegionId;
                    objSFDCSKUPrice.Changed_Flag__c = true;
                    objSFDCSKUPrice.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
                    objSFDCSKUPrice.Unique_id__c  = sUniqueKey;
                    if(objSFDCSKUPrice.Region__c!=null && objSFDCSKUPrice.Start_Date__c !=null)
                    lstSKUPricesToUpsert.add(objSFDCSKUPrice);
                  }
              }
          
            // If there are any SKU price records which are to be upserted, upsert
            if(!lstSKUPricesToUpdate.isEmpty()){
                update lstSKUPricesToUpdate;
            }
            if(!lstSKUPricesToUpsert.isEmpty()){
                Schema.SObjectField externalID = HEP_SKU_Price__c.Fields.Unique_id__c;
                //insert maplistCopperFormatToUpsert.values();
                Database.UpsertResult[] results =  Database.upsert(lstSKUPricesToUpsert, externalID, true);
            }
        }           
    }
    
    
    /**
    * Gets the picklist values for the price grade
    * @param  sTerritoryId id of the territory
    * @param  sFormat format
    * @param  sChannel channel
    * @return a list of PriceGradeWrapper containing the price grade id and the price grade value
    * @author Sachin Agarwal
    */
    public static list<PriceGradeWrapper> getPriceGradeDetails(list<RegionWrapper> lstRegionWrapper, set<String> setFilter, String sSKUPriceGradeType){
        list<PriceGradeWrapper> lstPriceGradeWrapper = new list<PriceGradeWrapper>();               
        map<String, set<String>> mapRateCardRegions = new map<String, set<String>>();
        map<String, String> mapRegion = new map<String, String>();
         
        if(lstRegionWrapper != null && !lstRegionWrapper.isEmpty()){
            
            set<String> setRegionIds = new set<String>();
            for(RegionWrapper objRegionWrapper : lstRegionWrapper){
                mapRegion.put(objRegionWrapper.sRegionName, objRegionWrapper.sRegionId);
            }
            
            list<HEP_Price_Grade__c> lstPriceGrades = new list<HEP_Price_Grade__c>();
            System.debug('setFilter 1>>' + setFilter);
            
            if(sSKUPriceGradeType == 'MASH' || sSKUPriceGradeType == 'MDP_BUNDLES' || sSKUPriceGradeType == 'MDP_VOD_ESRP'){
                // Finding all the active price grade records associated with the given territory
                lstPriceGrades = [SELECT id, Record_Status__c, Price_Filter_SKU_Master__c, Dealer_List_Price__c, PriceGrade__c, PriceGrade_Calc__c, Territory__c,
                                         Territory__r.Name
                                  FROM HEP_Price_Grade__c
                                  WHERE Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND Type__c = :sSKUPriceGradeType
                                  AND Territory__c IN :mapRegion.values() AND Price_Filter_SKU_Master__c IN :setFilter order by Order__c]; 
            }
            else if(sSKUPriceGradeType == 'MDP_EST'){
                String sFilter = '';
                if(!setFilter.isEmpty()){
                    sFilter = new list<string>(setFilter)[0];
                }
                setFilter.clear();
                
                //setFilter.add(sSKUPriceGradeType + '/' + lstSKUs[0].Catalog_Master__r.Product_Type__c + '/' + lstSKUs[0].Catalog_Master__r.Product_Sub_Type__c);
                list<HEP_List_Of_Values__c> lstLOVs = new list<HEP_List_Of_Values__c>();
                 
                lstLOVs = [SELECT Id, Name, Parent_Value__c, Type__c, Values__c
                           FROM HEP_List_Of_Values__c 
                           WHERE Parent_Value__c IN :mapRegion.keyset() AND Type__c = :sRegionToRateCard AND Record_Status__c = :sActive ORDER BY Order__c];
                 
                set<String> setRateCardIds = new set<String>(); 
                for(HEP_List_Of_Values__c objLOV : lstLOVs){
                    if(!String.isEmpty(objLOV.Values__c)){
                        setRateCardIds.add(objLOV.Values__c);
                        
                        if(mapRateCardRegions.containsKey(objLOV.Values__c)){
                            mapRateCardRegions.get(objLOV.Values__c).add(objLOV.Parent_Value__c);
                        }
                        else{
                            mapRateCardRegions.put(objLOV.Values__c, new set<String>{objLOV.Parent_Value__c});
                        }
                    }
                }      
                System.debug('mapRateCardRegions>>' + mapRateCardRegions);
                for(String sRateCardId : setRateCardIds){
                    setFilter.add(sRateCardId + '/' + sFilter);
                }     
                System.debug('setFilter 2>>' + setFilter);
                // Finding all the active price grade records associated with the given territory
                lstPriceGrades = [SELECT id, Record_Status__c, Price_Filter_SKU_Master__c, Dealer_List_Price__c, PriceGrade__c, PriceGrade_Calc__c, Territory__c, MM_RateCard_ID__c,
                                         Territory__r.Name
                                  FROM HEP_Price_Grade__c
                                  WHERE Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND Type__c = :sSKUPriceGradeType
                                  AND ((Price_Filter_SKU_Master__c IN :setFilter) OR (MDP_Price_Filter__c IN :setFilter)) order by Order__c];
                System.debug('lstPriceGrades>>' + lstPriceGrades);
                 
            }
            Set<String> setRegionPriceGrades = new Set<String>();
            // Going through all the price grades and creating the wrapper
            for(HEP_Price_Grade__c objPriceGrade : lstPriceGrades){
                if(sSKUPriceGradeType == 'MASH' || sSKUPriceGradeType == 'MDP_BUNDLES' || sSKUPriceGradeType == 'MDP_VOD_ESRP'){
                    if(!setRegionPriceGrades.contains(objPriceGrade.Territory__r.Name + objPriceGrade.PriceGrade__c)){
	                    setRegionPriceGrades.add(objPriceGrade.Territory__r.Name + objPriceGrade.PriceGrade__c);
	                    lstPriceGradeWrapper.add(createPriceGradeWrapper(objPriceGrade));
                    }
                }       
                else if(sSKUPriceGradeType == 'MDP_EST'){
                    if(objPriceGrade.MM_RateCard_ID__c != null && mapRateCardRegions.containsKey(objPriceGrade.MM_RateCard_ID__c)){
                        
                        for(String sRegion : mapRateCardRegions.get(objPriceGrade.MM_RateCard_ID__c)){
                            if(!setRegionPriceGrades.contains(sRegion + objPriceGrade.PriceGrade__c)){
	                    		setRegionPriceGrades.add(sRegion + objPriceGrade.PriceGrade__c);
	                            PriceGradeWrapper objPriceGradeWrapper = createPriceGradeWrapper(objPriceGrade);
	                            
	                            objPriceGradeWrapper.sRegionId = NULL;
	                            objPriceGradeWrapper.sRegion = sRegion;
	                            
	                            if(mapRegion.containsKey(sRegion)){
	                                objPriceGradeWrapper.sRegionId = mapRegion.get(sRegion);
	                            } 
	                            
	                            lstPriceGradeWrapper.add(objPriceGradeWrapper);
                            }
                        }
                    }
                }         
            }
        }       
        
        // Returning the wrapper
        return lstPriceGradeWrapper;
    }
    
    
    /**
    * Creates the price grade wrapper
    * @param  objPriceGrade price grade record
    * @return price grade wrapper
    * @author Sachin Agarwal
    */
    public static PriceGradeWrapper createPriceGradeWrapper(HEP_Price_Grade__c objPriceGrade){
    	
    	// Creating the price grade wrapper
        PriceGradeWrapper objPriceGradeWrapper = new PriceGradeWrapper();
        
        objPriceGradeWrapper.sRegionId = objPriceGrade.Territory__c;
        objPriceGradeWrapper.sRegion = objPriceGrade.Territory__r.Name;
        objPriceGradeWrapper.sPriceGradeId = objPriceGrade.id;  
        objPriceGradeWrapper.sPriceGrade = objPriceGrade.PriceGrade_Calc__c; 
        objPriceGradeWrapper.dDealerListPrice = objPriceGrade.Dealer_List_Price__c; 
        
        return objPriceGradeWrapper;
    }
    
     /**
    * Gets the regions associated with the territory
    * @param  sTerritoryId id of the territory
    * @return a list of RegionWrapper containing the region id and the region name
    * @author Sachin Agarwal
    */
    public static list<RegionWrapper> getChildRegions(String sTerritoryId){
        
        list<RegionWrapper> lstRegionWrapper = new list<RegionWrapper>();
        
        // Querying all the child territories of the given territory
        list<HEP_Territory__c> lstTerritories = new list<HEP_Territory__c>();
        lstTerritories = [SELECT ID, Name, Parent_Territory__c, SKU_Territory__c
                          FROM HEP_Territory__c
                          WHERE Parent_Territory__c = :sTerritoryId OR ID = :sTerritoryId];
        
        HEP_Territory__c objSFDCTerritory;
        
        // Going through all the territories                  
        for(HEP_Territory__c objTerritory : lstTerritories){
            
            if(objTerritory.id == sTerritoryId){
                objSFDCTerritory = objTerritory;
            }
            else{
                RegionWrapper objRegionWrapper = new RegionWrapper();
                objRegionWrapper.sRegionId = objTerritory.id;
                objRegionWrapper.sRegionName = objTerritory.Name;
                
                lstRegionWrapper.add(objRegionWrapper);
            }               
        }
        // If there are no child regions or if the parent territory is a SKU territory, we need to include the parent territory
        if(objSFDCTerritory != null && (lstRegionWrapper.isEmpty() || (objSFDCTerritory.SKU_Territory__c))){
            RegionWrapper objRegionWrapper = new RegionWrapper();
            objRegionWrapper.sRegionId = objSFDCTerritory.id;
            objRegionWrapper.sRegionName = objSFDCTerritory.Name;
            
            lstRegionWrapper.add(objRegionWrapper);
        }
        
        // Returning the wrapper
        return lstRegionWrapper;
    }
}