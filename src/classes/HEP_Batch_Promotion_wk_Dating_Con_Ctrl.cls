/**
* HEP_Batch_Promotion_wk_Dating_Con_Ctrl-- class for the VF component to dynamically fetch records based on the Territory and Promotion Dating and display it in Email Template for weekly batch
* @author    Lakshman Jinnuri
*/
public class HEP_Batch_Promotion_wk_Dating_Con_Ctrl{  
    public Id recID {get; set;} 
    public String sDailyWeekly {get; set;}
    public List<EmailWrapper> lstEmailWrapper{
       get{
           if(lstEmailWrapper == null){
                fetchDetails();
            }
            return lstEmailWrapper;
        }
        set;
    }
    
    /**
    * fetchDetails --- Helps to get details of Promotion records for the promotion dating Id passed from VF component.
    * @param none
    * @exception Any exception
    * @return null
    */
    public void fetchDetails(){    
        Set<String> lstDigitalData = new Set<String>();
        List<HEP_Promotion_Dating__c> lstPrDating;
        Date dtPreviousDay;
        dtPreviousDay = System.today()-7;
        Date dtPresentDay = System.today();
        //Variables to hold the custom Settings data
        String sHEP_WEEK = HEP_Utility.getConstantValue('HEP_WEEK');
        String sHEP_DATETYPE_DIGITAL_PREORDER_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_DIGITAL_PREORDER_DATE');
        String sHEP_DATETYPE_PVOD_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_PVOD_RELEASE_DATE');
        String sHEP_DATETYPE_EST_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_EST_RELEASE_DATE');
        String sHEP_DATETYPE_VOD_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE');
        String sHEP_DATETYPE_END_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_END_DATE');
        String sHEP_DATETYPE_SPEST_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_SPEST_RELEASE_DATE');
        String sHEP_DATETYPE_PEST_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_PEST_RELEASE_DATE'); 
        String sHEP_DATETYPE_SPVOD_RELEASE_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_SPVOD_RELEASE_DATE');
        String sHEP_DATETYPE_EST_AVAIL_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_EST_AVAIL_DATE');
        String sHEP_DATETYPE_VOD_AVAIL_DATE = HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_AVAIL_DATE');
        String sHEP_STATUS_CONFIRMED = HEP_Utility.getConstantValue('HEP_STATUS_CONFIRMED'); 
        String sHEP_PROMOTION_TYPE_NATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        String sHEP_PROMOTION_TYPE_GLOBAL = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        String sHEP_TERRITORY_TYPE_LICENSEE = HEP_Utility.getConstantValue('HEP_TERRITORY_TYPE_LICENSEE'); 
        String sHEP_DATETIME_FORMAT = HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT');
        if(sHEP_WEEK != null && sHEP_DATETYPE_DIGITAL_PREORDER_DATE != null && sHEP_DATETYPE_PVOD_RELEASE_DATE != null && sHEP_DATETYPE_EST_RELEASE_DATE != null 
            && sHEP_DATETYPE_VOD_RELEASE_DATE != null && sHEP_DATETYPE_END_DATE != null && sHEP_DATETYPE_SPEST_RELEASE_DATE != null && sHEP_DATETYPE_PEST_RELEASE_DATE != null 
            && sHEP_DATETYPE_SPVOD_RELEASE_DATE != null && sHEP_DATETYPE_EST_AVAIL_DATE != null && sHEP_DATETYPE_VOD_AVAIL_DATE != null){
            sDailyWeekly = sHEP_WEEK;
            lstDigitalData.add(sHEP_DATETYPE_DIGITAL_PREORDER_DATE);
            lstDigitalData.add(sHEP_DATETYPE_PVOD_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_EST_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_VOD_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_END_DATE);
            lstDigitalData.add(sHEP_DATETYPE_SPEST_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_PEST_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_SPVOD_RELEASE_DATE);
            lstDigitalData.add(sHEP_DATETYPE_EST_AVAIL_DATE);
            lstDigitalData.add(sHEP_DATETYPE_VOD_AVAIL_DATE);
        }        

        //Helps to get the details of the Promotion dating records where status is confirmed,date type is Digital,Promotion type is national or promotion type is global and territory type is LIO,last modified date should be within week        
        if(sHEP_STATUS_CONFIRMED != null && sHEP_PROMOTION_TYPE_NATIONAL != null && sHEP_PROMOTION_TYPE_GLOBAL != null && sHEP_TERRITORY_TYPE_LICENSEE != null)
            lstPrDating = [SELECT Id,Status__c,Language__r.Name,Territory__r.Region__c,Territory__r.Type__c,Territory__r.Name,Promotion__r.LocalPromotionCode__c,Date__c,
                           Promotion__r.PromotionName__c,HEP_Promotions_Dating_Matrix__r.Date_Type__c,LastModifiedBy.Name, LastModifiedDate,Territory__c,HEP_Catalog__r.Name 
                           FROM HEP_Promotion_Dating__c 
                           WHERE  Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND Status__c =: sHEP_STATUS_CONFIRMED AND HEP_Promotions_Dating_Matrix__r.Date_Type__c in:lstDigitalData  
                           AND (LastModifiedDate >=: dtPreviousDay AND LastModifiedDate <: dtPresentDay) 
                           AND ((Territory__c =: recID AND Promotion__r.Promotion_Type__c =: sHEP_PROMOTION_TYPE_NATIONAL) OR
                           (Promotion__r.Promotion_Type__c =: sHEP_PROMOTION_TYPE_GLOBAL AND Territory__r.Type__c =: sHEP_TERRITORY_TYPE_LICENSEE))];
        if(Test.isRunningTest()){
            lstPrDating = [SELECT Id,Status__c,Language__r.Name,Territory__r.Region__c,Territory__r.Type__c,Territory__r.Name,Promotion__r.LocalPromotionCode__c,
                    Date__c,Promotion__r.PromotionName__c,HEP_Promotions_Dating_Matrix__r.Date_Type__c, LastModifiedBy.Name, LastModifiedDate, 
                    HEP_Catalog__r.Name FROM HEP_Promotion_Dating__c];
        }  
        this.lstEmailWrapper = new List<EmailWrapper>();   
        EmailWrapper objMail  ;
        Integer iCount = 0;

        //creates a wrapper based on the list retrieved and wont send email if the territory have no confirmed dates.
        DateTime dtDat;
        try{
            if(lstPrDating.isEmpty()){
                System.debug('in exception');
                throw new ListException();
            }else{                  
                for(HEP_Promotion_Dating__c objPrDating:lstPrDating){
                    if(objPrDating.Date__c != null)  
                        dtDat = (datetime)objPrDating.Date__c;
                    objMail = new EmailWrapper(); 
                    objMail.sPrCode = objPrDating.Promotion__r.LocalPromotionCode__c;
                    objMail.sPrName = objPrDating.Promotion__r.PromotionName__c;
                    objMail.sDname = objPrDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c;
                    if(sHEP_DATETIME_FORMAT != null){  
                        objMail.sNewDate = string.valueof(dtDat.format(sHEP_DATETIME_FORMAT)); 
                        objMail.sModificationDate = string.valueof(objPrDating.LastModifiedDate.format(sHEP_DATETIME_FORMAT)); 
                    }    
                    objMail.sStatus = objPrDating.Status__c;
                    objMail.sCatalog = objPrDating.HEP_Catalog__r.Name; 
                    if(sHEP_TERRITORY_TYPE_LICENSEE != null && objPrDating.Territory__r.Type__c.contains(sHEP_TERRITORY_TYPE_LICENSEE))
                        objMail.sRegion = objPrDating.Territory__r.Name; 
                    else    
                        objMail.sRegion = objPrDating.Territory__r.Region__c; 
                    objMail.sLanguages = objPrDating.Language__r.Name;
                    objMail.sModifiedBy = objPrDating.LastModifiedBy.Name;  
                    lstEmailWrapper.add(objMail); 
                    objMail.iIndex = Math.mod(iCount ,2);
                    iCount ++;
                }                    
            }
        }finally{}
    }
    
    /**
    * EmailWrapper --- Class to hold data related fields displayed on email template
    * @author    Lakshman Jinnuri
    */   
    class EmailWrapper{
        public String sPrCode {get; set;}
        public String sPrName {get; set;}
        public String sDname{get; set;}
        public String sNewDate {get; set;}      
        public String sStatus {get; set;}      
        public String sCatalog {get; set;}      
        public String sRegion {get; set;}      
        public String sLanguages {get; set;}
        public String sModifiedBy {get; set;}            
        public String sModificationDate {get; set;}
        public Integer iIndex{get;set;}          
    }

    /**
    * ListException --- Class to throw exception if the email contains no records to be sent
    * @author    Lakshman Jinnuri
    */  
    public class ListException extends Exception {}
}