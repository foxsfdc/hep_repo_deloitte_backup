/**
 * HEP_SKU_Lock_Email_Reminder --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_SKU_Lock_Email_Reminder
{
    //Promotion SKU record id is passed in this variable
    public Id promotionSKUId {get; set;}
    public Id promoId {get; set;}
     
    //This variable is used to render table on Email template
    public List<ApprovalEmailWrapper> objPromotionSKUWrap{ 
        get{
            if(objPromotionSKUWrap == null ){
                fetchPromotionSKUDetails();
            }
            return objPromotionSKUWrap;
        }
        set;
    }
    
    /**
    * fetchPromotionSKUDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author Gaurav Mehrishi
    */ 
    public void fetchPromotionSKUDetails(){
    
        List<HEP_Outbound_Email__c> lstOutBoundEmail = new List<HEP_Outbound_Email__c>([Select HEP_Promotion__c,
                                                        Record_Id__c,
                                                        Email_Sent__c,CreatedDate 
                                                    from HEP_Outbound_Email__c
                                                    Where Email_Sent__c = False
                                                        AND Email_Template_Name__c = 'HEP_SKU_Lock_Email_Reminder'
                                                        
                                                        AND HEP_Promotion__c =:promoId ]);
        Set<Id> promoSKId = new Set<Id>();
        for(HEP_Outbound_Email__c rec:lstOutBoundEmail)
        {
            promoSKId.add(rec.Record_Id__c);
        }
        try{
            //Instantiate the Wrapper
            objPromotionSKUWrap = new List<ApprovalEmailWrapper>();           
            if(!promoSKId.isEmpty()){
                //Query the Record based on Record Id               
                for(HEP_Promotion_SKU__c objPromotionSKU :[SELECT Id, Promotion_Catalog__r.Promotion__r.PromotionName__c,
                                 Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Name , Promotion_Catalog__r.Promotion__r.International_Marketing_Manager__r.Name,
                                 Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c, Locked_Date__c
                                 FROM HEP_Promotion_SKU__c
                                 WHERE Id IN : promoSKId])
                                 
                {

                    ApprovalEmailWrapper tempWrappper =new ApprovalEmailWrapper();
                    tempWrappper.sPromotionName = objPromotionSKU.Promotion_Catalog__r.Promotion__r.PromotionName__c;
                    DateTime dttFad = DateTime.newInstance(objPromotionSKU.Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c.year(), objPromotionSKU.Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c.month(), objPromotionSKU.Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c.day());
                    tempWrappper.sFAD = dttFad.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    tempWrappper.sDomesticMarketingManager = objPromotionSKU.Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Name;
                    tempWrappper.sInternationalMarketingManager = objPromotionSKU.Promotion_Catalog__r.Promotion__r.International_Marketing_Manager__r.Name;
                    DateTime dttLock = DateTime.newInstance(objPromotionSKU.Locked_Date__c.year(), objPromotionSKU.Locked_Date__c.month(), objPromotionSKU.Locked_Date__c.day());
                    tempWrappper.sLockedDate = dttLock.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    objPromotionSKUWrap.add(tempWrappper);
                }
            }
        }catch(Exception e){
            System.debug('Exception occurred because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
    }
    
    
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class ApprovalEmailWrapper{
        
        public String   sPromotionName {get; set;}
        public String   sLockedDate {get; set;}
        public String   sFAD   {get; set;}
        public String   sDomesticMarketingManager   {get; set;}
        public String   sInternationalMarketingManager     {get; set;}  
    }

}