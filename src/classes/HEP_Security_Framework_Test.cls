/**
* HEP_Security_Framework_Test --- Test class for HEP_Security_Framework
* @author    Sachin Agarwal
*/
@isTest 
public with sharing class HEP_Security_Framework_Test {
    @testSetup 
    public static void createData(){
    	// List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        
    	HEP_Territory__c objTerritory  = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', true);
    	HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
    	
    	HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.id, objRole.id, UserInfo.getUserId(), true);
    }
    
    @isTest 
    public static void testGetTerritoryAccess(){
    	list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
    	
    	list<HEP_Territory__c> lstTerritories = new list<HEP_Territory__c>();
    	lstTerritories = [SELECT Id
    					  FROM HEP_Territory__c
    					  WHERE name = 'Australia']; 
    	
    	if(lstTerritories != null && !lstTerritories.isEmpty()){
    		HEP_Security_Framework.getTerritoryAccess(lstTerritories[0].id, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_PROMOTION_DETAILS'), 'Global', lstHeaderTabs);
    		HEP_Security_Framework.getTerritoryAccess(lstTerritories[0].id, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_PROMOTION_DETAILS'), 'National', lstHeaderTabs);
    		HEP_Security_Framework.getMultipleTerritoryAccess();
    		new HEP_Security_Framework().redirectToAccessDenied(false);
    	}
    	
    }
}