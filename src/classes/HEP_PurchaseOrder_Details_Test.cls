/**
* HEP_PurchaseOrder_Details_Test -- Test class for the HEP_PurchaseOrder_Details for JDE And E1 Interfaces 
* @author    Lakshman Jinnuri
*/
@isTest
public class HEP_PurchaseOrder_Details_Test{
    /**
    * HEP_PurchaseOrder_Details_SuccessTest --  Test method to for Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_PurchaseOrder_Details_SuccessTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_PurchaseOrderDetails','HEP_PurchaseOrder_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objPOWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objPOWrapper.sPromoId = '27933';
        objPOWrapper.sTerritory = 'DHE';
        String sInvoiceJson = JSON.serialize(objPOWrapper);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_PurchaseOrderDetails');
        System.debug('objTxnResponse.sResponse ' + objTxnResponse.sResponse);//
        HEP_PurchaseOrderWrapper objPODetails = (HEP_purchaseOrderWrapper)JSON.deserialize(objTxnResponse.sResponse,HEP_purchaseOrderWrapper.class);
        System.debug('objPODetails ' + objPODetails);
        System.assertEquals('HEP_PurchaseOrderDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_PurchaseOrder_Details_FailureTest -- Test method for the failure Response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_PurchaseOrder_Details_FailureTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_PurchaseOrderWrapper objPODetails = new HEP_PurchaseOrderWrapper();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_PurchaseOrderDetails','HEP_PurchaseOrder_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objPOWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objPOWrapper.sPromoId = '279332';
        objPOWrapper.sTerritory = 'DHE';
        String sInvoiceJson = JSON.serialize(objPOWrapper);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_PurchaseOrderDetails');
        System.assertEquals('HEP_PurchaseOrderDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_PurchaseOrder_Details_InvalidAcessTokenTest -- Test method to get Purchase Order details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_PurchaseOrder_Details_InvalidAcessTokenTest(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_PurchaseOrderWrapper objPODetails = new HEP_PurchaseOrderWrapper();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='JDE_E1_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_PurchaseOrderDetails','HEP_PurchaseOrder_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_InvoiceWrapper.HEP_TerritoryDetails objPOWrapper = new HEP_InvoiceWrapper.HEP_TerritoryDetails();
        objPOWrapper.sPromoId = '279332';
        objPOWrapper.sTerritory = 'DHE';
        String sInvoiceJson = JSON.serialize(objPOWrapper);
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sInvoiceJson,'','HEP_PurchaseOrderDetails');
        System.assertEquals('HEP_PurchaseOrderDetails',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
}