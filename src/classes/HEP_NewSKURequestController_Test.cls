/**
 * HEP_NewSKURequestController_Test --- Test class for HEP_NewSKURequestController
 * @author  Nidhin V K
 */
@isTest
private class HEP_NewSKURequestController_Test {

    @testSetup static void setup() { 
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'NID', 'nvk@deloitte.com', 'NID', 'NID', 'NID', '', false);
        insert objUser;

        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;

        HEP_Role__c objRole1 = HEP_Test_Data_Setup_Utility.createHEPRole('CAN Marketing Manager', '', false);
        objRole1.Destination_User__c = objUser.Id;
        objRole1.Source_User__c = objUser.Id;
        insert objRole1;
        
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Acquisitions', 'TV', true);
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', NULL, NULL, 
                                        'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c objChildTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Canada', 'EMEA', 'Subsidiary', 
                                            objTerritory.Id, objTerritory.Id, 'EUR', 'BE', '49', NULL, true);
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objChildTerritory.Id, '12-11', 11, false);
        objPriceGrade.Channel__c = 'RENTAL';
        objPriceGrade.Format__c = 'DVD';
        objPriceGrade.Type__c = 'MASH';
        objPriceGrade.MM_RateCard_ID__c = '4';
        insert objPriceGrade;
        
        HEP_Price_Grade__c objPriceGrade2 = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(NULL, '13-12', 12, false);
        objPriceGrade2.Type__c = 'MDP_EST';
        objPriceGrade2.MM_RateCard_ID__c = '4';
        insert objPriceGrade2;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo', 'Global', NULL, 
                                        objTerritory.Id, objLOB.Id, NULL, NULL, true);
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, 
                                                    objChildTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objHEPPromotionsDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix(
                                                                        'TV', 'Promotion', 'Physical Release Date (Rental)', 
                                                                        objTerritory.Id, objChildTerritory.Id, true);
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id,
                                                    objHEPPromotionsDatingMatrix.Id, false);
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        insert objDatingRecord;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('01234', 'SAMPLE', 'Box Set', NULL, 
                                    objTerritory.Id, 'Approved', 'Master', false);
        objCatalog.Licensor__c = 'FOX';
        insert objCatalog;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, 
                                                    objPromotion.Id, NULL, true);
        HEP_Customer__c objHEPCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon', objTerritory.Id, 'CUSTOMER123', 
                                        'SKU Customers', NULL, true);
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, 'SKU123456', 'Sample SKU', 
                                        'Master', NULL, 'RENTAL', 'DVD', true);
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKU.Id, 
                                        'Approved', 'Unlocked', true);
        //HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objHEPCustomer.Id, objSKU.Id, true);
        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Catalog_Components','HEP_Catalog_Components_Extractor',
        								false,30,1,'Outbound-Pull', true, true, true);
      	HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
    }
    
    @isTest static void testBasicMethods() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'NID'];
    	System.runAs(lstUser[0]) {
	        HEP_Promotion__c objPromotion = [SELECT Id FROM HEP_Promotion__c LIMIT 1];
	        Test.setCurrentPageReference(new PageReference('Page.HEP_NewSKURequest')); 
	        System.currentPageReference().getParameters().put('promoId', objPromotion.Id);
	        Test.startTest();
	        HEP_NewSKURequestController objCTRL = new HEP_NewSKURequestController();
	        System.assertEquals(NULL, objCTRL.checkPermissions());
	        Test.stopTest();
    	}
    }
    
    @isTest static void test_searchSKUs() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'NID'];
    	System.runAs(lstUser[0]) {
	        HEP_Promotion__c objPromotion = [SELECT Id, Territory__c FROM HEP_Promotion__c LIMIT 1];
	        HEP_SKU_Master__c objSKU = [SELECT Id FROM HEP_SKU_Master__c LIMIT 1];
	        Test.startTest();
	        List<HEP_NewSKURequestController.SKURequest> lstSKURequest = HEP_NewSKURequestController.searchSKUs('SKU12',objPromotion.Territory__c);
	        System.assertEquals(objSKU.Id,lstSKURequest[0].sModelSKUId);
	        Test.stopTest();
    	}
    }
    
    @isTest static void test_searchCatalogs() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'NID'];
    	System.runAs(lstUser[0]) {
	        HEP_Catalog__c objCatalog = [SELECT Id, Territory__c FROM HEP_Catalog__c LIMIT 1];
	        Test.startTest();
	        List<HEP_NewSKURequestController.Catalog> lstCatalogs = HEP_NewSKURequestController.searchCatalogs('SAMPL',objCatalog.Territory__c, false);
	        System.assertEquals(objCatalog.Id,lstCatalogs[0].sCatalogId);
	        Test.stopTest();
    	}
    }
    
    @isTest static void test_getBoxsetComponents() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'NID'];
    	System.runAs(lstUser[0]) {
	        HEP_Catalog__c objCatalog = [SELECT Id, Territory__c FROM HEP_Catalog__c LIMIT 1];
	        Test.startTest();
        	Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
	        HEP_NewSKURequestController.CatalogComponent objCatalogComponent = HEP_NewSKURequestController.getBoxsetComponents('01234', 'RENTAL', objCatalog.Territory__c);
	        System.assertEquals(1, objCatalogComponent.lstBoxsetComponents.size());
	        Test.stopTest();
    	}
    }
    
    @isTest static void test_saveData() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'NID'];
    	System.runAs(lstUser[0]) {
	        HEP_Promotion__c objPromotion = [SELECT Id, Territory__c FROM HEP_Promotion__c LIMIT 1];
	        HEP_Catalog__c objCatalog = [SELECT Id FROM HEP_Catalog__c LIMIT 1];
	        HEP_Territory__c objTerritory = [SELECT Id FROM HEP_Territory__c WHERE Name =: 'DHE' LIMIT 1];
	        Test.startTest();
	        HEP_NewSKURequestController.SKURequest objSKURequest = HEP_NewSKURequestController.loadData(objPromotion.Id);
	        System.assertEquals(objPromotion.Id,objSKURequest.sPromotionId);
	        objSKURequest.sSKUName = 'Sample SKU';
	        objSKURequest.sChannel = 'RENTAL';
	        objSKURequest.sFormat = 'DVD';
	        objSKURequest.sCatalogId = objCatalog.Id;
	        objSKURequest.sPromoCatalogTerritoryId = objPromotion.Territory__c;
	        objSKURequest.bIsDigital = false;
	        objSKURequest.bIsSubmit = true;
	        objSKURequest.bIsDigital = false;
	        for(HEP_NewSKURequestController.Region objRegion : objSKURequest.lstRegions){
	            objRegion.sRecordId = NULL;
	            objRegion.iUnits = 0;
	        }
	        for(HEP_Utility.MultiPicklistWrapper objCustomer : objSKURequest.lstCustomers){
	            objCustomer.bSelected = true;
	        }
	        String sReturnURL = HEP_NewSKURequestController.saveData(JSON.serialize(objSKURequest));
	        System.assertEquals(HEP_NewSKURequestController.getReturnURL(objPromotion.Id),sReturnURL);
	        HEP_SKU_Master__c objSKU = [SELECT Id FROM HEP_SKU_Master__c LIMIT 1];
	        objSKU.INTL_ETL_SKU__c = true;
	        objSKU.INTL_Catalog_Number__c = '01234';
	        update objSKU;
	        HEP_NewSKURequestController.ErrorWrapper objError = new HEP_NewSKURequestController.ErrorWrapper('','');
	        HEP_NewSKURequestController.getValidUnitsCount(objPromotion.Id);
	        Test.stopTest();
    	}
    }
}