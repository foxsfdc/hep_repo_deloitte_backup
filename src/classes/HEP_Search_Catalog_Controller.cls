/**
 * HEP_Search_Catalog_Controller ---   Class to display  all the Calatog based on the search criteria.
 * @author    Roshi Rai
 */
Public class HEP_Search_Catalog_Controller {
    /**
     *  CatalogWrapper  Wrapper class to  hold the Catalog Data
     *  @author    Roshi Rai
     **/
    Public class CatalogWrapper {
        Public Integer iSizeoflist;
        list < RowData > lstRowData;
        List < String > lstKeyWords;
        List < String > lstCriteria;
        public CatalogWrapper() {
            lstRowData = new list < RowData > ();
            iSizeoflist = 0;
            lstKeyWords = new List < String > ();
            lstCriteria = new List < String > ();
        }
    }
    /**
     * RowData Class to hold the row data to be displayed on AG Grid
     * @author    Roshi Rai
     **/
    public class RowData {
        Public String sCatalogName;
        Public string sID;
        Public String sType;
        Public String sTerritory;
        Public String sRegion;
        Public String sLicensor;
        Public String sCatalogRecordID;
    }
    /* Primary method which will be called from the HEP_Search_Catalog page to retrieve the Catalog Record as per the parameter passed.
     * @param          sType--Lable or type on basis of which we will search Name,Type,id,keyword     
     * @param          sCriteria--starts With OR contains
     * @param          sSearchKeyWord--value entered for search
     * @return         CatalogWrapper
     */
    @RemoteAction
    public Static CatalogWrapper extractAllCatalog(String sType, String sCriteria, String sSearchKeyWord) {
        try {
            
            set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_CATALOGS'));
            String sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            CatalogWrapper objCatalogWrap = new CatalogWrapper();
            String keywords = HEP_Utility.getConstantValue('HEP_SEARCH_CATALOG_KEYWORDS');
            objCatalogWrap.lstKeyWords = keywords.split(',');
            String criteria = HEP_Utility.getConstantValue('SEARCH_CRITERIA_VALUES');
            objCatalogWrap.lstCriteria = criteria.split(',');
            list < HEP_Catalog__c > lstAllCatalog = new list < HEP_Catalog__c > ();
            system.debug('**** ' + setOfUserTerritory);
            String sCatalogQuery = 'SELECT id, Name,Catalog_Name__c, CatalogId__c, Catalog_Type__c,Licensor_Type__c,Territory__c, Territory__r.Name, Territory__r.Region__c,Licensor__c FROM HEP_Catalog__c Where  Record_Status__c=:sActive AND Territory__c In: setOfUserTerritory';
            if(sSearchKeyWord != null && sCriteria != null && String.isNotBlank(sSearchKeyWord) && String.isNotBlank(sCriteria) ){
            if (sType != null && sSearchKeyWord != '') {
                if (sType == HEP_Utility.getConstantValue('HEP_CATALOG_NAME')) {
                    sCatalogQuery += ' AND  Catalog_Name__c';
                } else if (sType == HEP_Utility.getConstantValue('HEP_CATALOG_ID')) {
                    sCatalogQuery += ' AND  CatalogId__c';
                } else if (sType == HEP_Utility.getConstantValue('HEP_TYPE')) {
                    sCatalogQuery += ' AND  Catalog_Type__c';
                } else if (sType == HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD')) {
                    sCatalogQuery += ' AND (Catalog_Name__c ';
                }
                if (sCriteria != null && sSearchKeyWord != null && HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD') != sType) {
                    if (sCriteria == HEP_Utility.getConstantValue('HEP_STARTS_WITH')) {
                        sCatalogQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\'';
                        //sQuery +=' LIKE \''+sSearchKey+'%\'';
                    } else if (sCriteria == HEP_Utility.getConstantValue('HEP_CONTAINS')) {
                        sCatalogQuery += ' LIKE \'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\'';
                        //sQuery +=' LIKE \'%'+sSearchKey+'%\'';
                    }
                }
                if (sCriteria != null && sSearchKeyWord != null && HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD') == sType) {
                    if (sCriteria == HEP_Utility.getConstantValue('HEP_STARTS_WITH')) {
                        //sCatalogQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR CatalogId__c LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                        sCatalogQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR CatalogId__c LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR Name LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                    } else if (sCriteria == HEP_Utility.getConstantValue('HEP_CONTAINS')) {
                        sCatalogQuery += ' LIKE \'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR CatalogId__c LIKE\'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR Name LIKE\'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                        //sQuery +=' LIKE \'%'+sSearchKey+'%\'';
                    }
                }
            }

        }
            else{
                
                sCatalogQuery += ' AND (Catalog_Name__c LIKE \''+String.escapeSingleQuotes(sSearchKeyWord)+'%\' OR CatalogId__c LIKE\''+String.escapeSingleQuotes(sSearchKeyWord)+'%\')';
             }
             
            sCatalogQuery += ' LIMIT 10001';
            system.debug('****lstAllCatalog' + sCatalogQuery);
            lstAllCatalog = Database.Query(sCatalogQuery);
            system.debug('****lstAllCatalog' + lstAllCatalog);
            //to get the bumber of record to be displayed on screen
            objCatalogWrap.iSizeoflist = lstAllCatalog.size();
            if (lstAllCatalog != NULL && !lstAllCatalog.isEmpty()) {
                for (HEP_Catalog__c objcatalog: lstAllCatalog) {
                    RowData objRowData = new RowData();
                    objRowData.sCatalogName = objcatalog.Catalog_Name__c;
                    objRowData.sID = objcatalog.CatalogId__c;
                    objRowData.sType = objcatalog.Catalog_Type__c;
                    objRowData.sTerritory = objcatalog.Territory__r.Name;
                    objRowData.sRegion = objcatalog.Territory__r.Region__c;
                    objRowData.sLicensor = objcatalog.Licensor_Type__c;
                    objRowData.sCatalogRecordID = objcatalog.id;
                    objCatalogWrap.lstRowData.add(objRowData);
                }
            }
            return objCatalogWrap;
        }
        Catch(Exception ex) {
            return NULL;
        }
    }
}