@isTest
public class HEP_Foxipedia_Talent_Mock implements HttpCalloutMock{
     public HTTPResponse  respond(HttpRequest request){
        HttpResponse response=new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{sStatus:Failure}');
        response.setStatusCode(400);
        return response; 
    }

}