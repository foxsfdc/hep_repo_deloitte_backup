@isTest (seeAllData = false)
public class HEP_Dating_Lock_Email_Reminder_Test {
    @testSetup
    public static void testSetup(){
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@deloitte.com', 'R', 'Vis', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Dating', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date', 'HEP_ProphetProjectedDateDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
    }
    @isTest
    static void testDatingLock(){
            User u = [SELECT Id from User where LastName = 'Vishnu'];
            System.runAs(u) {
            
            //Creating Territory
            HEP_Territory__c Germany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Bulgaria', 'EMEA', 'Licensee', null, null, 'EUR', 'DE', '182', null, true);
            
            
            //Create LOB
            HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
            
            //Creating a title ID
            EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
            EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Where\'d You Go Bernadette', '197429', 'PUBLC', prodType.id, true);
            
            
            //Creating Dating Matrix records
            HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', Germany.Id, Germany.Id, false);
            VODRelease.Projected_FAD_Logic__c = 'Monday';
            insert VODRelease;
            
            HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', null, Germany.id, FoxNewRelease.Id, null, null, false);
            testPromo.Title__c = Title.Id;
            String FADDate = '2018-07-12';
            testPromo.FirstAvailableDate__c = Date.valueOf(FADDate); 
            insert testPromo;
            
            HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Germany.Id, testPromo.Id, VODRelease.id, false);
            DatingRec1.Status__c = 'Projected';
            insert DatingRec1;
        
            Test.startTest();
            HEP_Dating_Lock_Email_Reminder lock1 = new HEP_Dating_Lock_Email_Reminder();
            lock1.prDatingId = DatingRec1.Id;
            lock1.fetchPrDatingDetails();
            Test.stopTest();
        }
    }

}