@isTest
public class HEP_InboundEmailHandler_Test {
	@testSetup
    static void createUsers()
    {
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'jaggi', 'test.testing@hep.dev.com','majaggi','mayank','majaggi','', true);
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
        objRoleOperations.Destination_User__c = u.Id;
        objRoleOperations.Source_User__c = u.Id;
        insert objRoleOperations;
        HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
        objRoleFADApprover.Destination_User__c = u.Id;
        objRoleFADApprover.Source_User__c = u.Id;
        insert objRoleFADApprover;
     }
     
     @isTest
     static void testHandleInboundEmail()
     {
     	
        User u = [SELECT Id FROM User where Email = 'test.testing@hep.dev.com'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
        objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
        insert objUserRole;
        HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
        objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
        insert objUserRoleOperation;

        HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'SPEND','Hierarchical', 'Pending', true);
        HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

        //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
        HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

        HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
        objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
        insert objRecordApprover;

        String sApproverId = objRecordApprover.id;
        String Body ='';
        //Messaging.InboundEmail.binaryAttachment battachment = new Messaging.InboundEmail.binaryAttachment();
    	//battachment.fileName = 'filename.csv';
    	//battachment.body = Blob.valueOf(Body);
        List<String> lstToAddresses = new List<String>();
        lstToAddresses.add('test@test.com');

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.toAddresses = lstToAddresses;
        email.subject = 'Email Subject('+sApproverId+')test Approved';
        email.plainTextBody = 'Email text body APPROVE.';
        //email.binaryAttachments = new List<Messaging.InboundEmail.binaryAttachment>{battachment};
        email.fromAddress = 'test.testing@hep.dev.com';
        envelope.fromAddress = 'test.testing@hep.dev.com';

        test.startTest();
        Messaging.InboundEmailResult emailResult = new Messaging.InboundEmailResult();
        HEP_InboundEmailHandler inboundEmailHandlerInstance = new HEP_InboundEmailHandler();
        emailResult = inboundEmailHandlerInstance.handleInboundEmail(email, envelope);

        test.stopTest();
     }

     @isTest
     static void testHandleInboundEmail2()
     {
        //list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        User u = [SELECT Id FROM User where Email = 'test.testing@hep.dev.com'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
        objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
        insert objUserRole;
        HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
        objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
        insert objUserRoleOperation;

        HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'SPEND','Hierarchical', 'Pending', true);
        HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

        //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
        HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

        HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
        objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
        insert objRecordApprover;

        String sApproverId = objRecordApprover.id;
        String Body ='';
        //Messaging.InboundEmail.binaryAttachment battachment = new Messaging.InboundEmail.binaryAttachment();
        //battachment.fileName = 'filename.csv';
        //battachment.body = Blob.valueOf(Body);
        List<String> lstToAddresses = new List<String>();
        lstToAddresses.add('test@test.com');

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        email.toAddresses = lstToAddresses;
        email.subject = 'Email Subject('+sApproverId+')test Approved';
        email.plainTextBody = 'Email text body REJECTED.';
        //email.binaryAttachments = new List<Messaging.InboundEmail.binaryAttachment>{battachment};
        email.fromAddress = 'test.testing@hep.dev.com';
        envelope.fromAddress = 'test.testing@hep.dev.com';

        test.startTest();
        Messaging.InboundEmailResult emailResult = new Messaging.InboundEmailResult();
        HEP_InboundEmailHandler inboundEmailHandlerInstance = new HEP_InboundEmailHandler();
        emailResult = inboundEmailHandlerInstance.handleInboundEmail(email, envelope);

        test.stopTest();
     }

}