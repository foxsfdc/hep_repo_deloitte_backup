/**
 * HEP_Create_Local_Catalog_Controller --- Class for the creation of local catalogs.
 * @author    Gaurav Mehrishi
 */
public without sharing class HEP_Create_Local_Catalog_Controller {
    /**
     * LocalCatalogWrapper --- Class to hold data related to Local Catalog
     * @author    Gaurav Mehrishi
     */

    public class LocalCatalogWrapper implements Comparable{
        public String sValue;
        public String sKey;
        public String sYearOfProductions;
        public String sFTID;
        public String sCatalogType;
        public String sCatalogNumber;

        /**
         * LocalCatalogWrapper Constructor --- Constructor to initialize the List variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public LocalCatalogWrapper(String sKey, String sValue, String sFTID, String sCatalogType, String sCatalogNumber, String sYearOfProductions) {
            this.sValue = sValue;
            this.sKey = sKey;
            this.sYearOfProductions = sYearOfProductions;
            this.sFTID = sFTID;
            this.sCatalogType = sCatalogType;
            this.sCatalogNumber = sCatalogNumber;
        }
        
        
        /*
         * compareTo: Overriding the dfault CompareTo for Numeric Sort
         * @param  objToCompare : Wrapper Object
         * @param  sValue Name field
         * @return   Integer Difference
        */
        
        public Integer compareTo(Object objToCompare) {
            //Sort by CatalogNumber Numerically
            return Integer.valueOf(Integer.valueOf(sCatalogNumber) - Integer.valueOf(((LocalCatalogWrapper)objToCompare).sCatalogNumber));
        }    
    }

    /*
     * Helps to check if the current user has access to HEP_Create_Local_Catalog.Page
     * @return Map of Permission Name and its value 
    public PageReference checkPermissions() {
        Map < String, String > mapCustomPermissions = HEP_Utility.fetchCustomPermissions(null, 'HEP_Create_Local_Catalog');
        system.debug('--------->' + mapCustomPermissions);
        if (mapCustomPermissions == NULL || mapCustomPermissions.isEmpty() || !mapCustomPermissions.containsKey('HEP_Create_Local_Catalog')) {
            PageReference retURL = new PageReference('/apex/HEP_AccessDenied');
            retURL.setRedirect(true);
            return retURL;
        } else {
            return null;
        }
    }*/
    // Record Status Active Constraints
    public static String sACTIVE;
    public static String sApproved;
    public static String sDraft;
    static{
       sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
       sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
       sDraft = HEP_Utility.getConstantValue('HEP_CATALOG_STATUS_DRAFT');
    }   

    /**
     * Picklists --- Class to hold all the picklist to be shown on the screen.
     * @author    Gaurav Mehrishi
     */
    public class Picklists {
        public LIST < LocalGenreWrapper > lstLocalGenre;
        public LIST<HEP_Utility.MultiPicklistWrapper> lstGenres;
        public LIST < TerritoryWrapper > lstTerritory;
        public LIST < LicensorWrapper > lstLicensor;

        /**
         * Picklists Constructor --- Constructor to initialize the variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public Picklists() {
            lstLocalGenre = new LIST < LocalGenreWrapper > ();
            lstTerritory = new LIST < TerritoryWrapper > ();
            lstLicensor = new LIST < LicensorWrapper > ();
            lstGenres = new LIST<HEP_Utility.MultiPicklistWrapper>();

        }
    }


    /**
     * TerritoryWrapper --- Class to hold data related to TerritoryWrapper that's selected on the screen. 
     * @author    Gaurav Mehrishi
     */
    Public class TerritoryWrapper {
        public String sKey;
        public String sValue;
        public LIST < LicensorTypeWrapper > lstLicensorType;
        public LIST < LicensorTypeGroupWrapper > lstLicensorTypeGroups;
        public TerritoryWrapper() {}
        /**
         * TerritoryWrapper Constructor --- Constructor to initialize the List variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public TerritoryWrapper(String sKey, String sValue) {
            this.sKey = sKey;
            this.sValue = sValue;
            lstLicensorType = new LIST < LicensorTypeWrapper > ();
            lstLicensorTypeGroups = new LIST < LicensorTypeGroupWrapper > ();
        }
    }
    /**
     * LicensorWrapper --- Class to hold data related to lstLicensor that's selected on the screen. 
     * @author    Gaurav Mehrishi
     */
    Public class LicensorWrapper {
        public String sKey;
        public String sValue;
        /**
         * LicensorWrapper Constructor --- Constructor to initialize the List variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public LicensorWrapper(String sKey, String sValue) {
            this.sKey = sKey;
            this.sValue = sValue;
        }
    }
    /**
     * LicensorTypeWrapper --- Class to hold data related to lstLicensorType that's selected on the screen. 
     * @author    Gaurav Mehrishi
     */
    Public class LicensorTypeWrapper {
        public String sKey;
        public String sValue;
        /**
         * LicensorTypeWrapper Constructor --- Constructor to initialize the List variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public LicensorTypeWrapper(String sKey, String sValue) {
            this.sKey = sKey;
            this.sValue = sValue;
        }
    }
    /**
     * LocalGenreWrapper --- Class to hold data related to local genre that's selected on the screen. 
     * @author    Gaurav Mehrishi
     */
    Public class LocalGenreWrapper {
        public String sKey;
        public String sValue;
        /**
         * LocalGenreWrapper Constructor --- Constructor to initialize the List variables
         * @param  sKey Salesforce id
         * @param  sValue Name field
         * @return   N/A
         */
        public LocalGenreWrapper(String sKey, String sValue) {
            this.sKey = sKey;
            this.sValue = sValue;
        }
    }
    /**
     * LicensorTypeGroupWrapper --- Class to hold data related to Licensor Type and related Licensor group that's selected on the screen. 
     * @author    Gaurav Mehrishi
     */
    public class LicensorTypeGroupWrapper {
        public string sLicensorType;
        public string sLicensorGroup;

        public LicensorTypeGroupWrapper(string sLicensorType, string sLicensorGroup) {
            this.sLicensorType = sLicensorType;
            this.sLicensorGroup = sLicensorGroup;
        }
    }
    /**
     * LocalCatalogPageData --- Class to hold data to be shown on the screen.
     * @author    Gaurav Mehrishi
     */
    Public class LocalCatalogPageData {
        public LocalCatalogWrapper objGlobalCatalogNameOrNumber;
        public String sTerritory;
        public LocalGenreWrapper objLocalGenre;
        public TerritoryWrapper objTerritory;
        public LicensorTypeWrapper objLicensorType;
        public LicensorTypeGroupWrapper objLicensorTypeGroup;
        public LicensorWrapper objLicensor;
        public String sYearOfProductions;
        public String sFTID;
        public String sLocalCatalogName;
        public String sCatalogType;
        public String sLicensorGroup;
        public String sCatalogNumber;
        public String sPromotionDetailURL;
        public Picklists picklist = new Picklists();
        public Map < String, String > mapErrorMessages;
        /**
         * LocalCatalogPageData Constructor --- Constructor to initialize the List variables
         * @return   N/A
         */
        public LocalCatalogPageData() {
            mapErrorMessages = HEP_Utility.getAllCreateCatalogErrorMessages();
        }
    }

    /**
     * Helps to get Data to be shown on screen on Page Load.
     * @return List of Picklist Wrapper 
     */
    @RemoteAction
    public static LocalCatalogPageData fetchLocalCatalogDefaultValues(String sPromoId) {
        LocalCatalogPageData objLocalCatalogPageData = new LocalCatalogPageData();
        objLocalCatalogPageData.sYearOfProductions = '';
        objLocalCatalogPageData.objGlobalCatalogNameOrNumber = new LocalCatalogWrapper('', '', '', '', '', '');
        objLocalCatalogPageData.sTerritory = '';
        objLocalCatalogPageData.sFTID = '';
        objLocalCatalogPageData.sCatalogType = '';
        objLocalCatalogPageData.sLocalCatalogName = '';
        objLocalCatalogPageData.sLicensorGroup = '';
        objLocalCatalogPageData.sPromotionDetailURL = '';
        objLocalCatalogPageData.objLicensorTypeGroup = new LicensorTypeGroupWrapper('', '');
        objLocalCatalogPageData.objLicensorType = new LicensorTypeWrapper('', '');
        objLocalCatalogPageData.objLicensor = new LicensorWrapper('', '');
        objLocalCatalogPageData.objTerritory = new TerritoryWrapper();
        objLocalCatalogPageData.objLocalGenre = new LocalGenreWrapper('', '');
        
        try {
            //Map to hold local genre values
            map < string, LocalGenreWrapper > mapLocalGenre = new map < string, LocalGenreWrapper > ();
            map < string, string > mapGenre = new map < string, string > ();
            //Map to hold licensor values
            map < string, LicensorWrapper > mapLicensor = new map < string, LicensorWrapper > ();
            Map < String, TerritoryWrapper > mapTerritory = new Map < String, TerritoryWrapper > ();
            List<HEP_Utility.MultiPicklistWrapper> lstGenres = new List<HEP_Utility.MultiPicklistWrapper>();
            LIST < HEP_Promotion__c > lstPrTerritoriesRecs = new LIST < HEP_Promotion__c >();
            if(!String.isEmpty(sPromoId)){
               String sPromotionTerritoryQuery = HEP_Utility.buildQueryAllString('HEP_Promotion__c', 'SELECT Territory__r.Name,', ' WHERE Id = \'' + sPromoId + '\'');
               System.debug('sPromotionTerritoryQuery' + sPromotionTerritoryQuery);
               lstPrTerritoriesRecs = (LIST < HEP_Promotion__c > ) Database.query(sPromotionTerritoryQuery);
               System.debug('lstPrTerritoriesRecs' + lstPrTerritoriesRecs);
            }
            if(lstPrTerritoriesRecs != null && !lstPrTerritoriesRecs.isEmpty() && lstPrTerritoriesRecs[0].Promotion_Type__c == 'National'){
                //Code to update the JSON's Territory Node with the Promotion territory
                for (HEP_Promotion__c objPromotion: lstPrTerritoriesRecs) {
                   TerritoryWrapper objTerritory = new TerritoryWrapper(objPromotion.Territory__c, objPromotion.Territory__r.Name);
                   mapTerritory.put(objPromotion.Territory__r.Name, objTerritory);
                }
            } else {
                //Logged In User Id
                String sLoggedInUserId = UserInfo.getUserId();
                //Pre-Populating the List of Territories.
                String sTerritoryQuery = HEP_Utility.buildQueryAllString('HEP_User_Role__c', 'SELECT Territory__r.Name, User__r.HEP_Line_Of_Business__c,', ' WHERE User__c = \'' + UserInfo.getUserId() + '\'  AND Territory__r.Name != \'Global\' AND Territory__r.Name != \'DHE\' AND Territory__r.CatalogTerritory__c = NULL AND Write__c = true AND Record_Status__c =: sACTIVE');
                System.debug('sTerritoryQuery' + sTerritoryQuery);
                LIST < HEP_User_Role__c > lstTerritoriesRecs = (LIST < HEP_User_Role__c > ) Database.query(sTerritoryQuery);
                System.debug('lstTerritoriesRecs------' + lstTerritoriesRecs);
  
                //Code to update the JSON's Territory Node with all the available territories
                for (HEP_User_Role__c objUserTerritory: lstTerritoriesRecs) {
                    TerritoryWrapper objTerritory = new TerritoryWrapper(objUserTerritory.Territory__c, objUserTerritory.Territory__r.Name);
                    mapTerritory.put(objUserTerritory.Territory__r.Name, objTerritory);
                }
                System.debug('mapTerritory>>>>>>>' + mapTerritory);
            }
            
            
            objLocalCatalogPageData.picklist.lstTerritory.addAll(mapTerritory.values());
            
            
            //Pre-Populating the List of Genres.
            list < HEP_List_Of_Values__c > lstListofValues = [select Parent_Value__c,
                Type__c,
                Values__c,
                Order__c
                from HEP_List_Of_Values__c
                where
                Record_Status__c =: sACTIVE and
                (Type__c =: HEP_Utility.getConstantValue('HEP_FOX_GENRE')
                or Type__c =: HEP_Utility.getConstantValue('HEP_FOX_LICENSOR'))
                order by Order__c
            ];
            for (HEP_List_Of_Values__c objListOfValue: lstListofValues) {
                if (objListOfValue.Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_FOX_GENRE'))) {
                    //if the filed is selected and set as need to pass the selected value as true
                    if (mapGenre.containskey(objListOfValue.Values__c)) {
                        lstGenres.add(new HEP_Utility.MultiPicklistWrapper(objListOfValue.Values__c, objListOfValue.Id, true));
                    } else {
                        lstGenres.add(new HEP_Utility.MultiPicklistWrapper(objListOfValue.Values__c, objListOfValue.Id, false));
                    }
                    //LocalGenreWrapper objLocalGenre = new LocalGenreWrapper(objListOfValue.Parent_Value__c, objListOfValue.Values__c);
                    //mapLocalGenre.put(objListOfValue.Parent_Value__c, objLocalGenre);
                } else if (objListOfValue.Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_FOX_LICENSOR'))) {
                    LicensorWrapper objLicensor = new LicensorWrapper(objListOfValue.Parent_Value__c, objListOfValue.Values__c);
                    mapLicensor.put(objListOfValue.Parent_Value__c, objLicensor);
                }
            }
            System.debug('mapLocalGenre>>>>>>>' + mapLocalGenre);
            System.debug('mapLicensor>>>>>>>' + mapLicensor);
            objLocalCatalogPageData.picklist.lstLicensor.addAll(mapLicensor.values());
            //objLocalCatalogPageData.picklist.lstLocalGenre.addAll(mapLocalGenre.values());
            objLocalCatalogPageData.picklist.lstGenres = lstGenres;
            //Get the Base URL for redirection 
            objLocalCatalogPageData.sPromotionDetailURL = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/HEP_Promotion_Details?promoId=' + sPromoId + '&tab=HEP_Promotion_Products';
        } catch (Exception e) {
            //Code to log error..
            HEP_Error_Log.genericException('Error occurred during Creation of Local Catalog',
                'VF Controller',
                e,
                'HEP_Create_Local_Catalog',
                'fetchLocalCatalogDefaultValues',
                null,
                '');

            throw e;
        }
        return objLocalCatalogPageData;
    }

    /**
     * Helps to get Territories related to a Marketing Manager and territory related Licensor type and related licensor group
     * @param  sTerritoryId - Salesforce Id
     * @return sTerritoryName - Saleforce Territory Name
     */
    @RemoteAction
    public static TerritoryWrapper getTerritoriesDefaultValues(String sTerritoryId, String sTerritoryName) {
        TerritoryWrapper objTerritoryToReturn;
        map < string, string > mapLicensorGroupParentValue = new map < string, string > ();
        String sLicensorGroupValue;
        picklists objpicklists = new picklists();
        LicensorTypeGroupWrapper objLicensorTypeGroup;
        string sTerritory = '%' + sTerritoryName + ',' + '%';
        list < string > lstCatalogTerritoryParent;
        list < LicensorTypeGroupWrapper > lstobjLicensorTypeGroup = new List < LicensorTypeGroupWrapper > ();
        try {
            //Pre-Populating Field Visibility Data for selected Territory
            objTerritoryToReturn = new TerritoryWrapper(sTerritoryId, sTerritoryName);
            list < HEP_List_Of_Values__c > lstListofValues = [select Parent_Value__c,
                Type__c,
                Values__c,
                Order__c
                from HEP_List_Of_Values__c
                where(Type__c =: HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_TYPE')
                    and Parent_Value__c like: sTerritory and Record_Status__c =: sACTIVE)
                or Type__c =: HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_GROUP')
                or Type__c =: HEP_Utility.getConstantValue('HEP_FOX_GENRE')
                or Type__c =: HEP_Utility.getConstantValue('HEP_FOX_LICENSOR')
                order by Order__c
            ];
            for (HEP_List_Of_Values__c objListOfValue: lstListofValues) {
                if (objListOfValue.Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_GROUP'))) {
                    mapLicensorGroupParentValue.put(objListOfValue.Parent_Value__c, objListOfValue.Values__c);
                }
            }
            for (HEP_List_Of_Values__c objListOfValue: lstListofValues) {
                if (objListOfValue.Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_FOX_LICENSOR_TYPE'))) {
                    if (objListOfValue.Parent_Value__c != null) {
                        lstCatalogTerritoryParent = objListOfValue.Parent_Value__c.split(',');

                        if (lstCatalogTerritoryParent[0] != null) {
                            sTerritory = lstCatalogTerritoryParent[1];
                            if (sTerritory != null) {
                                sLicensorGroupValue = mapLicensorGroupParentValue.get(sTerritory);
                            }
                            objLicensorTypeGroup = new LicensorTypeGroupWrapper(objListOfValue.Values__c, sLicensorGroupValue);
                            lstobjLicensorTypeGroup.add(objLicensorTypeGroup);
                            system.debug(lstobjLicensorTypeGroup);
                        }
                    }
                }
            }
            objTerritoryToReturn.lstLicensorTypeGroups = lstobjLicensorTypeGroup;
        } catch (Exception e) {

            //Code to log error..
            HEP_Error_Log.genericException('Error occurred during Creation of Local Catalog', 'VF Controller', e, 'HEP_Create_Local_Catalog', 'getTerritoriesDefaultValues', sTerritoryId, '');
            throw e;
        }
        return objTerritoryToReturn;
    }

    /**
     * Helps to get List of Global Catalogs for the typed name.
     * @param  sCatalogName - Name typed on the Catalog Name.
     * @return List of Global Catalog Wrapper Class Objects.
     */
    @RemoteAction
    public static LIST < LocalCatalogWrapper > getGlobalCatalogs(String sCatalogName) {
        LIST < LocalCatalogWrapper > globCatalogsToReturn = new LIST < LocalCatalogWrapper > ();

        try {

            //SOQL to sQueryGlobalCatalogs all fields for the selected Global Catalog
            String sQueryGlobalCatalogs = HEP_Utility.buildQueryAllString('HEP_Catalog__c', 'SELECT Title_EDM__r.FIN_PROD_ID__c,Title_EDM__r.PROD_CAL_YR__c,', 'WHERE Global_Catalog__c = NULL AND Territory__r.Name = \'Global\' AND Record_Status__c =: sACTIVE AND Status__c =: sApproved AND (Catalog_Name__c LIKE \'%' + String.escapeSingleQuotes(sCatalogName) + '%\' OR CatalogId__c LIKE \'%' + String.escapeSingleQuotes(sCatalogName) + '%\')');
            LIST < HEP_Catalog__c > lstGlobalCatalog = (LIST < HEP_Catalog__c > ) Database.query(sQueryGlobalCatalogs);
            system.debug('--------->' + lstGlobalCatalog);
            for (HEP_Catalog__c objGCat: lstGlobalCatalog) {
                globCatalogsToReturn.add(new LocalCatalogWrapper(objGCat.Id, objGCat.Catalog_Name__c, objGCat.Title_EDM__r.FIN_PROD_ID__c, objGCat.Catalog_Type__c, objGCat.CatalogId__c, objGCat.Title_EDM__r.PROD_CAL_YR__c));
            }
        } catch (Exception e) {
            //Code to log error..
            HEP_Error_Log.genericException('Error occurred during Creation of Local Catalog','VF Controller',e,'HEP_Create_Local_Catalog','getGlobalCatalogs',null,'');
            throw e;
        }
        globCatalogsToReturn.sort();
        return globCatalogsToReturn;
    }

    /**
     * Helps in getting the TypeAhead values
     * @param  fieldName API name of the field to be searched
     * @param  objectName API name of the object to be searched
     * @param  value that user types
     * @param  extraWhere extra where clause for further filtering
     * @return List of Picklist Wrapper
     */
    
    @RemoteAction
    public static LIST < HEP_Utility.PicklistWrapper > getTypeAheadValues(String sFieldName, String sObjectName, String sValue, String sExtraWhere) {
        return HEP_Utility.getTypeAheadValues(sFieldName, sObjectName, sValue, sExtraWhere);
    }

    /**
     * Method to Save the Data for local catalog into DataBase
     * @param  objPageData Wrapper class having all the required values to be updated in the Database
     * @return String
     */
    @RemoteAction
    public static String saveLocalCatalog(LocalCatalogPageData objPageData, String sPromoId) {
        string sCNFDL = HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL');
        string sMaster = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER');   
        Integer iOrder = 1;
        HEP_Catalog__c objcatalog;
        String sTerritoryId;
        String toReturn = null;
        Savepoint objSavePoint = Database.setSavepoint();
        map < string, string > mapLocalGenre = new map < string, string > ();
        try {

            MAP < String, String > mapCustomPermissions = HEP_Utility.fetchCustomPermissions(null, 'HEP_Create_Local_Catalog');
            system.debug('objPageData.objTerritory.sValue : ' +objPageData.objTerritory.sValue);
            //If the logged in user do have access to create record then redirect to Access Denied page.
            if(!HEP_Security_Framework.getMultipleTerritoryAccess().get(objPageData.objTerritory.sValue)){
                HEP_Utility.redirectToErrorPage();
            }
            /*if (mapCustomPermissions == NULL || mapCustomPermissions.isEmpty() || !mapCustomPermissions.containsKey('Save')) {
                HEP_Utility.redirectToErrorPage();
            }*/ else {
                System.debug('objPageData.objTerritory.sValue : ' +objPageData.objTerritory.sValue);
                System.debug('objPageData.objGlobalCatalogNameOrNumber.sCatalogNumber : ' +objPageData.objGlobalCatalogNameOrNumber.sCatalogNumber);
                Boolean bDuplicateCatalog = validateDuplicateCatalog(objPageData.objTerritory.sValue, objPageData.objGlobalCatalogNameOrNumber.sCatalogNumber);

                if (bDuplicateCatalog) {
                    toReturn = 'Error : Duplicate catalog records';
                } else {
                    List < EDM_GLOBAL_TITLE__c > lstGlobalTtile = new List < EDM_GLOBAL_TITLE__c > ();
                    List<HEP_Catalog_Genre__c> lstSelectedGenreInsert = new List<HEP_Catalog_Genre__c>();
                    List<HEP_Promotion_Catalog__c> lstPromotionCatalog = new List<HEP_Promotion_Catalog__c>();
                    if (!String.isEmpty(objPageData.sFTID)) {
                        lstGlobalTtile = [Select id FROM EDM_GLOBAL_TITLE__c WHERE FIN_PROD_ID__c =: objPageData.sFTID AND LIFE_CYCL_STAT_GRP_CD__c !=: sCNFDL];
                    }
                    //Create the record for Local Catalog
                    objcatalog = new HEP_Catalog__c();
                    objcatalog.Catalog_Name__c = objPageData.objGlobalCatalogNameOrNumber.sValue;
                    objcatalog.Local_Catalog_Name__c = objPageData.sLocalCatalogName;
                    //objcatalog.Catalog_Name__c = objPageData.sLocalCatalogName;
                    if(!lstGlobalTtile.isEmpty()){
                       objCatalog.Title_EDM__c = lstGlobalTtile[0].id;
                    }
                    objCatalog.Catalog_Type__c = objPageData.objGlobalCatalogNameOrNumber.sCatalogType;
                    objCatalog.CatalogId__c = objPageData.objGlobalCatalogNameOrNumber.sCatalogNumber;
                    sTerritoryId = objPageData.objTerritory.sKey;
                    objcatalog.Territory__c = sTerritoryId;
                    objcatalog.Production_Year__c = objPageData.objGlobalCatalogNameOrNumber.sYearOfProductions;
                    objCatalog.Global_Catalog__c = objPageData.objGlobalCatalogNameOrNumber.sKey;
                    objCatalog.Record_Status__c = sACTIVE;
                    objCatalog.Licensor_Type__c = objPageData.objLicensorTypeGroup.sLicensorType;
                    objCatalog.Licensor_Group__c = objPageData.objLicensorTypeGroup.sLicensorGroup;
                    objCatalog.Licensor__c = objPageData.objLicensor.sValue;  
                    objCatalog.Status__c = sApproved; 
                    objCatalog.Type__c = sMaster;                   
                    
                    insert objcatalog;
                    toReturn = objcatalog.Id;
                    system.debug('toReturn' + toReturn);
                    
                    //Create Promotion Catalog records
                    if(!String.isEmpty(sPromoId)){
                        HEP_Promotion_Catalog__c objPromoCatalog = new HEP_Promotion_Catalog__c();
                        objPromoCatalog.Catalog__c = objcatalog.Id;
                        objPromoCatalog.Promotion__c = sPromoId;
                        objPromoCatalog.Unique_Id__c = sPromoId + ' / ' + objcatalog.Id;                        
                        lstPromotionCatalog.add(objPromoCatalog);
                    }
                    if(lstPromotionCatalog != null && !lstPromotionCatalog.isEmpty()){   
                       insert lstPromotionCatalog;
                    } 
                    
                    //Create Catalog Genre records
                    // converting the string into list of string and storing the value into map mapLocalGenre to be the selected value as false
                    list < HEP_Catalog_Genre__c > lstCatGenre = [SELECT id, Name, Catalog__c, Record_Status__c
                                                             FROM   HEP_Catalog_Genre__c
                                                             WHERE  Catalog__c = : toReturn and Record_Status__c = :sACTIVE];
                    System.debug('*** lstCatGenre****' + lstCatGenre);
                    if (lstCatGenre != null) {
                        for (HEP_Catalog_Genre__c obj: lstCatGenre) {
                            if (obj.Record_Status__c == sACTIVE) mapLocalGenre.put(obj.Name, obj.id);
                        }    
                    }
                    for(HEP_Utility.MultiPicklistWrapper objGenre : objPageData.picklist.lstGenres){
                        system.debug('objGenre : ' + JSON.serialize(objGenre));
                        if(!mapLocalGenre.containskey(objGenre.sValue)){
                            system.debug('Coming of Age : ' + objGenre.sValue);
                            HEP_Catalog_Genre__c objCatalogGenre = new HEP_Catalog_Genre__c();
                            objCatalogGenre.Catalog__c = objCatalog.id;
                            objCatalogGenre.Genre__c = objGenre.sValue;
                            objCatalogGenre.Record_Status__c = sACTIVE;
                            objCatalogGenre.Order__c = iOrder;
                            lstSelectedGenreInsert.add(objCatalogGenre);
                            iOrder += 1;
                        }  
                        mapLocalGenre.remove(objGenre.sValue);
                    }
                    
                    if(lstSelectedGenreInsert != null && !lstSelectedGenreInsert.isEmpty()){   
                       insert lstSelectedGenreInsert;
                    }  
                }
            }
        } catch (Exception e) {

            //Code to log error..
            HEP_Error_Log.genericException('Error occurred during Creation of Local Catalog','VF Controller',e,'HEP_Create_Local_Catalog','saveLocalCatalog',sTerritoryId,'');
            Database.RollBack(objSavePoint);

            throw e;
        }
        return toReturn;
    }
    /**
     * Helps in checking the duplicate records
     * @param  sTerritory - Territory Name
     * @param  sCatalogNumber - Catalog Id/Catalog Number
     * @return boolean
     */
    public static Boolean validateDuplicateCatalog(String sTerritory, String sCatalogNumber) {
        Boolean bResultToReturn = false;
        String sTerritoryId;
        try {
            //String to hold Unique ID for catalog
            String UniqueId = sCatalogNumber + ' / ' + sTerritory;
            system.debug('UniqueId' + UniqueId);
            //Where Clause to check for local catalog
            String sWhereClauseCatalog = ' WHERE Unique_Id__c = \'' + UniqueId + '\'';
            system.debug('sWhereClauseCatalog' + sWhereClauseCatalog);
            String sCatalogQuery = HEP_Utility.buildQueryAllString('HEP_Catalog__c', null, sWhereClauseCatalog);
            Map < Id, HEP_Catalog__c > mapCatalogs = new Map < Id, HEP_Catalog__c > ((LIST < HEP_Catalog__c > ) DataBase.query(sCatalogQuery));
            system.debug('mapCatalogs' + mapCatalogs);
            //Check if local catalog exist for the selected Territory with same catalog Number then throw an error.
            if (!mapCatalogs.isEmpty()) {
                bResultToReturn = true;
            }
        } catch (Exception e) {
            //Code to log error..
            HEP_Error_Log.genericException('Error occurred during validating DUplicate Records', 'VF Controller', e, 'HEP_Create_Local_Catalog', 'validateDuplicateCatalog', sTerritoryId, '');
            throw e;
        }
        return bResultToReturn;
    }
    
}