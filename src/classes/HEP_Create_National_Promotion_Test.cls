@isTest
public class HEP_Create_National_Promotion_Test {
    @TestSetup
    static void testSetupMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date','HEP_ProphetProjectedDateDetails',true,10,10,'Outbound-Pull',false,false,true);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        test.startTest();
        HEP_Create_National_Promotion_Controller.getProjectedDates('79' , '197429');
        test.stopTest();
        //Create User
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
                                                              'test@deloitte.com','testL','testL','Fox - New Release',true);

        //Create Line of Business
        HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);

        //Create Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','Promotion',true);

        //Create EDM Product Type.
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

        //Create EDM Title Record.
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '197429' , 'PUBLC' , objProductType.Id, true);

        //Create Territory
        List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('Bulgaria','EMEA','Licensee',null,null,'ARS','DE','79','JDE',false)};

        lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
        lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
        lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
        lstTerritory[0].E1_Code__c  = '12341';
        lstTerritory[1].E1_Code__c  = '12342';
        lstTerritory[2].E1_Code__c  = '12343';
        insert lstTerritory;
        System.debug('Territory : ' + lstTerritory);

        //HEP_Interface__c objProphetInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date', 'HEP_ProphetProjectedDateDetails',true,3,10,'Outbound-Pull',false,true,true);

        
        

        

        List<HEP_Territory__c> lstRegions = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium','Domestic','Subsidiary',null,lstTerritory[1].Id,'USD','BE','49','JDE',false)};
        insert lstRegions;

        //Create Dating Matrix Records.
        List<HEP_Promotions_DatingMatrix__c> lstDatingMatrix = new List<HEP_Promotions_DatingMatrix__c>{HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release' , 'Release Dates' , 'PVOD Release Date' , lstTerritory[0].Id , null , false),
                                                                                                        HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release' , 'Release Dates' , 'PVOD Release Date' , lstTerritory[1].Id , null , false),
                                                                                                        HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release' , 'Release Dates' , 'PVOD Release Date' , lstTerritory[2].Id , null , false)};

        insert lstDatingMatrix;
        System.debug('lstDatingMatrix --> ' + lstDatingMatrix);
        //Create User Role
        List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,UserInfo.getUserId(),false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,UserInfo.getUserId(),false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,UserInfo.getUserId(),false)};

        insert lstUserRoles;
        //Create Notification Record..
        HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',true);

        HEP_Create_National_Promotion_Controller.DateTypeWithDate obj = new HEP_Create_National_Promotion_Controller.DateTypeWithDate('test',Date.today());
        HEP_Create_National_Promotion_Controller.DateTypeWithDate obj1 = new HEP_Create_National_Promotion_Controller.DateTypeWithDate();
        HEP_Create_National_Promotion_Controller.DateCombinations obj2 = new HEP_Create_National_Promotion_Controller.DateCombinations();
        HEP_Create_National_Promotion_Controller.DateWrapper obj3 = new HEP_Create_National_Promotion_Controller.DateWrapper();
    }

    @isTest
    static void test_method_one(){
        new HEP_Create_National_Promotion_Controller().checkPermissions();

        HEP_Create_National_Promotion_Controller.NationalPromotionPageData objPromWrap = HEP_Create_National_Promotion_Controller.fetchNationalPromotionDefaultValues();
        HEP_Create_National_Promotion_Controller.getFinacialTitleId('1234');

        objPromWrap.sLinking = HEP_Utility.getConstantValue('HEP_NO_LINK');
        String sPromotionId = HEP_Create_National_Promotion_Controller.saveNationalPromotion(objPromWrap);
        
        HEP_Create_National_Promotion_Controller.getTypeAheadValues('Name','HEP_Promotion__c','c',null);

    }

    @isTest
    static void test_method_two(){
        //Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        //HEP_Create_National_Promotion_Controller.getProjectedDates('79' , '197429');



        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,[SELECT ID FROM HEP_Territory__c][0].Id,[SELECT ID FROM HEP_Line_Of_Business__c][0].Id, null,UserInfo.getUserId(), false)};
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        insert lstPromotions;

        System.debug('lstPromotions ---> ' + lstPromotions);

        HEP_Create_National_Promotion_Controller.getGlobalPromotions(lstPromotions[0].PromotionName__c , HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));

    }
    @isTest
    static void test_method_three(){
        new HEP_Create_National_Promotion_Controller().checkPermissions();

        HEP_Create_National_Promotion_Controller.NationalPromotionPageData objPromWrap = HEP_Create_National_Promotion_Controller.fetchNationalPromotionDefaultValues();
        HEP_Create_National_Promotion_Controller.getFinacialTitleId('1234');

        objPromWrap.sLinking = HEP_Utility.getConstantValue('HEP_FTID');
        String sPromotionId = HEP_Create_National_Promotion_Controller.saveNationalPromotion(objPromWrap);

    }
    @isTest
    static void test_method_four(){
        new HEP_Create_National_Promotion_Controller().checkPermissions();

        HEP_Create_National_Promotion_Controller.NationalPromotionPageData objPromWrap = HEP_Create_National_Promotion_Controller.fetchNationalPromotionDefaultValues();
        HEP_Create_National_Promotion_Controller.getFinacialTitleId('1234');
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,[SELECT ID FROM HEP_Territory__c][0].Id,[SELECT ID FROM HEP_Line_Of_Business__c][0].Id, null,UserInfo.getUserId(), false)};
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = [SELECT ID FROM EDM_GLOBAL_TITLE__c][0].Id;
        insert lstPromotions;
        objPromWrap.objGlobalPromotionNameOrId.sFTID = lstPromotions[0].Title__c;
        objPromWrap.objGlobalPromotionNameOrId.sKey = lstPromotions[0].Id;
        objPromWrap.sLinking = HEP_Utility.getConstantValue('HEP_GLOBAL_PROMOTION');

        //Create Approved Catalog for Global Promotion...
        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'testName', 'Single', null , [SELECT ID FROM HEP_Territory__c][0].Id, 'Approved', 'Master', true);
        //Create Promotion Catalog..
        HEP_Promotion_Catalog__c objGlobalPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objGlobalCatalog.Id, lstPromotions[0].Id , null, true);

        String sPromotionId = HEP_Create_National_Promotion_Controller.saveNationalPromotion(objPromWrap);
    }
    @isTest
    static void test_method_five(){
        new HEP_Create_National_Promotion_Controller().checkPermissions();

        HEP_Create_National_Promotion_Controller.NationalPromotionPageData objPromWrap = HEP_Create_National_Promotion_Controller.fetchNationalPromotionDefaultValues();
        HEP_Create_National_Promotion_Controller.getFinacialTitleId('1234');

        objPromWrap.sLinking = HEP_Utility.getConstantValue('HEP_Discounts_Only');
        String sPromotionId = HEP_Create_National_Promotion_Controller.saveNationalPromotion(objPromWrap);
    }
}