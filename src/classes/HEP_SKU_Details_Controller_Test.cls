@isTest
    public class HEP_SKU_Details_Controller_Test{
        public static testmethod void fetchDataTest(){
            //All HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
            //Create HEP Territory Object
            HEP_Territory__c objTerritory = new HEP_Territory__c();
            objTerritory.Name = 'Australia';
            objTerritory.Region__c = 'APAC';
            objTerritory.Type__c = 'Subsidiary';
            objTerritory.CurrencyCode__c = 'AUD';
            objTerritory.ERMTerritoryCode__c = 'Test Code';
            objTerritory.Territory_Code_Integration__c = '000';
            objTerritory.MM_Territory_Code__c = 'a';
            insert objTerritory;
            //Create HEP Catalog Object
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.Catalog_Name__c = 'Primary Catalog';
            objCatalog.Product_Type__c = 'TV';
            objCatalog.Catalog_Type__c = 'Bundle';
            objCatalog.Territory__c = objTerritory.Id;  
            objCatalog.Record_Status__c  = 'Active';
            objCatalog.Status__c = 'Draft';
            objCatalog.Type__c = 'Master';
            insert objCatalog;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
            objSKUMaster.Record_Status__c  = 'Active'; 
            insert objSKUMaster;
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('xBox',objTerritory.Id,'4','MDP Customers','',false);
            insert objCustomer;
            HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id,objSKUMaster.Id,false);
            insert objSKUCustomer;
            test.startTest();
            HEP_SKU_Details_Controller.fetchData(objSKUMaster.Id,'');
            test.stopTest();
        }
        public static testmethod void fetchDataTest2(){
            //All HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
            //Create HEP Territory Object
            HEP_Territory__c objTerritory = new HEP_Territory__c();
            objTerritory.Name = 'Australia';
            objTerritory.Region__c = 'APAC';
            objTerritory.Type__c = 'Subsidiary';
            objTerritory.CurrencyCode__c = 'AUD';
            objTerritory.ERMTerritoryCode__c = 'Test Code';
            objTerritory.Territory_Code_Integration__c = '000';
            objTerritory.MM_Territory_Code__c = 'a';
            insert objTerritory;
            //Create HEP Catalog Object
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.Catalog_Name__c = 'Primary Catalog';
            objCatalog.Product_Type__c = 'TV';
            objCatalog.Catalog_Type__c = 'Bundle';
            objCatalog.Territory__c = objTerritory.Id;  
            objCatalog.Record_Status__c  = 'Active';
            objCatalog.Status__c = 'Draft';
            objCatalog.Type__c = 'Master';
            insert objCatalog;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
            objSKUMaster.Record_Status__c  = 'Active'; 
            insert objSKUMaster;
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('xBox',objTerritory.Id,'4','MDP Customers','',false);
            objCustomer.Customer_Type__c = 'SKU Customers';
            objCustomer.Territory__c = objTerritory.Id;
            insert objCustomer;
            HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id,objSKUMaster.Id,false);
            insert objSKUCustomer;
            List<HEP_List_Of_Values__c> lstLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
            test.startTest();
            HEP_SKU_Details_Controller.fetchData(objSKUMaster.Id,'');
            test.stopTest();
        }
        public static testmethod void saveDataTest(){
            //All HEP Constants
            List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
            //Create HEP Territory Object
            HEP_Territory__c objTerritory = new HEP_Territory__c();
            objTerritory.Name = 'Australia';
            objTerritory.Region__c = 'APAC';
            objTerritory.Type__c = 'Subsidiary';
            objTerritory.CurrencyCode__c = 'AUD';
            objTerritory.ERMTerritoryCode__c = 'Test Code';
            objTerritory.Territory_Code_Integration__c = '000';
            objTerritory.MM_Territory_Code__c = 'a';
            insert objTerritory;
            //Create HEP Catalog Object
            HEP_Catalog__c objCatalog = new HEP_Catalog__c();
            objCatalog.Catalog_Name__c = 'Primary Catalog';
            objCatalog.Product_Type__c = 'TV';
            objCatalog.Catalog_Type__c = 'Bundle';
            objCatalog.Territory__c = objTerritory.Id;  
            objCatalog.Record_Status__c  = 'Active';
            objCatalog.Status__c = 'Draft';
            objCatalog.Type__c = 'Master';
            insert objCatalog;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
            objSKUMaster.Record_Status__c  = 'Active'; 
            insert objSKUMaster;
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('xBox',objTerritory.Id,'4','MDP Customers','',false);
            objCustomer.Customer_Type__c = 'SKU Customers';
            objCustomer.Territory__c = objTerritory.Id;
            insert objCustomer;
            HEP_Utility.MultiPicklistWrapper objSelected = new HEP_Utility.MultiPicklistWrapper();
            objSelected.sValue = objCustomer.Id;
            objSelected.sKey = objCustomer.Name;
            objSelected.bSelected = true; 
            HEP_SKU_Details_Controller.SKUWrapper objSKUWrapper = new HEP_SKU_Details_Controller.SKUWrapper();
            objSKUWrapper.sSKUId = objSKUMaster.Id;
            objSKUWrapper.sSKUName = 'tst sku name';
            objSKUWrapper.sSKUNumber = 'num';
            objSKUWrapper.sChannel = 'VOD';
            objSKUWrapper.sFormat = 'SD';
            objSKUWrapper.sWareshouseStock = '15000';
            objSKUWrapper.lstCustomersSelected.add(objSelected);
            String sSKUWrapper = JSON.Serialize(objSKUWrapper); 
            test.startTest();
            HEP_SKU_Details_Controller.saveData(sSKUWrapper);
            test.stopTest();
            
        }
        
    }