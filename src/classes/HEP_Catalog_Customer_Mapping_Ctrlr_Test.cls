@isTest
public class HEP_Catalog_Customer_Mapping_Ctrlr_Test{
    public static testmethod void fetchDataTest(){
        //To get Constant Values
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //Create HEP Territory Record
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        insert objTerritory;
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Test Role 
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create test User Role record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        HEP_Catalog__c objPrimaryCatalog = new HEP_Catalog__c();
        objPrimaryCatalog.Catalog_Name__c = 'Primary Catalog';
        objPrimaryCatalog.Product_Type__c = 'TV';
        objPrimaryCatalog.Catalog_Type__c = 'Bundle';
        objPrimaryCatalog.Territory__c = objTerritory.Id;   
        objPrimaryCatalog.Record_Status__c  = 'Active';
        objPrimaryCatalog.Status__c = 'Draft';
        objPrimaryCatalog.Type__c = 'Master';
        insert objPrimaryCatalog;
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Primary_Catalog_Id__c = objPrimaryCatalog.Id;
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        //To execute else part in fetch data
        HEP_Catalog__c objCatalog1 = new HEP_Catalog__c();
        objCatalog1.Catalog_Name__c = 'Primary Catalog';
        objCatalog1.Product_Type__c = 'TV';
        objCatalog1.Catalog_Type__c = 'Bundle';
        objCatalog1.Territory__c = objTerritory.Id; 
        objCatalog1.Primary_Catalog_Id__c = objPrimaryCatalog.Id;
        objCatalog1.Status__c = 'Draft';
        objCatalog1.Type__c = 'Master';
        insert objCatalog1;
        //Create HEP Customer record
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        //Create Product Mapping record
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.ASIN__c = 'id01';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'SD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Catalog__c = objCatalog.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        //To execute else clause when the customer field is empty on mapping record
        HEP_MDP_Product_Mapping__c objMapping1 = new HEP_MDP_Product_Mapping__c();
        objMapping1.ASIN__c = 'id01';
        objMapping1.Local_Description__c = 'Local';
        objMapping1.Channel__c = 'VOD';
        objMapping1.Format__c = 'SD';
        //objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping1.Catalog__c = objCatalog1.id;
        objMapping1.Record_Status__c = 'Active';
        //objMapping1.HEP_Territory__c = objTerritory.Id;
        insert objMapping1;
        System.runAs(u){
            test.startTest();
            HEP_Catalog_Customer_Mapping_Controller.fetchData(objCatalog.Id);
            HEP_Catalog_Customer_Mapping_Controller.fetchData(objCatalog1.Id);
            test.stopTest();
        }
    }
    public static testmethod void saveMappingDataTest(){
        //To get Constant Values
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        HEP_Catalog__c objPrimaryCatalog = new HEP_Catalog__c();
        objPrimaryCatalog.Catalog_Name__c = 'Primary Catalog';
        objPrimaryCatalog.Product_Type__c = 'TV';
        objPrimaryCatalog.Catalog_Type__c = 'Bundle';
        objPrimaryCatalog.Territory__c = objTerritory.Id;   
        objPrimaryCatalog.Record_Status__c  = 'Active';
        objPrimaryCatalog.Status__c = 'Draft';
        objPrimaryCatalog.Type__c = 'Master';
        insert objPrimaryCatalog;
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Primary_Catalog_Id__c = objPrimaryCatalog.Id;
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_Catalog_Customer_Mapping_Controller.ProductMappingWrapper objOuter = new HEP_Catalog_Customer_Mapping_Controller.ProductMappingWrapper();
        //Inner Wrapper record which holds mapping data fetched from Mapping object 
        HEP_Catalog_Customer_Mapping_Controller.ProductDataWrapper ObjInner = new HEP_Catalog_Customer_Mapping_Controller.ProductDataWrapper();
        objInner.sChannel = 'VOD';
        objInner.sFormat = 'HD';
        objOuter.lstProductData.add(objInner);
        objOuter.sSelectedRegion = 'Australia';
        objOuter.sSelectedCustomer = 'Amazon';
        objOuter.sIdCatalog = objCatalog.Id;
        objOuter.sSelectedRegionId = objTerritory.Id;
        map<String,String> tmpAccessMap = new map<String,String>();
        tmpAccessMap.put('WRITE','TRUE');
        objOuter.mapTerritoryAccess = tmpAccessMap;
        test.startTest();
        HEP_Catalog_Customer_Mapping_Controller.saveMappingData(objOuter);
        test.stopTest();
    }
    public static testmethod void saveMappingDataTest2(){
        //To get Constant Values
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        HEP_Catalog__c objPrimaryCatalog = new HEP_Catalog__c();
        objPrimaryCatalog.Catalog_Name__c = 'Primary Catalog';
        objPrimaryCatalog.Product_Type__c = 'TV';
        objPrimaryCatalog.Catalog_Type__c = 'Bundle';
        objPrimaryCatalog.Territory__c = objTerritory.Id;   
        objPrimaryCatalog.Record_Status__c  = 'Active';
        objPrimaryCatalog.Status__c = 'Draft';
        objPrimaryCatalog.Type__c = 'Master';
        insert objPrimaryCatalog;
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Primary_Catalog_Id__c = objPrimaryCatalog.Id;
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        HEP_MDP_Product_Mapping__c objMapping = new HEP_MDP_Product_Mapping__c();
        objMapping.ASIN__c = 'id01';
        objMapping.Local_Description__c = 'Local';
        objMapping.Channel__c = 'VOD';
        objMapping.Format__c = 'SD';
        objMapping.HEP_Customer__c = objCustomer.Id;
        objMapping.Catalog__c = objCatalog.id;
        objMapping.Record_Status__c = 'Active';
        objMapping.HEP_Territory__c = objTerritory.Id;
        insert objMapping;
        HEP_Catalog_Customer_Mapping_Controller.ProductMappingWrapper objOuter = new HEP_Catalog_Customer_Mapping_Controller.ProductMappingWrapper();
        //Inner Wrapper record which holds mapping data fetched from Mapping object 
        HEP_Catalog_Customer_Mapping_Controller.ProductDataWrapper ObjInner = new HEP_Catalog_Customer_Mapping_Controller.ProductDataWrapper();
        objInner.sRecordId = objMapping.Id;
        objInner.sChannel = 'VOD';
        objInner.sFormat = 'HD';
        objOuter.lstProductData.add(objInner);
        objOuter.sSelectedRegion = 'Australia';
        objOuter.sSelectedCustomer = 'Amazon';
        objOuter.sIdCatalog = objCatalog.Id;
        objOuter.sSelectedRegionId = objTerritory.Id;
        map<String,String> tmpAccessMap = new map<String,String>();
        tmpAccessMap.put('WRITE','TRUE');
        objOuter.mapTerritoryAccess = tmpAccessMap;
        test.startTest();
        HEP_Catalog_Customer_Mapping_Controller.saveMappingData(objOuter);
        test.stopTest();
    }
}