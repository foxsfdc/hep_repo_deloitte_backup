@isTest
public class HEP_Spend_ApprovalController_Test{
    
    @testSetup
    static void createSetupComponent(){
      //Create All test data;
      HEP_ApprovalProcess_Test.createTestUserSetup();
      
      HEP_List_Of_Values__c spendEmailLOV = new HEP_List_Of_Values__c();
        spendEmailLOV.Type__c = 'EMAIL_CC_USERS';
        spendEmailLOV.Name = 'HEP_Spends_Email_Notification';
        spendEmailLOV.Values__c = 'niskedia@deloitte.com';
        spendEmailLOV.Parent_Value__c = 'Germany';
        spendEmailLOV.Record_Status__c = 'Active';
        insert spendEmailLOV;
    }
    
    static TestMethod void testComponentForSpendApproved() {
       HEP_ApprovalProcess_Test.testBudgetRequestApproved();
    }
    
    static TestMethod void testComponentForSpendRejected() {
        HEP_ApprovalProcess_Test.testBudgetRequestRejected();
    }
}