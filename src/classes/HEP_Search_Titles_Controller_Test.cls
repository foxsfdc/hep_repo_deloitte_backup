/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest(seeAllData = false)
public class HEP_Search_Titles_Controller_Test {
    @isTest
    public static void testSetup()
    {   //Territory creation
        HEP_Territory__c gGermany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary' , null,null, 'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c gAustria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'AT', '33', null, true);
        HEP_Territory__c gLuxemborg = HEP_Test_Data_Setup_Utility.createHEPTerritory('Luxemborg', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'LU', '265', null, true);
        
        //User creation
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'Vishnu', 'vishnu@deloitte.com', 'R', 'Vis', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        //Role creation
        HEP_Role__c marketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
        HEP_Role__c accountManager = HEP_Test_Data_Setup_Utility.createHEPRole('Account Manager', null, true);
        
        //User role creation
        HEP_User_Role__c VRole1 = HEP_Test_Data_Setup_Utility.createHEPUserRole(gAustria.id, marketingManager.id, u.id, true);
        HEP_User_Role__c VRole2 = HEP_Test_Data_Setup_Utility.createHEPUserRole(gGermany.id, marketingManager.id, u.id, true);
        
        
    }
    @isTest
    static void testSearchTitles(){
        //Create Product type
        EDM_REF_PRODUCT_TYPE__c prodType1 = HEP_Test_Data_Setup_Utility.createEDMProductType('Episode', 'EPSD', true);
        EDM_REF_PRODUCT_TYPE__c prodType2 = HEP_Test_Data_Setup_Utility.createEDMProductType('Direct To Video', 'DTV', true);
        EDM_REF_PRODUCT_TYPE__c prodType3 = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature', 'FTR', true);
        EDM_REF_PRODUCT_TYPE__c prodType4 = HEP_Test_Data_Setup_Utility.createEDMProductType('Movie Of the Week', 'MOW', true);
        
        //Create titles
        EDM_GLOBAL_TITLE__c EDMTitle1 = HEP_Test_Data_Setup_Utility.createTitleEDM('Deadpool 2', '032058', 'PUBLC', prodType2.id , true);
        EDM_GLOBAL_TITLE__c EDMTitle2 =HEP_Test_Data_Setup_Utility.createTitleEDM('Siren of Baghdad','157440' , 'PUBLC', prodType4.id , true);
        EDM_GLOBAL_TITLE__c EDMTitle3 =HEP_Test_Data_Setup_Utility.createTitleEDM('G-Force','108256' , 'PUBLC', prodType2.id , true);
        EDM_GLOBAL_TITLE__c EDMTitle4 =HEP_Test_Data_Setup_Utility.createTitleEDM('I Am A HERO','175566' , 'PUBLC', prodType1.id , true);
        EDM_GLOBAL_TITLE__c EDMTitle5 =HEP_Test_Data_Setup_Utility.createTitleEDM('Obsessed','49463' , 'PUBLC', prodType3.id , true);
        EDM_GLOBAL_TITLE__c EDMTitle6 =HEP_Test_Data_Setup_Utility.createTitleEDM('Obsessed 2','49464' , 'PUBLC', prodType3.id , false);
        EDM_REF_DIVISION__c division = new EDM_REF_DIVISION__c();
        division.Name = 'Non-Fox';
        division.DIV_CD__c = '20056';
        EDMTitle6.FIN_DIV_CD__c = division.id;
        insert EDMTitle6;
        
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        //Search on title name
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Title Name', 'Starts With', 'Dead', null, null);
        
        //Search on FTID
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Financial Title ID', 'Contains', '082', null, null);
        
        //Search on Product type
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Type', 'Contains', 'pis', null, null);
        
        //Search on division
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Division', 'Contains', 'Non', null , null);
        
        //Search on keyword
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Keyword', 'Contains', 'ren', null , null);
        
        //Universal search null keyword and criteria
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', null, null, 'ren', null , null);
        
        //Release year search
        Date startDate = Date.today();
        Date endDate =  startDate.addDays(50);
        HEP_Search_Titles_Controller.getTitleData('DD-MM-YYYY', 'Release Year', 'Contains', 'ren', startDate , endDate);
        
    }

}