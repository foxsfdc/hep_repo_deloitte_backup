/**
 * HEP_Foxipedia_AlternateTitles -- Fetch Alternate titles from Foxipedia
 * @author  Impana K
 */

public with sharing class HEP_Foxipedia_AlternateTitles implements HEP_IntegrationInterface{

    /**
    * PromotionDatingWrapper --- main wrapper which holds the entire request
    * @author    Impana
    */
    public class HEP_AlternateTitlesWrapper{
        public search search; 
        
        public HEP_AlternateTitlesWrapper(){
            search = new search();
        }
    }

    public class search{
        public String titleName; 

        public search(){
        	titleName = '';
        }
    }
    
    /**
    * ErrorResponseWrapper --- Wrapper class for response parse
    * @author    Impana
    */
    public class ErrorResponseWrapper {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage) {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }

    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Impana K
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        HEP_Services__c serviceDetails;
        list<String> lstRecordIds = new list<String>();
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();

        system.debug('HEP_Utility.getConstantValue' + HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH') );

        if(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH'));
        
        system.debug('sAccessToken ' + sAccessToken);
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            serviceDetails = HEP_Services__c.getInstance(HEP_Utility.getConstantValue('HEP_FOXIPEDIA_ALTERNATE_TITLES'));
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                httpRequest.setHeader('system','SVC_API_MDPDEV');
                
                if(String.isNotBlank(objTxnResponse.sSourceId)){

	                httpRequest.setBody(getRequestBody(objTxnResponse.sSourceId));
	                HTTP http = new HTTP();
	                system.debug('httpRequest : ' + httpRequest);
	                HTTPResponse httpResp = http.send(httpRequest);
	                system.debug('httpResp : ' + httpResp);

	                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
	                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                    system.debug('objTxnResponse.sResponse ' + objTxnResponse.sResponse);	                
	                String sHttpStatus = httpResp.getStatus();

	                if(sHttpStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')) || sHttpStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){  
	                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
	                    objTxnResponse.bRetry = false;
	                } else{
	                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
	                    objTxnResponse.sErrorMsg = httpResp.getStatus();
	                    objTxnResponse.bRetry = true;
	                }
	            }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }

    /**
    * Get the serialized json of the request to be sent
    * @param  list of record ids
    * @return 
    * @author Impana K
    */
    @testvisible
    private String getRequestBody(String strTitleName){
    	HEP_AlternateTitlesWrapper objAlternateWrapper = new HEP_AlternateTitlesWrapper();
    	objAlternateWrapper.search.titleName = strTitleName;
    	system.debug('request ' + JSON.serialize(objAlternateWrapper) );
        return JSON.serialize(objAlternateWrapper);       
    }
}