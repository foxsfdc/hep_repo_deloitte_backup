global class HEP_Send_Emails {
    webservice static String sendOutboundEmails(List<Id> lstEmailIds) {
        //--------------------------------------//      
      return sendEmailLogic(lstEmailIds,null);
    }
    
    public static String sendOutboundEmails(List<HEP_Outbound_Email__c> lstEmailToSend) {
        //--------------------------------------//      
      return sendEmailLogic(null,lstEmailToSend);
    }
    
    public static string sendEmailLogic(List<Id> lstEmailIds,List<HEP_Outbound_Email__c> lstEmailToSend){
      try
        {   
            // get email templates that we need to skip
            String sEmailsToSkip = Label.HEP_Email_For_Batch;
            Set<String> setTemplatesToSkip;
            if(String.isNotBlank(sEmailsToSkip))
                setTemplatesToSkip = new Set<String>(sEmailsToSkip.split(','));
            else
                setTemplatesToSkip = new Set<String>();

            if(lstEmailToSend==null)
                lstEmailToSend = [Select CC_Email_Address__c,Email_Template_Name__c,Object_API__c,Record_Id__c,To_Email_Address__c from HEP_Outbound_Email__c where id=:lstEmailIds];
            List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
            Messaging.SingleEmailMessage mail;
            map<String,Id> mapEmailTemplates = new map<String,Id>();
            //Set<String> setCCEmailIdsToSent = new Set<String>();
            Set<String> setEmailIdsToSent = new Set<String>();
            for (HEP_Outbound_Email__c emailRec: lstEmailToSend) {
                if(setTemplatesToSkip.contains(emailRec.Email_Template_Name__c)){ // skip these emails from processing
                }
                else {
                    mapEmailTemplates.put(emailRec.Email_Template_Name__c,null);
                    setEmailIdsToSent.addAll(HEP_Utility.getActualEmailIds(emailRec.To_Email_Address__c.split(';')));
                     if(String.isNotBlank(emailRec.CC_Email_Address__c) && emailRec.CC_Email_Address__c != null){
                        setEmailIdsToSent.addAll(HEP_Utility.getActualEmailIds(emailRec.CC_Email_Address__c.split(';')));   
                    }    
                }
            }
            //Map of Email Template Name and Developer Id of Template
            for (EmailTemplate emailTemp: [Select id, DeveloperName from EmailTemplate where DeveloperName =: mapEmailTemplates.keySet()])
            {
                mapEmailTemplates.put(emailTemp.DeveloperName ,emailTemp.Id);                
            }
            
            //Map of Email users and User Id
            Map<String,Id> mapUsersWithEmailPlusUserIds = new Map<String,Id>();
            System.debug('Email Id to be Sent--->'+setEmailIdsToSent);
            if(setEmailIdsToSent != null && !setEmailIdsToSent.isEmpty()){      
                for(User tempUser :[Select  Id, Email from User where
                                                    Email In: setEmailIdsToSent
                                                    And IsActive = true]){
                    if(!mapUsersWithEmailPlusUserIds.containsKey(tempUser.Email)){
                        mapUsersWithEmailPlusUserIds.put(tempUser.Email,tempUser.Id);
                    }
                }
            }
            // searching for HEP Org Wide Address to use
            List<OrgWideEmailAddress> lstOrgWideId = [select id, Address,DisplayName from OrgWideEmailAddress where DisplayName like '%HEP%'];
            //Process Emails
            List<String> lstCCEmailAddresses,lstSetToEmailAddresses;
            Set<String> userIdsToSend, ccUsersToSend;
            if(!mapUsersWithEmailPlusUserIds.isEmpty()){
                for (HEP_Outbound_Email__c emailRec: lstEmailToSend) {
                    //Fetch the Object details for Approval Action
                    //Fetch the Approval Owner Email Id
                    if(setTemplatesToSkip.contains(emailRec.Email_Template_Name__c)){ // skip these emails from processing
                    }
                    else
                    {
                        userIdsToSend = new set <String>();
                        ccUsersToSend = new set <String>();
                        lstCCEmailAddresses = String.isNotBlank(emailRec.CC_Email_Address__c)?HEP_Utility.getActualEmailIds(emailRec.CC_Email_Address__c.split(';')): NULL;
                        lstSetToEmailAddresses = String.isNotBlank(emailRec.To_Email_Address__c)?HEP_Utility.getActualEmailIds(emailRec.To_Email_Address__c.split(';')): NULL;
                        // loop thru To and CC addresses to check if user exists else move them to CC email address list
                        if(lstCCEmailAddresses != null){
                            for (String sEmailAddress : lstCCEmailAddresses)
                            {
                                if(mapUsersWithEmailPlusUserIds.containsKey(sEmailAddress)){
                                    userIdsToSend.add(mapUsersWithEmailPlusUserIds.get(sEmailAddress));
                                }
                                else
                                    ccUsersToSend.add(sEmailAddress);
                            }
                        }
                        if(lstSetToEmailAddresses != null){
                            for (String sEmailAddress : lstSetToEmailAddresses)
                            {
                                if(mapUsersWithEmailPlusUserIds.containsKey(sEmailAddress)){
                                    userIdsToSend.add(mapUsersWithEmailPlusUserIds.get(sEmailAddress));
                                }
                                else
                                    ccUsersToSend.add(sEmailAddress);
                            }
                        }
                        Boolean firstEmail = true;
                        // add a mail to single email for each of the user in the list
                        System.debug('Total To users--->'+userIdsToSend.size());
                        for (String userId : userIdsToSend)
                        {
                          //String sEmailAddress = lstSetToEmailAddresses.get(0);
                        
                            mail = new Messaging.SingleEmailMessage();
                            if(firstEmail)
                            {
                                firstEmail = false;
                                if(ccUsersToSend.size()>0)
                                    mail.setCcAddresses(new List<String>(ccUsersToSend));
                                System.debug('CC users--->'+ccUsersToSend);
                            }
                            
                            mail.setTemplateID(mapEmailTemplates.get(emailRec.Email_Template_Name__c));
                            mail.setSaveAsActivity(false);
                            mail.setWhatId(emailRec.Record_Id__c);                                     
                            mail.setTargetObjectId(userId); 
                            if(lstOrgWideId.size()>0)
                                mail.setOrgWideEmailAddressId(lstOrgWideId[0].Id);                      
                            // Set who the email is sent from       
                            //mail.setSenderDisplayName(System.Label.HEP_EMAIL_SENDER_DISPLAY_NAME);
                            //Shoot the Email
                            mails.add(mail);
                        }   
                    }
                                    
                }
            }
            System.debug('mails sent---->'+mails.size());
            if(mails.size()>0)
                Messaging.sendEmail(mails);         
            return '';
            
        }
        catch (Exception e) {
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            return NULL;
        }
        
}
}