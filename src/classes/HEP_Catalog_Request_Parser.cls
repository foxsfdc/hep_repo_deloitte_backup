/**
 * HEP_Catalog_Request_Parser --- This class will capture "New catalog Request" response from Foxipedia
 * @author    Abhishek
 */
public without sharing class HEP_Catalog_Request_Parser {
    /**
     * parseNewCatalogresponse  : to get the list of all Title Associated with the Catalog.
     * @param           : sCatalogResponse : Response Captured from Foxipedia
     * @return          : void 
     */
    public Static void parseNewCatalogResponse(string sCatalogResponse) {
        try {
            CatalogResponseFoxipediaWrapper objCatalogResponseFoxipediaWrapper;
            system.debug('sCatalogResponse : ' + sCatalogResponse);
            list < HEP_Catalog__c > lstCatalogs = new list < HEP_Catalog__c > ();
            //List of catalog for which we need to send application alerts
            list < HEP_Catalog__c > lstCatalogAlerts = new list < HEP_Catalog__c > ();
            //List of users to notify based on Territory
            List<Id> lstUserId = new List<Id>();
            String sCatalogNumber;

            //All constant values 
            string sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            string sSUCCESS = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
            string sREJECTED = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
            string sINCOMPLETE = HEP_Utility.getConstantValue('HEP_Incomplete');
            string sINCORRECT = HEP_Utility.getConstantValue('HEP_Incorrect');
            string sDUPLICATE = HEP_Utility.getConstantValue('HEP_SKU_DUPLICATE');
            string sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
            string sMASTER = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER');
            string sGLOBAL = HEP_Utility.getConstantValue('HEP_GLOBAL_TERRITORY_NAME');

            //check if response from foxipedia is blank else convert it to wrapper structure
            if (!string.isBlank(sCatalogResponse)) {
                objCatalogResponseFoxipediaWrapper = (CatalogResponseFoxipediaWrapper) System.JSON.deserialize(sCatalogResponse, CatalogResponseFoxipediaWrapper.class);
            }
            system.debug('objCatalogResponseFoxipediaWrapper : ' + objCatalogResponseFoxipediaWrapper);

            //Check if Catalog is Approved 
            if (objCatalogResponseFoxipediaWrapper.status == sAPPROVED) {

                //List of all catalogs need to be created or updated
                list < HEP_Catalog__c > lstImpactedCatalogs = new list < HEP_Catalog__c > ();
                list < HEP_Catalog__c > lstFinalCatalogs = new list < HEP_Catalog__c > ();
                string sExistingCatalogId;
                map < Id, string > mapTerritoryIdsVsName = new map < Id, string > ();
                //Global Territory Details
                HEP_Catalog__c objGlobalCatalog;
                string sGlobalTerritoryId;
                string sGlobalTerritoryName;

                //Query all editable non-unique custom fields of catalog Object
                List < String > fields = new List < String > ();
                map < String, Schema.SObjectField > mapField = Schema.getGlobalDescribe().get('HEP_Catalog__c').getDescribe().fields.getMap();

                system.debug('mapField : ' + mapField);
                if (mapField != null) {
                    for (Schema.SObjectField ft: mapField.values()) { // loop through all field tokens (ft)
                        Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                        if (fd.isCustom()) { // field is creatable
                            fields.add(fd.getName());
                        }
                    }
                }

                system.debug('fields : ' + fields);

                //Query catalog with returned Salesforce Id
                sExistingCatalogId = objCatalogResponseFoxipediaWrapper.catalogRequestId;
                if (fields != null && !fields.isEmpty()) {
                    String sQuery = 'SELECT ' + String.join(fields, ',') + ' FROM HEP_Catalog__c where id = :sExistingCatalogId and record_status__C = :sACTIVE limit 1';
                    system.debug('sQuery : ' + sQuery);
                    lstCatalogs = database.query(sQuery);
                }
                system.debug('lstCatalogs : ' + lstCatalogs);
                //Update the catalog with approved details
                if (lstCatalogs != Null && !lstCatalogs.isEmpty()) {
                    lstCatalogs[0].Status__c = sAPPROVED;
                    lstCatalogs[0].CatalogId__c = objCatalogResponseFoxipediaWrapper.catalogNumber;
                    lstCatalogs[0].Type__c = sMASTER;
                    lstImpactedCatalogs.add(lstCatalogs[0]);
                }

                //list of Global and auto creation Territories
                List < HEP_Territory__c > lstTerritory = [Select id, Name, Auto_Catalog_Creation__c
                    FROM HEP_Territory__c where name =: sGLOBAL or Auto_Catalog_Creation__c != false
                ];

                //Get Global and auto creation Territories
                for (HEP_Territory__c objTerritory: lstTerritory) {
                    if (objTerritory.Name.equalsIgnoreCase(sGLOBAL)) {
                        sGlobalTerritoryId = objTerritory.id;
                        sGlobalTerritoryName = objTerritory.Name;
                    } else if (objTerritory.Auto_Catalog_Creation__c) {
                          if(lstCatalogs[0].Territory__c != objTerritory.id)
                              mapTerritoryIdsVsName.put(objTerritory.id, objTerritory.name);
                    }
                }

                //Create Global Catalog
                if (lstImpactedCatalogs[0].Territory__c != sGlobalTerritoryId) {
                    objGlobalCatalog = lstImpactedCatalogs[0].clone(false, false, false, false);
                    objGlobalCatalog.Global_Catalog__c = Null;
                    objGlobalCatalog.Territory__c = sGlobalTerritoryId;
                    objGlobalCatalog.Unique_Id__c = objCatalogResponseFoxipediaWrapper.catalogNumber + ' / ' + sGlobalTerritoryName;

                    upsert objGlobalCatalog Unique_Id__c;
                    system.debug('objGlobalCatalog : ' + objGlobalCatalog);
                    lstImpactedCatalogs[0].Global_Catalog__c = objGlobalCatalog.id;
                } else {
                    objGlobalCatalog = lstImpactedCatalogs[0];
                }
                //Create auto creation catalog
                if (mapTerritoryIdsVsName != null && !mapTerritoryIdsVsName.isEmpty()) {
                    for (Id sTerritoryId: mapTerritoryIdsVsName.keySet()) {
                        if(sTerritoryId != lstImpactedCatalogs[0].Territory__c){
                            HEP_Catalog__c objCatalog = lstImpactedCatalogs[0].clone(false, false, false, false);
                            objCatalog.Global_Catalog__c = objGlobalCatalog.id;
                            objCatalog.Territory__c = sTerritoryId;
                            objCatalog.Unique_Id__c = objCatalogResponseFoxipediaWrapper.catalogNumber + ' / ' + mapTerritoryIdsVsName.get(sTerritoryId);
                            lstImpactedCatalogs.add(objCatalog);
                        }    
                    }
                }
                system.debug('lstImpactedCatalogs : ' + lstImpactedCatalogs);  

                //upsert lstImpactedCatalogs Unique_Id__c;
                if (lstImpactedCatalogs != null && !lstImpactedCatalogs.isEmpty()) {
                    upsert lstImpactedCatalogs Unique_Id__c;
                }
                system.debug('lstImpactedCatalogs : ' + lstImpactedCatalogs);
                //Creating Application Alerts               
                lstUserId.add(lstCatalogs[0].CreatedById);
                if(!lstUserId.isEmpty() && !lstCatalogs.isEmpty()){
                    HEP_Notification_Utility.createNotificationAlerts(lstCatalogs,HEP_Utility.getConstantValue('HEP_Catalog_Approved'),lstUserId);
                }               
            }
            //Check if Catalog is Rejected  
            else {
                //Check if salesforce Id is there or not in Foxipedia Response
                if (!string.isBlank(objCatalogResponseFoxipediaWrapper.catalogRequestId)) {
                    lstCatalogs = [select id,CreatedById,Comments__c from HEP_Catalog__c where id =: objCatalogResponseFoxipediaWrapper.catalogRequestId];
                }
                //Check if the catalog is present or not in HEP
                if (lstCatalogs != Null && !lstCatalogs.isEmpty()) {
                    //Check if the Status of catalog is rejected
                    if (objCatalogResponseFoxipediaWrapper.status == sREJECTED) {
                        lstCatalogs[0].Status__c = sREJECTED;
                        //Check if Rejection Reason is incomplete or incorrect
                        if (objCatalogResponseFoxipediaWrapper.rejectionReason == sINCOMPLETE || objCatalogResponseFoxipediaWrapper.rejectionReason == sINCORRECT) {
                            lstCatalogs[0].Comments__c = (string.isBlank(lstCatalogs[0].Comments__c) ? '' : lstCatalogs[0].Comments__c + '. ') +
                                'Status : ' + objCatalogResponseFoxipediaWrapper.status +
                                '. rejectionReason : ' + objCatalogResponseFoxipediaWrapper.rejectionReason +
                                '. rejectionComment : ' + objCatalogResponseFoxipediaWrapper.rejectionComment;
                        }

                        //Check if Rejection Reason is duplicate
                        if (objCatalogResponseFoxipediaWrapper.rejectionReason == sDUPLICATE) {
                            lstCatalogs[0].Comments__c = (string.isBlank(lstCatalogs[0].Comments__c) ? '' : lstCatalogs[0].Comments__c + ' .') +
                                'Status : ' + objCatalogResponseFoxipediaWrapper.status +
                                '. rejectionReason : ' + objCatalogResponseFoxipediaWrapper.rejectionReason +
                                '. rejectionComment : ' + objCatalogResponseFoxipediaWrapper.rejectionComment +
                                '. The Duplicate Catalog Id present in Foxipedia is : ' + objCatalogResponseFoxipediaWrapper.originalCatalogNumber;
                        }
                    }
                    //update the catalog comments with rejection details
                    system.debug('lstCatalogs[0] : ' + lstCatalogs[0]);
                    update lstCatalogs[0];
                }
                //Creating Application Alerts               
                lstUserId.add(lstCatalogs[0].CreatedById);
                if(!lstUserId.isEmpty() && !lstCatalogs.isEmpty()){
                    HEP_Notification_Utility.createNotificationAlerts(lstCatalogs,HEP_Utility.getConstantValue('HEP_Catalog_Rejected'),lstUserId);
                }
            }
        } catch (Exception Ex) {
            HEP_Error_Log.genericException('Parses the response Foxipedia',
                'Apex Class',
                ex,
                'HEP_Catalog_Request_Parser',
                'parseNewCatalogresponse',
                null,
                '');
        }
    }

    /**
     * DateType --- Wrapper class for New catalog response captured from Foxipedia
     * @author    Abhishek Mishra
     */
    public class CatalogResponseFoxipediaWrapper {
        public String catalogNumber;
        public String requestSalesforceId;
        public String taskSuccess;
        public String taskId;
        public String status;
        public String originalCatalogNumber;
        public String rejectionReason;
        public String error;
        public String catalogRequestId;
        public String rejectionComment;

        /**
         * CatalogResponseFoxipediaWrapper  : to get the list of all Title Associated with the Catalog.
         * @param           : catalogNumber : catalog Number, requestSalesforceId : Task Id for catalog request,
         *                    taskSuccess : Task status, taskId : For catalog Request, status : Catalog Status, 
         *                    originalCatalogNumber : Used for Duplicate, rejectionReason : Catalog rejection reason,
         *                    error : Error while integration, catalogRequestId : Catalog Id, rejectionComment : Catalog rejection comment
         */
        public CatalogResponseFoxipediaWrapper(String catalogNumber, String requestSalesforceId, String taskSuccess, String taskId, String status, String originalCatalogNumber, String rejectionReason, String error, String catalogRequestId, String rejectionComment) {
            this.catalogNumber = catalogNumber;
            this.requestSalesforceId = requestSalesforceId;
            this.taskSuccess = taskSuccess;
            this.taskId = taskId;
            this.status = status;
            this.originalCatalogNumber = originalCatalogNumber;
            this.rejectionReason = rejectionReason;
            this.error = error;
            this.catalogRequestId = catalogRequestId;
            this.rejectionComment = rejectionComment;
        }
    }
}