@isTest
Public class HEP_Itunes_Price_Test{

    @testSetup 
    Static void setup(){
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, null, null,null,false);
        insert objPromotion;

        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;   
        
        HEP_MDP_Product_Mapping__c ObjMDPProductMappingHD = new HEP_MDP_Product_Mapping__c();
        ObjMDPProductMappingHD.Catalog__c = objCatalog.id;
        ObjMDPProductMappingHD.Adam_Id__c = '694505519';
        ObjMDPProductMappingHD.Channel__c = 'EST';
        ObjMDPProductMappingHD.Format__c = 'HD';
        insert ObjMDPProductMappingHD;
        
        
        HEP_MDP_Product_Mapping__c ObjMDPProductMappingSD = new HEP_MDP_Product_Mapping__c();
        ObjMDPProductMappingSD.Catalog__c = objCatalog.id;
        ObjMDPProductMappingSD.Adam_Id__c = '694505519';
        ObjMDPProductMappingSD.Channel__c = 'EST';
        ObjMDPProductMappingSD.Format__c = 'SD';
        insert ObjMDPProductMappingSD;
        

        HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,objTitle.id,objTerritory.id,false);
        objPromotionProduct.Product_End_Date__c = date.today()+30;
        objPromotionProduct.Product_Start_Date__c = date.today(); 
        objPromotionProduct.Approval_Status__c = 'Approved';
        objPromotionProduct.Record_Status__c = 'Active';
        objPromotionProduct.HEP_Catalog__c = objCatalog.id;
        objPromotionProduct.customer_id__c = '694505519';
        
        insert objPromotionProduct;
        
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsHD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'HD','EST',false);
        objPromotionDetialsHD.Promo_WSP__c = 10;
        objPromotionDetialsHD.Record_Status__c = 'Active';
        objPromotionDetialsHD.HEP_MDP_Product_Mapping__c = ObjMDPProductMappingHD.Id;
        insert objPromotionDetialsHD;
        
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsSD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'SD','EST',false);
        objPromotionDetialsSD.Promo_WSP__c = 10;
        objPromotionDetialsSD.Record_Status__c = 'Active';
        objPromotionDetialsSD.HEP_MDP_Product_Mapping__c = ObjMDPProductMappingSD.Id;
        insert objPromotionDetialsSD;       
        
        HEP_List_Of_Values__c objlistValueHD = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='HD');
        insert objlistValueHD;

        HEP_List_Of_Values__c objlistValueSD = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='SD');
        insert objlistValueSD;      
        
                            

        HEP_SKU_Master__c objSKUMasterSD = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, null, 'VOD SKU', 'Request', 'Physical', 'EST', 'SD', null);
        objSKUMasterSD.Record_Status__c = 'Active';
        insert objSKUMasterSD;

        HEP_SKU_Master__c objSKUMasterHD = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, null, 'VOD SKU', 'Request', 'Physical', 'EST', 'HD', null);
        objSKUMasterHD.Record_Status__c = 'Active';
        insert objSKUMasterHD;      
        
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.id, '10', 768.00, null);
        insert objPriceGrade;
        
        HEP_SKU_Price__c objSKUPriceSD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterSD.Id, objTerritory.Id, null, null, 'Active', false);
        insert objSKUPriceSD;       
        
        HEP_SKU_Price__c objSKUPriceHD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterHD.Id, objTerritory.Id, null, null, 'Active', false);
        insert objSKUPriceHD;

        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    
    @isTest 
    static void Promotion_Pro_Test(){   
        HEP_MDP_Promotion_Product__c objPromotionProduct = [Select id from HEP_MDP_Promotion_Product__c limit 1];
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProduct.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }    
    
    @isTest 
    static void Promotion_Pro_Detail_Test(){
        HEP_MDP_Promotion_Product_Detail__c objPromotionProductDetail = [Select id from HEP_MDP_Promotion_Product_Detail__c limit 1];
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProductDetail.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }
    
    @isTest 
    static void Promotion_Pro_Test1(){   
        HEP_MDP_Promotion_Product__c objPromotionProduct = [Select id from HEP_MDP_Promotion_Product__c limit 1];
        objPromotionProduct.Record_Status__c = 'InActive';
        objPromotionProduct.Title_EDM__c = NULL;
        update objPromotionProduct;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProduct.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        //demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    } 

    @isTest 
    static void Promotion_Pro_Detail_TestHD(){
        HEP_MDP_Promotion_Product_Detail__c objPromotionProductDetail = [Select id from HEP_MDP_Promotion_Product_Detail__c where Format__c = 'HD' limit 1];
        objPromotionProductDetail.Record_Status__c = 'InActive';
        update objPromotionProductDetail;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProductDetail.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }  
    
    @isTest 
    static void Promotion_Pro_Detail_TestSD(){
        HEP_MDP_Promotion_Product_Detail__c objPromotionProductDetail = [Select id from HEP_MDP_Promotion_Product_Detail__c where Format__c = 'SD' limit 1];
        objPromotionProductDetail.Record_Status__c = 'InActive';
        update objPromotionProductDetail;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProductDetail.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }  

    @isTest 
    static void Failure_Test(){
        HEP_MDP_Promotion_Product_Detail__c objPromotionProductDetail = [Select id from HEP_MDP_Promotion_Product_Detail__c where Format__c = 'SD' limit 1];
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_NTS_E1_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProductDetail.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }   

    @isTest 
    static void Response_Error(){
        HEP_MDP_Promotion_Product_Detail__c objPromotionProductDetail = [Select id from HEP_MDP_Promotion_Product_Detail__c where Format__c = 'SD' limit 1];
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        HEP_Services__c objService1 = new HEP_Services__c();

        objService.Name='HEP_NTS_E1_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token';
        lstServices.add(objService);

        objService1.Name='HEP_iTunes_Service';
        objService1.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService1.Service_URL__c = '/v1/iTuneService/LookupAndUploadMetadata/v1';
        lstServices.add(objService1);


        insert lstServices;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objPromotionProductDetail.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Itunes_Price demo = new HEP_Itunes_Price();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }      
    
}