/**
* HEP_INT_DheSkuCustomerWrapper --JDE SkuCustomer Wrapper to store the DheSkuCustomer details from JDE And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuCustomerWrapper{
    public List<DheSkuCustomer> dhe_sku_customer;
    public String calloutStatus;
    public String TxnId;
    public List<HEP_INT_DheSkuInvoiceWrapperUtility.Errors> errors;
    
    /**
    * DheSkuPrice -- Class to hold Sku Customer details 
    * @author    Ram Bairwa
    */
    public class DheSkuCustomer{
       //common fields for both interfaces JDE and E1
            public String Customer_Number;
            public String Action_Code;
            public String Time_Last_Updated;
            public String Date_Updated;
            public String SKU_No;//SKU#
    }

}