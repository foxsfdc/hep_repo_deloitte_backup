/**
* HEP_Scheduled_Promotion_Dating_Con_Ctrl -- Controller class for Scheduling the emails of Promotion Dating records with Confirmed Status
* @author    Lakshman Jinnuri
*/
global class HEP_Scheduled_Promotion_Dating_Con_Ctrl implements Schedulable{
    /**
    *This method gets the details of the batch job and based on that it will trigger the weekly and daily emails to be sent to operations and digital team
    *@param ctx gets the details of the scheduled job 
    *@return nothing
    */
    global void execute(SchedulableContext ctx){
        //Variables to hold the custom Settings data
        String sHEP_WEEKLY_EMAIL = HEP_Utility.getConstantValue('HEP_WEEKLY_EMAIL');
        String sHEP_BATCH_PHYSICAL = HEP_Utility.getConstantValue('HEP_BATCH_PHYSICAL');
        String sHEP_BATCH_DIGITAL = HEP_Utility.getConstantValue('HEP_DIGITALRECIPIENTS');       
        CronTrigger lstJob = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType 
                             FROM CronTrigger WHERE Id =: ctx.getTriggerId()];
        CronJobDetail lstCjd;
        if(lstJob != null) 
            lstCjd = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Id = :lstJob.CronJobDetail.Id];
   
        //This logic below fetches the details of Promotion Dating records and sends the emails Daily for the Physical Recipients   
        if(lstCjd != null && sHEP_WEEKLY_EMAIL != null && sHEP_BATCH_PHYSICAL != null && !lstCjd.Name.equals(sHEP_WEEKLY_EMAIL)){
            HEP_Utility.fetchEmailRecipients(sHEP_BATCH_PHYSICAL); 
        }else{  
            DateTime dtDateTime = DateTime.now();
            String sDay = dtDateTime.format('EEEE');
            System.debug('Day'+sDay);
            //This logic below fetches the details of Promotion Dating records and sends the emails Weekly for the Digital Recipients 
            if(sDay.equals(System.Label.HEP_Week_Day) && sHEP_BATCH_DIGITAL != null){
                HEP_Utility.fetchEmailRecipients(sHEP_BATCH_DIGITAL);
            }      
        }
    }   
}