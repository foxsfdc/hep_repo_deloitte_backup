/**
 * HEP_Catalog_Promotions_Controller --  To fetch the Promotions in Catalog Master Promotions Subtab (Excluding customer promotions)
 * @author  :  Nishit Kedia    
 */
public class HEP_Catalog_Promotions_Controller{
    public static String sActive;
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * CatalogPromotionsWrapper -- Final Outer Wrapper of CatalogPromotions
    * @author    Nishit Kedia
    */
     public class CatalogPromotionsWrapper{
     	
        List<MasterCatalogPromotionsRowWrapper> lstCatalogRowData;
        
        public CatalogPromotionsWrapper(){
            lstCatalogRowData = new List<MasterCatalogPromotionsRowWrapper>();
        }
        
        public CatalogPromotionsWrapper(List<MasterCatalogPromotionsRowWrapper> lstCatalogRowData){
            this.lstCatalogRowData= lstCatalogRowData;
        }
    }
    
    /**
    * MasterCatalogPromotionsRowWrapper -- Inner wrapper for CatalogPromotions
    * @author    Nishit Kedia
    */
    public class MasterCatalogPromotionsRowWrapper{
    	
        String sPromotionId,sPromotionName, sRegion, sCustomer, sLineOfBusiness;
        Date dFirstAvailableDate;
        String sLocalPromotionCode,sMarketingManager,sStatus;
      
      public MasterCatalogPromotionsRowWrapper(){
          
      }
      
        /*public MasterCatalogPromotionsRowWrapper(String sPromotionId,String sPromotionName,String sRegion, String sCustomer, String sLineOfBusiness, Date dFirstAvailableDate,String sLocalPromotionCode,String sMarketingManager,String sStatus){
			
			this.sPromotionName=sPromotionName;
			this.sRegion=sRegion;
			this.sCustomer=sCustomer;
			this.sLineOfBusiness=sLineOfBusiness;
			this.dFirstAvailableDate=dFirstAvailableDate;
			this.sLocalPromotionCode=sLocalPromotionCode;
			this.sMarketingManager=sMarketingManager;
			this.sStatus=sStatus;
        }*/
        
    }
    /*
    @RemoteAction
    public static CatalogPromotionsWrapper loadData(String sCatalogId){
        
        //String sQuery = 'SELECT Promotion__r.Id, Promotion__r.PromotionName__c, Promotion__r.LocalPromotionCode__c, Promotion__r.Territory__r.Region__c, Promotion__r.customer__r.Name, Promotion__r.LineOfBusiness__r.Type__c, Promotion__r.FirstAvailableDate__c,Promotion__r.Domestic_Marketing_Manager__r.Name, Promotion__r.Status__c FROM HEP_Promotion_Catalog__c WHERE Catalog__r.Id =:sCatalogId and Catalog__r.Type__c= \'Master\' ORDER BY Promotion__r.Territory__r.Territory_Custom_Sort__c ASC, Promotion__r.Territory__r.Name';
        List<HEP_Promotion_Catalog__c> lstHEPPromotionCatalog = new List<HEP_Promotion_Catalog__c>();
        lstHEPPromotionCatalog = Database.query(sQuery);
        
        MasterCatalogPromotionsRowWrapper catalogPromoRow;
        List<MasterCatalogPromotionsRowWrapper> lstCatalogRowData = new List<MasterCatalogPromotionsRowWrapper>();
        if(lstHEPPromotionCatalog != null){
            for(HEP_Promotion_Catalog__c obj : lstHEPPromotionCatalog){
                catalogPromoRow = new MasterCatalogPromotionsRowWrapper();
                catalogPromoRow.sPromotionId = obj.Promotion__r.Id;
            
                
                if(obj.Promotion__r.PromotionName__c !=null )
                    catalogPromoRow.sPromotionName = obj.Promotion__r.PromotionName__c;
                
                if(obj.Promotion__r.Territory__r.Region__c !=null )
                    catalogPromoRow.sRegion = obj.Promotion__r.Territory__r.Region__c;
                
                if(obj.Promotion__r.LineOfBusiness__r.Type__c !=null )
                    catalogPromoRow.sLineOfBusiness = obj.Promotion__r.LineOfBusiness__r.Type__c;
                    
                if(obj.Promotion__r.FirstAvailableDate__c !=null )
                    catalogPromoRow.dFirstAvailableDate = obj.Promotion__r.FirstAvailableDate__c;
                
                if(obj.Promotion__r.LocalPromotionCode__c !=null )
                    catalogPromoRow.sLocalPromotionCode = obj.Promotion__r.LocalPromotionCode__c;    
                
                if(obj.Promotion__r.Domestic_Marketing_Manager__c !=null )
                    catalogPromoRow.sMarketingManager = obj.Promotion__r.Domestic_Marketing_Manager__r.Name; 
                    
                if(obj.Promotion__r.Status__c !=null )
                    catalogPromoRow.sStatus = obj.Promotion__r.Status__c; 
                
                lstCatalogRowData.add(catalogPromoRow);
            }
            CatalogPromotionsWrapper CatalogPromoData = new CatalogPromotionsWrapper(lstCatalogRowData);
            return CatalogPromoData;
        }
        return new CatalogPromotionsWrapper();
    }*/
    
    
    /**
     * loadData -- Remote Action Method to fetch the data on Page Load
     * @param	Catalog Id passed as String
     * @return 	Json data for CatalogPromotionsWrapper 
     * @author  Nishit Kedia    
     */    
    @RemoteAction
    public static CatalogPromotionsWrapper loadData(String sCatalogId){
        
        String sQuery = 'SELECT Id, PromotionName__c,Promotion_Type__c, Territory__r.Name, customer__c, LineOfBusiness__r.Type__c, FirstAvailableDate__c,LocalPromotionCode__c,Domestic_Marketing_Manager__r.Name, Status__c FROM HEP_Promotion__c ';
        
        //Users can only view Promotions for territories they have been given access for; meaning a territory filter must be applied
        //List<HEP_User_Role__c> lstTerritoryRolesAcccessible = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
        Set<Id> setTerritorysAccessible = new Set<Id>();
        
        String sCurrentPageName = '';
        
        if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_CATALOG_DETAILS'))){
	    	sCurrentPageName = HEP_Utility.getConstantValue('HEP_CATALOG_DETAILS');
	    }
        setTerritorysAccessible = HEP_Utility.fetchTerritorysForPage(sCurrentPageName);
        
        System.debug('setTerritorysAccessible--------->' + setTerritorysAccessible);
        if(String.isNotBlank(sCatalogId)){
          
          //Make Territory Filter
          sQuery+=' Where Territory__c In: setTerritorysAccessible AND Record_Status__c =\'Active\' ';
          
          Set <Id> setPromoIds = fetchAllActivePromotionIds(sCatalogId);
          System.debug('Indirect promo Ids ----> ' + setPromoIds);
                 
          // Controller must perform the various logical queries, then merge the results, removing duplicate promotions: 
          if(setPromoIds != null && !setPromoIds.isEmpty()){
                      
            //Fetch All Global Promotions
            //sQuery+=' And ( Id IN:setPromoIds ';
             sQuery+=' And  Id IN:setPromoIds AND Record_Status__c =:sActive';
            //Fetch Child Promotions As well
            //sQuery+=' Or Global_Promotion__c IN:setPromoIds) ';
            
          }
          else{
              return new CatalogPromotionsWrapper();
          }
        }
        
        //Promotion sorting should by as follows:◾Sort by global promo, DHE+Top 5 territories, and remaining territories alpha-ascending &
        // then sort by FAD (desc), Territory (asc), Promotion Name (asc)
        sQuery+=' order by Territory__r.Territory_Custom_Sort__c Asc, FirstAvailableDate__c desc, Territory__r.Name asc, PromotionName__c asc ';
        System.debug('Dynamic Query---->'+ sQuery);
        
        List<HEP_Promotion__c> lstHEPPromotionCatalog = new List<HEP_Promotion__c>();
        lstHEPPromotionCatalog = Database.query(sQuery);
        
        MasterCatalogPromotionsRowWrapper catalogPromoRow;
        
        List<MasterCatalogPromotionsRowWrapper> lstGlobalPromos = new List<MasterCatalogPromotionsRowWrapper>();
        List<MasterCatalogPromotionsRowWrapper> lstOtherPromos = new List<MasterCatalogPromotionsRowWrapper>();
        List<MasterCatalogPromotionsRowWrapper> lstCatalogRowData = new List<MasterCatalogPromotionsRowWrapper>();
       
        if(lstHEPPromotionCatalog != null){
            for(HEP_Promotion__c promoCatalog : lstHEPPromotionCatalog){
                
                catalogPromoRow = new MasterCatalogPromotionsRowWrapper();
                catalogPromoRow.sPromotionId = promoCatalog.Id;
                
                if(String.isNotBlank(promoCatalog.PromotionName__c))
                    catalogPromoRow.sPromotionName = promoCatalog.PromotionName__c;
                
                if(String.isNotBlank(promoCatalog.Territory__r.Name) )
                    catalogPromoRow.sRegion = promoCatalog.Territory__r.Name;
                
                if(String.isNotBlank(promoCatalog.LineOfBusiness__c) )
                    catalogPromoRow.sLineOfBusiness = promoCatalog.LineOfBusiness__r.Type__c;
                    
                if(promoCatalog.FirstAvailableDate__c != null )
                    catalogPromoRow.dFirstAvailableDate = promoCatalog.FirstAvailableDate__c;
                
                if(String.isNotBlank(promoCatalog.LocalPromotionCode__c) )
                    catalogPromoRow.sLocalPromotionCode = promoCatalog.LocalPromotionCode__c;    
                
                if(String.isNotBlank(promoCatalog.Domestic_Marketing_Manager__c) )
                    catalogPromoRow.sMarketingManager = promoCatalog.Domestic_Marketing_Manager__r.Name; 
                    
                if(String.isNotBlank(promoCatalog.Status__c) ){
                    if(HEP_Constants__c.getValues('PROMOTION_TYPE_CUSTOMER').Value__c.equals(promoCatalog.Promotion_Type__c))
                            catalogPromoRow.sStatus = promoCatalog.Status__c; 
                }
                if(HEP_Constants__c.getValues('HEP_PROMOTION_TYPE_GLOBAL').Value__c.equals(promoCatalog.Territory__r.Name))
                lstGLobalPromos.add(catalogPromoRow);
                else 
                lstOtherPromos.add(catalogPromoRow);
                
            }
            if(lstGLobalPromos!=null && lstGLobalPromos.size()>0)
                lstCatalogRowData.addAll(lstGLobalPromos);
            if(lstOtherPromos!=null && lstOtherPromos.size()>0)
                lstCatalogRowData.addAll(lstOtherPromos);
            CatalogPromotionsWrapper CatalogPromoData = new CatalogPromotionsWrapper(lstCatalogRowData);
            return CatalogPromoData;
        }
        return new CatalogPromotionsWrapper();
    }
    
    
    /**
     * fetchAllActivePromotionIds --  To Apply Buisness Logics when fetching Elligible Promotion Ids
     * @param	Catalog Id passed as String
     * @return 	Set Of Promotions Ids
     * @author  Nishit Kedia    
     */
    public static Set<Id> fetchAllActivePromotionIds(String sCatalogId){
      
      //This Set will contain All Promo Ids that needs to be filtered
      Set<Id> setPromoId = new Set<Id>();
      
      if(String.isNotBlank(sCatalogId)){
        
        String sActiveStatus = '';
        String sMasterCatalog ='';
        //Fetch Active Status String
        if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'))){
          sActiveStatus = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        }
        
        //Fetch Master String 
        if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER'))){
          sMasterCatalog = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER');
        }
        
        //◾Promotion Catalogs have a Lookup F/K to Catalog, Catalog has a Lookup F/K to Title:
        
        List<HEP_Promotion_Catalog__c> lstPromotionCatalogs = [Select   Id,
                                        Promotion__c,
                                        Record_Status__c,
                                        Catalog__c
                                        from
                                        HEP_Promotion_Catalog__c
                                        where
                                        Catalog__c = :sCatalogId
                                        And
                                        Catalog__r.Record_Status__c =:sActiveStatus
                                        And
                                        Record_Status__c =:sActiveStatus
                                        /*And 
                                        Catalog__r.Type__c =:sMasterCatalog
                                        */
                                        ];
                                        
        //Store the promoIds from the Promotion Catalog List in the Set                              
        
        if(lstPromotionCatalogs != null && !lstPromotionCatalogs.isEmpty()){
          for(HEP_Promotion_Catalog__c objPromoCatalogs:lstPromotionCatalogs){
            setPromoId.add(objPromoCatalogs.Promotion__c);
          }
        }
      }      
      //Return Promo ids
      return setPromoId;
    }
}