/**
* HEP_Navigation_Menu_Component_Controller --- Class which will hold the logic for navigation menu 
* @author    Sachin Agarwal
*/

public without sharing class HEP_Navigation_Menu_Component_Controller {
    public String allDateFormats {get;set;}
    public String sLocaleNumberFormat {get;set;}
    public String sLocaleDateFormat {get;set;}
    public Boolean bEnableConsoleLog{get;set;}
    
    public static map<string,String> mapURLParam {get;set;}
    
    public String sInactiveSelectOptions{get;set;}
    public static map<string,navQuickLink> mapQuickLinks;
    public static selected selectObj;
    public static String sPage='';
    
    /**
    * InactiveSelectOptions --- Wrapper class for holding the data for inactive LOVs
    * @author    Sachin Agarwal
    */
    public class InactiveSelectOptions{
        public String sType;
        public list<HEP_Utility.InactiveSelectOption> lstInactiveSelectOptions;
    }
    
    /**
    * Class Constructor
    * @return nothing
    */
    public HEP_Navigation_Menu_Component_Controller(){
        
        HEP_Enable_Console_Log__c objConsoleLog = HEP_Enable_Console_Log__c.getValues(UserInfo.getName());
        
        if(objConsoleLog != null){
            bEnableConsoleLog = true;
        }
        else{
            bEnableConsoleLog = false;
        }
        
        list<String> lstDateFormats = new list<String>();
        
        User objUser = [SELECT id, DateFormat__c, NumberFormat__c
                        FROM User
                        WHERE id = :UserInfo.getUserId()];
        
        map<String, String> mapUserLocale = HEP_Utility.mapGetUserLocaleData(objUser);
        String sDateFormat;
        if(mapUserLocale.containsKey(HEP_Utility.getConstantValue('HEP_USER_LOCALE_DATE_FORMAT'))){
            sDateFormat = mapUserLocale.get(HEP_Utility.getConstantValue('HEP_USER_LOCALE_DATE_FORMAT'));
            
            sDateFormat = sDateFormat.replaceAll( '\\s+', '');
            
            allDateFormats = sDateFormat;
            
            lstDateFormats.addAll(sDateFormat.split(','));
           
            if(String.isEmpty(sLocaleDateFormat)){
                if(lstDateFormats.isEmpty()){
                    sLocaleDateFormat = 'dd-MMM-yyyy';
                }
                else{
                    sLocaleDateFormat = lstDateFormats[0];
                }
            }            
        }
        else{
            sDateFormat = 'dd-MMM-yyyy,dd-MM-yyyy,dd/MM/yyyy,yyyy-MM-dd';
            allDateFormats = sDateFormat;
            lstDateFormats.addAll(sDateFormat.split(','));
            sLocaleDateFormat = lstDateFormats[0];
        }
        if(mapUserLocale.containsKey(HEP_Utility.getConstantValue('HEP_USER_LOCALE_NUMBER_FORMAT'))){
            sLocaleNumberFormat = mapUserLocale.get(HEP_Utility.getConstantValue('HEP_USER_LOCALE_NUMBER_FORMAT'));            
        }
        else{
            sLocaleNumberFormat = '{"sGroupSeparator": \',\', "sDecimalSeparator": \'.\', "iNoOfDecimalPlaces" : 2}';
        }
        
        map<String, list<HEP_Utility.InactiveSelectOption>> mapInactiveSelectOptions = HEP_Utility.getInactiveSelectOptions(true);
        
        sInactiveSelectOptions = JSON.serialize(mapInactiveSelectOptions);
    }

    /**
    * SearchWrapper --- Class to hold picklist field values
    * @author    Arjun Narayanan
    */
    public class SearchWrapper {
        public String sKey;
        public String sValue;
        public String sURL;
        public String sType;
        public String sOrder;

        /**
        * Class constructor
        * @param  sKey value that we will be showing in the UI
        * @param  sValue value that we are going to actually store in the backend
        * @param  sURL URL used to redirect on click
        * @param  sType Type of the search results to be displayed
        * @return nothing
        */
        public SearchWrapper(String sKey, String sValue, String sURL, String sType,String sOrder) {
            this.sKey = sKey;
            this.sValue = sValue;
            this.sURL = sURL;
            this.sType = sType;
            this.sOrder = sOrder;
        }
    }
    
    @remoteAction
    public static String logError(String sPageName,  String sMessage,String sErrorDesc,String sPageURL){
        return HEP_Error_Log.BusinessException(sPageName + ' ' + sMessage, 'Javascript', sMessage, sErrorDesc, sPageName, '', '', sPageURL);
    }
    
    /**
    * searchUniversal --- get Typeahead on search
    * @param sValue stores key for the search
    * @param sTerritory to filter results based on territory
    * @return List of SearchWrapper
    * @author Arjun Narayanan
    */
    @remoteAction
    public static LIST<SearchWrapper> searchUniversal(String sValue) {
        Map<string,string> mapOfOrderingValue = new Map<string,string>();
        String sPromotionOrder;
        String sTitleOrder;
        String sCatalogOrder;
        String sSKUOrder;
        
        for(HEP_List_Of_Values__c objListofValues : [SELECT id,Name,Type__c,Values__c 
                                                     FROM HEP_List_Of_Values__c 
                                                     WHERE type__c = :HEP_Utility.getConstantValue('HEP_NAV_QUICKSEARCH_ORDER')
                                                     AND Record_Status__c = :HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')]){
            mapOfOrderingValue.put(objListofValues.Name,objListofValues.Values__c);
        }
        
        if(mapOfOrderingValue.containsKey('Promotion')){
            sPromotionOrder = mapOfOrderingValue.get('Promotion');
        }
        if(mapOfOrderingValue.containsKey('Title')){
            sTitleOrder = mapOfOrderingValue.get('Title');
        }
        if(mapOfOrderingValue.containsKey('Promotion')){
            sCatalogOrder = mapOfOrderingValue.get('Catalog');
        }
        if(mapOfOrderingValue.containsKey('Promotion')){
            sSKUOrder = mapOfOrderingValue.get('SKU');
        }

        String sPromotionQuery = 'SELECT id,PromotionName__c,LocalPromotionCode__c,Territory__r.Name,Record_Status__c FROM HEP_Promotion__c';
        String sTitleQuery = 'select id,Name,FIN_PROD_ID__c from EDM_GLOBAL_TITLE__c';
        String sCatalogQuery = 'Select Id, Catalog_Name__c,CatalogId__c,Territory__r.Name,Record_Status__c FROM HEP_Catalog__c';
        String sSKUQuery = 'SELECT id, Name,SKU_Title__c,Type__c,SKU_Number__c, Territory__r.Name,Record_Status__c,Catalog_Master__r.Name FROM HEP_SKU_Master__c';
        
        String sConfidentialTitle = HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL');
        
        if(sValue != null){
            sPromotionQuery += ' WHERE (PromotionName__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR LocalPromotionCode__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\')';
            sTitleQuery += ' WHERE LIFE_CYCL_STAT_GRP_CD__c != :sConfidentialTitle AND (Name LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR FIN_PROD_ID__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\')';
            sCatalogQuery += ' WHERE (CatalogId__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR Catalog_Name__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\')';
            sSKUQuery += ' WHERE (SKU_Title__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR SKU_Number__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\')';
            
            sPromotionQuery += ' AND Record_Status__c = \'' + HEP_Constants__c.getInstance('HEP_RECORD_ACTIVE').Value__c + '\'';
            sCatalogQuery += ' AND Record_Status__c = \'' + HEP_Constants__c.getInstance('HEP_RECORD_ACTIVE').Value__c + '\'';
            sSKUQuery += ' AND Record_Status__c = \'' + HEP_Constants__c.getInstance('HEP_RECORD_ACTIVE').Value__c + '\'';
            
            if(String.isNotBlank(sPromotionQuery)){
                sPromotionQuery +=  ' ORDER BY PromotionName__c ASC Limit 100';
            }
            if(String.isNotBlank(sTitleQuery)){
                sTitleQuery +=  ' ORDER BY Name ASC Limit 100';
            }
            if(String.isNotBlank(sCatalogQuery)){
                sCatalogQuery +=  ' ORDER BY Catalog_Name__c ASC Limit 100';
            }
            if(String.isNotBlank(sSKUQuery)){
                sSKUQuery +=  ' ORDER BY SKU_Title__c ASC Limit 100';
            }
        }
        System.debug('Typeahead Query Promotions---->' + sPromotionQuery);
        System.debug('Typeahead Query Titles---->' + sTitleQuery);
        System.debug('Typeahead Query Catalog---->' + sCatalogQuery);
        System.debug('Typeahead Query SKU---->' + sSKUQuery);
        
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>();
        List<EDM_GLOBAL_TITLE__c> lstTitles = new List<EDM_GLOBAL_TITLE__c>();
        List<HEP_Catalog__c> lstCatalogs = new List<HEP_Catalog__c>();
        List<HEP_SKU_Master__c> lstSKU = new List<HEP_SKU_Master__c>();
        
        lstPromotions = Database.Query(sPromotionQuery);
        lstTitles = Database.Query(sTitleQuery);
        lstCatalogs = Database.Query(sCatalogQuery);
        lstSKU = Database.Query(sSKUQuery);
        
        LIST<SearchWrapper> lstAllResults = new LIST<SearchWrapper>();
        
        if(lstPromotions != NULL && lstPromotions.size() > 0){

            for(HEP_Promotion__c objPromotion : lstPromotions){
                lstAllResults.add(new SearchWrapper(objPromotion.PromotionName__c+'/--/'+objPromotion.LocalPromotionCode__c,objPromotion.Territory__r.Name+' - '+objPromotion.PromotionName__c,'HEP_Promotion_Details?promoId='+objPromotion.id,'Promotion',sPromotionOrder));
            }
        }
        if(lstTitles != NULL && lstTitles.size() > 0){
            for(EDM_GLOBAL_TITLE__c objTitle : lstTitles){
                lstAllResults.add(new SearchWrapper(objTitle.Name+'/--/'+objTitle.FIN_PROD_ID__c,objTitle.FIN_PROD_ID__c+' - '+objTitle.Name,'HEP_Title_Details?titleId='+objTitle.id,'Title',sTitleOrder));
            }
        }
        if (lstCatalogs != NULL && lstCatalogs.size() > 0) {
            for (HEP_Catalog__c objCatalog : lstCatalogs) {
                lstAllResults.add(new SearchWrapper(objCatalog.Catalog_Name__c+'/--/'+objCatalog.CatalogId__c,objCatalog.Territory__r.Name +' - '+objCatalog.CatalogId__c+' - '+objCatalog.Catalog_Name__c,'HEP_Catalog_Details?catId='+objCatalog.id,'Catalog',sCatalogOrder));
            }
        } 
        if(lstSKU != NULL && lstSKU.size() > 0){
            for(HEP_SKU_Master__c objSKU : lstSKU){
                lstAllResults.add(new SearchWrapper(objSKU.SKU_Title__c+'/--/'+objSKU.SKU_Number__c,objSKU.Territory__r.Name+' - '+objSKU.SKU_Title__c+' - '+objSKU.Catalog_Master__r.Name,'HEP_SKU_Details?skuId='+objSKU.id,'SKU',sSKUOrder));
            }
        }
        
        return lstAllResults;
        
    }
    
    
    
        
    /*******************************************************************************************************************************************
     Murali -   This is a static function that gets the menu access from custom settings LOC_Menu_Access__c for that user.
      THis is then split by , and puts it into a map of menu/sub menu and the list of meta data menu item for name 
    ****************************************************************************************************************************************/  
    public static Map<String,List<String>> getUserMenuAccess()  {
        //
        String permissionName = '';
        List<String> lstPermissions = new List<String>();
        List <HEP_Menu_Access__mdt> lstMenuAccess;
        for(PermissionSetAssignment tempPermission : [SELECT Id, Assignee.Name, PermissionSet.Name FROM PermissionSetAssignment where                                                                                           AssigneeId= :UserInfo.getUserId()])
        {           
            lstPermissions.add(tempPermission.PermissionSet.Name);
        }
        permissionName = permissionName.replace('LOC ','');
        
        Set<String> setMenu = new set<String>();
        Set<String> setMenuGroup = new set<String>();
        Set<String> setMenuSubGroup = new set<String>();
        Set<String> setMenuStandalone = new set<String>();
        Map<String,List<String>> mapMenuForFilter = new Map<String,List<String>>();
        if(lstPermissions.size()>0){
            for (HEP_Menu_Access__mdt tempMenuAccess : [Select DeveloperName,Grouping__c,Menu_Item__c,Menu_Standalone__c,Menu_Sub_Grouping__c       from HEP_Menu_Access__mdt where DeveloperName = :lstPermissions]){
                if(!String.isBlank(tempMenuAccess.Menu_Item__c))
                    setMenu.addAll(tempMenuAccess.Menu_Item__c.split(','));
                if(!String.isBlank(tempMenuAccess.Grouping__c))
                    setMenuGroup.addAll(tempMenuAccess.Grouping__c.split(','));
                if(!String.isBlank(tempMenuAccess.Menu_Sub_Grouping__c))
                    setMenuSubGroup.addAll(tempMenuAccess.Menu_Sub_Grouping__c.split(','));
                if(!String.isBlank(tempMenuAccess.Menu_Standalone__c))
                    setMenuStandalone.addAll(tempMenuAccess.Menu_Standalone__c.split(','));
            }
        }
        mapMenuForFilter.put('Menu',new List<String>(setMenu));
        mapMenuForFilter.put('Group',new List<String>(setMenuGroup));
        mapMenuForFilter.put('SubGroup',new List<String>(setMenuSubGroup));
        mapMenuForFilter.put('Standalone',new List<String>(setMenuStandalone));
        
        return mapMenuForFilter;
    }   
    
    /*******************************************************************************************************************************************
    * Method Name: getNavData
    * Description: remoting function that is called on page load to return the JSON Data for page navComponent
    * Return Type: navWrapper containing nav menu and its child items
    ****************************************************************************************************************************************/
    @RemoteAction
    public static navWrapper getNavData(String sPageName, String sPageURL) {
        //String terr, String viewingTerr, 
        // saving the page URL
       // savePageHistory(sPageURL);
     //   if(''!=terr && null!=terr)
    //        territory = terr;
        
     //   if(''!=viewingTerr && null!=viewingTerr)
     //       viewingTerritory = viewingTerr;
        
        if(''!=sPageName && null!=sPageName)
            sPage = sPageName;
        // load URL params based on user defaults
           
        
        // adding the page to Recents
        //changeRecentsAndFavorites('Recent', sPageName,'add');
        // getting the list of Favorites and Recent pages
        //Map<String,set<String>> mapRecFav = getRecentsAndFavorites();
        // Get user Profile Access Filter
       // system.debug('mapRecFav' + mapRecFav);
        Map<String,List<String>> mapMenuAccess = getUserMenuAccess();

        system.debug('mapMenuAccess' + mapMenuAccess);

        getParamMap(mapMenuAccess); 
        // gets the list of menu items, me items chlid based on user's access 
        List<HEP_Menu_Item__mdt> lstMenuItems = [Select Id,    MasterLabel, Order__c, Column_Size__c,Hide_Child_Menu__c
                                                    from HEP_Menu_Item__mdt where DeveloperName=: mapMenuAccess.get('Menu') 
                                                    order by order__c ];
                                                    
        List<HEP_Menu_Item_Child__mdt> lstChildItems = [Select MasterLabel,Description__c,Order__c,Parent__c,DeveloperName,Type__c,URL_Param_Type__c,VF_Page__c,HEP_Menu_Item__c,Column_Size__c,Number_of_Records__c 
                                                            from HEP_Menu_Item_Child__mdt where Parent__c!='NA' and
                                                            (DeveloperName=: mapMenuAccess.get('Standalone') and Type__c ='Standalone') order by Type__c];                                                          
        List<HEP_Menu_Item_Child__mdt> tempChildItems = [Select MasterLabel,Description__c,Order__c,Parent__c,DeveloperName,Type__c,URL_Param_Type__c,VF_Page__c,HEP_Menu_Item__c,Column_Size__c,Number_of_Records__c 
                                                            from HEP_Menu_Item_Child__mdt where Parent__c!='NA' and                                                            
                                                            (DeveloperName=: mapMenuAccess.get('Group') and Type__c ='Grouping') order by Type__c];                 
        if(tempChildItems.size()>0)
            lstChildItems.addAll(tempChildItems);                               
        tempChildItems = [Select MasterLabel,Description__c,Order__c,Parent__c,DeveloperName,Type__c,URL_Param_Type__c,VF_Page__c,HEP_Menu_Item__c,Column_Size__c,Number_of_Records__c 
                                                            from HEP_Menu_Item_Child__mdt where Parent__c!='NA' and
                                                            (DeveloperName=: mapMenuAccess.get('SubGroup') and Type__c ='Sub-Grouping'
                                                            and Parent__c =: mapMenuAccess.get('Group') ) order by Type__c];
        if(tempChildItems.size()>0)
            lstChildItems.addAll(tempChildItems);   
        Map<Id, List<HEP_Menu_Item_Child__mdt>> mapMenuNChildItems = new Map<Id, List<HEP_Menu_Item_Child__mdt>>();
        List<HEP_Menu_Item_Child__mdt> lstChildItem;

        system.debug('lstChildItems' + lstChildItems);

        
        // loop adds menu item and its child items in a map
        for(HEP_Menu_Item_Child__mdt childItem : lstChildItems){
            if(mapMenuNChildItems.containsKey(childItem.HEP_Menu_Item__c))
                lstChildItem = mapMenuNChildItems.get(childItem.HEP_Menu_Item__c);          
            else
                lstChildItem = new List<HEP_Menu_Item_Child__mdt>();
            lstChildItem.add(childItem);
            mapMenuNChildItems.put(childItem.HEP_Menu_Item__c, lstChildItem);
        }
        system.debug('mapMenuNChildItems:'+mapMenuNChildItems);
        map<string, navGroup> mapGroupMenu;
        List<navRow> lstStandAloneMenu, lstRecent, lstFav;
        navGroup  tempNavGroup;
        navWrapper navWrapperInst = new navWrapper();
        navWrapperInst.selectedPage = selectObj;
        system.debug('lstMenuItems'+lstMenuItems);
        lstRecent = new List<navRow>(); 
        lstFav = new List<navRow>();
        // loops through menu items to add to wrapper
        for(HEP_Menu_Item__mdt menuRow : lstMenuItems){
            
            mapGroupMenu = new map<string, navGroup>();
            lstStandAloneMenu = new List<navRow>(); 
             
           // system.debug('menuRow.LOC_Menu_Item_Childs__r'+menuRow.LOC_Menu_Item_Childs__r); 
            if(mapMenuNChildItems.get(menuRow.Id)!=null){
                for(HEP_Menu_Item_Child__mdt menuChildRow : mapMenuNChildItems.get(menuRow.Id)){
                    Boolean fav = false;
                    
                    if(menuChildRow.Type__c=='Standalone')
                    {
                        lstStandAloneMenu.add(new navRow(menuChildRow.Order__c,menuChildRow.MasterLabel,returnURL(menuChildRow),menuChildRow.Description__c,fav,false));
                    } // if Group add to Map the group instance
                    else if (menuChildRow.Type__c=='Grouping')
                    {
                        mapGroupMenu.put(menuChildRow.DeveloperName, new navGroup(menuChildRow.Order__c,menuChildRow.MasterLabel,menuChildRow.Column_Size__c,menuChildRow.Number_of_Records__c));
                       
                    } // add to child node for sub group items
                    else if (menuChildRow.Type__c=='Sub-Grouping')
                    {   // in case missing as per the sort sequence the groupings are added to map before this
                        if(!mapGroupMenu.containsKey(menuChildRow.parent__c))
                            mapGroupMenu.put(menuChildRow.parent__c, new navGroup(1,menuChildRow.parent__c,menuChildRow.Column_Size__c,menuChildRow.Number_of_Records__c));                   
                        tempNavGroup = mapGroupMenu.get(menuChildRow.parent__c);
                        // adding boolean to differentiate between report and page
                        //if(menuChildRow.parent__c=='Folders')
                        //    tempNavGroup.rows.add(new navRow(menuChildRow.Order__c,menuChildRow.MasterLabel,returnURL(menuChildRow),menuChildRow.Description__c,fav,true));
                        //else
                            tempNavGroup.rows.add(new navRow(menuChildRow.Order__c,menuChildRow.MasterLabel,returnURL(menuChildRow),menuChildRow.Description__c,fav,false));
                        mapGroupMenu.put(menuChildRow.parent__c, tempNavGroup); 
                        system.debug('tempNavGroup'+tempNavGroup);
                    }
                }
            }
            system.debug('mapGroupMenu'+mapGroupMenu);
           
            navWrapperInst.lstMenuItems.add(new navMenuItem(menuRow.Hide_Child_Menu__c,menuRow.Order__c,menuRow.MasterLabel,mapGroupMenu.values(),lstStandAloneMenu,menuRow.Column_Size__c ));
        }
        
        return navWrapperInst;
    }
    
    

    /*******************************************************************************************************************************************
      Murali - method that creates the URL.
      It validates against the params if we have a saved data and then adds the URL parameters needed to URL
      accepts the menu item row and returns the url
    ****************************************************************************************************************************************/   
    public Static String returnURL(HEP_Menu_Item_Child__mdt menuChildRow ){
        String baseUrl = '/apex/';
       
        if(menuChildRow.URL_Param_Type__c == 'External URL')
            return menuChildRow.VF_Page__c;
        
        if(null != mapURLParam && mapURLParam.containsKey(menuChildRow.URL_Param_Type__c))
        {
           
            if(menuChildRow.URL_Param_Type__c == 'NA')
            {
                return baseUrl + menuChildRow.VF_Page__c + '?';
            }
            else if(menuChildRow.URL_Param_Type__c == 'Append URL Param')
            {
                return baseUrl + menuChildRow.VF_Page__c  + '?' + menuChildRow.Description__c;
            }
        }        
        return baseUrl + menuChildRow.VF_Page__c;        
    }
    
    
    /*******************************************************************************************************************************************
        Murali - method that creates the URL params needed for each type of data based on the page type
        called on the load data method to custruct default param map
    ****************************************************************************************************************************************/   

    public Static void getParamMap(Map<String,List<String>> mapMenuAccess){
        mapURLParam = new map<string,String>();     
        mapURLParam.put('NA','');
        mapURLParam.put('Append URL Param','');  

        selectObj = new selected();       
        List<HEP_Menu_Item_Child__mdt> lstChildItems = [Select Id, Order__c,MasterLabel,URL_Param_Type__c,HEP_Menu_Item__r.MasterLabel 
                                                from HEP_Menu_Item_Child__mdt where VF_Page__c like :sPage order by Order__c];
        if(lstChildItems.size()>0)
        {
            selectObj.name = lstChildItems[0].MasterLabel;
            selectObj.type = lstChildItems[0].URL_Param_Type__c;
            selectObj.menu = lstChildItems[0].HEP_Menu_Item__r.MasterLabel;     
        }
        system.debug('mapURLParam '+mapURLParam);
    }
    
    /*All wrappers for page goes here*/

    // wrapper that has all the data for nav comp 
    public class navWrapper {
        public List<navMenuItem> lstMenuItems  = new List<navMenuItem> ();
        public selected selectedPage;
        public List<navRow> reportItems;
        public List<navQuickLink> quickLinks;    
        public map<String, list<HEP_Utility.InactiveSelectOption>> mapInactiveSelectOptions = HEP_Utility.getInactiveSelectOptions(false);       
            
    }
    
    // wrapper for each menu item
    public class navMenuItem{
        String name;
        Decimal order;
        Decimal colSize;
        Boolean hide;
        List<navGroup> GroupMenu;
        List<navRow> StandAloneMenu;
       // public boolean isManagerUser;

        public navMenuItem(Boolean hide,Decimal order,String name,List<navGroup> GroupMenu,List<navRow> StandAloneMenu, Decimal colSize){        
            this.hide = hide;
            this.order = order;
            this.name = name;
            this.GroupMenu = GroupMenu;
            this.StandAloneMenu = StandAloneMenu;  
            this.colSize = colSize;


         //   isManagerUser = Localization_Utility.isLoggedInUserManager();           
        }
    }

    // selected page/current page wrapper
    public class selected{
        String name = '', type = '', territoryName = '', title = '', intlUlt = '', domUlt = '',menu='',viewTerritoryName='';                 
    }
    
    // user defaults wrapper
    public class UserDefaults{
        String sTerrId = '', sTitleId = '', sIntlUltId = '', sDomUltId = '';                
    }
    
    // navigation quick link wrapper
    public class navQuickLink{
        navRow quickLink;
        Decimal order;
        Boolean imp;      
        public navQuickLink(Boolean imp,Decimal order){
        //    this.quickLink = quickLink;
            this.order = order;
        //  this.quickLink.order = order;
            this.imp = imp;            
        }
    }
    
    // navigation group wrapper
    public class navGroup{
        String name;
        Decimal order;
        Decimal colSize;
        Decimal count;
        List<navRow> rows = new List<navRow>() ;        
        public navGroup(Decimal order,String name,Decimal colSize, Decimal count){
            this.order = order;
            this.name = name; 
            this.colSize = colSize;
            this.count = count;         
        }
    }
    
    // each menu item row standalone/ sub group
    public class navRow {
        Decimal order;
        String name;
        String Url;
        String description;
        Boolean isReport;
        boolean fav;
        public navRow(Decimal order,String name,String Url,String description, Boolean fav,Boolean isReport){
            this.order = order;
            this.name = name;
            this.Url = Url;
            this.description = description;   
            this.fav = fav;
            this.isReport = isReport;
        }
    }
    
  
    /**
    * Extracts all the notifications generated for the logged in user
    * @return an instance of NotificationData
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static HEP_Notifications_Controller.NotificationData getNotificationDetails(){
        return HEP_Notifications_Controller.getNotificationDetails();
    }   
    
    
    /**
    * Its used to get the page data
    * @param sRequestType to identify the type of approval
    * @return wrapper
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static HEP_My_Approvals_Panel_Controller.MyApprovalsData getPendingApprovalRecordsData(){
        return HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
    }
}