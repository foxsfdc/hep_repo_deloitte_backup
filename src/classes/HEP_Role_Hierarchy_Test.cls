/* 
 @HEP_Role_Hierarchy_Test---Test Class for Approval process.
 @author : Deloitte
 */

@isTest
public class HEP_Role_Hierarchy_Test{
    
  @testSetup
  public static void createTestUserSetup(){
    HEP_ApprovalProcess_Test.createTestUserSetup();
  }
  
  static TestMethod void  testUtilityMethodsForHierarchical(){
    
    HEP_ApprovalProcess_Test.testBudgetRequestApproved();
  }
  
  static TestMethod void  testMethodsForApprovalTypeHierarchy(){
  	
    Test.startTest();
   	//instantiate Role Hierarchy 
    HEP_Role_Hierarchy objroleHierarchy = new HEP_Role_Hierarchy();
	
	//HEP_Approval_Type__c objApprovalType = [Select Id from HEP_Approval_Type__c];
    //Generate Hierachy for the Requestor's Managers
	objroleHierarchy.GenerateRoleHierarchyforApprovalType('SPEND', HEP_Utility.getConstantValue('HEP_REPORTEE_HIERARCHY'));
	objroleHierarchy.GenerateRoleHierarchyforApprovalType('SPEND', HEP_Utility.getConstantValue('HEP_MANAGER_HIERARCHY'));
    Test.stopTest();
   
  }
  
   static TestMethod void  testMethodsForReportee(){
    
   Test.startTest();
   	//instantiate Role Hierarchy 
    HEP_Role_Hierarchy objroleHierarchy = new HEP_Role_Hierarchy();
	
	HEP_Hierarchy_Type__c objHierarchyType = [Select Id from HEP_Hierarchy_Type__c];
    //Generate Hierachy for the Requestor's Managers
	objroleHierarchy.GenerateRoleHierarchy(objHierarchyType.Id, HEP_Utility.getConstantValue('HEP_REPORTEE_HIERARCHY'));
    //fetch All manager Roles for Requestor
    //fetch all roles Under the LOB received as Input
    List<String> lstRolesId = new List<String>();
	for(HEP_User_Role__c objUserRole: [Select 	Id,
												Role__c,
												Role__r.LOB__c,
												Search_Id__c,
												Territory__c,
												User__c
												from 
												HEP_User_Role__c
												]){
		
		lstRolesId.add(objUserRole.Role__c);
	}           
    objroleHierarchy.GetRoleHierarchyList(lstRolesId,  HEP_Utility.getConstantValue('HEP_REPORTEE_HIERARCHY'));
    Test.stopTest();
  }   
}