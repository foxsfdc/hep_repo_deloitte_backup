@isTest
public class HEP_Siebel_PromotionSpendDetails_Test{
    @testSetup
    public static void createTestUserSetup(){
    //constant Data
    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
    List<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
    }
    public static testmethod void performTransaction_test(){
        //All HEP Constants
        //List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        insert objPromotion1;
        //Create GL Account 
        HEP_GL_Account__c objGLAcct = HEP_Test_Data_Setup_Utility.createGLAccount('tst gl','glacct','desc','grp','Active',false);
        insert objGLAcct;
        //Create Market Spend Record
        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('99',objPromotion1.Id,'Pending',false);
        insert objMarketSpend;
        //Create Market Spend Details Record
        HEP_Market_Spend_Detail__c objMarketSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objMarketSpend.Id,objGLAcct.Id,'Active',false);
        objMarketSpendDetail.Type__c = 'Budget Update';
        insert objMarketSpendDetail;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Siebel_Promotion_Spend','HEP_Siebel_PromotionSpendDetails',true,10,10,'Outbound-Push',true,true,false);
        objInterface.Record_Status__c = 'Active';
        insert objInterface;
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        Set<Id> setIds = new Set<Id>{objMarketSpendDetail.Id};
        test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(setIds),'','HEP_Siebel_Promotion_Spend');   
        test.stopTest();
    }
      
    public static testmethod void performTransaction_test1(){
        //All HEP Constants
        //List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        
        //Create Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        insert objPromotion1;
        //Create GL Account 
        HEP_GL_Account__c objGLAcct = HEP_Test_Data_Setup_Utility.createGLAccount('tst gl','glacct','desc','grp','Active',false);
        insert objGLAcct;
        //Create Market Spend Record
        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('99',objPromotion1.Id,'Pending',false);
        insert objMarketSpend;
        //Create Market Spend Details Record
        HEP_Market_Spend_Detail__c objMarketSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objMarketSpend.Id,objGLAcct.Id,'Active',false);
        objMarketSpendDetail.Type__c = 'Budget Update';
        insert objMarketSpendDetail;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Siebel_Promotion_Spend','HEP_Siebel_PromotionSpendDetails',true,10,10,'Outbound-Push',true,true,false);
        objInterface.Record_Status__c = 'Active';
        insert objInterface;
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        Set<Id> setIds = new Set<Id>{objMarketSpendDetail.Id};
        
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(setIds),'','HEP_Siebel_Promotion_Spend');   
        
    }  
}