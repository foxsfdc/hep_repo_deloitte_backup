/**
 * Batch Handler class to set the status of Promotion SKU as Locked if it has entered the locking period
 * @author    Ashutosh Arora
 */
public class HEP_Promotion_SKU_Batch_Handler {
    //Getting static values from HEP_Contants Custom Settings
    public static String sSKUOffset;
    public static String sInactive;
    public static String sACTIVE;
    public static String sLocked;
    public static String sApproved;
    public static String sCatalogLOB;
    public static String sTvLOB;
    public static String sNewReleaseLOB;
    public static String sNationalPromotion;
    public static String sUnlocked;
    public static String sDHE;
    static {
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sSKUOffset = HEP_Utility.getConstantValue('SKU_LOCK_OFFSET');
        sInactive = HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE');
        sLocked = HEP_Utility.getConstantValue('HEP_PROMOTION_SKU_STATUS_LOCKED');
        sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sCatalogLOB = HEP_Utility.getConstantValue('HEP_CATALOG');
        sTvLOB = HEP_Utility.getConstantValue('PROMOTION_LOB_TV');
        sNewReleaseLOB = HEP_Utility.getConstantValue('PROMOTION_LOB_NEW_RELEASE');
        sNationalPromotion = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        sUnlocked = HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED');
        sDHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
    }
    /**
     *lockPromotionSKU method - accepts the list of Unlocked and Active Promotion SKU's and set them as Locked/Inactive if they have entered the locking period -> Locked When Approved/Inactive when not Approved 
     * @param List of Promotion SKU's
     * @return No return value.
     */
    public static List < HEP_Promotion_SKU__c > lockPromotionSKU(Set<Id> setPrSKU) {
        System.debug('*****');
        //List to hold all records which will be locked in after 14 days.
        List < HEP_Promotion_SKU__c > lstRecordsToSendLockReminders = new List < HEP_Promotion_SKU__c > ();
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        String sHEP_E1_SpendDetail = HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'); 
        HEP_Interface__c objInterfaceRec = [SELECT id FROM HEP_Interface__c WHERE Name =: sHEP_E1_SpendDetail]; 
        map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRecord = new map<String,HEP_Interface_Transaction__c>();
        String sSerlizedWrap;
        try {
            //List to hold all records to update 
            List < HEP_Promotion_SKU__c > lstRecordsToUpdate = new List < HEP_Promotion_SKU__c > ();
            //Map to hold all records to update 
            Map < String,HEP_Promotion_SKU__c > mapRecordsToUpdate = new Map < String,HEP_Promotion_SKU__c > ();
            //Map of LOB Type, Map of LOV Parent Value and Offset Value
            Map < String, Map < String, String >> mapLOBTypeOffset = new Map < String, Map < String, String >> ();
            //List for Offset Values for Promotion SKU Locking from LOV
            List < HEP_List_Of_Values__c > lstSKUOffsetValues = [SELECT Name, Parent_Value__c, Record_Status__c, Type__c, Values__c
                FROM HEP_List_Of_Values__c
                WHERE Type__c =: sSKUOffset
                AND Record_Status__c =: sACTIVE
            ];
            System.debug('lstSKUOffsetValues : ' + lstSKUOffsetValues);
            for (HEP_List_Of_Values__c objOffsetValues: lstSKUOffsetValues) {
                if (mapLOBTypeOffset.containsKey(objOffsetValues.Name)) {
                    if (objOffsetValues.Parent_Value__c != null && objOffsetValues.Values__c != null) {
                        mapLOBTypeOffset.get(objOffsetValues.Name).put(objOffsetValues.Parent_Value__c, objOffsetValues.Values__c);
                    }
                } else {
                    if (objOffsetValues.Parent_Value__c != null && objOffsetValues.Values__c != null) {
                        mapLOBTypeOffset.put(objOffsetValues.Name, new Map < String, String > {
                            objOffsetValues.Parent_Value__c => objOffsetValues.Values__c
                        });
                    }
                }
            }
            System.debug('lstSKUOffsetValues : ' + mapLOBTypeOffset);
            //Map of Promotion SKU Id and respective Promotion SKU Record 
            Map < String, HEP_Promotion_SKU__c > mapSkuIdRecord = new Map < String, HEP_Promotion_SKU__c > ();
            //Map of Promotion SKU Id and respective Price Region Record 
            Map < Id, HEP_SKU_Price__c > mapPSKUIdDate = new Map < Id, HEP_SKU_Price__c > ();
            //Populate mapSkuIdRecord with Promotion SKU ID and it's record
            for (sObject objSKUPromo: [SELECT CreatedDate,Name,Approval_Status__c,Catalog_Territory__c,HEP_Promotion__c,SKU_Master__r.Media_Type__c,
                                    Locked_Status__c,Promotion_Catalog__c,Promotion_LOB__c,Record_Status__c,Release_Id__c,SKU_Master__c,Unique_Id__c,
                                    SKU_Master__r.Channel__c,SKU_Master__r.Territory__c,SKU_Master__r.Territory__r.Name,SKU_Master__r.Territory__r.Parent_Territory__r.Name
                                    FROM HEP_Promotion_SKU__c
                                    WHERE Promotion_Catalog__r.Promotion__r.Promotion_Type__c =:sNationalPromotion AND SKU_Master__r.Territory__r.Name =: sDHE AND Locked_Status__c =:sUnlocked
                                    AND Record_Status__c =:sActive AND Promotion_Catalog__r.Promotion__c IN: setPrSKU]) 
            {
                HEP_Promotion_SKU__c PromotionSKU = (HEP_Promotion_SKU__c) objSKUPromo;
                if (!mapSkuIdRecord.containsKey(PromotionSKU.Id)) {
                    mapSkuIdRecord.put(PromotionSKU.Id, PromotionSKU);
                }
            }
            System.debug('mapSkuIdRecord : ' + mapSkuIdRecord);
            //List of SKU Price Regions for Promotion SKU Records
            List < HEP_SKU_Price__c > lstSKUPrice = [SELECT Id, Promotion_Dating__r.Date__c, Promotion_SKU__c
                FROM HEP_SKU_Price__c
                WHERE Promotion_SKU__c IN: mapSkuIdRecord.keyset()
                AND Record_Status__c =: sACTIVE
                ORDER BY Promotion_Dating__r.Date__c ASC
            ];
            System.debug('lstSKUPrice : ' + lstSKUPrice);
            //Populate mapPSKUIdDate with Promotion SKU Id and respective Price Region record with earliest release date    
            for (HEP_SKU_Price__c objSKUPrice: lstSKUPrice) {
                if (!mapPSKUIdDate.containsKey(objSKUPrice.Promotion_SKU__c)) {
                    mapPSKUIdDate.put(objSKUPrice.Promotion_SKU__c, objSKUPrice);
                }
            }
            System.debug('mapPSKUIdDate : ' + mapPSKUIdDate);
            //Iterate over keyset of mapSkuIdRecord which has all the unlocked Promotion SKU's
            for (String objPromoSKUId: mapSkuIdRecord.keySet()) {
                HEP_Promotion_SKU__c objPromoSKU = new HEP_Promotion_SKU__c();
                HEP_SKU_Price__c objPriceRegion;
                //SKU Promotion Record for current iteration ID
                objPromoSKU = mapSkuIdRecord.get(objPromoSKUId);
                System.debug('objPromoSKU : ' + objPromoSKU);
                //Price Region record with earliest release date 
                if (mapPSKUIdDate.containsKey(objPromoSKUId)) {
                    objPriceRegion = mapPSKUIdDate.get(objPromoSKUId);
                }
                System.debug('objPriceRegion' +objPriceRegion);
                if (objPriceRegion != null && objPriceRegion.Promotion_Dating__r != null) {
                    //Check LOB Type for Promotion SKU Record
                    if (objPromoSKU.Promotion_LOB__c == sCatalogLOB) {
                        //If record is Unlocked and Approved, then lock the record
                        if (objPriceRegion.Promotion_Dating__r.Date__c != null && mapLOBTypeOffset.containsKey(objPromoSKU.Promotion_LOB__c)) {
                            if (mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.Promotion_LOB__c) != null) {
                                Integer idaysToLock = Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.Promotion_LOB__c)) + 14;
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.Promotion_LOB__c)) && objPromoSKU.Approval_Status__c == sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Locked_Status__c = sLocked;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                }
                                //If record is Unlocked but not Approved, then set the record status as Inactive
                                else if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.Promotion_LOB__c)) && objPromoSKU.Approval_Status__c != sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Record_Status__c = sInactive;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                }
                                //If record is Unlocked and Approved and will be locked in (offset+14) days, add the record in list.
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) == idaysToLock && objPromoSKU.Approval_Status__c == sApproved) {
                                    //Date dReleaseDate = objPriceRegion.Promotion_Dating__r.Date__c;
                                    Date dReleaseDate = Date.today();
                                    objPromoSKU.Locked_Date__c = dReleaseDate.addDays(14);
                                    lstRecordsToSendLockReminders.add(objPromoSKU);
                                }
                            }

                            //When LOB is New Release
                        }
                    } else if (objPromoSKU.Promotion_LOB__c == sNewReleaseLOB) {
                        if (objPriceRegion.Promotion_Dating__r.Date__c != null && mapLOBTypeOffset.containsKey(objPromoSKU.Promotion_LOB__c)) {
                            if (mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Territory__r.Name) != null) {
                                Integer idaysToLock = Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Territory__r.Name)) + 14;
                                //If record is Unlocked and Approved, then lock the record
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Territory__r.Name)) && objPromoSKU.Approval_Status__c == sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Locked_Status__c = sLocked;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                    //If record is Unlocked but not Approved, then set the record status as Inactive    
                                } else if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Territory__r.Name)) && objPromoSKU.Approval_Status__c != sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Record_Status__c = sInactive;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                }
                                //If record is Unlocked and Approved and will be locked in (offset+14) days, add the record in list.
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) == idaysToLock && objPromoSKU.Approval_Status__c == sApproved) {
                                    //Date dReleaseDate = objPriceRegion.Promotion_Dating__r.Date__c;
                                    Date dReleaseDate = Date.today();
                                    objPromoSKU.Locked_Date__c = dReleaseDate.addDays(14);
                                    lstRecordsToSendLockReminders.add(objPromoSKU);
                                }
                            }
                        }
                        //If LOB is TV
                    } else if (objPromoSKU.Promotion_LOB__c == sTvLOB) {
                        if (objPriceRegion.Promotion_Dating__r.Date__c != null && mapLOBTypeOffset.containsKey(objPromoSKU.Promotion_LOB__c)) {
                            if (mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Media_Type__c) != null) {
                                Integer idaysToLock = Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Media_Type__c)) + 14;
                                //If record is Unlocked and Approved, then lock the record
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Media_Type__c)) && objPromoSKU.Approval_Status__c == sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Locked_Status__c = sLocked;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                    //If record is Unlocked but not Approved, then set the record status as Inactive    
                                } else if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) <= Integer.valueOf(mapLOBTypeOffset.get(objPromoSKU.Promotion_LOB__c).get(objPromoSKU.SKU_Master__r.Media_Type__c)) && objPromoSKU.Approval_Status__c != sApproved) {
                                    if (System.isBatch()) {
                                        objPromoSKU.Record_Status__c = sInactive;
                                    }
                                    //lstRecordsToUpdate.add(objPromoSKU);
                                    mapRecordsToUpdate.put(objPromoSKU.id,objPromoSKU);
                                }
                                //If record is Unlocked and Approved and will be locked in (offset+14) days, add the record in list.
                                if (Date.today().daysBetween(objPriceRegion.Promotion_Dating__r.Date__c) == idaysToLock && objPromoSKU.Approval_Status__c == sApproved) {
                                    //Date dReleaseDate = objPriceRegion.Promotion_Dating__r.Date__c;
                                    Date dReleaseDate = Date.today();
                                    objPromoSKU.Locked_Date__c = dReleaseDate.addDays(14);
                                    lstRecordsToSendLockReminders.add(objPromoSKU);
                                }
                            }
                        }
                    }
                }
            }
            if (lstRecordsToSendLockReminders != null && !lstRecordsToSendLockReminders.isEmpty()) {
                system.debug('Record for which reminder needs to be sent :' + lstRecordsToSendLockReminders);
                update lstRecordsToSendLockReminders;
            }
            if (!mapRecordsToUpdate.isEmpty()) {
                //Update the List of Records which are within Locking Period    
                System.debug('Record to Update : ' + mapRecordsToUpdate.values());
                update mapRecordsToUpdate.values();
                //Changes for E1 with the Interface transaction records Start 
                sSerlizedWrap = '';
                String setPromoId = (new list<Id>(setPrSKU))[0];
                HEP_MarketSpendTriggerHandler.E1Wrapper objE1Wrapper = new HEP_MarketSpendTriggerHandler.E1Wrapper();
                if(setPromoId != NULL && setPromoId != '' && objInterfaceRec.Id != NULL){
                    //HEP_MarketSpendTriggerHandler.E1Wrapper objE1Wrapper = new HEP_MarketSpendTriggerHandler.E1Wrapper();
                    HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
                    objTxn.Object_Id__c = (new list<Id>(setPrSKU))[0];
                    objTxn.HEP_Interface__c = objInterfaceRec.Id;
                    objTxn.Transaction_Datetime__c = System.now();
                    objTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
                    mapPromoIdInterfaceRecord.put((new list<Id>(setPrSKU))[0],objTxn);
                    insert mapPromoIdInterfaceRecord.Values();
                    if(mapPromoIdInterfaceRecord.containsKey((new list<Id>(setPrSKU))[0])){
                        objE1Wrapper.sPromoId = (new list<Id>(setPrSKU))[0];
                        objE1Wrapper.sTransactionId = mapPromoIdInterfaceRecord.get((new list<Id>(setPrSKU))[0]).Id;
                    }
                    sSerlizedWrap = JSON.serialize(objE1Wrapper);
                }    
                //Changes for E1 with the Interface transaction records End
                //The Siebel queueable is chained inside the E1 spend as E1 handles only 1 promotion at a time
                if(objE1Wrapper.sPromoId != NULL && objE1Wrapper.sTransactionId != NULL)
                    System.enqueueJob(new HEP_E1_Queueable(sSerlizedWrap,HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'),new List < String > (mapRecordsToUpdate.keyset())));             
            
            }
        } catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured In SKU Batch Handler', 'DML Errors', objException, 'HEP_Promotion_SKU_Batch_Handler', 'lockPromotionSKU', null, null);
        }
        return lstRecordsToSendLockReminders;
    }

    public static void sendReminderEmail(List < HEP_Promotion_SKU__c > lstRecordsToSendLockReminders) {
        //Send Email to Local Marketing Manager.                   
        List < String > lstEmailAddresses = new List < String > ();
        List < Id > lstUserId = new List < Id > ();
        Set < Id > setUserId = new Set < Id > ();
        Set < Id > setPromotionSKUId = new set < Id > ();
        for (HEP_Promotion_SKU__c objPromoSKU: lstRecordsToSendLockReminders) {
            setPromotionSKUId.add(objPromoSKU.Id);
        }
        List < HEP_Promotion_SKU__c > lstPromotionSKU = [Select Id, Locked_Date__c, Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Email,
            Promotion_Catalog__r.Promotion__r.International_Marketing_Manager__r.Email,
            Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__c,
            Promotion_Catalog__r.Promotion__r.International_Marketing_Manager__c,
            Promotion_Catalog__r.Promotion__r.Territory__r.Name
            FROM HEP_Promotion_SKU__c
            WHERE Id IN: setPromotionSKUId
        ];
        if (!lstPromotionSKU.isEmpty()) {

            //Create Outbound Email record before sending the Email.

            List < HEP_Outbound_Email__c > lstOutboundEmailsToInsert = new List < HEP_Outbound_Email__c > ();
            for (HEP_Promotion_SKU__c objPrSKU: lstPromotionSKU) {
                HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c();
                objOutboundEmail.To_Email_Address__c = objPrSKU.Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Email;
                objOutboundEmail.Record_Id__c = objPrSKU.Id;
                objOutboundEmail.Object_API__c = objPrSKU.Id.getSObjectType().getDescribe().getName();
                objOutboundEmail.Email_Template_Name__c = 'HEP_SKU_Lock_Email_Reminder';

                //Fetch user need to kept in Email CC for the Territory: This will be coming from LOV Mapping 
                String sUsersInEMailCC='';
                String sTemplateDetails = 'HEP_SKU_Lock_Email_Reminder' + objPrSKU.Promotion_Catalog__r.Promotion__r.Territory__r.Name;
                for (HEP_List_Of_Values__c objTempLstRec: [Select Values__c, Name_And_Parent__c from HEP_List_Of_Values__c where Name_And_Parent__c =: sTemplateDetails AND Type__c = 'EMAIL_CC_USERS' And Record_Status__c =: sACTIVE]) {
                    if(sUsersInEMailCC=='')
                        sUsersInEMailCC = objTempLstRec.Values__c;
                    else    
                        sUsersInEMailCC = sUsersInEMailCC+ + ' ; ' + objTempLstRec.Values__c;
                }
                if (String.isNotBlank(sUsersInEMailCC)) {           
                    objOutboundEmail.CC_Email_Address__c = sUsersInEMailCC;
                }
                //Add in List
                lstOutboundEmailsToInsert.add(objOutboundEmail);
            }

            if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                insert lstOutboundEmailsToInsert;
                system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
                List < Id > lstOutboundEmails = new List < Id > ();
                for (HEP_Outbound_Email__c objEmail: lstOutboundEmailsToInsert) {
                    lstOutboundEmails.add(objEmail.Id);
                }
                HEP_Send_Emails.sendOutboundEmails(lstOutboundEmails);
            }
        }
        //Create Notification record before creating notification user action record
        List < HEP_Notification_User_Action__c > lstNotificationUsers = new List < HEP_Notification_User_Action__c > ();
        Map < String, HEP_Notification__c > mapNotifications = new Map < String, HEP_Notification__c > ();
        String sNotifName = HEP_Utility.getConstantValue('HEP_Product_Lock_Alert');
        ID nType = lstPromotionSKU[0].id;
        String sNotifType = nType.getSObjectType().getDescribe().getName();
        system.debug('>>>>>>>>>' + sNotifType);
        HEP_Notification_Template__c objHepNotifTemplate = HEP_Notification_Utility.getTemplate(sNotifName, sNotifType);
        String sNotifBody = objHepNotifTemplate.HEP_Body__c;
        for (HEP_Promotion_SKU__c objHPD: lstPromotionSKU) {
            // Create Notification Records              
            HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
            objHepNewNotif.HEP_Message__c = HEP_Notification_Utility.CreateNotificationBody(objHPD, sNotifBody);
            objHepNewNotif.HEP_Template_Name__c = sNotifName;
            objHepNewNotif.HEP_Object_API__c = objHepNotifTemplate.HEP_Object__c;
            objHepNewNotif.Notification_Template__c = objHepNotifTemplate.id;
            objHepNewNotif.HEP_Record_ID__c = objHPD.id;
            mapNotifications.put(objHPD.id,objHepNewNotif);
        }
        if (mapNotifications != NULL && !mapNotifications.isEmpty()) {
            System.debug('Map-->' + mapNotifications);
            System.debug('Maps Keyset is-->' + mapNotifications.keySet());
            insert mapNotifications.values();
            
            for (HEP_Promotion_SKU__c objHPD: lstPromotionSKU) {
                if (mapNotifications.containsKey(objHPD.id)) {
                    String sParentRecordId = mapNotifications.get(objHPD.id).Id;  
                    system.debug('------------>' + sParentRecordId);                                                      
                    HEP_Notification_User_Action__c objNotificationUsers = new HEP_Notification_User_Action__c();
                    objNotificationUsers.Notification_User__c = objHPD.Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__c;
                    objNotificationUsers.HEP_Notification__c = sParentRecordId;
                    lstNotificationUsers.add(objNotificationUsers);                                                      
                }
            }
            if (lstNotificationUsers.size() > 0) {
                insert lstNotificationUsers;
                system.debug('lstNotificationAlerts----> ' + lstNotificationUsers);
            }
        }
        
    }

}