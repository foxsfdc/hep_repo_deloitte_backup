@isTest
public class HEP_CustomerPromotionApproval_Test {

	@isTest
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    List<HEP_List_Of_Values__c> lstLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;
	    HEP_Role__c objRoleMDPMarketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager - MDP FILM','', false);
	    objRoleMDPMarketingManager.Destination_User__c = u.Id;
	    objRoleMDPMarketingManager.Source_User__c = u.Id;
	    insert objRoleMDPMarketingManager;
	    HEP_Role__c objRoleMDPMarketingExecutive = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Executive - MDP FILM','', false);
	    objRoleMDPMarketingManager.Destination_User__c = u.Id;
	    objRoleMDPMarketingManager.Source_User__c = u.Id;
	    insert objRoleMDPMarketingExecutive;

	    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);

    	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

    	HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,false);
        objPromotion.Domestic_Marketing_Manager__c = u.id;
        insert objPromotion;
        HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

    	EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature','FEATR', true);
	    EDM_GLOBAL_TITLE__c objTitleEDM = HEP_Test_Data_Setup_Utility.createTitleEDM('Test', '12345', 'PUBLC', objProductType.id, true);

	    HEP_MDP_Promotion_Product__c objMDPProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objNationalPromotion.Id, objTitleEDM.id, objNationalTerritory.Id, true);

	    HEP_MDP_Promotion_Product_Detail__c objMDPPRoductDetail = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objMDPProduct.Id,'HD','EST',false);
	    objMDPPRoductDetail.Record_Status__c = 'Active';
	    insert objMDPPRoductDetail;

	    HEP_User_Role__c objUserRoleMDPNational = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleMDPMarketingManager.id, u.id, false);
    	objUserRoleMDPNational.Search_Id__c = objRoleMDPMarketingManager.id+' / '+objNationalTerritory.Id;
    	insert objUserRoleMDPNational;
    	HEP_User_Role__c objUserRoleMDPCustomer = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleMDPMarketingExecutive.id, u.id, false);
    	objUserRoleMDPCustomer.Search_Id__c = objRoleMDPMarketingExecutive.id+' / '+objNationalTerritory.Id;
    	insert objUserRoleMDPCustomer;

    	HEP_Approval_Type__c objApprovalTypeMDPNational = HEP_Test_Data_Setup_Utility.createApprovalType('DHE MDP National', 'HEP_MDP_Promotion_Product__c', 'Approval_Status__c', 'Approved','Rejected', 'MDP National','Hierarchical', 'Submitted', true);
	    HEP_Approval_Type__c objApprovalTypeMDPCustomer = HEP_Test_Data_Setup_Utility.createApprovalType('DHE MDP Customer', 'HEP_MDP_Promotion_Product__c', 'Approval_Status__c', 'Approved','Rejected', 'MDP Customer','Hierarchical', 'Submitted', true);

	    HEP_Approvals__c objApprovalMDPNational = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeMDPNational.id, null, false);
	    objApprovalMDPNational.HEP_MDP_Promotion_Product__c = objMDPProduct.id;
	    objApprovalMDPNational.Record_ID__c = objMDPProduct.id;
	    insert objApprovalMDPNational;
	    HEP_Approvals__c objApprovalMDPCustomer = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeMDPCustomer.id, null, true);
  		
  		HEP_Approval_Type_Role__c objApprovalTypeRoleMDPNational = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeMDPNational.id, objRoleMDPMarketingManager.id, 'SEQUENTIAL', true);
  		HEP_Approval_Type_Role__c objApprovalTypeRoleMDPCustomer = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeMDPCustomer.id, objRoleMDPMarketingExecutive.id, 'SEQUENTIAL', true);

  		HEP_Record_Approver__c objRecordApproverMDPNational = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalMDPNational.id, null, objRoleMDPMarketingManager.id, 'Pending', false);
    	objRecordApproverMDPNational.Search_Id__c = objRoleMDPMarketingManager.id+' / '+objNationalTerritory.Id;
    	insert objRecordApproverMDPNational;
    	HEP_Record_Approver__c objRecordApproverMDPCustomer = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalMDPCustomer.id, null, objRoleMDPMarketingExecutive.id, 'Pending', false);
    	objRecordApproverMDPCustomer.Search_Id__c = objRoleMDPMarketingExecutive.id+' / '+objNationalTerritory.Id;
    	insert objRecordApproverMDPCustomer;

    	System.runAs(u){
    		Test.startTest();
    		HEP_CustomerPromotionApprovalController.PageData objWrapperReturned = new HEP_CustomerPromotionApprovalController.PageData();
    		HEP_CustomerPromotionApprovalController.Promotion objApprovalWrapper = new HEP_CustomerPromotionApprovalController.Promotion();
    		objApprovalWrapper.sProductApprovalId = objRecordApproverMDPNational.id;

    		objWrapperReturned.lstPromotions.add(objApprovalWrapper);
    		String sJSON = JSON.serialize(objWrapperReturned);

    		HEP_CustomerPromotionApprovalController.loadPageData('National');
    		HEP_CustomerPromotionApprovalController.updateApprovalRecords(sJSON, true, '');
            Test.stopTest();
    	}
  	}

  	//@isTest
   // static void promotionApprovalsTest(){


   // }


}