/**
 * HEP_FMC_TitleImages -- class will be used to handle the Inbound integration FMC to HEP to create the TitLe Asset records as required.
 * @author  ---Lakshman Jinnuri----
 */
public without sharing class HEP_FMC_TitleImages implements HEP_IntegrationInterface {

    /**
     * performTransaction -- makes the callout and updates the response object
     * @param HEP_InterfaceTxnResponse object 
     * @return no return value
     * @author Lakshman Jinnuri    
     */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse) {
        String sParameters = '';
        String sAccessToken;
        String sPreviewId;
        String sThumbnailId;
        Boolean titlePresent = false;
        String sWPRId;
        list<String> lstWPRId = new list<String>();
        //HEP_Title_Asset__c ObjAsset = new HEP_Title_Asset__c();
        HEP_TitleImagesWrapper objWrapper;
        HEP_Services__c objServiceDetails;
        HEP_TitleImagesWrapper ObjWrapperJson;
        string emdAuthCreds;
        list<EDM_GLOBAL_TITLE__c > lstTitle;
        list<HEP_Title_Asset__c> lstTitleAsset;
        list<HEP_Title_Asset__c> lstTitleAsset_Del =  new list<HEP_Title_Asset__c>();
        list<HEP_Title_Asset__c> lstTitleAssetupdate =  new list<HEP_Title_Asset__c>();
        if(objTxnResponse.sRequest != null)
            ObjWrapperJson = (HEP_TitleImagesWrapper)JSON.deserialize(objTxnResponse.sRequest, HEP_TitleImagesWrapper.class);
        System.debug('ObjWrapperJson :'+ObjWrapperJson);
        
        //Deleting the Asset Where "STATUS": "DELETED"
        if(ObjWrapperJson.Data.Payload.event.STATUS .equalsIgnoreCase(HEP_Constants__c.getValues('HEP_RECORD_DELETED').Value__c)){
            if(!String.isEmpty(ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID)){
                lstTitleAsset_Del = [SELECT Id,Asset_Id__c,Asset_Name__c FROM HEP_Title_Asset__c where Asset_Id__c =: ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID];
            }
        }

            System.debug('lstTitleAsset_Del'+lstTitleAsset_Del);

        
        if(lstTitleAsset_Del.isEmpty()){
            //Gives the JSessionId and Accesstoken
            Map<string,string> sessionIdTokenId = HEP_getSessionIdAuthId();            
            System.debug('map received :'+sessionIdTokenId);
            if((HEP_Constants__c.getValues('HEP_SESSION_ID') != null && HEP_Constants__c.getValues('HEP_SESSION_ID').Value__c != null
                && HEP_Constants__c.getValues('HEP_ACCESS_TOKEN') != null && HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c != null 
                && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c != null  
                && sessionIdTokenId.get(HEP_Constants__c.getValues('HEP_SESSION_ID').Value__c) != null 
                && sessionIdTokenId.get(HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c) != null 
                && sessionIdTokenId.get(HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c).startsWith(HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c) 
                && objTxnResponse != null)){
                HTTPRequest objHttpRequest = new HTTPRequest();
                //Gets the Details from custom setting for Authentication
                HEP_Services__c objServiceDetailsApigeeAssets = HEP_Services__c.getInstance(HEP_Constants__c.getValues('HEP_FMC_APIGEE_ASSETS').Value__c);            
                //Acess token and JSessionId is passed to get the Preview Id and Thumbnail Id    
                if(objServiceDetailsApigeeAssets != null){
                    objHttpRequest.setEndpoint(objServiceDetailsApigeeAssets.Endpoint_URL__c + objServiceDetailsApigeeAssets.Service_URL__c + '/' + ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID+ '?load_type=full');
                    objHttpRequest.setMethod('GET');
                    objHttpRequest.setHeader('Authorization',sessionIdTokenId.get(HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c));
                    objHttpRequest.setHeader('Content-Type','application/json');
                    objHttpRequest.setHeader('JSESSIONID',sessionIdTokenId.get(HEP_Constants__c.getValues('HEP_SESSION_ID').Value__c));
                    objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                    System.debug('Request is :' + objHttpRequest);
                    objTxnResponse.sRequest = objTxnResponse.sRequest + '\n Body : \t' + objHttpRequest.getBody(); //request details
                    HTTP objHttp = new HTTP();
                    HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                    objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                    if(HEP_Constants__c.getValues('HEP_STATUS_OK') != null && HEP_Constants__c.getValues('HEP_STATUS_OK').Value__c != null){
                        if(objHttpResp.getStatus().equals(HEP_Constants__c.getValues('HEP_STATUS_OK').Value__c)){
                            JSONParser parser = JSON.createParser(objHttpResp.getBody());
                            boolean bThumbnailContent = false;
                            boolean bThumbnailURL = false;
                            boolean bPreviewContent = false;
                            boolean bPreviewURL = false;
                            boolean bWPRId = false;
                            boolean bWPRIdContent = false;
                            //for fetching the thumbnail and Preview Id from the response received
                            while (parser.nextToken() != null) {
                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_THUMBNAIL_CONTENT') != null && HEP_Constants__c.getValues('HEP_THUMBNAIL_CONTENT').Value__c != null 
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_THUMBNAIL_CONTENT').Value__c) && !bThumbnailContent && !bThumbnailURL) {
                                        bThumbnailContent = true;
                                        system.debug('FMC_getThumbnailURL parsing 1: ' + parser.getText());  
                                }
                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_ID') != null && HEP_Constants__c.getValues('HEP_ID').Value__c != null 
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_ID').Value__c) && bThumbnailContent && !bThumbnailURL) {
                                        parser.nextToken();
                                        system.debug('FMC_getThumbnailURL parsing 2: ' + parser.getText());
                                        sThumbnailId = parser.getText();
                                        bThumbnailURL = true;

                                }
                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_PREVIEW_CONTENT') != null && HEP_Constants__c.getValues('HEP_PREVIEW_CONTENT').Value__c != null
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_PREVIEW_CONTENT').Value__c) && !bPreviewContent && !bPreviewURL) {
                                        bPreviewContent = true;
                                        system.debug('FMC_getPreviewURL parsing 1: ' + parser.getText());   
                                }
                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_ID') != null && HEP_Constants__c.getValues('HEP_ID').Value__c != null 
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_ID').Value__c) && bPreviewContent && !bPreviewURL) {
                                        parser.nextToken();
                                        system.debug('FMC_getPreviewURL parsing 2: ' + parser.getText());
                                        sPreviewId = parser.getText();
                                        bPreviewURL = true;
                                }

                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_ID') != null && HEP_Constants__c.getValues('HEP_ID').Value__c != null
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_ID').Value__c) && !bWPRIdContent && !bWPRId) {
                                        System.debug('parser.getCurrentToken()'+parser.getCurrentToken());
                                        System.debug('In id check'+parser.nextToken());
                                        System.debug('parser.getText()'+parser.getText());
                                        //parser.nextToken();
                                        if((('FOX.FIELD.PRODUCT_FOXIPEDIA').equals(parser.getText()))){
                                            bWPRIdContent = true;
                                            system.debug('WPR Check: ' + parser.getText());   
                                        }
                                }
                                if ((parser.getCurrentToken().equals(JSONToken.FIELD_NAME)) 
                                    && HEP_Constants__c.getValues('HEP_VALUE') != null && HEP_Constants__c.getValues('HEP_VALUE').Value__c != null 
                                    && parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_VALUE').Value__c) && bWPRIdContent && !bWPRId) {
                                        parser.nextToken();
                                        if(parser.getText().containsIgnoreCase('P___')){
                                            system.debug('bWPRId parsing 2: ' + parser.getText());
                                            sWPRId = parser.getText().remove('P___');
                                            bWPRIdContent = true;
                                            bWPRId = true;
                                            System.debug('sWPRId :'+sWPRId);
                                        }
                                }

                                // Cheking wheather Response has "value": "Title Rep". if Present then titlePresent = True

                                if(parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_VALUE').Value__c) && (parser.getCurrentToken().equals(JSONToken.FIELD_NAME))){
                                    parser.nextToken();
                                    if(parser.getText().equalsIgnoreCase(HEP_Constants__c.getValues('HEP_TITLE_REP').Value__c)){
                                        titlePresent = true ;
                                    }    
                                }
                            }


                            // if "value": "Title Rep" is not present then Delete the HEP Title Asset 

                            if(titlePresent == false){
                                System.debug('Gotinside'+parser.getText());
                                lstTitleAsset_Del = [SELECT Id,Asset_Id__c,Asset_Name__c FROM HEP_Title_Asset__c where Asset_Id__c =: ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID];
                                System.debug('lstTitleAsset_Del'+lstTitleAsset_Del);
                                if(lstTitleAsset_Del != null && !lstTitleAsset_Del.isEmpty())
                                    delete lstTitleAsset_Del;
                                if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c != null)             
                                    objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c;
                                objTxnResponse.bRetry = false;    
                                return;
                            }
                            //Gets the details from Foxipedia interface and stores in the wrapper 
                            HEP_AssetWrapper objAssetWrapper = (HEP_AssetWrapper)JSON.deserialize(objHttpResp.getBody(), HEP_AssetWrapper.class);    
                            System.debug('objWrapper :' + objAssetWrapper );
                            if(objAssetWrapper.errors != null && !objAssetWrapper.errors.isEmpty() && String.isNotBlank(objAssetWrapper.errors[0].detail)){
                                System.debug('in failure with errors'+objAssetWrapper.errors[0].detail);
                                if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null)
                                    objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                                if(objAssetWrapper.errors[0].detail != null && objAssetWrapper.errors[0].detail.length()>254)
                                    objTxnResponse.sErrorMsg = objAssetWrapper.errors[0].detail.substring(0, 254);
                                else
                                    objTxnResponse.sErrorMsg = objAssetWrapper.errors[0].detail;
                                objTxnResponse.bRetry = true;
                            }else{
                                System.debug('in success with serialize');
                                if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c != null)             
                                    objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c;
                                objTxnResponse.bRetry = false;
                            }
                        }
                        else{
                            System.debug('in failure condition');
                            if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null)
                                objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                            if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                                objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                            else    
                                objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                            objTxnResponse.bRetry = true;
                        }
                    }        
                }
            }
            //if the token is not valid or if we get the invalid transaction response
            else{
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Constants__c.getValues('HEP_INVALID_TOKEN').Value__c != null 
                    && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Constants__c.getValues('HEP_FAILURE').Value__c != null ){
                    objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_FAILURE').Value__c;
                    objTxnResponse.sErrorMsg = HEP_Constants__c.getValues('HEP_INVALID_TOKEN').Value__c;
                    objTxnResponse.bRetry = true;
                }
            }


            if(sWPRId != null)
                lstWPRId.add(sWPRId);
            else 
                lstWPRId.add(ObjWrapperJson.Data.Payload.event.ASSET_INFO.WPR_ID);    
            //Query to get Title and Title Assets by means of Financial Product Id and Asset Id respectively 
            //if(ObjWrapperJson.Data.Payload.event.ASSET_INFO.WPR_ID != null && !String.isEmpty(ObjWrapperJson.Data.Payload.event.ASSET_INFO.WPR_ID))
            if(lstWPRId != null && lstWPRId.Size()>0)
                lstTitle = [SELECT Id,FIN_PROD_ID__c,LIFE_CYCL_STAT_GRP_CD__c  
                            FROM EDM_GLOBAL_TITLE__c
                            WHERE FIN_PROD_ID__c =: lstWPRId AND LIFE_CYCL_STAT_GRP_CD__c !=: HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL') ];
            if(ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID != null && !lstWPRId.isEmpty())
                lstTitleAsset = [SELECT Id,Asset_Id__c,Asset_Name__c,Media_Id__c,Preview_Id__c,Thumbnail_Id__c,Title_EDM__c,Title_EDM__r.LIFE_CYCL_STAT_GRP_CD__c
                                 FROM HEP_Title_Asset__c 
                                 WHERE Asset_Id__c =: ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID AND Title_EDM__r.LIFE_CYCL_STAT_GRP_CD__c !=: HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL')]; 
            map<Id,HEP_Title_Asset__c> mapTitleAsset = new map<Id,HEP_Title_Asset__c>(); 
            for(HEP_Title_Asset__c objAsset :lstTitleAsset ){
                mapTitleAsset.put(objAsset.Title_EDM__c,objAsset);
            }
            for(EDM_GLOBAL_TITLE__c objTitle:lstTitle){
                HEP_Title_Asset__c ObjAsset;
                System.debug('checking else condition');
                if(mapTitleAsset.containsKey(objTitle.Id))
                    ObjAsset = mapTitleAsset.get(objTitle.Id);
                else
                    ObjAsset = new HEP_Title_Asset__c();
                ObjAsset.Asset_Id__c = ObjWrapperJson.Data.Payload.event.ASSET_INFO.ASSET_ID;
                ObjAsset.Media_Id__c  = ObjWrapperJson.Data.Payload.event.ASSET_INFO.MEDIA_ID;
                ObjAsset.Asset_Name__c = ObjWrapperJson.Data.Payload.event.ASSET_INFO.FILE_NAME;
                if(!String.isEmpty(sPreviewId))
                    ObjAsset.Preview_Id__c = sPreviewId;
                if(!String.isEmpty(sThumbnailId))    
                    ObjAsset.Thumbnail_Id__c = sThumbnailId;
                if(lstTitle != null && !lstTitle.isEmpty())
                    ObjAsset.Title_EDM__c =  objTitle.Id;
                if(!String.isEmpty(sPreviewId) && !String.isEmpty(sThumbnailId))     
                    lstTitleAssetupdate.add(ObjAsset); 
                System.debug('checking List in else'+lstTitleAssetupdate);
            }   
             //Insert or Update the Asset Records based on Asset Id
             if(!lstTitleAssetupdate.isEmpty())
                upsert lstTitleAssetupdate;   
            System.debug('lstTitleAssetupdate'+lstTitleAssetupdate);    
        }     

         //Delete Asset Records 
         System.debug('lstTitleAsset_Del'+lstTitleAsset_Del);
         if(lstTitleAsset_Del != null && !lstTitleAsset_Del.isEmpty())
         {
             delete lstTitleAsset_Del;
         }
         
    }   
    /**
     * HEP_getSessionIdAuthId -- makes the callout and gets the JSessionId through response
     * @param  Takes no input 
     * @return Returns SessionId and AccessToken in a Map   
     * @author Lakshman Jinnuri      
     */
    public static Map<string,string> HEP_getSessionIdAuthId(){
        string sSessionId;
        string sAuthId;
        String sResult;
        String sAccessToken;
        HEP_Services__c objServiceDetails;
        Map<string,string> mapSessionIdAuthId = new Map<string,string>();
        string sHepAuthCreds;
        //Get The Token
        if(HEP_Constants__c.getValues('HEP_FMC_APIGEE_OAUTH') != null && HEP_Constants__c.getValues('HEP_FMC_APIGEE_OAUTH').Value__c != null)
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Constants__c.getValues('HEP_FMC_APIGEE_OAUTH').Value__c);
        System.debug('accesstoken..'+sAccessToken);
        //Verify if Token is received and the Proceed.
        if(sAccessToken != null 
           && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c != null 
           && sAccessToken.startsWith(HEP_Constants__c.getValues('HEP_TOKEN_BEARER').Value__c)){
            // Fetch the Service Details
            if(HEP_Constants__c.getValues('HEP_FMC_OAUTH') != null && HEP_Constants__c.getValues('HEP_FMC_OAUTH').Value__c != null)
                objServiceDetails = HEP_Services__c.getInstance(HEP_Constants__c.getValues('HEP_FMC_OAUTH').Value__c);
            System.debug('objServiceDetails..'+objServiceDetails);  
            if(null != objServiceDetails){
                sHepAuthCreds =  '{"username":"' + objServiceDetails.Username__c + '","password":"' + objServiceDetails.Password__c + '"}';
                System.debug('emdAuthCreds..'+sHepAuthCreds);
                //Make callout to get JSessionId
                HttpRequest objReq = new HttpRequest(); 
                objReq.setMethod('POST'); 
                objReq.setEndpoint(objServiceDetails.Endpoint_URL__c+objServiceDetails.Service_URL__c); 
                objReq.setHeader('Authorization',sAccessToken);
                objReq.setHeader('system',System.Label.HEP_SYSTEM_FMC);
                objReq.setHeader('Content-Type','application/json');
                objReq.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objReq.setBody(sHepAuthCreds);
                System.debug('req..'+objReq);
                Http http = new Http();
                HTTPResponse objRes = http.send(objReq);
                sResult = objRes.getbody();
                system.debug('HEP_getSessionIdAuthId result: ' + sResult);
                list<string> lstResponseKeys = objRes.getHeaderKeys();
                Map<string, string> headers = new map<string, string>();
                //Checks the JSessionId and Id from the HeaderKeys
                for(string sResponseKeys : lstResponseKeys){
                    if(sResponseKeys != null){
                        if(HEP_Constants__c.getValues('HEP_FMC_JSESSIONID') != null && HEP_Constants__c.getValues('HEP_FMC_JSESSIONID').Value__c != null)
                            if(sResponseKeys.equalsIgnoreCase(HEP_Constants__c.getValues('HEP_FMC_JSESSIONID').Value__c)){
                                sSessionId = objRes.getHeader(sResponseKeys);
                                System.debug('sessionId..'+sSessionId);
                            }
                        if(HEP_Constants__c.getValues('HEP_FMC_ID') != null && HEP_Constants__c.getValues('HEP_FMC_ID').Value__c != null)    
                            if(sResponseKeys.equalsIgnoreCase(HEP_Constants__c.getValues('HEP_FMC_ID').Value__c)){
                                sAuthId = objRes.getHeader(sResponseKeys);
                                System.debug('authId..'+sAuthId);
                            }
                    }   
                }
                if(HEP_Constants__c.getValues('HEP_SESSION_ID') != null && HEP_Constants__c.getValues('HEP_SESSION_ID').Value__c != null 
                    && HEP_Constants__c.getValues('HEP_ACCESS_TOKEN') != null && HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c != null){
                mapSessionIdAuthId.put(HEP_Constants__c.getValues('HEP_SESSION_ID').Value__c,sSessionId);
                mapSessionIdAuthId.put(HEP_Constants__c.getValues('HEP_ACCESS_TOKEN').Value__c,sAccessToken);
                }
            }
            System.debug('MapIds..'+mapSessionIdAuthId);
            return mapSessionIdAuthId;
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            return null;
        }
    }   
}