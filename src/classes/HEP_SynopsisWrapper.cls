/**
* HEP_SynopsisWrapper -- Wrapper to store Synopsis details 
* @author    Lakshman Jinnuri
*/
public with sharing class HEP_SynopsisWrapper{
    public list< Data > data{get;set;}
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public LinksOuter links{get;set;}    
    public list<Errors> errors { get; set; }
    
    /**
    * Data -- Class to hold Data details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Attributes attributes{get;set;}
        public String id{get;set;}  
        public String type{get;set;}  
        public Relationships relationships{get;set;}
    }

    /**
    * Attributes -- Class to hold Attribute details 
    * @author    Lakshman Jinnuri
    */
    public class Attributes {
        public String type{get;set;}    
        public String foxId{get;set;}   
        public String foxVersionId{get;set;}   
        public String synopsisId{get;set;}  
        public String rowIdObject{get;set;} 
        public String languageCode{get;set;}    
        public String languageDescription{get;set;} 
        public String countryCode{get;set;} 
        public String countryDescription{get;set;} 
        public String mediaCode{get;set;}   
        public String mediaDescription{get;set;}    
        public String synopsisTypeCode{get;set;}   
        public String synopsisTypeDescription{get;set;} 
        public String synopsisText{get;set;}    
        public String titleVersionTypeCode{get;set;}    
        public String titleVersionTypeDescription{get;set;} 
        public String titleVersionDescription{get;set;} 
    }

    /**
    * Relationships -- Class to hold Relationships details 
    * @author    Lakshman Jinnuri
    */
    public class Relationships {
        public Title title{get;set;}
    }

    /**
    * Title -- Class to hold links details 
    * @author    Lakshman Jinnuri
    */
    public class Title {
        public Links links{get;set;}
    }

    /**
    * Links -- Class to hold related details 
    * @author    Lakshman Jinnuri
    */
    public class Links {
        public String related{get;set;} 
    }

    /**
    * Links_Outer -- Wrapper to store Links details 
    * @author    Lakshman Jinnuri
    */
    public class LinksOuter {
        public String self {get;set;} 
    }    
    
    /**
    * Errors -- Class to hold related Error details 
    * @author     Lakshman Jinnuri
    */
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }
}