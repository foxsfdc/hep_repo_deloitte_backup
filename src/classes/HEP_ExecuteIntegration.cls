/**
* HEP_ExecuteIntegration --- Class to schedule the execution of integration class
* @author    Sachin Agarwal
*/
global class HEP_ExecuteIntegration{
    
    /**
    * retryTxn --- Method that is called on clicking retry button on interface txn detail page
    * @param sSourceId id of the record or the inbound request body
    * @param sInterfaceTxnId id of the interface transaction record
    * @param sInterfaceName name of the interface
    * @author    Sachin Agarwal
    */
    webservice static void retryTxn(String sSourceId, String sInterfaceTxnId, String sInterfaceName){
        
        executeIntegMethod(sSourceId, sInterfaceTxnId, sInterfaceName);
    }
    
    /**
    * executeIntegMethod --- To call the relevant class for integration
    * @param sSourceId id of the record or the inbound request body
    * @param sInterfaceTxnId id of the interface transaction record
    * @param sInterfaceName name of the interface
    * @author    Sachin Agarwal
    */
    
    public static HEP_InterfaceTxnResponse executeIntegMethod(String sSourceId, String sInterfaceTxnId, String sInterfaceName){
        
        String sClassName;
        String sInterfaceId;
        String sRequest;
        Integer iRetryInterval = 0;
        Integer iRetryLimit = 0;
        Integer iCurrentRetryCount = 0;
        Boolean bRetryFlag = false;
        Boolean bIsInboundTxn = false;
        Boolean bIsSourceAsAttachment = false;
        Boolean bIsCreateContentFile = false;
        
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        // If interface txn id is present, find out the required details
        if(!String.isEmpty(sInterfaceTxnId)){
            list<HEP_Interface_Transaction__c> lstInterfaceTxn;
            lstInterfaceTxn = [SELECT id, HEP_Interface__c, HEP_Interface__r.Class__c, HEP_Interface__r.Integration_Name__c, Retry_Count__c,
                                      HEP_Interface__r.Retry_Flag__c, HEP_Interface__r.Retry_Interval__c, HEP_Interface__r.Retry_Limit__c,
                                      HEP_Interface__r.Type__c,HEP_Interface__r.Store_Source_As_Attachment__c,HEP_Interface__r.Create_Content_File__c
                               FROM HEP_Interface_Transaction__c
                               WHERE id = :sInterfaceTxnId];
            
            // If we have a matching interface transaction record
            if(lstInterfaceTxn != null && !lstInterfaceTxn.isEmpty()){
                sClassName = lstInterfaceTxn[0].HEP_Interface__r.Class__c;
                sInterfaceName = lstInterfaceTxn[0].HEP_Interface__r.Integration_Name__c;
                sInterfaceId = lstInterfaceTxn[0].HEP_Interface__c;
                iRetryInterval = lstInterfaceTxn[0].HEP_Interface__r.Retry_Interval__c != null ? Integer.valueOf(lstInterfaceTxn[0].HEP_Interface__r.Retry_Interval__c) : 0;
                bRetryFlag = lstInterfaceTxn[0].HEP_Interface__r.Retry_Flag__c;
                iRetryLimit = lstInterfaceTxn[0].HEP_Interface__r.Retry_Limit__c != null ? Integer.valueOf(lstInterfaceTxn[0].HEP_Interface__r.Retry_Limit__c) : 0;
                iCurrentRetryCount = lstInterfaceTxn[0].Retry_Count__c != null ? Integer.valueOf(lstInterfaceTxn[0].Retry_Count__c) : 0;
                bIsInboundTxn = lstInterfaceTxn[0].HEP_Interface__r.Type__c != null && HEP_Integration_Util.getConstantValue('HEP_INBOUND_INTERFACE_TYPE') != null && lstInterfaceTxn[0].HEP_Interface__r.Type__c.containsIgnoreCase(HEP_Integration_Util.getConstantValue('HEP_INBOUND_INTERFACE_TYPE')) ? true : false;
                bIsSourceAsAttachment =  lstInterfaceTxn[0].HEP_Interface__r.Store_Source_As_Attachment__c;
                bIsCreateContentFile =   lstInterfaceTxn[0].HEP_Interface__r.Create_Content_File__c;

                // If its an inbound txn, extract the request else if its the outbound and the surce as attachment is true extract the source
                if(bIsInboundTxn){
                    sSourceId = contentDocumentSourceId(sInterfaceTxnId,bIsInboundTxn);
                }else if(!bIsInboundTxn && bIsSourceAsAttachment){
                    sSourceId = contentDocumentSourceId(sInterfaceTxnId,bIsInboundTxn);
                } 
            } 
        }
        // If interface txn id is not present but interface name is, find out the required details
        else if(!String.isEmpty(sInterfaceName)){
            list<HEP_Interface__c> lstInterface = new list<HEP_Interface__c>();
            lstInterface = [SELECT id, Integration_Name__c, Class__c, Retry_Flag__c, Retry_Interval__c, Retry_Limit__c, Type__c,Store_Source_As_Attachment__c,Create_Content_File__c
                               FROM HEP_Interface__c
                               WHERE Integration_Name__c = :sInterfaceName];
            
            // If we have a matching interface record
            if(!lstInterface.isEmpty()){
                sClassName = lstInterface[0].Class__c;
                sInterfaceName = lstInterface[0].Integration_Name__c;
                sInterfaceId = lstInterface[0].id;
                iRetryInterval = lstInterface[0].Retry_Interval__c != null ? Integer.valueOf(lstInterface[0].Retry_Interval__c) : 0;
                bRetryFlag = lstInterface[0].Retry_Flag__c;
                iRetryLimit = lstInterface[0].Retry_Limit__c != null ? Integer.valueOf(lstInterface[0].Retry_Limit__c) : 0; 
                bIsInboundTxn = lstInterface[0].Type__c != null && HEP_Integration_Util.getConstantValue('HEP_INBOUND_INTERFACE_TYPE') != null && lstInterface[0].Type__c.containsIgnoreCase(HEP_Integration_Util.getConstantValue('HEP_INBOUND_INTERFACE_TYPE')) ? true : false;
                bIsSourceAsAttachment = lstInterface[0].Store_Source_As_Attachment__c;
                bIsCreateContentFile = lstInterface[0].Create_Content_File__c;
            } 
        }
        objTxnResponse.bIsSourceAsAttachment = bIsSourceAsAttachment;
        objTxnResponse.bIsCreateContentFile = bIsCreateContentFile;
        objTxnResponse.bInboundTxn = bIsInboundTxn;
        objTxnResponse.sInterfaceName = sInterfaceName;
        objTxnResponse.sInterfaceTxnId = sInterfaceTxnId;
        objTxnResponse.sInterfaceId = sInterfaceId;
        if(!bIsInboundTxn){
            objTxnResponse.sSourceId = sSourceId;
        }
        else{
            objTxnResponse.sRequest = sSourceId;
        }       
    
        // If the retry is allowed on the txn
        if(bRetryFlag){
            objTxnResponse.iMaxRetriesAllowed = iRetryLimit;
        }
        else{
            objTxnResponse.iMaxRetriesAllowed = 0;
        }
        objTxnResponse.bIsSourceAsAttachment = bIsSourceAsAttachment;
        objTxnResponse.iCurrentNoOfRetries = iCurrentRetryCount;
        objTxnResponse.iRetryInterval = iRetryInterval;
        // If the class to be called is not found
        if(String.isEmpty(sClassName)){
            objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_INTEGRATION_CLASS_NOT_FOUND_ERROR');
            return objTxnResponse;
        }
        else if(String.isEmpty(sInterfaceId)){
            objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_INTERFACE_NOT_ACTIVE_ERROR');
            return objTxnResponse;
        }
        else if(!bIsInboundTxn && String.isEmpty(sSourceId)){
            objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_INTERFACE_SOURCE_ID_NOT_EMPTY');
            return objTxnResponse;
        }
        else if(bIsInboundTxn == true && String.isEmpty(objTxnResponse.sRequest)){
            objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_INTERFACE_REQUEST_ID_NOT_EMPTY');
            return objTxnResponse;
        }
        
                
        Type objTargetClassType = Type.forName(sClassName);
        
        Exception objExceptionRaised;
        
        // If the class to be called is not a valid class       
        if(objTargetClassType == null){
            objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_INTEGRATION_CLASS_NOT_FOUND_ERROR');    
        }         
        else{
            try{       
                // Calling out the method responsible for carrying out the integration logic    
                HEP_IntegrationInterface objTargetClass = (HEP_IntegrationInterface)objTargetClassType.newInstance();
                objTargetClass.performTransaction(objTxnResponse);
            }
            catch(Exception ex){                
                objExceptionRaised = ex;
            }
            finally{
                // If its an exception case in the called class
                if(objExceptionRaised != null){
                    
                    objTxnResponse.sErrorMsg = objExceptionRaised.getMessage();
                    objTxnResponse.sStatus = HEP_Integration_Util.getConstantValue('HEP_FAILURE');
                    
                    HEP_Integration_Util.createInterfaceTxn(objTxnResponse);
                    
                    // To do: Create txn record and the error record
                    throw objExceptionRaised;
                }
                else{
                    HEP_Integration_Util.createInterfaceTxn(objTxnResponse);
                }
            }            
        }
        
        return objTxnResponse;
    }

    public static String contentDocumentSourceId(String sInterfaceTxnId,Boolean bIsInboundTxn){
        String sSourceId = null;
        String sTitle;
        list<ContentDocumentLink> lstContentDocumentLinks;
        lstContentDocumentLinks = [SELECT ID, ContentDocumentId
                                   FROM ContentDocumentLink
                                   WHERE LinkedEntityId = :sInterfaceTxnId];
        if(lstContentDocumentLinks != null && !lstContentDocumentLinks.isEmpty()){ 
                        
            set<id> setContentDocumentIds = new set<id>();
            for(ContentDocumentLink objContentDocumentLink : lstContentDocumentLinks){ 
                setContentDocumentIds.add(objContentDocumentLink.ContentDocumentId);
            }
            
            list<ContentVersion> lstContentVersion = new list<ContentVersion>();
            if(bIsInboundTxn)
                sTitle = HEP_Integration_Util.getConstantValue('HEP_INTEGRATION_REQUEST');
            else
                sTitle = HEP_Integration_Util.getConstantValue('HEP_INTEGRATION_SOURCE');  
            lstContentVersion = [SELECT id, VersionData, Title
                                 FROM ContentVersion
                                 WHERE ContentDocumentId IN :setContentDocumentIds AND Title = :sTitle order by createddate]; 
            
            if(!lstContentVersion.isEmpty()){
                sSourceId = lstContentVersion[0].VersionData != null ? EncodingUtil.base64Decode(EncodingUtil.base64Encode(lstContentVersion[0].VersionData)).toString()  : null;
            }
        }
        return sSourceId;       
    }

}