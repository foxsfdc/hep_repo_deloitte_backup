/**
* HEP_FAD_Override_Approval_Process --- This class drives the end to end logic for FAD override. 
                                        The request sent sends the request and and push values to notification and the
                                        response handler methods handles the response based on approval and rejection.
* @author    Abhishek Mishra
*/
public class HEP_FAD_Override_Approval_Process {
    Public static HEP_Promotion__c Promotion_Record;
    Public static String Query;
    public static List<string> allApprovalProcessByPassedTerritories;
    public static List<HEP_Promotion_Dating__c> AllDatings;
    Public static ResponseWrapper Res_Wrapper;
    public static String sActive;
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
   /**
    * Initiate the FAD override approval process
    * @param  Promotion Object Id, all related dating records for with FAD indicator is true
    * @return Response wrapper
    * @author Abhishek Mishra
    */
    public static ResponseWrapper initiateFADApprovalProcess(string promotionId, List<HEP_Promotion_Dating__c> lstPromotionDates){
        Res_Wrapper = new ResponseWrapper();
        try{
            System.debug('I am here 1');
            system.debug('lstPromotionDates : ' + lstPromotionDates);
            //Quering Promotion Details to check the bypass territory logic
            Query = HEP_Utility.buildQueryAllString('HEP_Promotion__c','Select Territory__r.Name, Territory__r.id, Global_Promotion__r.FirstAvailableDate__c, ',' WHERE    Record_Status__c=:sActive AND Id = \''+PromotionId + '\'');
            Promotion_Record = Database.Query(Query);
            if(string.isNotBlank(System.Label.HEP_FAD_OVERRIDE_BYPASS_TERRITORY))
                allApprovalProcessByPassedTerritories = System.Label.HEP_FAD_OVERRIDE_BYPASS_TERRITORY.split(',');
            if(!lstPromotionDates.isEmpty()){
                System.debug('I am here 2');
                if(allApprovalProcessByPassedTerritories.contains(Promotion_Record.Territory__r.Name)){
                    //Bypass Approval Process
                    byPassApprovalProcess(lstPromotionDates);
                }else{
                    System.debug('I am here 3');
                    callFADApprovalProcess(lstPromotionDates, Promotion_Record.Territory__r.id);
                }
            }                
            Res_Wrapper.responseStatus = 'Success';
        }catch(exception ex){
            system.debug('Error Captured : ' + ex);
            throw ex;
            //Res_Wrapper.responseStatus = 'Failure : ' + Ex;
        }    
        return Res_Wrapper;
    }

   /**
    * byPass the approval process for specific territories
    * @param  lstPromotionDates all valid promotion Dating records
    * @return void
    * @author Abhishek Mishra
    */
    Public static void byPassApprovalProcess(List<HEP_Promotion_Dating__c> lstPromotionDates){
        try{
            string HEP_APPROVAL_APPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'); 
            for(HEP_Promotion_Dating__c objProDatingRecord : lstPromotionDates){
                objProDatingRecord.FAD_Approval_Status__c = HEP_APPROVAL_APPROVED;
                objProDatingRecord.FAD_Override__c = true;
            }
            if(lstPromotionDates != null && !lstPromotionDates.isEmpty()){
                update lstPromotionDates;
            }
        }catch(exception ex){
            system.debug('Error Captured : ' + ex);
            throw ex;
        }
    }
    
   /**
    * Process the valid records for the approval process
    * @param  lstPromotionDates all valid promotion Dating records, territoryId of the promotion
    * @return void
    * @author Abhishek Mishra
    */
    Public static void callFADApprovalProcess(List<HEP_Promotion_Dating__c> lstPromotionDates, id territoryId){
        try{
            system.debug('I am here 4');
            string HEP_APPROVAL_APPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'); 
            string HEP_APPROVAL_PENDING = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING'); 
            string HEP_APPROVAL_TYPE_FAD_OVERRIDE = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_FAD_OVERRIDE');
            list<HEP_Promotion_Dating__c> lstProDatesUpdate = new list<HEP_Promotion_Dating__c>();
            list<HEP_Promotion_Dating__c> lstProDateMaxTenREcs = new list<HEP_Promotion_Dating__c>();
            list<Id> lstProDatingFADInitial = new list<Id>();
            list<Id> lstProDatingFADFinal = new list<Id>();
            integer iDatingRecCount = 0;
            for(HEP_Promotion_Dating__c objDatingRecord : lstPromotionDates){
                HEP_Promotion_Dating__c datingInstance = new HEP_Promotion_Dating__c(id= objDatingRecord.id);
                datingInstance.FAD_Approval_Status__c = HEP_APPROVAL_PENDING;
                datingInstance.FAD_Override__c = true;
                lstProDatesUpdate.add(datingInstance);
            }
            if(lstProDatesUpdate != null && !lstProDatesUpdate.isEmpty())
                update lstProDatesUpdate;
            //Call the alert Request method
            //HEP_Notification_Utility.CreateNotifications(lstPromotionDates, 'FAD_Request_Notification', 'HEP_Promotion_dating__c');
            for(HEP_Promotion_Dating__c objDatingRecord : lstPromotionDates){
                /*
                lstProDateMaxTenREcs.add(objDatingRecord);
                if(lstProDateMaxTenREcs.size() > 10){
                    //Call the Queable class Here
                    system.debug('Under Max 10 : ' + lstProDateMaxTenREcs);
                    HEP_ProcessFADApprovals objectInstance = new HEP_ProcessFADApprovals(lstProDateMaxTenREcs, HEP_Constants.HEP_APPROVAL_TYPE_FAD_OVERRIDE, territoryId);
                    System.enqueueJob(objectInstance); 
                    lstProDateMaxTenREcs.clear();
                } */  
                iDatingRecCount += 1;
                lstProDatingFADInitial.add(objDatingRecord.id);
                if(lstProDatingFADInitial.size() == 10){
                    system.debug('I am here Inside If');
                    system.debug('lstProDatingFADInitial : ' + lstProDatingFADInitial);
                    callApprovalprocessAsync(lstProDatingFADInitial, HEP_APPROVAL_TYPE_FAD_OVERRIDE, territoryId); 
                    lstProDatingFADInitial = new list<Id>();
                    lstProDatingFADFinal = new list<Id>();
                }else{
                        system.debug('I am here Inside Else');
                        system.debug('lstProDatingFADInitial : ' + lstProDatingFADInitial);
                        lstProDatingFADFinal.add(objDatingRecord.id);
                }   

                if(lstProDatingFADFinal != null && lstProDatingFADFinal.size() > 0 && lstPromotionDates.size() == iDatingRecCount){
                    system.debug('I am here Inside Final If');
                    system.debug('lstProDatingFADFinal : ' + lstProDatingFADFinal);
                    callApprovalprocessAsync(lstProDatingFADFinal, HEP_APPROVAL_TYPE_FAD_OVERRIDE, territoryId); 
                }     
            }
            /*
            if(lstProDateMaxTenREcs.size() > 0){
                //Call the Queable class Here 
                system.debug('Out max 10 : ' + lstProDateMaxTenREcs);
                HEP_ProcessFADApprovals objectInstance = new HEP_ProcessFADApprovals(lstProDateMaxTenREcs, HEP_Constants.HEP_APPROVAL_TYPE_FAD_OVERRIDE, territoryId);
                System.enqueueJob(objectInstance); 
            } */   
        }catch(Exception ex){
            system.debug('Call FAD Approval Process Error : ' + Ex);
            throw ex;
        }
    }
    
    
    
    /**
    * Process the valid records for the approval process
    * @param  DatingId : promation dating record Id, approvalType : type of approval need to fetch the approver, territoryId : territory linked with promotion
    * @return void
    * @author Abhishek Mishra
    */
    @future
    Public static void callApprovalprocessAsync(list<Id> lstPromoDatingIds, String approvalType, Id TerritoryId){
        try{
            for(Id sDatingId : lstPromoDatingIds){
                HEP_ApprovalProcess ApprovalProcessInsatace = new HEP_ApprovalProcess();
                ApprovalProcessInsatace.invokeApprovalProcess(sDatingId, approvalType, territoryId);
            }    
        }catch(Exception Ex){
            system.debug('Future Callout Errors : ' + ex);
            throw ex;
        }
    }

    /**
    * Delete all existing approvals
    * @param  DatingId : promation dating record Id, approvalType : type of approval need to fetch the approver, territoryId : territory linked with promotion
    * @return void
    * @author Abhishek Mishra
    */
    Public static void cancelExistingApprovalproces(string promotionId, List<HEP_Promotion_Dating__c> lstPromotionDates){
        try{
            //Delete all existing approval process
            system.debug('Delete all existing approval process');
            string HEP_APPROVAL_APPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'); 
            string HEP_APPROVAL_PENDING = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING'); 
            string HEP_APPROVAL_TYPE_FAD_OVERRIDE = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_FAD_OVERRIDE');
            List<HEP_Promotion_Dating__c> lstPromotionDatings = new List<HEP_Promotion_Dating__c>(); 
            for(HEP_Promotion_Dating__c objPromotionDating : lstPromotionDates){
                objPromotionDating.FAD_Approval_Status__c = '';
                objPromotionDating.FAD_Override__c = false;
                lstPromotionDatings.add(objPromotionDating);
            }
            system.debug('lstPromotionDatings : ' + lstPromotionDatings);
            if(lstPromotionDatings != null && !lstPromotionDatings.isEmpty()){
                update lstPromotionDatings;
            }
            list<HEP_Approvals__c> lstAllExistingApprovals = [select id from HEP_Approvals__c where  HEP_Promotion_Dating__c in :lstPromotionDates and Approval_Type__r.Type__c = :HEP_APPROVAL_TYPE_FAD_OVERRIDE];
            if(lstAllExistingApprovals != null && !lstAllExistingApprovals.isEmpty()){
                delete lstAllExistingApprovals;
            }
        }catch(Exception Ex){
            system.debug('Future Callout Errors : ' + ex);
            throw ex;
        }
    }
    
    /**
    * ResponseWrapper --- FAD Approval Response Wrapper
    * @author    Abhishek Mishra
    */
    public class ResponseWrapper{
        public string responseStatus;
        public void ResponseWrapper(string responseStatus){
            this.responseStatus = responseStatus;
        }
    }
}