/**
 * HEP_iTunes_Interface_Error_Notify_Batch -- This class gets the details of all the Promotion Datings that are confirmed and sends an email to the Predefined set of Users   
 * @author   Gaurav Mehrishi
 */
global class HEP_iTunes_Interface_Error_Notify_Batch implements Database.Batchable < sObject > {
    public static String sEmailTemplate;
        static{
            sEmailTemplate = HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify');
            }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // selects all the promotion dating records that are in Global & national which are not (reconfirm, not released, confirmed)

        String query = 'Select id,Object_API__c,Record_Id__c, ';
        query += 'Email_Template_Name__c,CC_Email_Address__c,To_Email_Address__c ';
        query += 'FROM HEP_Outbound_Email__c ';
        query += 'WHERE Email_Template_Name__c =: sEmailTemplate ';
        //query = 'Select Id from HEP_Outbound_Email__c WHERE Email_Template_Name__c =: sEmailTemplate and id in (' + query + ')';
        System.debug(query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List < sObject > scope) {
        try {
            system.debug('scope ' + scope.size());
            List<HEP_Outbound_Email__c> lstOutboundEmailsToInsert = new List<HEP_Outbound_Email__c>();
            List<HEP_Outbound_Email__c> lstOutboundEmails = new List<HEP_Outbound_Email__c>();
            List<HEP_Outbound_Email__c> lstUpdateEmail = new List<HEP_Outbound_Email__c>(); 
            Set<Id> setIDs = new Set<Id>();
            for (sobject objOutboundEmail: [Select id,Object_API__c,Record_Id__c,Email_Template_Name__c,CC_Email_Address__c,To_Email_Address__c,Email_Sent__c from    
                                HEP_Outbound_Email__c where Email_Template_Name__c = : HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify') 
                                AND Email_Sent__c=false
                                AND Id IN: scope]) 
            {   
                setIDs.add(objOutboundEmail.Id);            
            }
            system.debug('setIDs ' + setIDs);
            for (HEP_Outbound_Email__c objOutboundEmail: [Select id,Object_API__c,Record_Id__c,Email_Template_Name__c,CC_Email_Address__c,To_Email_Address__c,Email_Sent__c from    
                                HEP_Outbound_Email__c where Email_Template_Name__c = : HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify') 
                                AND Email_Sent__c=false
                                AND Id IN: setIDs]) 
            {
                lstOutboundEmailsToInsert.add(objOutboundEmail);                
            }
            if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
                lstOutboundEmails.add(lstOutboundEmailsToInsert[0]);             
                HEP_Send_Emails.sendOutboundEmails(lstOutboundEmails);
                for (HEP_Outbound_Email__c objEmail : lstOutboundEmailsToInsert){
                    objEmail.Email_Sent__c = True;
                    lstUpdateEmail.add(objEmail);
                }
                Update lstUpdateEmail;
            }                                   
        } catch (exception ex) {
            HEP_Error_Log.genericException('Auto Lock LIO and National Dates', 'DML Errors', ex, 'HEP_Auto_Lock_Promotion_Dating_Batch', 'execute', null, null);
            throw ex;
        }

    }

    global void finish(Database.BatchableContext BC) {

    }
}