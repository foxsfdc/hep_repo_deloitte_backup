/**
* HEP_InterfaceTxnResponse --- Controller class for the VF component to dynamically fetch Interface Transaction 
                               Error record and display it in Email Template
* @author    Abhishek Mishra
*/

public class HEP_Interface_Transaction_Error
{
    //Interface Transaction Error record id is passed in this variable
    public HEP_Interface_Transaction_Error__c iTEDetails{get; set;}
    public String iTERecId{
                      get; 
                      set{
                          iTERecId = value;
                          getHEP_Interface_Transaction_Error();
                      }
                   }
    public string iTELink{get; set;}                       

    /**
    * fetchInterfaceTransactionErrorRecordDetails --- fetch the Values of Merge fields used in Template
    * @author    Sachin Agarwal
    */
    
    public void getHEP_Interface_Transaction_Error(){
        try{
            system.debug('iTERecId : ' + iTERecId);
            system.debug('iTEDetails : ' + iTEDetails);
            if(iTERecId != null){
                //Query the Record based on Record Id               
                iTEDetails =[SELECT   Id,
                                    Name, 
                                    Error_Datetime__c,
                                    Error_Description__c,
                                    HEP_Interface_Transaction__r.Name,
                                    HEP_Interface_Transaction__r.HEP_Interface__r.Name,
                                    LastModifiedBy.Name, 
                                    Status__c
                                    FROM HEP_Interface_Transaction_Error__c
                                    Where Id=:iTERecId 
                                    LIMIT 1];
                system.debug('URL.getSalesforceBaseUrl() : ' + URL.getSalesforceBaseUrl().toExternalForm());                                   
                iTELink = URL.getSalesforceBaseUrl().toExternalForm() + '/'+iTERecId;
            }//-----------------------------------//
        }catch(Exception e){
            System.debug('Exception occured because of -->'+e.getMessage()+' @ Line Number-->'+e.getLineNumber());
        }
    }
}