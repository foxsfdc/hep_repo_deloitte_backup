/**
 * Test Class for HEP_Report_Controller
 * @author  : Abhishek Kumar
 */

@isTest
private class HEP_Report_Controller_Test {

    /**
    * testReportController -- Test method for the Report Controller 
    * @return  :  no return value
    * @author  :  Abhishek Kumar
    */

    @isTest
    public static  void testReportController() {
        HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.setCurrentPageReference(new PageReference('Page.HEP_IFrame_Report')); 
        System.currentPageReference().getParameters().put('reportType', 'ReportUpNext'); 
        HEP_Report_Controller objReportController = new HEP_Report_Controller();
        String sFoxReleaseSchedule = objReportController.sFoxReleaseSchedule; 
    }

}