@isTest
public class HEP_Foxipedia_Genre_Test {

    
    @testSetup
    static void testDataSetUpMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    
    @isTest 
    static void HEP_Foxipedia_Genre_InvalidAcessTokenTest() {
        
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer567854444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Genre demo = new HEP_Foxipedia_Genre();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
     @isTest 
     static void HEP_Foxipedia_Genre_SuccessTest() {
          
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '67728'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
       HEP_Foxipedia_Genre demo = new HEP_Foxipedia_Genre();
       // system.debug('TRANSAC RESPONSE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    @isTest 
    static void HEP_Foxipedia_Genre_FailureTest() {
        
      //  list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer567854444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Genre demo = new HEP_Foxipedia_Genre();
    //    system.debug('TRANSAC RESPONSE FAILURE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

}