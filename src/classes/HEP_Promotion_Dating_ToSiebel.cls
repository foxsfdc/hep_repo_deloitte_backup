/**
 * HEP_Promotion_Dating_ToSiebel --- Class for Calling the API to insert/update HEP Promotion Dating records
 * @author  Impana K
 */

public with sharing class HEP_Promotion_Dating_ToSiebel implements HEP_IntegrationInterface{

    /**
    * PromotionDatingWrapper --- main wrapper which holds the entire request
    * @author    Impana
    */
    public class PromotionDatingWrapper{
        public String ProducerID; // = HEP_SIEBEL_PRODUCERID
        public String EventType; // = HEP_SIEBEL_EVENTTYPE
        public String EventName; // = HEP_SIEBEL_EVENTNAME
        public Data Data;

        public PromotionDatingWrapper(){
            Data = new Data();
        }
    }

    public class Data{
        public Payload Payload; 

        public Data(){
            Payload = new Payload();
        }
    }

    public class Payload{
        list<PromotionDatingitems> items;

        public Payload(){
            items = new list<PromotionDatingitems>();
        }
    }

    /**
    * PromotionDatingitems --- record level wrapper to hold the record details
    * @author    Impana
    */
    public class PromotionDatingitems{
        public String objectType;
        public String createdDate;
        public String createdBy;
        public String updatedDate;
        public String updatedBy;
        public String recordId;
        public String recordStatus;
        public String territoryId;
        public String territoryName;
        public String promotionId;
        public String promotionName;
        public String promotionCode;
        public String dateType;
        public String channel;
        public date releaseDate;
        public String dateStatus;
        public String fadOverrideStatus;
        public String lockedStatus;
        public String catalogId;
        public Integer catalogNumber;
        public String regionId;
        public String regionName;
        public String languageList;
        public String customerName;     
    }
    
    /**
    * ErrorResponseWrapper --- Wrapper class for response parse
    * @author    Impana
    */
    public class ErrorResponseWrapper {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage) {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }

    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Impana K
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        HEP_Services__c serviceDetails;
        list<String> lstRecordIds = new list<String>();
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();

        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                
                if(String.isNotBlank(objTxnResponse.sSourceId))
                    lstRecordIds = objTxnResponse.sSourceId.split(',');

                httpRequest.setBody(getRequestBody(lstRecordIds));
                HTTP http = new HTTP();
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);

                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout

                String sHttpStatus = httpResp.getStatus();
                if(sHttpStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')) || sHttpStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){  
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }

    /**
    * Get the serialized json of the request to be sent
    * @param  list of record ids
    * @return 
    * @author Impana K
    */
    private String getRequestBody(list<String> lstPromotionDatingIds){
        PromotionDatingWrapper objPromotionDatingWrapper = new PromotionDatingWrapper();

        objPromotionDatingWrapper.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
        objPromotionDatingWrapper.EventType  = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
        objPromotionDatingWrapper.EventName  = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        String sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
        String sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
        List<String> lstChannelFormatTypes = new List<String>{sCHANNEL, sFORMAT};
        map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(lstChannelFormatTypes, 'Name', 'Values__c');
        System.debug('mapLOVMapping :' + mapLOVMapping);

        for(HEP_Promotion_Dating__c objPromotionDating : [SELECT 
                      Id, Promotion__r.Territory__c, Promotion__r.Territory__r.Name, Promotion__c, Promotion__r.PromotionName__c,
                      Promotion__r.LocalPromotionCode__c, DateType__c, Date__c, Status__c, FAD_Approval_Status__c, Locked_Status__c,
                      HEP_Catalog__c, HEP_Catalog__r.CatalogId__c, Language__r.Name, Channel__c,
                      Customer__r.CustomerName__c, CreatedDate, Promotion__r.Promotion_Type__c, CreatedBy.Name, LastModifiedDate,
                      LastModifiedBy.Name, Record_Status__c, Territory__c, Territory__r.Name
                      FROM
                        HEP_Promotion_Dating__c
                      WHERE 
                        Id IN :lstPromotionDatingIds                      
                     ]){
            PromotionDatingitems objPromotionDatingItems = new PromotionDatingitems();
            objPromotionDatingItems.objectType        = HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTIONDATINGOBJ');
            objPromotionDatingItems.createdDate       = String.valueOf(objPromotionDating.CreatedDate);
            objPromotionDatingItems.createdBy         = objPromotionDating.CreatedBy.Name;
            objPromotionDatingItems.updatedDate       = String.valueOf(objPromotionDating.LastModifiedDate);
            objPromotionDatingItems.updatedBy         = objPromotionDating.LastModifiedBy.Name;
            objPromotionDatingItems.recordId          = objPromotionDating.Id;
            objPromotionDatingItems.recordStatus      = objPromotionDating.Record_Status__c;
            objPromotionDatingItems.territoryId       = objPromotionDating.Promotion__r.Territory__c;
            objPromotionDatingItems.territoryName     = objPromotionDating.Promotion__r.Territory__r.Name;
            objPromotionDatingItems.promotionId       = objPromotionDating.Promotion__c;
            objPromotionDatingItems.promotionName     = objPromotionDating.Promotion__r.PromotionName__c;
            objPromotionDatingItems.promotionCode     = objPromotionDating.Promotion__r.LocalPromotionCode__c;
            objPromotionDatingItems.dateType          = objPromotionDating.DateType__c;
            if(objPromotionDating.Channel__c != null){
                String sChannelMeaning = mapLOVMapping.containsKey(sCHANNEL + '*' + objPromotionDating.Channel__c) ? mapLOVMapping.get(sCHANNEL + '*' + objPromotionDating.Channel__c) : objPromotionDating.Channel__c;
                objPromotionDatingItems.channel       = sChannelMeaning;
            }else
                objPromotionDatingItems.channel       = objPromotionDating.Channel__c;
            objPromotionDatingItems.releaseDate       = objPromotionDating.Date__c;
            objPromotionDatingItems.dateStatus        = objPromotionDating.Status__c;
            objPromotionDatingItems.fadOverrideStatus = objPromotionDating.FAD_Approval_Status__c;
            objPromotionDatingItems.lockedStatus      = objPromotionDating.Locked_Status__c;
            objPromotionDatingItems.catalogId         = objPromotionDating.HEP_Catalog__c;  
            objPromotionDatingItems.catalogNumber     = objPromotionDating.HEP_Catalog__r.CatalogId__c != null ? Integer.valueOf(objPromotionDating.HEP_Catalog__r.CatalogId__c): null;  
            objPromotionDatingItems.regionId          = objPromotionDating.Territory__c;  
            objPromotionDatingItems.regionName        = objPromotionDating.Territory__r.Name;  
            objPromotionDatingItems.languageList      = objPromotionDating.Language__r.Name;  
            objPromotionDatingItems.customerName      = objPromotionDating.Customer__r.CustomerName__c;  

            objPromotionDatingWrapper.Data.Payload.items.add(objPromotionDatingItems);
        }
        return JSON.serialize(objPromotionDatingWrapper);       
    }
}