/**
 * HEP_RuleCriterionExtension_Test --- Test Class for HEP_RuleCriterionExtension
 * @author  Nidhin V K
 */
@isTest
private class HEP_RuleCriterionExtension_Test {
    
    @testSetup static void setupData() {
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createRuleCriteria(HEP_Test_Data_Setup_Utility.createRule(true).Id, TRUE);
    }
    
    @isTest static void testRuleCriteriaMethods() {
        Test.startTest();
        PageReference pageRef = Page.HEP_CreateRuleCriterion;
        HEP_Rule__c objRule = [SELECT 
                                Id, Name, HEP_Target_SObject_API_Name__c,
                                HEP_Priority__c, HEP_IsActive__c
                            FROM 
                                HEP_Rule__c 
                            LIMIT 1];
        System.debug('objRule>>' + objRule);
        //objRule.HEP_IsActive__c = TRUE;
        //update objRule;
        HEP_Rule_Criterion__c objCriterion = [SELECT 
                                            Id, Name, RecordTypeId, HEP_Rule__c,
                                            HEP_Serial_Number__c
                                        FROM 
                                            HEP_Rule_Criterion__c 
                                        LIMIT 1];
        System.debug('objCriterion>>' + objCriterion);
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController objStdCntrl = new ApexPages.standardController(objCriterion);
        
        HEP_RuleCriterionExtension objExtension = new HEP_RuleCriterionExtension(objStdCntrl);
        objExtension.populateSObjectFields('InvalidObject');
        
        objExtension.save();
        
        objExtension.selectedRTName = HEP_Rule_Constants.HEP_RECORD_TYPE_COMPARE;
        objCriterion.HEP_Source_SObject_Field__c = 'Name';
        objExtension.save();
        
        objCriterion.HEP_Source_SObject_Field__c = 'abc';
        objExtension.save();
        
        objExtension.selectedRTName = HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_STATIC;
        objCriterion.HEP_Target_Task_Field__c = HEP_Rule_Constants.HEP_FIELD_DUE_DATE;
        objExtension.enableDisableTaskField();
        System.assertEquals(objExtension.bShowTask, TRUE);
        objExtension.save();
        
        objExtension.selectedRTName = HEP_Rule_Constants.HEP_RECORD_TYPE_MAP_SOBJECT;
        objExtension.enableDisableTaskField();
        System.assertEquals(objExtension.bShowTask, TRUE);
        objCriterion.HEP_Source_SObject_Field__c = 'Name';
        objExtension.save();
        HEP_CheckRecursive.clearSetIds();
        objExtension.deleteCriteria();
        
        objRule.HEP_IsActive__c = FALSE;
        update objRule;
        objRule.HEP_IsActive__c = TRUE;
        update objRule;
        HEP_CheckRecursive.clearSetIds();
        objExtension.deleteCriteria();
        
        Test.stopTest();
    }
}