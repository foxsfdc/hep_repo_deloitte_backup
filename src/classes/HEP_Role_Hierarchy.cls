/**
 * HEP_Role_Hierarchy ---  Determine the Resource Hierarchy for a User
 * @author  Nishit Kedia
 */
public  class HEP_Role_Hierarchy {
    
    //Memeber Variables of Class
    public Map<Id, Set<Id>> mapUsersInHierarchy = new Map<Id, Set<Id>>();   
    public String sManager = HEP_Utility.getConstantValue('HEP_MANAGER_HIERARCHY');
    public Map<Id, Set<Id>> mapRolesInHierarchy = new Map<Id, Set<Id>>();   
    public Map<Id, List<HEP_Role_Hierarchy__c>> mapParentRole = new Map<Id, List<HEP_Role_Hierarchy__c>>();
    
    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    // public static Map<String, List<String>> userMgrNamesMap = new Map<String, List<String>>();
    //public static string resourceId;
    
    /**
    * GenerateRoleHierarchy --- Fetch the Role Mapping in the Hierarchy
    * @param sReportingType is Either Manager/Reportee
    * @param sHierarchyId is the particular Hierarchy type
    * @return nothing
    * @author    Nishit Kedia
    */
    public void GenerateRoleHierarchy(String sHierarchyId, String sReportingType) {
        
        String sReportingTypeRoleId=''; 
        
        if(String.isNotBlank(sHierarchyId) && String.isNotBlank(sReportingType)) {
        
	        for (HEP_Role_Hierarchy__c resourceHierarchy : [SELECT id,User_Role__c, 
	                                                                    Manager_Role__c    
	                                                                    FROM 
	                                                                    HEP_Role_Hierarchy__c 
	                                                                    where Hierarchy_Type__c=:sHierarchyId
	                                                                    AND Record_Status__c =:sACTIVE ]) {
	            if(sReportingType.equalsIgnoreCase(sManager)){
	                sReportingTypeRoleId=resourceHierarchy.User_Role__c;
	            }else{
	                sReportingTypeRoleId=resourceHierarchy.Manager_Role__c;
	            }
	            
	            if (!mapParentRole.containsKey(sReportingTypeRoleId)) {
	                mapParentRole.put(sReportingTypeRoleId, new List<HEP_Role_Hierarchy__c> {resourceHierarchy});
	            } else {
	                mapParentRole.get(sReportingTypeRoleId).add(resourceHierarchy);
	            }
	        }
        }
    }
    
    
    
    /**
    * GenerateRoleHierarchyforApprovalType --- For Approval Screen based on Approval Type generate Role Mapping
    * @param sReportingType is Either Manager/Reportee
    * @param sApprovalType is the Distinct Approval Type
    * @return nothing
    * @author    Nishit Kedia
    */
    
    public void GenerateRoleHierarchyforApprovalType(String sApprovalType, String sReportingType) {
        
        String sReportingTypeRoleId='';
        
        if(String.isNotBlank(sReportingType) && String.isNotBlank(sApprovalType)) {
	        Set <String> setHierarchy = new Set <String>();
	        for(HEP_Approval_Type__c tempApprovalType : [select Id,
	        													Hierarchy_Type__c 
	        													from HEP_Approval_Type__c 
	        													where Type__c=:sApprovalType
	        													AND Record_Status__c =:sACTIVE ]) {
	        
	            setHierarchy.add(tempApprovalType.Hierarchy_Type__c);
	        }
	        
	        for (HEP_Role_Hierarchy__c resourceHierarchy : [SELECT id,User_Role__c, 
	                                                                    Manager_Role__c    
	                                                                    FROM 
	                                                                    HEP_Role_Hierarchy__c 
	                                                                    where Hierarchy_Type__c=:setHierarchy
	                                                                    AND Record_Status__c =:sACTIVE ]) {
	            if(sReportingType == sManager)
	                sReportingTypeRoleId=resourceHierarchy.User_Role__c;
	            else
	                sReportingTypeRoleId=resourceHierarchy.Manager_Role__c;
	            
	            if (!mapParentRole.containsKey(sReportingTypeRoleId)) {
	                mapParentRole.put(sReportingTypeRoleId, new List<HEP_Role_Hierarchy__c> {resourceHierarchy});
	            } else {
	                mapParentRole.get(sReportingTypeRoleId).add(resourceHierarchy);
	            }
	        }
        }
    }
    
    // accepts the list of user roles to get its child roles or parent roles based on reporting type
    
    /**
    * GetRoleHierarchyList --- For Approval Screen based on Approval Type generate Role Mapping
    * @param sReportingType is Either Manager/Reportee
    * @param lstUserRoles is the Role list on which role Mapping Base will be genrated
    * @return nothing
    * @author    Nishit Kedia
    */
    
    public void GetRoleHierarchyList(List<String> lstUserRoles, String sReportingType) { 
    	if(String.isNotBlank(sReportingType) && lstUserRoles != null) {
	        for(String sResourceRoleId : lstUserRoles){         
	            GetEachRoleHierarchy( sResourceRoleId, sResourceRoleId, sReportingType);
	        }
    	}
    }
    
    
    
    /**
    * GetEachRoleHierarchy --- For Approval Screen based on Approval Type generate Role Mapping
    * @param sReportingType is Either Manager/Reportee
    * @param roleId is the Role Id for Which Hierarchy needs to be created
    * @param sResourceRoleId is the Role Id for the Same Starting Role
    * @return nothing
    * @author    Nishit Kedia
    */
    public void GetEachRoleHierarchy(String roleId, String sResourceRoleId, String sReportingType) {        
        // for each role puts it into map for role and its list of mapped roles child/parent            
        if(!mapRolesInHierarchy.containsKey(sResourceRoleId)){
            mapRolesInHierarchy.put(sResourceRoleId,new Set<Id>{});         
        }
          
        system.debug('mapRolesInHierarchy '+ mapRolesInHierarchy);
        // if the role contains related roles in the hierarchy
        // put it into map then for each of it again check for child/parent roles to add to map
        if (mapParentRole.get(roleId) != null) {
            for (HEP_Role_Hierarchy__c mgr : mapParentRole.get(roleId)) {                
                if(sReportingType == sManager)
                {
                    mapRolesInHierarchy.get(sResourceRoleId).add(mgr.Manager_Role__c);
                    GetEachRoleHierarchy(mgr.Manager_Role__c, sResourceRoleId, sReportingType);
                }
                else
                {
                    mapRolesInHierarchy.get(sResourceRoleId).add(mgr.User_Role__c);
                    GetEachRoleHierarchy(mgr.User_Role__c, sResourceRoleId, sReportingType);
                }
            }
        }
    }
}