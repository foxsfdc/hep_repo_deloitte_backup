/**
* HEP_Synopsis -- Get Synopsis rowObjectId and update the Synopsis record in Salesforce
* @author    Ayan Sarkar
*/
public class HEP_Synopsis implements HEP_IntegrationInterface{ 
	/**
    * HEP_Synopsis -- 
    * @return  :  no return value
    * @author  :  Ayan Sarkar
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
    	//Initial Variables....
    	String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');


    	HEP_Foxipedia_Synopsis_Publish__c objCurrentSynopsisRecord;
    	EMD_Utilities.Message message;
    	EMD_Synopsis.Response jsonResponse = new EMD_Synopsis.Response();
        EMD_Synopsis.Attributes synopsisToSave = new EMD_Synopsis.Attributes();
        EMD_Synopsis.Data datatoReturn = new EMD_Synopsis.Data();
        Map<String , EMD_Utilities.Message> mapTypeToMessage = new Map<String , EMD_Utilities.Message>();

        //Get the parameters..
    	HEP_SynopsisWrapper objSynopsisWrapper = (HEP_SynopsisWrapper)JSON.deserialize(objTxnResponse.sSourceId ,HEP_SynopsisWrapper.class);
    	synopsisToSave = objSynopsisWrapper.objAttributes;
    	objCurrentSynopsisRecord = objSynopsisWrapper.objSynopsisRec;

    	System.debug('Attributes ----> ' + synopsisToSave);
    	System.debug('Synopsis Record ----> ' + objCurrentSynopsisRecord);

    	//Code to create wrapper to send the wrapper.
        EMD_Synopsis objEDMSynopsis = new EMD_Synopsis();
        objEDMSynopsis.data = new List<EMD_Synopsis.Data>();
        objEDMSynopsis.data.add(new EMD_Synopsis.Data());
        objEDMSynopsis.data[0].attributes = synopsisToSave;


    	//Actual Integration Call..
    	System.debug('*** In Post');
        //Step 1: Get The Token
        EMD_RestService_Authentication_Util.AccessTokenWrapper accessTokenObj = EMD_RestService_Authentication_Util.restCalloutDetails('Synopsis');
        
        //Step 2 : Verify if Token is received and the Proceed.
        if( null != accessTokenObj.message && String.isNotBlank(accessTokenObj.message.messageType) && accessTokenObj.message.messageType.equalsIgnoreCase('Success')){
            // Fetch the Service Details
            
            
            if(null != accessTokenObj.sandbox_url && null != accessTokenObj.service_url){
                HttpRequest req = new HttpRequest(); 
                req.setMethod('POST'); 
                req.setEndpoint(accessTokenObj.sandbox_url+accessTokenObj.service_url); 
                req.setHeader( 'Authorization', 'Bearer ' + accessTokenObj.access_token);
                req.setHeader('system', System.Label.HEP_SYSTEM_FOXIPEDIA);
                Http http = new Http();
                try{
                    
                    EMD_Synopsis.Data dataWrapper = new EMD_Synopsis.Data();
                    
                    dataWrapper.attributes = new EMD_Synopsis.Attributes();
                    
                    dataWrapper.attributes = synopsisToSave;
                    
                    //dataWrapper.attributes.userId = UserInfo.getFirstName();                            
                    System.debug('objEDMSynopsis ----> ' + Json.serializePretty(objEDMSynopsis, true));
                    String reqBody = Json.serializePretty(/*dataWrapper*/objEDMSynopsis,true);
                    String response;
                    System.debug('***1RESP '+reqBody);
                    req.setBody(reqBody);
                    HTTPResponse res = http.send(req);

                    //Initialize the Request Part to be saved in the Transaction Request.
                    objTxnResponse.sRequest = req.toString() + '\n Body : \t' + req.getBody();

                    jsonResponse = (EMD_Synopsis.Response)Json.deserialize(res.getBody() , EMD_Synopsis.Response.class);

                    //Initialize the Response part to be saved int he Transaction Response.
                    objTxnResponse.sResponse = res.getBody(); //Response from callout
                    System.debug('jsonResponse ----> '+jsonResponse);
                    
                    if(res.getStatusCode() != 200){
                        System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                        objTxnResponse.bRetry = true;
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        objTxnResponse.sErrorMsg = 'The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus();
                    }
                    else if(String.isNotBlank(jsonResponse.rowIdObject)){
                        EMD_Synopsis.Response objResponse = (EMD_Synopsis.Response) JSON.deserialize(res.getBody(), EMD_Synopsis.Response.class);
                        message = new EMD_Utilities.Message('Success', System.Label.EMD_Record_Saved , objResponse.rowIdObject);
                        response = res.getBody(); 
                        mapTypeToMessage.put('Success' , message);
                        objTxnResponse.bRetry = false;
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    }
                    /*else if(!jsonResponse.errors.isEmpty()){
                        message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , jsonResponse.errors[0].detail);
                        mapTypeToMessage.put('Error' , message);
                        objTxnResponse.bRetry = true;
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    }*/
                }
                /*catch(CalloutException excep){
                    message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , excep.getMessage());
                    mapTypeToMessage.put('Error' , message);
                    objTxnResponse.bRetry = true;
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                }*/
                catch(Exception excep){
                    /*message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , excep.getMessage());
                    mapTypeToMessage.put('Error' , message);
                    objTxnResponse.bRetry = true;
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');*/
                }
                
            }
        }
        
        datatoReturn.message = message;
        datatoReturn.attributes = synopsisToSave;
        System.debug('MYOBJ' + datatoReturn);
        System.debug('Success Id --> ' + datatoReturn.message.messageDetails);
        //return json.serializePretty(datatoReturn);
        //return datatoReturn.message.messageDetails;
        String sSynopsisRowId = (mapTypeToMessage.containsKey('Success') ? mapTypeToMessage.get('Success').messageDetails : null);

        if(String.isNotBlank(sSynopsisRowId)){
        	objCurrentSynopsisRecord.Synopsis_Row_Id__c = sSynopsisRowId;
        	update objCurrentSynopsisRecord;

        	System.debug('Success');
        }else{
        	objTxnResponse.sStatus = sHEP_FAILURE;
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
            System.debug('Failure');
        }
    }

    /**
    * HEP_SynopsisWrapper  -- Class to hold Input parameter details to be sent.
    * @author Ayan Sarkar
    */ 
    public class HEP_SynopsisWrapper {
        EMD_Synopsis.Attributes objAttributes;
        HEP_Foxipedia_Synopsis_Publish__c objSynopsisRec;

        public HEP_SynopsisWrapper(EMD_Synopsis.Attributes objAttributes , HEP_Foxipedia_Synopsis_Publish__c objSynopsisRec){
        	this.objAttributes = objAttributes;
        	this.objSynopsisRec = objSynopsisRec;
        }

        public HEP_SynopsisWrapper(){}
    }
}