/**
    *HEP_Catalog_Customer_Mapping - To show Catalog Customer mapping data for Global customers 
    *@author : Ashutosh Arora
    */
    public class HEP_Catalog_Customer_Mapping_Controller{
        //Get Costant Values from Custom Setting
        public static string sACTIVE;
        public static string sMDPCustomers;
        public static string sDELETED;
        public static string sTRUE;
        public static string sALLGLOBALCUSTOMERS;
        static{
            sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            sMDPCustomers = HEP_Utility.getConstantValue('HEP_CUSTOMER_TYPE_MDP');
            sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
            sTRUE = HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE');
            sALLGLOBALCUSTOMERS = HEP_Utility.getConstantValue('HEP_GLOBAL_CUSTOMERS');
        }
        /**
        *fetchData - To fetch the Customer Mapping data to be displayed on UI called on Page Load
        *@return ProductMappingWrapper (ProductMappingWrapper Wrapper with Customer Mapping Data)
        *@author Ashutosh Arora
        */
        @RemoteAction
        public static ProductMappingWrapper fetchData(String sCatalogId){
            List<String> lstGlobalCustomer = sALLGLOBALCUSTOMERS.split(',');
            List<HEP_User_Role__c> lstTmpUserRole = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
            System.debug('USER ROLE DATA : ' + lstTmpUserRole);
            System.debug('GLOBAL CUSTOMER LIST : ' + lstGlobalCustomer);
            Set<Id> setTerritoryIds = new Set<Id>();
            if(lstTmpUserRole != null){
                for(HEP_User_Role__c objUserRole : lstTmpUserRole){
                    if(objUserRole.read__c == true || objUserRole.write__c == true)
                        setTerritoryIds.add(objUserRole.Territory__c);
                }
            }
            System.debug('TERRITORY IDS THAT USER HAS ACCESS TO : ' + setTerritoryIds);
            //Get List of all Customers and Regions to display in dropdown for filter
            List < HEP_Customer__c > lstMDPCustomer = [SELECT Id, CustomerName__c,Customer_Type__c
                                                        FROM HEP_Customer__c
                                                        WHERE Record_Status__c =: sACTIVE
                                                        AND Customer_Type__c =: sMDPCustomers
                                                        AND CustomerName__c IN: lstGlobalCustomer
                                                        ORDER BY CustomerName__c ASC
            ];
            System.debug('Customers MDP Fetch ' + lstMDPCustomer);
            List < HEP_Territory__c > lstRegions = [SELECT Id, Name,
                                                        MM_Territory_Code__c,Parent_Territory__c
                                                        FROM HEP_Territory__c
                                                        WHERE ID IN: setTerritoryIds
                                                        ORDER BY Name ASC
                                                    ];
            List<HEP_Territory__c> lstTerritories = new List<HEP_Territory__c>();                                        
            List<String> lstRegionIds = new List<String>();
            for(HEP_Territory__c objTerritory : lstRegions){
                if(objTerritory.MM_Territory_Code__c != null)
                    lstTerritories.add(objTerritory);
                else
                    lstRegionIds.add(objTerritory.Id);
            }
            System.debug('Regions****' + lstRegionIds);
            //If there is access to any region, find it's territories
            if(lstRegionIds != null){
                for(HEP_Territory__c objTerr : [SELECT Id,Name,MM_Territory_Code__c FROM HEP_Territory__c WHERE Parent_Territory__c IN: lstRegionIds AND MM_Territory_Code__c != null]){
                    lstTerritories.add(objTerr);
                }   
            }
            // Getting first Global Customer Name and Territory from the list to display data initially on page load
            String sInitialCustomer = lstMDPCustomer[0].CustomerName__c;
            String sInitialRegion = lstTerritories[0].Name;
            ProductMappingWrapper objMappingData = getMappingData(sCatalogId, sInitialCustomer, sInitialRegion);
            //Return the mapping data to display on UI
            return objMappingData;
        }
        
        @RemoteAction
        public static ProductMappingWrapper getMappingData(String sCatalogId, String sCustomerName, String sRegionName){
            String sProductType;
            String sPrimaryCatalogId;
            List<String> lstGlobalCustomer = sALLGLOBALCUSTOMERS.split(',');
            System.debug('CATALOG ID : ' + sCatalogId);
            System.debug('Customer Name : ' + sCustomerName);
            System.debug('Region Name : ' + sRegionName);
                List<HEP_User_Role__c> lstTmpUserRole = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
                Set<Id> setTerritoryIds = new Set<Id>();
                if(lstTmpUserRole != null){
                    for(HEP_User_Role__c objUserRole : lstTmpUserRole){
                        if(objUserRole.read__c == true || objUserRole.write__c == true)
                            setTerritoryIds.add(objUserRole.Territory__c);
                    }
                }
                Map<String,String> mapChildParentTerrIds = new Map<String,String>();
                //Get the Primary Catalog Id to fetch the mapping data
                List<HEP_Catalog__c> lstCatalog = [SELECT Primary_Catalog_Id__c,Product_Type__c 
                                                            FROM HEP_Catalog__c
                                                            WHERE Id =: sCatalogId];
                System.debug('LIST INDEX OUT OF BOUNDS : ' + lstCatalog);                      
                if(lstCatalog[0].Primary_Catalog_Id__c != null && !String.isEmpty(lstCatalog[0].Primary_Catalog_Id__c)){
                    sPrimaryCatalogId = lstCatalog[0].Primary_Catalog_Id__c.trim();
                }
                System.debug('Primary Catalog Id : ' + sPrimaryCatalogId);
                List<HEP_Territory__c> lstTerritory = [SELECT Id, Name,Parent_Territory__c
                                                        FROM HEP_Territory__c
                                                        WHERE Name =:sRegionName];
                List<HEP_Territory__c> tmpLstRegion = [SELECT Id, Name ,MM_Territory_Code__c,Parent_Territory__c
                                                                    FROM HEP_Territory__c
                                                                    WHERE Id IN: setTerritoryIds];
                List<HEP_Territory__c> lstTerritoryAccessable = new List<HEP_Territory__c>();                                        
                List<String> lstRegionIds = new List<String>();
                for(HEP_Territory__c objTerritory : tmpLstRegion){
                    lstTerritoryAccessable.add(objTerritory);
                }
                System.debug('Germany??' +lstTerritoryAccessable);
                System.debug('Regions****' + lstRegionIds);
                //If there is access to any region, find it's territories
                if(lstRegionIds != null){
                    for(HEP_Territory__c objTerr : [SELECT Id,Name,MM_Territory_Code__c FROM HEP_Territory__c WHERE Parent_Territory__c IN: lstTerritoryAccessable AND MM_Territory_Code__c != null]){
                        System.debug('This :**' +objTerr);
                        lstTerritoryAccessable.add(objTerr);
                    }   
                }
                List<HEP_Territory__c> lstDropTerr = new List<HEP_Territory__c>();
                for(HEP_Territory__c objTerritory : lstTerritoryAccessable){
                    if(objTerritory.MM_Territory_Code__c != null){
                        lstDropTerr.add(objTerritory);
                    }
                }
                System.debug('Here :****' + lstTerritoryAccessable);
                List<HEP_Territory__c> lstTerr = [SELECT Id, Name, Parent_Territory__c 
                                                    FROM HEP_Territory__c
                                                    WHERE ID IN: lstDropTerr];
                //Populate map with region and it's parent territory Id
                for(HEP_Territory__c objChildParent : lstTerr){
                    if(!(mapChildParentTerrIds.containsKey(objChildParent.Id))){
                        mapChildParentTerrIds.put(objChildParent.Id,objChildParent.Parent_Territory__c);
                    }
                }
                System.debug('mapChildParentTerrIds***' + mapChildParentTerrIds);
                System.debug('Selected : ' +lstTerritory[0].Id);
                List < HEP_Customer__c > lstMDPCustomer = [SELECT Id, CustomerName__c,Customer_Type__c FROM HEP_Customer__c
                                                                WHERE Record_Status__c =: sACTIVE
                                                                AND Customer_Type__c =: sMDPCustomers
                                                                AND CustomerName__c IN: lstGlobalCustomer
                                                                ORDER BY CustomerName__c ASC
                                                            ];  
                System.debug('List Customers in MAPPING : ' + lstMDPCustomer);
                Set < String > setCustomers = new Set < String > ();
                Set < String > setRegions = new Set < String > ();
                List < HEP_MDP_Product_Mapping__c > lstMappingRecords;
                ProductMappingWrapper objProductMappingWrapper = new ProductMappingWrapper();
                List<HEP_Catalog__c> lstPrimaryCatalog = new List<HEP_Catalog__c>();
                if(sPrimaryCatalogId != null){
                    lstPrimaryCatalog = [SELECT Product_Type__c
                                          FROM HEP_Catalog__c
                                          WHERE CatalogId__c =: sPrimaryCatalogId];
                    if(!lstPrimaryCatalog.isEmpty()){
                        if(lstPrimaryCatalog[0].Product_Type__c != null){
                            System.debug('List Primary Cataog Record : ' + lstPrimaryCatalog);
                            objProductMappingWrapper.sMappingType = lstPrimaryCatalog[0].Product_Type__c;
                        }else if(lstCatalog[0].Product_Type__c != null){
                        objProductMappingWrapper.sMappingType = lstCatalog[0].Product_Type__c;
                        }
                    }
                }else if(lstCatalog[0].Product_Type__c != null){
                    objProductMappingWrapper.sMappingType = lstCatalog[0].Product_Type__c;
                }
                
                //Query the Mapping records only when Catalog Id, Customer Name and Region Name are present
                System.debug('Before Querying : ');
                if((sCatalogId != null && !String.isEmpty(sCatalogId)) && (sCustomerName != null && !String.isEmpty(sCustomerName)) && (sRegionName != null && !String.isEmpty(sRegionName))){
                    lstMappingRecords = [SELECT ASIN__c,Eidr__c,ETAG__c,
                                            Catalog__c,Big_Id__c,Format__c,
                                            HEP_Customer__r.CustomerName__c,
                                            Channel__c,Adam_Id__c,Media_Id__c,
                                            Series_Media_Id__c,Local_Description__c,
                                            Season_ASIN__c,Series_Title__c,HEP_Customer__c,
                                            X1st_Episode_ASIN__c,Catalog__r.Catalog_Type__c,
                                            Component_Id__c,Series_Big_Id__c,Season_Number__c,
                                            HEP_Territory__c,Record_Status__c,Episode_Number__c,
                                            Series_Id__c,Google_Id__c,
                                            (SELECT ParentId, OldValue, NewValue, Field, CreatedBy.Name, CreatedDate FROM Histories ORDER BY CreatedDate DESC)
                                            FROM HEP_MDP_Product_Mapping__c
                                            WHERE Catalog__c =: sCatalogId
                                            AND Catalog__r.Catalog_Type__c = 'Bundle'   //customSetting
                                            AND HEP_Territory__r.Name =: sRegionName
                                            AND HEP_Customer__r.CustomerName__c =: sCustomerName
                                            AND Record_Status__c =: sACTIVE];
                
                }else{// set list of mapping Records as null if any of CatalogId,CustomerName and Region Name value is not there 
                    ProductDataWrapper objProductDataWrapper = new ProductDataWrapper();
                    objProductMappingWrapper.sSelectedRegion = sRegionName;
                    objProductMappingWrapper.sSelectedCustomer = sCustomerName;
                    objProductMappingWrapper.sSelectedRegionId = lstTerritory[0].Id;
                    objProductMappingWrapper.sIdCatalog = sCatalogId;
                    if(objProductMappingWrapper.sSelectedRegionId != null){
                            objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(objProductMappingWrapper.sSelectedRegionId,'');
                        }
                    objProductMappingWrapper.lstProductData = null;
                }
                System.debug('After Querying : ');
                System.debug('lstMappingRecords : ' +lstMappingRecords);
                if(lstMappingRecords != null && !lstMappingRecords.isEmpty()){
                    for(HEP_MDP_Product_Mapping__c objCatalogCustomerMapping : lstMappingRecords){
                        ProductDataWrapper objProductDataWrapper = new ProductDataWrapper();
                        objProductDataWrapper.sRecordId = objCatalogCustomerMapping.Id;
                        if (objCatalogCustomerMapping.Channel__c != null) objProductDataWrapper.sChannel = objCatalogCustomerMapping.Channel__c;
                        if (objCatalogCustomerMapping.Format__c != null) objProductDataWrapper.sFormat = objCatalogCustomerMapping.Format__c;
                        if (objCatalogCustomerMapping.Eidr__c != null) objProductDataWrapper.sEidr = objCatalogCustomerMapping.Eidr__c;
                        if (objCatalogCustomerMapping.ETAG__c != null) objProductDataWrapper.sEtag = objCatalogCustomerMapping.ETAG__c;
                        if (objCatalogCustomerMapping.ETAG__c != null) objProductDataWrapper.sEtag = objCatalogCustomerMapping.ETAG__c;
                        if (objCatalogCustomerMapping.Big_Id__c != null) objProductDataWrapper.sBigId = objCatalogCustomerMapping.Big_Id__c;
                        if (objCatalogCustomerMapping.Media_Id__c != null) objProductDataWrapper.sMediaId = objCatalogCustomerMapping.Media_Id__c;
                        if (objCatalogCustomerMapping.Series_Id__c != null) objProductDataWrapper.sSeriesId = objCatalogCustomerMapping.Series_Id__c;
                        if (objCatalogCustomerMapping.Google_Id__c != null) objProductDataWrapper.sVideoId = objCatalogCustomerMapping.Google_Id__c;
                        if (objCatalogCustomerMapping.Season_ASIN__c != null) objProductDataWrapper.sSeasonASIN = objCatalogCustomerMapping.Season_ASIN__c;
                        if (objCatalogCustomerMapping.Series_Title__c != null) objProductDataWrapper.sSeriesTitle = objCatalogCustomerMapping.Series_Title__c;
                        if (objCatalogCustomerMapping.Component_Id__c != null) objProductDataWrapper.sComponentId = objCatalogCustomerMapping.Component_Id__c;
                        if (objCatalogCustomerMapping.Series_Big_Id__c != null) objProductDataWrapper.sSeriesBigId = objCatalogCustomerMapping.Series_Big_Id__c;
                        if (objCatalogCustomerMapping.Season_Number__c != null) objProductDataWrapper.sSeasonNumber = objCatalogCustomerMapping.Season_Number__c;
                        if (objCatalogCustomerMapping.Episode_Number__c != null) objProductDataWrapper.sEpisodeNumber = objCatalogCustomerMapping.Episode_Number__c;
                        if (objCatalogCustomerMapping.Series_Media_Id__c != null) objProductDataWrapper.sSeriesMediaId = objCatalogCustomerMapping.Series_Media_Id__c;
                        if (objCatalogCustomerMapping.X1st_Episode_ASIN__c != null) objProductDataWrapper.s1stEpisodeASIN = objCatalogCustomerMapping.X1st_Episode_ASIN__c;
                        if (objCatalogCustomerMapping.ASIN__c != null) objProductDataWrapper.sASIN = objCatalogCustomerMapping.ASIN__c;
                        if (objCatalogCustomerMapping.Local_Description__c != null) objProductDataWrapper.sLocalDescription = objCatalogCustomerMapping.Local_Description__c;
                        if (objCatalogCustomerMapping.Adam_Id__c != null) objProductDataWrapper.sADAMId = objCatalogCustomerMapping.Adam_Id__c;

                        if (objCatalogCustomerMapping.Record_Status__c.equalsignorecase(sACTIVE)) {
                            objProductDataWrapper.bIsDELETED = false;
                        }
                        //Iterating over field Histories for the record
                        for (HEP_MDP_Product_Mapping__History objHistory: objCatalogCustomerMapping.Histories) {
                            LastModifiedField objHistoryRecord = new LastModifiedField();
                            //Getting history data only for the required fields 
                            objHistoryRecord.sLastModifiedBy = objHistory.CreatedBy.Name;
                            dateTime tempDt = objHistory.CreatedDate;
                            String dateFormatte = tempDT.format('dd-MMM-yyyy') + ' ' + tempDT.format('hh:mm a');
                            objHistoryRecord.sRecordId = objHistory.ParentId;
                            objHistoryRecord.sFieldName = objHistory.Field;
                            objHistoryRecord.sLastModifiedDate = dateFormatte;
                            objHistoryRecord.sLastModifiedData = 'Updated : ' + objHistoryRecord.sLastModifiedBy + ' ' + objHistoryRecord.sLastModifiedDate;
                            if (!objProductDataWrapper.mapFieldHistory.containsKey(objHistoryRecord.sFieldName)) {
                                objProductDataWrapper.mapFieldHistory.put(objHistory.Field, objHistoryRecord);
                            }
                        }
                        System.debug('HERE*****' + objProductDataWrapper.mapFieldHistory);
                        objProductDataWrapper.bUniqueASIN = true;
                        objProductDataWrapper.bLocalDescUnique = true;
                        objProductDataWrapper.bIdADAMUnique = true;
                        objProductDataWrapper.bLocalDescUnique = true;
                        objProductDataWrapper.bUniqueVideoId = true;
                        objProductDataWrapper.bMediaIdUnique = true;
                        objProductDataWrapper.bBigIdUnique = true;
                        objProductDataWrapper.bUnique1stEpASIN = true;
                        objProductDataWrapper.bUniqueSeasonASIN = true;
                        objProductMappingWrapper.sSelectedRegion = sRegionName;
                        objProductMappingWrapper.sSelectedCustomer = sCustomerName;
                        objProductMappingWrapper.sIdCatalog = sCatalogId;
                        objProductMappingWrapper.sSelectedRegionId = lstTerritory[0].Id;
                        
                        
                        objProductMappingWrapper.lstProductData.add(objProductDataWrapper);
                        System.debug('objProductMappingWrapper.lstProductData.   :  ' + objProductMappingWrapper.lstProductData);
                        System.debug('objProductMappingWrapper.sIdCatalog : ' + objProductMappingWrapper.sIdCatalog);   
                    }
                }
                //If the customer name or region name value is null    
                else {
                    ProductDataWrapper objProductDataWrapper = new ProductDataWrapper();
                    objProductMappingWrapper.sSelectedRegion = sRegionName;
                    objProductMappingWrapper.sSelectedCustomer = sCustomerName;
                    objProductMappingWrapper.sSelectedRegionId = lstTerritory[0].Id;
                    objProductMappingWrapper.sIdCatalog = sCatalogId;
                    if(objProductMappingWrapper.sSelectedRegionId != null){
                            objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(objProductMappingWrapper.sSelectedRegionId,'');
                        }
                    objProductMappingWrapper.lstProductData = null;
                }
                if(objProductMappingWrapper.sSelectedRegionId != null && mapChildParentTerrIds.get(objProductMappingWrapper.sSelectedRegionId)!=null){
                    objProductMappingWrapper.parentTerritoryAccess = HEP_Utility.fetchCustomPermissions(mapChildParentTerrIds.get(objProductMappingWrapper.sSelectedRegionId),'');
                }else if(objProductMappingWrapper.sSelectedRegionId != null){
                    objProductMappingWrapper.mapTerritoryAccess = HEP_Utility.fetchCustomPermissions(objProductMappingWrapper.sSelectedRegionId,'');    
                }                   
                System.debug('parentTerritoryAccess :***' + objProductMappingWrapper.parentTerritoryAccess);
                for(HEP_Territory__c objTerritory : lstDropTerr){
                    setRegions.add(objTerritory.Name);
                }
                objProductMappingWrapper.lstRegions.addAll(setRegions);
                System.debug('Territories : ' + objProductMappingWrapper.lstRegions);
                for(HEP_Customer__c objCustomer : lstMDPCustomer){
                    setCustomers.add(objCustomer.CustomerName__c);
                }
                objProductMappingWrapper.lstCustomers.addAll(setCustomers);
                System.debug('Mapping Type  : ' + objProductMappingWrapper.sMappingType);
                System.debug('Customers : ' + objProductMappingWrapper.lstCustomers);
                System.debug('RECORDS RETURNED : ' +objProductMappingWrapper);
                return objProductMappingWrapper;
        }
        @RemoteAction
        public static void saveMappingData(ProductMappingWrapper objMappingData){
            //List that would hold records to upsert
            System.debug('objMappingData.mapTerritoryAccess : ' + objMappingData.mapTerritoryAccess);
            System.debug('objMappingData.parentTerritoryAccess : ' + objMappingData.parentTerritoryAccess);
            if((objMappingData.mapTerritoryAccess != null && objMappingData.mapTerritoryAccess.get('WRITE').equalsIgnoreCase(sTRUE)) || (objMappingData.parentTerritoryAccess != null && objMappingData.parentTerritoryAccess.get('WRITE').equalsIgnoreCase(sTRUE))){
                List<HEP_MDP_Product_Mapping__c> lstRecordstoUpsert = new List<HEP_MDP_Product_Mapping__c>();
                System.debug('USER HAS WRITE ACCESS');
                System.debug('DATA in SAVE FROM UI : ' +  objMappingData);
                    String customerName = objMappingData.sSelectedCustomer;
                    String regionName = objMappingData.sSelectedRegion;
                    List < HEP_Customer__c > lstCustomerId = [SELECT Id from HEP_Customer__c
                                                                WHERE CustomerName__c =: customerName
                                                                AND Customer_Type__c =: sMDPCustomers
                                                                AND Record_Status__c =: sACTIVE];
                    String IdCustomer = lstCustomerId[0].Id;
                    List < HEP_Territory__c > lstTerritoryId = [SELECT Id from HEP_Territory__c 
                                                                WHERE Name =: regionName];
                    String IdRegion = lstTerritoryId[0].Id;
                    String sCatalogId = objMappingData.sIdCatalog;
                    System.debug('sCatalogId ' + sCatalogId);
                    if(objMappingData.lstProductData != null && !objMappingData.lstProductData.isEmpty()){
                        for(ProductDataWrapper objMappingRecord : objMappingData.lstProductData){
                                if(objMappingRecord.sRecordId != null && !String.isEmpty(objMappingRecord.sRecordId)){
                                    System.debug('UPDATE EXISTING RECORD');
                                    HEP_MDP_Product_Mapping__c objMappingsData = new HEP_MDP_Product_Mapping__c();
                                    objMappingsData.Id = objMappingRecord.sRecordId;
                                    if (objMappingRecord.sChannel != null) objMappingsData.Channel__c = objMappingRecord.sChannel;
                                    if (objMappingRecord.sFormat != null) objMappingsData.Format__c = objMappingRecord.sFormat;
                                    if (objMappingRecord.sASIN != null) objMappingsData.ASIN__c = objMappingRecord.sASIN;
                                    if (objMappingRecord.sLocalDescription != null) objMappingsData.Local_Description__c = objMappingRecord.sLocalDescription;
                                    if (objMappingRecord.sADAMId != null) objMappingsData.Adam_Id__c = objMappingRecord.sADAMId;
                                    if (objMappingRecord.sBigId != null) objMappingsData.Big_Id__c = objMappingRecord.sBigId;
                                    if (objMappingRecord.sComponentId != null) objMappingsData.Component_Id__c = objMappingRecord.sComponentId;
                                    if (objMappingRecord.sEidr != null) objMappingsData.Eidr__c = objMappingRecord.sEidr;
                                    if (objMappingRecord.sEtag != null) objMappingsData.ETAG__c = objMappingRecord.sEtag;
                                    if (objMappingRecord.sMediaId != null) objMappingsData.Media_Id__c = objMappingRecord.sMediaId;
                                    if (objMappingRecord.sName != null) objMappingsData.Name__c = objMappingRecord.sName;
                                    if (objMappingRecord.bIsDELETED == true) {
                                        objMappingsData.Record_Status__c = sDELETED; 
                                    } else {
                                        objMappingsData.Record_Status__c = sACTIVE;   
                                    }
                                    if (objMappingRecord.sVideoId != null) objMappingsData.Google_Id__c = objMappingRecord.sVideoId;
                                    if (objMappingRecord.sSeasonNumber != null) objMappingsData.Season_Number__c = objMappingRecord.sSeasonNumber;
                                    if (objMappingRecord.sSeriesTitle != null) objMappingsData.Series_Title__c = objMappingRecord.sSeriesTitle;
                                    if (objMappingRecord.sSeriesId != null) objMappingsData.Series_Id__c = objMappingRecord.sSeriesId;
                                    if (objMappingRecord.sSeriesMediaId != null) objMappingsData.Series_Media_Id__c = objMappingRecord.sSeriesMediaId;
                                    if (objMappingRecord.sSeriesBigId != null) objMappingsData.Series_Big_Id__c = objMappingRecord.sSeriesBigId;
                                    if (objMappingRecord.s1stEpisodeASIN != null) objMappingsData.X1st_Episode_ASIN__c = objMappingRecord.s1stEpisodeASIN;
                                    if (objMappingRecord.sSeasonASIN != null) objMappingsData.Season_ASIN__c = objMappingRecord.sSeasonASIN;
                                    lstRecordsToUpsert.add(objMappingsData);
                                }else {
                                    System.debug('CREATE NEW RECORD');
                                    HEP_MDP_Product_Mapping__c objMappingsData = new HEP_MDP_Product_Mapping__c();
                                    if (objMappingRecord.sChannel != null) objMappingsData.Channel__c = objMappingRecord.sChannel;
                                    if (objMappingRecord.sFormat != null) objMappingsData.Format__c = objMappingRecord.sFormat;
                                    if (objMappingRecord.sASIN != null) objMappingsData.ASIN__c = objMappingRecord.sASIN;
                                    if (objMappingRecord.sLocalDescription != null) objMappingsData.Local_Description__c = objMappingRecord.sLocalDescription;
                                    if (objMappingRecord.sADAMId != null) objMappingsData.Adam_Id__c = objMappingRecord.sADAMId;
                                    if (objMappingRecord.sBigId != null) objMappingsData.Big_Id__c = objMappingRecord.sBigId;
                                    if (objMappingRecord.sComponentId != null) objMappingsData.Component_Id__c = objMappingRecord.sComponentId;
                                    if (objMappingRecord.sEidr != null) objMappingsData.Eidr__c = objMappingRecord.sEidr;
                                    if (objMappingRecord.sEtag != null) objMappingsData.ETAG__c = objMappingRecord.sEtag;
                                    if (objMappingRecord.sMediaId != null) objMappingsData.Media_Id__c = objMappingRecord.sMediaId;
                                    if (objMappingRecord.sName != null) objMappingsData.Name__c = objMappingRecord.sName;
                                    if (objMappingRecord.bIsDELETED == true) {
                                        objMappingsData.Record_Status__c = sDELETED; 
                                    } else {
                                        objMappingsData.Record_Status__c = sACTIVE;   
                                    }
                                    if (objMappingRecord.sVideoId != null) objMappingsData.Google_Id__c = objMappingRecord.sVideoId;
                                    if (objMappingRecord.sSeasonNumber != null) objMappingsData.Season_Number__c = objMappingRecord.sSeasonNumber;
                                    if (objMappingRecord.sSeriesTitle != null) objMappingsData.Series_Title__c = objMappingRecord.sSeriesTitle;
                                    if (objMappingRecord.sSeriesId != null) objMappingsData.Series_Id__c = objMappingRecord.sSeriesId;
                                    if (objMappingRecord.sSeriesMediaId != null) objMappingsData.Series_Media_Id__c = objMappingRecord.sSeriesMediaId;
                                    if (objMappingRecord.sSeriesBigId != null) objMappingsData.Series_Big_Id__c = objMappingRecord.sSeriesBigId;
                                    if (objMappingRecord.s1stEpisodeASIN != null) objMappingsData.X1st_Episode_ASIN__c = objMappingRecord.s1stEpisodeASIN;
                                    if (objMappingRecord.sSeasonASIN != null) objMappingsData.Season_ASIN__c = objMappingRecord.sSeasonASIN;
                                    if (objMappingRecord.sEpisodeNumber != null) objMappingsData.Episode_Number__c = objMappingRecord.sEpisodeNumber;
                                    objMappingsData.Catalog__c = sCatalogId;
                                    objMappingsData.HEP_Territory__c = IdRegion;
                                    objMappingsData.HEP_Customer__c = IdCustomer;
                                    lstRecordsToUpsert.add(objMappingsData);
                                }
                        }
                    //}// END If -> WRITE ACCESS
                    System.debug('RECORDS TO UPSERT : ');
                    upsert lstRecordsToUpsert;
                }
            }
        }
        
        /**
         *ProductMappingWrapper - Outer Wrapper that holds list of records to display + other data common to records
         *@author : Ashutosh Arora
        */
        public class ProductMappingWrapper {
            public List < ProductDataWrapper > lstProductData;
            public List < String > lstCustomers;
            public List < String > lstRegions;
            public String sMappingType;
            public string sSelectedRegion;
            public string sSelectedRegionId;
            public string sSelectedCustomer;
            public string sIdCatalog;
            //public string sPrimaryCatalogId;
            public map <String,String> mapTerritoryAccess;
            public map <String,String> parentTerritoryAccess;
            public ProductMappingWrapper() {
                this.lstProductData = new List < ProductDataWrapper > ();
                this.lstCustomers = new List < String > ();
                this.lstRegions = new List < String > ();
            }
        }
        /**
        *ProductDataWrapper - Wrapper class for storing individual records
        *@author : Ashutosh Arora
        */
        public class ProductDataWrapper {
            public string sRecordId;
            public string sChannel;
            public string sFormat;
            public string sName;
            public string sASIN;
            public string sADAMId;
            public string sComponentId;
            public string sMediaId;
            public string sCustomerId;
            public string sBigId;
            public string sEidr;
            public string sEtag;
            public string sVideoId;
            public string sSeasonNumber;
            public string sSeriesTitle;
            public string sSeriesId;
            public string sSeriesMediaId;
            public string sSeriesBigId;
            public string s1stEpisodeASIN;
            public string sSeasonASIN;
            public string sEpisodeNumber;
            public string sLocalDescription;
            public map < String, LastModifiedField > mapFieldHistory;
            public boolean bIsDELETED;
            public boolean bIdADAMUnique;
            public boolean bLocalDescUnique;
            public boolean bUniqueVideoId;
            public boolean bMediaIdUnique;
            public boolean bBigIdUnique;
            public boolean bUniqueASIN;
            public boolean bUnique1stEpASIN;
            public boolean bUniqueSeasonASIN;

            public ProductDataWrapper() {
                mapFieldHistory = new map < String, LastModifiedField > ();
            }
        }
        /**
        *LastModifiedField - Wrapper class for storing Field History Data
        *@author : Ashutosh Arora
        */
        public class LastModifiedField {
            public date dtLastModifiedDate;
            public string sLastModifiedBy;
            public string sFieldName;
            public string sRecordId;
            public string sLastModifiedDate;
            public string sLastModifiedData;
        }
        
    }