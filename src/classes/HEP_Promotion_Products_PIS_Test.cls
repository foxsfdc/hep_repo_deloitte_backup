@isTest
public class HEP_Promotion_Products_PIS_Test {

	@testSetup
	static void createUsers(){
		User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Elango', 'elango15@gmail.com','Elango','Elann','E','', true);
		List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
		HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Promotions' ,false);
		objRole.Destination_User__c = u.Id;
		objRole.Source_User__c = u.Id;  
		insert objRole;
	}

	static TestMethod void testfetchSKURecords(){

		User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'Elango'];
		HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id];

		//Inserting Territory Record
		HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182', HEP_Utility.getConstantValue('HEP_JDE'), true);
		
		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

		//Inserting User Role Record
		system.debug('TERRITORY ID...'+objTerritory.Id);
		system.debug('ROLE ID...'+objRole.Id);
		system.debug('USER ID...'+u.Id);
		HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, true);
		String sLocaleDateFormat =  u.DateFormat__c;

		System.runAs(u){
		    
		    HEP_Notification_Template__c objNotificationTemplate = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'),HEP_Utility.getConstantValue('HEP_PROMOTION') ,'A promotion has been created', 'Type', 'Active','-','sUnique',true);

			HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true);
			HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objTerritory.Id, 'Draft', 'Request', true);

			HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objPromotion.Id, null, true);
			HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '456', 'New SKU', 'Master', 'Physical', 'DD', 'DVD', false);
			objSKU.SKU_Configuration__c = '3D_DD';
			objSKU.No_Of_Discs__c = 3;
			insert objSKU;
            HEP_SKU_Master__c objSKU1 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '4567', 'New SKU 1', 'Request', 'Physical', 'DD', 'DVD', false);
            objSKU1.SKU_Configuration__c = '3D_DD';
            insert objSKU1;
            
            HEP_SKU_Master__c objSKU2 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '4568', 'New SKU 2', 'Master', 'Digital', 'DD', 'DVD', false);
            objSKU2.SKU_Configuration__c = 'Dig Ins_4K';
            insert objSKU2;
            
			HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id, HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'), 'Unlocked', true);
			HEP_Promotion_SKU__c objPromotionSKU1 = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU1.Id, HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'), 'Unlocked', true);
			HEP_Promotion_SKU__c objPromotionSKU2 = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU2.Id, HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'), 'Unlocked', true);
		    
		    HEP_SKU_BOM__c objSKUBOM = HEP_Test_Data_Setup_Utility.createSKUBOM('SKU BOM 1',objSKU.Id, objSKU1.Id, 'Active',true);
		    Test.startTest();
			HEP_Promotion_Products_PIS.ProductPIS objWrapperReturned = HEP_Promotion_Products_PIS.fetchSKURecords(objPromotion.Id);
			System.assertEquals(objPromotion.Id, objWrapperReturned.sPromotionID);
			Test.stopTest();
		}

	}

}