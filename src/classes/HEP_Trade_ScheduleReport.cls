/**
 * HEP_Trade_ScheduleReport --- Class to get the data for intl Trade schedule reports
 * @author    Ritesh Konduru
 */
public class HEP_Trade_ScheduleReport { //MP-820

    public static String sACTIVE;
    //public static String sPhysical = 'Physical';
    static {
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    /**
     * Trade Sch wrap --- Wrapper class to hold terriroy, date and multiple records data
     * @author    Ritesh Konduru
     */
    public class HEP_TradeScheduleWrap {

        public String dtStartDate;
        public String dtEndDate;
        public List < HEP_Utility.MultiPicklistWrapper > lstTerrioryWrap; //listTerrioryWrap
        public List < InternalScheduleWrapData > lstInternalScheduleWrapData; //listIntSchlWrapData
    }
    /**
     * Main Wrapper --- Wrapper class to hold one record 
     * @author    Ritesh Konduru
     */
    public class InternalScheduleWrapData {
        public String sSKUMasterId;
        public String sSKUPromotionId;
        public String sSKUMasterStartDate;
        public String sWeeksOfRelease;
        public String sTitleName; //Name   SKU Master
        public String sTitleFormat; //Format 
        public String sTitleChannel; //Channel  
        public String sTitleGenre; // catalog genre object
        public String sLicensorType; // Liscensor Catalog 
        public String sReleaseDate; // release data SKU master
        public String sSKU; // 
        public String sBarcode;
        public String sRegion; //territory 

        //different Fields Trade
        public String sOrderCutOffDate;
        public String sDigitalPhysical;
        public String sUKRating;
        public String sIrishRating;
        public String sSKUDLP;

    }
    /**
     * Price Grade Wrapper --- Wrapper class to hold price grade information under sKU
     * @author    Ritesh Konduru
     */
    public class HEPPriceGradeWrap {
        public String sPriceGradeId;
        public String sDelearListPrice;
        public String sTerritoryName;
        public String sMasterSKUId;
        public String sPromotionSKUId;
        public String sPriceGrade;
        public Date sStartDate;
    }
    /*
    Method: loading territory on the initial Load
    return: String
    Author: Ritesh Konduru
    **/
    @RemoteAction
    public static string territoryLoad() {

        List < HEP_Utility.MultiPicklistWrapper > lstTerritoryWrap = new List < HEP_Utility.MultiPicklistWrapper > ();
        HEP_TradeScheduleWrap objWrapData = new HEP_TradeScheduleWrap();
        try {

            for (HEP_territory__c objTerritory: [SELECT id,Name FROM HEP_territory__c WHERE Name != :HEP_Utility.getConstantValue('REGION_HOME_OFFICE') AND Parent_Territory__c = null ORDER BY Name]) {
                lstTerritoryWrap.add(new HEP_Utility.MultiPicklistWrapper(objTerritory.Name, objTerritory.id, false));
            }

            objWrapData.dtStartDate = string.valueof(date.today());
            objWrapData.lstTerrioryWrap = lstTerritoryWrap;

            String jsonData = JSON.serialize(objWrapData);
            System.debug('jsonData++' + jsonData);
            return jsonData;
        } catch (Exception e) {
            system.debug('Exception Line Number ' + e.getLineNumber() + ' Exception ' + e);
            throw e;
        }

    }
    /*
    Method: Loads reports data according to the parameters values.
    return: String
    Author: Ritesh Konduru
    **/
    @RemoteAction
    public static string initialLoad(String sStartDate, String sEndDate, List < HEP_Utility.MultiPicklistWrapper > lstSelecteTerritory) { //Need to get paramater from ui and add if contition

        Date dtStartDate;
        Date dtEndDate;
        list < String > lstSelectedTerritories = new list < String > ();
        HEP_TradeScheduleWrap objWrapData = new HEP_TradeScheduleWrap();
        List < InternalScheduleWrapData > lstReleaseSchWrap = new List < InternalScheduleWrapData > ();
        List < InternalScheduleWrapData > lstReleaseSchWraptoUI = new List < InternalScheduleWrapData > ();
        set < String > setTempIdsCatalog = new set < String > ();
        set < String > setTempIdsPriceGrade = new set < String > ();
        Map < String, String > mapGenreCatalogSKURec = new Map < String, String > ();
        Map < String, List<HEP_SKU_Price__c> > mapOfPromotionSKUIdToPriceGrade = new Map < String, List<HEP_SKU_Price__c> >();
        List < HEPPriceGradeWrap > lstPGCatalogRec = new List < HEPPriceGradeWrap > ();
        set < Id > setPriceGradePromotionIds = new set < id > ();
        try {

            if (String.isNotBlank(sStartDate)) {
                dtStartDate = date.valueOf(sStartDate);
            }
            if (String.isNotBlank(sEndDate)) {
                dtEndDate = date.valueOf(sEndDate);
            }
            System.debug('dtStartDate ' + dtStartDate + 'dtEndDate ' + dtEndDate);
            if (lstSelecteTerritory != null && lstSelecteTerritory.size() > 0) {
                for (HEP_Utility.MultiPicklistWrapper tempTerr: lstSelecteTerritory) {
                    if (tempTerr.bSelected == true)
                        lstSelectedTerritories.add(tempTerr.sKey);

                }
            }
            System.debug('lstSelectedTerritories ' + lstSelectedTerritories);

            List < HEP_Promotion_SKU__c > lstPromotionSKURec = [select SKU_Master__r.Format__c,
                SKU_Master__r.Channel__c,
                Promotion_Catalog__r.Catalog__r.id,
                Promotion_Catalog__r.Catalog__r.Licensor_Type__c,
                SKU_Master__r.Current_Release_Date__c,
                SKU_Master__r.SKU_Title__c,
                Digital_Physical__c,
                SKU_Master__r.SKU_Number__c,
                SKU_Master__r.Barcode__c,
                SKU_Master__r.Territory__r.Name,
                SKU_Master__r.id,
                SKU_Master__r.JDE_MPAA_Rating__c,
                SKU_Master__r.INTL_Secondary_Rating__c,
                SKU_Master__r.Catalog_Master__r.CatalogId__c,
                Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Name,
                Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
                Promotion_Catalog__r.Promotion__r.LineOfBusiness__r.Type__c,
                Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c,
                Promotion_Catalog__r.Catalog__r.Name,
                (select id, PriceGrade__r.Dealer_List_Price__c, SKU_Master__c, Promotion_SKU__c, Start_Date__c, Region__r.Name, PriceGrade__r.PriceGrade__c from HEP_SKU_Region_Prices__r where Promotion_SKU__c IN: setTempIdsPriceGrade AND Record_Status__c =: sACTIVE)
                from HEP_Promotion_SKU__c where SKU_Master__r.Territory__c IN: lstSelectedTerritories and SKU_Master__r.Current_Release_Date__c >=: dtStartDate and SKU_Master__r.Current_Release_Date__c <=: dtEndDate and Record_Status__c =: sACTIVE];


            //creating a map with catalog id's inorder to make a query and get genres.                                                     
            for (HEP_Promotion_SKU__c tempforGenre: lstPromotionSKURec) {
                if (tempforGenre.Promotion_Catalog__r.Catalog__r != null && tempforGenre.Promotion_Catalog__r != null) {

                    setTempIdsCatalog.add(tempforGenre.Promotion_Catalog__r.Catalog__r.id);
                }

                setTempIdsPriceGrade.add(tempforGenre.id);
            }
            system.debug('----->setTempIdsCatalog'+setTempIdsCatalog);
            //iterating through genres records and getting genre info
            List < HEP_Catalog__c > lstCatalogRec = [select id, (select id, Genre__c From Genres__r WHERE Record_Status__c =: sACTIVE) from HEP_Catalog__c where Id IN: setTempIdsCatalog and Record_Status__c =: sACTIVE];

            for (HEP_Catalog__c objCatalog: lstCatalogRec) {
                String sGenre = '';
                for (HEP_Catalog_Genre__c objGenre: objCatalog.Genres__r) {
                    if (objGenre.Genre__c != null) {
                        sGenre = sGenre + objGenre.Genre__c + ',';
                    }

                }
                sGenre = sGenre.trim().removeEnd(',');
                mapGenreCatalogSKURec.put(objCatalog.id, sGenre);

            }
            System.debug('mapGenreCatalogSKURec ------>' + mapGenreCatalogSKURec);


            //creating map for pricegrade object
           // List < HEP_SKU_Price__c > lstPriceGradeRec = 

            for (HEP_SKU_Price__c objPG: [select id, PriceGrade__r.Dealer_List_Price__c, SKU_Master__c, Promotion_SKU__c, Start_Date__c, Region__r.Name, PriceGrade__r.PriceGrade__c from HEP_SKU_Price__c where Promotion_SKU__c IN: setTempIdsPriceGrade AND Record_Status__c =: sACTIVE]) {
                if(!mapOfPromotionSKUIdToPriceGrade.containskey(objPG.Promotion_SKU__c)){
                    List<HEP_SKU_Price__c> lstSKUPrice = new List<HEP_SKU_Price__c>();
                    lstSKUPrice.add(objPG);
                    mapOfPromotionSKUIdToPriceGrade.put(objPG.Promotion_SKU__c,lstSKUPrice);
                }
                else{
                    mapOfPromotionSKUIdToPriceGrade.get(objPG.Promotion_SKU__c).add(objPG);
                }

              /*  HEPPriceGradeWrap objPriceGrade = new HEPPriceGradeWrap();

                objPriceGrade.sPriceGradeId = objPG.id;
                objPriceGrade.sTerritoryName = objPG.Region__r.Name;
                objPriceGrade.sDelearListPrice = string.valueof(objPG.PriceGrade__r.Dealer_List_Price__c);

                objPriceGrade.sPriceGrade = objPG.PriceGrade__r.PriceGrade__c;
                objPriceGrade.sStartDate = objPG.Start_Date__c;
                objPriceGrade.sMasterSKUId = objPG.SKU_Master__c;
                objPriceGrade.sPromotionSKUId = objPG.Promotion_SKU__c;
                lstPGCatalogRec.add(objPriceGrade); */
            }
            System.debug('lstPGCatalogRec------> ' + lstPGCatalogRec);
            System.debug('----lstPGCatalogRec---- size'+lstPGCatalogRec.size());

            for (HEP_Promotion_SKU__c hepRec: lstPromotionSKURec) {
                InternalScheduleWrapData wrapGridData = new InternalScheduleWrapData();
                wrapGridData.sSKUMasterId = hepRec.SKU_Master__r.id;
                wrapGridData.sSKUMasterStartDate = String.valueOf(hepRec.SKU_Master__r.Current_Release_Date__c);
                wrapGridData.sSKUPromotionId = hepRec.id;
                wrapGridData.sTitleName = hepRec.SKU_Master__r.SKU_Title__c;
                wrapGridData.sTitleFormat = hepRec.SKU_Master__r.Format__c;
                wrapGridData.sTitleChannel = hepRec.SKU_Master__r.Channel__c;
                if (mapGenreCatalogSKURec.containsKey(hepRec.Promotion_Catalog__r.Catalog__r.id)) {
                    wrapGridData.sTitleGenre = mapGenreCatalogSKURec.get(hepRec.Promotion_Catalog__r.Catalog__r.id); //Pending
                    //System.debug('wrapGridData.sTitleGenre ' + wrapGridData.sTitleGenre);
                }

                wrapGridData.sLicensorType = hepRec.Promotion_Catalog__r.Catalog__r.Licensor_Type__c;
                wrapGridData.sSKU = hepRec.SKU_Master__r.SKU_Number__c;
                wrapGridData.sBarcode = hepRec.SKU_Master__r.Barcode__c;
                wrapGridData.sRegion = hepRec.SKU_Master__r.Territory__r.Name;
                wrapGridData.sDigitalPhysical = hepRec.Digital_Physical__c;
                wrapGridData.sUKRating = hepRec.SKU_Master__r.JDE_MPAA_Rating__c;
                wrapGridData.sIrishRating = hepRec.SKU_Master__r.INTL_Secondary_Rating__c;
                lstReleaseSchWrap.add(wrapGridData);
            }
            //Integer count = 1;
            System.debug('lstReleaseSchWrap---> ' + lstReleaseSchWrap);
            System.debug('----lstReleaseSchWrap---- size'+lstReleaseSchWrap.size());
            //Adding multiple price grades for multiple master sku's
            //if (lstPGCatalogRec.size() > 0 && lstReleaseSchWrap != null && lstReleaseSchWrap.size() > 0) {
                //for (HEPPriceGradeWrap objWrapPG: lstPGCatalogRec) {
                    for (InternalScheduleWrapData objwrapDataTemp: lstReleaseSchWrap) {
                        //InternalScheduleWrapData tempIntSchlData = new InternalScheduleWrapData(); //= objwrapDataTemp;
                       // if (objWrapPG != null && objwrapDataTemp != null && (objWrapPG.sPromotionSKUId).equals(objwrapDataTemp.sSKUPromotionId)) {

                            //tempIntSchlData.sSKUDLP = objWrapPG.sDelearListPrice; //Pending
                            /*if (objWrapPG.sStartDate != null) {
                                
                                //tempIntSchlData.sReleaseDate = string.valueof(objWrapPG.sStartDate); //Pending
                                Integer weeks = date.today().daysBetween(objWrapPG.sStartDate);
                                if(weeks >= 0)
                                    tempIntSchlData.sWeeksOfRelease = (weeks / 7) + ''; //Pending -today
                                else
                                    tempIntSchlData.sWeeksOfRelease = 0 + '';
                                if (String.isNotBlank(objwrapDataTemp.sDigitalPhysical) && HEP_Constants__c.getValues('HEP_PHYSICAL_MEDIA_TYPE').Value__c.equals(objwrapDataTemp.sDigitalPhysical)) { //Digital_Physical__c
                                    tempIntSchlData.sOrderCutOffDate = string.valueof(objWrapPG.sStartDate.addDays(-17)); //Pending Release data -17
                                }
                            } */
                        if(mapOfPromotionSKUIdToPriceGrade.ContainsKey(objwrapDataTemp.sSKUPromotionId)){   
                            for(HEP_SKU_Price__c objPrice :  mapOfPromotionSKUIdToPriceGrade.get(objwrapDataTemp.sSKUPromotionId)){
                                InternalScheduleWrapData tempIntSchlData = new InternalScheduleWrapData();
                                if(objPrice.Start_Date__c != null){
                                    tempIntSchlData.sOrderCutOffDate = string.valueof(objPrice.Start_Date__c.addDays(-17));
                                    Integer weeks = date.today().daysBetween(objPrice.Start_Date__c);
                                    if(weeks >= 0)
                                        tempIntSchlData.sWeeksOfRelease = (weeks / 7) + ''; //Pending -today
                                    else
                                        tempIntSchlData.sWeeksOfRelease = 0 + '';
                                }
                                if(objPrice.PriceGrade__r.Dealer_List_Price__c != null)    
                                    tempIntSchlData.sSKUDLP = String.valueOf(objPrice.PriceGrade__r.Dealer_List_Price__c);
                                                                                                
                                tempIntSchlData.sReleaseDate = objwrapDataTemp.sSKUMasterStartDate;
                                tempIntSchlData.sSKUMasterId = objwrapDataTemp.sSKUMasterId;
                                tempIntSchlData.sSKUPromotionId = objwrapDataTemp.sSKUPromotionId;
                                tempIntSchlData.sTitleName = objwrapDataTemp.sTitleName;
                                tempIntSchlData.sTitleFormat = objwrapDataTemp.sTitleFormat;
                                tempIntSchlData.sTitleChannel = objwrapDataTemp.sTitleChannel;
                                tempIntSchlData.sTitleGenre = objwrapDataTemp.sTitleGenre;
                                tempIntSchlData.sLicensorType = objwrapDataTemp.sLicensorType;
                                tempIntSchlData.sSKU = objwrapDataTemp.sSKU;
                                tempIntSchlData.sBarcode = objwrapDataTemp.sBarcode;
                                tempIntSchlData.sRegion = objwrapDataTemp.sRegion;
                                tempIntSchlData.sDigitalPhysical = objwrapDataTemp.sDigitalPhysical;

                                tempIntSchlData.sUKRating = objwrapDataTemp.sUKRating;
                                tempIntSchlData.sIrishRating = objwrapDataTemp.sIrishRating;
                                lstReleaseSchWraptoUI.add(tempIntSchlData);
                            }
                        } 
                        else{
                            InternalScheduleWrapData tempIntSchlData = new InternalScheduleWrapData();
                            tempIntSchlData.sReleaseDate = objwrapDataTemp.sSKUMasterStartDate;
                                tempIntSchlData.sSKUMasterId = objwrapDataTemp.sSKUMasterId;
                                tempIntSchlData.sSKUPromotionId = objwrapDataTemp.sSKUPromotionId;
                                tempIntSchlData.sTitleName = objwrapDataTemp.sTitleName;
                                tempIntSchlData.sTitleFormat = objwrapDataTemp.sTitleFormat;
                                tempIntSchlData.sTitleChannel = objwrapDataTemp.sTitleChannel;
                                tempIntSchlData.sTitleGenre = objwrapDataTemp.sTitleGenre;
                                tempIntSchlData.sLicensorType = objwrapDataTemp.sLicensorType;
                                tempIntSchlData.sSKU = objwrapDataTemp.sSKU;
                                tempIntSchlData.sBarcode = objwrapDataTemp.sBarcode;
                                tempIntSchlData.sRegion = objwrapDataTemp.sRegion;
                                tempIntSchlData.sDigitalPhysical = objwrapDataTemp.sDigitalPhysical;
                                tempIntSchlData.sUKRating = objwrapDataTemp.sUKRating;
                                tempIntSchlData.sIrishRating = objwrapDataTemp.sIrishRating;
                                
                                lstReleaseSchWraptoUI.add(tempIntSchlData);
                            
                        }   
                    }

                //} //for end lstReleaseSchWrap
            //}    //} //For End lstPGCatalogRec 

                //Adding sKU's which dosent have Price Grade Records.we should not use NotNull/Break contition in above loops because there may be multiple pricegrades
            /*    for (HEPPriceGradeWrap objWrapPG1: lstPGCatalogRec) {
                    Id tempIDPromotionSKUId = objWrapPG1.sPromotionSKUId;
                    setPriceGradePromotionIds.add(tempIDPromotionSKUId);
                }
                System.debug('setPriceGradePromotionIds ' + setPriceGradePromotionIds);
                for (InternalScheduleWrapData objwrapDataTemp1: lstReleaseSchWrap) {
                    Id tempIDSKUPromotionId = objwrapDataTemp1.sSKUPromotionId;

                    if (!setPriceGradePromotionIds.contains(tempIDSKUPromotionId)) {
                        lstReleaseSchWraptoUI.add(objwrapDataTemp1);

                    }
                } */
             //size end
            objWrapData.lstInternalScheduleWrapData = lstReleaseSchWraptoUI;

            String jsonData = JSON.serialize(objWrapData);
            System.debug('jsonData++' + jsonData);
            return jsonData;
        } catch (Exception e) {
            system.debug('Exception Line Number ' + e.getLineNumber() + ' Exception ' + e);
            throw e;
        }
    }
}