@isTest(SEEALLDATA = false)
private class HEP_Promotion_Spend_Test {
	@TestSetup
	static void testSetupMethod(){
		list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();

        HEP_Interface__c objService = new HEP_Interface__c(Name = 'HEP_E1_SpendDetail',
        												   Source__c = 'HEP',
        												   Destination__c = 'E1',
        												   Type__c = 'Outbound-Push',
        												   Retry_Interval__c = 3,
        												   Class__c = 'HEP_E1_Spend_Interface',
        												   Record_Status__c = 'Active',
        												   Integration_Name__c = 'HEP_E1_SpendDetail');
        insert objService;

		//Create User
		User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
															  'test@deloitte.com','testL','testL','Fox - New Release',true);

		//Create Line of Business
		HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);

		//Create Role
		HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager - TV','Promotion',true);

		//Create EDM Product Type.
		EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

		//Create EDM Title Record.
		EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);

		//Create Territory
		List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
																		HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
																		HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false)};

		lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
		lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
		lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
		insert lstTerritory;
		System.debug('Territory : ' + lstTerritory);

		//Create User Role
		List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,objUser.Id,false),
																		 HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,objUser.Id,false),
																		 HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,objUser.Id,false)};

		insert lstUserRoles;

		//Insert Spend Templates
		List<HEP_Market_Spend_Template__c> lstSpendTemplates = new List<HEP_Market_Spend_Template__c>{HEP_Test_Data_Setup_Utility.createSpendTemplate('Global Template',null,'300',lstTerritory[0].Id,false),
																									  HEP_Test_Data_Setup_Utility.createSpendTemplate('Germany Template',null,'301',lstTerritory[1].Id,false),
																									  HEP_Test_Data_Setup_Utility.createSpendTemplate('DHE Template',null,'302',lstTerritory[2].Id,false)};

		
		lstSpendTemplates[0].Unique_Id__c = lstSpendTemplates[0].Name + ' / Global';
		lstSpendTemplates[1].Unique_Id__c = lstSpendTemplates[1].Name + ' / Germany';
		lstSpendTemplates[2].Unique_Id__c = lstSpendTemplates[1].Name + ' / DHE';
		System.debug(lstSpendTemplates);
		insert lstSpendTemplates;

		//Create Template LOB
		List<HEP_Template_Line_Of_Business__c> lstTemplateLOBs = new List<HEP_Template_Line_Of_Business__c>{HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[0].Id , false),
																										    HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[1].Id , false),
																										    HEP_Test_Data_Setup_Utility.createTemplateLOB(objLineOfBusiness.Id , lstSpendTemplates[2].Id , false)};

		insert lstTemplateLOBs;

		//Create GL Account
		List<HEP_GL_Account__c> lstGLAccount = new List<HEP_GL_Account__c>{HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 1',null,null,null,'Active',false),
																		   HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 2',null,null,null,'Active',false),
																		   HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 3',null,null,null,'Active',false),
																		   HEP_Test_Data_Setup_Utility.createGLAccount('GL Account 4',null,null,null,'Active',false)};

	    lstGLAccount[0].Account_Department__c = 'Department 1';
	    lstGLAccount[1].Account_Department__c = 'Department 2';
	    lstGLAccount[2].Account_Department__c = 'Department 3';
	    lstGLAccount[3].Account_Department__c = 'Department 4';

	    insert lstGLAccount;

	    //Create Spend Template Details
	    List<HEP_Market_Spend_Template_Detail__c> lstSpendTemplateDetails = new List<HEP_Market_Spend_Template_Detail__c>{HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[0].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[0].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[0].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[0].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[1].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[1].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[1].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[1].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[0].Id,lstSpendTemplates[2].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[1].Id,lstSpendTemplates[2].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('Test Name',12000,lstGLAccount[2].Id,lstSpendTemplates[2].Id , false),
																														  HEP_Test_Data_Setup_Utility.createSpendTemplateDetails('NTS',12000,lstGLAccount[3].Id,lstSpendTemplates[2].Id , false)};

	    insert lstSpendTemplateDetails;

		//Create Promotion
		List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
																		  HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
																		  HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)};
        
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;
        insert lstPromotions;

        //Create Catalog
        List<HEP_Catalog__c> lstCatalogs = new List<HEP_Catalog__c>{HEP_Test_Data_Setup_Utility.createCatalog('11111','Global Catalog','Single',null,lstTerritory[0].Id, 'Approved','Master',false),
    																HEP_Test_Data_Setup_Utility.createCatalog('11111','Germany Catalog','Single',null,lstTerritory[1].Id, 'Approved','Master',false),
    																HEP_Test_Data_Setup_Utility.createCatalog('11111','DHE Catalog','Single',null,lstTerritory[2].Id, 'Approved','Master',false)};

		lstCatalogs[0].Title_EDM__c = objTitle.Id;
		lstCatalogs[1].Title_EDM__c = objTitle.Id;
		lstCatalogs[2].Title_EDM__c = objTitle.Id;
		insert lstCatalogs[0];
		lstCatalogs[1].Global_Catalog__c = lstCatalogs[0].Id;
		lstCatalogs[2].Global_Catalog__c = lstCatalogs[0].Id;

		insert new List<HEP_Catalog__c>{lstCatalogs[1] , lstCatalogs[2]};

		//Create PromotionCatalog
		List<HEP_Promotion_Catalog__c> lstPromotionCatalogs = new List<HEP_Promotion_Catalog__c>{HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[0].Id,lstPromotions[0].Id,null,false),
																								 HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[1].Id,lstPromotions[1].Id,null,false),
																								 HEP_Test_Data_Setup_Utility.createPromotionCatalog(lstCatalogs[2].Id,lstPromotions[2].Id,null,false)};

		insert lstPromotionCatalogs;

		//Create User Favourite Records:
		HEP_User_Favourites__c objuserFavourite = new HEP_User_Favourites__c(User__c = UserInfo.getUserId() , Record_Id__c = lstPromotions[0].Id);
		insert objuserFavourite;

		//Create Notification Record..
		HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','<Date_Type__c>','test type' ,'Active','Approved', 'test',true);

		HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Avail Date', lstTerritory[0].Id, lstTerritory[1].Id, true);
        HEP_Promotion_Dating__c objPromDating = HEP_Test_Data_Setup_Utility.createDatingRecord(lstTerritory[0].Id, lstPromotions[0].Id, objDatingMatrix.id,false);
        objPromDating.Date_Type__c = 'EST Release Date';
        insert objPromDating;

        /*//Create ApprovalType Record:
        HEP_Utility.createApprovalType('DHE Spend', string sApprovalObject, string sApprovalField, string sApprovedStatus, 
                                                          string sRejectedStatus, string sType, string sRoutingType, string sInProgressStatus, boolean bInsert);*/

        HEP_Notification_Utility.createNotifications(new List<HEP_Promotion_Dating__c>{objPromDating} , HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change') , 'HEP_Promotion__c');
        HEP_Notification_Utility.createNotifications(new List<HEP_Promotion_Dating__c>{objPromDating} , 'Promotion_Dating_ReConfirmed' , 'HEP_Promotion__c');
	}

	@isTest static void test_method_one() {

		Test.setCurrentPageReference(new PageReference('Page.HEP_Promotion_Spend'));
		List<HEP_Promotion__c> lstPromotions = [SELECT Id , Name FROM HEP_Promotion__c];
		System.currentPageReference().getParameters().put('promoId', lstPromotions[0].Id);
		HEP_Promotion_Spend_Controller objPromotionSpend = new HEP_Promotion_Spend_Controller();

		List<HEP_Market_Spend_Template__c> lstSpendTemplates = [SELECT Id , Name FROM HEP_Market_Spend_Template__c];

		HEP_Promotion_Spend_Controller.getNTSPurchaseOrder(lstPromotions[1].Id, 'US', '1234' , 
                                                     '119157' , 'DHE' , '123' , '123'
                                                     , 'Pending' , lstSpendTemplates[0].Id);

		HEP_Promotion_Spend_Controller.getNTSPurchaseOrder(lstPromotions[1].Id, 'US', '1234' , 
                                                     '119157' , 'Germany' , '' , ''
                                                     , 'Pending' , lstSpendTemplates[0].Id);

		HEP_Promotion_Spend_Controller.getPurchaseOrderAndInvoice('sPromotionId', 'sPromotionTerritory', 'sPromotionLocalPromoCode' , 'sSpendStatus');
		HEP_Promotion_Spend_Controller.getPurchaseOrder(lstPromotions[1].Id, 'DHE', '27933' , 'Pending');
		HEP_Promotion_Spend_Controller.getPurchaseOrder(lstPromotions[1].Id, 'Germany', '27933' , 'Pending');
		HEP_Promotion_Spend_Controller.getInvoice(lstPromotions[1].Id, 'Germany', '40550', 'Pending');

		HEP_Promotion_Spend_Controller.SpendPageDataWrapper objSpendWrap = HEP_Promotion_Spend_Controller.fetchSpendPageData(lstPromotions[1].Id);

		
		//Create a Spend Record.
		/*HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234',lstPromotions[1].Id , 'Pending' , true);
		lstPromotions[1].HEP_Market_Spend_Template__c = [SELECT Id FROM HEP_Market_Spend_Template__c][0].Id;
		update lstPromotions[1];*/
		HEP_Integration_Util.upDateExistingorAddNew(lstPromotions[1].Id , 'sNTSPOIntegration' , HEP_Utility.getConstantValue('HEP_NTSPurchaseOrderIntegration'));
		objSpendWrap.objSelectedTemplate = new HEP_Utility.PicklistWrapper([SELECT Id FROM HEP_Market_Spend_Template__c][0].Id , 'test');
		objSpendWrap.objSelectedTemplate.sKey = [SELECT Id FROM HEP_Market_Spend_Template__c][0].Id;
		HEP_Promotion_Spend_Controller.saveSpendRequest(objSpendWrap);


		HEP_Promotion_Spend_Controller.NTSResponseWrapper obj = new HEP_Promotion_Spend_Controller.NTSResponseWrapper();
		HEP_Promotion_Spend_Controller.SpendPageDataWrapper objSpendWrap1 = HEP_Promotion_Spend_Controller.fetchSpendPageData(lstPromotions[1].Id);

		//Create Approval Records.
		HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test','HEP_Market_Spend__c','RecordStatus__c','Approved','Rejected','SPEND','Direct','Pending',true);
		HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalType.Id, null , false);
		objApproval.Record_ID__c = objSpendWrap1.sSpendId;
		objApproval.HEP_Market_Spend__c = objSpendWrap1.sSpendId;
		objApproval.History_Data__c = JSON.serializePretty(new HEP_Promotion_Spend_Controller.ApprovalHistory('Test Comment' , 1000, 2000));
		insert objApproval;
		HEP_Record_Approver__c objRecordApprovers = HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, UserInfo.getUserId(), [SELECT ID FROM HEP_Role__c][0].Id, 'Pending',true);

		Test.startTest();
		HEP_Promotion_Spend_Controller.saveSpendRequest(objSpendWrap1);

		HEP_Promotion_Spend_Controller.getApprovalHierarchy(objSpendWrap1.sSpendId);
		Test.stopTest();

	}
	
	@isTest static void test_method_two() {
		Test.setCurrentPageReference(new PageReference('Page.HEP_Promotion_Spend'));
		List<HEP_Promotion__c> lstPromotions = [SELECT Id , Name FROM HEP_Promotion__c WHERE PromotionName__c LIKE '%DHE%'];
		System.currentPageReference().getParameters().put('promoId', lstPromotions[0].Id);
		HEP_Promotion_Spend_Controller objPromotionSpend = new HEP_Promotion_Spend_Controller();

		List<HEP_Market_Spend_Template__c> lstSpendTemplates = [SELECT Id , Name FROM HEP_Market_Spend_Template__c];

		HEP_Promotion_Spend_Controller.getNTSInvoice(lstPromotions[0].Id, 'US', '1234' , 
                                                     '119157' , 'DHE' , '123' , '123'
                                                     , 'Pending' , lstSpendTemplates[0].Id);

		HEP_Promotion_Spend_Controller.getNTSInvoice(lstPromotions[0].Id, 'US', '1234' , 
                                                     '119157' , 'Germany' , '' , ''
                                                     , 'Pending' , lstSpendTemplates[0].Id);

		HEP_Promotion_Spend_Controller.NTSIntegrations(lstPromotions[0].Id, 'US', '1234' , '119157' , 'Germany' , '' , '', 'Pending' , lstSpendTemplates[0].Id);

		HEP_Promotion_Spend_Controller.fetchSpendPageData(lstPromotions[0].Id);
		HEP_Promotion_Spend_Controller.canLoggedUserAutoApprove('DHE');
	}
	
}