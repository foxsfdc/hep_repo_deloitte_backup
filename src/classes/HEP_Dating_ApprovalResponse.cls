/**
 * HEP_Dating_ApprovalResponse --- Dynamically fetch records in Email Template
 * @author    Nishit Kedia
 */

public class HEP_Dating_ApprovalResponse{
    
    //Promotion dating objDatingRecordApprover id is passed in this variable
    public Id recordApproverIdForDating {get; set;}
    public String sPageLink {get;set;}
    
    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    //This variable is used to render table on Email template
    public ApprovalEmailWrapper objApprovalwrapper{ 
        get{
            if(objApprovalwrapper == null){
                fetchPromotionDatingDetails();
            }
            return objApprovalwrapper;
        }
        set;
    }
     
    //Global variable      
    public  String  sRequestType = HEP_Utility.getConstantValue('HEP_DATING_REQUEST_TYPE');
    /**
    * fetchPromotionDatingDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author    Nishit Kedia
    */  
    public void fetchPromotionDatingDetails(){
        
        //------------------------------------//
        try{
            String sDatingPageUrl='/apex/HEP_Promotion_Details?promoId=';
            //Instantiate the Wrapper
            objApprovalwrapper = new ApprovalEmailWrapper();   
            //------------------------------------//      
            //sPageLink='https://foxgts--hep--c.cs70.visual.force.com/apex/HEP_Promotion_Dates_Dating_Clone';
            objApprovalwrapper.sStatus = HEP_Utility.getConstantValue('HEP_DATING_APPROVAL_MSG');
            if(recordApproverIdForDating != null){
                //-----------------------------------//
                //Query the objDatingRecordApprover based on objDatingRecordApprover Id               
                List<HEP_Record_Approver__c> lstRecordApproverForDating = [SELECT   Id,
                                                                                    Name,
                                                                                    Comments__c,
                                                                                    Approval_Record__r.CreatedDate, 
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.LocalPromotionCode__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Promotion__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Territory__r.Name,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Locked_Status__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Date__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.HEP_Promotions_Dating_Matrix__r.Date_Type__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.HEP_Catalog__r.Name
                                                                                    FROM HEP_Record_Approver__c
                                                                                    Where Id =: recordApproverIdForDating
                                                                                    AND Record_Status__c =:sACTIVE 
                                                                                    LIMIT 1];
                //-----------------------------------//
                //Validate the List and Populate the Wrapper
                if(lstRecordApproverForDating != null && !lstRecordApproverForDating.isEmpty()){    
                    //--------------------------------------//
                    System.debug('Dating List------->' + lstRecordApproverForDating);
                    HEP_Record_Approver__c objDatingRecordApprover = lstRecordApproverForDating.get(0);
                    objApprovalwrapper.sPromotionCode = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.LocalPromotionCode__c;
                    objApprovalwrapper.sPromotionName = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c;
                    objApprovalwrapper.sRequestType = sRequestType;  
                    DateTime dtRaisedRequsetTime = objDatingRecordApprover.Approval_Record__r.CreatedDate; 
                    
                    objApprovalwrapper.sRequestDate = dtRaisedRequsetTime.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    objApprovalwrapper.sComment = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c;
                    objApprovalwrapper.sRejectComment = objDatingRecordApprover.Comments__c;
                    //--------------------------------------//
                    //[MP-524][Starts]:Merge field Addition for Email template and Dynamic Page Link
                    String sBaseUrl =  System.URL.getSalesforceBaseUrl().toExternalForm();
                    String sUrlValue = sDatingPageUrl+objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__c;                  
                    String tabExtension = HEP_Utility.getConstantValue('HEP_DATES_TAB');
                    sPageLink = sBaseUrl + sUrlValue + tabExtension;
                    objApprovalwrapper.sDateType = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.HEP_Promotions_Dating_Matrix__r.Date_Type__c;
                    Date dtDatingRecord=objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Date__c;
                    if(dtDatingRecord != null){                     
                        Datetime dtDatingRecordCurrent = Datetime.newInstance(dtDatingRecord.year(),dtDatingRecord.month(),dtDatingRecord.day());                       
                        objApprovalwrapper.sCurrentDate = dtDatingRecordCurrent.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                    }                        
                    objApprovalwrapper.sCatalogName = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.HEP_Catalog__r.Name;
                    objApprovalwrapper.sRegion = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Territory__r.Name;                       
                    //[MP-524][Ends]:Merge field Addition for Email template and Dynamic Page Link
                    //--------------------------------------//
                    //On Basis Of Approval Send the status in Body                 
                    if(objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Locked_Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED'))){             
                        objApprovalwrapper.sStatus += ' ' + HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                        objApprovalwrapper.bRejectionstatus = false;
                    }else{              
                        objApprovalwrapper.sStatus += ' ' + HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
                        objApprovalwrapper.bRejectionstatus = true; 
                        objApprovalwrapper.sRejectionText = HEP_Utility.getConstantValue('HEP_REJECTION_TEXT');                        
                    }                       
                    
                }//-------------------------------//
                
            }//-----------------------------------//
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

       
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author    Nishit Kedia
    */
    public class ApprovalEmailWrapper{
        
        public String   sPromotionCode {get; set;}
        public String   sPromotionName {get; set;}
        public String   sRequestType   {get; set;}
        public String   sRequestDate   {get; set;}
        public String   sComment       {get; set;}
        public String   sStatus        {get; set;}
        public String   sRejectionText {get; set;}
        public Boolean  bRejectionstatus{get;set;}
        public String sRejectComment {get; set;}     
        // [MP-524]: New Fields in Wrapper for Template
        public String   sDateType      {get; set;}
        public String   sCurrentDate   {get; set;}
        public String   sCatalogName   {get; set;}
        public String   sRegion        {get; set;}             
    }

}