/**
 * Controller class to get transactions in a Department to display on Audit Trail Screen
 * @author  Anshul Jain
 */
public class HEP_Department_Spend_Trail_Controller {

    /**
     * CurrencyFormat in which the amount will be shown on the page 
     * @return nothing
     * @author Anshul Jain
     */
    public String sCurrencyFormat {
        get;
        set;
    }

    /**
     * Promotion Name that will be shown on the page for backButton 
     * @return nothing
     * @author Anshul Jain
     */
    public String sPromoName {
        get;
        set;
    }

    /**
     * Active flag used to get Active Records only
     * @return nothing
     * @author Anshul Jain
     */
    public static String sActive;


    /**
     * Write Permission flag used to get Write Permission
     * @return nothing
     * @author Anshul Jain
     */
    public static String sReadPermission;

    static {
        sReadPermission = HEP_Utility.getConstantValue('HEP_PERMISSION_READ');
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }

    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author Anshul Jain
     */
    public HEP_Department_Spend_Trail_Controller() {
        LIST < HEP_Promotion__c > lstPromotions = [SELECT ID, PromotionName__c, Territory__r.CurrencyCode__c
            FROM HEP_Promotion__c
            WHERE Record_Status__c =: sActive AND Id =: String.escapeSingleQuotes(ApexPages.currentpage().getparameters().get('promoId'))
        ];
        if (lstPromotions.size() > 0) {
            sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(lstPromotions[0].Territory__r.CurrencyCode__c);
            sPromoName = lstPromotions[0].PromotionName__c;

        }
    }



    /**
     * Helps to check if the current user has access to HEP_DEPARTMENT_SPEND_TRAIL.Page
     * @return Map of Permission Name and its value 
     */
    public PageReference checkPermissions() {
        LIST < HEP_Promotion__c > lstPromotions = [SELECT ID, PromotionName__c, Territory__c, Territory__r.Name, Territory__r.CatalogTerritory__c
            FROM HEP_Promotion__c
            WHERE Id =: ApexPages.currentpage().getparameters().get('promoId') AND Record_Status__c =: sActive
        ];
        map < String, String > mapCustomPermissions = HEP_Utility.fetchCustomPermissions(lstPromotions[0].Territory__c, 'HEP_DEPARTMENT_SPEND_TRAIL');
        if (mapCustomPermissions == NULL || mapCustomPermissions.isEmpty() || !mapCustomPermissions.get(sReadPermission).equalsIgnoreCase('true')) {
            return HEP_Utility.redirectToErrorPage();
        } else {
            return null;
        }
    }

    /**
     * Returns the object containing list of all transactions from Salesforce(Initial Budget and Budget Update) and Response(Attachment Object)
     * @param PromotionId , DepartmentName
     * @exception Any exception
     * @return DepartmentAudit class object.
     */

    @RemoteAction
    public static DepartmentAudit loadData(String sPromotionId, String sDepartmentId) {
        try {

            DepartmentAudit objDepartMent = new DepartmentAudit();
            String sDepartmentName;
            List < HEP_GL_Account__c > lstGlAccount = [Select Id, Account_Department__c FROM HEP_GL_Account__c Where Id =: String.escapeSingleQuotes(sDepartmentId)];
            if (lstGlAccount.size() > 0) {
                sDepartmentName = lstGlAccount[0].Account_Department__c;
            }

            if (String.isNotBlank(sPromotionId) && String.isNotBlank(sDepartmentName)) {
                objDepartMent.sDepartmentName = sDepartmentName;
                objDepartMent.sDepartmentId = sDepartmentId;


                // List of records from HEP Market Spend Detail where Type is Initial Budget or Budget Update         
                // Adding the Records to list of type DepartmentTransactions in DepartmentAudit object
                for (HEP_Market_Spend_Detail__c objRequest: [SELECT Id,
                        Requestor__r.Name,
                        Comments__c,
                        Budget_Difference__c,
                        HEP_Market_Spend__r.Name,
                        GL_Account__r.GLAccount__c,
                        GL_Account__r.Account_Department__c,
                        GL_Account__r.GLAccountDescription__c,
                        GL_Account__r.Format__c,
                        GL_Account__r.GLAccountGroup__c,
                        Type__c,
                        CreatedDate
                        FROM HEP_Market_Spend_Detail__c
                        WHERE HEP_Market_Spend__r.Promotion__c =: String.escapeSingleQuotes(sPromotionId) AND GL_Account__r.Account_Department__c =: String.escapeSingleQuotes(sDepartmentName) AND(Type__c =: HEP_Constants__c.getInstance('HEP_SPEND_DETAIL_TYPE_INITIAL_BUDGET').Value__c OR Type__c =: HEP_Constants__c.getInstance('HEP_SPEND_DETAIL_TYPE_BUDGET_UPDATE').Value__c)
                    ]) {

                    DepartmentTransactions objDepartMentTransaction = new DepartmentTransactions();
                    objDepartMentTransaction.sActionType = objRequest.Type__c;
                    objDepartMentTransaction.sRequest = objRequest.HEP_Market_Spend__r.Name.substringAfter('-');
                    if (objRequest.GL_Account__r != null) {
                        objDepartMentTransaction.sGLAccount = objRequest.GL_Account__r.GLAccount__c;
                        objDepartMentTransaction.sGLDescription = objRequest.GL_Account__r.GLAccountDescription__c;
                        objDepartMentTransaction.sGLAccountGroup = objRequest.GL_Account__r.GLAccountGroup__c;
                    }
                    objDepartMentTransaction.sFormat = objRequest.GL_Account__r.Format__c;
                    objDepartMentTransaction.dtDate = Date.valueOf(objRequest.CreatedDate);
                    objDepartMentTransaction.sRequestor = objRequest.Requestor__r.Name;
                    objDepartMentTransaction.sComments = objRequest.Comments__c;
                    objDepartMentTransaction.dAmount = objRequest.Budget_Difference__c;
                    objDepartMentTransaction.sPONumber = null;
                    objDepartMentTransaction.lstInvoices = null;
                    objDepartMent.lstDepartmentTransactions.add(objDepartMentTransaction);
                }


                // Filename to fetch from attachment from the Promotion
                HEP_Constants__c HEP_PO_INVOICE_RESPONSE = HEP_Constants__c.getInstance('HEP_PO_INVOICE_RESPONSE');

                //Map with PO# as key and PO record as value
                Map < String, DepartmentTransactions > mapDepartmentTransactions = new Map < String, DepartmentTransactions > ();

                // Call To the HEP_Integration_Util Method returning the json
                String sInvoicePoData = HEP_Integration_Util.retriveDatafromContent(sPromotionId, HEP_PO_INVOICE_RESPONSE.Value__c);

                System.debug('JSON Response on Integration methodcall--->' + sInvoicePoData);
                
                if (String.isNotBlank(sInvoicePoData)) {
                    HEP_Constants__c HEP_ACTION_TYPE_PO = HEP_Constants__c.getInstance('HEP_ACTION_TYPE_PO');
                    HEP_Constants__c HEP_ACTION_TYPE_INVOICE = HEP_Constants__c.getInstance('HEP_ACTION_TYPE_INVOICE');
                    HEP_InvoicePurchaseOrderData objInvoicePurchaseOrder = (HEP_InvoicePurchaseOrderData) JSON.deserialize(sInvoicePoData, HEP_InvoicePurchaseOrderData.class);
                    System.debug('Response For PO Invoice From Attachment After deserializing --------->' + JSON.serializePretty(objInvoicePurchaseOrder));
                    for (HEP_InvoicePurchaseOrderData.Department objInoviceDepartment: objInvoicePurchaseOrder.lstDepartments) {
                        if (objInoviceDepartment.sDepartmentName.equalsIgnoreCase(sDepartmentName)) {

                            //Adding all The PO Records to a Map  
                            for (HEP_InvoicePurchaseOrderData.ParentNode objPO: objInoviceDepartment.lstPurchaseOrders) {
                                DepartmentTransactions objPoInvoiceDepartment = new DepartmentTransactions();
                                objPoInvoiceDepartment.sActionType = HEP_ACTION_TYPE_PO.Value__c;
                                objPoInvoiceDepartment.sRequest = objPO.sRequestNumber;
                                objPoInvoiceDepartment.sGLAccount = objPO.sGLAccount;
                                objPoInvoiceDepartment.sGLDescription = objPO.sGLAccountDescription;
                                objPoInvoiceDepartment.sGLAccountGroup = objPO.sGLAccountGroup;
                                objPoInvoiceDepartment.sFormat = objPO.sFormat;
                                objPoInvoiceDepartment.dtDate = objPO.dtIssueDate != null ? Date.valueOf(objPO.dtIssueDate) : null;
                                objPoInvoiceDepartment.sRequestor = objPO.sRequestor;
                                objPoInvoiceDepartment.sComments = null;
                                objPoInvoiceDepartment.dAmount = objPO.dAmount != null ? objPO.dAmount - (2 * objPO.dAmount) : 0.0;
                                objPoInvoiceDepartment.sPONumber = null;
                                objPoInvoiceDepartment.lstInvoices = null;
                                mapDepartmentTransactions.put(objPoInvoiceDepartment.sRequest, objPoInvoiceDepartment);

                            }
                            System.debug('MAP with all PO Records----------->' + JSON.serializePretty(mapDepartmentTransactions));

                            //Adding all The Invoice Record in a list under PO if the PO Number matches with a PO#
                            //else Adding it to the list of type DepartmentTransactions in DepartmentAudit object
                            for (HEP_InvoicePurchaseOrderData.ParentNode objInvoice: objInoviceDepartment.lstInvoices) {

                                if (mapDepartmentTransactions.containsKey(objInvoice.sPONumber)) {
                                    DepartmentTransactions objTransaction = mapDepartmentTransactions.get(objInvoice.sPONumber);
                                    if (objTransaction.lstInvoices == null) {
                                        objTransaction.lstInvoices = new List < POInvoiceGroup > ();
                                    }
                                    POInvoiceGroup objPoInvoice = new POInvoiceGroup();
                                    objPoInvoice.sActionType = HEP_ACTION_TYPE_INVOICE.Value__c;
                                    objPoInvoice.sRequest = objInvoice.sRequestNumber;
                                    objPoInvoice.sGLAccount = objInvoice.sGLAccount;
                                    objPoInvoice.sGLDescription = objInvoice.sGLAccountDescription;
                                    objPoInvoice.sGLAccountGroup = objInvoice.sGLAccountGroup;
                                    objPoInvoice.sFormat = objInvoice.sFormat;
                                    objPoInvoice.dtDate = objInvoice.dtIssueDate != null ? Date.valueOf(objInvoice.dtIssueDate) : null;
                                    objPoInvoice.sRequestor = objInvoice.sRequestor;
                                    objPoInvoice.sComments = null;
                                    //objPoInvoice.dAmount = objInvoice.dAmount != null ? objInvoice.dAmount - (2 * objInvoice.dAmount) : 0.0;
                                    objPoInvoice.dAmount = objInvoice.dAmount != null ? objInvoice.dAmount : 0.0;
                                    objPoInvoice.sPONumber = objInvoice.sPONumber;
                                    objTransaction.lstInvoices.add(objPoInvoice);

                                } else {
                                    DepartmentTransactions objDepartMentTransaction = new DepartmentTransactions();
                                    objDepartMentTransaction.sActionType = HEP_Constants__c.getInstance('HEP_ACTION_TYPE_INVOICE').Value__c;
                                    objDepartMentTransaction.sRequest = objInvoice.sRequestNumber;
                                    objDepartMentTransaction.sGLAccount = objInvoice.sGLAccount;
                                    objDepartMentTransaction.sGLDescription = objInvoice.sGLAccountDescription;
                                    objDepartMentTransaction.sGLAccountGroup = objInvoice.sGLAccountGroup;
                                    objDepartMentTransaction.sFormat = objInvoice.sFormat;
                                    objDepartMentTransaction.dtDate = objInvoice.dtIssueDate != null ? Date.valueOf(objInvoice.dtIssueDate) : null;
                                    objDepartMentTransaction.sRequestor = objInvoice.sRequestor;
                                    objDepartMentTransaction.sComments = null;
                                    objDepartMentTransaction.dAmount = objInvoice.dAmount != null ? objInvoice.dAmount - (2 * objInvoice.dAmount) : 0.0;
                                    objDepartMentTransaction.sPONumber = null;
                                    objDepartMentTransaction.lstInvoices = null;
                                    objDepartMent.lstDepartmentTransactions.add(objDepartMentTransaction);
                                }
                            }
                        }
                    }
                    objDepartMent.lstDepartmentTransactions.addAll(mapDepartmentTransactions.values());
                }
            }
            System.debug('Object To Return----------->' + JSON.serializePretty(objDepartMent));
            return objDepartMent;
        } catch (Exception Ex) {
            HEP_Error_Log.genericException('DepartMent Audit Screen',
                'VF Controller',
                ex,
                'HEP_Department_Spend_Trail_Controller',
                'loadData',
                null,
                '');

            throw ex;
        }
    }

    /**
     * DepartmentAudit --- Wrapper class for holding the data for a Department with all child DepartmentTransactions under it
     *            
     * @author  Anshul Jain
     */

    public class DepartmentAudit {
        String sDepartmentId;
        String sDepartmentName;
        String sPromotionId;
        String sPromotionName;
        List < DepartmentTransactions > lstDepartmentTransactions;

        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public DepartmentAudit() {

            this.lstDepartmentTransactions = new List < DepartmentTransactions > ();
        }

        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public DepartmentAudit(String sDepartmentId, String sDepartmentName, String sPromotionId, String sPromotionName, List < DepartmentTransactions > lstDepartmentTransactions) {
            this.sDepartmentId = sDepartmentId;
            this.sDepartmentName = sDepartmentName;
            this.sPromotionId = sPromotionId;
            this.sPromotionName = sPromotionName;
            this.lstDepartmentTransactions = lstDepartmentTransactions;
        }
    }


    /**
     * DepartmentTransactions --- Wrapper class for holding the DepartmentTransactions  for a Department 
     * with all child POInvoiceGroup under it  
     * @author  Anshul Jain
     */
    public class DepartmentTransactions {

        String sActionType;
        String sRequest;
        String sGLAccount;
        String sGLDescription;
        String sGLAccountGroup;
        String sFormat;
        Date dtDate;
        String sRequestor;
        Decimal dAmount;
        String sPONumber;
        String sComments;
        List < POInvoiceGroup > lstInvoices;


        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public DepartmentTransactions() {
            this.lstInvoices = null;
        }

        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public DepartmentTransactions(String sActionType, String sRequest, String sGLAccount, String sGLDescription, String sGLAccountGroup, String sFormat, Date dtDate, String sRequestor, Decimal dAmount, String sPONumber) {
            this.sActionType = sActionType;
            this.sRequest = sRequest;
            this.sGLAccount = sGLAccount;
            this.sGLDescription = sGLDescription;
            this.sGLAccountGroup = sGLAccountGroup;
            this.sFormat = sFormat;
            this.dtDate = dtDate;
            this.sRequestor = sRequestor;
            this.dAmount = dAmount;
            this.sPONumber = sPONumber;
            this.lstInvoices = null;

        }
    }

    /**
     * POInvoiceGroup --- Wrapper class for holding the Invoices that are related to a PO 
     * @author  Anshul Jain
     */

    public class POInvoiceGroup {

        String sActionType;
        String sRequest;
        String sGLAccount;
        String sGLDescription;
        String sGLAccountGroup;
        String sFormat;
        Date dtDate;
        String sRequestor;
        Decimal dAmount;
        String sPONumber;
        String sComments;


        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public POInvoiceGroup() {}

        /**
         * Class constructor to initialize the class variables
         * @return nothing
         */
        public POInvoiceGroup(String sActionType, String sRequest, String sGLAccount, String sGLDescription, String sGLAccountGroup, String sFormat, Date dtDate, String sRequestor, Decimal dAmount, String sPONumber) {
            this.sActionType = sActionType;
            this.sRequest = sRequest;
            this.sGLAccount = sGLAccount;
            this.sGLDescription = sGLDescription;
            this.sGLAccountGroup = sGLAccountGroup;
            this.sFormat = sFormat;
            this.dtDate = dtDate;
            this.sRequestor = sRequestor;
            this.dAmount = dAmount;
            this.sPONumber = sPONumber;

        }
    }


}