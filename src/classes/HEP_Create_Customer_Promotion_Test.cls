@isTest(SeeAllData=false)
public class HEP_Create_Customer_Promotion_Test {

	@testSetup
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','Dreamworks - New Release', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    List<HEP_List_Of_Values__c> lstListOfValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager NR','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;
      HEP_Role__c objRoleDigital = HEP_Test_Data_Setup_Utility.createHEPRole('Digital Account Manager','', false);
      objRoleDigital.Destination_User__c = u.Id;
      objRoleDigital.Source_User__c = u.Id;
      insert objRoleDigital;
	    //HEP_Notification_Template__c objNotification = new HEP_Notification_Template__c(Name = 'Global_FAD_Date_Change',HEP_Body__c = '<old Global FAD> updated to <new Global FAD>',HEP_Object__c = 'HEP_Promotion__c',Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = 'Global_FAD_Date_Change',Status__c='-');
		//insert objNotification;
  	}

  	static testMethod void testData(){
  		User u = [SELECT Id,Name, DateFormat__c, HEP_Line_Of_Business__c,Email FROM User WHERE LastName = 'nara'];
      System.runAs(u){
  		HEP_Create_Customer_Promotion_Controller.CustomerPromotionPageData objWrapperReturned = new HEP_Create_Customer_Promotion_Controller.CustomerPromotionPageData();
  		HEP_Create_Customer_Promotion_Controller.TerritoryWrapper objTerritoryWrapper = new HEP_Create_Customer_Promotion_Controller.TerritoryWrapper();
  		

  		HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager NR'];
    	HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];
      HEP_Role__c objRoleDigital = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Digital Account Manager'];

    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
  		HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
  		objNationalTerritory.Flag_Country_Code__c = 'DE';
  		insert objNationalTerritory;
  		HEP_Territory__c objNationalTerritoryLicensee = HEP_Test_Data_Setup_Utility.createHEPTerritory('India','APAC','Licensee',null, null,'INR','IN','213',null, false);
  		objNationalTerritoryLicensee.FoxipediaCode__c = 'IN';
  		insert objNationalTerritoryLicensee;
  		HEP_Territory__c objNationalTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','EMEA','Licensee',objNationalTerritory.Id, objNationalTerritory.Id,'EUR','AT','33',null, true);


    	HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
    	objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
    	insert objUserRole;
    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
    	insert objUserRoleOperation;
      HEP_User_Role__c objUserRoleDigital = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleDigital.id, u.id, false);
      objUserRoleDigital.Search_Id__c = objRoleDigital.id+' / '+objNationalTerritory.Id;
      insert objUserRoleDigital;

    	HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test Customer', objNationalTerritory.Id, '123456', 'Dating Customers', 'Pay Per View - Home', true);
    	HEP_Customer__c objCustomerMDP = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test Customer', objNationalTerritory.Id, '123456', 'MDP Customers', '', true);
    	HEP_Customer__c objCustomerMDPChild = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test Customer', objNationalTerritory.Id, '123456', 'MDP Child Customers', '', true);

    	HEP_Utility.MultiPicklistWrapper objRegionMultiselect = new HEP_Utility.MultiPicklistWrapper(objNationalTerritoryRegion.Name,objNationalTerritoryRegion.Id,true);
    	LIST<HEP_Utility.MultiPicklistWrapper> lstRegion = new LIST<HEP_Utility.MultiPicklistWrapper>();
    	lstRegion.add(objRegionMultiselect);

    	objTerritoryWrapper.sKey = objNationalTerritory.id;
    	objTerritoryWrapper.sValue = objNationalTerritory.Name;
    	objTerritoryWrapper.lstRegions = lstRegion;
    	objWrapperReturned.objTerritory = objTerritoryWrapper;

    	HEP_Utility.PicklistWrapper objCustomerWrapper = new HEP_Utility.PicklistWrapper(objCustomer.id,objCustomer.Name);
    	objWrapperReturned.objCustomer = objCustomerWrapper;
    	objWrapperReturned.sExpenditedApproval = 'Yes';

    	HEP_Create_Customer_Promotion_Controller.Picklists objPicklists = new HEP_Create_Customer_Promotion_Controller.Picklists();
    	List<HEP_Create_Customer_Promotion_Controller.TerritoryWrapper> lstTerritory = new List<HEP_Create_Customer_Promotion_Controller.TerritoryWrapper>();
    	lstTerritory.add(objTerritoryWrapper);
    	HEP_Utility.PicklistWrapper objPromoTypeWrapper = new HEP_Utility.PicklistWrapper('Holiday','Holiday');
    	LIST<HEP_Utility.PicklistWrapper> lstPromoType = new LIST<HEP_Utility.PicklistWrapper>();
    	lstPromoType.add(objPromoTypeWrapper);
    	objPicklists.lstTerritory = lstTerritory;
    	objPicklists.lstPromoType = lstPromoType;
    	objWrapperReturned.objPicklist = objPicklists;
    	objWrapperReturned.objPromoType = objPromoTypeWrapper;

    	List<String> lstCustomerRegions = new List<String>();
    	lstCustomerRegions.add(objNationalTerritoryRegion.id);
    	lstCustomerRegions.add(objNationalTerritory.id);

  		
  			Test.startTest();
  			//HEP_Notification_Template__c objNotificationTemplate = [SELECT id FROM HEP_Notification_Template__c LIMIT 1];

  			HEP_Create_Customer_Promotion_Controller.fetchCustomerPromotionDefaultValues();
  			HEP_Create_Customer_Promotion_Controller.getTerritoriesDefaultValues(objNationalTerritory.id, objNationalTerritory.Name);
  			HEP_Create_Customer_Promotion_Controller.saveCustomerPromotion(objWrapperReturned);
  			HEP_Create_Customer_Promotion_Controller.getCustomers(objNationalTerritory.id);
  			HEP_Create_Customer_Promotion_Controller.getRegionAssociatedCustomers1(lstCustomerRegions);
  			HEP_Create_Customer_Promotion_Controller.getRegionAssociatedCustomers(lstCustomerRegions);
        new HEP_Create_Customer_Promotion_Controller().checkPermissions();
  			Test.stopTest();
  		}
  	}


}