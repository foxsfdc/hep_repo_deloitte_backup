/**
 * Controller class to add already Existing Catalogs Under a promotion
 * @author  Anshul Jain
 */

public class HEP_Add_Existing_Catalog_Controller {


    /**
     * Promotion Name that will be shown on the page 
     * @return nothing
     * @author Anshul Jain
     */
    public String sPromotionName {
        get;
        set;
    }


    /**
     * Promotion Territory that will be used to filter catalogs 
     * @return nothing
     * @author Anshul Jain
     */
    public String sPromotionTerritory {
        get;
        set;
    }


    /**
     * Catalog Territory that will be used to filter catalogs 
     * @return nothing
     * @author Anshul Jain
     */
    public String sCatalogTerritory {
        get;
        set;
    }


    /**
     * Active flag used to get Active Records only
     * @return nothing
     * @author Anshul Jain
     */
    public static String sACTIVE;

    /**
     * Approve flag used to get Approved Catalogs only
     * @return nothing
     * @author Anshul Jain
     */
    public static String sApproved;

    /**
     * Write Permission flag used to get Write Permission
     * @return nothing
     * @author Anshul Jain
     */
    public static String SWritePermission;


    /**
     * Bundle to filter records
     * @return nothing
     * @author Anshul Jain
     */
    public static String sBundle;

    static {
        sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        SWritePermission = HEP_Utility.getConstantValue('HEP_PERMISSION_WRITE');
        sBundle = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_BNDL');

    }


    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author Anshul Jain
     */
    public HEP_Add_Existing_Catalog_Controller() {
        LIST < HEP_Promotion__c > lstPromotions = [SELECT ID, PromotionName__c, Territory__c, Territory__r.CatalogTerritory__c
            FROM HEP_Promotion__c
            WHERE Id =: ApexPages.currentpage().getparameters().get('promoId') AND Record_Status__c =: sACTIVE
        ];
        if (lstPromotions.size() > 0) {
            if (String.isNotBlank(lstPromotions[0].PromotionName__c)) {
                sPromotionName = lstPromotions[0].PromotionName__c;
                sPromotionTerritory = lstPromotions[0].Territory__c;
            }
            if (String.isNotBlank(lstPromotions[0].Territory__r.CatalogTerritory__c)) {
                sCatalogTerritory = lstPromotions[0].Territory__r.CatalogTerritory__c;
            } else {
                sCatalogTerritory = lstPromotions[0].Territory__c;
            }
        }
    }

    public PageReference checkPermissions() {
        LIST < HEP_Promotion__c > lstPromotions = [SELECT ID, PromotionName__c, Territory__c, Territory__r.Name, Territory__r.CatalogTerritory__c
            FROM HEP_Promotion__c
            WHERE Id =: ApexPages.currentpage().getparameters().get('promoId') AND Record_Status__c =: sActive
        ];
        map < String, String > mapCustomPermissions = HEP_Utility.fetchCustomPermissions(lstPromotions[0].Territory__c, 'HEP_ADD_EXISTING_CATALOG');
        if (mapCustomPermissions == NULL || mapCustomPermissions.isEmpty() || !mapCustomPermissions.get(SWritePermission).equalsIgnoreCase('true')) {
            return HEP_Utility.redirectToErrorPage();
        } else {
            return null;
        }
    }


    /**
     * Links the catalog to a Promotion
     * @return List of catalogs that are already linked to current promotion 
     */
    @RemoteAction
    public static List < CatalogWrapper > saveCatalogData(String lstCatalogs, String sPromoId, String sCatalogTerritory) {
        try {
            System.debug('-------->' + lstCatalogs);
            List < HEP_Promotion__c > lstPromotion = [Select Id, Unique_Id__c FROM HEP_Promotion__c WHERE Id =: sPromoId];
            String sPromotionUniqueId = '';
            List < CatalogWrapper > lstCatalog = (List < CatalogWrapper > ) JSON.deserialize(lstCatalogs, List < CatalogWrapper > .class);
            if (!lstPromotion.isEmpty()) {
                sPromotionUniqueId = lstPromotion[0].Unique_Id__c;
                System.debug('---->' + sPromotionUniqueId);

                Map < String, HEP_Promotion_Catalog__c > mapCatalogs = new Map < String, HEP_Promotion_Catalog__c > ();
                for (CatalogWrapper objCatalogWrapper: lstCatalog) {
                    HEP_Promotion_Catalog__c objPromotionCatalog = new HEP_Promotion_Catalog__c();
                    objPromotionCatalog.Promotion__c = sPromoId;
                    objPromotionCatalog.Catalog__c = objCatalogWrapper.sCatalogId;
                    objPromotionCatalog.Unique_Id__c = sPromotionUniqueId + ' / ' + objCatalogWrapper.sUniqueId;
                    objPromotionCatalog.SKU_Template__c = null;
                    System.debug('---->' + sPromotionUniqueId + '------>' + objPromotionCatalog.Unique_Id__c);
                    objPromotionCatalog.Record_Status__c = sACTIVE;
                    mapCatalogs.put(objPromotionCatalog.Unique_Id__c, objPromotionCatalog);
                }

                System.debug('List To Save ' + mapCatalogs.values());

                lstCatalog = new List < CatalogWrapper > ();
                System.debug('Before mapCatalogs.values()-->' + mapCatalogs.values());
                List < HEP_Promotion_Catalog__c > lstDuplicatePromotionCatalog = [SELECT Id, Catalog__r.Catalog_Name__c, Catalog__r.CatalogId__c, Unique_Id__c FROM HEP_Promotion_Catalog__c WHERE Unique_Id__c IN: mapCatalogs.keySet() AND Record_Status__c =: sACTIVE];
                for (HEP_Promotion_Catalog__c objDuplicateCatalog: lstDuplicatePromotionCatalog) {
                    if (mapCatalogs.containsKey(objDuplicateCatalog.Unique_Id__c)) {
                        CatalogWrapper objCatalogWrapper = new CatalogWrapper();
                        objCatalogWrapper.sCatalog = objDuplicateCatalog.Catalog__r.CatalogId__c;
                        objCatalogWrapper.sCatalogName = objDuplicateCatalog.Catalog__r.Catalog_Name__c;
                        lstCatalog.add(objCatalogWrapper);
                        mapCatalogs.remove(objDuplicateCatalog.Unique_Id__c);
                    }
                }
                List < HEP_Promotion_Catalog__c > lstSavePromotionCatalog = mapCatalogs.values();
                System.debug('After mapCatalogs.values()-->' + mapCatalogs.values());
                if (lstSavePromotionCatalog.size() > 0) {
                    upsert lstSavePromotionCatalog HEP_Promotion_Catalog__c.Fields.Unique_Id__c;
                }
            }
            return lstCatalog;

        } catch (Exception Ex) {
            HEP_Error_Log.genericException('Add Catalog',
                'VF Controller',
                ex,
                'HEP_Add_Existing_Catalog_Controller',
                'saveCatalogData',
                null,
                '');
            throw ex;
        }


    }


    /**
     * Searches the catalogs For the search Keywords given by user
     * @return List of catalogs based on search keywords given by user.
     */
    @RemoteAction
    public static CatalogResults getCatalogData(String sValue, String sCatalogTerritory) {
        try {
            CatalogResults objCatalogResult = new CatalogResults();
            String sQuery = 'SELECT Id,Catalog_Name__c,Catalog_Type__c,CatalogId__c,Title_EDM__r.FIN_PROD_ID__c,Unique_Id__c,Territory__r.Name,Licensor_Type__c FROM HEP_Catalog__c ';
            String sWhereClause = '';
            Set < String > setExistingCatalogs = new Set < String > ();
            if (String.isNotBlank(sValue)) {
                if (sValue.contains('/--/')) {
                    for (String sSearch: sValue.split('/--/')) {
                        System.debug('---->' + sSearch);
                        if (String.isBlank(sWhereClause))
                            sWhereClause += 'WHERE (Catalog_Name__c = \'' + String.escapeSingleQuotes(sSearch) + '\' OR CatalogId__c = \'' + String.escapeSingleQuotes(sSearch) + '\' ';
                        else
                            sWhereClause += ' OR Catalog_Name__c = \'' + String.escapeSingleQuotes(sSearch) + '\' OR CatalogId__c = \'' + String.escapeSingleQuotes(sSearch) + '\' ';
                    }
                } else if (sValue.contains(',')) {

                    for (String sSearch: sValue.split(',')) {
                        if (String.isNotBlank(sSearch))
                            setExistingCatalogs.add(String.escapeSingleQuotes(sSearch.trim()));
                    }
                    sWhereClause += 'WHERE (Catalog_Name__c IN :setExistingCatalogs OR CatalogId__c IN:setExistingCatalogs ';
                } else {
                    sWhereClause += 'WHERE (Catalog_Name__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR CatalogId__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' ';
                }
                if (String.isNotBlank(sWhereClause)) {
                    sWhereClause += ') ';

                    sWhereClause += 'AND Territory__c = \'' + sCatalogTerritory + '\' AND Record_Status__c =:sACTIVE AND Status__c =:sApproved ';


                    if (String.isNotBlank(sWhereClause)) {
                        sWhereClause += 'ORDER BY Catalog_Name__c ASC LIMIT 1000';
                    }
                    sQuery += sWhereClause;
                }

            }
            System.debug('Catalog Search Query ---> ' + sQuery);
            List < HEP_Catalog__c > lstCatalogs = Database.Query(sQuery);
            objCatalogResult.lstCatalog = new List < CatalogWrapper > ();
            if (lstCatalogs != null) {
                for (HEP_Catalog__c objCatalog: lstCatalogs) {
                    CatalogWrapper objCatalogWrapper = new CatalogWrapper();
                    objCatalogWrapper.sCatalogId = objCatalog.Id;
                    objCatalogWrapper.sCatalog = objCatalog.CatalogId__c;
                    objCatalogWrapper.sCatalogName = objCatalog.Catalog_Name__c;
                    objCatalogWrapper.sType = objCatalog.Catalog_Type__c;
                    objCatalogWrapper.sWPRId = objCatalog.Title_EDM__r.FIN_PROD_ID__c;
                    objCatalogWrapper.sRegion = objCatalog.Territory__r.Name;
                    objCatalogWrapper.sLicensorType = objCatalog.Licensor_Type__c;
                    objCatalogWrapper.sUniqueId = objCatalog.Unique_Id__c;
                    objCatalogResult.lstCatalog.add(objCatalogWrapper);
                    if (setExistingCatalogs.size() > 0) {
                        if (setExistingCatalogs.contains(objCatalog.CatalogId__c)) {
                            setExistingCatalogs.remove(objCatalog.CatalogId__c);
                        }
                        if (setExistingCatalogs.contains(objCatalog.Catalog_Name__c)) {
                            setExistingCatalogs.remove(objCatalog.Catalog_Name__c);
                        }
                    }
                }
                if (setExistingCatalogs.size() > 0) {
                    for (String sNotFound: setExistingCatalogs) {
                        if (String.isBlank(objCatalogResult.sNoRecord)) {
                            objCatalogResult.sNoRecord += sNotFound;
                        } else {
                            objCatalogResult.sNoRecord += ' , ' + sNotFound;
                        }
                    }
                }
            }
            System.debug('CatalogsResult ---->' + objCatalogResult.lstCatalog);

            return objCatalogResult;
        } catch (Exception Ex) {
            HEP_Error_Log.genericException('Get Catalog Data',
                'VF Controller',
                ex,
                'HEP_Add_Existing_Catalog_Controller',
                'getCatalogData',
                null,
                '');
            throw ex;
        }

    }



    /**
     * Searches the catalogs For the search Keywords given by user shown in typeahead results
     * @return List of catalogs based on search keywords given by user.
     */
    @RemoteAction
    public static LIST < HEP_Utility.PicklistWrapper > searchCatalog(String sValue, String sCatalogTerritory) {
        try {
            String sCatalogQuery = 'Select Id, Catalog_Name__c,CatalogId__c,Territory__r.Name FROM HEP_Catalog__c';
            if (sValue.contains(',')) {
                String sWhere = '';
                Set < String > setSearchCatalogs = new Set < String > ();
                for (String sSearch: sValue.split(',')) {

                    setSearchCatalogs.add(String.escapeSingleQuotes(sSearch));

                }
                sWhere += ' WHERE (CatalogId__c IN :setSearchCatalogs OR Catalog_Name__c IN :setSearchCatalogs )';
                sCatalogQuery += sWhere;

            } else {
                sCatalogQuery += ' WHERE (CatalogId__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\' OR Catalog_Name__c LIKE \'%' + String.escapeSingleQuotes(sValue) + '%\')';
            }
            sCatalogQuery += ' AND Territory__c = \'' + sCatalogTerritory + '\'  AND Record_Status__c =:sACTIVE AND Status__c =:sApproved ';

            if (String.isNotBlank(sCatalogQuery)) {
                sCatalogQuery += ' ORDER BY Catalog_Name__c ASC Limit 100';
            }
            System.debug('Typeahead Query ---->' + sCatalogTerritory);
            LIST < HEP_Catalog__c > lstCatalogs = new List < HEP_Catalog__c > ();
            lstCatalogs = Database.Query(sCatalogQuery);

            if (lstCatalogs != NULL && lstCatalogs.size() > 0) {
                LIST < HEP_Utility.PicklistWrapper > lstCatalogResults = new LIST < HEP_Utility.PicklistWrapper > ();
                for (HEP_Catalog__c objCatalog: lstCatalogs) {
                    lstCatalogResults.add(new HEP_Utility.PicklistWrapper(objCatalog.Catalog_Name__c + '/--/' + objCatalog.CatalogId__c, objCatalog.CatalogId__c + ' / ' + objCatalog.Catalog_Name__c + ' / ' + objCatalog.Territory__r.Name));
                }
                return lstCatalogResults;
            } else {
                return null;
            }
        } catch (Exception Ex) {
            HEP_Error_Log.genericException('Search Catalog',
                'VF Controller',
                ex,
                'HEP_Add_Existing_Catalog_Controller',
                'searchCatalog',
                null,
                '');
            throw ex;
        }

    }

    /**
     * CatalogResults --- Wrapper class for holding the lstOfCatalogs and Duplicate catalogs found while linking 
     * @author  Anshul Jain
     */
    public class CatalogResults {
        List < CatalogWrapper > lstCatalog;
        String sNoRecord;
        public CatalogResults() {
            lstCatalog = new List < CatalogWrapper > ();
            sNoRecord = '';
        }

    }

    /**
     * CatalogWrapper --- Wrapper class for holding the lstOfCatalogs
     * @author  Anshul Jain
     */
    public class CatalogWrapper {

        String sCatalogId;
        String sCatalog;
        String sCatalogName;
        String sType;
        String sWPRId;
        String sLicensorType;
        String sRegion;
        String sUniqueId;

    }

}