/* Class Name   : HEP_Rule_Constants
 * Description  : All Constants.    
 * Created By   : Nidhin V K
 * Created On   : 07-29-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-29-2016        1000            Initial version
 *
 *
 */
public with sharing class HEP_Rule_Constants{
    
    //Start 20 Feb 2018 Added By Nidhin
    public static final String HEP_EMPTY = '';
    public static final String HEP_NONE = 'None';
    
    public static final String HEP_STRING_EQUALS_TO = 'Equals To';
    public static final String HEP_STRING_NOT_EQUALS_TO = 'Not Equals To';
    public static final String HEP_STRING_GREATER_THAN = 'Greater Than';
    public static final String HEP_STRING_GREATER_THAN_OR_EQUALS_TO = 'Greater Than Or Equals To';
    public static final String HEP_STRING_LESS_THAN = 'Less Than';
    public static final String HEP_STRING_LESS_THAN_OR_EQUALS_TO = 'Less Than Or Equals To';
    
    //For Rule Types
    public static final String HEP_RULE_TYPE_QUEUE = 'Queue';
    public static final String HEP_RULE_TYPE_TASK = 'Task';
    public static final String HEP_RULE_TYPE_EMAIL = 'Email';
    
    public static final String HEP_TASK_STATUS_NEW = 'New';
    public static final String HEP_TASK_STATUS_IN_PROGRESS = 'In Progress';
    public static final String HEP_TASK_STATUS_COMPLETE = 'Complete';
    
    public static final Set<String> HEP_RULE_TYPES = new Set<String>{'Queue', 'Task', 'Email'};
    public static final Set<String> HEP_RECORD_OWNER_TYPES = new Set<String>{'User', 'Group'};

    //public static final Set<String> HEP_TARGET_OBJECTS = new Set<String>{'LTSS_Application_Info__c', 'LTSS_LOC_Info__c', 'LTSS_Enrolled_Program__c', 'LTSS_Application_Detail__c', 'LTSS_POC__c'};
    public static final Set<String> HEP_RESTRICTED_FIELDS = new Set<String>{'HEP_Record_Id__c', 'HEP_Record_Type__c'};
    
    public static final List<String> HEP_RULE_PRIORITY_LIST = new List<String>{'High', 'Normal', 'Low'};
    
    public static final String HEP_OBJECT_TASK = 'HEP_Task__c';
    public static final String HEP_OBJECT_RULE_CRITERION = 'HEP_Rule_Criterion__c';
    
    public static final String HEP_FIELD_IS_ACTIVE = 'HEP_IsActive__c';
    public static final String HEP_FIELD_RULE = 'HEP_Rule__c';
    public static final String HEP_FIELD_SL_NO = 'HEP_Serial_Number__c';
    public static final String HEP_FIELD_OWNER_ID = 'OwnerId';
    public static final String HEP_FIELD_COMMENTS = 'HEP_Comments__c';
    public static final String HEP_FIELD_STATUS = 'HEP_Status__c';
    public static final String HEP_FIELD_DUE_DATE = 'HEP_Due_Date__c';
    public static final String HEP_FIELD_NAME = 'Name';
    public static final String HEP_FIELD_DEACTIVATED_DATE = 'HEP_Deactivated_Date__c';
    public static final String HEP_FIELD_MASTER_RECORD_ID = 'HEP_Master_Record_Id__c';
    public static final String HEP_FIELD_OWNERID = 'OwnerId';
    public static final String HEP_FIELD_TASK_NAME = 'HEP_Task_Name__c';
    public static final String HEP_FIELD_MODULE = 'HEP_Module__c';
    
    public static final String HEP_URL_VIEW_RULE_LIST = '/apex/HEP_ViewRuleList';
    public static final String HEP_URL_VIEW_RULE = '/apex/HEP_ViewRule';
    
    public static final String HEP_ERROR_TYPE_BUSINESS_VALIDATION = 'Business Validation';
    public static final String HEP_ERROR_TYPE_VF_CONTROLLER = 'Apex Controller';
    public static final String HEP_ERROR_TYPE_APEX_TRIGGER = 'Apex Trigger';
    
    public static final String HEP_CLASS_RULE_EXTENSION = 'HEP_RuleExtension';
    public static final String HEP_CLASS_RULE_VERSION_EXTENSION = 'HEP_RuleVersionExtension';
    public static final String HEP_CLASS_RULE_CRITERIA_EXTENSION = 'HEP_RuleCriteriaExtension';
    public static final String HEP_CLASS_RULE_VERSION_TRIGGER_HANDLER = 'HEP_RuleVersionTriggerHandler';
    public static final String HEP_CLASS_TASK_ASSIGNMENT = 'HEP_TaskAssignment';
    public static final String HEP_CLASS_QUEUE_ASSIGNMENT = 'HEP_QueueAssignment';
    public static final String HEP_CLASS_SEND_EMAIL = 'HEP_SendEmail';
    public static final String HEP_CLASS_UTIL = 'HEP_Util';
    
    
    public static final String HEP_METHOD_TOGGLE_ACTIVATE = 'toggleActivate';
    public static final String HEP_METHOD_SAVE = 'save';
    public static final String HEP_METHOD_EVALUATE_TASK_RULES = 'evaluateTaskRules';
    public static final String HEP_METHOD_EVALUATE_QUEUE_RULES = 'evaluateQueueRules';
    public static final String HEP_METHOD_EVALUATE_EMAIL_RULES = 'evaluateEmailRules';
    public static final String HEP_METHOD_CREATE_TASK = 'createTask';
    public static final String HEP_METHOD_SEND_EMAIL = 'sendEmail';
    public static final String HEP_METHOD_CHECK_VALIDATIONS = 'checkValidations';
    public static final String HEP_METHOD_EVALUATE_RULE_CRITERIA = 'evaluateRuleCriteria';
    public static final String HEP_METHOD_POPULATE_OBJECT_FIELDS = 'populateSObjectFields';
    public static final String HEP_METHOD_DELETE_RULE = 'deleteRule';
    public static final String HEP_METHOD_DELETE_VERSION = 'deleteVersion';
    public static final String HEP_METHOD_DELETE_CRITERIA = 'deleteCriteria';
    public static final String HEP_METHOD_NAVIGATE_URL = 'getNavigateURL';
    
    public static final String HEP_RECORD_TYPE_MAP_STATIC = 'Map Static Values To Task';
    public static final String HEP_RECORD_TYPE_MAP_SOBJECT = 'Map Object Fields To Task';
    public static final String HEP_RECORD_TYPE_COMPARE = 'Compare Field Value';
    
    public static final String HEP_VALUE_NOT_STARTED = 'Not Started';
    public static final String HEP_VALUE_OTHER = 'Other';
    public static final String HEP_VALUE_NORMAL = 'Normal';
    
    public static final String HEP_VALUE_MASTER_RECORD_MISSING = 'Master Record Id Missing';
    public static final String HEP_ASSIGN_BACK_TO_PERSON = 'Assign it back to the Submitted Person';
    public static final String HEP_ASSIGNEE_TYPE_TASK = 'Assign it to latest Task Owner';
    public static final String HEP_ASSIGNEE_TYPE_MODULE = 'Assign it to latest Module Owner';
    public static final String HEP_ASSIGNEE_SELF = 'Self';
    
    public static final String HEP_DATETYPE_PHYSICAL_RELEASE_DATE_RETAIL = 'Physical Release Date (Retail)';
    public static final String HEP_DATETYPE_PHYSICAL_RELEASE_DATE_KIOSK = 'Physical Release Date (Kiosk)';
    public static final String HEP_DATETYPE_PHYSICAL_RELEASE_DATE_RENTAL = 'Physical Release Date (Rental)';
    public static final String HEP_DATETYPE_PHYSICAL_ORDER_DATE = 'Physical Order Date';
    public static final String HEP_DATETYPE_PHYSICAL_TRADE_ANNOUNCE = 'Physical Trade Announce';
    public static final String HEP_DATETYPE_DIGITAL_PREORDER_DATE = 'Digital Pre-order Date';
    public static final String HEP_WEEKLY_EMAIL = 'HEP_WeeklyEmail';
    public static final String HEP_DATETYPE_PVOD_RELEASE_DATE = 'PVOD Release Date';
    public static final String HEP_DATETYPE_EST_RELEASE_DATE = 'EST Release Date';
    public static final String HEP_DATETYPE_VOD_RELEASE_DATE = 'VOD Release Date';
    public static final String HEP_DATETYPE_END_DATE = 'End Date';
    public static final String HEP_DATETYPE_SPEST_RELEASE_DATE = 'SPEST Release Date';
    public static final String HEP_DATETYPE_PEST_RELEASE_DATE = 'PEST Release Date';
    public static final String HEP_DATETYPE_SPVOD_RELEASE_DATE = 'SPVOD Release Date';
    public static final String HEP_DATETYPE_EST_AVAIL_DATE = 'EST Avail Date';
    public static final String HEP_DATETYPE_VOD_AVAIL_DATE = 'VOD Avail Date';
    public static final String HEP_STATUS_CONFIRMED = 'Confirmed';
    public static final String HEP_PROMOTION_TYPE_NATIONAL = 'National';
    public static final String HEP_TERRITORY_TYPE_LICENSEE = 'Licensee';
    public static final String HEP_DATETYPE_RENTAL_REVENUE_SHARE = 'Rental Revenue Share';
    public static final String HEP_PROMOTION_TYPE_GLOBAL = 'Global';
    //End 21 Feb 2018 Added By Lakshman
}