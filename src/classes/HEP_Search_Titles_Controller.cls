/**
* HEP_Search_Titles_Controller --- Class to get search results for titles
* @author    Arjun Narayanan
*/
public without sharing class HEP_Search_Titles_Controller {

	//wrapper class that stores theJson
	public class HEP_Search_Wrapper{
		list<searchRowData> lstSearchData;
		string sKeywordFilter;
		string sStartsWithFilter;
		string sSearch;
		integer iNoOfResults;
		
		List<String> lstKeyWords;
        List<String> lstCriteria;

	//contructor
	public HEP_Search_Wrapper(){
		lstSearchData = new list<searchRowData>();
		iNoOfResults = 0;
	}
}

//wrapper which holds the data of rows displayed
public class searchRowData{
	String sTitleId;
	String sImageURL;
	String sName;
	String sFinancialTitle;
	String sUSReleaseDate;
	String sDivision;
	String sType;
	String sLifecycleStatus;
	String sDirector;
	
}

/**
    * Its used to get the data to show on the page
    * @param sLocaleDateFormat to get dates based of user locale
    * @param sKeyword gives a filter criteria
    * @param sCriteria gives the secondary filter criteria
    * @param sSearchKey gives the value to be searched with
    * @return list of matching records
    * @author Arjun Narayanan
    */
    @RemoteAction
    public static HEP_Search_Wrapper getTitleData(String sLocaleDateFormat, string sKeyword, string sCriteria, string sSearchKey, Date startDate, Date endDate){
        try{
    	HEP_Search_Wrapper objSearchWrapper = new HEP_Search_Wrapper();
    	List<EDM_GLOBAL_TITLE__c> lstAllTitles = new List<EDM_GLOBAL_TITLE__c>();
    	List<GTS_Title__c> lstGTSTitles = new List<GTS_Title__c>();
    	List<GTS_Title__c> lstGTSReleases_Dates = new List<GTS_Title__c>();
    	List<String> FinprodIds = new List<String>();
    	
    	String CNFDL = HEP_Constants__c.getValues('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL').Value__c;
    	
    	Map<string,GTS_Title__c> mapOfReleaseDates = new Map<string,GTS_Title__c>();
    	String defaultImageURL = HEP_Utility.getPromoDefaultImageUrl();
    	
    	String keywords = HEP_Constants__c.getValues('SEARCH_TITLES_KEYWORDS_VALUES').Value__c;
        objSearchWrapper.lstKeyWords = keywords.split(',');
    
        String criteria = HEP_Constants__c.getValues('SEARCH_CRITERIA_VALUES').Value__c;
        objSearchWrapper.lstCriteria = criteria.split(',');

	//Query All Titles
	        //set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_TITLES'));
		String sQuery = 'SELECT id,name,FIN_PROD_ID__c,FIN_DIV_CD__r.Name,PROD_TYP_CD__r.Name,LIFE_CYCL_STAT_CD__r.LIFE_CYCL_STAT_DESC__c,LIFE_CYCL_STAT_CD__r.LIFE_CYCL_STAT_CD__c,DIR_NM__c,LIFE_CYCL_STAT_GRP_CD__c from EDM_GLOBAL_TITLE__c WHERE LIFE_CYCL_STAT_GRP_CD__c !=: CNFDL ';
        if(sKeyWord != null && sCriteria != null && String.isNotBlank(sKeyWord) && String.isNotBlank(sCriteria) )
		{	if(sKeyword != null ){
				if(HEP_Constants__c.getValues('HEP_TITLE_NAME').Value__c.equals(sKeyword)){
					sQuery += ' AND Name';
				}
				else if(HEP_Constants__c.getValues('HEP_FINANCIAL_TITLE_ID').Value__c.equals(sKeyword)){
					sQuery += ' AND FIN_PROD_ID__c';
				}
				else if(HEP_Constants__c.getValues('HEP_TYPE').Value__c.equals(sKeyword)){
					sQuery += ' AND PROD_TYP_CD__r.Name';
				}
				else if(HEP_Constants__c.getValues('HEP_DIVISION').Value__c.equals(sKeyword)){
					sQuery += ' AND FIN_DIV_CD__r.Name';
				}
				else if(HEP_Constants__c.getValues('HEP_RELEASE_YEAR').Value__c.equals(sKeyword)){
				    lstGTSReleases_Dates = [SELECT Id,US_Release_Date__c,WPR__c FROM GTS_Title__c WHERE US_Release_Date__c>=:startDate AND US_Release_Date__c<=:endDate AND WPR__c != null];
				    List<EDM_GLOBAL_TITLE__c> lstTitles = new List<EDM_GLOBAL_TITLE__c> ();
				    
				    
				    for(GTS_Title__c release: lstGTSReleases_Dates){
				        FinprodIds.add(release.WPR__c);
				    }
				    sQuery += ' AND FIN_PROD_ID__c IN :FinprodIds';
				    
				}
				else if(HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyWord)){
				    sQuery+= ' AND (Name';
				}
				
				if(HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword)){
				    if(sCriteria != null && HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria) ){
    					sQuery +=' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\' OR FIN_PROD_ID__c  LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\')';
    						//sQuery +=' LIKE \''+sSearchKey+'%\'';
    				}
    				else if(sCriteria != null && HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria)){
    					sQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\'OR FIN_PROD_ID__c  LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\')';
    						//sQuery +=' LIKE \'%'+sSearchKey+'%\'';
    				}
				}
				//Adding a condition to criteria so search for release year doesnt include LIKE
    			if(!(HEP_Constants__c.getValues('HEP_RELEASE_YEAR').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword))){	
    				if(sCriteria != null && HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria) ){
    					sQuery +=' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\'';
    						//sQuery +=' LIKE \''+sSearchKey+'%\'';
    				}
    				else if(sCriteria != null && HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria)){
    					sQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\'';
    						//sQuery +=' LIKE \'%'+sSearchKey+'%\'';
    				}
    			}
    							
			}
        }
		else{ 
		    sQuery += ' AND (Name LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\' OR FIN_PROD_ID__c  LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\')';
		}
       	sQuery += ' Limit 10001';
		system.debug('---------->Query'+sQuery);
		lstAllTitles = Database.Query(sQuery);

		objSearchWrapper.iNoOfResults = lstAllTitles.size();

        List<String> lstFinProdIds = new List<String>();
        for(EDM_GLOBAL_TITLE__c title: lstAllTitles){
            lstFinProdIds.add(title.FIN_PROD_ID__c);
        }
        System.debug('<>>>>>> '+ lstFinProdIds);
		lstGTSTitles = [SELECT Id,WPR__c, US_Release_Date__c FROM GTS_Title__c WHERE WPR__c IN :lstFinProdIds];
        
		for(GTS_Title__c objTitles : lstGTSTitles){
	        //System.debug('objReleases.Title__r.WPR__c !!! '+ objReleases.Title__r.WPR__c);
			if(objTitles.WPR__c != null){
			   // System.debug('2objReleases.Title__r.WPR__c !!! '+ objReleases.Title__r.WPR__c);
				mapOfReleaseDates.put(objTitles.WPR__c,objTitles);
			}
		}

			for(EDM_GLOBAL_TITLE__c objTitle : lstAllTitles){

				searchRowData objSearchRowData = new searchRowData();
					//setWPRIds.add(objTitle.FIN_PROD_ID__c);
					/*if(objTitle.Image_URL__c != null){
                        //objSearchRowData.sImageURL = (!String.isBlank(promo.Promotion_Image_URL__c)) ? promo.Promotion_Image_URL__c : defaultImageURL;
                        objSearchRowData.sImageURL = objTitle.Image_URL__c;
                    }
                    else{
                        objSearchRowData.sImageURL = defaultImageURL;
                    }*/
                    objSearchRowData.sTitleId = objTitle.ID;
					if(objTitle.Name != null){
						objSearchRowData.sName = objTitle.Name;
					}
					if(objTitle.FIN_PROD_ID__c != null){
						objSearchRowData.sFinancialTitle = objTitle.FIN_PROD_ID__c;
					}
					if(objTitle.FIN_DIV_CD__r.Name != null){
						objSearchRowData.sDivision = objTitle.FIN_DIV_CD__r.Name;
					}
					if(objTitle.PROD_TYP_CD__r.Name != null){
						objSearchRowData.sType = objTitle.PROD_TYP_CD__r.Name;
					}
					if(objTitle.LIFE_CYCL_STAT_CD__c != null){
						objSearchRowData.sLifecycleStatus = objTitle.LIFE_CYCL_STAT_CD__r.LIFE_CYCL_STAT_DESC__c;
					}
					if(objTitle.DIR_NM__c != null){
						objSearchRowData.sDirector = objTitle.DIR_NM__c;
					}
					//System.debug('mapOfReleaseDates ^&*)*YYGUGI '+ mapOfReleaseDates);
					
					if(mapOfReleaseDates.containsKey(objTitle.FIN_PROD_ID__c)){
						if(mapOfReleaseDates.get(objTitle.FIN_PROD_ID__c).US_Release_Date__c != null){
						    //System.debug('Assigning this date !! :' + HEP_Utility.getFormattedDate(mapOfReleaseDates.get(objTitle.FIN_PROD_ID__c).US_Release_Date__c, sLocaleDateFormat));
							objSearchRowData.sUSReleaseDate = HEP_Utility.getFormattedDate(mapOfReleaseDates.get(objTitle.FIN_PROD_ID__c).US_Release_Date__c, sLocaleDateFormat);
						}
					}

						//system.debug();
						objSearchWrapper.lstSearchData.add(objSearchRowData);
					}

					

					system.debug('wrapper----------->'+objSearchWrapper);
					return objSearchWrapper;

				}
	Catch(Exception ex) {
            return NULL;
    }
    }
   

		}