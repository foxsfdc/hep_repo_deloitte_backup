@isTest 
private class HEP_MM_Batch_Scheduler_Test{

 @isTest 
    static void testBatchScheduler() {
        String cronExpr = '0 0 0 15 3 ? 2022';
List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        // HEP_Notification_Template__c notifTemplate = [select id from HEP_Notification_Template__c];
            EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title', null, '', null, false);
            insert objTitle;
            system.debug('inserted objTitle---> '+ objTitle);

            HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS1', 'AUS1', '', false);
            objTerr.Flag_Country_Code__c = 'AU';
            objTerr.Send_to_Prophet__c = True;
            insert objTerr;

            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple', objTerr.Id, '11', 'MDP Customers', '', false);
            objCustomer.Record_Status__c = 'Active';
            insert objCustomer;
            system.debug('inserted objCustomer---> '+ objCustomer);

            HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null, objTerr.Id, null, objCustomer.Id, null, false);
            objGlobalPromotion.Record_Status__c = 'Active';
            insert objGlobalPromotion;
            system.debug('inserted objGlobalPromotion---> '+ objGlobalPromotion);

        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Customer Promotion', 'Customer', objGlobalPromotion.Id, objTerr.Id, null,null,null,false);
            objPromotion.LocalPromotionCode__c = '0000000032'; 
            objPromotion.StartDate__c = date.today();
            objPromotion.EndDate__c = date.today();
            objPromotion.Record_Status__c = 'Active';
            objPromotion.Status__c = 'Approved'; //HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED'); // changed this 6/26
            system.debug('HEP_APPROVAL_APPROVED----------> '+ objPromotion.Status__c);
            objPromotion.Promotion_Type__c = 'Customer'; //HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
            system.debug('PROMOTION_TYPE_CUSTOMER----------> '+ objPromotion.Promotion_Type__c);
            insert objPromotion;

        HEP_MDP_Promotion_Product__c objPromoProd = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.Id, objTitle.Id, objTerr.Id, false);
        objPromoProd.Record_Status__c = 'Active';
            insert objPromoProd;
            system.debug('inserted objPromoProd---> '+ objPromoProd);
            
            objPromotion.Status__c = 'Approved';
            update objPromotion;

        HEP_MDP_Promotion_Product_Detail__c objpromoproddet = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromoProd.Id, 'SD', 'ESD', false);
            objpromoproddet.CustomerId__c = '121';
            objpromoproddet.Record_Status__c = 'Active';
            objpromoproddet.Promo_WSP__c = 60.00;
            objpromoproddet.MM_Currency_Code__c = 'USD';
            objpromoproddet.Channel__c = HEP_Utility.getConstantValue('HEP_CHANNEL_EST');
            

            Test.startTest();
            insert objpromoproddet;
                HEP_ProductPricing_To_MM_Batch_Scheduler  objScheduler= new HEP_ProductPricing_To_MM_Batch_Scheduler ();
                String jobId = System.schedule('TestJob', cronExpr, objScheduler);
            Test.stopTest();
        }
    }