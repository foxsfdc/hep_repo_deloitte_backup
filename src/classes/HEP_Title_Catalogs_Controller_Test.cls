@isTest
public class HEP_Title_Catalogs_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'roshrai45', 'roshirai675@deloitte.com', 'Rai6', 'ros6', 'N', '', true);
        list < HEP_Constants__c > objHEPConstant = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void test1() {
        //Inserting Territory Record
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        //Inserting Region
        List < HEP_Territory__c > lstjRegion = new List < HEP_Territory__c > {
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '890', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Netherlands', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'CAD', 'CA', '891', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('New Zealand', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '892', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Norway', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '893', 'JDE', true),
            HEP_Test_Data_Setup_Utility.createHEPTerritory('Poland', 'Domestic', 'Subsidiary', objTerritory.Id, null, 'USD', 'US', '894', 'JDE', true)
        };
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog_Title', 'HEP_CatalogDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        HEP_Interface__c objInterfaceproduct = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Product', 'HEP_ProductDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        //HEP_Interface__c objInterfacetitle = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog', 'HEP_Foxipedia_Catalog_Title', true, 10, 10, 'Outbound-Pull', true, true, true);
        //All HEP Constants
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'roshrai45'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, true);
        List < HEP_Line_Of_Business__c > lstLOB = new List < HEP_Line_Of_Business__c > {
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true),
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Catalog', 'Catalog', true),
            HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('TV', 'TV', true)
        };
        //Create PRoduct Type Object.
        EDM_REF_PRODUCT_TYPE__c objEDM_REF_PRODUCT_TYPE = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature', 'FEATR', true);
        //Create Title Object.
        EDM_GLOBAL_TITLE__c objFinancialTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('TESTFinancial', '908763', 'PUBLC', objEDM_REF_PRODUCT_TYPE.Id, false);
        objFinancialTitle.LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC';
        objFinancialTitle.FOX_ID__c = '72186';
        insert objFinancialTitle;
        set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_TITLE_CATALOGS'));
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Title_EDM__c = objFinancialTitle.ID;
        insert objCatalog;
        System.runAs(u) {
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Title_Catalogs_Controller.extractAllCatalogForTitleCatalog(objFinancialTitle.id);
            HEP_Title_Catalogs_Controller.extractAllCatalogForTitleProduct(objFinancialTitle.id);
            HEP_Title_Catalogs_Controller.getCatalogRecordId(objCatalog.id);
            test.stopTest();
        }
    }
}