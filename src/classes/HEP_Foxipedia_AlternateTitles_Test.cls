@isTest
Public class HEP_Foxipedia_AlternateTitles_Test{

    @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        HEP_Services__c objService1 = new HEP_Services__c();
        objService1.Name='Foxipedia_OAuth';
        objService1.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService1.Service_URL__c = '/oauth/oauth20/token';
        insert objService1;

        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Foxipedia_AlternateTitles';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/foxipedia/global/api/search/title';
        insert objService;
    }

    @isTest 
    static void Failure_Test(){   
        HEP_Services__c objService = [Select id,Name from HEP_Services__c where Name = 'Foxipedia_OAuth'];
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        update objService;

        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'Logan'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_AlternateTitles testAT = new HEP_Foxipedia_AlternateTitles();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        testAT.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
    }

    @isTest 
     static void Success_Test() {

        HEP_Services__c objService = [Select id,Name,Endpoint_URL__c,Service_URL__c from HEP_Services__c where Name = 'Foxipedia_OAuth'];

        HEP_Services__c objService1 = [Select id,Name,Endpoint_URL__c,Service_URL__c from HEP_Services__c where Name = 'HEP_Foxipedia_AlternateTitles'];

        system.debug('Services!! ' + objService + ' aa ' + objService1);

        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_AlternateTitles','HEP_Foxipedia_AlternateTitles',false,null,null,'Outbound-Pull',true,true,true);

        Test.starttest ();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'Logan'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_AlternateTitles testAT = new HEP_Foxipedia_AlternateTitles();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        testAT.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();

    } 
    
    /*@isTest 
     static void Success_Test() {
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Foxipedia_AlternateTitles';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/foxipedia/global/api/search/title';
        
        HEP_Services__c objService1 = new HEP_Services__c();
        objService1.Name='Foxipedia_OAuth';
        objService1.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService1.Service_URL__c = '/oauth/oauth20/token';
        lstServices.add(objService1);

        lstServices.add(objService);
        insert lstServices;

        RestRequest req = new RestRequest();
        String requestBody = '{"search":{"titleName":"Logan"}}';
        req.requestBody = Blob.valueOf(requestBody);
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_AlternateTitles','HEP_Foxipedia_AlternateTitles',false,null,null,'Outbound',true,true,true);
        
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod('Logan','','HEP_Foxipedia_AlternateTitles');
        HEP_Foxipedia_AlternateTitles testAT = new HEP_Foxipedia_AlternateTitles();
        testAT.performTransaction(objTxnResponse);
        Test.stoptest();
    }   

    @isTest 
    static void Failure_Test(){   
        HEP_Services__c objService = [Select id,Name from HEP_Services__c where Name = 'Foxipedia_OAuth'];
        objService.Endpoint_URL__c = '/oauth/oauth20/tokenFail';
        update objService;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'Logan'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_AlternateTitles testAT = new HEP_Foxipedia_AlternateTitles();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        testAT.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
    }  

    @isTest 
    static void Access_Error_Test(){ 
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Foxipedia_AlternateTitles';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/foxipedia/global/api/search/title';

        HEP_Services__c objService1 = new HEP_Services__c();
        objService1.Name='Foxipedia_OAuth';
        objService1.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService1.Service_URL__c = '/oauth/oauth20/token';
        lstServices.add(objService1);

        lstServices.add(objService1);
        lstServices.add(objService);
        insert lstServices;

        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'Logan'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_AlternateTitles testAT = new HEP_Foxipedia_AlternateTitles();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        testAT.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();
    } */ 
     
}