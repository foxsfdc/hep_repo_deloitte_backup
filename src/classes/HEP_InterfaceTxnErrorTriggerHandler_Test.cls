@isTest
private class HEP_InterfaceTxnErrorTriggerHandler_Test{
    
    @testSetUp
    static void createData() {

        //EmailTemplate objEmail = new EmailTemplate (developerName = 'HEP_Interface_Transaction_Error', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'test'); 
        //insert objEmail;
        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Constants__c objConstant = new HEP_Constants__c();
        objConstant.Name = 'HEP_Itunes_Interface_Error_Notify';
        objConstant.Value__c = 'HEP_Itunes_Interface_Error_Notify';
        insert objConstant;
        
        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        HEP_Interface_Transaction_Error__c objTxnError = new HEP_Interface_Transaction_Error__c();
        insert objTxnError;
        
        HEP_Interface__c objInt = new HEP_Interface__c();
        objInt.Name = 'HEP_iTunes_Price';
        objInt.Type__c = 'Outbound-Push';
        objInt.Class__c = 'HEP_Itunes_Price';
        objInt.Integration_Name__c = 'HEP_iTunes_Price';
        objInt.Record_Status__c = 'Active';
        insert objInt;
        
        HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
        objTxn.HEP_Interface__c = objInt.id;
        insert objTxn;
        
        HEP_Interface_Transaction_Error__c objTxnError1 = new HEP_Interface_Transaction_Error__c();
        objTxnError1.HEP_Interface_Transaction__c = objTxn.id;
        insert objTxnError1;
		
        String sRequest = 'Request String';
        String sResponse = 'Response String';
        Blob blobRequest = Blob.valueof(sRequest);
        Blob blobResponse = Blob.valueof(sResponse);

        ContentVersion cvRequest = new ContentVersion();
        cvRequest.ContentLocation = 'S';
        cvRequest.VersionData = blobRequest;
        cvRequest.title = 'Request';      
        cvRequest.PathOnClient ='Request';                   
        insert cvRequest; 

        ContentVersion cvResponse = new ContentVersion();
        cvResponse.VersionData = blobResponse;          
        cvResponse.ContentLocation = 'S';
        cvResponse.title = 'Response';      
        cvResponse.PathOnClient ='Response';           
        insert cvResponse;                

        ContentVersion objcvRequest = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cvRequest.Id];
        ContentVersion objcvResponse = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cvResponse.Id];


        ContentDocumentLink clRequest =new ContentDocumentLink();
        clRequest.LinkedEntityId= objTxnError.id;
        clRequest.ShareType= 'V';
        clRequest.ContentDocumentId=objcvRequest.ContentDocumentId;
        clRequest.Visibility = 'AllUsers'; 
        insert clRequest;

        ContentDocumentLink clResponse =new ContentDocumentLink();
        clResponse.LinkedEntityId= objTxnError.id;
        clResponse.ShareType= 'V';
        clResponse.ContentDocumentId=objcvResponse.ContentDocumentId;
        clResponse.Visibility = 'AllUsers'; 
        insert clResponse;
        
        //Create Outbound Email Record
        HEP_Outbound_Email__c objOutEmail = new HEP_Outbound_Email__c();
        objOutEmail.Record_Id__c = objTxnError.Id;
        objOutEmail.Email_Sent__c = false;
        objOutEmail.Email_Template_Name__c = 'HEP_Itunes_Interface_Error_Notify';
        objOutEmail.Object_API__c = 'HEP_Interface_Transaction_Error__c'; //Adding any random Id to required field
        insert objOutEmail;


    }
    
    //Method to test SearchSku Method when searching from Promotion
    @isTest
    static void testhandleAfterInsert() {

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        List<HEP_Interface_Transaction_Error__c> lstInterfaceTransactionError1 = [select Id,
                                    Name, 
                                    Error_Datetime__c,
                                    Error_Description__c,
                                    HEP_Interface_Transaction__r.Name,
                                    HEP_Interface_Transaction__r.HEP_Interface__r.Name,
                                    HEP_Interface_Transaction__c,
                                    HEP_Interface_Transaction__r.Object_Id__c,
                                    LastModifiedBy.Name, 
                                    Status__c from HEP_Interface_Transaction_Error__c where HEP_Interface_Transaction__r.HEP_Interface__r.Name = 'HEP_iTunes_Price'];
        List<HEP_Interface_Transaction_Error__c> lstInterfaceTransactionError = [select id, name from HEP_Interface_Transaction_Error__c];
        Set<id> setIds = new set <id>();
        setIds = new Map<Id, HEP_Interface_Transaction_Error__c>(lstInterfaceTransactionError1).keySet();
        
        System.runAs(u) {
            Test.startTest();
                HEP_InterfaceTxnErrorTriggerHandler.handleAfterInsert(lstInterfaceTransactionError);
                HEP_InterfaceTxnErrorTriggerHandler.sendiTunesInterfaceErrorEmail(lstInterfaceTransactionError1);
            Test.stoptest();
        }
    }    
}