/**
* HEP_Interface_Retry_Batch_Test -- Test class for the Interface Retry in HEP
* @author    Deloitte
*/
@isTest 
private class HEP_Interface_Retry_Batch_Test{
    /**
    * CreateData --  Test method to get Create the Interface Records
    * @return  :  no return value
    */
    @testSetup
    public static void createTestDataSetup(){
    
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        //Create Interface:
        HEP_Interface__c objInterfaceRecord = HEP_Test_Data_Setup_Utility.createInterface('HEP_FoxRetail_SKUImage','HEP_Interface_Retry_Batch',true,1,5,'Outbound-Pull',false,true,true);
        
    }
    @isTest 
    static void testInterfaceBatch() {
    	
    	HEP_Interface__c objInterface = [Select Id from HEP_Interface__c];
    	//Interface Transaction
    	HEP_Interface_Transaction__c objTransaction = new HEP_Interface_Transaction__c();
    	objTransaction.Status__c = 'Failure';
    	objTransaction.Next_Retry_Time__c = System.now();
    	objTransaction.Retry_Count__c = 0;
    	objTransaction.Transaction_Datetime__c = System.now();
    	objTransaction.HEP_Interface__c = objInterface.Id;
    	objTransaction.Object_Id__c='{"sTerritoryCode":"US","lstSkuObjectNumber":["9999"]}';
    	insert objTransaction;
    	
    	Test.startTest();
    	Database.executeBatch(new HEP_Interface_Retry_Batch(),10);
    	Test.stopTest();
    }
 }