/**
* HEP_INT_DheSkuMaster_Details -- DheSkuMaster Order details will be stored from NTS And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_INT_DheSkuMaster_Details implements HEP_IntegrationInterface{ 
    public static String sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    /**
    * HEP_INT_DheSkuMaster_Details -- dhe_sku_master details to store sku master details
    * @return  :  no return value
    * @author  :  Ram Bairwa
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_INT_DheSkuMasterWrapper objWrapper = new HEP_INT_DheSkuMasterWrapper();         
        if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sTimeStamp;
            String sLastPulledDate;
            String sNoOfRows;
            String sStartingFrom;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the TimeStamp, LastPulledDate, NoOfRows, StartingFrom details to get the sku master details through integration 
            HEP_INT_DheSkuInvoiceWrapperUtility objDheSkuInvoiceWrapperUtility = (HEP_INT_DheSkuInvoiceWrapperUtility)JSON.deserialize(sDetails,HEP_INT_DheSkuInvoiceWrapperUtility.class);
            if(objDheSkuInvoiceWrapperUtility != null){                             
            sTimeStamp = objDheSkuInvoiceWrapperUtility.sTimeStamp;
            sLastPulledDate = objDheSkuInvoiceWrapperUtility.sLastPulledDate;
            sNoOfRows = objDheSkuInvoiceWrapperUtility.sNoOfRows;
            sStartingFrom = objDheSkuInvoiceWrapperUtility.sStartingFrom;            
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_JDE_E1_OAUTH') != null && HEP_Utility.getConstantValue('HEP_JDE_SKU_MASTERDETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_JDE_SKU_MASTERDETAILS'));                                   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?HHMMSS=' + sTimeStamp + '&YYYYMMDD=' + sLastPulledDate+ '&NO_OF_ROWS=' + sNoOfRows+ '&STARTING_FROM=' + sStartingFrom);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                system.debug('reponse body'+objTxnResponse.sResponse);
                if(/*HEP_Constants__c.getValues('HEP_SUCCESS') != null  && HEP_Utility.getConstantValue('HEP_SUCCESS') != null
                    &&*/ objHttpResp.getStatus().equals('OK')){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                                       
                    String sResponse = objHttpResp.getBody();
                    sResponse = sResponse.replace(HEP_Utility.getConstantValue('HEP_CatalogNo') , HEP_Utility.getConstantValue('HEP_REPLACE_CatalogNo')); 
                    sResponse = sResponse.replace(HEP_Utility.getConstantValue('HEP_ECopy_Flag') , HEP_Utility.getConstantValue('HEP_REPLACE_ECopy_Flag'));
                    sResponse = sResponse.replace(HEP_Utility.getConstantValue('HEP_BW_Color') , HEP_Utility.getConstantValue('HEP_REPLACE_BW_Color'));
                    sResponse = sResponse.replace(HEP_Utility.getConstantValue('HEP_ThreeD_Flag') , HEP_Utility.getConstantValue('HEP_REPLACE_ThreeD_Flag'));
                    //system.debug('Rihdi'+JSON.deserialize(sResponse, HEP_INT_DheSkuMasterWrapper.dheSkuMaster.class));
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    objWrapper = (HEP_INT_DheSkuMasterWrapper)JSON.deserialize(sResponse, HEP_INT_DheSkuMasterWrapper.class);   
                     System.debug('after decalre objWrapper+++++   ' + objWrapper);
                        if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug('objWrapper.errors[0].detail'+objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        //calling helper method to perform mapping and upsert
                        objTxnResponse.bRetry = false;
                        System.debug('objWrapper+++++   ' + objWrapper);
                        updateMapping(objWrapper);
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }      
    } 
    @TestVisible   
    private static void updateMapping(HEP_INT_DheSkuMasterWrapper masterWrapper){
    	try{
    		System.debug('masterWrapper >>' + masterWrapper);
	        if(masterWrapper.dhe_sku_master == null || masterWrapper.dhe_sku_master.isEmpty()) return;
	        System.debug('masterWrapper >>' + masterWrapper);
	        List<HEP_Promotion__c> lstPromotionUpsert=New List<HEP_Promotion__c>();
	        Map<Id,HEP_Promotion__c> mapPromo=New Map<Id,HEP_Promotion__c>();
	        Map<Id,Id> mapPromoSkuMaster=New Map<Id,Id>();
	        Map<ID,ID> mapMasterSkuId=New Map<ID,ID>();
	        Map<String,Id> mapCatalogId=New Map<String,Id>();
	        List<String> lstCatalogId=New List<String>();
	        Map<String,Id> mapTerritory=New Map<String,Id>();
	        List<HEP_SKU_Master__c> updateMasterObject=New List<HEP_SKU_Master__c>();
	        List<HEP_Promotion_SKU__c> updatePromotionObject=New List<HEP_Promotion_SKU__c>();
	        Map<String,HEP_INT_DheSkuMasterWrapper.DheSkuMaster> mapSkuMasterWrapper=New Map<String,HEP_INT_DheSkuMasterWrapper.DheSkuMaster>();
	        Map<String,HEP_INT_DheSkuMasterWrapper.DheSkuMaster> mapSkuMasterPromoWrapper=New Map<String,HEP_INT_DheSkuMasterWrapper.DheSkuMaster>();
	        Map<String,HEP_SKU_Master__c> mapSkuMaster = New Map<String,HEP_SKU_Master__c>();
	        Map<String,HEP_SKU_Master__c> mapSkuPromotion = New Map<String,HEP_SKU_Master__c>();
	        //Store Format and Channel MAP Map<Meaning,code>
	                List<String> lstLOVFormatFields=new List<String>();
	                List<String> lstLOVChannelFields=new List<String>();
	                Map<String,String> mapLOVFormatValues=new Map<String,String>();
	                Map<String,String> mapLOVChannelValues=new Map<String,String>();
	                
	                String sFORMAT=HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
	                String sCHANNEL=HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
	                
	                lstLOVFormatFields.add(sFORMAT);
	                lstLOVChannelFields.add(sCHANNEL);
	                String sLOVName = HEP_Utility.getConstantValue('HEP_NAME_FIELD');
	                String sLOVValues = HEP_Utility.getConstantValue('HEP_LOV_FIELD_VALUES');
	                
	                mapLOVFormatValues=HEP_Utility.getLOVMapping(lstLOVFormatFields,sLOVValues,sLOVName);
	                mapLOVChannelValues=HEP_Utility.getLOVMapping(lstLOVChannelFields,sLOVValues,sLOVName);
	        
	        System.debug('masterWrapper >>' + masterWrapper);
	        System.debug('masterWrapper.dhe_sku_master >>' + masterWrapper.dhe_sku_master);
	        for(HEP_INT_DheSkuMasterWrapper.DheSkuMaster var:masterWrapper.dhe_sku_master){
	        	if(String.isBlank(var.Request_Id))
	            	mapSkuMasterWrapper.put(var.Item_Number,var);
	            else
	            	mapSkuMasterWrapper.put(var.Request_Id,var);
	            mapSkuMasterPromoWrapper.put(var.HEP_Promotion_SKU_Id,var);
	            lstCatalogId.add(var.Catalog);
	        }
	        System.debug('mapSkuMasterWrapper >>' + mapSkuMasterWrapper.keySet());
	
	        for(HEP_Catalog__c varCatalog:[Select Id,CatalogId__c from HEP_Catalog__c Where CatalogId__c in :lstCatalogId]){
	          mapCatalogId.put(varCatalog.CatalogId__c,varCatalog.Id);  
	        }
	        //storing territory id and code in Map
	        for(HEP_Territory__c varTerritory: [Select Id, Name, Territory_Code_Integration__c from HEP_Territory__c where Territory_Code_Integration__c !=null]){
	            mapTerritory.put(varTerritory.Territory_Code_Integration__c,varterritory.Id);
	        }
	        //getting HEP_SKU_Master__c record with the help of Item Number - SKU_Number__c
	        List<HEP_SKU_Master__c> resultMaster = [SELECT Id, Name, SKU_Number__c,SKU_Title__c,Barcode__c,JDE_Prelim_Flag__c,JDE_Item_Status__c,Channel__c,
	                                          Format__c,JDE_Digital_Copy_Flag__c,No_Of_Discs__c,SKU_Type__c,JDE_Price_Rule__c,
	                                          JDE_Wholesale_Price_CAN__c,JDE_Wholesale_Price_US__c,SKU_Configuration__c,
	                                          Record_Status__c,JDE_Feature_Run__c,JDE_SMF_Run__c,JDE_Total_Run__c,JDE_BD_Plus__c,
	                                          JDE_Dubbed_Language__c,JDE_Subtitle_Language__c,JDE_Aspect_Ratio__c,JDE_Cust_Exclusive__c,
	                                          JDE_Additional_Media__c,JDE_BD_Enhancement__c,JDE_BW_Color__c,JDE_Primary_Language__c,
	                                          JDE_Packaging__c,JDE_Enhanced_Packaging__c,JDE_Gift_Purchase__c,JDE_MPAA_Rating__c,
	                                          JDE_Canadian_Rating__c,JDE_Quebec_Rating__c,Catalog_Master__r.CatalogId__c,Territory__r.Name
	                                          FROM HEP_SKU_Master__c WHERE Name in :mapSkuMasterWrapper.keySet() OR SKU_Number__c IN :mapSkuMasterWrapper.keySet()];
	        for(HEP_SKU_Master__c varResultMaster:resultMaster){
	
	            //Kunal - updating logic to work with both Request_Id and Item_Number scenarios
	            if(mapSkuMasterWrapper.containsKey(varResultMaster.SKU_Number__c))
	                mapSkuMaster.put(varResultMaster.SKU_Number__c,varResultMaster);
	            else 
	                mapSkuMaster.put(varResultMaster.Name,varResultMaster);
	        }
	        System.debug('mapSkuMaster >>' + mapSkuMaster);
	        //getting HEP_SKU_Master__c record with the help of HEP Promotion SKU Id fields
	        for(HEP_Promotion_SKU__c varPromotionSku:[Select Id, SKU_Master__c from HEP_Promotion_SKU__c WHERE Name in :mapSkuMasterPromoWrapper.keySet()]){
	            mapMasterSkuId.put(varPromotionSku.SKU_Master__c,varPromotionSku.ID);
	        }
	        
	        System.debug('mapMasterSkuId ' + mapMasterSkuId);
	        List<HEP_SKU_Master__c> resultPromotion = [SELECT Id,SKU_Number__c,SKU_Title__c,Barcode__c,JDE_Prelim_Flag__c,JDE_Item_Status__c,Channel__c,
	                                          Format__c,JDE_Digital_Copy_Flag__c,No_Of_Discs__c,SKU_Type__c,JDE_Price_Rule__c,
	                                          JDE_Wholesale_Price_CAN__c,JDE_Wholesale_Price_US__c,SKU_Configuration__c,
	                                          Record_Status__c,JDE_Feature_Run__c,JDE_SMF_Run__c,JDE_Total_Run__c,JDE_BD_Plus__c,
	                                          JDE_Dubbed_Language__c,JDE_Subtitle_Language__c,JDE_Aspect_Ratio__c,JDE_Cust_Exclusive__c,
	                                          JDE_Additional_Media__c,JDE_BD_Enhancement__c,JDE_BW_Color__c,JDE_Primary_Language__c,
	                                          JDE_Packaging__c,JDE_Enhanced_Packaging__c,JDE_Gift_Purchase__c,JDE_MPAA_Rating__c,
	                                          JDE_Canadian_Rating__c,JDE_Quebec_Rating__c,Catalog_Master__r.CatalogId__c,Territory__r.Name
	                                          FROM HEP_SKU_Master__c WHERE ID in :mapMasterSkuId.KeySet() and Type__c = 'Request'];           
	        for(HEP_SKU_Master__c varResultPromotion:resultPromotion)  {
	            mapSkuPromotion.put(mapMasterSkuId.get(varResultPromotion.Id),varResultPromotion);
	        }   
	        System.debug('mapSkuPromotion ' + mapSkuPromotion);
	        //get Hep Promotion Record with the help of HEP_SKU_Master__c     
	        for(HEP_Promotion_SKU__c varPromSkuMaster:[select id,Promotion_Catalog__r.Promotion__r.Id,Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c, SKU_Master__r.Id from HEP_Promotion_SKU__c where SKU_Master__r.Name in: mapSkuMaster.KeySet() or Id in: mapSkuPromotion.KeySet()]){
	            mapPromoSkuMaster.put(varPromSkuMaster.Promotion_Catalog__r.Promotion__r.Id,varPromSkuMaster.SKU_Master__r.Id);
	            }
	        System.debug('mapPromoSkuMaster ' + mapPromoSkuMaster);
	        for(HEP_Promotion__c varProm:[select Id,LocalPromotionCode__c from HEP_Promotion__c where Id =:mapPromoSkuMaster.KeySet()]){
	            mapPromo.put(mapPromoSkuMaster.get(varProm.Id), varProm);
	        }        
	        for(String numberCount:mapSkuMasterWrapper.keySet()){
	            //Changing record for HEP_SKU_Master__c for Item Number 
	            if(mapSkuMaster.get(numberCount)!=null){
	            mapSkuMaster.get(numberCount).SKU_Title__c=mapSkuMasterWrapper.get(numberCount).Item_Description;
	            mapSkuMaster.get(numberCount).SKU_Number__c=mapSkuMasterWrapper.get(numberCount).Item_Number;
	            mapSkuMaster.get(numberCount).Barcode__c=mapSkuMasterWrapper.get(numberCount).UPC_Code;
	            mapSkuMaster.get(numberCount).JDE_Prelim_Flag__c='Y'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).Prelim_flag)? True : False;
	            mapSkuMaster.get(numberCount).JDE_Item_Status__c=mapSkuMasterWrapper.get(numberCount).Item_Status;
	            mapSkuMaster.get(numberCount).Current_Release_Date__c=convertStringToDate(mapSkuMasterWrapper.get(numberCount).Current_Release_Date);
	            //mapSkuMaster.get(numberCount).Territory__c=mapTerritory.get(mapSkuMasterWrapper.get(numberCount).Territory); 
	            system.debug('Format '+mapLOVFormatValues.containsKey(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type)+' value '+mapLOVFormatValues.get(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type));
	            system.debug('Channel '+mapLOVChannelValues.containsKey(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel)+' value '+mapLOVChannelValues.get(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel));
	            mapSkuMaster.get(numberCount).Format__c=mapLOVFormatValues.containsKey(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type)?mapLOVFormatValues.get(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type):mapSkuMasterWrapper.get(numberCount).Media_Type;
	            mapSkuMaster.get(numberCount).Channel__c=mapLOVChannelValues.containsKey(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel)?mapLOVChannelValues.get(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel):mapSkuMasterWrapper.get(numberCount).Channel; 
	            //mapSkuMaster.get(numberCount).Channel__c=mapSkuMasterWrapper.get(numberCount).Channel;
	            //mapSkuMaster.get(numberCount).Format__c=mapSkuMasterWrapper.get(numberCount).Media_Type;
	            mapSkuMaster.get(numberCount).JDE_Digital_Copy_Flag__c='Y'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).Digital_Copy_Flag)? True : False;
	            mapSkuMaster.get(numberCount).No_Of_Discs__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Number_of_Discs);
	            mapSkuMaster.get(numberCount).SKU_Type__c=mapSkuMasterWrapper.get(numberCount).Pack_Type;
	            mapSkuMaster.get(numberCount).JDE_Price_Rule__c=mapSkuMasterWrapper.get(numberCount).Price_Rule;
	            mapSkuMaster.get(numberCount).JDE_Wholesale_Price_US__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Wholesale_Price_US)/10000;
	            mapSkuMaster.get(numberCount).JDE_Wholesale_Price_CAN__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Wholesale_Price_CAN)/10000;
	            
	            if(mapCatalogId.containsKey(mapSkuMasterWrapper.get(numberCount).Catalog)){
	            	mapSkuMaster.get(numberCount).Catalog_Master__c=mapCatalogId.get(mapSkuMasterWrapper.get(numberCount).Catalog); 
	            }
	            mapSkuMaster.get(numberCount).JDE_Price_Rule__c=mapSkuMasterWrapper.get(numberCount).Price_Rule;
	            mapSkuMaster.get(numberCount).Record_Status__c = 'D'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).ActionCode) ? 'Inactive' : sACTIVE;                               
	            mapSkuMaster.get(numberCount).JDE_Feature_Run__c=mapSkuMasterWrapper.get(numberCount).Feature_Run;
	            mapSkuMaster.get(numberCount).JDE_SMF_Run__c=mapSkuMasterWrapper.get(numberCount).SMF_Run;     
	            mapSkuMaster.get(numberCount).JDE_Total_Run__c=mapSkuMasterWrapper.get(numberCount).Total_Run;
	            mapSkuMaster.get(numberCount).JDE_BD_Plus__c=mapSkuMasterWrapper.get(numberCount).BD_Plus;                                                                      
	            mapSkuMaster.get(numberCount).JDE_Dubbed_Language__c=mapSkuMasterWrapper.get(numberCount).Dubbed_Language;
	            mapSkuMaster.get(numberCount).JDE_Subtitle_Language__c=mapSkuMasterWrapper.get(numberCount).Subtitle_Language;
	            mapSkuMaster.get(numberCount).JDE_Aspect_Ratio__c=mapSkuMasterWrapper.get(numberCount).Aspect_Ratio;
	            mapSkuMaster.get(numberCount).JDE_Cust_Exclusive__c=mapSkuMasterWrapper.get(numberCount).Customer_Exclusive;
	            mapSkuMaster.get(numberCount).JDE_Additional_Media__c=mapSkuMasterWrapper.get(numberCount).Additional_Media;
	            mapSkuMaster.get(numberCount).JDE_BD_Enhancement__c=mapSkuMasterWrapper.get(numberCount).BD_Enhancement;
	            mapSkuMaster.get(numberCount).JDE_BW_Color__c=mapSkuMasterWrapper.get(numberCount).BW_Color;
	            mapSkuMaster.get(numberCount).JDE_Primary_Language__c=mapSkuMasterWrapper.get(numberCount).Primary_Language;
	            mapSkuMaster.get(numberCount).JDE_Packaging__c=mapSkuMasterWrapper.get(numberCount).Packaging;
	            mapSkuMaster.get(numberCount).JDE_Enhanced_Packaging__c=mapSkuMasterWrapper.get(numberCount).Enhanced_Packaging;
	            mapSkuMaster.get(numberCount).JDE_Gift_Purchase__c=mapSkuMasterWrapper.get(numberCount).Gift_With_Purchase_Flag;
	            mapSkuMaster.get(numberCount).JDE_MPAA_Rating__c=mapSkuMasterWrapper.get(numberCount).MPAA_Rating;
	            mapSkuMaster.get(numberCount).JDE_Canadian_Rating__c=mapSkuMasterWrapper.get(numberCount).Canadian_Rating;
	            mapSkuMaster.get(numberCount).JDE_Quebec_Rating__c=mapSkuMasterWrapper.get(numberCount).Quebec_Rating;
	            mapSkuMaster.get(numberCount).Type__c = 'Master';
	            mapSkuMaster.get(numberCount).SKU_Configuration__c=updateConfigure(mapSkuMasterWrapper.get(numberCount).e_Copy_Flag,mapSkuMasterWrapper.get(numberCount).ThreeD_Flag,mapSkuMasterWrapper.get(numberCount).UV_Code);
	            /*if(mapPromo.containsKey(mapSkuMaster.get(numberCount).Id)) {
	                mapPromo.get(mapSkuMaster.get(numberCount).Id).LocalPromotionCode__c=mapSkuMasterWrapper.get(numberCount).Local_PromoCode;
	                lstPromotionUpsert.add(mapPromo.get(mapSkuMaster.get(numberCount).Id));
	            }*/
	            updateMasterObject.add(mapSkuMaster.get(numberCount));  
	            
	             //Changing HEP_SKU_Master__c record for HEP_Promotion_SKU__c Id                                                                     
	            }else if (mapSkuPromotion.get(numberCount)!=null){
	              mapSkuPromotion.get(numberCount).SKU_Title__c=mapSkuMasterWrapper.get(numberCount).Item_Description;
	              mapSkuPromotion.get(numberCount).SKU_Number__c=mapSkuMasterWrapper.get(numberCount).Item_Number;
	            mapSkuPromotion.get(numberCount).Barcode__c=mapSkuMasterWrapper.get(numberCount).UPC_Code;
	            mapSkuPromotion.get(numberCount).JDE_Prelim_Flag__c='Y'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).Prelim_flag)? True : False;
	            mapSkuPromotion.get(numberCount).JDE_Item_Status__c=mapSkuMasterWrapper.get(numberCount).Item_Status;
	            mapSkuPromotion.get(numberCount).Current_Release_Date__c=convertStringToDate(mapSkuMasterWrapper.get(numberCount).Current_Release_Date);
	            //mapSkuPromotion.get(numberCount).Territory__c=mapTerritory.get(mapSkuMasterWrapper.get(numberCount).Territory); 
	            //mapSkuPromotion.get(numberCount).Channel__c=mapSkuMasterWrapper.get(numberCount).Channel;
	            //mapSkuPromotion.get(numberCount).Format__c=mapSkuMasterWrapper.get(numberCount).Media_Type;
	            mapSkuPromotion.get(numberCount).Format__c=mapLOVFormatValues.containsKey(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type)?mapLOVFormatValues.get(sFORMAT+'*'+mapSkuMasterWrapper.get(numberCount).Media_Type):mapSkuMasterWrapper.get(numberCount).Media_Type;
	            mapSkuPromotion.get(numberCount).Channel__c=mapLOVChannelValues.containsKey(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel)?mapLOVChannelValues.get(sCHANNEL+'*'+mapSkuMasterWrapper.get(numberCount).Channel):mapSkuMasterWrapper.get(numberCount).Channel; 
	            mapSkuPromotion.get(numberCount).JDE_Digital_Copy_Flag__c='Y'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).Digital_Copy_Flag)? True : False;
	            mapSkuPromotion.get(numberCount).No_Of_Discs__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Number_of_Discs);
	            mapSkuPromotion.get(numberCount).SKU_Type__c=mapSkuMasterWrapper.get(numberCount).Pack_Type;
	            mapSkuPromotion.get(numberCount).JDE_Price_Rule__c=mapSkuMasterWrapper.get(numberCount).Price_Rule;
	            mapSkuPromotion.get(numberCount).JDE_Wholesale_Price_US__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Wholesale_Price_US)/10000;
	            mapSkuPromotion.get(numberCount).JDE_Wholesale_Price_CAN__c=Decimal.ValueOf(mapSkuMasterWrapper.get(numberCount).Wholesale_Price_CAN)/10000;
	            
	            if(mapCatalogId.containsKey(mapSkuMasterWrapper.get(numberCount).Catalog)){
	            	mapSkuPromotion.get(numberCount).Catalog_Master__c=mapCatalogId.get(mapSkuMasterWrapper.get(numberCount).Catalog); 
	            }
	            mapSkuPromotion.get(numberCount).JDE_Price_Rule__c=mapSkuMasterWrapper.get(numberCount).Price_Rule;
	            mapSkuPromotion.get(numberCount).Record_Status__c = 'D'.equalsIgnoreCase(mapSkuMasterWrapper.get(numberCount).ActionCode) ? 'Inactive' : sACTIVE;                               
	            mapSkuPromotion.get(numberCount).JDE_Feature_Run__c=mapSkuMasterWrapper.get(numberCount).Feature_Run;
	            mapSkuPromotion.get(numberCount).JDE_SMF_Run__c=mapSkuMasterWrapper.get(numberCount).SMF_Run;     
	            mapSkuPromotion.get(numberCount).JDE_Total_Run__c=mapSkuMasterWrapper.get(numberCount).Total_Run;
	            mapSkuPromotion.get(numberCount).JDE_BD_Plus__c=mapSkuMasterWrapper.get(numberCount).BD_Plus;                                                                      
	            mapSkuPromotion.get(numberCount).JDE_Dubbed_Language__c=mapSkuMasterWrapper.get(numberCount).Dubbed_Language;
	            mapSkuPromotion.get(numberCount).JDE_Subtitle_Language__c=mapSkuMasterWrapper.get(numberCount).Subtitle_Language;
	            mapSkuPromotion.get(numberCount).JDE_Aspect_Ratio__c=mapSkuMasterWrapper.get(numberCount).Aspect_Ratio;
	            mapSkuPromotion.get(numberCount).JDE_Cust_Exclusive__c=mapSkuMasterWrapper.get(numberCount).Customer_Exclusive;
	            mapSkuPromotion.get(numberCount).JDE_Additional_Media__c=mapSkuMasterWrapper.get(numberCount).Additional_Media;
	            mapSkuPromotion.get(numberCount).JDE_BD_Enhancement__c=mapSkuMasterWrapper.get(numberCount).BD_Enhancement;
	            mapSkuPromotion.get(numberCount).JDE_BW_Color__c=mapSkuMasterWrapper.get(numberCount).BW_Color;
	            mapSkuPromotion.get(numberCount).JDE_Primary_Language__c=mapSkuMasterWrapper.get(numberCount).Primary_Language;
	            mapSkuPromotion.get(numberCount).JDE_Packaging__c=mapSkuMasterWrapper.get(numberCount).Packaging;
	            mapSkuPromotion.get(numberCount).JDE_Enhanced_Packaging__c=mapSkuMasterWrapper.get(numberCount).Enhanced_Packaging;
	            mapSkuPromotion.get(numberCount).JDE_Gift_Purchase__c=mapSkuMasterWrapper.get(numberCount).Gift_With_Purchase_Flag;
	            mapSkuPromotion.get(numberCount).JDE_MPAA_Rating__c=mapSkuMasterWrapper.get(numberCount).MPAA_Rating;
	            mapSkuPromotion.get(numberCount).JDE_Canadian_Rating__c=mapSkuMasterWrapper.get(numberCount).Canadian_Rating;
	            mapSkuPromotion.get(numberCount).JDE_Quebec_Rating__c=mapSkuMasterWrapper.get(numberCount).Quebec_Rating;
	            mapSkuPromotion.get(numberCount).Type__c = 'Master';
	            mapSkuPromotion.get(numberCount).SKU_Configuration__c=updateConfigure(mapSkuMasterWrapper.get(numberCount).e_Copy_Flag,mapSkuMasterWrapper.get(numberCount).ThreeD_Flag,mapSkuMasterWrapper.get(numberCount).UV_Code);
	            updateMasterObject.add(mapSkuPromotion.get(numberCount));
	            /* if(mapPromo.containsKey(mapSkuMaster.get(numberCount).Id)) {
	                mapPromo.get(mapSkuPromotion.get(numberCount).Id).LocalPromotionCode__c=mapSkuMasterWrapper.get(numberCount).Local_PromoCode;
	                lstPromotionUpsert.add(mapPromo.get(mapSkuMaster.get(numberCount).Id));
	            } */
	            }
	        }     
	        system.debug('Updating HEP_SKU_Master__c and HEP_Promotion__c record');
	      // if(!lstPromotionUpsert.isEmpty()) upsert lstPromotionUpsert;
	        if(!updateMasterObject.isEmpty()) upsert updateMasterObject;
        
        } catch(Exception ex){
            //throw error
            System.debug('Exception on Class : HEP_INT_DheSkuMaster_Details - updateMapping, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            HEP_Error_Log.genericException('Query/Update Erros','Query/Update Errors',ex,'HEP_INT_DheSkuMaster_Details','updateMapping',null,null);
        }
        
    }    
    private static String updateConfigure(String eCopy,String threeD,String uvCode){
        
        //Start -- Nidhin on 19-6-18
        /*String inputConfig,skuConfig,tempFlag;
        inputConfig=eCopy+threeD+uvCode+uvCode;
        for(HEP_Configuration__c varConfig:HEP_Configuration__c.getall().values())
        {
            tempFlag=varConfig.E_Copy_Flag__c+varConfig.ThreeD_Flag__c+varConfig.UV_Code__c+varConfig.FourK_UHD_HDR__c;
        	if(inputConfig.equalsIgnoreCase(tempFlag)){
        		skuConfig=varConfig.Value__c;
        	}
        }
        return skuConfig;*/
        String sSKUConfiguration = '';
        if(String.isNotBlank(threeD) && threeD.equals('Y')){
        	sSKUConfiguration += sDELIMITER + s3D;
        }
        if(String.isNotBlank(uvCode) && uvCode.equals('Y')){
        	sSKUConfiguration += sDELIMITER + sDIG_INS;
        }
        if(String.isNotBlank(eCopy) && eCopy.equals('Y')){
        	sSKUConfiguration += sDELIMITER + sMA;
        }
        sSKUConfiguration = sSKUConfiguration.removeStart(sDELIMITER);
        return sSKUConfiguration;
    }
    public static String sMA, s3D, sDIG_INS, sDELIMITER;
    static{
    	sDELIMITER = '_+_';
    	sMA = HEP_Utility.getConstantValue('HEP_SKU_CONFIG_MA');
    	s3D = HEP_Utility.getConstantValue('HEP_SKU_CONFIG_3D');
    	sDIG_INS = HEP_Utility.getConstantValue('HEP_SKU_CONFIG_DIG_INS');
    }
    //End -- Nidhin on 19-6-18
    private static Date convertStringToDate(String jdeDate){
	    Date parseDate = Date.parse(JdeDate);
	    String format = DateTime.newInstance(parseDate.year(),parseDate.month(),parseDate.day()).format('yyyy-MM-dd');
	    Date formatDate = Date.valueOf(format);
	    return formatDate;
    }
}