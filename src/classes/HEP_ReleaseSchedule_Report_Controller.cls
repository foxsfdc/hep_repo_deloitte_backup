/**
* HEP_ReleaseSchedule_Report_Controller --- Class to get the data for releaseschedule reports
* @author    Arjun Narayanan
*/
public without sharing class HEP_ReleaseSchedule_Report_Controller {
    public static String sACTIVE,sGERMANY;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sGERMANY = HEP_Utility.getConstantValue('HEP_TERRITORY_GERMANY');
        }

    public class MultiPicklistWrapper{
    public LIST<HEP_Utility.MultiPicklistWrapper> lstTerritories;
    public LIST<HEP_Utility.MultiPicklistWrapper> lstLOB; 

/**
* Class Constructor
* @return nothing
* @author Arjun Narayanan
*/
        public MultiPicklistWrapper(){
            lstTerritories = new LIST<HEP_Utility.MultiPicklistWrapper>();
            lstLOB = new LIST<HEP_Utility.MultiPicklistWrapper>();
        }
    }

/**
* pageData --- Wrapper class to hold all the page data
* @author    Arjun Narayanan
*/
    public class PageData{
        //Datetime dtExecutionTime;
        list<ReportRowData> lstDatingReportData;
        public Boolean bException;

/**
* Class Constructor
* @return nothing
* @author Arjun Narayanan
*/
        public PageData(){
            lstDatingReportData = new list<reportRowData>();
            bException = false;
        }
    }

/**
* reportRowData --- Wrapper class to hold one record data
* @author    Arjun Narayanan
*/
    public class ReportRowData{
        String sRegion;
        String sTitleName;
        String sFeatureOrTV;
        String sFinancialTitleId;
        Date dtReleaseDate;
        Date dtESTDate;
        String sESTStatus;
        Decimal dESTWindow;
        Date dtRetailDate;
        String sRetailStatus;
        Decimal dRetailWindow;
        Date dtVODDate;
        String sVODStatus;
        Decimal dVODWindow;
        Date dtRentalDate;
        String sRentalStatus;
        Decimal dRentalWindow;
        String sLanguage;
        String sLOB;
        String sDivision;
        String sProductionCompany;
        String sMarketingManager;
        Date dtFAD;
    }

/**
     * Helps to picklist values on page load.
     * @return MultiPicklistWrapper wrapper object 
     */
    @RemoteAction
    public static MultiPicklistWrapper getPicklistData(){
        
        MultiPicklistWrapper objMultiPicklist = new MultiPicklistWrapper();

        for(HEP_territory__c objTerritory : [SELECT id,Name FROM HEP_territory__c WHERE Name != :HEP_Utility.getConstantValue('REGION_HOME_OFFICE') AND Parent_Territory__c = null ORDER BY Name]){
            objMultiPicklist.lstTerritories.add(new HEP_Utility.MultiPicklistWrapper(objTerritory.Name, objTerritory.id, false));
        } 
        for(HEP_Line_Of_Business__c objLOB : [SELECT id,Name FROM HEP_Line_Of_Business__c ORDER BY Name]){
            objMultiPicklist.lstLOB.add(new HEP_Utility.MultiPicklistWrapper(objLOB.Name, objLOB.id, false));
        }

        return objMultiPicklist;

    }

/**
     * Helps to display data when report is run.
     * @param  dtStartDate value of start date for the report
     * @param  dtEndDate value of end date for the report
     * @param  objMultiPicklistWrapper selected values of the multiselect picklists
     * @return PageData wrapper object 
     */
    @RemoteAction
    public static PageData getPageData(String sStartDate, String sEndDate, MultiPicklistWrapper objMultiPicklistWrapper){
	    PageData objPageData = new PageData();
	       
	        Date dtStartDate;
	        Date dtEndDate;
	        if (String.isNotBlank(sStartDate)) {
	                dtStartDate = date.valueOf(sStartDate);
	            }
	        if (String.isNotBlank(sEndDate)) {
	            dtEndDate = date.valueOf(sEndDate);
	        }
	        system.debug('--------startdate'+ dtStartDate);
	        system.debug('--------enddate'+ dtEndDate);
	        
	        set<String> setTerritory = new set<String>();
	        set<String> setTerritoryClone = new set<String>();
	        set<String> setLOB = new set<String>();
	        set<String> setWPRId = new set<String>();
	        set<String> setTitleId = new set<String>();
	        set<String> setForFeature = new set<String>();
	        set<String> setUniqueKey = new set<String>();
	        set<String> setCatalogTitle = new set<String>();
	        set<String> setManagerName = new set<String>();
	        set<String> setLOBs = new set<String>();
	        setForFeature.add(HEP_Utility.getConstantValue('HEP_INT_JDE_DTV'));
	        setForFeature.add(HEP_Utility.getConstantValue('HEP_INT_JDE_FEATR'));
	        setForFeature.add(HEP_Utility.getConstantValue('HEP_INT_JDE_MKGOF'));
	        setForFeature.add(HEP_Utility.getConstantValue('HEP_INT_JDE_NMFLM'));
	        set<String> setForTV = new set<String>();
	        setForTV.add(HEP_Utility.getConstantValue('HEP_SEASN'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_FRMT'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_MINIS'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_MOW'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SHRTS'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SPCL'));
	        setForTV.add(HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SPRTS'));
	        set<String> setERMTerritoryCode = new set<String>();
	        set<String> setFoxipediaCode = new set<String>();
	        set<HEP_Promotion_Dating__c> setUniqueDatingRecord = new set<HEP_Promotion_Dating__c>();
	        List<String> lstDateTypes = new List<String>();
	        lstDateTypes.add(HEP_Utility.getConstantValue('HEP_DATETYPE_EST_RELEASE_DATE'));
	        lstDateTypes.add(HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE'));
	        lstDateTypes.add(HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RETAIL'));
	        lstDateTypes.add(HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RENTAL'));
	        list<HEP_Promotion_Dating__c> lstPromotionDating = new list<HEP_Promotion_Dating__c>();
	        list<HEP_Promotion_Dating__c> lstPromotionDatingUI = new list<HEP_Promotion_Dating__c>();
	        list<GTS_Release__c> lstGTSReleases = new list<GTS_Release__c>();
	        list<TVD_Release_Dates__c> lstTVDReleaseDates = new list<TVD_Release_Dates__c>();
	        list<Date> lstDate = new list<Date>();
	        map<String, HEP_Promotion_Dating__c> mapOfDatingRecords = new map<String, HEP_Promotion_Dating__c>();
	        map<String, String> mapOfLOBs = new map<String, String>();
	        map<String, String> mapOfManagers = new map<String, String>();
	        Map<string,GTS_Release__c> mapOfReleaseDates = new Map<string,GTS_Release__c>();
	        Map<string,TVD_Release_Dates__c> mapOFTVDates = new Map<string,TVD_Release_Dates__c>();
	        map<String,HEP_Promotion_Dating__c> mapOfRegionToESTDate = new map<String,HEP_Promotion_Dating__c>();
	        map<String,HEP_Promotion_Dating__c> mapOfRegionToVODDate = new map<String,HEP_Promotion_Dating__c>();
	        map<String,HEP_Promotion_Dating__c> mapOfRegionToRentalDate = new map<String,HEP_Promotion_Dating__c>();
	        map<String,HEP_Promotion_Dating__c> mapOfRegionToRetailDate = new map<String,HEP_Promotion_Dating__c>();
	        map<String, String> mapOfTitleToProductionCompany = new map<String,String>();
	        
	        //looping for selected territories
	        for(HEP_Utility.MultiPicklistWrapper objMultipicklistTerritory : objMultiPicklistWrapper.lstTerritories){
	            if(objMultipicklistTerritory.bSelected == true){
	                setTerritory.add(objMultipicklistTerritory.sValue);
	                setTerritoryClone.add(objMultipicklistTerritory.sValue);
	            }
	        }
	        for(HEP_Territory__c objTerritory : [SELECT Id,Name,ERMTerritoryCode__c,FoxipediaCode__c FROM HEP_Territory__c WHERE Parent_Territory__r.Name IN:setTerritoryClone]){
	            setTerritory.add(objTerritory.Name);
	            setERMTerritoryCode.add(objTerritory.ERMTerritoryCode__c);
	            setFoxipediaCode.add(objTerritory.FoxipediaCode__c);
	        }
	        system.debug('Territories ----->'+setTerritory);
	        //looping for selected Line of business's
	        for(HEP_Utility.MultiPicklistWrapper objMultipicklistLOB : objMultiPicklistWrapper.lstLOB){
	            if(objMultipicklistLOB.bSelected == true){
	                setLOB.add(objMultipicklistLOB.sValue);
	            }
	        }
	        system.debug('LOB ----->'+setLOB);
	        //Query Dating Records
	       

	        //system.debug('DatingRecords ----->'+lstPromotionDating);                          
	        for(HEP_Promotion_Dating__c objPromotionDating : [SELECT id,Name,Territory__r.Name,Territory__r.ERMTerritoryCode__c,HEP_Catalog__r.Catalog_Name__c,HEP_Catalog__r.Title_EDM__r.FIN_DIV_CD__r.Name,
	                                    promotion__r.Title__r.FIN_PROD_ID__c,Status__c,HEP_Promotions_Dating_Matrix__r.Date_Type__c,
	                                    Date__c,Language__c,Language__r.Name,promotion__r.LineOfBusiness__r.Name,HEP_Promotions_Dating_Matrix__c,
	                                    promotion__r.Title__r.FIN_DIV_CD__c,promotion__r.Domestic_Marketing_Manager__r.Name,HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
	                                    HEP_Catalog__r.Title_EDM__c,HEP_Catalog__r.Title_EDM__r.PROD_TYP_CD__r.PROD_TYP_CD__c,Promotion__r.LineOfBusiness__r.Type__c,
	                                    Territory__r.FoxipediaCode__c,Promotion__r.FirstAvailableDate__c
	                                    FROM HEP_Promotion_Dating__c
	                                    WHERE promotion__r.LineOfBusiness__r.Name IN :setLOB
	                                    AND Territory__r.Name IN :setTerritory
	                                    AND Record_Status__c =: sACTIVE
	                                    AND Date__c >=: dtStartDate 
	                                    AND Date__c <=: dtEndDate
	                                    AND Date__c != null
	                                    AND HEP_Promotions_Dating_Matrix__r.Date_Type__c in :lstDateTypes
	                                    ORDER BY Territory__r.Name ASC , HEP_Catalog__r.Catalog_Name__c ASC]){

	            if(objPromotionDating.promotion__r.Title__r.FIN_PROD_ID__c != null){
	                setWPRId.add(objPromotionDating.promotion__r.Title__r.FIN_PROD_ID__c);
	            }
	            
	            if(objPromotionDating.promotion__r.Title__c != null && (HEP_Constants__c.getValues('PROMOTION_LOB_TV').Value__c.equals(objPromotionDating.Promotion__r.LineOfBusiness__r.Type__c))){
	                setTitleId.add(objPromotionDating.promotion__r.Title__c);
	            }
	            //mapOfDatingRecords.put(objPromotionDating.Id, objPromotionDating);
	            
	            if(objPromotionDating.HEP_Catalog__r.Title_EDM__c != null){
	                setCatalogTitle.add(objPromotionDating.HEP_Catalog__r.Title_EDM__c);
	            }

	            if(!mapOfLOBs.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                mapOfLOBs.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c, objPromotionDating.promotion__r.LineOfBusiness__r.Name);
	            }
	            else{
	                String sLOBValues;
	                sLOBValues = mapOfLOBs.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c);
	                setLOBs.addAll(sLOBValues.split(' , '));
	                if(!setLOBs.contains(objPromotionDating.promotion__r.LineOfBusiness__r.Name)){
	                    sLOBValues += ' , '+objPromotionDating.promotion__r.LineOfBusiness__r.Name;
	                    mapOfLOBs.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c, sLOBValues); 
	                }
	            }

	            if(objPromotionDating.promotion__r.Domestic_Marketing_Manager__r.Name != null){
	                if(!mapOfManagers.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                    mapOfManagers.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c, objPromotionDating.promotion__r.Domestic_Marketing_Manager__r.Name);
	                }
	                else{
	                    String sManagerValues;
	                    sManagerValues = mapOfManagers.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c);
	                    setManagerName.addAll(sManagerValues.split(','));
	                    if(!setManagerName.contains(objPromotionDating.promotion__r.Domestic_Marketing_Manager__r.Name)){
	                        sManagerValues += ','+objPromotionDating.promotion__r.Domestic_Marketing_Manager__r.Name;
	                        mapOfManagers.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c, sManagerValues); 
	                    }
	                }
	            }
	            if(objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATETYPE_EST_RELEASE_DATE'))){
	                if(mapOfRegionToESTDate.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                    if(mapOfRegionToESTDate.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c).Date__c > objPromotionDating.Date__c){
	                        mapOfRegionToESTDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                    }
	                }
	                else{
	                    mapOfRegionToESTDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                }
	            }
	            else if(objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATETYPE_VOD_RELEASE_DATE'))){
	                if(mapOfRegionToVODDate.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                    if(mapOfRegionToVODDate.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c).Date__c > objPromotionDating.Date__c){
	                        mapOfRegionToVODDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                    }
	                }
	                else{
	                    mapOfRegionToVODDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                }
	            }
	            else if(objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RENTAL'))){
	                if(mapOfRegionToRentalDate.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                    if(mapOfRegionToRentalDate.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c).Date__c > objPromotionDating.Date__c){
	                        mapOfRegionToRentalDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                    }
	                }
	                else{
	                    mapOfRegionToRentalDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                }
	            }
	            else if(objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c != null && objPromotionDating.HEP_Promotions_Dating_Matrix__r.Date_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATETYPE_PHY_RELEASE_DATE_RETAIL'))){
	                if(mapOfRegionToRetailDate.containsKey(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c)){
	                    if(mapOfRegionToRetailDate.get(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c).Date__c > objPromotionDating.Date__c){
	                        mapOfRegionToRetailDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                    }
	                }
	                else{
	                    mapOfRegionToRetailDate.put(objPromotionDating.Territory__r.Name+'/'+objPromotionDating.HEP_Catalog__c+'/'+objPromotionDating.Language__c,objPromotionDating);
	                }
	            }
	        
	            
	            
	        }

	        system.debug('1 '+ Limits.getHeapSize());
	                
	        //looping to get map to match with WPR id   
	        // query GTS_title NOt Gts Release            
	        for(GTS_Release__c objReleases : [SELECT Id,Territory__r.GTS2_ERM_Territory_Id__c,Release_Date__c,Title__r.US_Release_Date__c,Total_GBO_from_Current_Estimate__c,Territory__r.Name,Title__r.WPR__c 
	                                          FROM GTS_Release__c 
	                                          WHERE Title__r.WPR__c != null
	                                          AND Release_Date__c != null
	                                          AND Title__r.WPR__c in :setWPRId
	                                          AND Territory__r.GTS2_ERM_Territory_Id__c IN :setERMTerritoryCode
	                                          ]){
	            if(objReleases.Title__r.WPR__c != null){
	                mapOfReleaseDates.put(objReleases.Title__r.WPR__c+'/'+objReleases.Territory__r.GTS2_ERM_Territory_Id__c,objReleases);
	            }
	        }
	        
	        
	        system.debug('2 '+ Limits.getHeapSize());
	        //looping to get map to match with title id     
	        //set update for only TV              
	        for(TVD_Release_Dates__c objTVDReleaseDates : [SELECT Id, Country_Code__c, TVD_Release_Date__c,Title_EDM__c 
	                                                       FROM TVD_Release_Dates__c 
	                                                       WHERE Title_EDM__c in :setTitleId
	                                                       AND Title_EDM__c != null
	                                                       AND Country_Code__c != null
	                                                       AND Country_Code__c IN :setFoxipediaCode
	                                                       AND TVD_Release_Date__c != null]){
	            if(objTVDReleaseDates.Title_EDM__c != null){
	                mapOFTVDates.put(objTVDReleaseDates.Title_EDM__c+'/'+objTVDReleaseDates.Country_Code__c,objTVDReleaseDates);
	            }
	        }
	        
	        system.debug('3 '+ Limits.getHeapSize());
	        system.debug('mapEST---->'+mapOfRegionToESTDate);
	        system.debug('mapVOD---->'+mapOfRegionToVODDate);
	        system.debug('mapRental---->'+mapOfRegionToRentalDate);
	        system.debug('mapRetail---->'+mapOfRegionToRetailDate);

	        for(HEP_Production_Company__c objProductionCompany : [SELECT Id,Name,Production_Company_Code__c,Production_Company_Description__c,
	                                                                     Sort_Order__c,Title__c,Unique_Id__c
	                                                                FROM HEP_Production_Company__c
	                                                                WHERE Title__c IN :setCatalogTitle]){
	            if(!mapOfTitleToProductionCompany.containsKey(objProductionCompany.Title__c)){
	                mapOfTitleToProductionCompany.put(objProductionCompany.Title__c, objProductionCompany.Production_Company_Description__c);
	            }
	            else{
	                String sProductionCompanyValues;
	                sProductionCompanyValues = mapOfTitleToProductionCompany.get(objProductionCompany.Title__c);
	                sProductionCompanyValues += ' , '+objProductionCompany.Production_Company_Description__c;
	                mapOfTitleToProductionCompany.put(objProductionCompany.Title__c, sProductionCompanyValues);     
	            }
	        }
	        system.debug('4 '+ Limits.getHeapSize());

	        //List<HEP_Promotion_Dating__c> lstDatingRecordUI = new List<HEP_Promotion_Dating__c>();
	        lstPromotionDating  = new list<HEP_Promotion_Dating__c>();
	        lstPromotionDating.addall(mapOfRegionToRetailDate.Values());
	        lstPromotionDating.addall(mapOfRegionToRentalDate.Values());
	        lstPromotionDating.addall(mapOfRegionToVODDate.Values());
	        lstPromotionDating.addAll(mapOfRegionToESTDate.Values());
	        system.debug('size--->'+lstPromotionDating.size());
	        //setUniqueDatingRecord.addall(lstTest);
	        ////setUniqueDatingRecord.add(mapOfRegionToRetailDate.Values());
	        //setUniqueDatingRecord.add(mapOfRegionToRentalDate.Values());
	        //setUniqueDatingRecord.add(mapOfRegionToVODDate.Values());
	        //setUniqueDatingRecord.add(mapOfRegionToESTDate.Values());

	        //lstPromotionDatingUI.addall(setUniqueDatingRecord);

	        //looping to get records to display
	        for(HEP_Promotion_Dating__c objDating : lstPromotionDating){
	            if(!setUniqueKey.contains(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c)){
	                lstDate.clear();    
	                ReportRowData objRowData = new ReportRowData();
	                setUniqueKey.add(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c);
	                objRowData.sRegion = objDating.Territory__r.Name;
	                objRowData.sTitleName = objDating.HEP_Catalog__r.Catalog_Name__c;

	                if(setForFeature.contains(objDating.HEP_Catalog__r.Title_EDM__r.PROD_TYP_CD__r.PROD_TYP_CD__c)){
	                    objRowData.sFeatureOrTV = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_FEATURE');
	                }
	                else if(setForTV.contains(objDating.HEP_Catalog__r.Title_EDM__r.PROD_TYP_CD__r.PROD_TYP_CD__c)){
	                    objRowData.sFeatureOrTV = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_TV');
	                }

	                objRowData.sFinancialTitleId = objDating.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c;
	                
	                if(HEP_Constants__c.getValues('PROMOTION_LOB_TV').Value__c.equals(objDating.Promotion__r.LineOfBusiness__r.Type__c)){
	                    String sUSCountryCode = HEP_Utility.getConstantValue('HEP_COUNTRY_CODE_US');
	                    String sTerritoryCode;
	                    if(objDating.Territory__r.FoxipediaCode__c != null){
	                          sTerritoryCode = objDating.Territory__r.FoxipediaCode__c;
	                    }
	                    if(mapOFTVDates.containsKey(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c)){
	                      if(mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).Country_Code__c != null){
	                        if(sTerritoryCode != null && (sTerritoryCode == mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).Country_Code__c || sUSCountryCode == mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).Country_Code__c)){
	                          objRowData.dtReleaseDate = mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).TVD_Release_Date__c;
	                        }
	                        else if(!String.isNotEmpty(sTerritoryCode) && sUSCountryCode == mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).Country_Code__c){
	                          objRowData.dtReleaseDate = mapOFTVDates.get(objDating.HEP_Catalog__r.Title_EDM__c+'/'+objDating.Territory__r.FoxipediaCode__c).TVD_Release_Date__c;
	                        }
	                      }
	                    }
	                }
	                else{
	                    if(mapOfReleaseDates.containsKey(objDating.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c+'/'+objDating.Territory__r.ERMTerritoryCode__c)){
	                        if(mapOfReleaseDates.get(objDating.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c+'/'+objDating.Territory__r.ERMTerritoryCode__c).Release_Date__c != null){
	                            objRowData.dtReleaseDate = mapOfReleaseDates.get(objDating.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c+'/'+objDating.Territory__r.ERMTerritoryCode__c).Release_Date__c;
	                        }
	                    }
	                }
	                if(mapOfRegionToESTDate.containsKey(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c)){
	                    objRowData.dtESTDate = mapOfRegionToESTDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Date__c;
	                    lstDate.add(objRowData.dtESTDate);
	                    objRowData.sESTStatus = mapOfRegionToESTDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Status__c;
	                    if(objRowData.dtReleaseDate != null && objRowData.dtESTDate != null && objRowData.dtReleaseDate.daysBetween(objRowData.dtESTDate) >= 0){
	                        objRowData.dESTWindow = objRowData.dtReleaseDate.daysBetween(objRowData.dtESTDate);             
	                    }
	                }
	                if(mapOfRegionToVODDate.containsKey(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c)){
	                    objRowData.dtVODDate = mapOfRegionToVODDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Date__c;
	                    lstDate.add(objRowData.dtVODDate);
	                    objRowData.sVODStatus = mapOfRegionToVODDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Status__c;
	                    if(objRowData.dtReleaseDate != null && objRowData.dtVODDate != null && objRowData.dtReleaseDate.daysBetween(objRowData.dtVODDate) >= 0){
	                        objRowData.dVODWindow = objRowData.dtReleaseDate.daysBetween(objRowData.dtVODDate);             
	                    }
	                }
	                if(mapOfRegionToRentalDate.containsKey(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c)){
	                    objRowData.dtRentalDate = mapOfRegionToRentalDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Date__c;
	                    lstDate.add(objRowData.dtRentalDate);
	                    objRowData.sRentalStatus = mapOfRegionToRentalDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Status__c;
	                    if(objRowData.dtReleaseDate != null && objRowData.dtRentalDate != null && objRowData.dtReleaseDate.daysBetween(objRowData.dtRentalDate) >= 0){
	                        objRowData.dRentalWindow = objRowData.dtReleaseDate.daysBetween(objRowData.dtRentalDate);           
	                    }
	                }
	                if(mapOfRegionToRetailDate.containsKey(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c)){
	                    objRowData.dtRetailDate = mapOfRegionToRetailDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Date__c;
	                    lstDate.add(objRowData.dtRetailDate);
	                    objRowData.sRetailStatus = mapOfRegionToRetailDate.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c).Status__c;
	                    if(objRowData.dtReleaseDate != null && objRowData.dtRetailDate != null && objRowData.dtReleaseDate.daysBetween(objRowData.dtRetailDate) >= 0){
	                        objRowData.dRetailWindow = objRowData.dtReleaseDate.daysBetween(objRowData.dtRetailDate);           
	                    }
	                }

	                if(objRowData.sESTStatus == null){
	                        objRowData.sESTStatus = HEP_Utility.getConstantValue('HEP_STATUS_NOT_RELEASED');
	                }
	                if(objRowData.sRetailStatus == null){
	                        objRowData.sRetailStatus = HEP_Utility.getConstantValue('HEP_STATUS_NOT_RELEASED');
	                }
	                if(objRowData.sRentalStatus == null){
	                        objRowData.sRentalStatus = HEP_Utility.getConstantValue('HEP_STATUS_NOT_RELEASED');
	                }
	                if(objRowData.sVODStatus == null){
	                        objRowData.sVODStatus = HEP_Utility.getConstantValue('HEP_STATUS_NOT_RELEASED');
	                }
	                objRowData.sLanguage = objDating.Language__r.Name;
	                objRowData.sLOB = mapOfLOBs.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c); 
	                objRowData.sDivision = objDating.HEP_Catalog__r.Title_EDM__r.FIN_DIV_CD__r.Name;
	                objRowData.sMarketingManager = mapOfManagers.get(objDating.Territory__r.Name+'/'+objDating.HEP_Catalog__c+'/'+objDating.Language__c); 
	                lstDate.sort();
	                objRowData.dtFAD = lstDate[0];
	                objRowData.sProductionCompany = mapOfTitleToProductionCompany.get(objDating.HEP_Catalog__r.Title_EDM__c);

	                objPageData.lstDatingReportData.add(objRowData);
	            }
	        }
	            system.debug('5 '+ Limits.getHeapSize());
	        
	    
        return objPageData;
    }

}