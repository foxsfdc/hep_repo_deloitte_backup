/**
* HEP_INT_NTSPurchaseOrderWrapper --NTS Purchase Order Wrapper to store the Purchase order details from NTS And E1 Interfaces 
* @author    Ram Bairwa
*/
public class HEP_NTSPurchaseOrderWrapper{
    public List<purchaseOrders> purchaseOrders;
    public String calloutStatus;
    public String TxnId;
    public List<Errors> errors;
    
    /**
    * PurchaseOrders -- Class to hold purchaseOrders details 
    * @author    Ram Bairwa
    */
    public class PurchaseOrders{
       //common fields for both interfaces NTS and E1
        public String OPEN;
        public String RUSHCODE;
        public String POLINE_NO;
        public String NAME;   
        public String PO_NO;
        public String COUNTRY;    
        public String OBJECT_NO;   
        public String ITEM_AMT;
		public String TERRITORY;    
        public String LINE_DESC;      
        public String ACTUAL; 
        public String REQ_DATE;
        public String POSTATUS;
        public String NETWORKID;  
        public String RUSHGPCODE;  
        public String CURRENCY_TYPE;
        public String CATALOG;
        public String FORMAT;
        public String REQUESTOR;  
        public String POTYPE;   
        public String VENDOR; 

    }
    
    /**
    * Errors -- Class to hold related details 
    * @author    Ram Bairwa
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }   
}