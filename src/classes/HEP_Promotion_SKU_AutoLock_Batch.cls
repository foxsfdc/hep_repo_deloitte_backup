/**
     * Batch class to set the status of Promotion SKU as Locked if it has entered the locking period
     * @author    Ashutosh Arora
    */
     public class HEP_Promotion_SKU_AutoLock_Batch implements Database.Batchable < sObject > {
        //Getting static Values from HEP_Constants
        public static String sNationalPromotion;
        public static String sRecordActive;
        public static String sUnlocked;
        public static String sDHE;
        static{
            sNationalPromotion = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
            sRecordActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'); 
            sUnlocked = HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED');
            sDHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
        }
    /**
     *start method - creating query and fetching all the Active and Unlocked Promotion SKU's
     * @param Batchable Context Object
     * @return Returns the query results with a List of Promotion SKU's.
     */         
    public Database.QueryLocator start(Database.BatchableContext objSKUData) {
        //Query National Promotion SKU Records which are Unlocked and Active
        //Setting all the fields to query
        String query = 'SELECT Promotion_Catalog__r.Promotion__c ';
        //query += 'Locked_Status__c,Promotion_Catalog__c,Promotion_LOB__c,Record_Status__c,Release_Id__c,SKU_Master__c,Unique_Id__c,SKU_Master__r.Channel__c, ';
        //query += 'SKU_Master__r.Territory__c,SKU_Master__r.Territory__r.Name,SKU_Master__r.Territory__r.Parent_Territory__r.Name ';
        //set from object in query
        query += 'FROM HEP_Promotion_SKU__c ';
        //setting conditions in query
        query += 'WHERE Promotion_Catalog__r.Promotion__r.Promotion_Type__c =: sNationalPromotion AND SKU_Master__r.Territory__r.Name =: sDHE AND Locked_Status__c =:sUnlocked ';
        query += 'AND Record_Status__c =:sRecordActive';
        //query = 'Select Id from hep_Promotion__c where Promotion_Type__c =: sNationalPromotion and id in (' + query + ')';
        System.debug(query);
        return Database.getQueryLocator(query);
    }
    
    /**
     *execute method - Calls the SKU Locking Batch Handler to process all the promotion SKU's
     * @param Batchable context object and List of HEP_Promotion_SKU__c 
     * @return nothing
     */
    public void execute(Database.BatchableContext objSKUData, List < sObject > scope) {
        if(scope != null && !scope.isEmpty()){
            Set<Id> setPrSKU = new Set<Id>();
            for (sObject objPrSKU : scope){
                HEP_Promotion_SKU__c objPromoSKU = (HEP_Promotion_SKU__c) objPrSKU;
                setPrSKU.add(objPromoSKU.Promotion_Catalog__r.Promotion__c);
            }
            List < HEP_Promotion_SKU__c > lstRecordsToSendLockReminders = HEP_Promotion_SKU_Batch_Handler.lockPromotionSKU(setPrSKU);
            if(lstRecordsToSendLockReminders != null && !lstRecordsToSendLockReminders.isEmpty()){
                HEP_Promotion_SKU_Batch_Handler.sendReminderEmail(lstRecordsToSendLockReminders);
            }   
        }
    }

    public void finish(Database.BatchableContext objSKUData) {}
}