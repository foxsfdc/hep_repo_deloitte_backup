/**
* HEP_Promotion_Reallocate_Controller --- Class Reallocate Set-Up Page
* @author Ayan Sarkar
*/
public class HEP_Promotion_Reallocate_Controller {
    public String sCurrencyFormat{get;set;}
    public String sPromotionTerritory{get;set;}
    public String sPromotionLocalPromoCode{get;set;}
    public String sPromotionId{get;set;}

     /**
    * HEP_Promotion_Reallocate_Controller Contructor --- Constructor to initialize the sCurrencyFormat
    * @return   N/A
    */
    public HEP_Promotion_Reallocate_Controller(){
        //Code to fetch the Promotion's Local Promo Code and Territory
        HEP_Market_Spend__c objMarketSpend = [SELECT ID, Name, Promotion__c, Promotion__r.Territory__r.Territory_Code_Integration__c,
                                              Promotion__r.LocalPromotionCode__c , Promotion__r.Territory__r.CurrencyCode__c
                                              FROM HEP_Market_Spend__c
                                              WHERE Id =: ApexPages.currentpage().getparameters().get('spendId')];

        sPromotionTerritory = objMarketSpend.Promotion__r.Territory__r.Territory_Code_Integration__c;
        sPromotionLocalPromoCode = objMarketSpend.Promotion__r.LocalPromotionCode__c;
        sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(objMarketSpend.Promotion__r.Territory__r.CurrencyCode__c);
        sPromotionId = objMarketSpend.Promotion__c;
    }

    /**
    * HEP_TerritoryDetails  -- Class to hold related details to be sent to the JDE and E1
    * @author    Abhishek Mishra
    */    
    public class SourecIds {
        public String sPromoId;
        public String sTerritory;
        public sourecIds(string sPromoId, string sTerritory) {
            this.sPromoId = sPromoId;
            this.sTerritory = sTerritory;
        }
    }

    /**
    * ReallocatePageDataWrapper --- Class to hold data for the Page
    * @author    Ayan Sarkar
    */
    public class ReallocatePageDataWrapper{
        public String sKey;
        public String sValue;
        public String sRequestor;
        //public String sBudgetRequestNumber;
        public String sDateSubmitted;
        public String sBudgetRequestStatus;
        public Decimal dTotalAmount;
        public Decimal dTotalAmountNew;
        public String sComments;
        //public HEP_Approvals__c objSpendApproval;
        public ApprovalWrapper objSpendApproval;
        public HEP_Market_Spend__c objMarketSpend;
        public List<SpendDetailsWrapper> lstSpendDetails;

        /**
        * SpendPageDataWrapper Contructor --- Constructor to initialize the variables
        * @return   N/A
        */
        public ReallocatePageDataWrapper(){
            //sBudgetRequestNumber = null;
            sDateSubmitted = null;
            sBudgetRequestStatus = null;
            dTotalAmount = 0.0;
            dTotalAmountNew = 0.0;
            lstSpendDetails = new List<SpendDetailsWrapper>();
            objSpendApproval = new ApprovalWrapper();
        }

    }

    /**
    * ReallocatePageDataWrapper --- Class to hold data for the Page
    * @author    Ayan Sarkar
    */
    public class ApprovalWrapper{
        public HEP_Approvals__c objApprovalRec;
        public List<HEP_Record_Approver__c> lstRecordApprover;
    }

    /**
    * SpendDetailsWrapper --- Class to the details of the Spend Details
    * @author    Ayan Sarkar
    */
    public class SpendDetailsWrapper{
        public String sKey;
        public String sValue;
        public Decimal dCurrentBudget; /*Budget alloted via the main spend -- Spend Detail Status = Current)*/
        public Decimal dOpenCommitments;
        public Decimal dPaidActuals;
        public Decimal dRequestedBudget;
        public String sGLAccountId;
        public String sDepartmentName;
        public Boolean bIsEdited;

         /**
        * SpendDetailsWrapper Contructor --- Constructor to initialize the variables
        * @return   N/A
        */
        public SpendDetailsWrapper(String sKey, String sValue, Decimal dCurrentBudget, Decimal dOpenCommitments, Decimal dPaidActuals,
                                   Decimal dRequestedBudget , String sGLAccountId, String sDepartmentName){
            this.sKey = sKey;
            this.sValue = sValue;
            this.dCurrentBudget = dCurrentBudget;
            this.dOpenCommitments = dOpenCommitments;
            this.dPaidActuals = dPaidActuals;
            this.dRequestedBudget = dRequestedBudget;
            this.sGLAccountId = sGLAccountId;
            this.sDepartmentName = sDepartmentName;
            this.bIsEdited = false;
        }

        public SpendDetailsWrapper(){}
    }

    /**
    * Method to integrate with Purchase Order.
    * @return SpendPageDataWrapper
    */
    @RemoteAction
    public static String getPurchaseOrder(String sPromotionId, String sPromotionTerritory, String sPromotionLocalPromoCode){
        //HEP_InvoicePurchaseOrderData objInvoicePurchaseOrderData;
        SourecIds sourecIdsValues = new SourecIds(sPromotionLocalPromoCode, sPromotionTerritory);
        //Callout for Purchase Order Integration
        HEP_InterfaceTxnResponse objPOIntegration = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(sourecIdsValues), '', HEP_Utility.getConstantValue('HEP_PURCHASE_ORDER_DETAILS'));
        //Saving the response to the Content Object.
        Map<string, string> mapResponseDataContent = new Map<string, string>();
        mapResponseDataContent.put(HEP_Utility.getConstantValue('HEP_PurchaseOrderIntegration'), JSON.serialize(objPOIntegration)/*.replace('\\' , '')*/);

        //Code to check if there's any existing Content..
        try{
            String sOldFileData = HEP_Integration_Util.retriveDatafromContent(sPromotionId, HEP_Utility.getConstantValue('HEP_PurchaseOrderIntegration'));
            if(sOldFileData != null){
                HEP_Integration_Util.updateExistingContent(sPromotionId, JSON.serialize(objPOIntegration), HEP_Utility.getConstantValue('HEP_PurchaseOrderIntegration'));
            }else{
                HEP_Integration_Util.saveFileDatainContent(sPromotionId, mapResponseDataContent, HEP_Utility.getConstantValue('HEP_CONTENT_TYPE_PDF'));
            }

        }catch(Exception e){
             //Code to log error..
            HEP_Error_Log.genericException('Error occured during PO Integration',
                                             'VF Controller',
                                              e,
                                             'HEP_Promotion_Spend_Controller',
                                             'getPurchaseOrder',
                                             null,
                                             '');

            throw e;
        }
        system.debug('JSON.serialize(objPOIntegration) : ' + JSON.serialize(objPOIntegration).replace('\\' , ''));
        return 'Success';
    }

    /**
    * Method to integrate with Purchase Order.
    * @return SpendPageDataWrapper
    */
    @RemoteAction
    public static String getInvoice(String sPromotionId, String sPromotionTerritory, String sPromotionLocalPromoCode){
        //HEP_InvoicePurchaseOrderData objInvoicePurchaseOrderData;
        SourecIds sourecIdsValues = new SourecIds(sPromotionLocalPromoCode, sPromotionTerritory);
        //Callout for Purchase Order Integration
        HEP_InterfaceTxnResponse objInvIntegration = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(sourecIdsValues), '', HEP_Utility.getConstantValue('HEP_INVOICE_DETAILS'));
        //Saving the response to the Content Object.
        Map<string, string> mapResponseDataContent = new Map<string, string>();
        mapResponseDataContent.put(HEP_Utility.getConstantValue('HEP_InvoiceIntegration'), JSON.serialize(objInvIntegration)/*.replace('\\' , '')*/);

        //Code to check if there's any existing Content..
        try{
            String sOldFileData = HEP_Integration_Util.retriveDatafromContent(sPromotionId, HEP_Utility.getConstantValue('HEP_InvoiceIntegration'));
            if(sOldFileData != null){
                HEP_Integration_Util.updateExistingContent(sPromotionId, JSON.serialize(objInvIntegration), HEP_Utility.getConstantValue('HEP_InvoiceIntegration'));
            }else{
                HEP_Integration_Util.saveFileDatainContent(sPromotionId, mapResponseDataContent, HEP_Utility.getConstantValue('HEP_CONTENT_TYPE_PDF'));
            }
        }catch(Exception e){
             //Code to log error..
            HEP_Error_Log.genericException('Error occured during PO Integration',
                                             'VF Controller',
                                              e,
                                             'HEP_Promotion_Spend_Controller',
                                             'getInvoice',
                                             null,
                                             '');

            throw e;
        }
        system.debug('JSON.serialize(objInvIntegration) : ' + JSON.serialize(objInvIntegration).replace('\\' , ''));
        return 'Success';
    }

    /**
    * Helps to get Data to be shown on screen on Page Load.
    * @return ReallocatePageDataWrapper
    */
    @RemoteAction
    public static ReallocatePageDataWrapper fetchReallocatePageData(String sSpendId, String sFormat , String sApprovalId){
        ReallocatePageDataWrapper objReallocatePageDataWrapper = new ReallocatePageDataWrapper();
        String sPromotionId;
        HEP_Market_Spend__c objMarketSpend;
        //JDE integration response saved against the spend
        String POINVOICEDATA;
        String sPurchaseOrderData;
        String sInvoiceData;
        HEP_InvoicePurchaseOrderData objInvoicePurchaseOrderData;
        Map<String , Decimal> mapDepartmentToPaidActuals;
        Map<String , Decimal> mapDepartmentToOpenCommitments;



        //Code to fetch all the Spend Related Information..
        Map<String, Schema.SObjectField> mapSpendDetailField = Schema.getGlobalDescribe().get('HEP_Market_Spend_Detail__c').getDescribe().fields.getMap();
        list<String> listSpendDetailField = new list<String>();
        listSpendDetailField.addAll(mapSpendDetailField.keyset());
        //Query to get the details for the Spends under the current Promotion.
        String sSpendDetailChildQuery = String.join(listSpendDetailField, ', ');
        sSpendDetailChildQuery += ' , GL_Account__r.Account_Department__c, GL_Account__r.Name FROM HEP_Market_Spend_Details__r '; 
        sSpendDetailChildQuery = 'SELECT (SELECT ' + sSpendDetailChildQuery + ' WHERE Type__c = \'Current\' AND Record_Status__c = \'Active\'),';
        String sWhereClause = ' WHERE Id = \''+ sSpendId + '\' AND Record_Status__c = \'Active\'';
        String sSpendQuery = HEP_Utility.buildQueryAllString('HEP_Market_Spend__c', sSpendDetailChildQuery, sWhereClause);
        System.debug('!@$%^ Query : ' + sSpendQuery);
        List<HEP_Market_Spend__c> lstMarketSpend = Database.query(sSpendQuery);
        System.debug('#@!#@! lstMarketSpend : ' + lstMarketSpend);

        if(lstMarketSpend != NULL && !lstMarketSpend.isEmpty()){

            //Code to populate approval related data..

            Map<String, Schema.SObjectField> mapRecordApproverField = Schema.getGlobalDescribe().get('HEP_Record_Approver__c').getDescribe().fields.getMap();
            list<String> listRecordApproverField = new list<String>();
            listRecordApproverField.addAll(mapRecordApproverField.keyset());
            //Query to get the details for the Spends under the current Promotion.
            String sRecordApproverChildQuery = String.join(listRecordApproverField, ', ');
            sRecordApproverChildQuery += ' FROM Record_Approvers__r '; 
            sRecordApproverChildQuery = 'SELECT CreatedBy.Name ,  (SELECT ' + sRecordApproverChildQuery + ' WHERE Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' ORDER BY Sequence__c ASC),';
            //String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND HEP_Market_Spend__c = \'' + sSpendId + '\'';
            String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND Id = \'' + sApprovalId + '\'';
            String sApprovalQuery = HEP_Utility.buildQueryAllString('HEP_Approvals__c', sRecordApproverChildQuery, sApprovalWhereClause);
            
            List<HEP_Approvals__c> lstSpendApprovals = Database.query(sApprovalQuery);
            System.debug('sApprovalQuery ---> ' + sApprovalQuery);
            System.debug('lstSpendApprovals ---> ' + lstSpendApprovals);

            /*String sApprovalWhereClause = ' WHERE Approval_Status__c = \'' + HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING') + '\' AND HEP_Market_Spend__c = \'' + sSpendId + '\' ORDER BY Sequence__c DESC';
            String sApprovalQuery = HEP_Utility.buildQueryAllString('HEP_Approvals__c' , 'SELECT CreatedBy.Name , ' , sApprovalWhereClause);
            System.debug('sApprovalQuery ---> ' + sApprovalQuery);
            List<HEP_Approvals__c> lstSpendApprovals = Database.query(sApprovalQuery);*/
            if(lstSpendApprovals != NULL && !lstSpendApprovals.isEmpty()){
                //objReallocatePageDataWrapper.objSpendApproval = lstSpendApprovals[0];
                objReallocatePageDataWrapper.objSpendApproval.objApprovalRec = new HEP_Approvals__c(Id = lstSpendApprovals[0].Id,
                                                                                    Approval_Status__c = lstSpendApprovals[0].Approval_Status__c,
                                                                                    Approval_Type__c = lstSpendApprovals[0].Approval_Type__c,
                                                                                    Comments__c = lstSpendApprovals[0].Comments__c,
                                                                                    HEP_Market_Spend__c = lstSpendApprovals[0].HEP_Market_Spend__c,
                                                                                    HEP_Promotion_Dating__c = lstSpendApprovals[0].HEP_Promotion_Dating__c,
                                                                                    HEP_Promotion_SKU__c = lstSpendApprovals[0].HEP_Promotion_SKU__c,
                                                                                    Prior_Submitter__c = lstSpendApprovals[0].Prior_Submitter__c,
                                                                                    Record_ID__c = lstSpendApprovals[0].Record_ID__c);
                
                objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover = lstSpendApprovals[0].Record_Approvers__r;

                System.debug('Error might come ----> ' + objReallocatePageDataWrapper.objSpendApproval.objApprovalRec);

                DateTime dtSubDate = DateTime.newInstance(lstSpendApprovals[0].CreatedDate.year(), lstSpendApprovals[0].CreatedDate.month(), lstSpendApprovals[0].CreatedDate.day());
                String formattedDate = dtSubDate.format(sFormat);

                objReallocatePageDataWrapper.sRequestor = lstSpendApprovals[0].CreatedBy.Name;
                objReallocatePageDataWrapper.sDateSubmitted = formattedDate;
                objReallocatePageDataWrapper.sBudgetRequestStatus = lstSpendApprovals[0].Approval_Status__c;
            }


            objMarketSpend = lstMarketSpend[0];
            //objReallocatePageDataWrapper.objMarketSpend = objMarketSpend;
            objReallocatePageDataWrapper.objMarketSpend = new HEP_Market_Spend__c(Id = objMarketSpend.Id,
                                                                                  Comments__c = objMarketSpend.Comments__c,
                                                                                  Promotion__c = objMarketSpend.Promotion__c,
                                                                                  Record_Status__c = objMarketSpend.Record_Status__c,
                                                                                  RequestDate__c = objMarketSpend.RequestDate__c,
                                                                                  RequestNumber__c = objMarketSpend.RequestNumber__c,
                                                                                  RequestType__c = objMarketSpend.RequestType__c,
                                                                                  RecordStatus__c = objMarketSpend.RecordStatus__c);
            objReallocatePageDataWrapper.sKey = objMarketSpend.Id;
            //objReallocatePageDataWrapper.sBudgetRequestNumber = objMarketSpend.Name;
            objReallocatePageDataWrapper.sValue = objMarketSpend.Name;
            objReallocatePageDataWrapper.dTotalAmount = objMarketSpend.Request_Amount__c;
            objReallocatePageDataWrapper.sComments = objMarketSpend.Comments__c;
            sPromotionId = objMarketSpend.Promotion__c;

            //Integration Part --> Start
            Map<String, list<String>> mapParentIdsFileNames = new Map<String, list<String>>();
            LIST<String> lstFileNames = new LIST<String>{'PurchaseOrderIntegration','InvoiceIntegration'};

            mapParentIdsFileNames.put(sPromotionId, lstFileNames);
            Map<String, map<string, string>> mapPromotionToContentFiles = HEP_Integration_Util.retriveDatafromContentBulkified(mapParentIdsFileNames);
            System.debug('!@#@! mapPromotionToContentFiles --> ' + mapPromotionToContentFiles);
            HEP_InterfaceTxnResponse objDeserializedPO = new HEP_InterfaceTxnResponse();
            HEP_InterfaceTxnResponse objDeserializedInv = new HEP_InterfaceTxnResponse();

            System.debug('@@@ sPromotionId : ' + sPromotionId);
            String sPromotionId18Digit = (ID) sPromotionId;
            if(mapPromotionToContentFiles.containskey(sPromotionId18Digit)){
                Map<string, string> mapContentDataNameWise = mapPromotionToContentFiles.get(sPromotionId18Digit);
                System.debug('mapContentDataNameWise --> ' + mapContentDataNameWise);
                if(mapContentDataNameWise.containsKey('PurchaseOrderIntegration')){
                    objDeserializedPO = (HEP_InterfaceTxnResponse) JSON.deserialize(mapContentDataNameWise.get('PurchaseOrderIntegration') , HEP_InterfaceTxnResponse.class); 
                    System.debug('objDeserializedPO --> ' + objDeserializedPO);
                }

                if(mapContentDataNameWise.containsKey('InvoiceIntegration')){
                    objDeserializedInv = (HEP_InterfaceTxnResponse) JSON.deserialize(mapContentDataNameWise.get('InvoiceIntegration') , HEP_InterfaceTxnResponse.class);
                    System.debug('objDeserializedInv --> ' + objDeserializedInv);
                }
            }


            objInvoicePurchaseOrderData = HEP_Utility.processPOInvoice(objDeserializedPO.sResponse, objDeserializedInv.sResponse , sPromotionId);  
            
            System.debug('Integration Details --> ' + objInvoicePurchaseOrderData);

            //Create Map for Department To Commitments and Paid Actuals.
            if(objInvoicePurchaseOrderData != NULL){
                mapDepartmentToPaidActuals = new Map<String , Decimal>();
                mapDepartmentToOpenCommitments = new Map<String , Decimal>();
                for(HEP_InvoicePurchaseOrderData.Department objDepartment : objInvoicePurchaseOrderData.lstDepartments){
                    mapDepartmentToPaidActuals.put(objDepartment.sDepartmentName , objDepartment.dPaidActuals);
                    mapDepartmentToOpenCommitments.put(objDepartment.sDepartmentName , objDepartment.dCommitments);
                }
            }
            //Integration Part --> End



            //Code to fetch the Spend Details Data.
            for(HEP_Market_Spend_Detail__c objMarketSpendDetailRec : objMarketSpend.HEP_Market_Spend_Details__r){
                Decimal dOpenCommitmentsTemp = 0;
                Decimal dPaidActualsTemp = 0;
                //Code to set the Paid Actuals
                if(mapDepartmentToPaidActuals!= NULL && !mapDepartmentToPaidActuals.isEmpty()){
                    if(mapDepartmentToPaidActuals.get(objMarketSpendDetailRec.Department__c) != NULL){
                        dPaidActualsTemp = mapDepartmentToPaidActuals.get(objMarketSpendDetailRec.Department__c);
                    }
                }
                //Code to set the open Commitments
                if(mapDepartmentToOpenCommitments!= NULL && !mapDepartmentToOpenCommitments.isEmpty()){
                    if(mapDepartmentToOpenCommitments.get(objMarketSpendDetailRec.Department__c) != NULL){
                        dOpenCommitmentsTemp = mapDepartmentToOpenCommitments.get(objMarketSpendDetailRec.Department__c);
                    }
                }

                //Populate the Spend Details Fields data for the Page.
                SpendDetailsWrapper objSpendDetailRec = new SpendDetailsWrapper(objMarketSpendDetailRec.ID,
                                                                                objMarketSpendDetailRec.Name,
                                                                                objMarketSpendDetailRec.Budget__c,
                                                                                dOpenCommitmentsTemp,
                                                                                dPaidActualsTemp,
                                                                                objMarketSpendDetailRec.RequestAmount__c,
                                                                                objMarketSpendDetailRec.GL_Account__c,
                                                                                objMarketSpendDetailRec.Department__c);

                objReallocatePageDataWrapper.lstSpendDetails.add(objSpendDetailRec);

            }
        }
        return objReallocatePageDataWrapper;
    }


    /**
    * Helps to get Data to be shown on screen on Page Load.
    * @return ReallocatePageDataWrapper
    */
    @RemoteAction
    public static Boolean saveReallocate(ReallocatePageDataWrapper objReallocatePageDataWrapper){
        Boolean bSuccess = false;
        HEP_Record_Approver__c objLastRecordApprover = null;
        HEP_Approvals__c objApproval = objReallocatePageDataWrapper.objSpendApproval.objApprovalRec;
        List<SObject> lstToUpdate = new List<SObject>();

        

        //Step - 1 : Code to update the Spend Detail Records..
        //Data to be updated for Spend Details
        List<HEP_Market_Spend_Detail__c> lstSpendDetailsToUpdate = new List<HEP_Market_Spend_Detail__c>();
        for(SpendDetailsWrapper objMarketSpendDetail : objReallocatePageDataWrapper.lstSpendDetails){
            //if(objMarketSpendDetail.bIsEdited){
                HEP_Market_Spend_Detail__c objSpendDetailsToUpdate = new HEP_Market_Spend_Detail__c();
                objSpendDetailsToUpdate.Id = objMarketSpendDetail.sKey;
                objSpendDetailsToUpdate.GL_Account__c = objMarketSpendDetail.sGLAccountId;
                objSpendDetailsToUpdate.RequestAmount__c = objMarketSpendDetail.dRequestedBudget;
                //objSpendDetailsToUpdate.Comments__c = objSpendPageDataWrapper.sComments;
                objSpendDetailsToUpdate.Requestor__c = UserInfo.getUserId();
                //Add all the Details to a list to insert it.
                lstSpendDetailsToUpdate.add(objSpendDetailsToUpdate);
           // }
        }
        System.debug('New Spend and Spend Details Updated --> ' + JSON.serializePretty(lstSpendDetailsToUpdate));
        //update lstSpendDetailsToUpdate;
        if(lstSpendDetailsToUpdate != NULL && !lstSpendDetailsToUpdate.isEmpty())
            //lstToUpdate.addAll(lstSpendDetailsToUpdate);
            update lstSpendDetailsToUpdate;

        HEP_Market_Spend__c objMarketSpendCurrent = [SELECT Id, Name, Request_Amount__c , Approved_Amount__c , Comments__c
                                                     FROM HEP_Market_Spend__c
                                                     WHERE Id =: objReallocatePageDataWrapper.objMarketSpend.Id];


        //Step - 2 : Code to handle the Approval Process..
        System.debug('objReallocatePageDataWrapper ----> ' + JSON.serializePretty(objReallocatePageDataWrapper));
        if(objReallocatePageDataWrapper.dTotalAmountNew <= objReallocatePageDataWrapper.dTotalAmount){
            

            System.debug('objApproval ----> ' + objApproval);
            System.debug('objApproval.Record_Approvers__r ----> ' + objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover);
            System.debug('objReallocatePageDataWrapper.dTotalAmount ----> ' + objReallocatePageDataWrapper.dTotalAmount);
            System.debug('objReallocatePageDataWrapper.dTotalAmountNew ----> ' + objReallocatePageDataWrapper.dTotalAmountNew);

            if(objApproval != NULL && objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover != NULL && !objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover.isEmpty()){
                for(HEP_Record_Approver__c objRecordApprover : objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover){
                    //objRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                    //objRecordApprover.IsActive__c = true;
                    if(objRecordApprover.IsActive__c){
                        objRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                        objRecordApprover.Approver__c = UserInfo.getUserId();
                    }
                    else
                        objRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_CANCELLED');
                }
            }
            //update objReallocatePageDataWrapper.objSpendApproval.Record_Approvers__r;
            lstToUpdate.addAll(objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover);

            objApproval.Approval_Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
            objApproval.History_Data__c = JSON.serializePretty(new HEP_Promotion_Spend_Controller.ApprovalHistory(objMarketSpendCurrent.Comments__c , 
                                                                    objMarketSpendCurrent.Request_Amount__c,
                                                                    objMarketSpendCurrent.Approved_Amount__c));
            //update objApproval;
            lstToUpdate.add(objApproval);

            
            objReallocatePageDataWrapper.objMarketSpend.RecordStatus__c = HEP_Utility.getConstantValue('HEP_SPEND_BUDGET_APPROVED');
            //update objReallocatePageDataWrapper.objMarketSpend;
            lstToUpdate.add(objReallocatePageDataWrapper.objMarketSpend);

            //update lstToUpdate;
        }else{
            HEP_Record_Approver__c objPendingRecordApprover;
            for(HEP_Record_Approver__c objRecordApprover : objReallocatePageDataWrapper.objSpendApproval.lstRecordApprover){
                if(objRecordApprover.Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING')) &&
                   objRecordApprover.IsActive__c){
                    objRecordApprover.Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
                    objRecordApprover.Approver__c = UserInfo.getUserId();
                    objPendingRecordApprover = objRecordApprover;
                }
            }
                System.debug('lstToUpdate ----> ' + lstToUpdate);
                lstToUpdate.add(objPendingRecordApprover);

            if(objApproval != NULL){
                objApproval.History_Data__c = JSON.serializePretty(new HEP_Promotion_Spend_Controller.ApprovalHistory(objMarketSpendCurrent.Comments__c , 
                                                                    objMarketSpendCurrent.Request_Amount__c,
                                                                    objMarketSpendCurrent.Approved_Amount__c));
                //update objApproval;
                lstToUpdate.add(objApproval);
            }
        }
            
        if(lstToUpdate != NULL && !lstToUpdate.isEmpty())
            update lstToUpdate;
        return bSuccess;
    }

/**
* Its used to approve or reject records
* @param bApprove to check type of action
* @param sApprovalId to get affected records
* @param rejectCommets to store comments if rejected
* @return nothing
* @author Arjun Narayanan
*/
    @RemoteAction 
    public static void approveRejectRecords(Boolean bApprove, String sApprovalId, String rejectComments){

        String sStatus = bApprove ? HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED') : HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
   
        list<HEP_Record_Approver__c> lstRecordApprovers = new list<HEP_Record_Approver__c>();
        lstRecordApprovers = [SELECT Id FROM HEP_Record_Approver__c WHERE Approval_Record__r.Id =:sApprovalId AND IsActive__c = true AND Approver__c = null];           
 
        HEP_Record_Approver__c objRecordApprover = new HEP_Record_Approver__c();
        objRecordApprover.Status__c = sStatus;
        objRecordApprover.id = lstRecordApprovers[0].id;
        objRecordApprover.Approver__c = UserInfo.getUserId();
        objRecordApprover.Comments__c = rejectComments;
        
        update objRecordApprover;
    }
}