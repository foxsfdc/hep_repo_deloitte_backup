/**
* HEP_SKU_Customer_Siebel_Wrapper -- SKU Customer Wrapper for getting field names for HEP_Customer
* object and then converting them to JSon output fields
* @author    Mayank Jaggi
*/

public class HEP_SKU_Customer_Siebel_Wrapper implements HEP_IntegrationInterface
{

    /**
    * Item --- record level wrapper to hold the record details
    * @author   Mayank Jaggi
    */
    public class Item
    {
        public string objectType;
        public string createdDate;
        public string createdBy;
        public string updatedDate;
        public string updatedBy;
        public string recordId;
        public string recordStatus;
        public string territoryId;
        public string territoryName;
        public string skuId;
        public string skuNumber;
        public string customerName;
        public string legacyId;         
    }

    public class Payload
    {
        public List<Item> items;    
    }

    public class Data
    {
        public Payload Payload;
        public Data(Payload CustomerPayload)
        {
            this.Payload = CustomerPayload;
        }
    }

    /**
    * SKUCustomer --- main wrapper which holds the entire request
    * @author    Mayank Jaggi
    */
    public class SKUCustomer
    {
        public string ProducerID;
        public string EventType;
        public string EventName;
        public Data Data;
        public SKUCustomer(Data CustomerData)
        {
            this.Data = CustomerData;
            this.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
            this.EventType = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
            this.EventName = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        }
    }

    /**
    * ErrorResponseWrapper --- Wrapper class for response parse
    * @author    Mayank Jaggi
    */
    public class ErrorResponseWrapper 
     {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage)
        {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }

    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Mayank Jaggi
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        List <String> lstRecordIds = new List <String>();
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();

        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');

            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization', sAccessToken);
                if(String.isNotBlank(objTxnResponse.sSourceId))
                {
                    lstRecordIds = objTxnResponse.sSourceId.split(',');
                    system.debug('Request ---- ' + getCustomerFields(lstRecordIds));
                    httpRequest.setBody(getCustomerFields(lstRecordIds));                    
                }
                else
                {
                    system.debug('objTxnResponse.sSourceId is Null');
                }                                                      
                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); 
                objTxnResponse.sResponse = httpResp.getBody(); 
                if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK') || httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')){
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');  
            objTxnResponse.bRetry = true;
        }    
    }

    /**
    * Get the serialized json of the request to be sent
    * @param  list of record ids
    * @return 
    * @author Mayank Jaggi
    */
    public static string getCustomerFields(List <Id> SKUCustomerIdList)
    {
        String JsonOutput = '';
        List <HEP_SKU_Customer__c> SKUCustomerRecords = new  List <HEP_SKU_Customer__c>();

        
        SKUCustomerRecords = [Select CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name,
                              Record_Status__c, SKU_Master__r.SKU_Number__c,
                              Customer__r.CustomerName__c, SKU_Master__r.Territory__r.Id, Legacy_Id__c,         
                              SKU_Master__r.Territory__r.Name from HEP_SKU_Customer__c where 
                              HEP_SKU_Customer__c.Id in :SKUCustomerIdList];

        if(!SKUCustomerRecords.isEmpty())
        {
            List<Item> lstListitem = new List<Item>();

            for (HEP_SKU_Customer__c SKUCus : SKUCustomerRecords)
            {
                Item CusItem = new Item();
                CusItem.createdDate = String.ValueOf(SKUCus.CreatedDate);
                CusItem.createdBy = SKUCus.CreatedBy.Name;
                CusItem.updatedDate = String.ValueOf(SKUCus.LastModifiedDate);
                CusItem.updatedBy = SKUCus.LastModifiedBy.Name;
                CusItem.recordId = SKUCus.Id;
                CusItem.recordStatus = SKUCus.Record_Status__c;
                CusItem.territoryId = SKUCus.SKU_Master__r.Territory__c;
                CusItem.territoryName = SKUCus.SKU_Master__r.Territory__r.Name;
                CusItem.skuId = SKUCus.SKU_Master__c;  
                CusItem.skuNumber = SKUCus.SKU_Master__r.SKU_Number__c;
                CusItem.customerName = SKUCus.Customer__r.CustomerName__c;
                CusItem.objectType = HEP_Utility.getConstantValue('HEP_SKU_Customer_objecttype'); 
                CusItem.legacyId = SKUCus.Legacy_Id__c;         
                lstListitem.add(CusItem);
            }

            Payload CustomerPayload = new Payload();
            CustomerPayload.items = lstListitem;   
            Data CustomerData = new Data(CustomerPayload);
            SKUCustomer SKUCust = new SKUCustomer(CustomerData);
            
            JsonOutput = Json.serialize(SKUCust);            
        }
        return JsonOutput;
    }
}