/*------------------------------------Fox HEP----------------------------------
This class handles the business logic for inbound REST from Foxipedia for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
public class HEP_TitleReleases_Foxipedia_Inbound implements HEP_IntegrationInterface {
    public HEP_TitleReleases_Foxipedia_Inbound() {

    }
    
    /**
    * @param objWrap - Integration Transaction Response Object
    * @return void 
    */
    public void performTransaction(HEP_InterfaceTxnResponse objWrap) {
        TitleReleasesRequest req = (TitleReleasesRequest) JSON.deserialize(objWrap.sRequest, TitleReleasesRequest.class);

        // Split territories into a set of unique territories
        List<String> terrList = req.territories.split(','); 
        Set<String> terrSet = new Set<String>(); 
        terrSet.addAll(terrList);  

        List<HEP_Promotion_Dating__c> pdList = [SELECT  Id, Date__c, Language__r.Name, Language__r.Code__c, Date_Type__c, Record_Status__c,
                                                        FAD_Approval_Status__c, Locked_Status__c, Language__r.Prophet_Language_Id__c, Channel__c,
                                                        HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, 
                                                        HEP_Catalog__r.Title_EDM__r.FOX_VERSION_ID__c,
                                                        HEP_Catalog__r.Title_EDM__r.Name,
                                                        HEP_Catalog__r.CatalogId__c,
                                                        Territory__r.Territory_Code__c, Territory__r.Send_to_Prophet__c,
                                                        Territory__r.Name, Territory__r.Territory_Code_Integration__c,
                                                        LastModifiedDate

                                                FROM    HEP_Promotion_Dating__c 
                                                WHERE   HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c = :req.finTitleId AND 
                                                        Territory__r.Territory_Code_Integration__c IN :terrSet AND 
                                                        HEP_Catalog__c != NULL AND
                                                        Date__c != NULL AND
                                                        Territory__r.Send_to_Prophet__c = true AND 
                                                        DateType__c LIKE '%Release Date%' 
                                                        AND Customer__c = NULL
                                                        //Status__c = 'Confirmed' AND 
                                                        ];

        List<TitleReleases> trList = new List<TitleReleases>(); 
        for (HEP_Promotion_Dating__c hpd : pdList) {
            TitleReleases tr = new TitleReleases(   hpd.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
                                                    hpd.Id,
                                                    hpd.HEP_Catalog__r.CatalogId__c,
                                                    hpd.Territory__r.Name,
                                                    hpd.Territory__r.Territory_Code_Integration__c, 
                                                    hpd.Channel__c,
                                                    hpd.Language__r.Name,
                                                    hpd.Language__r.Prophet_Language_Id__c,
                                                    hpd.Date__c,
                                                    hpd.HEP_Catalog__r.Title_EDM__r.FOX_VERSION_ID__c,
                                                    hpd.HEP_Catalog__r.Title_EDM__r.Name,
                                                    hpd.Locked_Status__c,
                                                    hpd.Record_Status__c,
                                                    hpd.Language__r.Code__c,
                                                    null,
                                                    hpd.LastModifiedDate);
                                                    
            trList.add(tr); 
        }   

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(trList));
        objWrap.sResponse = JSON.serialize(trList); 
    }

    //private static Map<String,String> channelMap = new Map<String,String> {
    //    'PVOD Release Date'                 => 'PVOD',
    //    'EST Release Date'                  => 'DHD',
    //    'VOD Release Date'                  => 'VOD',
    //    'Physical Release Date (Retail)'    => 'RETAIL',
    //    'Physical Release Date (Kiosk)'     => 'KIOSK',
    //    'Physical Release Date (Rental)'    => 'RENTAL',
    //    'SPEST Release Date'                => 'SPDHD',
    //    'PEST Release Date'                 => 'PDHD',
    //    'SPVOD Release Date'                => 'SPVOD'
    //}; 

    public class TitleReleases {
        public String finTitleId; 
        public String recordId;  
        public String ctlgId; 
        public String territoryName; 
        public String territoryCode; 
        public String channel; 
        public String releaseDate; 
        public String deletedFlag; 
        public Integer versionId; 
        public String active; 
        public String format; 
        public String updateDate; 
        public String titleName; 
        
        public TitleReleases(String finTitleId, Id recordId, String ctlgId, String territoryName, String territoryCode, String channel, String languageName, String languageId,
                            Date releaseDate, String versionId, String titleName, String releaseDateLocked, String active, String languageCode, String format, Datetime updateDate) {
            this.recordId = String.valueOf(recordId); 
            this.finTitleId = finTitleId; 
            this.ctlgId = ctlgId; 
            this.territoryName = territoryName; 
            this.channel = channel; 
            this.releaseDate = String.valueOf(releaseDate);
            this.deletedFlag = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_DELETED').Value__c) ? true : false); 
            this.versionId = Integer.valueOf(versionId); 
            this.territoryCode = territoryCode; 
            this.active = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c) ? true : false);
            this.format = format; 
            this.titleName = titleName;
            this.updateDate = updateDate.format('yyyy-MM-dd\'T\'HH:mm:ss.S\'Z\'');
        }
    }

    public class TitleReleasesRequest {
        public String finTitleId;
        public String territories; 

        public TitleReleasesRequest(String finTitleId, String territories) {
            this.finTitleId = finTitleId; 
            this.territories = territories; 
        }
    }
}