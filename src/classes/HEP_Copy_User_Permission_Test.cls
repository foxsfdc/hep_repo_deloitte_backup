@isTest(seeallData = false)
public class HEP_Copy_User_Permission_Test{
    @testSetup
    static void createUsers(){
        User u1 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Elango', 'elango15@gmail.com','Elango','Elann','E','',true);
        User u2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@gmail.com','Vishnu','Vishnr','V','',true);
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_List_Of_Values__c> lstListOfValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
    }
    static testmethod void testcopy(){
        HEP_Territory__c gGermany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary' , null,null, 'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c gAustria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'AT', '33', null, true);
        HEP_Territory__c gLuxemborg = HEP_Test_Data_Setup_Utility.createHEPTerritory('Luxemborg', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'LU', '265', null, true);               
        User oldUser = [SELECT Id,Name FROM User WHERE LastName = 'Vishnu'];
        User newUser = [SELECT Id,Name FROM User WHERE LastName = 'Elango'];
        String sTransfer = 'Append Source user role to Destination user';
        HEP_Role__c marketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
        HEP_Role__c accountManager = HEP_Test_Data_Setup_Utility.createHEPRole('Account Manager', null, true);
        HEP_Role__c defaultAccess = HEP_Test_Data_Setup_Utility.createHEPRole('Default Access', null, true);
        if(gAustria.id != null &&  marketingManager.id != null && oldUser.id != null)
            HEP_User_Role__c VRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(gAustria.id, marketingManager.id, oldUser.id, true);
        if(gLuxemborg.id !=null && accountManager.id !=null && newuser.id !=null)
            HEP_User_Role__c ARole = HEP_Test_Data_Setup_Utility.createHEPUserRole(gLuxemborg.id, accountManager.id, newuser.id, true);
        //HEP_Test_Data_Setup_Utility.createHEPUserRole();
        test.startTest();
        //if(!Test.isRunningTest)
        HEP_Copy_User_Permission.copyPermissions(oldUser.Id, newUser.Id, sTransfer);
        HEP_Copy_User_Permission.clearUsers(marketingManager.id);
        test.stopTest();
        
        
    }
    static testmethod void testcopyDefault(){
        HEP_Territory__c gGermany = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary' , null,null, 'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c gAustria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'AT', '33', null, true);
        HEP_Territory__c gLuxemborg = HEP_Test_Data_Setup_Utility.createHEPTerritory('Luxemborg', 'EMEA', 'Subsidiary' , gGermany.id,null, 'EUR', 'LU', '265', null, true);               
        User oldUser = [SELECT Id,Name FROM User WHERE LastName = 'Vishnu'];
        User newUser = [SELECT Id,Name FROM User WHERE LastName = 'Elango'];
        String sTransfer = 'Add Default Access';
        HEP_Role__c marketingManager = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
        HEP_Role__c accountManager = HEP_Test_Data_Setup_Utility.createHEPRole('Account Manager', null, true);
        HEP_Role__c defaultAccess = HEP_Test_Data_Setup_Utility.createHEPRole('Default Access', null, true);
        if(gAustria.id != null &&  marketingManager.id != null && oldUser.id != null)
            HEP_User_Role__c VRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(gAustria.id, marketingManager.id, oldUser.id, true);
        if(gLuxemborg.id !=null && accountManager.id !=null && newuser.id !=null)
            HEP_User_Role__c ARole = HEP_Test_Data_Setup_Utility.createHEPUserRole(gLuxemborg.id, accountManager.id, newuser.id, true);
        //HEP_Test_Data_Setup_Utility.createHEPUserRole();
        test.startTest();
        //if(!Test.isRunningTest)
        HEP_Copy_User_Permission.copyPermissions(oldUser.Id, newUser.Id, sTransfer);
        HEP_Copy_User_Permission.clearUsers(marketingManager.id);
        test.stopTest();
        
        
    }
}