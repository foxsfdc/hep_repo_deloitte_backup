/**
 * HEP_Internal_ScheduleReport --- Class to get the data for intl internal schedule reports
 * @author    Ritesh Konduru
 */
public without sharing class HEP_Internal_ScheduleReport { //MP-821

    public static String sACTIVE;
    //public static String sTV = 'TV';
    static {
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    /**
     * intenal Sch wrap --- Wrapper class to hold terriroy, date and multiple records data
     * @author    Ritesh Konduru
     */
    public class HEP_InternalScheduleWrap {

        public String dtStartDate; // HEP_SKU_Master__c Current_Release_Date__c     
        public String dtEndDate; // HEP_SKU_Master__c Current_Release_Date__c
        public List < HEP_Utility.MultiPicklistWrapper > lstTerrioryWrap; //HEP_SKU_Master__c  Territory__c //listTerrioryWrap
        public List < InternalScheduleWrapData > lstInternalScheduleWrapData; //Data //listIntSchlWrapData

    }
    /**
     * Main Wrapper --- Wrapper class to hold one record 
     * @author    Ritesh Konduru
     */
    public class InternalScheduleWrapData {
        public String sSKUMasterId;
        public String sSKUPromotionId;
        public String sWeeksOfRelease; //Release date - today  and displayed in weeks
        public String sTitleName; // SKU_Master__r.SKU_Title__c  
        public String sTitleFormat; // SKU_Master__r.Format__c   
        public String sTitleChannel; // HEP_SKU_Master__c Channel__c    
        public String sTitleGenre; // HEP_Promotion_SKU__c Promotion_Catalog__c Catalog__c id then go to Genre__c
        public String sLicensorType; // HEP_Promotion_SKU__c Promotion_Catalog__c Catalog__c Licensor__c
        public String sReleaseDate; // 
        public String sSKU; // HEP_SKU_Master__c Name   
        public String sBarcode; // HEP_SKU_Master__c Barcode__c
        public String sRegion; //HEP_Territory__c Name    

        //different Fields Internal
        public String sLPG; //HEP_SKU_Master__c id then go to HEP_SKU_Price__c PriceGrade__c  
        public String sMapId; //HEP_Promotion_SKU__c Promotion_Catalog__c  LocalPromotionCode__c
        public String sProductManager; //HEP_Promotion_SKU__c Promotion_Catalog__c Promotion__c Domestic_Marketing_Manager__c  
        public String sTheatricalReleaseDate; //HEP_Promotion_SKU__c Promotion_Catalog__c Promotion__c TitleEDM 
        public String sEarliestAvliableDate; //HEP_Promotion_SKU__c Promotion_Catalog__c Promotion__c FirstAvailableDate__c    
        public String sCatalog; //HEP_Promotion_SKU__c Promotion_Catalog__c Catalog__c Name  


    }
    /**
     * Price Grade Wrapper --- Wrapper class to hold price grade information under sKU
     * @author    Ritesh Konduru
     */
    public class HEPPriceGradeWrap {
        public String sPriceGradeId;
        public String sDelearListPrice;
        public String sTerritoryName;
        public String sPriceGrade;
        public String sMasterSKUId;
        public String sPromotionSKUId;
        public Date sStartDate;
    }
    /*
    Method: loading territory on the initial Load
    return: String
    Author: Ritesh Konduru
    **/
    @RemoteAction
    public static string territoryLoad() {

        List < HEP_Utility.MultiPicklistWrapper > lstTerritoryWrap = new List < HEP_Utility.MultiPicklistWrapper > ();
        HEP_InternalScheduleWrap objWrapData = new HEP_InternalScheduleWrap();
        try {

            for (HEP_territory__c objTerritory: [SELECT id,Name FROM HEP_territory__c WHERE Name != :HEP_Utility.getConstantValue('REGION_HOME_OFFICE') AND Parent_Territory__c = null ORDER BY Name]) {
                lstTerritoryWrap.add(new HEP_Utility.MultiPicklistWrapper(objTerritory.Name, objTerritory.id, false));
            }

            objWrapData.dtStartDate = string.valueof(date.today());
            objWrapData.lstTerrioryWrap = lstTerritoryWrap;

            String jsonData = JSON.serialize(objWrapData);
            System.debug('jsonData++' + jsonData);
            return jsonData;
        } catch (Exception e) {
            system.debug('Exception Line Number ' + e.getLineNumber() + ' Exception ' + e);
            throw e;
        }

    }
    /*
    Method: Loads reports data according to the parameters values.
    return: String
    Author: Ritesh Konduru
    **/
    @RemoteAction
    public static string initialLoad(String sStartDate, String sEndDate, List < HEP_Utility.MultiPicklistWrapper > lstSelectedTerritory) { //Need to get paramater from ui and add if contition

        Date dtStartDate;
        Date dtEndDate;
        list < String > lstSelectedTerritories = new list < String > ();
        HEP_InternalScheduleWrap objWrapData = new HEP_InternalScheduleWrap();
        List < InternalScheduleWrapData > lstReleaseSchWrap = new List < InternalScheduleWrapData > ();
        List < InternalScheduleWrapData > lstReleaseSchWraptoUI = new List < InternalScheduleWrapData > ();
        set < String > setTempIdsCatalog = new set < String > ();
        set < String > setTempIdsPriceGrade = new set < String > ();
        set < String > setTempWPRIdsTV = new set < String > ();
        set < String > setTempWPRIdsCatalogAndNewRelease = new set < String > ();
        Map < String, String > mapGenreCatalogSKURec = new Map < String, String > ();
        Map < String, String > mapWPR = new Map < String, String > ();
        List < HEPPriceGradeWrap > lstPGCatalogRec = new List < HEPPriceGradeWrap > ();
        set < Id > setPriceGradePromotionIds = new set < id > ();
        try {

            if (String.isNotBlank(sStartDate)) {
                dtStartDate = date.valueOf(sStartDate);
            }
            if (String.isNotBlank(sEndDate)) { //sEndDate != null &&
                dtEndDate = date.valueOf(sEndDate);
            }

            System.debug('dtStartDate ' + dtStartDate + 'dtEndDate ' + dtEndDate);
            if (lstSelectedTerritory != null && lstSelectedTerritory.size() > 0) {
                for (HEP_Utility.MultiPicklistWrapper tempTerr: lstSelectedTerritory) {
                    if (tempTerr.bSelected == true)
                        lstSelectedTerritories.add(tempTerr.sKey);

                }
            }
            System.debug('lstSelectedTerritories ' + lstSelectedTerritories);

            //pulling records from Promotion master with start, end date  and territory filter from UI
            List < HEP_Promotion_SKU__c > lstPromotionSKURec = [select SKU_Master__r.Format__c,
                SKU_Master__r.Channel__c,
                Promotion_Catalog__r.Catalog__r.id,
                Promotion_Catalog__r.Catalog__r.Licensor_Type__c,
                SKU_Master__r.Current_Release_Date__c,
                SKU_Master__r.SKU_Title__c,
                SKU_Master__r.SKU_Number__c,
                SKU_Master__r.Barcode__c,
                SKU_Master__r.Territory__r.Name,
                SKU_Master__r.id,
                SKU_Master__r.Catalog_Master__r.CatalogId__c,
                Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Name,
                Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
                Promotion_Catalog__r.Promotion__r.LineOfBusiness__r.Type__c,
                Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c,
                Promotion_Catalog__r.Catalog__r.Name
                from HEP_Promotion_SKU__c where SKU_Master__r.Territory__c IN: lstSelectedTerritories and SKU_Master__r.Current_Release_Date__c >=: dtStartDate and SKU_Master__r.Current_Release_Date__c <=: dtEndDate and Record_Status__c =: sACTIVE
            ];


            //creating a List with catalog id's inorder to make a query and get genres.                                                     
            for (HEP_Promotion_SKU__c tempforGenre: lstPromotionSKURec) {
                if (tempforGenre.Promotion_Catalog__r.Catalog__r != null && tempforGenre.Promotion_Catalog__r != null) {
                    //List for Genres
                    setTempIdsCatalog.add(tempforGenre.Promotion_Catalog__r.Catalog__r.id);
                    //if else condition for seperating wpr ids bassed on LOB's
                    if (tempforGenre.Promotion_Catalog__r.Promotion__r.LineOfBusiness__r != null && HEP_Constants__c.getValues('PROMOTION_LOB_TV').Value__c.equals(tempforGenre.Promotion_Catalog__r.Promotion__r.LineOfBusiness__r.Type__c)) {
                        if (String.isNotBlank(tempforGenre.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c)) {
                            setTempWPRIdsTV.add(tempforGenre.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c);
                        }

                    } else {
                        if (String.isNotBlank(tempforGenre.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c)) {
                            setTempWPRIdsCatalogAndNewRelease.add(tempforGenre.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c);
                        }
                    }
                } //list for PriceGrade
                // if (tempforGenre.SKU_Master__r != null) {
                //setTempIdsPriceGrade.add(tempforGenre.SKU_Master__r.id);
                setTempIdsPriceGrade.add(tempforGenre.id);
                //}
            }
            //System.debug('setTempWPRIdsTV ' + setTempWPRIdsTV);
            //System.debug('setTempWPRIdsCatalogAndNewRelease ' + setTempWPRIdsCatalogAndNewRelease);

            //Comparing for TV LOB. quering release dates which are maped to  wpr ids
            List < TVD_Release_Dates__c > lstTVReleaseDatesRec = [select id, Title_EDM__r.FIN_PROD_ID__c, TVD_Release_Date__c from TVD_Release_Dates__c where Title_EDM__r.FIN_PROD_ID__c IN: setTempWPRIdsTV];
            //for catalog and new release. quering release dates which are maped to wpr ids
            List < GTS_Title__c > lstCReleaseDatesRec = [select id, WPR__c, US_Release_Date__c from GTS_Title__c where WPR__c IN: setTempWPRIdsCatalogAndNewRelease];

            //got release dates related to WPRid's now iterating it to add it into a map.
            for (TVD_Release_Dates__c tempTV: lstTVReleaseDatesRec) {
                if (tempTV.Title_EDM__r.FIN_PROD_ID__c != null)
                    mapWPR.put(tempTV.Title_EDM__r.FIN_PROD_ID__c, string.valueof(tempTV.TVD_Release_Date__c));
            }
            for (GTS_Title__c tempGTS: lstCReleaseDatesRec) {

                if (tempGTS.WPR__c != null)
                    mapWPR.put(tempGTS.WPR__c, string.valueof(tempGTS.US_Release_Date__c));
            }

            System.debug('mapWPR Map ' + mapWPR);

            // iterating through genres records and getting genre info
            List < HEP_Catalog__c > lstCatalogRec = [select id, (select id, Genre__c From Genres__r) from HEP_Catalog__c where Id IN: setTempIdsCatalog];

            for (HEP_Catalog__c objCatalog: lstCatalogRec) {
                String sGenre = '';
                for (HEP_Catalog_Genre__c objGenre: objCatalog.Genres__r) {
                    if (objGenre.Genre__c != null) {
                        sGenre = sGenre + objGenre.Genre__c + ',';
                    }

                }
                sGenre = sGenre.trim().removeEnd(',');
                mapGenreCatalogSKURec.put(objCatalog.id, sGenre);

            }


            System.debug('mapGenreCatalogSKURec ' + mapGenreCatalogSKURec);

            //creating map for pricegrade object
            //List < HEP_SKU_Price__c > lstPriceGradeRec = [select id, PriceGrade__r.Dealer_List_Price__c, SKU_Master__c, Start_Date__c, Region__r.Name, PriceGrade__r.PriceGrade__c from HEP_SKU_Price__c where SKU_Master__c IN: setTempIdsPriceGrade];
            List < HEP_SKU_Price__c > lstPriceGradeRec = [select id, PriceGrade__r.Dealer_List_Price__c, SKU_Master__c, Promotion_SKU__c, Start_Date__c, Region__r.Name, PriceGrade__r.PriceGrade__c from HEP_SKU_Price__c where Promotion_SKU__c IN: setTempIdsPriceGrade];

            for (HEP_SKU_Price__c objPG: lstPriceGradeRec) {

                HEPPriceGradeWrap objPriceGrade = new HEPPriceGradeWrap();

                objPriceGrade.sPriceGradeId = objPG.id;
                objPriceGrade.sTerritoryName = objPG.Region__r.Name;
                if (String.isNotBlank(objPG.PriceGrade__r.Dealer_List_Price__c + ''))
                    objPriceGrade.sDelearListPrice = string.valueof(objPG.PriceGrade__r.Dealer_List_Price__c);
                objPriceGrade.sPriceGrade = objPG.PriceGrade__r.PriceGrade__c;
                objPriceGrade.sMasterSKUId = objPG.SKU_Master__c;
                objPriceGrade.sPromotionSKUId = objPG.Promotion_SKU__c;
                objPriceGrade.sStartDate = objPG.Start_Date__c;
                lstPGCatalogRec.add(objPriceGrade);

            }
            System.debug('lstPGCatalogRec ' + lstPGCatalogRec);

            //for theritical id 

            for (HEP_Promotion_SKU__c hepRec: lstPromotionSKURec) {
                InternalScheduleWrapData wrapGridData = new InternalScheduleWrapData();
                wrapGridData.sSKUMasterId = hepRec.SKU_Master__r.id;
                wrapGridData.sSKUPromotionId = hepRec.id;
                wrapGridData.sTitleName = hepRec.SKU_Master__r.SKU_Title__c;
                wrapGridData.sTitleFormat = hepRec.SKU_Master__r.Format__c;
                wrapGridData.sTitleChannel = hepRec.SKU_Master__r.Channel__c;
                if (mapGenreCatalogSKURec.containsKey(hepRec.Promotion_Catalog__r.Catalog__r.id)) {
                    wrapGridData.sTitleGenre = mapGenreCatalogSKURec.get(hepRec.Promotion_Catalog__r.Catalog__r.id); //Pending
                    //System.debug('wrapGridData.sTitleGenre ' + wrapGridData.sTitleGenre);
                }

                wrapGridData.sLicensorType = hepRec.Promotion_Catalog__r.Catalog__r.Licensor_Type__c;
                wrapGridData.sSKU = hepRec.SKU_Master__r.SKU_Number__c;
                wrapGridData.sBarcode = hepRec.SKU_Master__r.Barcode__c;
                wrapGridData.sRegion = hepRec.SKU_Master__r.Territory__r.Name;
                wrapGridData.sMapId = hepRec.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c;
                wrapGridData.sProductManager = hepRec.Promotion_Catalog__r.Promotion__r.Domestic_Marketing_Manager__r.Name;
                if (mapWPR.containsKey(hepRec.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c)) {
                    wrapGridData.sTheatricalReleaseDate = mapWPR.get(hepRec.Promotion_Catalog__r.Catalog__r.Title_EDM__r.FIN_PROD_ID__c); //Pending
                }
                wrapGridData.sEarliestAvliableDate = string.valueof(hepRec.Promotion_Catalog__r.Promotion__r.FirstAvailableDate__c); //Convert it to date
                wrapGridData.sCatalog = hepRec.SKU_Master__r.Catalog_Master__r.CatalogId__c;
                lstReleaseSchWrap.add(wrapGridData);

            }
            System.debug('lstReleaseSchWrap ' + lstReleaseSchWrap);
            //Adding multiple price grades for multiple master sku's
            if (lstPGCatalogRec.size() > 0 && lstReleaseSchWrap != null && lstReleaseSchWrap.size() > 0) {
                for (HEPPriceGradeWrap objWrapPG: lstPGCatalogRec) {
                    for (InternalScheduleWrapData objwrapDataTemp: lstReleaseSchWrap) {
                        InternalScheduleWrapData tempIntSchlData = new InternalScheduleWrapData(); //= objwrapDataTemp;
                        //if (objWrapPG != null && objwrapDataTemp != null && (objWrapPG.sMasterSKUId).equals(objwrapDataTemp.sSKUMasterId)) {
                        if (objWrapPG != null && objwrapDataTemp != null && (objWrapPG.sPromotionSKUId).equals(objwrapDataTemp.sSKUPromotionId)) {

                            tempIntSchlData.sLPG = objWrapPG.sPriceGrade; //Pending
                            if (objWrapPG.sStartDate != null) {

                                tempIntSchlData.sReleaseDate = string.valueof(objWrapPG.sStartDate); //Pending
                                Integer weeks = date.today().daysBetween(objWrapPG.sStartDate);
                                tempIntSchlData.sWeeksOfRelease = (weeks / 7) + ''; //Pending -today

                            }
                            tempIntSchlData.sSKUMasterId = objwrapDataTemp.sSKUMasterId;
                            tempIntSchlData.sSKUPromotionId = objwrapDataTemp.sSKUPromotionId;
                            tempIntSchlData.sTitleName = objwrapDataTemp.sTitleName;
                            tempIntSchlData.sTitleFormat = objwrapDataTemp.sTitleFormat;
                            tempIntSchlData.sTitleChannel = objwrapDataTemp.sTitleChannel;
                            tempIntSchlData.sTitleGenre = objwrapDataTemp.sTitleGenre;
                            tempIntSchlData.sLicensorType = objwrapDataTemp.sLicensorType;
                            tempIntSchlData.sSKU = objwrapDataTemp.sSKU;
                            tempIntSchlData.sBarcode = objwrapDataTemp.sBarcode;
                            tempIntSchlData.sRegion = objwrapDataTemp.sRegion;
                            tempIntSchlData.sMapId = objwrapDataTemp.sMapId;
                            tempIntSchlData.sProductManager = objwrapDataTemp.sProductManager;
                            tempIntSchlData.sTheatricalReleaseDate = objwrapDataTemp.sTheatricalReleaseDate;
                            tempIntSchlData.sEarliestAvliableDate = objwrapDataTemp.sEarliestAvliableDate;
                            tempIntSchlData.sCatalog = objwrapDataTemp.sCatalog;
                            lstReleaseSchWraptoUI.add(tempIntSchlData);
                        }
                    } //for end  lstReleaseSchWrap
                } //For End  lstPGCatalogRec

                //Adding sKU's which dosent have Price Grade Records.we should not use NotNull/Break contition in above loops because there may be multiple pricegrades
                for (HEPPriceGradeWrap objWrapPG1: lstPGCatalogRec) {
                    Id tempIDPromotionSKUId = objWrapPG1.sPromotionSKUId;
                    setPriceGradePromotionIds.add(tempIDPromotionSKUId);
                }
                System.debug('setPriceGradePromotionIds ' + setPriceGradePromotionIds);
                for (InternalScheduleWrapData objwrapDataTemp1: lstReleaseSchWrap) {
                    Id tempIDSKUPromotionId = objwrapDataTemp1.sSKUPromotionId;

                    if (!setPriceGradePromotionIds.contains(tempIDSKUPromotionId)) {
                        lstReleaseSchWraptoUI.add(objwrapDataTemp1);

                    }
                }
            } //size end
            objWrapData.lstInternalScheduleWrapData = lstReleaseSchWraptoUI;

            String jsonData = JSON.serialize(objWrapData);
            System.debug('jsonData++' + jsonData);
            return jsonData;

        } catch (Exception e) {
            system.debug('Exception Line Number ' + e.getLineNumber() + ' Exception ' + e);
            throw e;
        }
    }
}