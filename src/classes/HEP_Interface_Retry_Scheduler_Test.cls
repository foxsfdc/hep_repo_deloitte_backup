/**
* HEP_Interface_Retry_Scheduler_Test -- Test class for the Interface Retry Scheduler in HEP
* @author    Deloitte
*/
@isTest 
private class HEP_Interface_Retry_Scheduler_Test{

 @isTest 
    static void testBatchScheduler() {
        String cronExpr = '0 0 0 15 3 ? 2022';
            Test.startTest();
                HEP_Interface_Retry_Scheduler objScheduler= new HEP_Interface_Retry_Scheduler();
                String jobId = System.schedule('TestJob', cronExpr, objScheduler);
            Test.stopTest();
        }
    }