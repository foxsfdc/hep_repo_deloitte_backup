@isTest(SEEALLDATA = false)
private class HEP_Synopsis_TriggerHandler_Test {
	@TestSetup
	static void testSetupMethod(){
		list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
		//Create Custom Setting records for EDM...
		EDM_Services__c	objSynopsisService = new EDM_Services__c();
		objSynopsisService.Name = 'Synopsis';
		objSynopsisService.Client_Id__c = 'SalesForce';
		objSynopsisService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
		objSynopsisService.Service_URL__c = '/v1/foxipedia/global/api/title/synopsis';
		objSynopsisService.Sandbox_URL__c = 'https://ms-devapi.foxinc.com';

		EDM_Services__c	objSynopsisTitle = new EDM_Services__c();
		objSynopsisTitle.Name = 'TitleKeyInformation';
		objSynopsisTitle.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
		objSynopsisTitle.Service_URL__c = '/v1/foxipedia/global/api/title/synopsis';
		objSynopsisTitle.Sandbox_URL__c = 'https://ms-devapi.foxinc.com';

		EDM_Services__c objCachedOAuthToken = new EDM_Services__c();
		objCachedOAuthToken.Name = 'CachedOAuthToken';
		objCachedOAuthToken.CachedAuthorizationToken__c = 'QAoMYT89u4t3kjHwPgk8HqWUewE9';

		List<EDM_Services__c> lstEDMServies = new List<EDM_Services__c>{objSynopsisService , objSynopsisTitle , objCachedOAuthToken};
		insert lstEDMServies;


		//Code to create Territory Record
		HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        insert objTerritory;

        //Code to create Catalog Data.
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('12334','test Catalog','Single',null,objTerritory.Id,'Approved','Master',true);

        //Create HEP Interface Records..
        HEP_Interface__c objSynopsiInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Synopsis','HEP_Synopsis',true,10,3,'Outbound-Pull',false,true,true);
	}
	@isTest static void test_method_one() {
		Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
		
		//fetch the inserted Catalog Data.
		List<HEP_Catalog__c> lstCatalogs = [SELECT Id , Name FROM HEP_Catalog__c];

		//Code to create Synopsis Record.
        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTLong = new HEP_Foxipedia_Synopsis_Publish__c();
        objSynopsisESTLong.Name = 'Test Synopsis';
        objSynopsisESTLong.HEP_Catalog__c = lstCatalogs[0].Id;
        objSynopsisESTLong.FOX_ID__c = 2029858;
        objSynopsisESTLong.WPR_ID__c = '127814';
        objSynopsisESTLong.Fox_Version_ID__c = 1482429; 
        objSynopsisESTLong.Version_Type__c = 'DFLT';
        objSynopsisESTLong.Catalog_Number__c = 12344;
        objSynopsisESTLong.Country_Code__c = 'US';
        objSynopsisESTLong.Language_Code__c = 'ENG';
        objSynopsisESTLong.Media_Code__c = 'EST';
        objSynopsisESTLong.Synopsis_Type__c = 'longText';
        objSynopsisESTLong.Synopsis_Text__c = 'Test foxepedia';
        //insert objSynopsis;

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTLongPreOrderText = objSynopsisESTLong.clone();
        objSynopsisESTLongPreOrderText.Synopsis_Type__c = 'LongPreOrderText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTShortText = objSynopsisESTLong.clone();
        objSynopsisESTShortText.Synopsis_Type__c = 'ShortText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTShortPreOrderText = objSynopsisESTLong.clone();
        objSynopsisESTShortPreOrderText.Synopsis_Type__c = 'ShortPreOrderText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisVIDlongText = objSynopsisESTLong.clone();
        objSynopsisVIDlongText.Media_Code__c = 'VID';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisVIDLongPreOrderText = objSynopsisESTLong.clone();
        objSynopsisVIDLongPreOrderText.Media_Code__c = 'VID';
        objSynopsisVIDLongPreOrderText.Synopsis_Type__c = 'LongPreOrderText';

        List<HEP_Foxipedia_Synopsis_Publish__c> lstSynopsis = new List<HEP_Foxipedia_Synopsis_Publish__c>{objSynopsisESTLong,
													  //objSynopsisESTLongPreOrderText,
													  objSynopsisESTShortText,
													  //objSynopsisESTShortPreOrderText,
													  objSynopsisVIDlongText//,
													  //objSynopsisVIDLongPreOrderText
													};

		insert lstSynopsis;
		update lstSynopsis;
	}
	
	@isTest static void test_method_two() {
		Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
		
		//fetch the inserted Catalog Data.
		List<HEP_Catalog__c> lstCatalogs = [SELECT Id , Name FROM HEP_Catalog__c];

		//Code to create Synopsis Record.
        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTLong = new HEP_Foxipedia_Synopsis_Publish__c();
        objSynopsisESTLong.Name = 'Test Synopsis';
        objSynopsisESTLong.HEP_Catalog__c = lstCatalogs[0].Id;
        objSynopsisESTLong.FOX_ID__c = 2029858;
        objSynopsisESTLong.WPR_ID__c = '127814';
        objSynopsisESTLong.Fox_Version_ID__c = 1482429; 
        objSynopsisESTLong.Version_Type__c = 'DFLT';
        objSynopsisESTLong.Catalog_Number__c = 12344;
        objSynopsisESTLong.Country_Code__c = 'US';
        objSynopsisESTLong.Language_Code__c = 'ENG';
        objSynopsisESTLong.Media_Code__c = 'EST';
        objSynopsisESTLong.Synopsis_Type__c = 'longText';
        objSynopsisESTLong.Synopsis_Text__c = 'Test foxepedia';
        //insert objSynopsis;

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTLongPreOrderText = objSynopsisESTLong.clone();
        objSynopsisESTLongPreOrderText.Synopsis_Type__c = 'LongPreOrderText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTShortText = objSynopsisESTLong.clone();
        objSynopsisESTShortText.Synopsis_Type__c = 'ShortText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisESTShortPreOrderText = objSynopsisESTLong.clone();
        objSynopsisESTShortPreOrderText.Synopsis_Type__c = 'ShortPreOrderText';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisVIDlongText = objSynopsisESTLong.clone();
        objSynopsisVIDlongText.Media_Code__c = 'VID';

        HEP_Foxipedia_Synopsis_Publish__c objSynopsisVIDLongPreOrderText = objSynopsisESTLong.clone();
        objSynopsisVIDLongPreOrderText.Media_Code__c = 'VID';
        objSynopsisVIDLongPreOrderText.Synopsis_Type__c = 'LongPreOrderText';

        List<HEP_Foxipedia_Synopsis_Publish__c> lstSynopsis = new List<HEP_Foxipedia_Synopsis_Publish__c>{//objSynopsisESTLong,
													  objSynopsisESTLongPreOrderText,
													  //objSynopsisESTShortText,
													  objSynopsisESTShortPreOrderText,
													  //objSynopsisVIDlongText,
													  objSynopsisVIDLongPreOrderText
													};

		insert lstSynopsis;
		update lstSynopsis;
	}
	
}