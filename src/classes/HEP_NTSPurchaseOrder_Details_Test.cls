@isTest
public class HEP_NTSPurchaseOrder_Details_Test {

     @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    
    @isTest 
    static void Success_Test(){           
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_INT_NTSPurchaseOrder_Details','HEP_NTSPurchaseOrder_Details',true,10,10,'Outbound-Pull',true,true,true);
        HEP_NTSPurchaseOrder_Details.HEP_NTSInvoiceWrapper  objWrapper = new HEP_NTSPurchaseOrder_Details.HEP_NTSInvoiceWrapper();
        objWrapper.sCatalog = '83298';
        objWrapper.sTerritory = '301';
        objWrapper.sFrom_Date = '01%2F01%2F90';
        objWrapper.sTo_Date = '07%2F11%2F20';
        String sPurchaseJson = JSON.serialize(objWrapper);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sPurchaseJson,'','HEP_INT_NTSPurchaseOrder_Details');
        System.assertEquals('HEP_INT_NTSPurchaseOrder_Details',objTxnResponse.sInterfaceName);
        Test.stoptest();   
    } 
    
  /*  @isTest 
    static void Failure_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTerritoryE1Code":"US","sCatalogNumber":"12345","sFromDate":"2017-02-02","sToDate":"2017-04-06"}';
        objTxnResponse.sStatus='OK';
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTSPurchaseOrder_Details demo = new HEP_NTSPurchaseOrder_Details();
        System.debug('RESPONSE BEFOREE1 -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR1 -- '+objTxnResponse);
        Test.stoptest();   
    }  */
   
    @isTest 
    static void Access_Token_Failure_Test(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_NTS_E1_POOAUTH']; 
        delete objConstant;      
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTerritoryE1Code":"US","sCatalogNumber":"12345","sFromDate":"2017-02-03","sToDate":"2017-04-06"}';
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTSPurchaseOrder_Details demo = new HEP_NTSPurchaseOrder_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }                  
}