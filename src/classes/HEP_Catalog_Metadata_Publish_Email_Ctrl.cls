public class HEP_Catalog_Metadata_Publish_Email_Ctrl{  
    public Id recID {get; set;} 
    public String sTitleName {get; set;}
    public List<EmailWrapper> lstEmailWrapper{
       get{
           if(lstEmailWrapper == null){
                fetchDetails();
            }
            return lstEmailWrapper;
        }
        set;
    }
    

    public void fetchDetails(){    

                
       try{
            String sPromotionId; 
            this.lstEmailWrapper = new List<EmailWrapper>();   
            EmailWrapper objMail; 
            if(recID != null){ 
               List<HEP_Catalog_MetaData__c> lstTemp = [select 
                                                        Catalog__c,
                                                        Catalog__r.Title_EDM__r.Name, 
                                                        Catalog__r.Title_EDM__r.FIN_PROD_ID__c,

                                                        Digital_Short_Synopsis__c,
                                                        Digital_Long_Synopsis__c,
                                                        Digital_HD_Short_Synopsis__c,
                                                        Digital_HD_Long_Synopsis__c,
                                                        Digital_VAM_Description__c,

                                                        Physical_Long_Synopsis__c,
                                                        Physical_Short_Synopsis__c,
                                                        Physical_VAM_Description__c,

                                                        LastModifiedBy.Name,
                                                        Last_Published_By__r.Name,
                                                        Last_Saved_On__c,
                                                        Last_Published_On__c,

                                                        Publish_Comments__c,
                                                        Unique_Id__c

                                                        from HEP_Catalog_MetaData__c where id =:recID];

                if(lstTemp != null && !lstTemp.isEmpty()){
                    for(HEP_Catalog_MetaData__c objCatMetadata : lstTemp){
                        objMail = new EmailWrapper();
                        if(objCatMetadata.Catalog__r != null && objCatMetadata.Catalog__r.Title_EDM__r != null){
                            objMail.sFTID = objCatMetadata.Catalog__r.Title_EDM__r.FIN_PROD_ID__c;
                            objMail.sTitleName = objCatMetadata.Catalog__r.Title_EDM__r.Name;
                            }  

                        objMail.sDigitalShortPreOrder = objCatMetadata.Digital_Short_Synopsis__c;
                        objMail.sDigitalLongPreOrder = objCatMetadata.Digital_Long_Synopsis__c;
                        objMail.sDigitalLongSynosis = objCatMetadata.Digital_HD_Long_Synopsis__c;
                        objMail.sDigitalShortSynosis = objCatMetadata.Digital_HD_Short_Synopsis__c; 
                        objMail.sDigitalVam = objCatMetadata.Digital_VAM_Description__c;


                        objMail.sPreOrder = objCatMetadata.Physical_Short_Synopsis__c; 
                        objMail.sLongSynosis = objCatMetadata.Physical_Long_Synopsis__c;
                        objMail.sVam = objCatMetadata.Physical_VAM_Description__c;
                        
                        if (objCatMetadata.LastModifiedBy != null)
                            objMail.sLastUpdatedBy = objCatMetadata.LastModifiedBy.Name;
                        
                        if (objCatMetadata.Last_Published_By__c != null)
                            objMail.sLastPublishedBy = objCatMetadata.Last_Published_By__r.Name; 
                        
                        objMail.sLastUpdatedOn = objCatMetadata.Last_Saved_On__c + '';
                        objMail.sLastPublishedOn = objCatMetadata.Last_Published_On__c + '';

                        objMail.sPublishComment = objCatMetadata.Publish_Comments__c;
                        if (objCatMetadata.Last_Published_By__c != null)
                         objMail.sMarketingManager = objCatMetadata.Last_Published_By__r.Name;
                        
                        if(objCatMetadata.Unique_Id__c != null ){
                            sPromotionId = objCatMetadata.Unique_Id__c.subStringAfter('-');
                            objMail.sSFPromotionId =URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_Promotion_Details?promoId=' + sPromotionId;
                        }
                        lstEmailWrapper.add(objMail); 
                    }   
                }
            }
            }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        } 
    }
    public class EmailWrapper{
        public String sFTID {get; set;}
        public String sTitleName {get; set;}

        public String sDigitalShortPreOrder{get; set;}
        public String sDigitalLongPreOrder {get; set;}      
        public String sDigitalLongSynosis {get; set;}
        public String sDigitalShortSynosis {get; set;}      
        public String sDigitalVam {get; set;}

        public String sPreOrder {get; set;}      
        public String sLongSynosis {get; set;}      
        public String sVam {get; set;}

        public String sLastUpdatedBy {get; set;}      
        public String sLastPublishedBy {get; set;}      
        public String sLastUpdatedOn {get; set;}
        public String sLastPublishedOn {get; set;}

        public String sPublishComment {get; set;}
        public String sMarketingManager {get; set;}
        
        public String sSFPromotionId {get; set;}
            
    }
}