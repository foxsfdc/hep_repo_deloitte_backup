/**
* HEP_INT_DheSkuPrice_Details_Test 
* @author    Vishnu Raghunath
*/ 

@isTest (seeAllData = false)
public class HEP_INT_DheSkuPrice_Details_Test {
    @testSetup 
    static void setupSKUs(){
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', NULL, NULL, 
                                        'EUR', 'DE', '182', 'ESCO', true);
        HEP_Territory__c objChildTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Canada', 'EMEA', 'Subsidiary', 
                                            objTerritory.Id, objTerritory.Id, 'EUR', 'BE', '49', NULL, true);
        HEP_Territory__c objChildTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', 
                                            objTerritory.Id, objTerritory.Id, 'USD', 'US', '509', NULL, true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'NID', 'nvk@deloitte.com', 'NID', 'NID', 'NID', '', false);
        insert objUser;

        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;

        HEP_Role__c objRole1 = HEP_Test_Data_Setup_Utility.createHEPRole('CAN Marketing Manager', '', false);
        objRole1.Destination_User__c = objUser.Id;
        objRole1.Source_User__c = objUser.Id;
        insert objRole1;
        
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Acquisitions', 'TV', true);
        
        
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objChildTerritory.Id, '$19.99-$15.99', 15.99, false);
        objPriceGrade.Channel__c = 'RENTAL';
        objPriceGrade.Format__c = 'DVD';
        objPriceGrade.Type__c = 'MASH';
        objPriceGrade.MM_RateCard_ID__c = '4';
        insert objPriceGrade;
        
        HEP_Price_Grade__c objPriceGrade1 = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objChildTerritory1.Id, '$19.99-$25.99', 25.99, false);
        objPriceGrade1.Channel__c = 'RENTAL';
        objPriceGrade1.Format__c = 'DVD';
        objPriceGrade1.Type__c = 'MASH';
        objPriceGrade1.MM_RateCard_ID__c = '4';
        insert objPriceGrade1;
        
        HEP_Price_Grade__c objPriceGrade2 = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objChildTerritory.Id, '$29.99-$15.99', 15.99, false);
        objPriceGrade2.Channel__c = 'RENTAL';
        objPriceGrade2.Format__c = 'DVD';
        objPriceGrade2.Type__c = 'MASH';
        objPriceGrade2.MM_RateCard_ID__c = '4';
        insert objPriceGrade2;
        
        HEP_Price_Grade__c objPriceGrade3 = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objChildTerritory1.Id, '$29.99-$25.99', 25.99, false);
        objPriceGrade3.Channel__c = 'RENTAL';
        objPriceGrade3.Format__c = 'DVD';
        objPriceGrade3.Type__c = 'MASH';
        objPriceGrade3.MM_RateCard_ID__c = '4';
        insert objPriceGrade3;
        
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo', 'Global', NULL, 
                                        objTerritory.Id, objLOB.Id, NULL, NULL, false);
        
        objPromotion.LocalPromotionCode__c = '88055';
        insert objPromotion;
        HEP_Promotion__c objPromotion1 = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo1', 'Global', NULL, 
                                        objTerritory.Id, objLOB.Id, NULL, NULL, false);
        
        objPromotion1.LocalPromotionCode__c = '88056';
        insert objPromotion1;
        
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, 
                                                    objChildTerritory.Id, true);
        HEP_Promotion_Region__c objPromotionRegion1 = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion1.Id, 
                                                    objChildTerritory1.Id, true);
        HEP_Promotions_DatingMatrix__c objHEPPromotionsDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix(
                                                                        'TV', 'Promotion', 'Physical Release Date (Rental)', 
                                                                        objTerritory.Id, objChildTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objHEPPromotionsDatingMatrix1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix(
                                                                        'TV', 'Promotion', 'Physical Release Date (Rental)', 
                                                                        objTerritory.Id, objChildTerritory1.Id, true);
                                                                    
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id,
                                                    objHEPPromotionsDatingMatrix.Id, false);
        objDatingRecord.FAD_Approval_Status__c = 'Approved';
        insert objDatingRecord;
        HEP_Promotion_Dating__c objDatingRecord1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion1.Id,
                                                    objHEPPromotionsDatingMatrix1.Id, false);
        objDatingRecord1.FAD_Approval_Status__c = 'Approved';
        
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('01234', 'SAMPLE', 'Box Set', NULL, 
                                    objTerritory.Id, 'Approved', 'Master', false);
        objCatalog.Licensor__c = 'FOX';
        insert objCatalog;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, 
                                                    objPromotion.Id, NULL, true);
        HEP_Promotion_Catalog__c objPromotionCatalog1 = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, 
                                                    objPromotion1.Id, NULL, true);
        
        HEP_Customer__c objHEPCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon', objTerritory.Id, 'CUSTOMER123', 
                                        'SKU Customers', NULL, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '2357753', 'Sample SKU', 
                                        'Master', NULL, 'RENTAL', 'DVD', false);
        objSKU.JDE_Wholesale_Price_CAN__c = 15.99;
        objSKU.JDE_Wholesale_Price_US__c = 25.99;
        insert objSKU;
        
        HEP_SKU_Master__c objSKU1 = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, '2357751', 'Sample SKU', 
                                        'Master', NULL, 'RENTAL', 'DVD', false);
        objSKU1.JDE_Wholesale_Price_CAN__c = 15.99;
        objSKU1.JDE_Wholesale_Price_US__c = 25.99;
        insert objSKU1;
        
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKU.Id, 
                                        'Approved', 'Unlocked', true);
        HEP_Promotion_SKU__c objPromoSKU1 = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog1.Id, objSKU1.Id, 
                                        'Approved', 'Unlocked', true);
        //HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objHEPCustomer.Id, objSKU.Id, true);
        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Catalog_Components','HEP_Catalog_Components_Extractor',
        								false,30,1,'Outbound-Pull', true, true, true);
    }
    @isTest
    static void callSKUInteg(){
        
        HEP_INT_DheSkuInvoiceWrapperUtility obj=new HEP_INT_DheSkuInvoiceWrapperUtility();
        obj.sTimeStamp= '120000';
        obj.sLastPulledDate= '20180619';
        obj.sNoOfRows= '1000';
        obj.sStartingFrom= '0';
        HEP_INT_DheSkuPrice_Details exe = new HEP_INT_DheSkuPrice_Details();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = JSON.serialize(obj);
        
        HEP_INT_DheSkuInvoiceWrapperUtility obj1 = new HEP_INT_DheSkuInvoiceWrapperUtility();
        obj1.sTimeStamp= '120001';
        obj1.sLastPulledDate= '20180619';
        obj1.sNoOfRows= '1000';
        obj1.sStartingFrom= '0';
        //HEP_INT_DheSkuPrice_Details exe = new HEP_INT_DheSkuPrice_Details();
        HEP_InterfaceTxnResponse objTxnResponse1 = new HEP_InterfaceTxnResponse();
        objTxnResponse1.sSourceId = JSON.serialize(obj1);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        exe.performTransaction(objTxnResponse);
        exe.performTransaction(objTxnResponse1);
        Test.stopTest();
        
    }
}