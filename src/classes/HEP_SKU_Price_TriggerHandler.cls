/**
 *HEP_SKU_Price_TriggerHandler --- SKU Price Object trigger Handler
 *@author  Himanshi Mayal
 */
public class HEP_SKU_Price_TriggerHandler extends TriggerHandler{
    /**
    * afterUpdate --- all after update logic
    * @param 
    * @exception Any exception
    * @return void
    */
    public override void afterUpdate() {
        if(Trigger.oldMap != Null)
        {
            System.debug('oldMap>> '+ Trigger.oldMap);
            callQueueableForE1((Map<Id, HEP_SKU_Price__c>) Trigger.oldMap);         
            System.debug('newMap>>' + Trigger.newMap);
            Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_SKU_Price__c', 'SKU_Price_Field_Set');
            System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
            if(checkFieldUpdate)
                callQueueableMethod((Map<Id, HEP_SKU_Price__c>) Trigger.newMap);            
        }
    }
    /**
    * afterInsert --- all after update logic
    * @param
    * @exception Any exception
    * @return void
    */
    public override void afterInsert() {
        System.debug('oldMap>>' + Trigger.oldMap);
        System.debug('newMap>>' + Trigger.newMap);
        callQueueableMethod((Map<Id, HEP_SKU_Price__c>) Trigger.newMap);
    }    
    /**
    * callQueueableMethod --- Calls the queueable method
    * @param Map<Id, HEP_SKU_Price__c> mapSKUs
    * @exception Any exception
    * @return void
    */
    public static void callQueueableMethod(Map<Id, HEP_SKU_Price__c> mapSKUs){    
        List<Id> lstSKUIds = new List<Id>();
        lstSKUIds.addAll(mapSKUs.keySet());
        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
            System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstSKUIds, ',') , HEP_Utility.getConstantValue('HEP_SIEBEL_SKUPRICE')));
        }
    }
    /**
    * callQueueableForE1 -- send the details to make the call out to Queueable class
    * @param  
    * @return 
    * @author Himanshi Mayal     
    */
    public void callQueueableForE1(Map<Id, HEP_SKU_Price__c> oldMap)
    {
        //HEP_E1_Queueable.E1Wrapper wrapperE1;
        //List<HEP_E1_Queueable.E1Wrapper> lstWrapper = new List<HEP_E1_Queueable.E1Wrapper>();  
        //Changes for E1 with the Interface transaction records Start 
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        String sHEP_E1_SpendDetail = HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'); 
        HEP_Interface__c objInterfaceRec = [SELECT id FROM HEP_Interface__c WHERE Name =: sHEP_E1_SpendDetail]; 
        map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRecord = new map<String,HEP_Interface_Transaction__c>();
        //Changes for E1 with the Interface transaction records End
        Set<id> setPromoId = new Set<id>();
        for(HEP_SKU_Price__c objSkuPrice : [SELECT id, Unit__c, PriceGrade__c, Promotion_Dating__c, 
                                            Promotion_SKU__r.Promotion_Catalog__r.Promotion__c, Start_Date__c
                                            FROM HEP_SKU_Price__c WHERE id IN :Trigger.newMap.keySet()
                                            AND Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != NULL])
        {
            if(((oldMap.get(objSkuPrice.Id)).Unit__c != objSkuPrice.Unit__c) || 
                    ((oldMap.get(objSkuPrice.Id)).PriceGrade__c != objSkuPrice.PriceGrade__c) || 
                        ((oldMap.get(objSkuPrice.Id)).Promotion_Dating__c != objSkuPrice.Promotion_Dating__c) ||
                            ((oldMap.get(objSkuPrice.Id)).Start_Date__c != objSkuPrice.Start_Date__c))
            {
                setPromoId.add(objSkuPrice.Promotion_SKU__r.Promotion_Catalog__r.Promotion__c);
                //wrapperE1  = new HEP_E1_Queueable.E1Wrapper();
                //wrapperE1.promotionSkuId = objSkuPrice.Promotion_SKU__c;
                //System.debug('WrapperE1 --- '+wrapperE1);
                //lstWrapper.add(wrapperE1);
                //System.debug('List of WrapperE1 --- '+lstWrapper);
            }
        }
        //Changes for E1 with the Interface transaction records Start
        if(setPromoId != NULL && !setPromoId.isEmpty() && objInterfaceRec.Id != NULL){
            //Creation of interface transaction records are being done as the autonumber is being used as the unique number 
            for(String promoId : setPromoId){
                HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
                objTxn.Object_Id__c = promoId;
                objTxn.HEP_Interface__c = objInterfaceRec.Id;
                objTxn.Transaction_Datetime__c = System.now();
                objTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
                mapPromoIdInterfaceRecord.put(promoId,objTxn);
            }
            insert mapPromoIdInterfaceRecord.Values();
            //Changes for E1 with the Interface transaction records End
            for(String promoId : setPromoId)
            {
                //String sjson = json.serialize(lstPromoId);
                //Changes for E1 with the Interface transaction records Start
                HEP_MarketSpendTriggerHandler.E1Wrapper objE1Wrapper = new HEP_MarketSpendTriggerHandler.E1Wrapper();
                if(mapPromoIdInterfaceRecord.containsKey(promoId)){
                    objE1Wrapper.sPromoId = promoId;
                    objE1Wrapper.sTransactionId = mapPromoIdInterfaceRecord.get(promoId).Id;
                }
                String sSerlizedWrap = JSON.serialize(objE1Wrapper);
                //Changes for E1 with the Interface transaction records End
                System.debug('STRING --- '+promoId);
                if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
                    if(objE1Wrapper.sPromoId != NULL && objE1Wrapper.sTransactionId != NULL){
                        System.debug('GOING FOR QUEUEABLE-----');
                        System.enqueueJob(new HEP_E1_Queueable(sSerlizedWrap , HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL')));
                    }    
                }
            }
        }    
    }        
}