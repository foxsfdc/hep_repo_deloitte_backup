@isTest(SeeAllData=false)
public class HEP_Dating_Global_Subsidiary_Cntrl_Test {

    @testSetup
     static void createUsers(){
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
        objRoleOperations.Destination_User__c = u.Id;
        objRoleOperations.Source_User__c = u.Id;
        insert objRoleOperations;
        HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
        objRoleFADApprover.Destination_User__c = u.Id;
        objRoleFADApprover.Source_User__c = u.Id;
        insert objRoleFADApprover;
    }

    @isTest
    private static void testClass()
    {
    HEP_Dating_Global_Subsidiary_Controller controller = new HEP_Dating_Global_Subsidiary_Controller() ;
    }

    static testMethod void testLoadData(){

        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];

        HEP_Dating_Global_Subsidiary_Controller.HEP_Global_Subsidiary_Wrapper objWrapperReturned = new HEP_Dating_Global_Subsidiary_Controller.HEP_Global_Subsidiary_Wrapper();

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
        objNationalTerritory.Flag_Country_Code__c = 'DE';
        insert objNationalTerritory;
        HEP_Territory__c objNationalTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','EMEA','Licensee',objNationalTerritory.Id, objNationalTerritory.Id,'EUR','AT','33',null, false);
        objNationalTerritoryRegion.Flag_Country_Code__c = 'AT';
        insert objNationalTerritoryRegion;
        HEP_Territory__c objNationalTerritoryLicensee = HEP_Test_Data_Setup_Utility.createHEPTerritory('India','APAC','Licensee',null, null,'INR','IN','213',null, false);
        objNationalTerritoryLicensee.FoxipediaCode__c = 'IN';
        insert objNationalTerritoryLicensee;
        HEP_Territory__c objNationalTerritoryAustralia = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia','APAC','Subsidiary',null, null,'AUD','AU','31',null, false);
        objNationalTerritoryAustralia.Flag_Country_Code__c = 'AU';
        insert objNationalTerritoryAustralia;
        HEP_Territory__c objNationalTerritoryUK = HEP_Test_Data_Setup_Utility.createHEPTerritory('UK','EMEA','Subsidiary',null, null,'GBP','GB','505',null, true);
        HEP_Territory__c objNationalTerritoryChina = HEP_Test_Data_Setup_Utility.createHEPTerritory('China','APAC','Subsidiary',null, null,'CNY','CN','109',null, true);
        HEP_Territory__c objNationalTerritoryGreece = HEP_Test_Data_Setup_Utility.createHEPTerritory('Greece','EMEA','Licensee',null, null,'EUR','GR','188',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixGreece = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objNationalTerritoryGreece.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNationalUK = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','PVOD Release Date', objNationalTerritoryUK.Id, objNationalTerritoryUK.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNationalRegion = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritoryRegion.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixLicensee = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','VOD Release Date', objTerritory.Id, objNationalTerritoryLicensee.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixLicenseeRetail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','Physical Release Date (Retail)', objNationalTerritoryLicensee.Id, objNationalTerritoryLicensee.Id, true);
        
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
        HEP_Line_Of_Business__c objLOBTV = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks TV','TV',true);
        
        String sLocaleDateFormat =  u.DateFormat__c;

        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature','FEATR', true);
        EDM_GLOBAL_TITLE__c objTitleEDM = HEP_Test_Data_Setup_Utility.createTitleEDM('Test', '12345', 'PUBLC', objProductType.id, true);

        GTS_Territory__c objGTSTerritory = new GTS_Territory__c(Name__c = 'Test',Name = 'Test',GTS2_ERM_Territory_Id__c = '33');
        insert objGTSTerritory;

        GTS_Title__c objGTSTitle = new GTS_Title__c(Name = 'Test',Foxipedia_ID__c = '1234', Title__c = 'Test',WPR__c= '12345');
        objGTSTitle.US_Release_Date__c =system.today().addDays(-1);
        insert objGTSTitle;

        GTS_Release__c objGTSRelease = new GTS_Release__c(Name = 'Test',Title__c = objGTSTitle.Id, Release_Date__c = system.today(), Territory__c = objGTSTerritory.id);
        insert objGTSRelease;
        
        System.runAs(u){
            Test.startTest();
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,false);
            objPromotion.Domestic_Marketing_Manager__c = u.id;
            objPromotion.Title__c = objTitleEDM.id;
            insert objPromotion;
            HEP_Promotion__c objPromotionTVMaster = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion TV', 'Global', null,objTerritory.Id, objLOBTV.Id, null,null,true);
            HEP_Promotion__c objPromotionTV = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion TV', 'Global', objPromotionTVMaster.id,objTerritory.Id, objLOBTV.Id, null,null,false);
            objPromotionTV.TV_Viewership__c = 1;
            objPromotion.Title__c = objTitleEDM.id;
            insert objPromotionTV;
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);
            HEP_Promotion__c objNationalPromotionUK = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion UK', 'National', objPromotion.Id, objNationalTerritoryUK.Id, objLOB.Id,'','', true);
            
            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNationalRegion = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objPromotion.Id, objDatingMatrixNational.Id, false);
            objDatingRecordNationalRegion.FAD_Approval_Status__c = 'Pending';
            insert objDatingRecordNationalRegion;
            HEP_Promotion_Dating__c objDatingRecordNationalRegionTV = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objPromotionTV.Id, objDatingMatrixNational.Id, false);
            objDatingRecordNationalRegionTV.FAD_Approval_Status__c = 'Pending';
            insert objDatingRecordNationalRegionTV;
            HEP_Promotion_Dating__c objDatingRecordNationalUK = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryGreece.Id, objPromotion.Id, objDatingMatrixGreece.Id, false);
            objDatingRecordNationalUK.FAD_Approval_Status__c = 'Approved';
            insert objDatingRecordNationalUK;
            HEP_Promotion_Dating__c objDatingRecordLicensee = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryLicensee.Id, objPromotion.Id, objDatingMatrixLicensee.Id, false);
            objDatingRecordLicensee.FAD_Approval_Status__c = 'Approved';
            insert objDatingRecordLicensee;
            HEP_Promotion_Dating__c objDatingRecordLicenseeForTV = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryLicensee.Id, objPromotionTV.Id, objDatingMatrixLicensee.Id, false);
            objDatingRecordLicenseeForTV.FAD_Approval_Status__c = 'Pending';
            insert objDatingRecordLicenseeForTV;
            HEP_Promotion_Dating__c objDatingRecordLicenseeRetail = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryLicensee.Id, objPromotion.Id, objDatingMatrixLicenseeRetail.Id, false);
            objDatingRecordLicenseeRetail.FAD_Approval_Status__c = 'Rejected';
            insert objDatingRecordLicenseeRetail;
            //objDatingRecordNational.Domestic_Marketing_Manager__c = u;
            //update objDatingRecordNational;
            
             objWrapperReturned = HEP_Dating_Global_Subsidiary_Controller.getSubsidiaryDates(sLocaleDateFormat, objPromotion.id);

            HEP_Dating_Global_Subsidiary_Controller.getSubsidiaryDates(sLocaleDateFormat, objPromotion.id);
            HEP_Dating_Global_Subsidiary_Controller.getSubsidiaryDates(sLocaleDateFormat, objPromotionTV.id);
            HEP_Dating_Global_Subsidiary_Controller.saveComments(objWrapperReturned);
            Test.stopTest();
        }

    }
}