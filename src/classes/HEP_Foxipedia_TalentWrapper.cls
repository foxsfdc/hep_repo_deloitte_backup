/**
* HEP_Foxipedia_TalentWrapper -- Wrapper to store Talent details for foxipedia integration
* @author    Tejaswini Veliventi
*/

public with sharing class HEP_Foxipedia_TalentWrapper {

    public List< Data > data{get;set;}
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public Links links{get;set;}
    public List<Errors> errors { get; set; }
    
    
    public class Data {
        public Attributes attributes{get;set;}
        public String id{get;set;}  
        public String type{get;set;}  
        public Relationships relationships{get;set;}
    }
    
    
    /**
    * Attributes -- Class to hold Attribute details 
    * @author    Tejaswini Veliventi
    */
    public class Attributes {
        public String type {get;set;} 
        public String rowIdObject {get;set;} 
        public String talentTypeCode {get;set;}
        public String talentTypeDescription {get;set;}
        public String talentRoleSequence {get;set;} 
        public String titleSequence {get;set;} 
        public String characterName {get;set;} 
        public String talentId {get;set;} 
        public String talentFirstName {get;set;}
        public String talentLastName {get;set;} 
        public String isMajorTalent {get;set;} 
        public String languageCode {get;set;} 
        public String languageDescription {get;set;}
        public String talentMiddleName {get;set;} 
        public String suffix {get;set;} 
        public String honorific {get;set;} 
        public String specialty {get;set;} 
        public String foxVersionId {get;set;} 
        public String jdeVaultId {get;set;} 
        public String titleVersionTypeCode {get;set;} 
        public String titleVersionTypeDescription {get;set;}
        public String titleVersionDescription {get;set;} 
        public String foxId {get;set;} 
    }
    
    
    /**
    * Relationships -- Class to hold Relationships details 
    * @author    Tejaswini Veliventi
    */
    public class Relationships {
        public Title title{get;set;}
    }
    
    
    /**
    * Title -- Class to hold links details 
    * @author    Tejaswini Veliventi
    */
    public class Title {
        public Links links{get;set;}
    }
    
    
    /**
    * Links -- Class to hold related details 
    * @author    Tejaswini Veliventi
    */
    public class Links {
        public String related{get;set;} 
    }
    
    
    /**
    * Links -- Class to hold related details 
    * @author    Tejaswini Veliventi
    */
    public class Errors{
        public String title { get; set; }
        public String detail { get; set; }
        public Integer status { get; set; } //400
    }

}