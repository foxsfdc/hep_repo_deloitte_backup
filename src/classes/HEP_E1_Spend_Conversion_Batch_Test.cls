/**
* HEP_E1_Spend_Conversion_Batch_Test -- Test class for E1 Spend
* @author    Deloitte
*/
@isTest 
private class HEP_E1_Spend_Conversion_Batch_Test{
    /**
    * CreateData --  Test method to get Create the Interface Records
    * @return  :  no return value
    */
    @testSetup
    public static void createTestDataSetup(){
    
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        
        User objTestUser1 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
        
    
        System.runAs (objTestUser1) {

            HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_Spend_Interface',true,10,10,'Outbound-Push',true,true,true);
            objInterface.Name = 'HEP_E1_SpendDetail';
            update objInterface;
            //Territory Set up
            HEP_Territory__c objDHETerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null, null, 'ARS', 'DHE' , '921', 'JDE', false);
            objDHETerritory.E1_Code__c = 'E1';
            insert objDHETerritory;
            //Global Territory
            HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
            //Fetch LOB     
            HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
            //Create Promotion:
            HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion', 'Global', null,objGlobalTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objDHEPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', objGlobalPromotion.Id,objDHETerritory.Id, objLOB.Id, null,null,true);
            //Create Spend:
            HEP_CheckRecursive.clearSetIds();
            HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objDHEPromotion.Id,'None',true);
            
        }
        
    }
    
    @isTest 
    static void testE1SpendConversionBatch() {
      User objTestUser = [Select Id from User where Email = 'ApprovalTestUser@hep.com'];
      
      System.runAs (objTestUser) {
	      HEP_Market_Spend__c objTestMarketSpend = [Select Id,Record_Status__c from HEP_Market_Spend__c];
	      objTestMarketSpend.RecordStatus__c = 'Budget Approved';
	      update objTestMarketSpend;
	      Test.startTest();
	      Database.executeBatch(new HEP_E1_Spend_Conversion_Batch('DHE',1),10);
	      Test.stopTest();
      }
    } 
 }