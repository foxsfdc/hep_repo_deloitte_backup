@isTest
private class HEP_Department_Spend_Trail_Ctrl_Test {

    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        
        //List<HEP_Constants__c> lstHEP = Test.loadData(HEP_Constants__c.sobjectType, 'testHEPConstant');
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
          objInterface.Name = 'HEP_E1_SpendDetail';
          Update objInterface;
        // User Created
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Jain', 'anshuljain787@deloitte.com', 'AJ', 'AJ', 'AJ', '', false);
        insert objUser;

        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', '', false);
        objRole.Destination_User__c = objUser.Id;
        objRole.Source_User__c = objUser.Id;
        insert objRole;
        HEP_Line_Of_Business__c objHEPLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks - New Release', 'New Release', false);
        insert objHEPLineOfBusiness;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'ARS', 'DHE', 'DHE', '', false);
        insert objTerritory;

        HEP_GL_Account__c objGlAccount = new HEP_GL_Account__c();
        objGlAccount.Name = 'DHE/Publicity';
        objGlAccount.Record_Status__c = 'Active';
        objGlAccount.GLAccount__c = '642100';
        objGlAccount.Account_Department__c = 'Publicity';
        objGlAccount.GLAccountGroup__c = 'Marketing Spend';
        objGlAccount.GLAccountDescription__c = 'Marketing Spend';
        objGlAccount.Format__c = 'PHY';
        insert objGlAccount;


        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('TestPromotion', 'Global', '', objTerritory.Id, objHEPLineOfBusiness.Id, '', '', false);
        insert objPromotion;



        ContentVersion objContentVersionRecord = new ContentVersion();
        objContentVersionRecord.ContentLocation = 'S';
        objContentVersionRecord.VersionData = blob.valueOf(createPOInvoiceData());
        objContentVersionRecord.Title = 'POINVOICEDATA';
        objContentVersionRecord.isMajorVersion = false;
        objContentVersionRecord.PathOnClient = 'POINVOICEDATA';
        insert objContentVersionRecord;

        list < ContentVersion > lstContentVersions = [SELECT Id, ContentDocumentId
            FROM ContentVersion
            WHERE Id =: objContentVersionRecord.id
        ];

        ContentDocumentLink objContentDocLink = new ContentDocumentLink();
        objContentDocLink.ContentDocumentId = lstContentVersions[0].ContentDocumentId;
        objContentDocLink.LinkedEntityId = objPromotion.Id;
        objContentDocLink.ShareType = 'V';
        insert objContentDocLink;

        HEP_Market_Spend__c objMarketSpendRecord = new HEP_Market_Spend__c();
        objMarketSpendRecord.Promotion__c = objPromotion.Id;
        insert objMarketSpendRecord;

        HEP_Market_Spend_Detail__c objMarketSpend = new HEP_Market_Spend_Detail__c();
        objMarketSpend.Requestor__c = objUser.Id;
        objMarketSpend.Comments__c = 'TestComments';
        objMarketSpend.Budget_Difference__c = 10000;
        objMarketSpend.HEP_Market_Spend__c = objMarketSpendRecord.Id;
        objMarketSpend.GL_Account__c = objGlAccount.Id;
        objMarketSpend.Type__c = 'Initial Budget';
        insert objMarketSpend;



    }

    static String createPOInvoiceData() {
        String sPoInvoiceBody = '{"lstDepartments":[{"sGLAccount":"642100","sDepartmentName":"Publicity","lstPurchaseOrders":[{"sVendorName":"71389","sStatus":"Open","sRequestor":null,"sRequestNumber":"27934","sPONumber":null,"sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"630100","sFormat":"EST","sCurrency":null,"lstLines":[{"sLineStatus":"Open","sLineNumber":"50000","sLineDescription":"STORAGE&FULFILLMENTSVCS","sGLAccount":"53202","sFormat":"EST","sCountry":"DHE","dOpenAmount":0.00,"dAmount":0,"dActuals":0.00},{"sLineStatus":"Open","sLineNumber":"4.000","sLineDescription":"OtherPromoCosts-Misc","sGLAccount":"630100","sFormat":"VOD","sCountry":"DHE","dOpenAmount":300.00,"dAmount":0,"dActuals":0.00}],"dtIssueDate":"2018-03-22","dAmount":1633.33},{"sVendorName":"71389","sStatus":"Open","sRequestor":null,"sRequestNumber":"27933","sPONumber":null,"sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"630100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":"Open","sLineNumber":"50000","sLineDescription":"STORAGE&FULFILLMENTSVCS","sGLAccount":"53202","sFormat":"PHY","sCountry":"DHE","dOpenAmount":0.00,"dAmount":0,"dActuals":0.00},{"sLineStatus":"Open","sLineNumber":"4.000","sLineDescription":"OtherPromoCosts-Misc","sGLAccount":"630100","sFormat":"EST","sCountry":"DHE","dOpenAmount":300.00,"dAmount":0,"dActuals":0.00}],"dtIssueDate":null,"dAmount":1633.33}],"lstInvoices":[{"sVendorName":"354802","sStatus":"JournalOnly","sRequestor":null,"sRequestNumber":"47685","sPONumber":"27933","sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"642100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":null,"sLineNumber":"55.0","sLineDescription":"","sGLAccount":"642100","sFormat":"PHY","sCountry":"","dOpenAmount":0,"dAmount":0,"dActuals":21.49}],"dtIssueDate":"2018-03-21","dAmount":21.49},{"sVendorName":"354802","sStatus":"JournalOnly","sRequestor":null,"sRequestNumber":"47685","sPONumber":"27933","sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"642100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":null,"sLineNumber":"55.0","sLineDescription":"","sGLAccount":"642100","sFormat":"PHY","sCountry":"","dOpenAmount":0,"dAmount":0,"dActuals":21.49}],"dtIssueDate":"2018-03-20","dAmount":21.49},{"sVendorName":"354802","sStatus":"JournalOnly","sRequestor":null,"sRequestNumber":"47685","sPONumber":"27933","sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"642100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":null,"sLineNumber":"55.0","sLineDescription":"","sGLAccount":"642100","sFormat":"PHY","sCountry":"","dOpenAmount":0,"dAmount":0,"dActuals":21.49}],"dtIssueDate":"2018-03-23","dAmount":21.49},{"sVendorName":"354802","sStatus":"JournalOnly","sRequestor":null,"sRequestNumber":"47685","sPONumber":"","sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"642100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":null,"sLineNumber":"55.0","sLineDescription":"","sGLAccount":"642100","sFormat":"PHY","sCountry":"","dOpenAmount":0,"dAmount":0,"dActuals":21.49}],"dtIssueDate":"2018-03-20","dAmount":21.49},{"sVendorName":"354802","sStatus":"JournalOnly","sRequestor":null,"sRequestNumber":"47685","sPONumber":"27933","sGLAccountGroup":null,"sGLAccountDescription":null,"sGLAccount":"642100","sFormat":"VOD","sCurrency":null,"lstLines":[{"sLineStatus":null,"sLineNumber":"55.0","sLineDescription":"","sGLAccount":"642100","sFormat":"PHY","sCountry":"","dOpenAmount":0,"dAmount":0,"dActuals":21.49}],"dtIssueDate":null,"dAmount":21.49}],"dPaidActuals":21.49,"dCommitments":0}]}';
        return sPoInvoiceBody;
    }

    // Method to test the loadData
    @isTest
    static void testLoadData1() {
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_GL_Account__c > lstDepartments = [SELECT Id FROM HEP_GL_Account__c WHERE Name = 'DHE/Publicity'];
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Department_Spend_Trail_Controller.DepartmentAudit objDepartMent = HEP_Department_Spend_Trail_Controller.loadData(lstPromotion[0].Id, lstDepartments[0].Id);
            System.assertNotEquals(null, objDepartMent);
            test.stopTest();
        }
    }

    // Method to test the loadData and throw exception
    @isTest
    static void testLoadData2() {
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_GL_Account__c > lstDepartments = [SELECT Id FROM HEP_GL_Account__c WHERE Name = 'DHE/Publicity'];
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        System.runAs(lstUser[0]) {
            test.startTest();
            try {
                HEP_Department_Spend_Trail_Controller.DepartmentAudit objDepartMent = HEP_Department_Spend_Trail_Controller.loadData(null, null);
            } catch (Exception ex) {
                System.debug('----->' + ex.getMessage());
                System.assertEquals(true, ex.getMessage().containsIgnoreCase('Argument cannot be null'));
                test.stopTest();
            }

        }
    }
    
     // Method to cover HEP_InvoicePurchaseOrderData class
    @isTest
    static void testHEPInvoicePurchaseOrderData() {
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_GL_Account__c > lstDepartments = [SELECT Id FROM HEP_GL_Account__c WHERE Name = 'DHE/Publicity'];
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        System.runAs(lstUser[0]) {
            test.startTest();
                HEP_InvoicePurchaseOrderData objInvoiceData = new HEP_InvoicePurchaseOrderData();
                System.assertEquals(0, objInvoiceData.lstDepartments.size());
                HEP_InvoicePurchaseOrderData.Department objDepartMent = new HEP_InvoicePurchaseOrderData.Department();
                System.assertEquals(0,objDepartMent.dPaidActuals);
                HEP_InvoicePurchaseOrderData.ParentNode objParent = new HEP_InvoicePurchaseOrderData.ParentNode();
                System.assertEquals(0,objParent.dAmount);
                HEP_InvoicePurchaseOrderData.Line objLine = new HEP_InvoicePurchaseOrderData.Line();
                System.assertEquals(0,objLine.dAmount);
                
            test.stopTest();
          
        }
    }

    @isTest
    static void testCheckPermissions1() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Department_Spend_Trail_Controller objSpendTrail = new HEP_Department_Spend_Trail_Controller();
            PageReference objPageReference = objSpendTrail.checkPermissions();
            System.assertEquals('/apex/HEP_AccessDenied', objPageReference.getUrl());
            test.stopTest();
        }
    }

    @isTest
    static void testCheckPermissions2() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        List < HEP_Territory__c > lstTerritory = [SELECT Id FROM HEP_Territory__c WHERE Name = 'DHE'];
        List < HEP_Role__c > lstRole = [SELECT Id FROM HEP_Role__c WHERE Name = 'Brand Manager'];
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id, lstRole[0].Id, lstUser[0].Id, false);
        objUserRole.Record_Status__c = 'Active';
        insert objUserRole;
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Department_Spend_Trail_Controller objSpendTrail = new HEP_Department_Spend_Trail_Controller();
            PageReference objPageReference = objSpendTrail.checkPermissions();
            System.assertEquals(null, null);
            test.stopTest();
        }
    }


    @isTest
    static void testClassesObject1() {
        List < User > lstUser = [SELECT Id, Name FROM User WHERE LastName = 'Jain'];
        List < HEP_Promotion__c > lstPromotion = [SELECT Id FROM HEP_Promotion__c WHERE PromotionName__c = 'TestPromotion'];
        //  List<HEP_Territory__c> lstTerritory = [SELECT Id FROM HEP_Territory__c WHERE Name ='DHE'];
        //  List<HEP_Role__c> lstRole = [SELECT Id FROM HEP_Role__c WHERE Name = 'Brand Manager'];
        //  HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,lstRole[0].Id,lstUser[0].Id,false);
        //  objUserRole.Record_Status__c = 'Active';
        //  insert objUserRole;
        PageReference objPageRef = Page.HEP_Department_Spend_Trail;
        objPageRef.getParameters().put('promoId', String.valueOf(lstPromotion[0].Id));
        Test.setCurrentPage(objPageRef);

        System.runAs(lstUser[0]) {
            test.startTest();
            HEP_Department_Spend_Trail_Controller objSpendTrail = new HEP_Department_Spend_Trail_Controller();
            HEP_Department_Spend_Trail_Controller.DepartmentTransactions objDepartMentTransactions = new HEP_Department_Spend_Trail_Controller.DepartmentTransactions('Initial Budget', 'PO', '612004', 'Publicity', 'DHE/Publicity', 'PHY', System.today(), 'AJ', 13.99, '612004');
            List < HEP_Department_Spend_Trail_Controller.DepartmentTransactions > lstDepartmentTransactions = new List < HEP_Department_Spend_Trail_Controller.DepartmentTransactions > ();
            lstDepartmentTransactions.add(objDepartMentTransactions);
            HEP_Department_Spend_Trail_Controller.DepartmentAudit objDepartMentAudit = new HEP_Department_Spend_Trail_Controller.DepartmentAudit('PublicityId', 'Publicity', 'TestPromotionId', 'TestPromotion', lstDepartmentTransactions);
            System.assertNotEquals(null, objDepartMentAudit);
            HEP_Department_Spend_Trail_Controller.POInvoiceGroup objPoInvoice = new HEP_Department_Spend_Trail_Controller.POInvoiceGroup('PO', 'AJ', '612004', 'Publicity', 'DHE/Publicity', 'PHY', System.today(), 'AJ', 13.99, '612004');
            System.assertNotEquals(null, objPoInvoice);
            test.stopTest();
        }
    }

}