/**
* HEP_InvoicePurchaseOrderData -- Wrapper class for holding Purchase Order and Invoice data 
* @author    Sachin Agarwal
*/
public class HEP_InvoicePurchaseOrderData {
    public list<Department> lstDepartments;
    
    /**
    * Class constructor
    * @return nothing
    */
    public HEP_InvoicePurchaseOrderData(){
      lstDepartments = new list<Department>(); 
    }
    
    
    /**
  * Department -- Wrapper class for holding department data 
  * @author    Sachin Agarwal
  */
  public class Department {
      public String sDepartmentName;
      public String sGLAccount;
      public Decimal dPaidActuals;
      public Decimal dCommitments;
      
      public list<ParentNode> lstPurchaseOrders;
      public list<ParentNode> lstInvoices;
      
      /**
      * Class constructor
      * @return nothing
      */
      public Department(){
        dPaidActuals = 0;
        dCommitments = 0;
        lstPurchaseOrders = new list<ParentNode>();
        lstInvoices = new list<ParentNode>(); 
      }
  }
  
  /**
  * ParentNode -- Wrapper class for holding PO or Invoice data
  * @author    Sachin Agarwal
  */
  public class ParentNode{
    public String sVendorName;
    public String sPONumber;
    public String sRequestNumber;
    public String sStatus;
    public Date dtIssueDate;
    public Decimal dAmount;
    public String sCurrency;
    public String sRequestor;
    public String sGLAccount;
    public String sFormat;
    public String sGLAccountGroup;
    public String sGLAccountDescription;
    
    public list<Line> lstLines;
    
    /**
      * Class constructor
      * @return nothing
      */
    public ParentNode(){
        dAmount = 0;
        lstLines = new list<Line>();
      }
  }
  
  /**
  * Line -- Wrapper class for holding Line data
  * @author    Sachin Agarwal
  */
  public class Line{
    public String sLineNumber;
    public String sGLAccount;
    public String sFormat;
    public String sLineDescription;
    public String sLineStatus;
    public Decimal dAmount;
    public Decimal dActuals;
    public Decimal dOpenAmount;
    public String sPONumber;
    public String sCountry;
    
    /**
      * Class constructor
      * @return nothing
      */
    public Line(){
        dAmount = 0;
        dActuals = 0;
        dOpenAmount = 0;
      }
  }
}