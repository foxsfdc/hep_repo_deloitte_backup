/**
 * HEP_Global_Promotion_Created_Controller --- Dynamically fetch records in Email Template
 * @author    Nishit Kedia
 */

public class HEP_Global_Promotion_Created_Controller
{
    //Promotion dating record id is passed in this variable
    public Id promotionId {get; set;}
     
    //This variable is used to render table on Email template
    public ApprovalEmailWrapper objPromotionWrap{ 
        get{
            if(objPromotionWrap == null){
                fetchPromotionDatingDetails();
            }
            return objPromotionWrap;
        }
        set;
    }
    
    /**
    * fetchPromotionDatingDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author    Nishit Kedia
    */ 
    public void fetchPromotionDatingDetails(){
        
        //------------------------------------//
        try{
            //Instantiate the Wrapper
            objPromotionWrap = new ApprovalEmailWrapper();   
            //------------------------------------//         
            if(promotionId != null){
                //-----------------------------------//
                //Query the Record based on Record Id               
                HEP_Promotion__c objPromotion = [SELECT Id, PromotionName__c,
                                                 Domestic_Marketing_Manager__r.Name , International_Marketing_Manager__r.Name,
                                                 Territory__r.Name , Name , FirstAvailableDate__c
                                                 FROM HEP_Promotion__c
                                                 WHERE Id =: promotionId];


                objPromotionWrap.sTerritory = objPromotion.Territory__r.Name;
                objPromotionWrap.sPromotionName = objPromotion.PromotionName__c;
                objPromotionWrap.sPromoNumber = objPromotion.Name.subString(objPromotion.Name.indexOf('-')+1 , objPromotion.Name.length());
                DateTime dttFad = DateTime.newInstance(objPromotion.FirstAvailableDate__c.year(), objPromotion.FirstAvailableDate__c.month(), objPromotion.FirstAvailableDate__c.day());
                objPromotionWrap.sFAD = dttFad.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                objPromotionWrap.sDomesticMarketingManager = objPromotion.Domestic_Marketing_Manager__r.Name;
                objPromotionWrap.sInternationalMarketingManager = objPromotion.International_Marketing_Manager__r.Name;
                objPromotionWrap.sHref = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/HEP_Promotion_Details?promoId='+promotionId;
            }//-----------------------------------//
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
    }
    
    
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author    Nishit Kedia
    */
    public class ApprovalEmailWrapper{
        
        public String   sTerritory  {get; set;}
        public String   sPromotionName {get; set;}
        public String   sPromoNumber {get; set;}
        public String   sFAD   {get; set;}
        public String   sDomesticMarketingManager   {get; set;}
        public String   sInternationalMarketingManager     {get; set;}  
        public String   sHref {get;set;}      
    }

}