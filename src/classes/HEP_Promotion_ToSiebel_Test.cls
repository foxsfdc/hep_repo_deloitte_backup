@istest
public class HEP_Promotion_ToSiebel_Test {
    /*  @testSetup
    static void createUsers(){
    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Mayal', 'hmayal@deloitte.com','Himanshi','Himanshi','mmaya','',null);
    insert u;

    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager');
    objRole.Destination_User__c = u.Id;
    objRole.Source_User__c = u.Id;
    insert objRole;
    }*/

    @isTest static void HEP_Promotion_ToSiebel_SuccessTest(){
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();

        List<HEP_Constants__c> lstConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',null);
        insert objLOB;

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'AED','DE','182','NON-ESCO', null);
        insert objTerritory;

        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',null);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',null);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        System.debug('objPromotion>> ' + objPromotion);
        
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id, null);
        insert objPromotionRegion;
        
        HEP_Promotion__c objPromo = [SELECT createdDate, createdBy.Name, lastModifiedDate, lastModifiedBy.Name, id, Record_Status__c,
                                     Territory__c, Territory__r.Name, PromotionName__c, LocalPromotionCode__c, StartDate__c, Title__r.FIN_PROD_ID__c,
                                     LineofBusiness__r.Name, Owner.Name, Global_Promotion__c, Name, Promotion_Type__c, Legacy_Id__c, TPR_Only__c
                                     FROM HEP_Promotion__c where id = :objPromotion.id];
        HEP_Promotion_Region__c objPromoReg = [SELECT id, Parent_Promotion__c, Territory__r.Name FROM HEP_Promotion_Region__c WHERE Parent_Promotion__c = :objPromo.Id];
        
        String stempRegionName = '';
        stempRegionName = stempRegionName + objPromoReg.Territory__r.Name + ';';
        
        if(stempRegionName.length()>100)
            stempRegionName = stempRegionName.substring(0, 99);
        
        
        List<String> lstStrId = new List<String>();
        lstStrId.add(String.valueOf(objPromotion.Id));
        String sresultJson = HEP_Promotion_ToSiebel.getPromotionRequestBody(lstStrId);
        String updatedDate = String.ValueOf(objPromo.lastModifiedDate);
        String sActualResult = '{"ProducerID":"HEP","EventType":"HEPPromoOutbound","EventName":"HEPBroadcastEvent","Data":{"Payload":{"items":[{"updatedDate":"' + updatedDate + '","updatedBy":"' + String.ValueOf(objPromo.lastModifiedBy.Name) + '","titleNumber":"' + String.ValueOf(objPromo.Title__r.FIN_PROD_ID__c) + '","territoryName":"' + String.ValueOf(objPromo.Territory__r.Name) + '","territoryId":"' + String.ValueOf(objPromo.Territory__c) + '","startDate":' + String.ValueOf(objPromo.StartDate__c) + ',"regionName":"' + stempRegionName + '","recordStatus":"' + String.ValueOf(objPromo.Record_Status__c) + '","recordId":"' + String.ValueOf(objPromo.id) + '","promotionName":"' + String.ValueOf(objPromo.PromotionName__c) + '","promotionCode":"' + String.ValueOf(objPromo.LocalPromotionCode__c) + '","parentId":"' + String.ValueOf(objPromo.Global_Promotion__c) + '","ownerLogin":"","objectType":"promotion","lineOfBusiness":"' + String.ValueOf(objPromo.LineofBusiness__r.Name) + '","legacyId":' + String.ValueOf(objPromo.Legacy_Id__c) + ',"createdDate":"' + (String.ValueOf(objPromo.createdDate)) + '","createdBy":"' + String.ValueOf(objPromo.createdBy.Name) + '"}]}}}';

        System.assertEquals(sresultJson, sActualResult);


        Test.startTest();
        
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = String.valueOf(objPromotion.Id); 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_ToSiebel demo = new HEP_Promotion_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }
    
    @isTest static void HEP_Promotion_ToSiebel_FailureTest(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Siebel_Service';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/edf/events/failure';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer54444'; 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_ToSiebel demo = new HEP_Promotion_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();
    }
    
    @isTest static void HEP_Promotion_ToSiebel_InvalidAcessTokenTest(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Siebel_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        //objTxnResponse.sSourceId = 'aer54444'; 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_ToSiebel demo = new HEP_Promotion_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();        
        System.assertEquals('Invalid Access Token Or Invalid txnResponse data sent', objTxnResponse.sErrorMsg);         
    }  
}