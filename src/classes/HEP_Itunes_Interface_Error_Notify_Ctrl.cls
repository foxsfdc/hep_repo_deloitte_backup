/**
* HEP_Itunes_Interface_Error_Notify_Ctrl -- class for the VF component to dynamically fetch interface transaction error records and display it in Email Template for iTunes interface. 
* @author    Gaurav Mehrishi
*/
public class HEP_Itunes_Interface_Error_Notify_Ctrl{  
    public Id iTEDetails {get; set;}
    public String sDailyWeekly {get; set;}
    public List<EmailWrapper> lstEmailWrapper{
        get{
            if(lstEmailWrapper == null){
                fetchDetails();
            }
            return lstEmailWrapper;
        }
        set;
    }
    /**
    * Helps to get details of interface transaction error records for iTunes interface passed from VF component.
    * @return null
    */
    public void fetchDetails(){
         try{
            lstEmailWrapper = new List<EmailWrapper>();   
            EmailWrapper objMail; 
            Integer iCount = 1;
            //This is the main map with Key--> MDPPromotionProduct Id and value is MDPWrapper
            Map < Id, MDPWrapper > mapMDPWrapper = new Map < Id, MDPWrapper > ();            
            //Helps to get the details interface error records.           
            if(iTEDetails != null){  
                List<HEP_Interface_Transaction_Error__c> lstITE = new List<HEP_Interface_Transaction_Error__c> (); 
                Set<Id> setMDPPromotionProductId = new Set<Id>();  
                Set<Id> setIDs = new Set<Id>();
                for (HEP_Outbound_Email__c objOutboundEmail: [Select id,Object_API__c,Record_Id__c,Email_Template_Name__c,CC_Email_Address__c,To_Email_Address__c,Email_Sent__c from    
                                HEP_Outbound_Email__c where Email_Template_Name__c = : HEP_Utility.getConstantValue('HEP_Itunes_Interface_Error_Notify') 
                                AND Email_Sent__c = false
                                ]) 
                            {   
                                setIDs.add(objOutboundEmail.Record_Id__c);            
                            }
                system.debug('setIDs ' + setIDs);               
                //Query to get all interface transaction error records for the iTunes interface
                for(HEP_Interface_Transaction_Error__c objITE : [SELECT Id,
                                    Name, 
                                    Error_Datetime__c,
                                    Error_Description__c,
                                    HEP_Interface_Transaction__r.Name,
                                    HEP_Interface_Transaction__r.HEP_Interface__r.Name,
                                    HEP_Interface_Transaction__c,
                                    HEP_Interface_Transaction__r.Object_Id__c,
                                    HEP_Interface_Transaction__r.Status__c,
                                    LastModifiedBy.Name, 
                                    Status__c
                        FROM HEP_Interface_Transaction_Error__c
                        WHERE HEP_Interface_Transaction__r.HEP_Interface__r.Name =: 'HEP_iTunes_Price'
                        AND HEP_Interface_Transaction__r.Status__c  =: 'Failure'                    
                        AND id IN: setIDs                       
                        ]){
                            lstITE.add(objITE); 
                            setMDPPromotionProductId.add(objITE.HEP_Interface_Transaction__r.Object_Id__c); 
                            MDPWrapper objITEPromotionProduct = new MDPWrapper();
                            objITEPromotionProduct.sErrorMessage = objITE.Error_Description__c;
                            mapMDPWrapper.put(objITE.HEP_Interface_Transaction__r.Object_Id__c,objITEPromotionProduct);                         
                        }
                system.debug('-------->setMDPPromotionProductId' + setMDPPromotionProductId);
                system.debug('-------->mapMDPWrapper' + mapMDPWrapper);
                for(HEP_MDP_Promotion_Product__c objMDPPromotionProduct : [SELECT Id, 
                                    Product_End_Date__c,
                                    HEP_Promotion__c,
                                    HEP_Promotion__r.PromotionName__c,
                                    Product_Start_Date__c,
                                    Title_Name__c,
                                    HEP_Territory__r.Name
                        FROM HEP_MDP_Promotion_Product__c
                        WHERE Id IN: setMDPPromotionProductId 
                        ]){
                            MDPWrapper objMDP = mapMDPWrapper.get(objMDPPromotionProduct.Id);
                            //MDPWrapper objMDP = new MDPWrapper();
                            objMDP.sPromotionName = objMDPPromotionProduct.HEP_Promotion__r.PromotionName__c;
                            objMDP.sProductName = objMDPPromotionProduct.Title_Name__c;
                            objMDP.sEndDate = HEP_Utility.getFormattedDate(objMDPPromotionProduct.Product_End_Date__c,'dd-MMM-yyyy');
                            objMDP.sStartDate = HEP_Utility.getFormattedDate(objMDPPromotionProduct.Product_Start_Date__c,'dd-MMM-yyyy');
                            objMDP.sRegion = objMDPPromotionProduct.HEP_Territory__r.Name;
                            mapMDPWrapper.put(objMDPPromotionProduct.Id,objMDP);                                
                        }
                system.debug('---> mapMDPWrapper' + mapMDPWrapper);
           
                    for(Id sKey: mapMDPWrapper.keySet()){                                                    
                            MDPWrapper objMDP = mapMDPWrapper.get(sKey);
                            objMail = new EmailWrapper();
                            objMail.sPromotionName = objMDP.sPromotionName;
                            objMail.sProductName = objMDP.sProductName;
                            objMail.sStartDate = objMDP.sStartDate;
                            objMail.sEndDate = objMDP.sEndDate;
                            objMail.sRegion = objMDP.sRegion;
                            objMail.sErrorMessage = objMDP.sErrorMessage;
                            lstEmailWrapper.add(objMail);  
                            objMail.iIndex = iCount;
                            iCount++;
                    }
                               
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

       
    /*
    **  EmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class EmailWrapper{       
        public String sPromotionName {get; set;}
        public String sProductName {get; set;}
        public String sStartDate{get; set;}     
        public String sEndDate {get; set;}      
        public String sErrorMessage {get; set;}
        public String sRegion {get; set;}
        public Integer iIndex {get;set;}        
    }
    /**
    * MDPWrapper --- Class to hold data related to MDP Promotion Product. 
    * @author    Gaurav Mehrishi
    */
    public class MDPWrapper {
        public String sPromotionName {get; set;}
        public String sProductName {get; set;}
        public String sStartDate{get; set;}     
        public String sEndDate {get; set;}      
        public String sErrorMessage {get; set;}  
        public String sRegion {get; set;}       
        public MDPWrapper() {}

        /**
         * MDPWrapper Constructor --- Constructor to initialize the List variables
         * @return   N/A
         */
        public MDPWrapper(String sPromotionName, String sProductName, String sStartDate, String sEndDate, String sErrorMessage, String sRegion) {
            this.sPromotionName = sPromotionName;
            this.sProductName = sProductName;
            this.sStartDate = sStartDate;
            this.sEndDate = sEndDate;
            this.sErrorMessage = sErrorMessage;
            this.sRegion = sRegion;
        }
    }
}