/*------------------------------------Fox HEP----------------------------------
This class handles the business logic for inbound REST from Copper for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
public class HEP_TitleReleases_Copper_Inbound implements HEP_IntegrationInterface {
    public HEP_TitleReleases_Copper_Inbound() {

    }
    
    /**
    * @param objWrap - Integration Transaction Response Object
    * @return void 
    */
    public void performTransaction(HEP_InterfaceTxnResponse objWrap) {
        string sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        String catalogId = objWrap.sRequest; 
        Date earliestReleaseDate = Date.newInstance(2009, 1, 1);
        List<HEP_List_Of_Values__c> copperChannelObj = [SELECT Values__c FROM HEP_List_Of_Values__c WHERE Type__c = 'COPPER_CHANNEL' and record_status__C = :sACTIVE]; 
        List<HEP_List_Of_Values__c> copperFormatObj = [SELECT Values__c FROM HEP_List_Of_Values__c WHERE Type__c = 'COPPER_FORMAT' and record_status__C = :sACTIVE]; 
        Set<String> copperChannel = new Set<String>(); 
        Set<String> copperFormat = new Set<String>(); 

        for (HEP_List_Of_Values__c cc : copperChannelObj) {
            copperChannel.add(cc.Values__c); 
        }
        for (HEP_List_Of_Values__c cf : copperFormatObj) {
            copperFormat.add(cf.Values__c); 
        }

        //System.debug('XOXOXOX copperChannel = ' + copperChannel); 
        //System.debug('XOXOXOX copperChannel = ' + copperFormat); 
        
        List<HEP_Promotion_Dating__c> pdList = [
            SELECT  Id, HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, 
                    HEP_Catalog__r.CatalogId__c, Date__c, 
                    Territory__r.Territory_Code_Integration__c, Territory__r.Name,
                    Date_Type__c, Promotion__r.PromotionName__c, 
                    Promotion__r.Status__c, 
                    Promotion__r.Record_Status__c,
                    Promotion__r.LocalPromotionCode__c, 
                    Promotion__r.LineOfBusiness__r.Type__c,
                    Promotion__r.LineOfBusiness__r.Name, 
                    HEP_Catalog__r.Title_EDM__r.Name, 
                    HEP_Catalog__r.Catalog_Name__c, 
                    Record_Status__c, Channel__c        
            FROM    HEP_Promotion_Dating__c
            WHERE   HEP_Promotion_Dating__c.HEP_Catalog__r.CatalogId__c = :catalogId AND 
                    Territory__r.Send_to_Prophet__c = true AND 
                    Date__c != NULL AND 
                    Date__c > :earliestReleaseDate AND 
                    Channel__c IN :copperChannel AND
                    Record_Status__c = :sACTIVE AND 
					Customer__c = NULL AND
					DateType__c LIKE '%Release Date%'   
                    //Status__c = 'Confirmed'   AND 
        ]; 

        List<HEP_SKU_Price__c> spList = [
            SELECT  Id, Promotion_Dating__r.Id,
                    Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c,
                    Region__r.Territory_Code_Integration__c,
                    Region__r.Name,
                    SKU_Master__r.Catalog_Master__r.CatalogId__c,
                    Promotion_Dating__r.Date__c,
                    Promotion_Dating__r.Channel__c,
                    Promotion_Dating__r.Territory__r.Territory_Code_Integration__c,
                    Promotion_Dating__r.Territory__r.Name,
                    Promotion_Dating__r.Date_Type__c,
                    Promotion_Dating__r.Record_Status__c,
                    Promotion_Dating__r.Promotion__r.PromotionName__c,
                    Promotion_Dating__r.Promotion__r.Status__c,
                    Promotion_Dating__r.Promotion__r.Record_Status__c,
                    Promotion_Dating__r.Promotion__r.LocalPromotionCode__c,
                    Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Type__c,
                    Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Name,
                    Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.Name,
                    Promotion_Dating__r.HEP_Catalog__r.Catalog_Name__c,
                    Promotion_SKU__r.SKU_Master__r.SKU_Number__c,
                    Promotion_SKU__r.SKU_Master__r.SKU_Title__c,
                    Promotion_SKU__r.SKU_Master__r.Record_Status__c,
                    Promotion_SKU__r.SKU_Master__r.Channel__c,
                    Promotion_SKU__r.SKU_Master__r.JDE_Aspect_Ratio__c,
                    Promotion_SKU__r.SKU_Master__r.Format__c,
                    Promotion_SKU__r.SKU_Master__r.SKU_Type__c
            FROM    HEP_SKU_Price__c
            WHERE   SKU_Master__r.Catalog_Master__r.CatalogId__c = :catalogId AND 
                    Promotion_Dating__r.Territory__r.Send_to_Prophet__c = true AND 
                    Promotion_Dating__r.Date__c > :earliestReleaseDate AND 
                    Promotion_Dating__r.Customer__c = NULL AND
					Promotion_Dating__r.DateType__c LIKE '%Release Date%' AND
                    Promotion_SKU__r.SKU_Master__r.Channel__c IN :copperChannel AND 
                    Promotion_SKU__r.SKU_Master__r.Format__c IN :copperFormat AND
                    Promotion_SKU__r.SKU_Master__r.Type__c = 'Master' AND
                    Record_Status__c = :sACTIVE
        ];
        
        // Get unique Promotion_Dating__r.Id
        Set<Id> promoDatingIds = new Set<Id>(); 
        for (HEP_Promotion_Dating__c pd : pdList) {
            promoDatingIds.add(pd.Id);
        }
        for (HEP_SKU_Price__c sp : spList) {
            promoDatingIds.add(sp.Promotion_Dating__r.Id); 
        }

        // Merge HEP_Promotion_Dating__c and HEP_SKU_Price__c by Promotion_Dating__r.Id
        List<CatalogReleases> crMergeList = new List<CatalogReleases>(); 
        List<Id> mergedPromoDatingIds = new List<Id>(); 

        for (Id promoDatingId : promoDatingIds) {
            for (HEP_Promotion_Dating__c promoDating : pdList) {
                for (HEP_SKU_Price__c skuPrice : spList) {
                    if (promoDatingId == promoDating.Id && promoDating.Id == skuPrice.Promotion_Dating__r.Id) {
                        CatalogReleases cr = new CatalogReleases (
                            mergeValue(promoDating.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, skuPrice.Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c), // finTitleId
                            promoDating.Id, // recordId
                            Integer.valueOf(mergeValue(promoDating.HEP_Catalog__r.CatalogId__c, skuPrice.SKU_Master__r.Catalog_Master__r.CatalogId__c)), // ctlgId
                            //mergeValue(promoDating.Territory__r.Name, skuPrice.Promotion_Dating__r.Territory__r.Name), // territoryName
                            //mergeValue(promoDating.Territory__r.Territory_Code_Integration__c, skuPrice.Promotion_Dating__r.Territory__r.Territory_Code_Integration__c), // territoryCode
                            mergeValue(skuPrice.Region__r.Name, promoDating.Territory__r.Name), // territoryName
                            mergeValue(skuPrice.Region__r.Territory_Code_Integration__c, promoDating.Territory__r.Territory_Code_Integration__c), // territoryCode
                            mergeValue(promoDating.Channel__c, skuPrice.Promotion_SKU__r.SKU_Master__r.Channel__c), // channel
                            promoDating.Date__c, // releaseDate
                            mergeValue(promoDating.HEP_Catalog__r.Title_EDM__r.Name, skuPrice.Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.Name), // titleName
                            mergeValue(promoDating.Promotion__r.PromotionName__c, skuPrice.Promotion_Dating__r.Promotion__r.PromotionName__c), // promoName
                            mergeValue(promoDating.Promotion__r.Record_Status__c, skuPrice.Promotion_Dating__r.Promotion__r.Record_Status__c), // promoStatus
                            mergeValue(promoDating.Promotion__r.LocalPromotionCode__c, skuPrice.Promotion_Dating__r.Promotion__r.LocalPromotionCode__c), // localPromoCode
                            mergeValue(promoDating.Promotion__r.LineOfBusiness__r.Type__c, skuPrice.Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Type__c), // lineOfBusinessType
                            mergeValue(promoDating.Promotion__r.LineOfBusiness__r.Name, skuPrice.Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Name), // lineOfBusiness
                            mergeValue(promoDating.HEP_Catalog__r.Catalog_Name__c, skuPrice.Promotion_Dating__r.HEP_Catalog__r.Catalog_Name__c), // ctlgName
                            skuPrice.Promotion_SKU__r.SKU_Master__r.SKU_Number__c, // skuNum
                            skuPrice.Promotion_SKU__r.SKU_Master__r.SKU_Title__c, // skuName
                            promoDating.Record_Status__c,//skuPrice.Promotion_SKU__r.SKU_Master__r.Record_Status__c, // skuStatus
                            promoDating.Record_Status__c, // active
                            skuPrice.Promotion_SKU__r.SKU_Master__r.JDE_Aspect_Ratio__c, // aspectRatio
                            skuPrice.Promotion_SKU__r.SKU_Master__r.SKU_Type__c, // skuType
                            skuPrice.Promotion_SKU__r.SKU_Master__r.Format__c // format
                        );

                        crMergeList.add(cr); 

                        // Keep tracke of merged promo dating Ids
                        mergedPromoDatingIds.add(promoDatingId); 
                    }
                }
            }
        }

        // Remove merged promo ids from list
        for (Id promoDatingID : mergedPromoDatingIds) {
            if (promoDatingIds.contains(promoDatingID)) {
                promoDatingIds.remove(promoDatingID); 
            }
        }

        // Add un-merged promo dating ids to response list
        for (Id pdId : promoDatingIds) {
            // TODO: Get response from pdList
            for (HEP_Promotion_Dating__c pd : pdList) {
                if (pd.Id == pdId && pd.Id != null) {
                    CatalogReleases cr = new CatalogReleases (
                        pd.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, //String finTitleId, 
                        pd.Id, //Id recordId, 
                        Integer.valueOf(pd.HEP_Catalog__r.CatalogId__c), //Integer ctlgId, 
                        pd.Territory__r.Name,//String territoryName, 
                        pd.Territory__r.Territory_Code_Integration__c, //String territoryCode, 
                        pd.Channel__c, //String channel, 
                        pd.Date__c, //Date releaseDate, 
                        pd.HEP_Catalog__r.Title_EDM__r.Name, //String titleName, 
                        pd.Promotion__r.PromotionName__c, //String promoName, 
                        pd.Promotion__r.Record_Status__c, //String promoStatus, 
                        pd.Promotion__r.LocalPromotionCode__c, //String localPromoCode, 
                        pd.Promotion__r.LineOfBusiness__r.Type__c, //String lineOfBusinessType, 
                        pd.Promotion__r.LineOfBusiness__r.Name, //String lineOfBusiness, 
                        pd.HEP_Catalog__r.Catalog_Name__c, //String ctlgName, 
                        '',//String skuNum, 
                        '',//String skuName, 
                        pd.Record_Status__c,//'',//String skuStatus, 
                        pd.Record_Status__c, //String active, 
                        '',//String aspectRatio, 
                        '',//String skuType, 
                        ''//String format
                    );

                    crMergeList.add(cr); 
                }
            }

            // TODO: Get response from spList
            for (HEP_SKU_Price__c sp : spList) {
                if (sp.Promotion_Dating__r.Id == pdId && sp.Promotion_Dating__r.Id != null) { // QUESTIONS: DO I PREVENT NULL PROMO DATING HERE?? 
                    CatalogReleases cr = new CatalogReleases (
                        sp.Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c, //String finTitleId, 
                        sp.Promotion_Dating__r.Id, //Id recordId, 
                        Integer.valueOf(sp.SKU_Master__r.Catalog_Master__r.CatalogId__c), //Integer ctlgId, 
                        sp.Region__r.Name,//sp.Promotion_Dating__r.Territory__r.Name, //String territoryName, 
                        sp.Region__r.Territory_Code_Integration__c,//sp.Promotion_Dating__r.Territory__r.Territory_Code_Integration__c, //String territoryCode, 
                        sp.Promotion_SKU__r.SKU_Master__r.Channel__c, //String channel, 
                        sp.Promotion_Dating__r.Date__c, //Date releaseDate, 
                        sp.Promotion_Dating__r.HEP_Catalog__r.Title_EDM__r.Name, //String titleName, 
                        sp.Promotion_Dating__r.Promotion__r.PromotionName__c,//String promoName, 
                        sp.Promotion_Dating__r.Promotion__r.Record_Status__c,//String promoStatus, 
                        sp.Promotion_Dating__r.Promotion__r.LocalPromotionCode__c,//String localPromoCode, 
                        sp.Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Type__c,//String lineOfBusinessType, 
                        sp.Promotion_Dating__r.Promotion__r.LineOfBusiness__r.Name,//String lineOfBusiness, 
                        sp.Promotion_Dating__r.HEP_Catalog__r.Catalog_Name__c,//String ctlgName, 
                        sp.Promotion_SKU__r.SKU_Master__r.SKU_Number__c,//String skuNum, 
                        sp.Promotion_SKU__r.SKU_Master__r.SKU_Title__c,//String skuName, 
                        sp.Promotion_Dating__r.Record_Status__c,//sp.Promotion_SKU__r.SKU_Master__r.Record_Status__c,//String skuStatus, 
                        sp.Promotion_Dating__r.Record_Status__c,//String active, 
                        sp.Promotion_SKU__r.SKU_Master__r.JDE_Aspect_Ratio__c,//String aspectRatio, 
                        sp.Promotion_SKU__r.SKU_Master__r.SKU_Type__c,//String skuType, 
                        sp.Promotion_SKU__r.SKU_Master__r.Format__c//String format
                    );

                    crMergeList.add(cr); 
                }
            }
        }

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(crMergeList));  
        objWrap.sResponse = JSON.serialize(crMergeList); 
    }

    private String mergeValue(String value1, String value2) {
        return (value1 == null || String.isEmpty(value1))? value2 : value1;  
    }

 //   private static Map<String,String> channelMap = new Map<String,String> {
    //  'PVOD Release Date'                 => 'PVOD',
    //  'EST Release Date'                  => 'DHD',
    //  'VOD Release Date'                  => 'VOD',
    //  'Physical Release Date (Retail)'    => 'RETAIL',
    //  'Physical Release Date (Kiosk)'     => 'KIOSK',
    //  'Physical Release Date (Rental)'    => 'RENTAL',
    //  'SPEST Release Date'                => 'SPDHD',
    //  'PEST Release Date'                 => 'PDHD',
    //  'SPVOD Release Date'                => 'SPVOD'
    //}; 

    public class CatalogReleases {
        public String finTitleId; 
        public String recordId; 
        public Integer ctlgId; 
        public String territoryName; 
        public String territoryCode; 
        public String channel; 
        public String releaseDate; 
        public String deletedFlag; 
        public String titleName; 
        public String promoName; 
        public String promoStatus; 
        public String localPromoCode; 
        public String lineOfBusinessType; 
        public String lineOfBusiness; 
        public String ctlgName; 
        public String active;
        public String skuNum; 
        public String skuName; 
        public String skuStatus; 
        public String aspectRatio; 
        public String ctlgKey; 
        public String region;
        public String skuType; 
        public String format; 

        public CatalogReleases(String finTitleId, Id recordId, Integer ctlgId, String territoryName, String territoryCode, String channel, Date releaseDate, String titleName, 
                                String promoName, String promoStatus, String localPromoCode, String lineOfBusinessType, String lineOfBusiness, String ctlgName, String skuNum, String skuName, 
                                String skuStatus, String active, String aspectRatio, String skuType, String format) {

            this.finTitleId = finTitleId; 
            this.recordId = String.valueOf(recordId); 
            this.ctlgId = ctlgId; 
            this.territoryName = territoryName; 
            this.territoryCode = territoryCode; 
            //if (channelMap.get(channel) != null) {
            //  this.channel = channelMap.get(channel);
            //}
            //else {
            //  this.channel = channel; 
            //}
            this.channel = channel; 
            this.releaseDate = String.valueOf(releaseDate); 
            this.deletedFlag = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_DELETED').Value__c) ? true : false);
            this.titleName = titleName; 
            this.promoName = promoName; 
            this.promoStatus = promoStatus.toUpperCase(); 
            this.localPromoCode = localPromoCode; 
            if(String.isNotEmpty(lineOfBusinessType))
                this.lineOfBusinessType = lineOfBusinessType.replace(' ','_').toUpperCase(); 
            this.lineOfBusiness = lineOfBusiness; 
            this.ctlgName = ctlgName; 
            this.skuNum = skuNum; 
            this.skuName = skuName; 
            this.skuStatus = String.valueOf((skuStatus == HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c) ? true : false); 
            this.active = String.valueOf((active == HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c) ? true : false);
            this.aspectRatio = aspectRatio; 
            this.ctlgKey = this.recordId; 
            this.region = territoryCode; 
            this.skuType = skuType; 
            this.format = format; 
        }
    }
}