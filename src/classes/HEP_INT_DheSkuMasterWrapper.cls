/**
* HEP_INT_DheSkuMasterWrapper --JDE SkuMaster Wrapper to store the DheSkuMaster details from JDE And E1 Interfaces 
* @author    Ram Bairwa
*/
public  class HEP_INT_DheSkuMasterWrapper{
    public  List<DheSkuMaster> dhe_sku_master;
    public String calloutStatus;
    public String TxnId;
    public List<HEP_INT_DheSkuInvoiceWrapperUtility.Errors> errors;
    
    /*public HEP_INT_DheSkuMasterWrapper()
    {
        dheSkuMaster=New List<DheSkuMaster>();
    }*/
    /**
    * DheSkuMaster -- Class to hold SkuMaster details 
    * @author    Ram Bairwa
    */
    public class DheSkuMaster{
       //common fields for both interfaces NTS and E1
        	public String MPAA_Rating;
            public String Number_of_Discs;
            public String Price_Rule;
            public String Wholesale_Price_US;
            public String User_ID;
            public String Channel;
            public String Licensor;
            public String ActionCode;
            public String Feature_Run;
            public String Territory;
            public String BW_Color; //BW/Color
            public String Quebec_Rating;
            public String Additional_Media;
            public String HEP_Promotion_SKU_Id;
            public String UV_Code;
            public String UPC_Code;
            public String Customer_Exclusive;
            public String Digital_Copy_Flag;
            public String Current_Release_Date;
            public String Packaging;
            public String Catalog; //Catalog#
            public String Local_PromoCode;
            public String Pack_Type;
            public String Primary_Language;
            public String Aspect_Ratio;
            public String Subtitle_Language;
            public String Request_Id;
            public String Total_Run;
            public String BD_Enhancement;
            public String Canadian_Rating;
            public String Gift_With_Purchase_Flag;
            public String BD_Plus;
            public String Item_Number;
            public String Prelim_flag;
            public String SMF_Run;
            public String Item_Description;
            public String e_Copy_Flag; //e-Copy_Flag
            public String Date_Updated;
            public String Dubbed_Language;
            public String Enhanced_Packaging;
            public String Time_Updated;
            public String Item_Status;
            public String Media_Type;
            public String ThreeD_Flag; //3D_Flag
            public String Wholesale_Price_CAN;

    }
    
   /**
    * Errors -- Class to hold related details 
    * @author    Ram Bairwa
    
    public class Errors{
        public String title;
        public String detail;
        public Integer status;//400
    }*/
}