@isTest
Public class HEP_NTSInvoice_Details_Test{

    @testSetup 
    Static void setup(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    
    @isTest 
    static void Success_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTerritoryE1Code":"US","sCatalogNumber":"1234","sFromDate":"2017-02-03","sToDate":"2017-04-06"}'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTSInvoice_Details demo = new HEP_NTSInvoice_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    } 
    
     @isTest 
    static void Failure_Test(){           
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTerritoryE1Code":"US","sCatalogNumber":"12345","sFromDate":"2017-02-03","sToDate":"2017-04-06"}';
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTSInvoice_Details demo = new HEP_NTSInvoice_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }  
    
   @isTest 
    static void Access_Token_Failure_Test(){  
        HEP_Constants__c objConstant = new HEP_Constants__c(); 
        objConstant =[select id,name from HEP_Constants__c where Name = 'HEP_NTS_E1_POOAUTH']; 
        delete objConstant;      
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '{"sTerritoryE1Code":"US","sCatalogNumber":"12345","sFromDate":"2017-02-03","sToDate":"2017-04-06"}';
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_NTSInvoice_Details demo = new HEP_NTSInvoice_Details();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();   
    }                  
}