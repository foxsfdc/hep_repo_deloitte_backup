/**
* HEP_PurchaseOrder_Details -- purchase Order details will be stored from JDE And E1 Interfaces 
* @author    Lakshman Jinnuri
*/
public class HEP_PurchaseOrder_Details implements HEP_IntegrationInterface{ 
    /**
    * HEP_PurchaseOrderDetails -- Rating details to store Purchase details
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_purchaseOrderWrapper objWrapper = new HEP_purchaseOrderWrapper();
        String sHEP_JDE_E1_OAUTH = HEP_Utility.getConstantValue('HEP_JDE_E1_OAUTH');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_JDE_E1_PODETAILS = HEP_Utility.getConstantValue('HEP_JDE_E1_PODETAILS');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        String sHEP_CURRENCY = HEP_Utility.getConstantValue('HEP_CURRENCY');
        String sHEP_REPLACE_CURRENCY = HEP_Utility.getConstantValue('HEP_REPLACE_CURRENCY');
        String sHEP_POLine = HEP_Utility.getConstantValue('HEP_POLineNo');
        String sHEP_REPLACE_POLine_NO = HEP_Utility.getConstantValue('HEP_REPLACE_POLine_NO');
        String sHEP_PONo = HEP_Utility.getConstantValue('HEP_PONo');
        String sHEP_REPLACE_PO_NO = HEP_Utility.getConstantValue('HEP_REPLACE_PO_NO');
        String sHEP_OBJECT = HEP_Utility.getConstantValue('HEP_OBJECT');
        String sHEP_REPLACE_OBJECT_NO = HEP_Utility.getConstantValue('HEP_REPLACE_OBJECT_NO');
        list<HEP_Promotion__c> lstTerritory;
        if(sHEP_JDE_E1_OAUTH != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_JDE_E1_OAUTH);
        
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sPromoId;
            String sTerritory;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Territory and PromoId details to get the purchase order details through integration 
            HEP_InvoiceWrapper.HEP_TerritoryDetails objTerritoryDetails = (HEP_InvoiceWrapper.HEP_TerritoryDetails)JSON.deserialize(sDetails,HEP_InvoiceWrapper.HEP_TerritoryDetails.class);
            sPromoId = objTerritoryDetails.sPromoId;
            sTerritory = objTerritoryDetails.sTerritory;
            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            if(sHEP_JDE_E1_OAUTH != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_JDE_E1_PODETAILS);   
            if(objServiceDetails != null){
                //Check if the territory is empty, if so only the promoId is used to send the request
                if(sTerritory != null && !String.isEmpty(sTerritory))
                    objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?promoCode=' + sPromoId + '&territory=' + sTerritory);
                else
                    objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?promoCode=' + sPromoId);
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(sHEP_SUCCESS != null  && objHttpResp.getStatus().equals(sHEP_SUCCESS) && sHEP_CURRENCY != null && sHEP_REPLACE_CURRENCY != null  
                    && sHEP_POLine != null && sHEP_REPLACE_POLine_NO != null && sHEP_PONo != null  && sHEP_REPLACE_PO_NO != null  
                    && sHEP_OBJECT != null && sHEP_REPLACE_OBJECT_NO != null){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    String sResponse = objHttpResp.getBody();
                    sResponse = sResponse.replace(sHEP_CURRENCY , sHEP_REPLACE_CURRENCY); 
                    sResponse = sResponse.replace(sHEP_POLine , sHEP_REPLACE_POLine_NO);
                    sResponse = sResponse.replace(sHEP_PONo , sHEP_REPLACE_PO_NO);
                    sResponse = sResponse.replace(sHEP_OBJECT , sHEP_REPLACE_OBJECT_NO);
                    objWrapper = (HEP_purchaseOrderWrapper)JSON.deserialize(sResponse,HEP_purchaseOrderWrapper.class);
                    System.debug('objWrapper :' + objWrapper );
                     if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(sHEP_Int_Txn_Response_Status_Success != null && sHEP_Int_Txn_Response_Status_Success != null)        
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                        objTxnResponse.bRetry = false;
                    }
                }
                else{
                    if(sHEP_FAILURE != null && sHEP_FAILURE != null)
                        objTxnResponse.sStatus = sHEP_FAILURE;
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
        }
        else{
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }        
    }    
}