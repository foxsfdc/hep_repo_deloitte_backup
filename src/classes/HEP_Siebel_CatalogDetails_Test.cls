/**
* HEP_Siebel_CatalogDetails_Test -- Test class for the HEP_Siebel_CatalogDetails for Siebel Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Siebel_CatalogDetails_Test {
    /**
    * HEP_CatalogDetails_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_SuccessTest() {
        HEP_Territory__c territory = HEP_Test_Data_Setup_Utility.createHEPTerritory('France', 'EMEA', 'Subsidiary', null, null, 'EUR', 'FRA', '170',null, true);
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<ID> lstCatalogIds = new list<ID>();
        Test.startTest();
        HEP_Catalog__c catalog = HEP_Test_Data_Setup_Utility.createCatalog('34939493', 'TitleCatalog', 'Single', null, territory.Id, 'Pending', 'Request', true);
        lstCatalogIds.add(catalog.Id);
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '["'+ catalog.Id +'"]'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogDetails objCatalogDetails = new HEP_Siebel_CatalogDetails();
        objCatalogDetails.performTransaction(objTxnResponse);
        objCatalogDetails.getRequestBody(lstCatalogIds);
        Test.stoptest();
    }
    /**
    * HEP_CatalogDetails_FailureTest --  Test method for Failure response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '[]'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogDetails objCatalogDetails = new HEP_Siebel_CatalogDetails();
        objCatalogDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    /**
    * HEP_CatalogDetails_InvalidAcessTokenTest --  Test method for Invalid Access Token
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_CatalogDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '["0013D00000QBoZL", "0013D00000QBoZB"]'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Siebel_CatalogDetails objCatalogDetails = new HEP_Siebel_CatalogDetails();
        objCatalogDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }
}