/**
 * HEP_Fetch_SKU_ImageDetails --  To fetch Image Details For SKU Images in Master Screens 
 * @author  :  Nishit Kedia    
 */
public class HEP_Fetch_SKU_ImageDetails implements HEP_IntegrationInterface {
    
    
    /**
     * performTransaction --  To fetch Image Details For SKU Images in Master Screens
     * @return no return value 
     * @author  :  Nishit Kedia    
     */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse) {

        HEP_SKU_ImageWrapper objWrapper = new HEP_SKU_ImageWrapper();

        //if(sAccessToken != null && sAccessToken.startsWith(HEP_Constants.HEP_TOKEN_BEARER) && objTxnResponse != null){
        if (objTxnResponse != null) {

            //Fetch source String from the Wrapper
            HEP_SKU_InputWrapper objInputSourceWrapper = new HEP_SKU_InputWrapper();
            String sSourceId = objTxnResponse.sSourceId;

            if (sSourceId != null) {                
                //Deserialize the Wrapper 
                objInputSourceWrapper = (HEP_SKU_InputWrapper) JSON.deserialize(sSourceId, HEP_SKU_InputWrapper.class);
                HTTPRequest objHttpRequest = new HTTPRequest();
                
                //Gets the Details from custom setting for Authentication
                String sInterfaceName = HEP_Utility.getConstantValue('HEP_SKU_ImageDetails');
                if(String.isNotBlank(sInterfaceName)){
                    HEP_Services__c objServiceDetails = HEP_Services__c.getInstance(sInterfaceName);
                    if(objServiceDetails != null){
                        //Fetch EndPoint Details
                        String sEndPoint = objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '&user_id=' + objServiceDetails.Username__c + '&password=' + objServiceDetails.Password__c;
                        //Build The Extension String for Api
                        String sSkuImageExtension = getSkuExtensions(objInputSourceWrapper);
                        //Check If it is Not null and hence It will not be Appended
                        if (String.isNotBlank(sSkuImageExtension)) {
                            sEndPoint += sSkuImageExtension;
                        }
                        
                        if (objServiceDetails != null) {
                            //Set EndPoint
                            objHttpRequest.setEndpoint(sEndPoint);
                            objHttpRequest.setMethod('GET');
                            //Set Timeout
                            objHttpRequest.setTimeout(Integer.ValueOf(System.Label.HEP_TIMEOUT));
                            System.debug('Request is :' + objHttpRequest);
                            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                            //Initiate The Callout Request
                            HTTP objHttp = new HTTP();
                            HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                            //Response from callout
                            objTxnResponse.sResponse = objHttpResp.getBody(); 
                            //Fetching Image
                            String sResponse = objTxnResponse.sResponse;
                            if(String.isNotBlank(sResponse)){
                                sResponse = sResponse.replaceAll('\\/', '/');
                                System.debug('Response Body--->' + sResponse);
                            }
                            
                           // if (objHttpResp.getStatus() == HEP_Constants.SUCCESS) {
                            if (objHttpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK')) {
                                //Gets the details from Foxipedia interface and stores in the wrapper 
                                objWrapper = (HEP_SKU_ImageWrapper) JSON.deserialize(objHttpResp.getBody(), HEP_SKU_ImageWrapper.class);
                                System.debug('objWrapper :' + objWrapper);
                                if (objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)) {
                                    System.debug(objWrapper.errors[0].detail);
                                    
                                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                                    if (objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length() > 254)
                                        objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                                    else
                                        objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                                    objTxnResponse.bRetry = true;
                                } else {                                   
                                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                                    objTxnResponse.bRetry = false;
                                }
                            } else {
                                 objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                                if (objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                                    objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                                else
                                    objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                                objTxnResponse.bRetry = true;
                            }
                        }
                    }
                }
            } else {
                System.debug('Source Id is Net set');                
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }
        } else {
            //if the token is not valid or if we get the invalid transaction response            
            System.debug('Invalid Access Token Or Invalid txnResponse data sent'); 
            objTxnResponse = new HEP_InterfaceTxnResponse();          
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }
    }


    /**
     * getSkuExtensions --  To fetch Endpoint Extension for fetching Sku Image
     * @param objInputSourceWrapper wrapper Instance of the source objTxnResponse;
     * @return String returning the Extension
     * @author  :  Nishit Kedia    
     */
    public String getSkuExtensions(HEP_SKU_InputWrapper objInputSourceWrapper) {
        String sUrlExtension = '';
        //Check If null is not given as Input
        if (objInputSourceWrapper != null) {
            List < String > lstSkuNumbers = objInputSourceWrapper.lstSkuObjectNumber;
            String sTerritoryCode = objInputSourceWrapper.sTerritoryCode;
            if (lstSkuNumbers != null && lstSkuNumbers.size() > 0 && String.isNotBlank(sTerritoryCode)) {
                Integer iIndexCount = 0;
                String sBuildApiString = '';
                for (String sSkuObjectNo: lstSkuNumbers) {
                    sBuildApiString += '&sku[' + iIndexCount + ']=' + sSkuObjectNo;
                    iIndexCount++;
                }

                if (String.isNotBlank(sBuildApiString)) {
                    sBuildApiString += '&market=' + sTerritoryCode;
                }

                if (String.isNotBlank(sBuildApiString)) {   
                    sUrlExtension = sBuildApiString;
                }
            }
            System.debug('The Url  Extension Built is ------->' + sUrlExtension);           
        }
        return sUrlExtension;
    }
}