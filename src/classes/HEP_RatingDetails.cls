/**
* HEP_RatingDetails -- Rating details to store Rating details 
* @author    Lakshman Jinnuri
*/
public class HEP_RatingDetails implements HEP_IntegrationInterface{ 
    /**
    * HEP_RatingDetails -- Rating details to store Rating details
    * @return no return value 
    * @author  :  Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_Services__c objServiceDetails;
        HEP_RatingWrapper objWrapper = new HEP_RatingWrapper();
        //Variables to hold the custom Settings data
        String sHEP_FOXIPEDIA_OAUTH = HEP_Utility.getConstantValue('HEP_FOXIPEDIA_OAUTH');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_RATING_DETAILS = HEP_Utility.getConstantValue('HEP_RATING_DETAILS');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        if(sHEP_FOXIPEDIA_OAUTH != null)      
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_FOXIPEDIA_OAUTH);
        System.debug('sAccessToken++++'+sAccessToken);

        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sFoxId = objTxnResponse.sSourceId;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Details from custom setting for Authentication
            if(sHEP_RATING_DETAILS != null && sHEP_RATING_DETAILS != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_RATING_DETAILS);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?foxId=' + sFoxId);
                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('system',System.Label.HEP_SYSTEM_FOXIPEDIA);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(sHEP_SUCCESS != null && objHttpResp.getStatus().equals(sHEP_SUCCESS)){
                    //Gets the details from Foxipedia interface and stores in the wrapper 
                    objWrapper = (HEP_RatingWrapper)JSON.deserialize(objHttpResp.getBody(), HEP_RatingWrapper.class);    
                    System.debug('objWrapper :' + objWrapper );
                    if(sHEP_FAILURE != null && objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(sHEP_Int_Txn_Response_Status_Success != null)         
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                        objTxnResponse.bRetry = false;
                    }
                }
                else{
                    if(sHEP_FAILURE != null)
                        objTxnResponse.sStatus = sHEP_FAILURE;
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }     
    }    
}