/**
* HEP_JDE_Spend_Details -- Controller to make the callout to JDE interface for sending the Spend Details.
* @author    Lakshman Jinnuri
*/
public class HEP_INT_JDE_Spend_Details implements HEP_IntegrationInterface{
    /**
    * HEP_JDE_SpendWrapper -- Wrapper to store Spend details for interface JDE 
    * @author    Lakshman Jinnuri
    */
    public class HEP_JDE_SpendWrapper{
        public String ProducerID;   
        public String EventName;    
        public String EventType;    
        public Data Data;
        
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */
        public HEP_JDE_SpendWrapper(){
            this.Data = new Data();
        }
    }
    
    /**
    * Data -- Class to store Rating details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Payload Payload;
        
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */ 
        public Data(){
            this.Payload = new Payload();
        }
    }
    
    /**
    * Payload -- Class to store Payload details 
    * @author    Lakshman Jinnuri
    */
    public class Payload {
        public String BatchNumber;  
        public String TriggerName;  
        public String AccountCreation;                                    
        public list<String> BudgetDetails;
        
        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */ 
        public Payload(){
            this.BudgetDetails = new list<String>();
        }  
    }

    //Variables to hold the custom Settings data
    String sHEP_JDE_OAUTH = HEP_Utility.getConstantValue('HEP_JDE_OAUTH');
    String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
    String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
    String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
    String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
    String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
    String sHEP_JDE_PRODUCERID = HEP_Utility.getConstantValue('HEP_JDE_PRODUCERID');
    String sHEP_JDE_EVENTTYPE = HEP_Utility.getConstantValue('HEP_JDE_EVENTTYPE');
    String sHEP_JDE_EVENTNAME = HEP_Utility.getConstantValue('HEP_JDE_EVENTNAME');
    String sHEP_JDE_SERVICE = HEP_Utility.getConstantValue('HEP_JDE_SERVICE');
    String sHEP_SPEND = HEP_Utility.getConstantValue('HEP_SPEND');
    String sHEP_TERRITORY_DHE = HEP_Utility.getConstantValue('HEP_TERRITORY_DHE');
    String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
    String sHEP_GL_ACC_FORMAT_PHY = HEP_Utility.getConstantValue('HEP_LINE_FORMAT_PHY');
    String sHEP_GL_ACC_FORMAT_VOD = HEP_Utility.getConstantValue('HEP_CHANNEL_VOD');
    String sHEP_GL_ACC_FORMAT_VR = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_VR');
    String sHEP_GL_ACC_FORMAT_DHD = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_DHD');
    String sHEP_GL_ACC_51557 = HEP_Utility.getConstantValue('HEP_GL_ACC_51557');
    String sHEP_GL_ACC_FORMAT_00915 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00915');
    String sHEP_GL_ACC_FORMAT_00910 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00910');
    String sHEP_GL_ACC_FORMAT_00950 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00950');
    String sHEP_GL_ACC_FORMAT_00960 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00960');
    String sHEP_GL_ACC_FORMAT_00980 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00980');
    String sHEP_GL_ACC_FORMAT_00000 = HEP_Utility.getConstantValue('HEP_GL_ACC_FORMAT_00000');
    String sHEP_TERRITORY_CA = HEP_Utility.getConstantValue('HEP_TERRITORY_CA');
    String sHEP_JDE_TERRITORY_CODE_C = HEP_Utility.getConstantValue('HEP_JDE_TERRITORY_CODE_C');
    String sHEP_JDE_TERRITORY_CODE_U = HEP_Utility.getConstantValue('HEP_JDE_TERRITORY_CODE_U');
    String sHEP_JDE_TIMEFORMAT = HEP_Utility.getConstantValue('HEP_JDE_TIMEFORMAT');
    String sHEP_Market_Spend_Current = HEP_Utility.getConstantValue('HEP_SPEND_DETAIL_STATUS_CURRENT');
    
    

    /**
    * performTransaction -- Method to store Catalog details 
    * @param  Takes HEP_InterfaceTxnResponse wrapper as input
    * @return no return value 
    * @author Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        String sCatalogJson;
        if(sHEP_JDE_OAUTH != null )   
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_JDE_OAUTH);
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sSpendWrapper;
            if(objTxnResponse.sSourceId != null){
                sSpendWrapper = getRequestBody((List<Id>)JSON.deserialize(objTxnResponse.sSourceId,list<Id>.class));
            }
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for Authentication
            if(sHEP_JDE_SERVICE != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_JDE_SERVICE);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c);
                objHttpRequest.setMethod('POST');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('Accept','application/json');
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objHttpRequest.setBody(sSpendWrapper);
                System.debug('Request is :' + objHttpRequest);
                System.debug('Request Body :' +objHttpRequest.getBody());
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(sHEP_SUCCESS != null && sHEP_STATUS_ACCEPTED != null &&  sHEP_FAILURE != null){
                    if((objHttpResp.getStatus().equals(sHEP_SUCCESS) || objHttpResp.getStatus().equals(sHEP_STATUS_ACCEPTED)) && sHEP_Int_Txn_Response_Status_Success != null){         
                        objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                    }
                    else{
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    }
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }     
    }

    /**
    * getRequestBody -- gets the details from Market Spend Object based on input paramters
    * @param  Takes List of Id's
    * @return Serialised HEP_JDE_SpendWrapper wrapper   
    * @author Lakshman Jinnuri      
    */
    private string getRequestBody(list<ID> lstSpendIds){
        HEP_JDE_SpendWrapper objSpendWrapper = new HEP_JDE_SpendWrapper();
        Map<Id,HEP_Approvals__c> mapApproval = new  Map<Id,HEP_Approvals__c>();
        String sFormattedDate;
        String sLocalPromoCode;
        String sGLAccount;
        String sMarketSpendDetails;
        String sFormat;
        String sTerritoryCode;
        Integer iIndex = 0;
        String sBudget;
        String sTempMrktSpendDtl = '';
        Map<Id,list<HEP_Market_Spend_Detail__c>> mapSpendDetail = new Map<Id,list<HEP_Market_Spend_Detail__c>>();
        //list<Id> lstSpendId = new list<Id>();
        if(sHEP_JDE_PRODUCERID != null && sHEP_JDE_EVENTTYPE != null && sHEP_JDE_EVENTNAME != null){
            objSpendWrapper.ProducerId = sHEP_JDE_PRODUCERID;
            objSpendWrapper.EventType = sHEP_JDE_EVENTTYPE;
            objSpendWrapper.EventName = sHEP_JDE_EVENTNAME;
            objSpendWrapper.Data.Payload.TriggerName = sHEP_SPEND;
        }   
        //gets the latest Approval record associated with Market Spend
        for(HEP_Approvals__c objApproval : [SELECT Id,Name,HEP_Market_Spend__c,LastModifiedDate FROM HEP_Approvals__c WHERE HEP_Market_Spend__c in : lstSpendIds]){
            if(mapApproval.containsKey(objApproval.HEP_Market_Spend__c) && objApproval.LastModifiedDate > mapApproval.get(objApproval.HEP_Market_Spend__c).LastModifiedDate)
                mapApproval.put(objApproval.HEP_Market_Spend__c,objApproval);
            else{
                if(mapApproval.containsKey(objApproval.HEP_Market_Spend__c))
                    continue;    
                mapApproval.put(objApproval.HEP_Market_Spend__c,objApproval);
            }
        }  
        //gets the list of Market spend details associated with the Market Spend
        for(HEP_Market_Spend_Detail__c objSpendDetail : [SELECT Id,HEP_Market_Spend__c,GL_Account__r.GLAccount__c,GL_Account__r.Format__c,Budget__c FROM HEP_Market_Spend_Detail__c WHERE HEP_Market_Spend__c in : lstSpendIds AND Type__c =: sHEP_Market_Spend_Current]){
             if(!mapSpendDetail.containsKey(objSpendDetail.HEP_Market_Spend__c)){
                mapSpendDetail.put(objSpendDetail.HEP_Market_Spend__c,new list<HEP_Market_Spend_Detail__c>{objSpendDetail});
            }
            else{
                list<HEP_Market_Spend_Detail__c> lstSpendDtl = mapSpendDetail.get(objSpendDetail.HEP_Market_Spend__c);
                lstSpendDtl.add(objSpendDetail);
                mapSpendDetail.put(objSpendDetail.HEP_Market_Spend__c,lstSpendDtl);
            }    
        } 
        //creates the JSON which needs to be send to the JDE interface with Market spend,Approval and Market spend details   
        for(HEP_Market_Spend__c objSpend : [SELECT Id,Name,RequestDate__c,Promotion__c,Promotion__r.Territory__c,Promotion__r.LocalPromotionCode__c,Promotion__r.Territory__r.FoxipediaCode__c,Promotion__r.Territory__r.Name
                      FROM
                        HEP_Market_Spend__c
                      WHERE 
                        Id IN :lstSpendIds]){
            if(objSpend.RequestDate__c != null){  
                DateTime dtDat; 
                dtDat = datetime.newInstance(objSpend.RequestDate__c.year(), objSpend.RequestDate__c.month(),objSpend.RequestDate__c.day());
                sFormattedDate = dtDat.format(sHEP_JDE_TIMEFORMAT);
            }    
            else
                sFormattedDate = String.ValueOf('').leftPad(12,'');
            if(mapApproval.containsKey(objSpend.Id) && sHEP_JDE_PRODUCERID != null && sHEP_TERRITORY_DHE != null && sHEP_JDE_TIMEFORMAT != null){
                objSpendWrapper.Data.Payload.BatchNumber = mapApproval.get(objSpend.Id).Name.substring(4,12);     
                objSpendWrapper.Data.Payload.AccountCreation = mapApproval.get(objSpend.Id).Name.substring(4,12) + sHEP_JDE_PRODUCERID.rightPad(39) + objSpend.Promotion__r.LocalPromotionCode__c.rightPad(6) + objSpend.Promotion__r.Territory__r.Name.rightPad(3) + sFormattedDate + sHEP_JDE_PRODUCERID;
            } 
            if(mapSpendDetail.containsKey(objSpend.Id)){ 
                iIndex = 1; 
                for(HEP_Market_Spend_Detail__c objMarketSpndDtl : mapSpendDetail.get(objSpend.Id)){
                    sMarketSpendDetails = '';
                    sFormat = '';
                    sBudget = '';
                    if(objSpend.Promotion__c != null && objSpend.Promotion__r.Territory__c != null){
                        if(objSpend.Promotion__r.Territory__r.FoxipediaCode__c != null)
                            sTerritoryCode = objSpend.Promotion__r.Territory__r.FoxipediaCode__c.equals(sHEP_TERRITORY_CA) ? sHEP_JDE_TERRITORY_CODE_C : sHEP_JDE_TERRITORY_CODE_U;
                        else
                            sTerritoryCode = sHEP_JDE_TERRITORY_CODE_U;
                    }
                    if(objMarketSpndDtl.GL_Account__r.Format__c != null && objMarketSpndDtl.GL_Account__r.Format__c.equals(sHEP_GL_ACC_FORMAT_PHY) 
                        && objMarketSpndDtl.GL_Account__r.GLAccount__c != null && objMarketSpndDtl.GL_Account__r.GLAccount__c.equals(sHEP_GL_ACC_51557))
                        sFormat = sHEP_GL_ACC_FORMAT_00915;
                    else if(objMarketSpndDtl.GL_Account__r.Format__c != null && objMarketSpndDtl.GL_Account__r.Format__c.equals(sHEP_GL_ACC_FORMAT_PHY) 
                        && objMarketSpndDtl.GL_Account__r.GLAccount__c != null && !objMarketSpndDtl.GL_Account__r.GLAccount__c.equals(sHEP_GL_ACC_51557))
                        sFormat = sHEP_GL_ACC_FORMAT_00910;
                    else if(objMarketSpndDtl.GL_Account__r.Format__c != null && objMarketSpndDtl.GL_Account__r.Format__c.equals(sHEP_GL_ACC_FORMAT_VOD))
                        sFormat = sHEP_GL_ACC_FORMAT_00950; 
                    else if(objMarketSpndDtl.GL_Account__r.Format__c != null && objMarketSpndDtl.GL_Account__r.Format__c.equals(sHEP_GL_ACC_FORMAT_DHD))
                        sFormat = sHEP_GL_ACC_FORMAT_00960;
                    else if(objMarketSpndDtl.GL_Account__r.Format__c != null && objMarketSpndDtl.GL_Account__r.Format__c.equals(sHEP_GL_ACC_FORMAT_VR))
                        sFormat = sHEP_GL_ACC_FORMAT_00980;
                    else    
                        sFormat = sHEP_GL_ACC_FORMAT_00000;     
                    sBudget = objMarketSpndDtl.Budget__c != null ? string.valueof(objMarketSpndDtl.Budget__c).rightPad(14) : string.valueof(0).rightPad(14);
                    sLocalPromoCode = objSpend.Promotion__r.LocalPromotionCode__c != null ? string.valueof(objSpend.Promotion__r.LocalPromotionCode__c) : string.valueof('').rightPad(6,'');
                    if(objMarketSpndDtl.GL_Account__r.GLAccount__c != null){
                        sTempMrktSpendDtl = (sMarketSpendDetails + string.valueOf(iIndex).rightPad(12) + sTerritoryCode + sLocalPromoCode + '.' + String.valueof(objMarketSpndDtl.GL_Account__r.GLAccount__c).leftPad(5) + '.' + sFormat.rightPad(15)).rightPad(42); 
                        sMarketSpendDetails =  sTempMrktSpendDtl + sBudget.rightPad(14) + mapApproval.get(objSpend.Id).Name.substring(4,12);
                        objSpendWrapper.Data.Payload.BudgetDetails.add(sMarketSpendDetails);
                        iIndex++;
                    }    
                }
            }
        }
        return JSON.serialize(objSpendWrapper);       
    }     
}