@isTest
private class HEP_Utility_Test {
	@isTest static void test_method_one(){
		list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_List_Of_Values__c> objLOVs = HEP_Test_Data_Setup_Utility.createHEPListOfValues();

        HEP_Utility.MultiPicklistWrapper objMultiWrap = new HEP_Utility.MultiPicklistWrapper();
		//Create User
		User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
															  'test@deloitte.com','testL','testL','Fox - New Release',true);

		//Create Line of Business
		HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);
		System.debug('objLineOfBusiness --> ' + objLineOfBusiness);

		//Create Role
		HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','Promotion',true);

		//Create EDM Product Type.
		EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','FEATR',true);

		//Create EDM Title Record.
		EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);

		//Create Territory
		List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
																		HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
																		HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false),
																	    HEP_Test_Data_Setup_Utility.createHEPTerritory('India','EMEA','Subsidiary',null,null,'USD','IN','999','JDE',false)};

		lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
		lstTerritory[0].Flag_Country_Code__c = '1';
		lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
		lstTerritory[1].Flag_Country_Code__c = '2';
		lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
		lstTerritory[2].Flag_Country_Code__c = '3';
		insert lstTerritory;
		System.debug('Territory : ' + lstTerritory);

		/*Date_Time_Locale__c objDateTimeLocale = new Date_Time_Locale__c(Name = 'en_US' , Date_Format__c = 'M/d/yyyy' , Date_Time_Format__c = 'M/d/yyyy h:mm a');
		insert objDateTimeLocale;*/

		

		//Create User Role
		List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,UserInfo.getUserId(),false),
																		 HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,UserInfo.getUserId(),false),
																		 HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,UserInfo.getUserId(),false)};

		insert lstUserRoles;

		//Create Promotion
		List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false),
																		  HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
																		  HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)};
        
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[0].Digital_Physical__c = 'Both';
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[1].Digital_Physical__c = 'Both';
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;
        lstPromotions[2].Digital_Physical__c = 'Both';
        insert lstPromotions;

        System.debug('Test promotions ---> ' + lstPromotions);

		//Create User Favourite Records:
		HEP_User_Favourites__c objuserFavourite = new HEP_User_Favourites__c(User__c = UserInfo.getUserId() , Record_Id__c = lstPromotions[0].Id);
		insert objuserFavourite;

		//Create Notification Record..
		HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','<Date_Type__c>','test type' ,'Active','Approved', 'test',true);

		List<HEP_Promotions_DatingMatrix__c> lstDatingMatrix = new List<HEP_Promotions_DatingMatrix__c>();
		HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Avail Date', lstTerritory[0].Id, lstTerritory[1].Id, false);

		HEP_Promotions_DatingMatrix__c objPVOD = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','PVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objPVOD.Media_Type__c = 'Digital';
		objPVOD.Date_Offset__c = 20;
		objPVOD.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objPVOD);
		HEP_Promotions_DatingMatrix__c objPVOD1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','PVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objPVOD1.Media_Type__c = 'Physical';
		objPVOD1.Date_Offset__c = 20;
		objPVOD1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objPVOD1);
		HEP_Promotions_DatingMatrix__c objEST = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','EST Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objEST.Media_Type__c = 'Digital';
		objEST.Date_Offset__c = 20;
		objEST.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objEST);
		HEP_Promotions_DatingMatrix__c objEST1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','EST Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objEST1.Media_Type__c = 'Physical';
		objEST1.Date_Offset__c = 20;
		objEST1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objEST1);
		HEP_Promotions_DatingMatrix__c objVOD = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','VOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objVOD.Media_Type__c = 'Digital';
		objVOD.Date_Offset__c = 20;
		objVOD.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objVOD);
		HEP_Promotions_DatingMatrix__c objVOD1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','VOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objVOD1.Media_Type__c = 'Physical';
		objVOD1.Date_Offset__c = 20;
		objVOD1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objVOD1);
		HEP_Promotions_DatingMatrix__c objRetail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Retail)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objRetail.Media_Type__c = 'Digital';
		objRetail.Date_Offset__c = 20;
		objRetail.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objRetail);
		HEP_Promotions_DatingMatrix__c objRetail1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Retail)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objRetail1.Media_Type__c = 'Physical';
		objRetail1.Date_Offset__c = 20;
		objRetail1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objRetail1);
		HEP_Promotions_DatingMatrix__c objKiosk = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Kiosk)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objKiosk.Media_Type__c = 'Digital';
		objKiosk.Date_Offset__c = 20;
		objKiosk.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objKiosk);
		HEP_Promotions_DatingMatrix__c objKiosk1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Kiosk)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objKiosk1.Media_Type__c = 'Physical';
		objKiosk1.Date_Offset__c = 20;
		objKiosk1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objKiosk1);
		HEP_Promotions_DatingMatrix__c objRental = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Rental)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objRental.Media_Type__c = 'Digital';
		objRental.Date_Offset__c = 20;
		objRental.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objRental);
		HEP_Promotions_DatingMatrix__c objRental1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','Physical Release Date (Rental)',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objRental1.Media_Type__c = 'Physical';
		objRental1.Date_Offset__c = 20;
		objRental1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objRental1);
		HEP_Promotions_DatingMatrix__c objSPEST = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SPEST Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSPEST.Media_Type__c = 'Digital';
		objSPEST.Date_Offset__c = 20;
		objSPEST.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSPEST);
		HEP_Promotions_DatingMatrix__c objSPEST1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SPEST Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSPEST1.Media_Type__c = 'Physical';
		objSPEST1.Date_Offset__c = 20;
		objSPEST1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSPEST1);
		HEP_Promotions_DatingMatrix__c objSPVOD = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SPVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSPVOD.Media_Type__c = 'Digital';
		objSPVOD.Date_Offset__c = 20;
		objSPVOD.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSPVOD);
		HEP_Promotions_DatingMatrix__c objSPVOD1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SPVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSPVOD1.Media_Type__c = 'Physical';
		objSPVOD1.Date_Offset__c = 20;
		objSPVOD1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSPVOD1);
		HEP_Promotions_DatingMatrix__c objSVOD = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSVOD.Media_Type__c = 'Digital';
		objSVOD.Date_Offset__c = 20;
		objSVOD.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSVOD);
		HEP_Promotions_DatingMatrix__c objSVOD1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates','SVOD Release Date',lstTerritory[0].Id , lstTerritory[0].Id, false);
		objSVOD1.Media_Type__c = 'Physical';
		objSVOD1.Date_Offset__c = 20;
		objSVOD1.Date_Offset_Source__c = 'PVOD Release Date';
		lstDatingMatrix.add(objSVOD1);
		System.debug('lstDatingMatrix ---> ' + JSON.serialize(lstDatingMatrix));
		insert lstDatingMatrix;

		
        HEP_Promotion_Dating__c objPromDating = HEP_Test_Data_Setup_Utility.createDatingRecord(lstTerritory[0].Id, lstPromotions[0].Id, objPVOD.id,false);
        objPromDating.Date_Type__c = 'EST Release Date';
        insert objPromDating;

        HEP_Notification_Utility.createNotifications(new List<HEP_Promotion_Dating__c>{objPromDating} , HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change') , 'HEP_Promotion__c');
        HEP_Notification_Utility.createNotifications(new List<HEP_Promotion_Dating__c>{objPromDating} , 'Promotion_Dating_ReConfirmed' , 'HEP_Promotion__c');

        Contact con = new Contact(LastName = 'Contact');
        List<Contact> lstContact =  new List<Contact>{con};
        insert lstContact;

        HEP_Utility.fetchCustomPermissions(lstTerritory[0].Id , 'HEP_Create_National_Promotion');
		HEP_Utility.redirectToErrorPage();
		HEP_Utility.buildQueryAllString('HEP_Promotion__c','Select Territory__r.Name,','WHERE Name LIKE \'%PR-%\'');
		HEP_Utility.buildQueryAllString('HEP_Promotion__c',null,'WHERE Name LIKE \'%PR-%\'');
		HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
		HEP_Utility.getPickListValues('HEP_Promotion__c', new List<String>{'Customer_Promotion_Status__c'});
		HEP_Utility.getDateTimeLocaleFormat(true , true);
		HEP_Utility.getDateTimeLocaleFormat(true , false);
		HEP_Utility.getDateTimeLocaleFormat(false , true);
		HEP_Utility.getDateTimeLocaleFormat(false , false);
		Date_Time_Locale__c objDateTimeLocale = new Date_Time_Locale__c(Name = 'en_US' , Date_Format__c = 'M/d/yyyy' , Date_Time_Format__c = 'M/d/yyyy h:mm a');
		insert objDateTimeLocale;
		HEP_Utility.getDateTimeLocaleFormat(true , true);
		HEP_Utility.getDateTimeLocaleFormat(true , false);
		HEP_Utility.getDateTimeLocaleFormat(false , true);
		HEP_Utility.getDateTimeLocaleFormat(false , false);

		HEP_Utility.getFormattedDate(Date.today() , 'DD-MM-YYYY');

		HEP_Utility.getTypeAheadValues('PromotionName__c','HEP_Promotion__c','Global Promotion','PromotionName__c LIKE \'%Promotion%\'');
		HEP_Utility.getResourceURL();
		HEP_Utility.getPromoDefaultImageUrl();
		HEP_Utility.mapGetUserLocaleData([SELECT Id , DateFormat__c , NumberFormat__c FROM User WHERE Id =: UserInfo.getUserId()]);
		HEP_Utility.getUserCurrencyFormat('COMMA_PERIOD');

		HEP_Utility.getPickListValuesWrappers('HEP_Promotion__c',new List<String>{'Digital_Physical__c'});
		HEP_Utility.getPickListValuesWrappers('HEP_Promotion__c',new List<String>{'PromotionName__c'});
		HEP_Utility.getPicklistValuesWrapper('HEP_Promotion__c',new List<String>{'Digital_Physical__c'});
		HEP_Utility.getPicklistValuesWrapper('HEP_Promotion__c',new List<String>{'PromotionName__c'});
		HEP_Utility.getPicklistValuesWrapper('HEP_Promotion__c','Digital_Physical__c');
		HEP_Utility.getPicklistValuesWrapper('HEP_Promotion__c','PromotionName__c');

		HEP_Utility.getResourceURL('1',HEP_Utility.getHEPStaticResource());
		HEP_Utility.getResourceURL(null,HEP_Utility.getHEPStaticResource());

		HEP_Utility.getFinacialTitleId(objTitle.Name);
		HEP_Utility.getFinacialTitleId('q23');

		HEP_Utility.getFinTitleId(objTitle.Name);
		HEP_Utility.getFinTitleId('12341234');

		HEP_Utility.getGlobalPromotions(lstPromotions[0].PromotionName__c);
		HEP_Utility.getAllCreatePromotionErrorMessages();

		HEP_Utility.getDateStatus(Date.today() , Date.today());
		HEP_Utility.getDateStatus(Date.today() , Date.today().addDays(1));

		HEP_Utility.getFavouriteDetails((new Map<Id , HEP_Promotion__c>(lstPromotions)).keyset());
		Map<Id , sObject> mapObjects = new Map<Id , sObject>(lstPromotions);
		HEP_Utility.checkFieldUpdation(mapObjects , mapObjects, 'HEP_Promotion__c' , 'Promotion_Field_Set');

		HEP_Utility.getAllValidDateTypes(lstPromotions[1].Id);

		HEP_Utility.fetchEmailRecipients(HEP_Utility.getConstantValue('HEP_BATCH_PHYSICAL'));
		HEP_Utility.fetchEmailRecipients('Digital');

		String sPO = '{ "PurchaseOrders": [ { "OPEN": "0.00", "RUSHCODE": "", "POLINE#": "2000", "NAME": "ANDERSON MERCHANDISERS", "PO_#": "339719", "COUNTRY": "US", "OBJECT": "63205", "ITEM_AMT": "3500.00", "LINE_DESC": "SPONSORSHIP FEE NM 10/23-26", "PROMO": "69789", "ACTUAL": "3500.00", "REQ_DATE": "08/29/2017", "POSTATUS": "400", "NETWORKID": "DIANEL", "RUSHGPCODE": "", "FORMAT": "915", "REQUESTOR": "DIANE LARSON", "VENDOR": "104286" }, { "OPEN": "0.00", "RUSHCODE": "", "POLINE#": "1000", "NAME": "EDELMANN USA INC", "PO_#": "339853", "COUNTRY": "US", "OBJECT": "51430", "ITEM_AMT": "32434.70", "LINE_DESC": "2345901 BOX", "PROMO": "69789", "ACTUAL": "32434.70", "REQ_DATE": "09/20/2017", "POSTATUS": "400", "NETWORKID": null, "RUSHGPCODE": "", "FORMAT": "915", "REQUESTOR": null, "VENDOR": "116481" }, { "OPEN": "0.00", "RUSHCODE": "", "POLINE#": "1000", "NAME": "EDELMANN USA INC", "PO_#": "339871", "COUNTRY": "US", "OBJECT": "51430", "ITEM_AMT": "70359.60", "LINE_DESC": "2344187 BOX", "PROMO": "69789", "ACTUAL": "70359.60", "REQ_DATE": "09/22/2017", "POSTATUS": "400", "NETWORKID": null, "RUSHGPCODE": "", "FORMAT": "915", "REQUESTOR": null, "VENDOR": "116481" }, { "OPEN": "0.00", "RUSHCODE": "", "POLINE#": "2000", "NAME": "EDELMANN USA INC", "PO_#": "339871", "COUNTRY": "US", "OBJECT": "51430", "ITEM_AMT": "14925.00", "LINE_DESC": "2344166 BOX", "PROMO": "69789", "ACTUAL": "14925.00", "REQ_DATE": "09/22/2017", "POSTATUS": "400", "NETWORKID": null, "RUSHGPCODE": "", "FORMAT": "934", "REQUESTOR": null, "VENDOR": "116481" } ] }';
		String sInvoice = '{ "Invoices": [ { "GL_LINE#": "145.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "2026.85", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "291.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "422.68", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "145.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-2026.85", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "291.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-422.68", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "152.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "2198.42", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "322.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "423.51", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "152.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-2198.42", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "322.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-423.51", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "146.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "2791.12", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "407.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "572.58", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "146.0", "ACTUAL_TYPE": "JE", "GL_PERIOD": "11", "NAME": "", "ACCOUNT#": "48795925", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-2791.12", "LINE_DESCRIPTION": "", "INV_DATE": "04/20/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "642.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48956356", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "987.30", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "642.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48956356", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-987.30", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "668.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48956356", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "911.60", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "668.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48956356", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-911.60", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "652.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48956356", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "931.30", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1154.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48795931", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "3581.92", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1154.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795931", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-3581.92", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1291.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795931", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "4285.16", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1291.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795931", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-4285.16", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1243.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795931", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "4150.06", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "290.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48795966", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "179.00", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "290.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795966", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-179.00", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "321.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795966", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "168.66", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "321.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795966", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-168.66", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "406.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795966", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "53202", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "268.32", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "641.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48956429", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "418.10", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "641.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48956429", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-418.10", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "667.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48956429", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "363.06", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "667.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48956429", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-363.06", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "651.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48956429", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "62104", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "436.43", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1153.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "8", "NAME": "", "ACCOUNT#": "48795969", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "1516.91", "LINE_DESCRIPTION": "", "INV_DATE": "02/15/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1153.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795969", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-1516.91", "LINE_DESCRIPTION": "", "INV_DATE": "02/16/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1290.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "9", "NAME": "", "ACCOUNT#": "48795969", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "1706.64", "LINE_DESCRIPTION": "", "INV_DATE": "03/22/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1290.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795969", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "-1706.64", "LINE_DESCRIPTION": "", "INV_DATE": "03/23/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "1242.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48795969", "INVOICE#": "", "COUNTRY": "US", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "1944.86", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "658.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48796049", "INVOICE#": "", "COUNTRY": "CA", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "105.50", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "910", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" }, { "GL_LINE#": "657.0", "ACTUAL_TYPE": "NT", "GL_PERIOD": "10", "NAME": "", "ACCOUNT#": "48796008", "INVOICE#": "", "COUNTRY": "CA", "INVOICE_STATUS": null, "OBJECT": "63205", "FISCAL_YEAR": "17", "PROMO": "W2486", "ACTUAL": "93.02", "LINE_DESCRIPTION": "", "INV_DATE": "04/19/2018", "FORMAT": "915", "REVERSE": "R", "PO#": "", "POTYPE": "", "VENDOR": "0" } ] }';

		HEP_Utility.processNTSPOInvoice(sPO , sInvoice , lstPromotions[1].Id);

		HEP_Utility.getDateValue('1990/12/12');

		HEP_Utility.getActualEmailIds(new List<String>{'testuser1@deloitte.com' , 'testUser2@deloitte.com'});

		HEP_Utility.getUserRole(UserInfo.getUserId() , lstTerritory[0].Id);
		HEP_Utility.getUserRole(UserInfo.getUserId() , lstTerritory[3].Id);

		HEP_Utility.getAllCreateCatalogErrorMessages();
		HEP_Utility.fetchTerritorysForPage('HEP_Create_National_Promotion');

		List<String> lstEmails = new List<String>{'testuser1@deloitte.com','testUser2@deloitte.com'};
		List<EmailTemplate> lstTemplate = [SELECT Id, Name FROM EmailTemplate WHERE Name LIKE '%HEP%'];
		Map<Id , Contact> mapContact = new Map<Id , Contact>(lstContact);
		List<Id> lstIds = new List<Id>(mapContact.keySet());
		HEP_Utility.createSingleEmailMessage(lstEmails,lstPromotions[0].Id , lstTemplate[0].Id , lstIds);

		List<String> lstTypes = new List<String>{'FOX_LICENSOR_TYPE'};
		HEP_Utility.getLOVMapping(lstTypes , 'Name' , 'Values__c');
		HEP_Utility.cronExpressionWithOneMinDelay();

		HEP_Utility.getInactiveSelectOptions(true);
		HEP_Utility.getInactiveSelectOptions(false);

		String sPO1 = '{\"purchaseOrders\":[{\"OPEN\":\"1000.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019781\",\"COUNTRY\":\"\",\"OBJECT\":\"629701\",\"ITEM_AMT\":\"1000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"0.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Open\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"},{\"OPEN\":\"0.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019782\",\"COUNTRY\":\"\",\"OBJECT\":\"611700\",\"ITEM_AMT\":\"20000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"20000.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Closed\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"},{\"OPEN\":\"30000.00\",\"RUSHCODE\":\"\",\"POLINE#\":\"1.000\",\"NAME\":\"ABCreativ Service\",\"PO_#\":\"1019783\",\"COUNTRY\":\"\",\"OBJECT\":\"630122\",\"ITEM_AMT\":\"30000.00\",\"LINE_DESC\":\"3RD Party Promo Satz/Rein\",\"PROMO\":\"90722\",\"ACTUAL\":\"0.00\",\"REQ_DATE\":\"07/19/2018\",\"POSTATUS\":\"Open\",\"NETWORKID\":\"SANTOSHK\",\"RUSHGPCODE\":\"\",\"CURRENCY\":\"EUR\",\"FORMAT\":\"DVD\",\"REQUESTOR\":\"Krishnamoorthy, Santosh\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"}]}';
		String sInv1 = '{\"invoices\":[{\"GL_LINE#\":\"1.0\",\"ACTUAL_TYPE\":\"PO\",\"NAME\":\"ABCreativ Service\",\"INVOICE#\":\"TEST5637\",\"COUNTRY\":\"\",\"INVOICE_STATUS\":\"Approved for Payment\",\"OBJECT\":\"611700\",\"PROMO\":\"90722\",\"ACTUAL\":\"20000.00\",\"LINE_DESCRIPTION\":\"3RD Party Promo Satz/Rein\",\"CURRENCY\":\"EUR\",\"INV_DATE\":\"07/19/2018\",\"FORMAT\":\"DVD\",\"PO#\":\"1019782\",\"POTYPE\":\"OH\",\"VENDOR\":\"104053\"}]}';

		HEP_Utility.processPOInvoice(sPO1 , sinv1 , lstPromotions[0].Id);

		HEP_Utility.SecurityWrapper objSecWrap = new HEP_Utility.SecurityWrapper();
		new HEP_Utility.SecurityWrapper(1, 'test' , 'test' , true);
		new HEP_Utility.ErrorMessageWrapper('test','test');
		new HEP_Utility.CustomPermissionWrapper('test','test');
		new HEP_Utility.CustomPermissionAssignmentWrapper(new Map<String , String>{'test'=>'test'} , new Set<String>{'test'});
		new HEP_Utility.PicklistFinancialWrapper('test','test','test');
		HEP_Utility.PicklistDetails objpicklistDetails = new HEP_Utility.PicklistDetails();
		objpicklistDetails.sActive = 'test';
		objpicklistDetails.sDefaultValue = 'test';
		objpicklistDetails.sLabel = 'test';
		objpicklistDetails.sValue = 'test';
		objpicklistDetails.sValidFor = 'test';
		new HEP_Utility.MultiPicklistWrapper('test','test',true);
		new HEP_Utility.MultiPicklistOrderWrapper('test','test',true,1);
		new HEP_Utility.MultiPicklistOrderWrapper();

		Map<String , Date> mapDates = new Map<String , Date>();
		mapDates.put('PVOD Release Date',Date.today());
		mapDates.put('EST Release Date' ,Date.today());
		mapDates.put('VOD Release Date', Date.today());
		mapDates.put('Physical Release Date (Retail)', Date.today());
		mapDates.put('Physical Release Date (Kiosk)',Date.today());
		mapDates.put('Physical Release Date (Rental)',Date.today());
		mapDates.put('SPEST Release Date',Date.today());
		mapDates.put('PEST Release Date',Date.today());
		mapDates.put('SPVOD Release Date',Date.today());
		mapDates.put('EST Avail Date',Date.today());
		mapDates.put('VOD Avail Date',Date.today());
		mapDates.put('SVOD Release Date',Date.today());

		Map<String , Date> mapDates1 = new Map<String , Date>();
		mapDates.put('PVOD Release Date',null);
		mapDates.put('EST Release Date' ,null);
		mapDates.put('VOD Release Date', null);
		mapDates.put('Physical Release Date (Retail)', null);
		mapDates.put('Physical Release Date (Kiosk)',null);
		mapDates.put('Physical Release Date (Rental)',null);
		mapDates.put('SPEST Release Date',null);
		mapDates.put('PEST Release Date',null);
		mapDates.put('SPVOD Release Date',null);
		mapDates.put('EST Avail Date',null);
		mapDates.put('VOD Avail Date',null);
		mapDates.put('SVOD Release Date',null);

		Set<Id> setRegions = new Set<Id>{lstPromotions[0].Territory__c};
		HEP_Utility.createDatingRecords(lstPromotions[0].Id , lstPromotions[0].Territory__c , mapDates , mapDates , setRegions);
		HEP_Utility.createDatingRecords(lstPromotions[0].Id , lstPromotions[0].Territory__c , mapDates , mapDates , null);

		lstPromotions[0].Digital_Physical__c = 'Both';
		update lstPromotions[0];
		HEP_Utility.createDatingRecords(lstPromotions[0].Id , lstPromotions[0].Territory__c , mapDates , mapDates , setRegions);
		HEP_Utility.createDatingRecords(lstPromotions[0].Id , lstPromotions[0].Territory__c , mapDates , mapDates , null);
		
		HEP_Utility.getAllValidGlobalDateTypes(lstPromotions[0].Id);
	}

	@isTest static void testGetInactiveSelectOptions() {
        HEP_Test_Data_Setup_Utility.createHEPListOfValues();
             
        // Creating territories
        HEP_Territory__c objGermanyTerritory  = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        objGermanyTerritory.SKU_Territory__c = true;
        insert objGermanyTerritory;
             
        // Creating price grades
        list<HEP_Price_Grade__c> lstPriceGrades = new list<HEP_Price_Grade__c>(); 
        HEP_Price_Grade__c objMashPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGermanyTerritory.Id, '12-11', 11, false);
        objMashPriceGrade.Channel__c = 'RENTAL';
        objMashPriceGrade.Format__c = 'DVD';
        objMashPriceGrade.Type__c = 'MASH';
        objMashPriceGrade.Record_Status__c = 'Active';
        
        lstPriceGrades.add(objMashPriceGrade);
        
        HEP_Price_Grade__c objMDP_ESTPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGermanyTerritory.Id, '12-11', 11, false);
        objMDP_ESTPriceGrade.Channel__c = 'EST';
        objMDP_ESTPriceGrade.Format__c = 'SD';
        objMDP_ESTPriceGrade.MM_RateCard_ID__c = '49';
        objMDP_ESTPriceGrade.Type__c = 'MDP_EST';
        objMDP_ESTPriceGrade.Record_Status__c = 'Inactive';
        
        lstPriceGrades.add(objMDP_ESTPriceGrade);
        insert lstPriceGrades;
        
        HEP_Utility.getInactiveSelectOptions(true);
        HEP_Utility.getInactiveSelectOptions(false);
    }
}