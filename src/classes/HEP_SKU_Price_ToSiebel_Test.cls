@isTest
private class HEP_SKU_Price_ToSiebel_Test{
     
    @testsetup
    static void HEP_CreateData() {
    
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_List_Of_Values__c> objHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        insert objTitle;
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',false);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;    
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',false);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;  
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id,false);
        objPromotionRegion.Unique_Id__c = '123456';
        insert objPromotionRegion;    
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id,false);    
        insert objDatingMatrix;
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id,false);      
        objDatingRecord.Status__c = 'Confirmed';
        insert objDatingRecord;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','The Simpsons', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        objCatalog.Catalog_Type__c = 'Bundle'; 
        insert objCatalog;
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple iTunes',objTerritory.id,'1111','MDP Customers',NUll,False);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;       
        HEP_MDP_Product_Mapping__c objPromotionProductMapping = HEP_Test_Data_Setup_Utility.createPromotionProductMapping(objCatalog.Id,objTerritory.Id,objCustomer.id,objTitle.id,false);
        objPromotionProductMapping.Adam_Id__c = '756481';
        objPromotionProductMapping.Local_Description__c = '756481';
        insert objPromotionProductMapping;
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;    
        HEP_SKU_Template__c objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU TEMP',objTerritory.id,objLOB.id,true);
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, objSKUTemp.id, true);
        HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objTerritory.Id, 'SKUTEST', 'KINGSMAN BD', 'Master', 'Physical', 'FOX', 'HD', true);
        HEP_Promotion_SKU__c objPromotionSku = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat.id, objSKU.id, 'Pending', 'Locked',true);
        HEP_Approvals__c  objHEPApprvals = HEP_Test_Data_Setup_Utility.createApproval('Pending', null, objPromotionSku.id , true);
        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objHEPApprvals.id, u.id, objRole.id, 'Pending',true);

        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.id, '345', 768.00, null);
        insert objPriceGrade;

        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKU.Id, objTerritory.Id, objPromotionSku.Id, objDatingRecord.Id, 'Active', null);
        objSKUPrice.Unit__c = 30;
        insert objSKUPrice;
    
        
    } 
    
    /**
    * HEP_RatingDetails_SuccessTest --  Test method for the Successful response
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_SKU_SiebelUpdate_SuccessTest() {
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Siebel_SkuPrice','HEP_SKU_Price_ToSiebel',true,10,10,'Outbound-Push',true,true,false);
        objInterface.Record_Status__c = 'Active';
        insert objInterface;

        HEP_SKU_Price__c objSKUPrice = [SELECT Id FROM HEP_SKU_Price__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(objSKUPrice.Id,'','HEP_Siebel_SkuPrice');
        System.assertEquals('HEP_Siebel_SkuPrice',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
}