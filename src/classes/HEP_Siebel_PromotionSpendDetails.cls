/**
* HEP_Siebel_CatalogDetails -- Controller to make the callout to Siebel interface for inserting/updating/deleting the Catalog details
* @author  Ashutosh Arora
*/
public class HEP_Siebel_PromotionSpendDetails implements HEP_IntegrationInterface{
    public static string sHEP_SIEBEL_OAUTH;
    public static string sHEP_TOKEN_BEARER;
    public static string sHEP_SIEBEL_SERVICE;
    public static string sHEP_STATUS_OK;
    public static string sHEP_STATUS_ACCEPTED;
    public static string sHEP_Int_Txn_Response_Status_Success;
    public static string sHEP_FAILURE;
    public static string sHEP_INVALID_TOKEN;
    public static string sHEP_SIEBEL_PRODUCERID;
    public static string sHEP_SIEBEL_EVENTTYPE;
    public static string sHEP_SIEBEL_EVENTNAME;
    public static string sHEP_SIEBEL_PROMOTION_SPEND_VALUE;
    static{
        sHEP_SIEBEL_OAUTH = HEP_Utility.getConstantValue('HEP_SIEBEL_OAUTH');
        sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        sHEP_SIEBEL_SERVICE = HEP_Utility.getConstantValue('HEP_SIEBEL_SERVICE');
        sHEP_STATUS_OK = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        sHEP_SIEBEL_PRODUCERID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
        sHEP_SIEBEL_EVENTTYPE = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
        sHEP_SIEBEL_EVENTNAME = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        sHEP_SIEBEL_PROMOTION_SPEND_VALUE = HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION_SPEND_VALUE');
    }
 
/**
* performTransaction -- Method to store Spend details 
* @param  Takes HEP_InterfaceTxnResponse wrapper as input
* @return no return value 
* @author Ashutosh Arora    
*/
public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
    System.debug('objTxnResponse : ' +objTxnResponse);
    System.debug('sHEP_TOKEN_BEARER : ' +sHEP_TOKEN_BEARER);
    String sParameters = '';
    String sAccessToken;
    String sPromotionDetailsJson;
        System.debug('****1');
        if(sHEP_SIEBEL_OAUTH != null)   
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(sHEP_SIEBEL_OAUTH);
        System.debug('sAccessToken :'+sAccessToken);

        if(sAccessToken != null 
            && sHEP_TOKEN_BEARER != null            
            && sAccessToken.startsWith(sHEP_TOKEN_BEARER) 
            && objTxnResponse != null){
            String sPromotionSpendWrapper;
            if(objTxnResponse.sSourceId != null){
                sPromotionSpendWrapper = getRequestBody((List<Id>)JSON.deserialize(objTxnResponse.sSourceId,list<Id>.class));
                System.debug('request body :' + sPromotionSpendWrapper);
            }
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for Authentication
            if(sHEP_SIEBEL_SERVICE != null) 
                objServiceDetails = HEP_Services__c.getInstance(sHEP_SIEBEL_SERVICE);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c);
                objHttpRequest.setMethod('POST');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setHeader('Accept','application/json');
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objHttpRequest.setBody(sPromotionSpendWrapper);
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                System.debug('objTxnResponse.sResponse****' +objTxnResponse.sResponse);
                System.debug('objHttpResp.getBody() :' + objHttpResp.getBody()); 
                if(sHEP_STATUS_OK != null && sHEP_STATUS_ACCEPTED != null){
                    if(objHttpResp.getStatus().equals(sHEP_STATUS_OK)  || objHttpResp.getStatus().equals(sHEP_STATUS_ACCEPTED)){
                        if(sHEP_Int_Txn_Response_Status_Success != null)         
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                    }
                    else{
                        objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
    
                    }
                }    
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null ){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }  

}

/**
* getRequestBody -- gets the details from Market Spend Detail Object based on input paramters
* @param  Takes List of Id's
* @return Serialised HEP_Siebel_PromotionSpendWrapper wrapper   
* @author Ashutosh Arora    
*/
private String getRequestBody(List<ID> promotionSpendIds){
    HEP_Siebel_PromotionSpendWrapper objPromotionSpendWrapper = new HEP_Siebel_PromotionSpendWrapper();
    List<HEP_Siebel_PromotionSpendWrapper.Items> lstTempItems = new List<HEP_Siebel_PromotionSpendWrapper.Items>();
    objPromotionSpendWrapper.ProducerID = sHEP_SIEBEL_PRODUCERID;
    objPromotionSpendWrapper.EventType = sHEP_SIEBEL_EVENTTYPE;
    objPromotionSpendWrapper.EventName = sHEP_SIEBEL_EVENTNAME;
    for (HEP_Market_Spend_Detail__c objpromoSpend: [SELECT 
                      Id,CreatedDate,CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Record_Status__c,HEP_Market_Spend__r.Promotion__r.Territory__r.Id,HEP_Market_Spend__r.Promotion__r.Territory__r.Name,HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c,Department__c,
                      RequestAmount__c,GL_Account__r.Sphere_Group__c,Budget__c,GL_Account__r.E1_Group__c,GL_Account__r.JDE_Group__c,HEP_Market_Spend__r.Promotion__r.PromotionName__c
                      FROM
                        HEP_Market_Spend_Detail__c
                      WHERE 
                        Id IN :promotionSpendIds]) {
            //HEP_Siebel_PromotionSpendWrapper objpromoSpendWrapper = new HEP_Siebel_PromotionSpendWrapper();
            HEP_Siebel_PromotionSpendWrapper.Items objpromoSpendItem = new HEP_Siebel_PromotionSpendWrapper.Items();
            if(sHEP_SIEBEL_PROMOTION_SPEND_VALUE != null)  
            objpromoSpendItem.objectType = sHEP_SIEBEL_PROMOTION_SPEND_VALUE;
            if(objpromoSpend.CreatedDate != null )objpromoSpendItem.createdDate = String.ValueOf(objpromoSpend.CreatedDate);
            if(objpromoSpend.CreatedBy.Name != null ) objpromoSpendItem.createdBy = objpromoSpend.CreatedBy.Name;    
            if(objpromoSpend.LastModifiedDate != null )objpromoSpendItem.updatedDate = String.ValueOf(objpromoSpend.LastModifiedDate);
            if(objpromoSpend.LastModifiedBy.Name != null) objpromoSpendItem.updatedBy= objpromoSpend.LastModifiedBy.Name;    
            if(objpromoSpend.Id != null) objpromoSpendItem.recordId = objpromoSpend.Id; 
            if(objpromoSpend.Record_Status__c != null) objpromoSpendItem.recordStatus= objpromoSpend.Record_Status__c; 
            if(objpromoSpend.HEP_Market_Spend__r.Promotion__r.Territory__r.Id != null) objpromoSpendItem.territoryId = objpromoSpend.HEP_Market_Spend__r.Promotion__r.Territory__r.Id;
            if(objpromoSpend.HEP_Market_Spend__r.Promotion__r.Territory__r.Name != null)objpromoSpendItem.territoryName = objpromoSpend.HEP_Market_Spend__r.Promotion__r.Territory__r.Name;
            if(objpromoSpend.HEP_Market_Spend__r.Promotion__r.Id != null)objpromoSpendItem.promotionId = objpromoSpend.HEP_Market_Spend__r.Promotion__r.Id;   
            if(objpromoSpend.HEP_Market_Spend__r.Promotion__r.PromotionName__c != null)objpromoSpendItem.promotionName = objpromoSpend.HEP_Market_Spend__r.Promotion__r.PromotionName__c;  
            if(objpromoSpend.HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c != null) objpromoSpendItem.promotionCode = objpromoSpend.HEP_Market_Spend__r.Promotion__r.LocalPromotionCode__c;  
            if(objpromoSpend.Department__c != null) objpromoSpendItem.department = objpromoSpend.Department__c;objpromoSpendItem.amount = objpromoSpend.Budget__c;  
            if(objpromoSpend.GL_Account__r.Sphere_Group__c != null) objpromoSpendItem.sphereGroup = objpromoSpend.GL_Account__r.Sphere_Group__c; 
            if(objpromoSpend.GL_Account__r.E1_Group__c != null) objpromoSpendItem.e1Group = objpromoSpend.GL_Account__r.E1_Group__c; 
            if(objpromoSpend.GL_Account__r.JDE_Group__c != null) objpromoSpendItem.jdeGroup = objpromoSpend.GL_Account__r.JDE_Group__c;
            lstTempItems.add(objpromoSpendItem);
        }
        objPromotionSpendWrapper.Data.Payload.items.addAll(lstTempItems);  
        System.debug('objPromotionSpendWrapper****' + objPromotionSpendWrapper);
        return JSON.serialize(objPromotionSpendWrapper);       
}


}