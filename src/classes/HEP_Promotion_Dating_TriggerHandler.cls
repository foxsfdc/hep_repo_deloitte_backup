/**
 *HEP_Promotion_Dating_TriggerHandler --- Promotion Dating Object trigger Handler
 *@author  Gaurav Mehrishi
 */
public class HEP_Promotion_Dating_TriggerHandler extends TriggerHandler {

    public static List < HEP_Promotion_Dating__c > lstConfirmHEPDating;
    public static List < HEP_Promotion_Dating__c > lstReConfirmHEPDating;
    public static List < Id > lstApprovers;
    list<Id> lstPromotionDatingIds;
    public Set<String> territorySet = new Set<String>{'US', 'Canada', 'DHE'}; 
    public static String sACTIVE;
    static {
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    /**
     *Class Constructor to initialise class variables
     *@return nothing
     */

    public HEP_Promotion_Dating_TriggerHandler() {
        lstConfirmHEPDating = new List < HEP_Promotion_Dating__c > ();
        lstReConfirmHEPDating = new List < HEP_Promotion_Dating__c > ();
        lstApprovers = new List < Id > ();
        lstPromotionDatingIds = new list<Id>();
    }

    /**
     * afterInsert -- Context specific afterinsert method
     * @return nothing
     */

    public override void afterInsert() {
        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
            callQueuableMethod(Trigger.newMap.keyset());
            Set<String> promoIds = new Set<String>();
            //Avinash : Outbound callout to JDE
            for(HEP_Promotion_Dating__c promoDating : (List<HEP_Promotion_Dating__c>)Trigger.new){
                promoIds.add(promoDating.Promotion__c);
            }
            if(!promoIds.isEmpty() && !HEP_Utility.isJDETrigger){
                for(HEP_Promotion__c promo : [Select Id from HEP_Promotion__c Where Id IN :promoIds AND Territory__r.Name IN :territorySet] ){
                    System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(promo.Id));
                    HEP_Utility.isJDETrigger = true;
                }
            }
        }
    }

    /**
     * afterUpdate -- Context specific afterUpdate method
     * @return nothing
     */

    public override void afterUpdate() {
        handleAfterUpsert((List < HEP_Promotion_Dating__c > ) Trigger.new);
        stampDatingRecordsAfterUpdate((List < HEP_Promotion_Dating__c > ) Trigger.new, (Map<Id,HEP_Promotion_Dating__c>) Trigger.oldMap);  
        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){      
            Set<String> promoIds = new Set<String>();       
            Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Promotion_Dating__c', 'Promotion_Dating_Field_Set');
            System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate); 
    
            if(checkFieldUpdate)
                callQueuableMethod(Trigger.newMap.keyset()); 
        
            //JDE Outbound Integration
            Boolean checkFieldUpdateForJDE = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Promotion_Dating__c', 'Promotion_Dating_JDE_Fields');
            System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdateForJDE);
            if(checkFieldUpdateForJDE){
                for(HEP_Promotion_Dating__c promoDating : (List<HEP_Promotion_Dating__c>)Trigger.new){
                    promoIds.add(promoDating.Promotion__c);
                }
                for(HEP_Promotion__c promo : [Select Id from HEP_Promotion__c Where Id IN :promoIds AND Territory__r.Name IN :territorySet] ){
                    System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(promo.Id));
                }
            }
        }
    }

       
    /**
     * beforeUpdate -- Context specific beforeUpdate method
     * @return nothing
     */

    public override void beforeUpdate() {
       handleBeforeUpsert((List < HEP_Promotion_Dating__c > ) Trigger.new, (Map<Id,HEP_Promotion_Dating__c>) Trigger.oldMap);
    }
    
    /**
     * handleAfterUpsert -- Create notification when dating record is Confirmed or Reconfirm.
     * @param lstHepDating list of dating records.
     * @return nothing
     */

    //Dating record is confirmed/Reconfirmed
    public static void handleAfterUpsert(List < HEP_Promotion_Dating__c > lstHepDating) {
        System.debug('Intrigger+++++++++++');
        lstConfirmHEPDating = new List < HEP_Promotion_Dating__c > ();
        lstReConfirmHEPDating = new List < HEP_Promotion_Dating__c > ();
        lstApprovers = new List < Id > ();
        for (HEP_Promotion_Dating__c objHPD: lstHepDating) {
            
            if (String.isNotBlank(objHPD.Status__c) && objHPD.Status__c.equals(HEP_Utility.getConstantValue('HEP_STATUS_CONFIRMED'))) {
                lstConfirmHEPDating.add(objHPD);
            } else if (String.isNotBlank(objHPD.Status__c) && objHPD.Status__c.equals(HEP_Utility.getConstantValue('HEP_STATUS_RECONFIRM'))) {
                lstReConfirmHEPDating.add(objHPD);
            }
        }
        System.debug('lstConfirmHEPDating++++++++++' + lstConfirmHEPDating);
        if (lstConfirmHEPDating.size() > 0) {
            
            HEP_Notification_Utility.CreateNotifications(lstConfirmHEPDating,HEP_Utility.getConstantValue('HEP_NOTIFICATION_CONFIRMED'),HEP_Utility.getConstantValue('HEP_PROMOTION_DATING'));

        }
        /*if (lstReConfirmHEPDating.size() > 0) {
            
            HEP_Notification_Utility.CreateNotifications(lstReConfirmHEPDating,HEP_Utility.getConstantValue('HEP_NOTIFICATION_RECONFIRMED'),HEP_Utility.getConstantValue('HEP_PROMOTION_DATING'));
        }*/
    }
    
    public static void handleBeforeUpsert(List < HEP_Promotion_Dating__c > lstDatingRecords , Map<Id,HEP_Promotion_Dating__c> mapOldDatingRecords){
        for(HEP_Promotion_Dating__c objPromotionDating : lstDatingRecords){
            HEP_Promotion_Dating__c oldDatingRecord = mapOldDatingRecords.get(objPromotionDating.Id);
            //To change the status to re-confirm when the record's unlock request is approved
            if(objPromotionDating.Locked_Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATING_UNLOCKED')) && (objPromotionDating.Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATUS_CONFIRMED')) || objPromotionDating.Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATUS_NOT_RELEASED')) )){
                objPromotionDating.Status__c = HEP_Utility.getConstantValue('HEP_STATUS_RECONFIRM');
            }

            //To add the current date field value to previous date field while update of date field
            if(objPromotionDating.Date__c != null && objPromotionDating.Date__c != oldDatingRecord.Date__c){
                objPromotionDating.Previous_Date__c = oldDatingRecord.Date__c;
            }
        }
        
    }
    /**
     * stampDatingRecordsAfterUpdate -- Update dating records in SKU price region records after change in Record Status , FAD Approval Status and Date.
     * @param lstDatingRecords list of dating records.
     * @param mapOldDatingRecords map of old dating records
     * @return nothing
     */

    public static void stampDatingRecordsAfterUpdate(List < HEP_Promotion_Dating__c > lstDatingRecords , Map<Id,HEP_Promotion_Dating__c> mapOldDatingRecords) {
        System.debug('Intrigger-->');
        System.debug('list of dating records---->' + lstDatingRecords);
        System.debug('map of old dating records---->' + mapOldDatingRecords);
        List < HEP_SKU_Utility.SKUWrapper > lstPriceGradeUpdate = new List < HEP_SKU_Utility.SKUWrapper > ();       
        Set<Id> setPromotionDatingIds = new Set<Id>();
        Set<Id> setPromotionIds = new Set<Id>();
        Set<Id> setPriceRegionIds = new Set<Id>();
        for(HEP_Promotion_Dating__c objPromotionDating : lstDatingRecords){
            HEP_Promotion_Dating__c oldDatingRecord = mapOldDatingRecords.get(objPromotionDating.Id);
            if((objPromotionDating.Date__c != oldDatingRecord.Date__c) || (objPromotionDating.Record_Status__c != null && objPromotionDating.Record_Status__c != oldDatingRecord.Record_Status__c) || (objPromotionDating.HEP_Catalog__c != oldDatingRecord.HEP_Catalog__c)){
                setPromotionDatingIds.add(objPromotionDating.id);
                setPromotionIds.add(objPromotionDating.Promotion__c);
            }
        }
        system.debug('Dating Records Updated ---->' + setPromotionDatingIds);
        system.debug('Promotion Records tagged to updated dating records---->' + setPromotionIds);
        for(HEP_Promotion_SKU__c objPrSKU : [SELECT Id,                                                                          
                        Record_Status__c,
                        (Select Id,Record_Status__c,Promotion_SKU__c from HEP_SKU_Region_Prices__r)
                        FROM HEP_Promotion_SKU__c
                        WHERE Promotion_Catalog__r.Promotion__c IN: setPromotionIds
                        AND Record_Status__c =: sACTIVE
                        ]){
                            for (HEP_SKU_Price__c objSKUPrice : objPrSKU.HEP_SKU_Region_Prices__r){
                                if(objSKUPrice.Record_Status__c.equalsIgnoreCase(sACTIVE)){
                                    setPriceRegionIds.add(objSKUPrice.id);                                    
                                } 
                                
                            }                                                       
                        }
        system.debug('Price Region Records ---->' + setPriceRegionIds);             
        if (setPriceRegionIds != null && !setPriceRegionIds.isEmpty()) {
            for (HEP_SKU_Price__c objSkuPriceRecord: [SELECT Id, PriceGrade__c, Promotion_SKU__c,Promotion_Dating__c, Promotion_SKU__r.SKU_Master__c, Promotion_SKU__r.Promotion_Catalog__r.Catalog__r.CatalogId__c,Promotion_SKU__r.HEP_Promotion__c FROM HEP_SKU_Price__c WHERE Id IN: setPriceRegionIds]) {
                HEP_SKU_Utility.SKUWrapper objSkuWrapper = new HEP_SKU_Utility.SKUWrapper();
                objSkuWrapper.sPromoId = objSkuPriceRecord.Promotion_SKU__r.HEP_Promotion__c;
                objSkuWrapper.sCatalogId = objSkuPriceRecord.Promotion_SKU__r.Promotion_Catalog__r.Catalog__r.CatalogId__c;
                objSkuWrapper.sSKUId = objSkuPriceRecord.Promotion_SKU__r.SKU_Master__c;
                objSkuWrapper.sPromotionSKUID = objSkuPriceRecord.Promotion_SKU__c;
                objSkuWrapper.sSKUPriceId = objSkuPriceRecord.Id;
                lstPriceGradeUpdate.add(objSkuWrapper);
            }
            if(lstPriceGradeUpdate != null && !lstPriceGradeUpdate.isEmpty()){
                System.debug('lstPriceGrade Records Tagged to Updated Dating Records ---->' + lstPriceGradeUpdate);         
                HEP_SKU_Utility.defaultSKUPriceRules(lstPriceGradeUpdate, HEP_Utility.getConstantValue('HEP_EDIT_DATING'));
            }
        }       
    }
    /**
    * callQueuableMethod --- Calls the queuable method
    * @param Map<Id, HEP_Promotion_Dating__c> mapPromoDatingNew
    * @exception Any exception
    * @return void
    */
    public static void callQueuableMethod(set<Id> setPromotionDatingIds){
        List<Id> lstPromotionDatingIds = new List<Id>();
        System.debug('in queuebale method'+setPromotionDatingIds);
        for(HEP_Promotion_Dating__c promo : [Select Id,Promotion__r.Promotion_Type__c from HEP_Promotion_Dating__c Where Id IN :setPromotionDatingIds AND (Promotion__r.Promotion_Type__c = :HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL') OR Promotion__r.Promotion_Type__c =: HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL'))] ){
            lstPromotionDatingIds.add(promo.Id);
        } 

        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable()) && !lstPromotionDatingIds.isEmpty() ){
            if(HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING') != null && HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c != null) 
                System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstPromotionDatingIds, ',') , HEP_Constants__c.getValues('HEP_SIEBEL_PROMOTIONDATING').Value__c));
        }
    }
}