/**
* HEP_NTS_DHE_PurchaseOrder_Details -- purchase Order details will be stored from NTS And E1 Interfaces 
* @author Ayan Sarkar
*/
public class HEP_NTS_DHE_PurchaseOrder_Details implements HEP_IntegrationInterface{ 
    /**
    * HEP_INT_NTSPurchaseOrder_Details -- Rating details to store Purchase details
    * @return  :  no return value
    * @author  :  Ayan Sarkar
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        String sHEP_CURRENCY = HEP_Utility.getConstantValue('HEP_CURRENCY');
        String sHEP_REPLACE_CURRENCY = HEP_Utility.getConstantValue('HEP_REPLACE_CURRENCY');
        String sHEP_OBJECT = HEP_Utility.getConstantValue('HEP_OBJECT');
        String sHEP_REPLACE_OBJECT_NO = HEP_Utility.getConstantValue('HEP_REPLACE_OBJECT_NO');
        String sHEP_PO = HEP_Utility.getConstantValue('HEP_PO');
        String sHEP_REPLACE_PO_NO = HEP_Utility.getConstantValue('HEP_REPLACE_PO_NO');
        String sHEP_POLine = HEP_Utility.getConstantValue('HEP_POLineNo');
        String sHEP_REPLACE_POLine_NO = HEP_Utility.getConstantValue('HEP_REPLACE_POLine_NO');
        
        HEP_NTSPurchaseOrderWrapper objWrapper = new HEP_NTSPurchaseOrderWrapper();         
        if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            String sFromDate;
        	String sToDate;
        	String sLegalTitle;
        	String sFinDivisionCode;
        	String sFoxId;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            //Gets the Territory and PromoId details to get the purchase order details through integration 
            //HEP_NTSInvoiceWrapper objNTSInvoiceWrapper = (HEP_NTSInvoiceWrapper)JSON.deserialize(sDetails,HEP_NTSInvoiceWrapper.class);
            HEP_NTSInvoiceParametersWrapper objNTSInvoiceParametersWrapper=(HEP_NTSInvoiceParametersWrapper)JSON.deserialize(sDetails,HEP_NTSInvoiceParametersWrapper.class);
                
            if(objNTSInvoiceParametersWrapper != null){
            /*sCatalog = objNTSInvoiceParametersWrapper.sCatalogNumber;
            sTerritory = objNTSInvoiceParametersWrapper.sTerritoryE1Code;
            sFrom_Date = objNTSInvoiceParametersWrapper.sFromDate;
            sTo_Date = objNTSInvoiceParametersWrapper.sToDate;*/

            sFromDate = objNTSInvoiceParametersWrapper.sFromDate;
            sToDate = objNTSInvoiceParametersWrapper.sToDate;
            sLegalTitle = objNTSInvoiceParametersWrapper.sLegalTitle;
            sFinDivisionCode = objNTSInvoiceParametersWrapper.sFinDivisionCode;
            sFoxId = objNTSInvoiceParametersWrapper.sFoxId;

            objHttpRequest.setMethod('GET');
            objHttpRequest.setHeader('Authorization',sAccessToken);
            objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
            objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
            if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_DHE_E1_PODETAILS') != null) 
                objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_NTS_DHE_E1_PODETAILS'));   
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?fromDate=' + sFromDate + '&toDate=' + sToDate+ '&legalTitle=' + sLegalTitle+ '&finDivisionCode=' + sFinDivisionCode+ '&foxId=' + sFoxId);
                //objHttpRequest.setEndpoint('https://feg-devapi.foxinc.com/v1/finance/read/api/dom/nts/pos?fromDate=90001&toDate=118081&legalTitle=69789&finDivisionCode=abc&foxId=6985148');
                system.debug('NTS DHE Purchase Order Endpoint ---->  ' + objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?fromDate=' + sFromDate + '&toDate=' + sToDate+ '&legalTitle=' + sLegalTitle+ '&finDivisionCode=' + sFinDivisionCode+ '&foxId=' + sFoxId);
                
                HTTP objHttp = new HTTP();                 
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                if(HEP_Constants__c.getValues('HEP_STATUS_OK') != null  && HEP_Utility.getConstantValue('HEP_STATUS_OK') != null
                    && objHttpResp.getStatus().equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    String sResponse = objHttpResp.getBody();
                    sResponse = sResponse.replace(sHEP_CURRENCY , sHEP_REPLACE_CURRENCY);
                    sResponse = sResponse.replace(sHEP_POLine , sHEP_REPLACE_POLine_NO);
                    sResponse = sResponse.replace(sHEP_OBJECT , sHEP_REPLACE_OBJECT_NO);
                    sResponse = sResponse.replace(sHEP_PO , sHEP_REPLACE_PO_NO);

                    System.debug('DHE NTS Purchade Order Response ---- > ' + sResponse);
                    //Replacing the tags in the response receiving as we are using the generic wrapper
                    objWrapper = (HEP_NTSPurchaseOrderWrapper)JSON.deserialize(sResponse, HEP_NTSPurchaseOrderWrapper.class);	
                    //objWrapper = (HEP_NTSPurchaseOrderWrapper)JSON.deserialize(objHttpResp.getBody(), HEP_NTSPurchaseOrderWrapper.class);
                     System.debug('DHE NTS Purchade Order Converted Response ----> ' + objWrapper);
                     if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                        System.debug(objWrapper.errors[0].detail);
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                        else
                            objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                        objTxnResponse.bRetry = true;
                    }else{
                        if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                        objTxnResponse.bRetry = false;
                    }
                }
                else{
                    if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                        objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }                
            }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }        
    }    
    
     /**
    * HEP_INT_NTSInvoiceWrapper  -- Class to hold Input parameter details to be sent to the NTS and E1
    * @author    Ayan Sarkar
     
public class HEP_NTSInvoiceWrapper {
    public String sCatalog;
    public String sTerritory;
    public String sFrom_Date;
    public String sTo_Date;
}*/
    /**
    * HEP_INT_NTSInvoiceWrapper  -- Class to hold Input parameter details to be sent to the NTS and PO
    * @author Ayan Sarkar
    */ 
    public class HEP_NTSInvoiceParametersWrapper {
        public String sFromDate;
        public String sToDate;
        public String sLegalTitle;
        public String sFinDivisionCode;
        public String sFoxId;
    }
}