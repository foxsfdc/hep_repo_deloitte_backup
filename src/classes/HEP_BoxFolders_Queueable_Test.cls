/**
* HEP_BoxFolders_Queueable_Test-- Test class for the HEP_BoxFolders_Queueable class for all Interfaces 
* @author    Rtiesh Konduru
*/
@isTest
private class HEP_BoxFolders_Queueable_Test{
    /**
    * HEP_BoxFolders_Queueable_Method --  Test method for the HEP_BoxFolders_Queueable
    * @return  :  no return value
    * @author  :  Rtiesh Konduru
    */
    @isTest static void HEP_BoxFolders_Queueable_Method(){
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.SKU_Interface__c = 'ESCO';
        objTerritory.Territory_Code__c = 'CH';
        objTerritory.DepartmentList__c = 'Dept';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        HEP_Services__c objServices1 = new HEP_Services__c(Name = 'HEP_INT_DheSkuCustomer_Details');
        HEP_Services__c objServices2 = new HEP_Services__c(Name = 'HEP_INT_DheSkuBom_Details');
        insert new List<HEP_Services__c>{objServices1 , objServices2};
        List<HEP_Territory__c> lstTerritoryRecords = new List<HEP_Territory__c>();
        lstTerritoryRecords.add(objTerritory);
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true);
        Map<String,String> mapStringofStrings = new Map<String,String> ();
        mapStringofStrings.put('General','type');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxFolders_Queueable objQueueable = new HEP_BoxFolders_Queueable(lstTerritoryRecords,mapStringofStrings,objPromotion, false);
        Test.startTest();        
        System.enqueueJob(objQueueable);
        System.assertEquals('Dept',objTerritory.DepartmentList__c);
        
        HEP_INT_DheSku_Queueable obj1 = new HEP_INT_DheSku_Queueable(new Map<String , String>{'HEP_INT_DheSkuCustomer_Details' => 'HEP_INT_DheSkuCustomer_Details' , 'HEP_INT_DheSkuBom_Details' => 'HEP_INT_DheSkuBom_Details'},
                                                                    new Map<String , HEP_INT_DheSkuInvoiceWrapperUtility>{'HEP_INT_DheSkuCustomer_Details' => new HEP_INT_DheSkuInvoiceWrapperUtility()},
                                                                    [SELECT ID FROM AsyncApexJob][0].Id);

        System.enqueueJob(obj1);        

        HEP_INT_DheSkuDetails_Scheduler obj = new HEP_INT_DheSkuDetails_Scheduler();        
        try{
          obj.execute(null);
        }catch(Exception e){}
        Test.stopTest();
    }
    /**
    * HEP_BoxFolders_Queueable_MethodTerritoryCode --  Test method for the HEP_BoxFolders_Queueable_Method with TerritoryCode World Wide
    * @return  :  no return value
    * @author  :  Rtiesh Konduru
    */
    @isTest static void HEP_BoxFolders_Queueable_MethodTerritoryCode(){
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.SKU_Interface__c = 'ESCO';
        objTerritory.Territory_Code__c = 'World Wide';
        objTerritory.DepartmentList__c = 'Dept';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_Territory__c> lstTerritoryRecords = new List<HEP_Territory__c>();
        lstTerritoryRecords.add(objTerritory);
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true); 
        Map<String,String> mapStringofStrings = new Map<String,String> ();
        mapStringofStrings.put('General','type');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxFolders_Queueable objQueueable = new HEP_BoxFolders_Queueable(lstTerritoryRecords,mapStringofStrings,objPromotion,true);
        Test.startTest();        
        System.enqueueJob(objQueueable);
        System.assertEquals('Dept',objTerritory.DepartmentList__c);
        Test.stopTest();
    }

    @isTest static void HEP_BoxFolders_Queueable_Method1(){
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.SKU_Interface__c = 'ESCO';
        objTerritory.Territory_Code__c = 'World Wide';
        objTerritory.DepartmentList__c = 'Dept';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        HEP_Services__c objServices1 = new HEP_Services__c(Name = 'HEP_INT_DheSkuCustomer_Details');
        HEP_Services__c objServices2 = new HEP_Services__c(Name = 'HEP_INT_DheSkuBom_Details');
        insert new List<HEP_Services__c>{objServices1 , objServices2};
        List<HEP_Territory__c> lstTerritoryRecords = new List<HEP_Territory__c>();
        lstTerritoryRecords.add(objTerritory);
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',false);
        objLOB.Record_Status__c = 'Active';
        insert objLOB;
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null,objTerritory.Id, objLOB.Id, null,null,true);
        Map<String,String> mapStringofStrings = new Map<String,String> ();
        mapStringofStrings.put('General','type');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_BoxServices objBox = new HEP_BoxServices();
        HEP_BoxFolders_Queueable objQueueable = new HEP_BoxFolders_Queueable(lstTerritoryRecords,mapStringofStrings,objPromotion, false);
        Test.startTest();        
        System.enqueueJob(objQueueable);
        Test.stopTest();
    }
}