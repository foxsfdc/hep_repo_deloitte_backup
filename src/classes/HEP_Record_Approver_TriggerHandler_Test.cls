/* 
 @HEP_ApprovalProcess_Test ---Test Class for Approval process.
 @author : Deloitte
 */

@isTest
public class HEP_Record_Approver_TriggerHandler_Test{
    
  @testSetup
  public static void createTestUserSetup(){
    HEP_ApprovalProcess_Test.createTestUserSetup();
  }

  static TestMethod void  testUtilityMethodsForStatic(){
    
    HEP_ApprovalProcess_Test.testDatingUnlockRequest();    
  }
  
  static TestMethod void  testUtilityMethodsForHierarchical(){
    
    HEP_ApprovalProcess_Test.testBudgetRequestApproved();
  }
  
  static TestMethod void  testUtilityMethodsForAutoUpdate(){
    
    HEP_ApprovalProcess_Test.testBudgetRequestAutoApproved();
  }
  
   static TestMethod void  testUtilityMethodsForRejected(){
    
    HEP_ApprovalProcess_Test.testBudgetRequestRejected();
  }   
}