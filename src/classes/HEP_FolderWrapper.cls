public with sharing class HEP_FolderWrapper {
	public class RequestFolder{
		public String name{get;set;}
		public ParentDetails parent {get;set;}
	}

	public class ParentDetails{
		public String id {get;set;}
	}

	public class FolderDetails{
		public String type{get;set;}
		public String id {Get;set;}
		public String name {Get;set;}
		public Context context_info{get;set;}
		public Shared_Link shared_link{get;set;}
        
	}
								
	public class  TokenWrapper{
        public String access_token {Get;set;}
        public String refresh_token{get;set;}
        public String expires_in {get;set;}
    }

    public class FolderItems{
    	public String total_count {get;set;}
    	public List<Entry> entries{get;set;}
    }

    public class Entry{
    	public String type {get;set;}
        public String id {get;set;}
        public String sequence_id {get;set;}
        public String etag {get;set;}
        public String name {get;set;}
        public String status{get;set;}
        public String code{get;set;}
    }

    public class Context{
    	public List<FolderDetails> conflicts;
        public List<Error> errors{get;set;}
    }

    public class Shared_Link{
    	public String url {get;set;}
    	public String download_url {get;set;}
    	public String access {get;set;}
    	public String is_password_enabled {get;set;}
    }

    public class Error{
        public String reason{get;set;}
        public String name{get;set;}
        public String message{get;set;}
    }
}