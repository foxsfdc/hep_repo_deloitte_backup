/**
* HEP_ProphetProjectedDateDetails -- Rating details to store Rating details 
* @author    Lakshman Jinnuri
*/
public class HEP_ProphetProjectedDateDetails implements HEP_IntegrationInterface{ 

    /**
    * HEP_RatingDetails -- Rating details to store Rating details
    * @return no return value 
    * @author  :  Lakshman Jinnuri    
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_Services__c objServiceDetails;
        HEP_ProphetProjectedDateWrapper objWrapper = new HEP_ProphetProjectedDateWrapper();
        //Variables to hold the custom Settings data
        String sHEP_PROPHET_OAUTH = HEP_Utility.getConstantValue('HEP_PROPHET_OAUTH');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_PROPHET_PROJECTED_DATE = HEP_Utility.getConstantValue('HEP_PROPHET_PROJECTED_DATE');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        if(HEP_Constants__c.getValues('HEP_PROPHET_OAUTH') != null && HEP_Constants__c.getValues('HEP_PROPHET_OAUTH').Value__c != null)      
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Constants__c.getValues('HEP_PROPHET_OAUTH').Value__c);
        
        if(sAccessToken != null && sHEP_TOKEN_BEARER != null && sAccessToken.startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            String sDetails = objTxnResponse.sSourceId;
            //List to hold the Financial title Id and territory Id to create the end point 
            list<String> lstFinTitleId;
            list<String> lstTerritoryId;
            HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objTerritoryFinTitleDetails;
            String sEndPoint;
            HTTPRequest objHttpRequest = new HTTPRequest();
            if(!String.isBlank(sDetails))
            objTerritoryFinTitleDetails = (HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails)JSON.deserialize(sDetails,HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails.class);
            System.debug('objTerritoryFinTitleDetails :'+objTerritoryFinTitleDetails);
            lstFinTitleId = objTerritoryFinTitleDetails.sFinTitleId;
            lstTerritoryId = objTerritoryFinTitleDetails.sTerritoryId;
            //Gets the Details from custom setting for Authentication
            if(sHEP_PROPHET_PROJECTED_DATE != null)
                objServiceDetails = HEP_Services__c.getInstance(sHEP_PROPHET_PROJECTED_DATE);
            if(objServiceDetails != null){
                //creates the end point URL if the financial title Id and territory Id lists is not empty
                if(lstFinTitleId != null && lstTerritoryId != null){
                    sEndPoint = objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?finTitleIds='; 
                    for(String sFinTitleId : lstFinTitleId){
                        if(sFinTitleId != null)
                        sEndPoint = sEndPoint + '\'' + sFinTitleId + '\'' +',';
                    }    
                    sEndPoint = sEndPoint.trim().removeEnd(',');
                    System.debug('lstTerritoryId.size()'+lstTerritoryId.size());
                    System.debug('lstTerritoryId'+lstTerritoryId);
                    if(lstTerritoryId.size()>0)
                        sEndPoint = sEndPoint + '&territoryIds=';
                    for(String sTerritoryId : lstTerritoryId){
                        if(sTerritoryId != null)
                        sEndPoint = sEndPoint + '\'' + sTerritoryId + '\'' +',';
                    }
                    sEndPoint = sEndPoint.trim().removeEnd(',');
                    //Set the end point created to the request to be sent to Prophet system
                    objHttpRequest.setEndpoint(sEndPoint);
                    objHttpRequest.setMethod('GET');
                    objHttpRequest.setHeader('Authorization',sAccessToken);
                    objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                    System.debug('Request is :' + objHttpRequest);
                    objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                    HTTP objHttp = new HTTP();
                    //Response from callout
                    HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                    objTxnResponse.sResponse = objHttpResp.getBody();   
                    if(sHEP_SUCCESS != null && objHttpResp.getStatus().equals(sHEP_SUCCESS)){
                        //Gets the details from Foxipedia interface and stores in the wrapper 
                        objWrapper = (HEP_ProphetProjectedDateWrapper)JSON.deserialize(objHttpResp.getBody(),HEP_ProphetProjectedDateWrapper.class);    
                        System.debug('objWrapper :' + objWrapper );
                        if(sHEP_FAILURE != null && objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                            System.debug(objWrapper.errors[0].detail);
                            objTxnResponse.sStatus = sHEP_FAILURE;
                            if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                            	objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                            else
                            	objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                            objTxnResponse.bRetry = true;
                        }else{
                            if(sHEP_Int_Txn_Response_Status_Success != null)  
                            objTxnResponse.sStatus = sHEP_Int_Txn_Response_Status_Success;
                            objTxnResponse.bRetry = false;                              
                        }
                    }
                    else{
                        if(sHEP_FAILURE != null)
                            objTxnResponse.sStatus = sHEP_FAILURE;
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                        else    
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    } 
                }   
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }  
    }
}