/**
* HEP_Security_Framework --- Class to handle the HEP application security
* @author    Sachin Agarwal
*/
public with sharing class HEP_Security_Framework {
    
    public static String sActive;
    public static String SReadPermission;
    public static String SWritePermission;
    public static String sBooleanValueTrue;
    public static String sBooleanValueFalse;
    public static String sTerritoryDatesTab;
    
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        SReadPermission = HEP_Utility.getConstantValue('HEP_PERMISSION_READ');
        SWritePermission = HEP_Utility.getConstantValue('HEP_PERMISSION_WRITE');
		sBooleanValueTrue = HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_TRUE');
		sBooleanValueFalse = HEP_Utility.getConstantValue('HEP_BOOLEAN_VALUE_FALSE');
		sTerritoryDatesTab = HEP_Utility.getConstantValue('HEP_TAB_NAME_TERRITORY_DATES');
    }
    
    
    /**
    * Tab --- Wrapper class for holding the data for a tab including the tab name, order of the tab and the 
    *         associated sub tabs if any.
    * @author    Sachin Agarwal
    */
    public class Tab{
        public String sTabName;
        public String sPageName;
        
        // List of all the child tabs under a given tab
        public list<Tab> lstSubTabs;
        public Integer iOrder;
    }
    
    
    /**
     * Helps in getting read and write access to a particular territory. In addition to that, it returns a list of tabs under a header page along with the access
     * @param  sTerritoryId id of the territory for which access is to be checked
     * @param sCurrentPage name of the header page
     * @param sFilter filter to be used to identify child tabs under a header page
     * @param lstTabs tabs to be displayed under a page
     * @return a map 
     */
    public static map<String, String> getTerritoryAccess(String sTerritoryId, String sCurrentPage, String sFilter, list<Tab> lstTabs){
        
        map<String, String> mapTerritoryAccess = new map<String, String>{SReadPermission => sBooleanValueFalse, SWritePermission => sBooleanValueFalse};
        list<HEP_User_Role__c> lstUserRoles = new list<HEP_User_Role__c>();
        
        String sTerritoryName;
        if(String.isEmpty(sTerritoryId)){
        	mapTerritoryAccess.put(SReadPermission, sBooleanValueTrue);
        	mapTerritoryAccess.put(SWritePermission, sBooleanValueTrue);
        }
        else{
        	// Finding all the user roles associated with the given territory and the user
	        lstUserRoles = [SELECT id, Territory__c, Record_Status__c, User__c, Read__c, Write__c, Territory__r.Name
	                        FROM HEP_User_Role__c
	                        WHERE Territory__c = :sTerritoryId AND User__c = :Userinfo.getUserId() AND Record_Status__c =: sActive];
	        
	        // Going through all the user roles to figure out if the user has read/write access on the territory    
	        for(HEP_User_Role__c objRole : lstUserRoles){
	            sTerritoryName = objRole.Territory__r.Name;
	            if(objRole.Read__c){
	                mapTerritoryAccess.put(SReadPermission, sBooleanValueTrue);
	            }
	            if(objRole.Write__c){
	                mapTerritoryAccess.put(SWritePermission, sBooleanValueTrue);
	            }
	            if((mapTerritoryAccess.get(SReadPermission) == sBooleanValueTrue) && (mapTerritoryAccess.get(SWritePermission)  == sBooleanValueTrue)){
	                break;
	            }
	        } 
        }        
        
        // If its a header page, check for the access to the child tabs
        if(String.isNotEmpty(sCurrentPage) && String.isNotEmpty(sFilter)){
        	mapTerritoryAccess.putAll(getPageAccessForHeaderPages(sCurrentPage, sFilter, lstTabs));
        	
        	if(sFilter != null && sFilter.containsIgnoreCase(HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL')) && 
        		!(sTerritoryName == HEP_Utility.getConstantValue('HEP_TERRITORY_DHE') || sTerritoryName == HEP_Utility.getConstantValue('HEP_TERRITORY_US')) && 
        		mapTerritoryAccess.containsKey(HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_PROMOTION_METADATA'))){
        			
        		mapTerritoryAccess.put(HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_PROMOTION_METADATA'), sBooleanValueFalse);
        		
        		Integer iCount = 0;
        		while(iCount < lstTabs.size()){
        			if(lstTabs[iCount].sTabName == HEP_Utility.getConstantValue('HEP_AUDIT_META')){
        				lstTabs.remove(iCount);
        			}
        			else{
        				 iCount++;
        			}
        		}        		
        	}
        }        
                
        // Returning the map
        return mapTerritoryAccess;
    }
    
    
    /**
     * Helps in getting access to the child pages of a header page
     * @param sCurrentPage name of the header page
     * @param sFilter filter to be used to identify child tabs under a header page
     * @param lstTabs tabs to be displayed under a page
     * @return a map 
     */
    public static map<String, String> getPageAccessForHeaderPages(String sCurrentPage, String sFilter, list<Tab> lstTabs){
    	
    	map<String, String> mapPageAccess = new map<String, String>();
    	
    	// Query to get the sub tabs under the Page Parameter.
        list<HEP_Menu_Item_Child__mdt> lstSubTabs = [SELECT Id, DeveloperName, MasterLabel, Order__c,
                                                     Filter_Criteria__c , Parent__c, Type__c
                                                     FROM HEP_Menu_Item_Child__mdt
                                                     WHERE Parent__c = :sCurrentPage AND 
                                                     Type__c = :HEP_Utility.getConstantValue('HEP_MENU_CHILD_ITEM_TYPE_SUB_TABS') order by Order__c, MasterLabel];        
        
        list<HEP_Menu_Item_Child__mdt> lstOrderedTabs = new list<HEP_Menu_Item_Child__mdt>();
        
        // Going through all the child tabs
        for(HEP_Menu_Item_Child__mdt objMenuItem : lstSubTabs){
        	
        	if(objMenuItem.Filter_Criteria__c != null && objMenuItem.Filter_Criteria__c.containsIgnoreCase(sFilter)){
        		
        		lstOrderedTabs.add(objMenuItem);
        		mapPageAccess.put(objMenuItem.DeveloperName, sBooleanValueTrue);
        	}
        	else{
        		mapPageAccess.put(objMenuItem.DeveloperName, sBooleanValueFalse);
        	}
        }
        
        if(!lstOrderedTabs.isEmpty()){
        	getTabs(lstOrderedTabs, lstTabs, sFilter);
        }
        
        // Returning the map
        return mapPageAccess;
    }
        
    
    /**
     * Helps in getting a list of territories that the user has write access to
     * @return a map of the territories in which the logged in user has write access
     */   
    public static map<String, Boolean> getMultipleTerritoryAccess(){
        map<String, Boolean> mapTerritoryAccess = new map<String, Boolean>();
        list<HEP_User_Role__c> lstUserRoles = new list<HEP_User_Role__c>();
        
    	lstUserRoles = [SELECT id, Territory__c, Territory__r.Name, Record_Status__c, User__c, Read__c, Write__c
                        FROM HEP_User_Role__c
                        WHERE User__c = :Userinfo.getUserId() AND Record_Status__c =: sActive AND Write__c = true];
       
        for(HEP_User_Role__c objRole : lstUserRoles){           
            mapTerritoryAccess.put(objRole.Territory__r.Name, true);            
        } 
        return mapTerritoryAccess;
    }    
    
    
    /**
    * Redirects the user to access denied page
    * @param bHasPageAccess indicates whether the user has access to the page or not
    * @return a page reference
    * @author Sachin Agarwal
    */
    public PageReference redirectToAccessDenied(Boolean bHasPageAccess){
    	// If the user has access to the territory, then only show the promotion details page, otherwise not
        if(!bHasPageAccess){
        	PageReference retURL = new PageReference('/apex/HEP_AccessDenied');
            retURL.setRedirect(true);
            return retURL;
        }
        else{
        	return null;
        }
    }
    
    
    /**
    * Gets the tabs under a master page
    * @param lstOrderedTabs a list of the tabs that fall under a master page
    * @return a list of tabs
    * @author Sachin Agarwal
    */
    public static void getTabs(list<HEP_Menu_Item_Child__mdt> lstOrderedTabs, list<Tab> lstTabs, String sFilter){
           
        set<String> setPageNames = new set<String>();
        map<String, String> mapPageNameLabel = new map<String, String>(); 
        map<String, list<String>> mapParentChildTabs = new map<String, list<String>>();
        
        for(HEP_Menu_Item_Child__mdt objMenuItem : lstOrderedTabs){
            if(!setPageNames.contains(objMenuItem.DeveloperName.toLowerCase())){
                setPageNames.add(objMenuItem.DeveloperName.toLowerCase());
                
                // Getting labels for all the child tabs
                mapPageNameLabel.put(objMenuItem.DeveloperName, objMenuItem.MasterLabel);
                         
                if(objMenuItem.DeveloperName == HEP_Utility.getConstantValue('HEP_PROMOTION_GLOBAL_DATING_ALL') || 
                	objMenuItem.DeveloperName == HEP_Utility.getConstantValue('HEP_PROMOTION_GLOBAL_DATING_LIO')){
                		
                    if(mapParentChildTabs.containsKey(sTerritoryDatesTab)){
                        mapParentChildTabs.get(sTerritoryDatesTab).add(objMenuItem.DeveloperName);
                    }
                    else{
                        mapParentChildTabs.put(sTerritoryDatesTab, new list<String>{objMenuItem.DeveloperName});
                    }
                } 
                // If its not a grouped tab, add it as a standalone tab
                else{
                    mapParentChildTabs.put(objMenuItem.DeveloperName, new list<String>{});
                }
            }
        }
        
        map<String, Tab> mapTabNameTab = new map<String, Tab>();           
        
        // Going through all the tabs
        for(String sPageName : mapParentChildTabs.keyset()){
            // If its a standalone tab and there are no child tabs
            if(mapParentChildTabs.get(sPageName).size() == 0){
                
                // Creating tab record to show on the UI
                Tab objParentTab = new Tab();
                
                if(mapPageNameLabel.containsKey(sPageName)){
                    objParentTab.sTabName = mapPageNameLabel.get(sPageName);
                }    
                
                objParentTab.sPageName = sPageName;
                mapTabNameTab.put(objParentTab.sTabName, objParentTab);
            }
            // If the number of child tabs is one, 
            else if(mapParentChildTabs.get(sPageName).size() == 1){
                
                // Creating tab record to show on the UI
                
                Tab objParentTab = new Tab();
                
                objParentTab.sTabName = sPageName;
                objParentTab.sPageName = mapParentChildTabs.get(sPageName)[0];
                
                objParentTab.lstSubTabs = new list<Tab>();
               
                // Creating child tab record to show on the UI
                Tab objChildTab = new Tab();
                
                if(mapPageNameLabel.containsKey(mapParentChildTabs.get(sPageName)[0])){
                    objChildTab.sTabName = mapPageNameLabel.get(mapParentChildTabs.get(sPageName)[0]);
                } 
                
                objChildTab.sPageName = mapParentChildTabs.get(sPageName)[0];
                objParentTab.lstSubTabs.add(objChildTab);
                
                mapTabNameTab.put(objParentTab.sTabName, objParentTab);
            }
            else{
                // Creating tab record to show on the UI
                Tab objParentTab = new Tab();
                
                objParentTab.sTabName = sPageName;
                objParentTab.sPageName = mapParentChildTabs.get(sPageName)[0];
                
                objParentTab.lstSubTabs = new list<Tab>();
                
                // Creating all the child tab records to show on the UI
                for(String sChildPage : mapParentChildTabs.get(sPageName)){
                    
                    Tab objChildTab = new Tab();
                    
                    if(mapPageNameLabel.containsKey(sChildPage)){
                        objChildTab.sTabName = mapPageNameLabel.get(sChildPage);
                    } 
                    
                    objChildTab.sPageName = sChildPage;
                    objParentTab.lstSubTabs.add(objChildTab);
                }
                
                mapTabNameTab.put(objParentTab.sTabName, objParentTab);
            }
        } 
        
        set<String> setTabNames = new set<String>();
        
        // Going through all the order tabs to order the tabs
        for(HEP_Menu_Item_Child__mdt objMenuItem : lstOrderedTabs){
            if(mapTabNameTab.containsKey(objMenuItem.MasterLabel)){
            	if(!setTabNames.contains(objMenuItem.MasterLabel)){
            		setTabNames.add(objMenuItem.MasterLabel);
            		Tab objTab = mapTabNameTab.get(objMenuItem.MasterLabel);
            		if(objTab.sTabName == HEP_Utility.getConstantValue('HEP_TAB_NAME_SPEND') && objMenuItem.Filter_Criteria__c != null && 
            			objMenuItem.Filter_Criteria__c.containsIgnoreCase(HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL')) && sFilter != null && sFilter.containsIgnoreCase(HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL'))){
            				
            			objTab.sTabName = HEP_Utility.getConstantValue('HEP_TAB_NAME_HO_SPEND');
            		}
            		lstTabs.add(objTab);
            	}                
            }
            else if((objMenuItem.DeveloperName == HEP_Utility.getConstantValue('HEP_PROMOTION_GLOBAL_DATING_ALL') || 
            		objMenuItem.DeveloperName == HEP_Utility.getConstantValue('HEP_PROMOTION_GLOBAL_DATING_LIO')) && 
            		mapTabNameTab.containsKey(sTerritoryDatesTab)){
            	
            	if(!setTabNames.contains(sTerritoryDatesTab)){
            		setTabNames.add(sTerritoryDatesTab);
            		lstTabs.add(mapTabNameTab.get(sTerritoryDatesTab));
            	}
            }
        }        
    }
}