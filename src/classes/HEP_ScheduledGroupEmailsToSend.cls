global class HEP_ScheduledGroupEmailsToSend implements Schedulable {
   global void execute(SchedulableContext sc) {
       processGroupEmails();
   }



public static void processGroupEmails()
{
    // get email templates that we need to process
String sEmailsToSkip = Label.HEP_Email_For_Batch;
Set<String> setTemplatesToSkip, tempSet;
if(String.isNotBlank(sEmailsToSkip))
    setTemplatesToSkip = new Set<String>(sEmailsToSkip.split(','));
else
    setTemplatesToSkip = new Set<String>();

Set<String> setEmailIdsToSent = new Set<String>();  
            
Map<String,List<HEP_Outbound_Email__c>> mapRecordIdsEmails = new Map<String,List<HEP_Outbound_Email__c>>(); 
Map<String,Set<String>> mapTemplateRecordIds = new Map<String,Set<String>>(); 
Map<String,List<HEP_Outbound_Email__c>> mapPromoRecIdsEmails = new Map<String,List<HEP_Outbound_Email__c>>(); 
for(HEP_Outbound_Email__c rec :[Select id,Object_API__c,Record_Id__c,Email_Template_Name__c,CC_Email_Address__c,To_Email_Address__c from    
                                HEP_Outbound_Email__c where Email_Template_Name__c = :  setTemplatesToSkip and Email_Sent__c=false and
                                Record_Id__c != null
                                 order by createddate desc])
{// search all the emails where the type is matching and sent is false(not processed) and promotion lookup is blank
// data is added 2 maps one with record id as the key and list of email records grouped to that record id
// second man with email temaple and the record ids list for that email template
    if(mapRecordIdsEmails.containsKey(rec.Record_Id__c))
    {
        List<HEP_Outbound_Email__c> temp = mapRecordIdsEmails.get(rec.Record_Id__c);
        temp.add(rec);
        mapRecordIdsEmails.put(rec.Record_Id__c,temp);
    }
    else
        mapRecordIdsEmails.put(rec.Record_Id__c,new List<HEP_Outbound_Email__c>{rec} );
    
    if(mapTemplateRecordIds.containsKey(rec.Email_Template_Name__c))
    {
        tempSet = mapTemplateRecordIds.get(rec.Email_Template_Name__c);
        tempSet.add(rec.Record_Id__c);
        mapTemplateRecordIds.put(rec.Email_Template_Name__c,tempSet );
        
    }
    else
        mapTemplateRecordIds.put(rec.Email_Template_Name__c,new Set<String>{rec.Record_Id__c} );
}
List<HEP_Outbound_Email__c> lstUpdateEmails = new List<HEP_Outbound_Email__c>();
system.debug('mapTemplateRecordIds'+mapTemplateRecordIds);
integer recSize = 0;
String sKey;
// search on the approval record to get the promotion id via formula field on the approlca parent rec
if(mapTemplateRecordIds.containsKey('HEP_Customer_Promotion_SUBMITTED') || mapTemplateRecordIds.containsKey('HEP_SKU_Activation_Request') || 
    mapTemplateRecordIds.containsKey('HEP_TPR_National_Promotion_SUBMITTED') )
{
    if(!mapTemplateRecordIds.containsKey('HEP_Customer_Promotion_SUBMITTED'))
        mapTemplateRecordIds.put('HEP_Customer_Promotion_SUBMITTED',new Set<String>());
    if(!mapTemplateRecordIds.containsKey('HEP_SKU_Activation_Request'))
        mapTemplateRecordIds.put('HEP_SKU_Activation_Request',new Set<String>());
    if(!mapTemplateRecordIds.containsKey('HEP_TPR_National_Promotion_SUBMITTED'))
        mapTemplateRecordIds.put('HEP_TPR_National_Promotion_SUBMITTED',new Set<String>());
    
    
    for(HEP_Record_Approver__c rec:[ select Id,Approval_Record__r.Promotion_Id__c from HEP_Record_Approver__c where IsActive__c=true and Status__c = 'Pending'                              and ( id = : mapTemplateRecordIds.get('HEP_Customer_Promotion_SUBMITTED') OR
                                            id = : mapTemplateRecordIds.get('HEP_SKU_Activation_Request') OR 
                                            id = : mapTemplateRecordIds.get('HEP_TPR_National_Promotion_SUBMITTED'))])
    {
        system.debug('rec'+rec);
        if(rec.Approval_Record__r.Promotion_Id__c!=null && mapRecordIdsEmails.containsKey(rec.Id))
        {
            for(HEP_Outbound_Email__c tempRec : mapRecordIdsEmails.get(rec.Id)){
                if(recSize<5000){
                    tempRec.HEP_Promotion__c = rec.Approval_Record__r.Promotion_Id__c;
                    lstUpdateEmails.add(tempRec);
                    recSize = recSize+1;
                    sKey = tempRec.Email_Template_Name__c+'-'+rec.Approval_Record__r.Promotion_Id__c;
                    mapPromoRecIdsEmails = mapEmailstoPromotion(mapPromoRecIdsEmails,tempRec,sKey);
                    
                    setEmailIdsToSent = addEmailstoSet(setEmailIdsToSent,tempRec);
                }
            }           
        }
    }
}
system.debug('mapPromoRecIdsEmails'+mapPromoRecIdsEmails);
system.debug('setEmailIdsToSent'+setEmailIdsToSent);
// template has the object id as promotion id so no changes
if(mapTemplateRecordIds.containsKey('HEP_SKU_Promotion_Creation_Canada') && recSize<5000 )
{
    for(String recId: mapTemplateRecordIds.get('HEP_SKU_Promotion_Creation_Canada'))
    {
        if(mapRecordIdsEmails.containsKey(recId))
        {
            for(HEP_Outbound_Email__c tempRec : mapRecordIdsEmails.get(recId)){
                if(recSize<5000)
                {
                    tempRec.HEP_Promotion__c = recId;
                    lstUpdateEmails.add(tempRec);
                    recSize = recSize+1;
                    sKey = tempRec.Email_Template_Name__c+'-'+recId;
                    
                    mapPromoRecIdsEmails = mapEmailstoPromotion(mapPromoRecIdsEmails,tempRec,sKey);
                    
                    setEmailIdsToSent = addEmailstoSet(setEmailIdsToSent,tempRec);
                }
            }
        }       
    }   
}


// send data grouped by promotion getting it via promotion SKU
if(mapTemplateRecordIds.containsKey('HEP_SKU_Lock_Email_Reminder') && recSize<5000)
{
    for(HEP_Promotion_SKU__c rec:[select Id, HEP_Promotion__c from HEP_Promotion_SKU__c where id = :mapTemplateRecordIds.get('HEP_SKU_Lock_Email_Reminder')])
    {
        if(rec.HEP_Promotion__c!=null && recSize<5000 && mapRecordIdsEmails.containsKey(rec.Id))
        {
            for(HEP_Outbound_Email__c tempRec : mapRecordIdsEmails.get(rec.Id)){
                if(recSize<5000)
                {
                    tempRec.HEP_Promotion__c = rec.HEP_Promotion__c;
                    lstUpdateEmails.add(tempRec);
                    recSize = recSize+1;
                    
                    sKey = tempRec.Email_Template_Name__c+'-'+rec.HEP_Promotion__c;
                    
                    mapPromoRecIdsEmails = mapEmailstoPromotion(mapPromoRecIdsEmails,tempRec,sKey);
                    
                    setEmailIdsToSent = addEmailstoSet(setEmailIdsToSent,tempRec);
                
                }
            }
            
        }
    }
    
}

if(lstUpdateEmails.size()>0)
    update lstUpdateEmails;

 //Map of Email users and User Id
Map<String,Id> mapUsersWithEmailPlusUserIds = new Map<String,Id>();
System.debug('Email Id to be Sent--->'+setEmailIdsToSent);
if(setEmailIdsToSent != null && !setEmailIdsToSent.isEmpty()){      
    for(User tempUser :[Select  Id, Email from User where Email In: setEmailIdsToSent And IsActive = true]){
        if(!mapUsersWithEmailPlusUserIds.containsKey(tempUser.Email)){
            mapUsersWithEmailPlusUserIds.put(tempUser.Email,tempUser.Id);
        }
    }
}

sendGroupedSingleEmail( mapPromoRecIdsEmails, mapUsersWithEmailPlusUserIds,setTemplatesToSkip);

}

public static Set<String> addEmailstoSet(Set<String> setEmailIdsToSent,HEP_Outbound_Email__c emailRec)
{
    if(emailRec.To_Email_Address__c != null && String.isNotBlank(emailRec.To_Email_Address__c)){
        setEmailIdsToSent.addAll(HEP_Utility.getActualEmailIds(emailRec.To_Email_Address__c.split(';')));
    } 
     if(String.isNotBlank(emailRec.CC_Email_Address__c) && emailRec.CC_Email_Address__c != null){
        setEmailIdsToSent.addAll(HEP_Utility.getActualEmailIds(emailRec.CC_Email_Address__c.split(';')));   
    }
    return setEmailIdsToSent;
}


public static Map<String,List<HEP_Outbound_Email__c>> mapEmailstoPromotion(Map<String,List<HEP_Outbound_Email__c>> mapPromoRecIdsEmails,
                                                                            HEP_Outbound_Email__c emailRec, String sKey)
{
    HEP_Outbound_Email__c tempSentRec = new HEP_Outbound_Email__c(id=emailRec.Id,Email_Sent__c=true,CC_Email_Address__c = emailRec.CC_Email_Address__c,
                                                                To_Email_Address__c =  emailRec.To_Email_Address__c,
                                                                Record_Id__c=emailRec.Record_Id__c);
    List <HEP_Outbound_Email__c> listTemp;  
    if(mapPromoRecIdsEmails.containsKey(sKey))
    {
        listTemp = mapPromoRecIdsEmails.get(sKey);
        listTemp.add(tempSentRec);
        mapPromoRecIdsEmails.put(sKey,listTemp);
        
    }
    else
        mapPromoRecIdsEmails.put(sKey,new List<HEP_Outbound_Email__c>{tempSentRec});    
    
    return mapPromoRecIdsEmails;
}

public static void sendGroupedSingleEmail(Map<String,List<HEP_Outbound_Email__c>> mapPromoRecIdsEmails,Map<String,Id> mapUsersWithEmailPlusUserIds,Set<String> setTemplatesToSkip)
{
    map<String,Id> mapEmailTemplates = new map<String,Id>();            
    //Map of Email Template Name and Developer Id of Template
    for (EmailTemplate emailTemp: [Select id, DeveloperName from EmailTemplate where DeveloperName =: setTemplatesToSkip])
    {
        mapEmailTemplates.put(emailTemp.DeveloperName ,emailTemp.Id);                
    }
    List<HEP_Outbound_Email__c> lstUpdateEmailsAfterSend = new List<HEP_Outbound_Email__c>();
    // searching for HEP Org Wide Address to use
    List<OrgWideEmailAddress> lstOrgWideId = [select id, Address,DisplayName from OrgWideEmailAddress where DisplayName like '%HEP%'];
    //Process Emails
    List<String> lstCCEmailAddresses,lstSetToEmailAddresses;
    Set<String> userIdsToSend, ccUsersToSend;
                
    List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
    Messaging.SingleEmailMessage mail;
    //Boolean callOnce;
    // loop through the map where key is template/promotion id and has child records under it that are grouped to be sent
    // for each of this it loops through the first record only for processing
    if(mapPromoRecIdsEmails.size()>0)
    {   
        for(String sKeyVal:mapPromoRecIdsEmails.keySet())
        {
            //callOnce = true;
            List<String> lstSplitVal = sKeyVal.split('-');
            //sKey = tempRec.Email_Template_Name__c+'-'+rec.HEP_Promotion__c;
        //  for(HEP_Outbound_Email__c emailRec:mapPromoRecIdsEmails.get(sKeyVal))
            // get the first record for processing only
            HEP_Outbound_Email__c emailRec = mapPromoRecIdsEmails.get(sKeyVal)[0];
            {
                system.debug('emailRec'+emailRec);
                //if(callOnce)
                {
                    //Fetch the Object details for Approval Action
                    //Fetch the Approval Owner Email Id
                
                    userIdsToSend = new set <String>();
                    ccUsersToSend = new set <String>();
                    lstCCEmailAddresses = String.isNotBlank(emailRec.CC_Email_Address__c)?HEP_Utility.getActualEmailIds(emailRec.CC_Email_Address__c.split(';')): NULL;
                    lstSetToEmailAddresses = String.isNotBlank(emailRec.To_Email_Address__c)?HEP_Utility.getActualEmailIds(emailRec.To_Email_Address__c.split(';')): NULL;
                    // loop thru To and CC addresses to check if user exists else move them to CC email address list
                    if(lstCCEmailAddresses != null){
                        for (String sEmailAddress : lstCCEmailAddresses)
                        {
                            if(mapUsersWithEmailPlusUserIds.containsKey(sEmailAddress)){
                                userIdsToSend.add(mapUsersWithEmailPlusUserIds.get(sEmailAddress));
                            }
                            else
                                ccUsersToSend.add(sEmailAddress);
                        }
                    }
                    if(lstSetToEmailAddresses != null){
                        for (String sEmailAddress : lstSetToEmailAddresses)
                        {
                            if(mapUsersWithEmailPlusUserIds.containsKey(sEmailAddress)){
                                userIdsToSend.add(mapUsersWithEmailPlusUserIds.get(sEmailAddress));
                            }
                            else
                                ccUsersToSend.add(sEmailAddress);
                        }
                    }
                    Boolean firstEmail = true;
                    // add a mail to single email for each of the user in the list
                    System.debug('Total To users--->'+userIdsToSend.size());
                    for (String userId : userIdsToSend)
                    {
                      //String sEmailAddress = lstSetToEmailAddresses.get(0);
                    
                        mail = new Messaging.SingleEmailMessage();
                        if(firstEmail)
                        {
                            firstEmail = false;
                            if(ccUsersToSend.size()>0)
                                mail.setCcAddresses(new List<String>(ccUsersToSend));
                            System.debug('CC users--->'+ccUsersToSend);
                        }

                        mail.setTemplateID(mapEmailTemplates.get(lstSplitVal[0]));
                        mail.setSaveAsActivity(false);
                        mail.setWhatId(lstSplitVal[1]);    // using the record if for template for now 
                        //mail.setWhatId(emailRec.Record_Id__c);            
                        mail.setTargetObjectId(userId); 
                        if(lstOrgWideId.size()>0)
                            mail.setOrgWideEmailAddressId(lstOrgWideId[0].Id);                      
                        // Set who the email is sent from       
                        //mail.setSenderDisplayName(System.Label.HEP_EMAIL_SENDER_DISPLAY_NAME);
                        //Shoot the Email
                        mails.add(mail);
                        
                    }
                    
                    //callOnce = false;
                }
            
            }
            
            lstUpdateEmailsAfterSend.addAll((List<HEP_Outbound_Email__c>) mapPromoRecIdsEmails.get(sKeyVal));
        }
     System.debug('mails sent---->'+mails.size());
      System.debug('mails sent---->'+mails);
    if(mails.size()>0)
        Messaging.sendEmail(mails);     
        
    if(lstUpdateEmailsAfterSend.size()>0)
        update lstUpdateEmailsAfterSend;
    }
}

}