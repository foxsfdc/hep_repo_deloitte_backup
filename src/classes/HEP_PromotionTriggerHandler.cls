/**
 *HEP_PromotionTriggerHandler --- Promotion Object trigger Handler
                                  This will run everytime a promotion record is inserted, updated, deleted or undeleted
 *@author  Abhishek Mishra
 */
Public Class HEP_PromotionTriggerHandler extends TriggerHandler{
    public static map<id, list<HEP_Promotion_Dating__c>> mapPrmotionIdDatingRecods;
    public static list<HEP_Promotion_Dating__c> lstAllPrmotionDatesFADApprovals;
    public static list<HEP_Promotion_Dating__c> lstAllPrmotionDatesFADNoApprovals;
    public static map<id, date> mapAllGlobalFADIdDates;
    public Set<String> territorySet = new Set<String>{'US', 'Canada', 'DHE'};
    public static list < HEP_Notification__c > lstNotificationsToInsert;
    public static List < HEP_Notification_User_Action__c > lstNotificationsActionToInsert;
    public static Set < Id > setApprovers;
    public static List < HEP_Promotion__c > lstLocalPromotions;
    public static String sTemplateName;
    public static String sPromotionObject;
    public static String sGlobal;
    public static Set < Id > setExecutedPromotionIds;
    public static String sNATIONAL, sPUBLISHED;
    
    static {
        sTemplateName = HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change');
        sPromotionObject = HEP_Utility.getConstantValue('HEP_PROMOTION');
        sGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        setExecutedPromotionIds = new Set<ID>();
        sPUBLISHED = HEP_Utility.getConstantValue('HEP_PROMOTION_PUBLISHED');
        sNATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
    }
    /**
     *Class Constructor to initialise class variables
     *@return nothing
     */

    public HEP_PromotionTriggerHandler() {
        mapPrmotionIdDatingRecods = new map<id, list<HEP_Promotion_Dating__c>>();
        lstAllPrmotionDatesFADApprovals = new list<HEP_Promotion_Dating__c>();
        lstAllPrmotionDatesFADNoApprovals = new list<HEP_Promotion_Dating__c>();
        mapAllGlobalFADIdDates = new map<id, date>();
        lstNotificationsToInsert = new List < HEP_Notification__c > ();
        lstNotificationsActionToInsert = new List < HEP_Notification_User_Action__c > ();
        setApprovers = new Set < Id > ();
        lstLocalPromotions = new List < HEP_Promotion__c > ();
    }
    
    /**
     * afterUpdate-- Context specific beforeUpdate method
     * @return nothing
     */

    public override void afterUpdate() {
        if(Trigger.oldMap != null){
            System.debug('OLD MAP ------- '+Trigger.oldMap);
            handleAfterUpdate((List < HEP_Promotion__c> ) Trigger.new, (map<id, HEP_Promotion__c>)Trigger.oldMap );
            System.debug('Checking field updation process------');
            System.debug('Old Map ------> ' + Trigger.newMap);
            System.debug('Old Map ------> ' + Trigger.oldMap);
            Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Promotion__c', 'Promotion_Field_Set');
            System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
            if(checkFieldUpdate)
                callQueuableMethod(Trigger.newMap.keyset());
            
            //JDE Outbound Integration
            Boolean checkFieldUpdateForJDE = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Promotion__c', 'Promotion_JDE_Fields');
                    System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdateForJDE);
            if(checkFieldUpdateForJDE){
                for(HEP_Promotion__c promo : [Select Id from HEP_Promotion__c Where Id IN :Trigger.newMap.keyset() AND Territory__r.Name IN :territorySet] ){
                  if(null != setExecutedPromotionIds 
                && !setExecutedPromotionIds.contains(promo.Id) ){
                  System.debug('setExecutedPromotionIds :: '+ setExecutedPromotionIds);
                        System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(promo.Id));
              }
              setExecutedPromotionIds.add(promo.Id);
                }
            }
            AlertsAfterUpdate((List < HEP_Promotion__c > ) Trigger.new, (Map<Id,HEP_Promotion__c>) Trigger.oldMap);
            sendNationalPublishedEmail((List < HEP_Promotion__c > ) Trigger.new, (Map<Id,HEP_Promotion__c>) Trigger.oldMap);
        }
            
    }
    /**
     * handleAfterUpsert -- Handler method for trigger before update
     * @param List of promotion records after change and map of promotion after change
     * @return void
     */

    public static void handleAfterUpdate(List<HEP_Promotion__c> lstNewPromotions, map<id, HEP_Promotion__c> mapOldPromotions){
        mapPrmotionIdDatingRecods = new map<id, list<HEP_Promotion_Dating__c>>();
        lstAllPrmotionDatesFADApprovals = new list<HEP_Promotion_Dating__c>();
        lstAllPrmotionDatesFADNoApprovals = new list<HEP_Promotion_Dating__c>();
        mapAllGlobalFADIdDates = new map<id, date>();
        try{
            //create the map for promotions and respective dating records
            for(HEP_Promotion_Dating__c objProDateRecord : [select id, Date__c,Promotion__c 
                                                            from HEP_Promotion_Dating__c 
                                                            where Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND Promotion__c in :lstNewPromotions]){
                if(mapPrmotionIdDatingRecods.containsKey(objProDateRecord.Promotion__c))
                    mapPrmotionIdDatingRecods.get(objProDateRecord.Promotion__c).add(objProDateRecord);
                else
                    mapPrmotionIdDatingRecods.put(objProDateRecord.Promotion__c, new List<HEP_Promotion_Dating__c>{objProDateRecord}); 
            }  
            system.debug('mapPrmotionIdDatingRecods : ' + mapPrmotionIdDatingRecods); 
            //Create the map for promotions and global FAD
            for(HEP_Promotion__c objPromotionRecord : [select id, Global_Promotion__r.FirstAvailableDate__c 
                                                       from HEP_Promotion__c 
                                                       where   Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND id in :lstNewPromotions]){
                mapAllGlobalFADIdDates.put(objPromotionRecord.id, objPromotionRecord.Global_Promotion__r.FirstAvailableDate__c);
            }
            system.debug('mapAllGlobalFADIdDates : ' + mapAllGlobalFADIdDates);
            //Check for if dates before global FAD dates
            for(HEP_Promotion__c objPromotionRecord : lstNewPromotions){
                if(objPromotionRecord.Global_Promotion__c != mapOldPromotions.get(objPromotionRecord.id).Global_Promotion__c){
                    list<HEP_Promotion_Dating__c> allDatings = mapPrmotionIdDatingRecods.get(objPromotionRecord.id);
                    if(allDatings != null && !allDatings.isEmpty()){
                        for(HEP_Promotion_Dating__c objDateRecord : allDatings){
                            if(objPromotionRecord.Global_Promotion__c != null){
                                //check to initiate approval
                                if(objDateRecord.Date__c < mapAllGlobalFADIdDates.get(objPromotionRecord.id)){
                                    objDateRecord.FAD_Override__c = true;
                                    lstAllPrmotionDatesFADApprovals.add(objDateRecord);
                                }
                            }else{
                                //Check for cancel approval
                                lstAllPrmotionDatesFADNoApprovals.add(objDateRecord);
                            }
                        }          
                    }
                }
                system.debug('lstAllPrmotionDatesFADApprovals : ' + lstAllPrmotionDatesFADApprovals);
                system.debug('lstAllPrmotionDatesFADNoApprovals : ' + lstAllPrmotionDatesFADNoApprovals);
                if(!lstAllPrmotionDatesFADApprovals.isEmpty()){
                    //Delete the Existing Approvals
                    if(mapPrmotionIdDatingRecods.containsKey(objPromotionRecord.id)){
                        HEP_FAD_Override_Approval_Process.cancelExistingApprovalproces(objPromotionRecord .id, mapPrmotionIdDatingRecods.get(objPromotionRecord.id));    
                    }    
                    //Initiate the approval Process
                    HEP_FAD_Override_Approval_Process.initiateFADApprovalProcess(objPromotionRecord .id, lstAllPrmotionDatesFADApprovals);    
                }    
                if(!lstAllPrmotionDatesFADNoApprovals.isEmpty())
                    //Cancel the approval process
                    HEP_FAD_Override_Approval_Process.cancelExistingApprovalproces(objPromotionRecord .id, lstAllPrmotionDatesFADNoApprovals);    
            }
        }catch(Exception Ex){
            system.debug('Error in Promotion Trigger : ' + Ex);
            HEP_Error_Log.genericException('Error in Promotion Trigger','Promotion Trigger',ex,'HEP_PromotionTriggerHandler','handleAfterUpdate',null,'');
            throw(Ex);
            
        }
                
    }
    /**
     * AlertsAfterUpdate -- Create notification when Global FAD is getting updated
     * @param List of promotion records after change and map of promotion after change
     * @return nothing
     */

    public static void AlertsAfterUpdate(List < HEP_Promotion__c > lstHepPromotion , map<id, HEP_Promotion__c> mapOldPromotions) {
        lstNotificationsToInsert = new List < HEP_Notification__c > ();
        lstNotificationsActionToInsert = new List < HEP_Notification_User_Action__c > ();
        setApprovers = new Set < Id > ();
        lstLocalPromotions = new List < HEP_Promotion__c > ();
        //fetch template
        System.debug('Intrigger+++++++++++');
        Date dOldFAD;
        Date dNewFAD;
        HEP_Notification_Template__c objHepNotifTemplate = HEP_Notification_Utility.getTemplate(sTemplateName , sPromotionObject);
        //setPromotionIds:set to store promotion record id's
        set < id > setPromotionIds = new set < id > ();
        //Populate the Values in Set with the promotion record id
        setPromotionIds = new Map<Id, HEP_Promotion__c>(lstHepPromotion).keySet();
        lstLocalPromotions = [SELECT Domestic_Marketing_Manager__c
                    FROM HEP_Promotion__c
                    WHERE Global_Promotion__c IN: setPromotionIds AND Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')
        ];
        system.debug('lstLocalPromotions----->' + lstLocalPromotions);
        for (HEP_Promotion__c objPromotion : lstLocalPromotions){
            setApprovers.add(objPromotion.Domestic_Marketing_Manager__c);
        }
        system.debug('-----' + setApprovers);
        for (HEP_Promotion__c objPromo : lstHepPromotion){ 
            System.debug('In loop');
            system.debug('objPromo.FirstAvailableDateFormula__c : ' + objPromo.FirstAvailableDateFormula__c);
            system.debug('mapOldPromotions.get(objPromo.Id).FirstAvailableDateFormula__c : ' + mapOldPromotions.get(objPromo.Id).FirstAvailableDateFormula__c);
            system.debug('objPromo.Promotion_Type__c == sGlobal : ' + objPromo.Promotion_Type__c);
            if ((objPromo.FirstAvailableDateFormula__c != null) && (objPromo.FirstAvailableDateFormula__c != mapOldPromotions.get(objPromo.Id).FirstAvailableDateFormula__c) && objPromo.Promotion_Type__c == sGlobal)
            {
                dOldFAD = mapOldPromotions.get(objPromo.Id).FirstAvailableDateFormula__c; 
                dNewFAD = objPromo.FirstAvailableDateFormula__c;
                // Create Notification Records              
                HEP_Notification__c objHepNewNotif = new HEP_Notification__c();
                objHepNewNotif.HEP_Message__c = HEP_Utility.getFormattedDate(dOldFAD,'dd-MMM-yyyy') + ' updated to ' + HEP_Utility.getFormattedDate(dNewFAD,'dd-MMM-yyyy');
                objHepNewNotif.HEP_Template_Name__c = sTemplateName;
                objHepNewNotif.HEP_Object_API__c = sPromotionObject;
                objHepNewNotif.Notification_Template__c = objHepNotifTemplate.id;
                objHepNewNotif.HEP_Record_ID__c = objPromo.id;
                lstNotificationsToInsert.add(objHepNewNotif);
            }
        }
        if (lstNotificationsToInsert != NULL && lstNotificationsToInsert.size() > 0) {
            Database.SaveResult[] lstSaveResult = Database.insert(lstNotificationsToInsert, false);
            system.debug('lstNotificationsToInsert ' + lstNotificationsToInsert);
            if (setApprovers != NULL && setApprovers.size() > 0) {
                for (Database.SaveResult objSaveResult: lstSaveResult) {
                    for (Id objUser: setApprovers) {
                        if (objSaveResult.isSuccess()) {
                            HEP_Notification_User_Action__c objHepNua = new HEP_Notification_User_Action__c();
                            objHepNua.HEP_Notification__c = objSaveResult.getId();
                            objHepNua.Notification_User__c = objUser;
                            lstNotificationsActionToInsert.add(objHepNua);
                        }
                    }
                }
            }
            if (lstNotificationsActionToInsert != NULL && lstNotificationsActionToInsert.size() > 0) {
                insert lstNotificationsActionToInsert;
                system.debug('lstNotificationsActionToInsert ' + lstNotificationsActionToInsert);
            }
        }
        
        
    }
    /**
     * sendNationalPublishedEmail -- Create notification when national promotion is published
     * @param List of promotion records after change and map of promotion after change
     * @return nothing
     */
    public static void sendNationalPublishedEmail(List < HEP_Promotion__c > lstHepPromotion , map<id, HEP_Promotion__c> mapOldPromotions)
    {       
        List<String> lstEmailAddresses =  new List<String>();
        Set<String> setEmailAddresses = new Set<String>();
        Set<String> setToEmailAddresses = new Set<String>();
        Set<String> setFromEmailAddresses = new Set<String>();
        List<Id> lstUserId = new List<Id>();
        Set<Id> setUserId = new Set<Id>();
        String sRole = HEP_Utility.getConstantValue('Customer_Promotion_Role_Names');
        String sPromotionTerritory;
        String sPromotionId;
        List < HEP_Promotion__c > lstNationalPromotions = new List < HEP_Promotion__c > ();       
        Set < id > setPromotionIds = new set < id > ();
        for(HEP_Promotion__c objPromotion : lstHepPromotion){
            HEP_Promotion__c oldPromotionRecord = mapOldPromotions.get(objPromotion.Id);
            if((objPromotion.Status__c != oldPromotionRecord.Status__c) && objPromotion.Status__c == sPUBLISHED && objPromotion.Promotion_Type__c == sNational){
                setPromotionIds.add(objPromotion.Id);
            } 
        }
        if (setPromotionIds != null && !setPromotionIds.isEmpty()) {
            lstNationalPromotions = [SELECT id, Promotion_Type__c,Territory__r.Name,Domestic_Marketing_Manager__c,Domestic_Marketing_Manager__r.Email
                    FROM HEP_Promotion__c
                    WHERE Id IN: setPromotionIds AND Promotion_Type__c =: sNational AND Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')
            ];
        }
        system.debug('lstNationalPromotions----->' + lstNationalPromotions);
        if(lstNationalPromotions != null && !lstNationalPromotions.isEmpty() && lstNationalPromotions[0].Territory__r.Name != null) {
            sPromotionTerritory = lstNationalPromotions[0].Territory__r.Name;
            sPromotionId = lstNationalPromotions[0].Id;
            if(lstNationalPromotions[0].Domestic_Marketing_Manager__c != null){
                setEmailAddresses.add(lstNationalPromotions[0].Domestic_Marketing_Manager__r.Email);
                setUserId.add(lstNationalPromotions[0].Domestic_Marketing_Manager__c);
            }                       
        }           
        system.debug('sPromotionTerritory----->' + sPromotionTerritory);
        system.debug('sPromotionId----->' + sPromotionId);
        if(sRole != null && sPromotionTerritory != null){
            //Code to send Emails to Digital Account Manager
            List<HEP_User_Role__c> lstEmail = [SELECT ID , User__c , Role__c , User__r.Email, Territory__c , Territory__r.Name
                                                    FROM HEP_User_Role__c
                                                    WHERE Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')
                                                    AND Role__r.Name =: sRole
                                                    AND Territory__r.Name =: sPromotionTerritory];
            if (!lstEmail.isEmpty()) {
                for (HEP_User_Role__c objUserRole : lstEmail) {
                    setEmailAddresses.add(objUserRole.User__r.Email);
                    setUserId.add(objUserRole.User__c);
                }
            }
        }
       
        System.debug('1. Email Ids TO : ----> ' + setEmailAddresses);

        //Code to add additional Email Ids for Fox Users....
        String sEmailTemplateName = HEP_Utility.getConstantValue('HEP_National_Promotion_Published');
        if(setEmailAddresses != NULL && !setEmailAddresses.isEmpty()){
            lstEmailAddresses.addAll(setEmailAddresses);
        }

        if(!lstEmailAddresses.isEmpty()){
                     
            //Create Outbound Email record before sending the Email.

            List<HEP_Outbound_Email__c> lstOutboundEmailsToInsert = new List<HEP_Outbound_Email__c>();

            HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c(); //Single Instance of the Object
            objOutboundEmail.Record_Id__c = sPromotionId; // The merge Id that will be accessed by the Template (WhatId)
            objOutboundEmail.Object_API__c = 'HEP_Promotion__c'; //The Object Name of the above Record
            objOutboundEmail.Email_Template_Name__c = HEP_Utility.getConstantValue('HEP_National_Promotion_Published');      // The Email Template Name

            String sEmailAddresses = String.join(lstEmailAddresses , ';');
            objOutboundEmail.To_Email_Address__c = sEmailAddresses;

            /*if (String.isNotBlank(sUsersInEMailCC)) {           
                objOutboundEmail.CC_Email_Address__c = sUsersInEMailCC;
            }*/

            lstOutboundEmailsToInsert.add(objOutboundEmail);

            if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                insert lstOutboundEmailsToInsert;
                system.debug('Email Alerts list ' + lstOutboundEmailsToInsert);
                HEP_Send_Emails.sendOutboundEmails(lstOutboundEmailsToInsert);
            }
        }
        if(sPromotionId != null){
            List<HEP_Promotion__c> lstNewPromotion = [Select id,PromotionName__c from HEP_Promotion__c where id =: sPromotionId];
            lstUserId.addAll(setUserId);
            if(!lstUserId.isEmpty() && !lstNewPromotion.isEmpty()){
                HEP_Notification_Utility.createNotificationAlerts(lstNewPromotion,HEP_Utility.getConstantValue('National_Promotion_Published'),lstUserId);
            }
        }
    }
    /**
     * afterInsert -- Context specific afterinsert method
     * @return nothing
     */
    public override void afterInsert() {
        callQueuableMethod(Trigger.newMap.keyset());
        //Avinash : Outbound callout to JDE
        for(HEP_Promotion__c promo : [Select Id from HEP_Promotion__c Where Id IN :Trigger.newMap.keyset() AND Territory__r.Name IN :territorySet] ){
          if(null != setExecutedPromotionIds 
            && !setExecutedPromotionIds.contains(promo.Id) ){
              System.debug('setExecutedPromotionIds :: '+setExecutedPromotionIds);
              System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(promo.Id));
          }
            setExecutedPromotionIds.add(promo.Id);
        }
    }
    public static void callQueuableMethod(set<Id> setPromotionIds){
        System.debug('inside callQueuableMethod');    
        Boolean valueFalse = false;    
        String promotionTypeGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        String promotionTypeNational = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        List<Id> lstPromotionIds = new List<Id>();
        for(HEP_Promotion__c promo : [Select Id,Promotion_Type__c from HEP_Promotion__c Where Id IN :setPromotionIds AND TPR_Only__c = :valueFalse AND (Promotion_Type__c = :promotionTypeGlobal OR Promotion_Type__c = :promotionTypeNational)] ){
            lstPromotionIds.add(promo.Id);
        }   

        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable()) && !lstPromotionIds.isEmpty()){
            System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstPromotionIds, ',') , HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTION')));
        }
    }

    
}