@isTest
public class HEP_SKU_Promotion_Creation_Canada_Test{
    
    @testSetup
    public static void createTestUserSetup(){
        //constant Data
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    }
    public static testmethod void fetchDetails_Test(){
        //HEP Constants
        //List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'Canada';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory1.Id, '12-11', 11, false);
        objPriceGrade.Channel__c = 'VOD';
        objPriceGrade.Format__c = 'HD';
        objPriceGrade.Type__c = 'MDP_VOD_ESRP';
        objPriceGrade.MM_RateCard_ID__c = '4';
        insert objPriceGrade;
        
        HEP_Price_Grade__c objPriceGrade2 = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(NULL, '13-12', 12, false);
        objPriceGrade2.Type__c = 'MDP_VOD_ESRP';
        objPriceGrade.Format__c = 'HD';
        objPriceGrade2.MM_RateCard_ID__c = '4';
        insert objPriceGrade2;
        
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        insert objPromotion1;
        //Create Catalog Record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory1.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory1.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
        objSKUMaster.Record_Status__c  = 'Active'; 
        insert objSKUMaster;
        HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Tst',objTerritory1.Id,null,false);
        insert objSKUTemplate;
        //Create Promotion Catalog Record
        HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemplate.Id,false);
        insert objPromoCatalog;
        //Create Promotion SKU Record
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Draft','Unlocked',false);
        insert objPromoSKU;
        HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id,objSKUMaster.Id,objTerritory1.Id,objPromoSKU.Id,NULL,'Active',true);
        test.startTest();
        HEP_SKU_Promotion_Creation_Canada objPromoCanada = new HEP_SKU_Promotion_Creation_Canada();
        objPromoCanada.promotionId = objPromotion1.Id;
        objPromoCanada.fetchDetails();
        test.stopTest();
    }
}