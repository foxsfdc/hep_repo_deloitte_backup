/**
* HEP_INT_DheSku_Queueable --- 
* @author Avinash Harwani
*/
public class HEP_INT_DheSku_Queueable implements Queueable, Database.AllowsCallouts {
    
    Map<String,HEP_INT_DheSkuInvoiceWrapperUtility> mapClassToObjWrap;
    String cronId;
    Map<String,String> mapClassToInterfaces;
    HEP_INT_DheSkuInvoiceWrapperUtility obj;
    List<String> classNames = new List<String>{'HEP_INT_DheSkuMaster_Details','HEP_INT_DheSkuPrice_Details','HEP_INT_DheSkuBom_Details','HEP_INT_DheSkuCustomer_Details'};
    /**
    * Class constructor to initialize the class variables
    * @return nothing 
    * @author Avinash Harwani 
    */  
    public HEP_INT_DheSku_Queueable(Map<String,String> mapClassToInterfaces, Map<String,HEP_INT_DheSkuInvoiceWrapperUtility> mapClassToObjWrap, String cronId){
        this.mapClassToInterfaces = mapClassToInterfaces;
        this.mapClassToObjWrap = mapClassToObjWrap;
        this.cronId = cronId;
    }
    
    /**
    * execute -- 
    * @param  Takes no input 
    * @return nothing   
    * @author Avinash Harwani      
    */
    public void execute(QueueableContext qContext) {
        //First execute only for Master details
        String className = '';
        for(String cName : classNames){
            if(mapClassToObjWrap.containsKey(cName)){
                className = cName;
                break;
            }
        }
        System.debug('****** className  ****** ' + className );
        obj = mapClassToObjWrap.get(className);
        Integer iRetryInterval = executeInterfaceCallouts(className);
        HEP_Services__c service = HEP_Services__c.getInstance(mapClassToInterfaces.get(className));
        Datetime dtCurrentBatchTime = System.Now();
        service.Hep_Jde_Time__c = appendZero(String.valueOf(dtCurrentBatchTime.hour())) + appendZero(String.valueOf(dtCurrentBatchTime.minute())) + appendZero(String.valueOf(dtCurrentBatchTime.second()));
        service.Hep_Jde_Date__c = appendZero(String.valueOf(dtCurrentBatchTime.Year())) + appendZero(String.valueOf(dtCurrentBatchTime.month())) + appendZero(String.valueOf(dtCurrentBatchTime.day()));
        //Update custom setting for next callout
        update service;
        mapClassToObjWrap.remove(className);
        
        //Abort current job and schedule a new job after the last callout is made
        if(cronId != null && className.equalsIgnoreCase(HEP_INT_DheSkuCustomer_Details.class.getName())){
            System.debug('cronId ' + cronId);
            System.abortJob(cronId);
        
            //Instantiate the Scheduler Class
            HEP_INT_DheSkuDetails_Scheduler objScheduler = new HEP_INT_DheSkuDetails_Scheduler();  
            
            //Fetch the add minutes from the Interface Record that determines the time interval in which the batch job runs for Next scheduling.
            if(System.Label.HEP_Integ_Retry_Interval != null){
                dtCurrentBatchTime = dtCurrentBatchTime.addMinutes(iRetryInterval); 
                String sScheduledRuntimeExpression = String.valueOf(dtCurrentBatchTime.second()) + ' ' + String.valueOf(dtCurrentBatchTime.minute()) + ' ' + String.valueOf(dtCurrentBatchTime.hour()) + ' '  + String.valueOf(dtCurrentBatchTime.day()) +  ' ' + String.valueOf(dtCurrentBatchTime.month()) + ' '  + '? ' + String.valueOf(dtCurrentBatchTime.Year());
                
                //Schedule the Batch from Scheduler
                if(!Test.isRunningTest())
                    System.schedule(HEP_INT_DheSkuDetails_Scheduler.class.getName() + ' ' + String.valueof(dtCurrentBatchTime), sScheduledRuntimeExpression, objScheduler);
            }
        } else {
            if(!mapClassToObjWrap.isEmpty())
                System.enqueueJob(new HEP_INT_DheSku_Queueable(mapClassToInterfaces,mapClassToObjWrap, cronId));
        }
        
    }
    
    /**
    * execute -- 
    * @param  Takes no input 
    * @return Retry Interval   
    * @author Avinash Harwani      
    */
    public Integer executeInterfaceCallouts(String intMethod) {
        HEP_InterfaceTxnResponse intResponse = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(obj),'',intMethod);
        //Incremental logic
        /*if(intResponse.iNoOfRecords == Integer.valueOf(Label.HEP_JDE_PAGINATION)){
            obj.sStartingFrom += intResponse.iNoOfRecords;
            executeInterfaceCallouts(intMethod);
        }*/
        return intResponse.iRetryInterval;
    }
    
    public String appendZero(string param){
        if(param.length() == 1) param = '0'+param;
        return param;
    }
    
}