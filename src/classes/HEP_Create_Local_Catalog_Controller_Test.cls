@isTest
public class HEP_Create_Local_Catalog_Controller_Test{
    public static testmethod void fetchLocalCatalogDefaultValues_Test(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        objPromotion1.Territory__c = objTerritory1.Id;
        objPromotion1.Promotion_Type__c = 'National';
        insert objPromotion1;
        HEP_List_Of_Values__c objLOV = new HEP_List_Of_Values__c();
        objLOV.Name = 'Name';
        objLOV.Record_Status__c = 'Active';
        objLOV.Type__c = 'FOX_GENRE';
        objLOV.Values__c = 'Action';
        objLOV.Order__c = '1';
        objLOV.Parent_Value__c = 'ACTN';
        insert objLOV;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.fetchLocalCatalogDefaultValues(objPromotion1.Id);
        //HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }
    public static testmethod void fetchLocalCatalogDefaultValues_Test2(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create Test Role 
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create test User Role record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        objPromotion1.Territory__c = objTerritory1.Id;
        //objPromotion1.Promotion_Type__c = 'National';
        insert objPromotion1;
        HEP_List_Of_Values__c objLOV = new HEP_List_Of_Values__c();
        objLOV.Name = 'Name';
        objLOV.Record_Status__c = 'Active';
        objLOV.Type__c = 'FOX_GENRE';
        objLOV.Values__c = 'Action';
        objLOV.Order__c = '1';
        objLOV.Parent_Value__c = 'ACTN';
        insert objLOV;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.fetchLocalCatalogDefaultValues(objPromotion1.Id);
        //HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }
    public static testmethod void fetchLocalCatalogDefaultValues_Test1(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        objPromotion1.Territory__c = objTerritory1.Id;
        objPromotion1.Promotion_Type__c = 'National';
        insert objPromotion1;
        HEP_List_Of_Values__c objLOV = new HEP_List_Of_Values__c();
        objLOV.Name = 'Name';
        objLOV.Record_Status__c = 'Active';
        objLOV.Type__c = 'FOX_LICENSOR';
        objLOV.Values__c = 'Action';
        objLOV.Order__c = '1';
        objLOV.Parent_Value__c = 'ACTN';
        insert objLOV;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.fetchLocalCatalogDefaultValues(objPromotion1.Id);
        //HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }
    public static testmethod void getTerritoriesDefaultValues_Test(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        objPromotion1.Territory__c = objTerritory1.Id;
        objPromotion1.Promotion_Type__c = 'National';
        insert objPromotion1;
        //Create List of Values Records
        HEP_List_Of_Values__c objLOV = new HEP_List_Of_Values__c();
        objLOV.Name = 'Name';
        objLOV.Record_Status__c = 'Active';
        objLOV.Type__c = 'FOX_LICENSOR_GROUP';
        objLOV.Values__c = 'Action';
        objLOV.Order__c = '1';
        objLOV.Parent_Value__c = 'ACTN';
        insert objLOV;
        HEP_List_Of_Values__c objLOV1 = new HEP_List_Of_Values__c();
        objLOV1.Name = 'Name';
        objLOV1.Record_Status__c = 'Active';
        objLOV1.Type__c = 'FOX_LICENSOR_TYPE';
        objLOV1.Values__c = 'Action';
        objLOV1.Order__c = '2';
        objLOV1.Parent_Value__c = 'US,DHE';
        insert objLOV1;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }
    public static testmethod void getGlobalCatalogs_Test(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create Catalog Record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory1.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        HEP_Create_Local_Catalog_Controller.getTypeAheadValues('PromotionName__c','HEP_Promotion__c','Global Promotion','PromotionName__c LIKE \'%Promotion%\''); 
        insert objCatalog;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.getGlobalCatalogs('Primary Catalog');
        test.stopTest();
    }
    public static testmethod void saveLocalCatalog_Test(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory1.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','TV',false);
        insert objLOB;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        objPromotion1.LineOfBusiness__c = objLOB.Id;
        objPromotion1.Territory__c = objTerritory1.Id;
        objPromotion1.Promotion_Type__c = 'National';
        insert objPromotion1;
        //Create Catalog Record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory1.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        HEP_Catalog__c objCatalog1 = new HEP_Catalog__c();
        objCatalog1.Catalog_Name__c = 'Global Catalog';
        objCatalog1.Product_Type__c = 'TV';
        objCatalog1.Catalog_Type__c = 'Bundle';
        objCatalog1.Territory__c = objTerritory1.Id;  
        objCatalog1.Record_Status__c  = 'Active';
        objCatalog1.Status__c = 'Pending';
        objCatalog1.Type__c = 'Master';
        objCatalog1.Unique_Id__c = '78989/US';
        insert objCatalog1;
        //Creating Product Type record for Title -> Season
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        //Create Test Role 
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create test User Role record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory1.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Creating Title Record
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'CNFDL';
        objTitle.FIN_PROD_ID__c = 'FIN_PROD_ID__c';
        insert objTitle;
        HEP_Create_Local_Catalog_Controller.LocalCatalogWrapper objLocCatalogWrap = new HEP_Create_Local_Catalog_Controller.LocalCatalogWrapper(objCatalog.Id,'tst cat name','id','single','78989','1999');
        HEP_Create_Local_Catalog_Controller.LicensorTypeGroupWrapper objLicensorTypeGroup = new HEP_Create_Local_Catalog_Controller.LicensorTypeGroupWrapper('Canal Plus','Licensor Group');
        //HEP_Create_Local_Catalog_Controller.LicensorTypeWrapper objLicensorType = new HEP_Create_Local_Catalog_Controller.LicensorTypeWrapper('GIBRO FILMS','GIBRO FILMS');
        HEP_Create_Local_Catalog_Controller.LicensorWrapper objLicensorWrap = new HEP_Create_Local_Catalog_Controller.LicensorWrapper('GIBRO FILMS','GIBRO FILMS');
        HEP_Create_Local_Catalog_Controller.TerritoryWrapper objTerrWrap = new HEP_Create_Local_Catalog_Controller.TerritoryWrapper(objTerritory1.Id,'US');
        HEP_Create_Local_Catalog_Controller.LocalCatalogPageData objPageData = new HEP_Create_Local_Catalog_Controller.LocalCatalogPageData();
        objPageData.sFTID = 'FIN_PROD_ID__c';
        objPageData.sTerritory = objTerritory1.Id;
        objPageData.objTerritory = objTerrWrap;
        objPageData.objGlobalCatalogNameOrNumber = objLocCatalogWrap;
        objPageData.objLicensorTypeGroup = objLicensorTypeGroup;
        objPageData.objLicensor = objLicensorWrap;
        //HEP_Create_Local_Catalog_Controller.LocalGenreWrapper objLocalGenreWrap = new HEP_Create_Local_Catalog_Controller.LocalGenreWrapper();
        objPageData.sLocalCatalogName = 'Local Catalog';
        test.startTest();
        System.runAs(u){
            HEP_Create_Local_Catalog_Controller.saveLocalCatalog(objPageData,objPromotion1.Id);
        }
        test.stopTest();
    }
    public static testmethod void fetchLocalCatalogDefaultValues_Test3(){
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        test.startTest();
        HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues('objTerritory1.Id','Mars');
        //HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }
    public static testmethod void fetchLocalCatalogDefaultValues_Exception_Test(){
        test.startTest();
        String sPromoId = 'Testing Done';
        try{
        HEP_Create_Local_Catalog_Controller.fetchLocalCatalogDefaultValues(sPromoId);
        } catch(Exception e) {}
        //HEP_Create_Local_Catalog_Controller.getTerritoriesDefaultValues(objTerritory1.Id,'US');
        test.stopTest();
    }

}