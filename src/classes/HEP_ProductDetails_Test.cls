@isTest
private class HEP_ProductDetails_Test {
    /**
    * HEP_ProductDetails_SuccessTest -- Test method for the Successful Response
    * @return  :  no return value
    * @author  :  Abhishek Kumar
    */
    @isTest static void HEP_ProductDetails_SuccessTest() {
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Product','HEP_ProductDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse  = HEP_ExecuteIntegration.executeIntegMethod('108544','','HEP_Foxipedia_Product');
        System.assertEquals('HEP_Foxipedia_Product',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_ProductDetails_FailureTest -- Test method in case of Error
    * @return  :  no return value
    * @author  :  Abhishek Kumar
    */
    @isTest static void HEP_ProductDetails_FailureTest() {
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Product','HEP_ProductDetails',true,10,10,'Outbound-Pull',true,true,true);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse  = HEP_ExecuteIntegration.executeIntegMethod('pd108544','','HEP_Foxipedia_Product');
        System.assertEquals('HEP_Foxipedia_Product',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_ProductDetails_InvalidAcessTokenTest -- Test method for Invalid Access Token
    * @return  :  no return value
    * @author  :  Abhishek Kumar
    */
    @isTest static void HEP_ProductDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'ad108544'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_ProductDetails objProductDetails = new HEP_ProductDetails();
        objProductDetails.performTransaction(objTxnResponse);
        Test.stoptest();
    }   
    /**
    * HEP_ProductCatalogWrapper_Test -- Test method for Wrapper
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_ProductCatalogWrapper_Test() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_ProductCatalogWrapper  objProductCatalogDetails = new HEP_ProductCatalogWrapper();
        Test.starttest ();
        objProductCatalogDetails.TxnId=null;
        objProductCatalogDetails.calloutStatus=null; 
        HEP_ProductCatalogWrapper.Attributes objAttributes = new HEP_ProductCatalogWrapper.Attributes();
        objAttributes.type = 'TITLE';
        objAttributes.foxId  = '363029';
        objAttributes.primaryClassificationCode = '363029';
        objAttributes.productTypeCode = '690061';
        objAttributes.productTypeDescription = '67728';
        objAttributes.productId = '363029';
        objAttributes.productName = 'New Zealand';
        objAttributes.productStatusCode = 'NZ';
        objAttributes.productStatusDescription = 'Home Entertainment';
        objAttributes.globalEpisodeConfigIndicator = 'VID';
        objAttributes.productStatusComment = 'OFLC - Office of Film and Literature Classification';
        objAttributes.productComment = 'OFLC';
        objAttributes.primaryCatalogId = 'R16';
        objAttributes.primaryCatalogName = 'OFLC-R16';
        objAttributes.primaryClassificationDescription  = null;
        list<HEP_ProductCatalogWrapper.Data> lstData = new list<HEP_ProductCatalogWrapper.Data>();
        HEP_ProductCatalogWrapper.Data objData = new HEP_ProductCatalogWrapper.Data();
        objData.attributes = objAttributes;
        objData.type = 'Product';
        objProductCatalogDetails.errors=null;
        HEP_ProductCatalogWrapper.Links objLinks = new HEP_ProductCatalogWrapper.Links();
        objLinks.related = '/foxipedia/global/api/title/details?foxId=67728';
        HEP_ProductCatalogWrapper.Title objTtle = new HEP_ProductCatalogWrapper.Title();
        objTtle.links = objLinks;
        HEP_ProductCatalogWrapper.Errors objErrors = new HEP_ProductCatalogWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_ProductCatalogWrapper.Relationships objRelationships = new HEP_ProductCatalogWrapper.Relationships();
        objRelationships.title = null;
        Test.stoptest();
    }
}