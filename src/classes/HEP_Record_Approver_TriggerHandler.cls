/**
 * HEP_Record_Approver_TriggerHandler ---  Helper Class to handle context-Specific triggers
 * @author  Nishit Kedia
 */
public class HEP_Record_Approver_TriggerHandler {
  
    //Global constants for Approval f/W
    //Global constants for Approval f/W
    public static String sPendingStatus;
    public static String sApprovedStatus;
    public static String sRejectedStatus;
    public static string sACTIVE;
     
    static{    
        sPendingStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        sApprovedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sRejectedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
  
  //Member Variable
  //Set of Approval Role Ids
  public Set<Id> setApproverRoleIds;
  
  public HEP_Record_Approver_TriggerHandler(){
        setApproverRoleIds = new Set<Id>();
  }
    /**
    * handleAfterInsert --- Method to handle After insert Scenario on Record Approver.
    * @param lstRecordApprovers is the list of Approvers being Inserted
    * @return nothing
    * @author    Nishit Kedia
    */ 
    public void handleAfterInsert(List<HEP_Record_Approver__c> lstRecordApprovers){
      System.debug('lstRecordApprovers '+lstRecordApprovers.size());
        try{
            if(lstRecordApprovers != null && !lstRecordApprovers.isEmpty()){
                
                //Map that holds Key-->Search Id and value-->Set of Role Ids
                Map<String,Set<Id>> mapApproverRolesToNotify = new Map<String,Set<Id>>();
                
                
                //Set to store the Inserteed Record Approvers Id
                Set<Id> setRecordApproversId = new Set<Id>();                
                for(HEP_Record_Approver__c ObjApprover:lstRecordApprovers){
                    setRecordApproversId.add(ObjApprover.Id);
                }
                
               /* Store the Record Approvers in Map only for those Users  which are Currently Assigned in Approval Chain(isactive=true)
                * and where the Approval Status is Set to pending when created Initially
                 */
                Map<Id,HEP_Record_Approver__c> mapRecordApprovers = new Map<Id,HEP_Record_Approver__c>([ Select  Id,
                                                                                                                 Approval_Record__c,
                                                                                                                 Approver__c,
                                                                                                                 Search_Id__c,
                                                                                                                 Approver_Role__c,
                                                                                                                 IsActive__c,
                                                                                                                 Sequence__c,
                                                                                                                 Status__c
                                                                                                                 from HEP_Record_Approver__c
                                                                                                                 where Id=:setRecordApproversId
                                                                                                                 And IsActive__c=true
                                                                                                                 And Status__c=:sPendingStatus
                                                                                                                 AND Record_Status__c =:sACTIVE]);
                // Check whether map is generated correctly
                if(mapRecordApprovers != null && !mapRecordApprovers.isEmpty()){
                  
                    // Iteriate over Map's keySet                                                               
                    for(Id approver:mapRecordApprovers.keySet()){  
                      //Fetch the Record Approver                 
                        HEP_Record_Approver__c objEligibleApprover = mapRecordApprovers.get(approver);
                        //Generate Approval record as Key
                        String approvalSearchIdKey = objEligibleApprover.Search_Id__c;
                         //Add the next Sequence Of Approver Role Id
                        setApproverRoleIds.add(objEligibleApprover.Approver_Role__c);
                        
                        if(mapApproverRolesToNotify.containskey(approvalSearchIdKey)){
                            mapApproverRolesToNotify.get(approvalSearchIdKey).add(objEligibleApprover.Approval_Record__c);
                        }else{
                            mapApproverRolesToNotify.put(approvalSearchIdKey, new Set<Id>{objEligibleApprover.Approval_Record__c});
                        }
                    }
                    
                    //Send the Map for Processing Alerts and Emails Notifications
                    if(mapApproverRolesToNotify != null && !mapApproverRolesToNotify.isEmpty()){
                        
                        //Map that holds Key--> Approval Id and value-->Set of Approver User Ids
                        Map<Id,Set<Id>> mapApproversToNotify = HEP_Approval_Utility.fetchApproversToNotify(mapApproverRolesToNotify);
                        System.debug('mapApproversToNotify----->'+ mapApproversToNotify);
                        HEP_Queue_Email_Alerts instQueueableEmail =  new HEP_Queue_Email_Alerts(mapApproversToNotify,setApproverRoleIds);
                        instQueueableEmail.execute(null);
                        //System.enqueueJob(new HEP_Queue_Email_Alerts(mapApproversToNotify,setApproverRoleIds));
                    }
                }                                                                            
            }
        }catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured In After Insert of Approval records','DML Errors',objException,'HEP_Record_Approver_TriggerHandler','handleAfterInsert',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }
    }
    
    
    /**
    * handleAfterUpdate --- Method to handle After Update Scenario on Record Approver.
    * @param lstRecordApprovers is the list of Approvers being Update in a Txn
    * @return nothing
    * @author    Nishit Kedia
    */ 
    public void handleAfterUpdate(List<HEP_Record_Approver__c> lstRecordApprovers){
        
        try{
            if(lstRecordApprovers != null && !lstRecordApprovers.isEmpty()){
              
                //Map that holds Key--> Approval Id and value-->Set of Approvers User Ids when Record has been Finally Approved At all Levels
                Map<Id,Set<Id>> mapApproversToNotify = new Map<Id,Set<Id>>();
                //Map that holds Key--> Approval Id and value-->Set of Approvers User Ids when Record has been Rejected At Any Level
                Map<Id,Set<Id>> mapRejectorsToNotify = new Map<Id,Set<Id>>();
                //Map that holds Key--> Approval Id and value-->Set of Approvers User Ids when Record has been Finally Approved In between Approval Chain
                Map<Id,Set<Id>> mapIntermediateRolesToNotify = new Map<Id,Set<Id>>();
                
                //Set to store the Updated Record Approvers Id
                Set<Id> setRecordApproversId = new Set<Id>();
                for(HEP_Record_Approver__c ObjApprover:lstRecordApprovers){
                    setRecordApproversId.add(ObjApprover.Id);
                }
                
                /* Store the Record Approvers in Map only for those Users  which are Currently Assigned in Approval Chain(isactive=true)
                * and where the Approval Status is Set toApproved/Rejected: On 2nd Fire it this Map should return null and Stop Trigger Handler
                 */
                Map<Id,HEP_Record_Approver__c> mapRecordApprovers = new Map<Id,HEP_Record_Approver__c>([ Select  Id,
                                                                                                                 Approval_Record__c,
                                                                                                                 Approval_Record__r.Approved_Count__c,
                                                                                                                 Approval_Record__r.Total_Approval_Count__c,
                                                                                                                 Approval_Record__r.Rejected_Count__c,
                                                                                                                 Approval_Record__r.CreatedById,
                                                                                                                 Approver__c,
                                                                                                                 Approval_Record__r.Prior_Submitter__c,
                                                                                                                 Approver_Role__c,
                                                                                                                 Search_Id__c,
                                                                                                                 IsActive__c,
                                                                                                                 Sequence__c,
                                                                                                                 Status__c
                                                                                                                 from HEP_Record_Approver__c
                                                                                                                 where Id =: setRecordApproversId
                                                                                                                 And
                                                                                                                 Approval_Record__r.Approval_Status__c =:sPendingStatus
                                                                                                                 And IsActive__c=true
                                                                                                                 AND Record_Status__c =:sACTIVE
                                                                                                                 And (Status__c=:sApprovedStatus 
                                                                                                                 OR  Status__c=:sRejectedStatus)]);
                // Check whether map is generated correctly                                                     
                if(mapRecordApprovers != null && !mapRecordApprovers.isEmpty()){                                                                         
                    for(Id approver:mapRecordApprovers.keySet()){
                        //fetch approvers                                           
                        HEP_Record_Approver__c objApprover = mapRecordApprovers.get(approver);
                        String sPriorSubmitters = objApprover.Approval_Record__r.Prior_Submitter__c;
                        Id approvalRecordKey = objApprover.Approval_Record__c;
                          
                        // Store the Approval id with rejected Status in One Map
                        if(objApprover.Status__c.equalsIgnoreCase(sRejectedStatus)){
                          // The notification should be sent to the Requestor on Final Approval
                            mapRejectorsToNotify.put(approvalRecordKey, new set<Id>{objApprover.Approval_Record__r.CreatedById});
                            
                            //Also add the prior Submitters if any who requested:
                            if(String.isNotBlank(sPriorSubmitters)){
                                Set<Id> setPriorUserIds = new Set<Id>();
                                for(Id sPriorSubmitterUserId : sPriorSubmitters.split(',')){
                                    setPriorUserIds.add(sPriorSubmitterUserId);
                                }
                                mapRejectorsToNotify.get(approvalRecordKey).addAll(setPriorUserIds);
                            }
                            
                            //Add the next Sequence Of Approver Role Id
                            setApproverRoleIds.add(objApprover.Approver_Role__c);
                            
                                                        
                        }else if((objApprover.Approval_Record__r.Total_Approval_Count__c == objApprover.Approval_Record__r.Approved_Count__c + 1) && objApprover.Status__c.equalsIgnoreCase(sApprovedStatus)){
                           
                           // The notification should be sent to the Requestor On Final Rejection
                            mapApproversToNotify.put(approvalRecordKey, new set<Id>{objApprover.Approval_Record__r.CreatedById});
                            
                           //Also add the prior Submitters if any who requested:
                             if(String.isNotBlank(sPriorSubmitters)){
                                Set<Id> setPriorUserIds = new Set<Id>();
                                for(Id sPriorSubmitterUserId : sPriorSubmitters.split(',')){
                                    setPriorUserIds.add(sPriorSubmitterUserId);
                                }
                                mapApproversToNotify.get(approvalRecordKey).addAll(setPriorUserIds);
                            }
                            
                            //Add the next Sequence Of Approver Role Id
                            setApproverRoleIds.add(objApprover.Approver_Role__c);
                        }else{ 
                            
                            //Can be Multi Approvers for an Approval Record
                            if(mapIntermediateRolesToNotify.containskey(approvalRecordKey)){
                                mapIntermediateRolesToNotify.get(approvalRecordKey).add(objApprover.Approver_Role__c);
                            }else{
                                mapIntermediateRolesToNotify.put(approvalRecordKey,new Set<Id>{objApprover.Approver_Role__c});
                            }
                            
                        }
                    }
                    
                    ///Send the Rejectors Map for Processing Alerts and Emails Notifications 
                    if(mapRejectorsToNotify != null && !mapRejectorsToNotify.isEmpty()){
                        System.debug('Rejection List to be Notified--->' + mapRejectorsToNotify);
                        updateStatusOnParentApproval(mapRejectorsToNotify.KeySet(),sRejectedStatus);
                        System.enqueueJob(new HEP_Queue_Email_Alerts(mapRejectorsToNotify, setApproverRoleIds));
                        
                    }
                    
                    //Send the Approved Map for Processing Alerts and Emails Notifications
                    if(mapApproversToNotify != null && !mapApproversToNotify.isEmpty()){
                        System.debug('Approved List to be Notified--->' + mapApproversToNotify);
                        updateStatusOnParentApproval(mapApproversToNotify.keySet(),sApprovedStatus);
                        System.enqueueJob(new HEP_Queue_Email_Alerts(mapApproversToNotify, setApproverRoleIds));
                    }
                    
                    if(mapIntermediateRolesToNotify !=null && !mapIntermediateRolesToNotify.isEmpty()){
                        //make a Map who all will be the Approvers in Next Sequence:These will be notified
                        Map<Id,HEP_Record_Approver__c> mapNextSequenceOfRoleApprovers = fetchNextSequenceOfApprovers(mapIntermediateRolesToNotify.keySet());
                        //make a Map who all will be the Approvers in Next Sequence:These will be notified
                        Map<String,Set<Id>> mapNextApproverRolesInSequence = new Map<String,Set<Id>>();
                        //List of Next Approvers to be Updated
                        List<HEP_Record_Approver__c> lstApproverSequenceToUpdate = new List<HEP_Record_Approver__c>();
                            
                        for(Id approvalRecordId:mapNextSequenceOfRoleApprovers.keySet()){
                            //fetch the Approver                        
                            HEP_Record_Approver__c objRecordApprover=mapNextSequenceOfRoleApprovers.get(approvalRecordId);
                            String searchIdKey = objRecordApprover.Search_Id__c;
                            
                             //Can be Multi Approvers for an Approval Record
                            if(mapNextApproverRolesInSequence.containskey(searchIdKey)){
                                mapNextApproverRolesInSequence.get(searchIdKey).add(objRecordApprover.Approval_Record__c);
                            }else{
                                mapNextApproverRolesInSequence.put(searchIdKey, new Set<Id>{objRecordApprover.Approval_Record__c});
                            }
                            
                            //make the Next Approver as Active
                            objRecordApprover.IsActive__c = true;
                            lstApproverSequenceToUpdate.add(objRecordApprover);
                        }
                        //Send the Intermediate Level Map for Processing Alerts and Emails Notifications
                        if(!mapNextApproverRolesInSequence.isEmpty()){
                            System.debug('Intermediate List to be Notified--->' + mapNextApproverRolesInSequence);
                            Map<Id,Set<Id>> mapNextApproversInSequence = HEP_Approval_Utility.fetchApproversToNotify(mapNextApproverRolesInSequence);
                            System.enqueueJob(new HEP_Queue_Email_Alerts(mapNextApproversInSequence, setApproverRoleIds));
                            
                            //This fires the after update Trigger Again as Next Sequence needs to be Activated
                            if(lstApproverSequenceToUpdate != null && !lstApproverSequenceToUpdate.isEmpty()){
                                update lstApproverSequenceToUpdate;
                            }
                        }
                    }               
                }                                                                                                                    
            }                                                                            
        } catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured In After Update of Approval records','DML Errors',objException,'HEP_Record_Approver_TriggerHandler','handleAfterUpdate',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }
    }
    
    
    /**
    * fetchNextSequenceOfApprovers --- Method to fetch next sequence of Approvers in Approval Chain
    * @param setApprovalId is the Set of ApprovalIds for which Next Sequence of Approvers are determined
    * @param mapNextSequenceOfApprovers is a map with (Key-->Approval id, value-->next Approver object)
    * @return nothing
    * @author    Nishit Kedia
    */
    public Map<Id,HEP_Record_Approver__c> fetchNextSequenceOfApprovers(Set<Id> setRecordApprovalId){
      
      //Map to Reurn from this Method
        Map<Id,HEP_Record_Approver__c> mapNextSequenceOfApprovers = new Map<Id,HEP_Record_Approver__c>();
        
        if(setRecordApprovalId != null && !setRecordApprovalId.isEmpty()){
          //Fetch the Next Sequence on Basis of  Set of ApprovalIds and pending Status and ordering of Approval id and Next Sequence
            Map<Id,HEP_Record_Approver__c> mapFetchNextApprovers = new Map<Id,HEP_Record_Approver__c>([ Select   Id,
                                                                                                                 Approval_Record__c,
                                                                                                                 Approver__c,
                                                                                                                 Approver_Role__c,
                                                                                                                 IsActive__c,
                                                                                                                 Search_Id__c,
                                                                                                                 Sequence__c,
                                                                                                                 Status__c
                                                                                                                 from HEP_Record_Approver__c
                                                                                                                 where Approval_Record__c =: setRecordApprovalId
                                                                                                                 And Status__c =: sPendingStatus
                                                                                                                 AND Record_Status__c =:sACTIVE 
                                                                                                                 Order by Approval_Record__c,Sequence__c asc ]);
                                                                                                 
            System.debug('Next Approvers in Map-->' + mapFetchNextApprovers);
            if(mapFetchNextApprovers != null && !mapFetchNextApprovers.isEmpty()){
              //Iteriate over map Keyset                                                                       
                for(Id approver:mapFetchNextApprovers.keySet()){
                  //Store the record approver object                 
                    HEP_Record_Approver__c objNextApprover = mapFetchNextApprovers.get(approver);
                    Id approvalRecordIdKey=objNextApprover.Approval_Record__c;                   
                    //Approvers next in Approval Chain will have Status as Pending
                    if(objNextApprover.Status__c.equalsIgnoreCase(sPendingStatus)){
                        //Only populate the next approver and Skip the Rest of Approvers
                        if(!mapNextSequenceOfApprovers.containskey(approvalRecordIdKey)){
                            mapNextSequenceOfApprovers.put(approvalRecordIdKey,objNextApprover);
                            //Add the next Sequence Of Approver Role Id
                            setApproverRoleIds.add(objNextApprover.Approver_Role__c);
                        }
                    }                       
                }                                                                                                                    
            }
        }
        return mapNextSequenceOfApprovers;                                                               
    }
    
    
    /**
    * updateStatusOnParentApproval --- Method to Update The Status field of parent Approval on which Approval was Invoked.
    * @param setApprovalId is the Set of ApprovalIds for which Parent object Approvals will  be updated
    * @param sApprovalStatus is the Status field implying that record is Either Approved/Rejected
    * @return nothing
    * @author    Nishit Kedia
    */ 
    public void updateStatusOnParentApproval(Set<Id> setApprovalId, String sApprovalStatus){
        
        Savepoint objSavepoint = Database.setSavepoint();
        try{
            if(setApprovalId != null && !setApprovalId.isEmpty() && String.isNotBlank(sApprovalStatus) ){
              
              //parent Approval List rquired to update Approved/rejected  Status on Approval Object
                List<HEP_Approvals__c> lstApprovals = new List<HEP_Approvals__c>();
                //Parent Sobject which will contain Parent record on which approval was invoked
                List<Sobject> lstParentApprovals = new List<Sobject>();
                //Fetch the Approvals on basis of passed set of Approval Id
                Map<Id,HEP_Approvals__c> mapApprovalRecords = new Map<Id,HEP_Approvals__c>([ Select  Id,                                                                                                    
                                                                                                     Approval_Status__c,
                                                                                                     Record_ID__c,
                                                                                                     Approval_Type__r.Approved_Status__c,
                                                                                                     Approval_Type__r.Approval_Field__c,
                                                                                                     Approval_Type__r.Rejected_Status__c                                                                                                    
                                                                                                     from HEP_Approvals__c
                                                                                                     where Id =: setApprovalId
                                                                                                     AND Record_Status__c =:sACTIVE
                                                                                                     ]);
                //Checking Scenario when the Parent needs to be updated with Rejected Status                                                                                     
                if(sApprovalStatus.equalsIgnoreCase(sRejectedStatus)){
                    if(mapApprovalRecords != null && !mapApprovalRecords.isEmpty()){
                        for(Id approvalRecordId:mapApprovalRecords.keySet()){
                          //Parent Approval Processing
                            HEP_Approvals__c objApproval = mapApprovalRecords.get(approvalRecordId);                            
                            objApproval.Approval_Status__c = sRejectedStatus;
                            lstApprovals.add(objApproval);
                            //Processing for Parent Sobject on on which approval was invoked
                            Id approvalIdRecord=Id.valueOf(objApproval.Record_ID__c);
                            sObject objDynamicSobject =approvalIdRecord.getSobjectType().newSObject(approvalIdRecord);
                            objDynamicSobject.put(objApproval.Approval_Type__r.Approval_Field__c,objApproval.Approval_Type__r.Rejected_Status__c);
                            lstParentApprovals.add(objDynamicSobject);            
                        }
                        
                    }
                }
                
                //Checking Scenario when the Parent needs to be updated with Rejected Status
                if(sApprovalStatus.equalsIgnoreCase(sApprovedStatus)){
                    if(mapApprovalRecords != null && !mapApprovalRecords.isEmpty()){
                        for(Id approvalRecordId:mapApprovalRecords.keySet()){
                          //Parent Approval Processing
                            HEP_Approvals__c objApproval = mapApprovalRecords.get(approvalRecordId);                            
                            objApproval.Approval_Status__c = sApprovedStatus;
                            lstApprovals.add(objApproval);
                            Id approvalIdRecord=Id.valueOf(objApproval.Record_ID__c);
                            //Processing for Parent Sobject on on which approval was invoked
                            sObject objDynamicSobject = approvalIdRecord.getSobjectType().newSObject(approvalIdRecord);
                            objDynamicSobject.put(objApproval.Approval_Type__r.Approval_Field__c,objApproval.Approval_Type__r.Approved_Status__c);
                            lstParentApprovals.add(objDynamicSobject);            
                        }
                        
                    }
                }   
                //update the parent Approval List                                                                                                     
                update lstApprovals;
                //update the Parent Sobject List
                update lstParentApprovals;                                                           
            }
        } catch (Exception objException) {
            Database.rollback(objSavepoint);
            HEP_Error_Log.genericException('Exception occured When Updating Parent Approval Record','DML Errors',objException,'HEP_Record_Approver_TriggerHandler','updateStatusOnParentApproval',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            throw objException;
        }
    }
}// end of TriggerHandler