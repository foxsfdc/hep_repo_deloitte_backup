/*------------------------------------Fox HEP----------------------------------
This class handles the business logic for inbound REST from Copper for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
public class HEP_ReleaseKeys_Copper_Inbound implements HEP_IntegrationInterface {
    public HEP_ReleaseKeys_Copper_Inbound() {

    }
    
    /**
    * @param objWrap - Integration Transaction Response Object
    * @return void 
    */
    public void performTransaction(HEP_InterfaceTxnResponse objWrap) {
        String modifiedSince = objWrap.sRequest; 

        Datetime lastModifiedDate; 
        boolean isUnix = modifiedSince.isNumeric();

        if (isUnix) { // Convert Unix datetime 
            lastModifiedDate = Datetime.newInstance(Long.valueOf(modifiedSince));
        }
        else { // Convert ISO datetime 
            lastModifiedDate = (DateTime) JSON.deserialize('"' + modifiedSince + '"', DateTime.class);
        }

        string sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        List<HEP_List_Of_Values__c> copperChannelObj = [SELECT Values__c FROM HEP_List_Of_Values__c WHERE Type__c = 'COPPER_CHANNEL' and record_status__C = :sACTIVE]; 
        List<HEP_List_Of_Values__c> copperFormatObj = [SELECT Values__c FROM HEP_List_Of_Values__c WHERE Type__c = 'COPPER_FORMAT' and record_status__C = :sACTIVE]; 
        Set<String> copperChannel = new Set<String>(); 
        Set<String> copperFormat = new Set<String>(); 

        for (HEP_List_Of_Values__c cc : copperChannelObj) {
            copperChannel.add(cc.Values__c); 
        }
        for (HEP_List_Of_Values__c cf : copperFormatObj) {
            copperFormat.add(cf.Values__c); 
        }

        List<HEP_Promotion_Dating__c> pdList = [
          SELECT  HEP_Catalog__r.CatalogId__c 
          FROM    HEP_Promotion_Dating__c
          WHERE   (LastModifiedDate >= :lastModifiedDate OR 
                  HEP_Catalog__r.LastModifiedDate >= :lastModifiedDate OR 
                  Promotion__r.LastModifiedDate >= :lastModifiedDate) AND 
                  Territory__r.Send_to_Prophet__c = true AND
                  Date__c != NULL AND 
                  Channel__c IN :copperChannel AND
                  Record_Status__c = :sACTIVE AND 
                  Customer__c = NULL AND
                  DateType__c LIKE '%Release Date%'  
                  //Status__c = 'Confirmed'
        ];

        List<HEP_SKU_Price__c> spList = [
          SELECT  SKU_Master__r.Catalog_Master__r.CatalogId__c 
          FROM    HEP_SKU_Price__c
          WHERE   (LastModifiedDate >= :lastModifiedDate OR 
                  Promotion_SKU__r.LastModifiedDate >= :lastModifiedDate OR 
                  Promotion_Dating__r.Promotion__r.LastModifiedDate >= :lastModifiedDate OR 
                  Promotion_Dating__r.LastModifiedDate >= :lastModifiedDate OR
                  Promotion_SKU__r.SKU_Master__r.LastModifiedDate >= :lastModifiedDate OR
                  SKU_Master__r.Catalog_Master__r.LastModifiedDate >= :lastModifiedDate) AND 
                  Promotion_Dating__r.Territory__r.Send_to_Prophet__c = true AND 
                  Promotion_Dating__r.Customer__c = NULL AND
                  Promotion_Dating__r.DateType__c LIKE '%Release Date%' AND
                  Promotion_SKU__r.SKU_Master__r.Channel__c IN :copperChannel AND 
                  Promotion_SKU__r.SKU_Master__r.Format__c IN :copperFormat AND
                  Promotion_SKU__r.SKU_Master__r.Type__c = 'Master' AND
                  Record_Status__c = :sACTIVE
        ];

        Set<Integer> catalogIDs = new Set<Integer>(); 
        for (HEP_Promotion_Dating__c pd : pdList) {
          if (pd.HEP_Catalog__r.CatalogId__c != null) {
              catalogIDs.add(Integer.valueOf(pd.HEP_Catalog__r.CatalogId__c)); 
          }
        }
        for (HEP_SKU_Price__c sp : spList) {
          if (sp.SKU_Master__r.Catalog_Master__r.CatalogId__c != null) {
            catalogIDs.add(Integer.valueOf(sp.SKU_Master__r.Catalog_Master__r.CatalogId__c)); 
          }
        }

        RestContext.response.addHeader('Content-Type', 'application/json');

        if (lastModifiedDate.addDays(30) >= DateTime.now()) {
          RestContext.response.responseBody = Blob.valueOf(JSON.serialize(catalogIDs));
          objWrap.sResponse = JSON.serialize(catalogIDs); 
        }
        else {
          String res = HEP_Constants__c.getValues('HEP_RK_INBOUND_30DAY_ERROR').Value__c; 
          RestContext.response.responseBody = Blob.valueOf(JSON.serialize(res));
          RestContext.response.statuscode = 404; 
          objWrap.sResponse = res; 
        }
        
    }
}