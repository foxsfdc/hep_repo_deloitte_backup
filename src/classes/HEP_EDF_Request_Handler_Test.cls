@isTest(seeAllData=False) 
public class HEP_EDF_Request_Handler_Test {
    static testmethod void testRequestHandler() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_EDF_Request_Handler objHandler = new HEP_EDF_Request_Handler();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/v1/HEP_EDF_Request_Handler/*';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type      
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "15047271","prntCtlgId": "15047271"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        RestContext.request = req;
		HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_EDF_Request_Handler.getEDFRequest();
		
    }
	static testmethod void testRequestHandlerForFMC() {
        HEP_EDF_Request_Handler objHandler = new HEP_EDF_Request_Handler();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/v1/HEP_EDF_Request_Handler/*';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type      
        String requestBody = '{"EventName": "FMCBroadcastEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "15047271","prntCtlgId": "15047271"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        RestContext.request = req;
        HEP_EDF_Request_Handler.getEDFRequest();
    }
	static testmethod void testRequestHandlerForProductionCompany() {
        HEP_EDF_Request_Handler objHandler = new HEP_EDF_Request_Handler();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/v1/HEP_EDF_Request_Handler/*';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type      
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "TitleChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "15047271","prntCtlgId": "15047271"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        RestContext.request = req;
        HEP_EDF_Request_Handler.getEDFRequest();
    }
	static testmethod void exceptionHandling() {
        HEP_EDF_Request_Handler objHandler = new HEP_EDF_Request_Handler();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/v1/HEP_EDF_Request_Handler/*';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type      
        String requestBody = Null;
        req.requestBody = Null;
        RestContext.request = req;
        HEP_EDF_Request_Handler.getEDFRequest();
    }
}