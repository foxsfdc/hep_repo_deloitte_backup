/**
* HEP_NTS_DHE_Invoice_Details -- purchase Order details will be stored from NTS And E1 Interfaces 
* @author    Ayan Sarkar
*/
public class HEP_NTS_DHE_Invoice_Details implements HEP_IntegrationInterface{ 
    /**
    * HEP_NTSInvoice_Details -- 
    * @return  :  no return value
    * @author  :  Ayan Sarkar
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        HEP_InvoiceWrapper objWrapper = new HEP_InvoiceWrapper();

        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        String sHEP_CURRENCY = HEP_Utility.getConstantValue('HEP_CURRENCY');
        String sHEP_REPLACE_CURRENCY = HEP_Utility.getConstantValue('HEP_REPLACE_CURRENCY');
        String sHEP_GL_LINE = HEP_Utility.getConstantValue('HEP_GL_LINE');
        String sHEP_REPLACE_GL_LINE_NO = HEP_Utility.getConstantValue('HEP_REPLACE_GL_LINE_NO');
        String sHEP_INVOICE = HEP_Utility.getConstantValue('HEP_INVOICE');
        String sHEP_REPLACE_INVOICE_NO = HEP_Utility.getConstantValue('HEP_REPLACE_INVOICE_NO');
        String sHEP_OBJECT = HEP_Utility.getConstantValue('HEP_OBJECT');
        String sHEP_REPLACE_OBJECT_NO = HEP_Utility.getConstantValue('HEP_REPLACE_OBJECT_NO');
        String sHEP_PO = HEP_Utility.getConstantValue('HEP_PO');
        String sHEP_REPLACE_PO_NO = HEP_Utility.getConstantValue('HEP_REPLACE_PO_NO');

        if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH') != null) 
            sAccessToken = HEP_Integration_Util.getAuthenticationToken(HEP_Utility.getConstantValue('HEP_NTS_E1_POOAUTH'));
        
        if(sAccessToken != null 
            && HEP_Constants__c.getValues('HEP_TOKEN_BEARER') != null && HEP_Utility.getConstantValue('HEP_TOKEN_BEARER') != null            
            && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) 
            && objTxnResponse != null){

            String sDetails = objTxnResponse.sSourceId;
            String sFromDate;
            String sToDate;
            String sLegalTitle;
            String sFinDivisionCode;
            String sFoxId;
            HEP_Services__c objServiceDetails;
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_NTSInvoiceParametersWrapper objNTSInvoiceWrapper = (HEP_NTSInvoiceParametersWrapper)JSON.deserialize(objTxnResponse.sSourceId ,HEP_NTSInvoiceParametersWrapper.class);
            if(objNTSInvoiceWrapper != null){
                //sCatalogNumber = objNTSInvoiceWrapper.sCatalogNumber;
                //sTerritoryE1Code = objNTSInvoiceWrapper.sTerritoryE1Code;
                sFromDate = objNTSInvoiceWrapper.sFromDate;
                sToDate = objNTSInvoiceWrapper.sToDate;
                sLegalTitle = objNTSInvoiceWrapper.sLegalTitle;
                sFinDivisionCode = objNTSInvoiceWrapper.sFinDivisionCode;
                sFoxId = objNTSInvoiceWrapper.sFoxId;

                objHttpRequest.setMethod('GET');
                objHttpRequest.setHeader('Authorization',sAccessToken);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details

                system.debug('objTxnResponse.sRequest ' + objTxnResponse.sRequest);
                if(HEP_Constants__c.getValues('HEP_NTS_E1_POOAUTH') != null && HEP_Utility.getConstantValue('HEP_NTS_DHE_E1_INVOICE_DETAILS') != null) 
                    objServiceDetails = HEP_Services__c.getValues(HEP_Utility.getConstantValue('HEP_NTS_DHE_E1_INVOICE_DETAILS'));   

                if(objServiceDetails != null){
                    //objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?TERRITORY=' + sTerritoryE1Code + '&CATALOG=' + sCatalogNumber+ '&FROM_DATE=' + sFromDate+ '&TO_DATE=' + sToDate);
                    objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?fromDate=' + sFromDate + '&toDate=' + sToDate+ '&legalTitle=' + sLegalTitle+ '&finDivisionCode=' + sFinDivisionCode+ '&foxId=' + sFoxId);
                    //objHttpRequest.setEndpoint('https://feg-devapi.foxinc.com/v1/finance/read/api/dom/nts/invoices?fromDate=118023&toDate=118123&legalTitle=ABCER&finDivisionCode=MGM&foxId=6985148');

                    system.debug('NTS DHE Invoice Endpoint ---->   ' + objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c + '?fromDate=' + sFromDate + '&toDate=' + sToDate+ '&legalTitle=' + sLegalTitle+ '&finDivisionCode=' + sFinDivisionCode+ '&foxId=' + sFoxId);

                    HTTP objHttp = new HTTP();                 
                    HTTPResponse objHttpResp = objHttp.send(objHttpRequest);

                    objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout

                    system.debug('1. DHE NTS Purchade Order Response ---- > ' + objTxnResponse.sResponse);

                    if(sHEP_SUCCESS != null && objHttpResp.getStatus().equals(sHEP_SUCCESS) && sHEP_CURRENCY != null && sHEP_REPLACE_CURRENCY != null  
                    && sHEP_GL_LINE != null && sHEP_REPLACE_GL_LINE_NO != null && sHEP_INVOICE != null && sHEP_REPLACE_INVOICE_NO != null  
                    && sHEP_OBJECT != null && sHEP_REPLACE_OBJECT_NO != null  && sHEP_PO != null && sHEP_REPLACE_PO_NO != null){
                        //Replacing the tags in the response receiving as we are using the generic wrapper
                        String sResponse = objHttpResp.getBody();
                        sResponse = sResponse.replace(sHEP_CURRENCY , sHEP_REPLACE_CURRENCY);
                        sResponse = sResponse.replace(sHEP_GL_LINE , sHEP_REPLACE_GL_LINE_NO);
                        sResponse = sResponse.replace(sHEP_INVOICE , sHEP_REPLACE_INVOICE_NO);
                        sResponse = sResponse.replace(sHEP_OBJECT , sHEP_REPLACE_OBJECT_NO);
                        sResponse = sResponse.replace(sHEP_PO , sHEP_REPLACE_PO_NO);
                        System.debug('2. DHE NTS Purchade Order Converted Response ---- > ' + JSON.serializePretty(sResponse));
                        objWrapper = (HEP_InvoiceWrapper)JSON.deserialize(sResponse, HEP_InvoiceWrapper.class);  
                        System.debug('3. DHE NTS Purchade Order Converted Response ---- > ' + JSON.serializePretty(objWrapper));
                        if(objWrapper.errors != null && objWrapper.errors[0] != null && String.isNotBlank(objWrapper.errors[0].detail)){
                            System.debug(objWrapper.errors[0].detail);
                            objTxnResponse.sStatus = sHEP_FAILURE;
                            if(objWrapper.errors[0].detail != null && objWrapper.errors[0].detail.length()>254)
                                objTxnResponse.sErrorMsg = objWrapper.errors[0].detail.substring(0, 254);
                            else
                                objTxnResponse.sErrorMsg = objWrapper.errors[0].detail;
                            objTxnResponse.bRetry = true;
                        }else{
                            if(HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success') != null && HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success') != null)        
                                objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                            objTxnResponse.bRetry = false;
                        }
                    }
                    else{
                        if(HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null)
                            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                        if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length()>254)
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                        else    
                            objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                        objTxnResponse.bRetry = true;
                    }                
                }
            }
            }
        else{
            if(HEP_Constants__c.getValues('HEP_INVALID_TOKEN') != null && HEP_Utility.getConstantValue('HEP_INVALID_TOKEN') != null 
                && HEP_Constants__c.getValues('HEP_FAILURE') != null && HEP_Utility.getConstantValue('HEP_FAILURE') != null ){
                System.debug('Invalid Access Token Or Invalid txnResponse data sent');
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
                objTxnResponse.bRetry = true;
            }    
        }        
    }    
    
     /**
    * HEP_INT_NTSInvoiceWrapper  -- Class to hold Input parameter details to be sent to the NTS and E1
    * @author    Ayan Sarkar
    */ 
    public class HEP_NTSInvoiceParametersWrapper {
        //public String sTerritoryE1Code;
        //public String sCatalogNumber;
        public String sFromDate;
        public String sToDate;
        public String sLegalTitle;
        public String sFinDivisionCode;
        public String sFoxId;
    }
}