/**
* HEP_E1_Queueable --- Class to perform callouts to Insert/Update data at E1 interface
* @author Himanshi Mayal
*/
public class HEP_E1_Queueable implements Queueable, Database.AllowsCallouts
{
    String sObjType; 
    String sJson; 
    String sTransactionId;
    List<Id> lstPrSKUIds;

    public HEP_E1_Queueable(String sJson, String sObjType)
    {   
        HEP_MarketSpendTriggerHandler.E1Wrapper objE1WrapperDetails = (HEP_MarketSpendTriggerHandler.E1Wrapper)JSON.deserialize(sJson,HEP_MarketSpendTriggerHandler.E1Wrapper.class);
        this.sObjType = sObjType;
        this.sJson = objE1WrapperDetails.sPromoId;
        this.sTransactionId = objE1WrapperDetails.sTransactionId;
    }
    public HEP_E1_Queueable(String sJson, String sObjType, List<Id> lstPrSKUIds)
    {
        HEP_MarketSpendTriggerHandler.E1Wrapper objE1WrapperDetails = (HEP_MarketSpendTriggerHandler.E1Wrapper)JSON.deserialize(sJson,HEP_MarketSpendTriggerHandler.E1Wrapper.class);
        this.sObjType = sObjType;
        //this.sJson = sJson;
        this.lstPrSKUIds = lstPrSKUIds;
        this.sJson = objE1WrapperDetails.sPromoId;
        this.sTransactionId = objE1WrapperDetails.sTransactionId;
    }
    /**
    * execute -- makes the callout and gets the JSessionId through response
    * @param   
    * @return  
    * @author Himanshi Mayal      
    */    
    public void execute(QueueableContext qContext) {
        system.debug('Inside execute E1 Queueable job');
        system.debug('sJson in execute integrate method ' + sJson);
        system.debug('sObjType in execute integrate method ' + sObjType);
        system.debug('sTransactionId in execute integrate method ' + sTransactionId);
        HEP_ExecuteIntegration.executeIntegMethod(sJson,sTransactionId,sObjType);
        
        // chaining Promotion SKU queueable
        if(!Test.isRunningTest() && lstPrSKUIds != null && lstPrSKUIds.size()>0){
            System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstPrSKUIds, ','),HEP_Constants__c.getValues('HEP_PROMOTION_SKU').Value__c));
        }   
    }
    /*public class E1Wrapper
    {
        public String promotionSkuId;
        public String spendId;
    }*/
}