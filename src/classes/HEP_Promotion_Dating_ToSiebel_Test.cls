@isTest
Public class HEP_Promotion_Dating_ToSiebel_Test{

    @testSetup 
    Static void setup(){
        
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, null, null,null,false);
        insert objPromotion;

        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;      

        HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,objTitle.id,objTerritory.id,false);
        objPromotionProduct.Product_End_Date__c = date.today()+30;
        objPromotionProduct.Product_Start_Date__c = date.today(); 
        objPromotionProduct.Approval_Status__c = 'Approved';
        objPromotionProduct.Record_Status__c = 'Active';
        objPromotionProduct.HEP_Catalog__c = objCatalog.id;
        objPromotionProduct.customer_id__c = '694505519';
        insert objPromotionProduct;
        
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, false);
        insert objDatingMatrix;
        
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.id,objPromotion.id,objDatingMatrix.id,False);
        objDatingRecord.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        objDatingRecord.Status__c =  HEP_Utility.getConstantValue('HEP_STATUS_CONFIRMED');
        insert objDatingRecord;
    }
    
    @isTest 
    static void Success_Test(){   
        HEP_Promotion_Dating__c objDatingRecord = [Select id from HEP_Promotion_Dating__c limit 1];
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objDatingRecord.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_Dating_ToSiebel demo = new HEP_Promotion_Dating_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }   

    @isTest 
    static void Failure_Test(){   
        HEP_Promotion_Dating__c objDatingRecord = [Select id from HEP_Promotion_Dating__c limit 1];
        HEP_Services__c objService = [Select id,Name from HEP_Services__c where Name = 'HEP_Siebel_OAuth'];
        objService.Endpoint_URL__c = '/oauth/oauth20/tokenFail';
        update objService;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objDatingRecord.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_Dating_ToSiebel demo = new HEP_Promotion_Dating_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }  

    @isTest 
    static void Access_Error_Test(){   
        HEP_Promotion_Dating__c objDatingRecord = [Select id from HEP_Promotion_Dating__c limit 1];
        HEP_Services__c objService = [Select id,Name from HEP_Services__c where Name = 'HEP_Siebel_Service'];
        objService.Service_URL__c = '/v1/edf/events/failure';
        update objService;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = objDatingRecord.Id; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Promotion_Dating_ToSiebel demo = new HEP_Promotion_Dating_ToSiebel();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();
        //System.assertEquals('Success', objTxnResponse.sStatus);
    }   
     
}