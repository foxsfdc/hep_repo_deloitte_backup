@isTest
Public class HEP_Foxipedia_Talent_Test{
    
    @testSetup
    static void testDataSetUpMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    

    /*public static testmethod void test(){
         String catalogFoxId = '5818497';
         HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(catalogFoxId, '', 'HEP_Foxipedia_Run');
         HEP_Foxipedia_Run obj = new HEP_Foxipedia_Run();
         obj.performTransaction(objIntTxnRsp);
    }*/
    
    
    @isTest 
    static void HEP_Foxipedia_Talent_InvalidAcessTokenTest() {
        
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer567854444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Talent demo = new HEP_Foxipedia_Talent();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
     @isTest 
     static void HEP_Foxipedia_Talent_SuccessTest() {
          
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = '67728'; 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Talent demo = new HEP_Foxipedia_Talent();
       // system.debug('TRANSAC RESPONSE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    @isTest 
    static void HEP_Foxipedia_Talent_FailureTest() {
        
      //  list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = 'aer567854444'; 
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Foxipedia_Talent demo = new HEP_Foxipedia_Talent();
    //    system.debug('TRANSAC RESPONSE FAILURE...' +objTxnResponse);
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
      /**
    * HEP_TalentDetails_WrapperTest -- Test method for the Talent Wrapper
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_TalentDetails_WrapperTest() {
        //list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        //list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Foxipedia_TalentWrapper  objTalentDetails = new HEP_Foxipedia_TalentWrapper ();
        Test.starttest ();
        objTalentDetails.TxnId=null;
        objTalentDetails.calloutStatus=null; 
        HEP_Foxipedia_TalentWrapper.Attributes objAttributes = new HEP_Foxipedia_TalentWrapper.Attributes();
        objAttributes.type = 'TITLE';
        objAttributes.rowIdObject  = '363029';
        objAttributes.talentTypeCode  = '363029';
        objAttributes.talentTypeDescription  = '690061';
        objAttributes.talentRoleSequence  = '67728';
        objAttributes.titleSequence  = '363029';
        objAttributes.characterName  = 'New Zealand';
        objAttributes.talentId  = 'NZ';
        objAttributes.talentFirstName  = 'Home Entertainment';
        objAttributes.talentLastName  = 'VID';
        objAttributes.isMajorTalent  = 'OFLC - Office of Film and Literature Classification';
        objAttributes.languageCode  = 'OFLC';
        objAttributes.languageDescription  = 'R16';
        objAttributes.talentMiddleName  = 'OFLC-R16';
        objAttributes.suffix  = null;
        objAttributes.honorific  = 'UNR';
        objAttributes.specialty  = 'Unrated';
        objAttributes.foxVersionId  = 'INTERNATIONAL/UNRATED/EXTENDED VERSION';
        objAttributes.jdeVaultId  = 'Restricted to persons 16 Years and over';
        objAttributes.titleVersionTypeCode   = 'OFLC';
        objAttributes.titleVersionTypeDescription   = 'R16';
        objAttributes.titleVersionDescription   = 'OFLC-R16';
        objAttributes.foxId   = null;
        list<HEP_Foxipedia_TalentWrapper.Data> lstData = new list<HEP_Foxipedia_TalentWrapper.Data>();
        HEP_Foxipedia_TalentWrapper.Data objData = new HEP_Foxipedia_TalentWrapper.Data();
        objData.attributes = objAttributes;
        objData.type = 'RATING';
        objTalentDetails.errors=null;
        HEP_Foxipedia_TalentWrapper.Links objLinks = new HEP_Foxipedia_TalentWrapper.Links();
        objLinks.related = '/foxipedia/global/api/title/details?foxId=67728';
        HEP_Foxipedia_TalentWrapper.Title objTtle = new HEP_Foxipedia_TalentWrapper.Title();
        objTtle.links = objLinks;
        HEP_Foxipedia_TalentWrapper.Errors objErrors = new HEP_Foxipedia_TalentWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_Foxipedia_TalentWrapper.Relationships objRelationships = new HEP_Foxipedia_TalentWrapper.Relationships();
        objRelationships.title = null;
        Test.stoptest();
    }

}