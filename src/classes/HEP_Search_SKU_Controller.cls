/**
 * HEP_Search_SKU_Controller ---   Class to display  all the SKU based on the search criteria.
 * @author    Roshi Rai
 */
Public class HEP_Search_SKU_Controller {
    /**
     *  SKUWrapper  -Wrapper class to  data of Catalog 
     *  @author     -Roshi Rai
     **/
    Public class SKUWrapper {
        Public Integer iSizeoflist;
        list < RowData > lstRowData;
        List < String > lstKeyWords;
        List < String > lstCriteria;
        public SKUWrapper() {
            lstRowData = new list < RowData > ();
            iSizeoflist = 0;
            lstKeyWords = new List < String > ();
            lstCriteria = new List < String > ();
        }
    }
    /**
     * RowData    -Class to hold the row data to be displayed on AG Grid
     * @author    -Roshi Rai
     **/
    public class RowData {
        Public String sSKUName;
        Public string sID;
        Public String sType;
        Public String sTerritory;
        Public String sRegion;
        Public String sLicensor;
        Public String sSKURecordID;
    }
    /* Primary method which will be called from the HEP_Search_SKU page to retrieve the SKU Record as per the parameter passed.
     * @param          sType--Lable or type on basis of which we will search Name,Barcode,id,keyword     
     * @param          sCriteria--starts With OR contains
     * @param          sSearchKeyWord--value entered for search
     * @return         SKUWrapper
     */
    @RemoteAction
    public Static SKUWrapper extractAllSKU(String sType, String sCriteria, String sSearchKeyWord) {
        try {
            String sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            SKUWrapper objSKUWrapper = new SKUWrapper();
            set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_SKU'));

            String keywords =  HEP_Utility.getConstantValue('HEP_SEARCH_SKU_KEYWORDS');
            objSKUWrapper.lstKeyWords = keywords.split(',');
            String criteria = HEP_Utility.getConstantValue('SEARCH_CRITERIA_VALUES');
            objSKUWrapper.lstCriteria = criteria.split(',');
            list < HEP_SKU_Master__c > lstAllSKU = new list < HEP_SKU_Master__c > ();
            String sSKUQuery = 'SELECT id, SKU_Title__c,Name,Media_Type__c, Type__c,UI_SKU_Number__c,SKU_Number__c,Catalog_Master__r.Licensor_Type__c, Barcode__c,Territory__c, Territory__r.Name, Territory__r.Region__c,Catalog_Master__c,Catalog_Master__r.Licensor__c FROM HEP_SKU_Master__c WHERE Record_Status__c=:sActive AND Territory__c In: setOfUserTerritory';
            //putting the WHERE Condition
            if(sSearchKeyWord != null && sCriteria != null && String.isNotBlank(sSearchKeyWord) && String.isNotBlank(sCriteria) ){
            if (sType != null && sSearchKeyWord != null) {
                if (sType == HEP_Utility.getConstantValue('HEP_SKU_NAME')) {
                    sSKUQuery += ' AND SKU_Title__c';
                } else if (sType == HEP_Utility.getConstantValue('HEP_SKU_ID')) {
                    sSKUQuery += ' AND  UI_SKU_Number__c';  
                } else if (sType == HEP_Utility.getConstantValue('HEP_UPC_BARCODE')) {
                    sSKUQuery += ' AND  Barcode__c';
                } else if (sType == HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD')) {
                    sSKUQuery += ' AND (SKU_Title__c ';
                }
                //putting the WHERE Search keyword
                if (sCriteria != null && sSearchKeyWord != null && HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD') != sType) {
                    if (sCriteria == HEP_Utility.getConstantValue('HEP_STARTS_WITH')) {
                        sSKUQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\'';
                    } else if (sCriteria == HEP_Utility.getConstantValue('HEP_CONTAINS')) {
                        sSKUQuery += ' LIKE \'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\'';
                    }
                }
                if (sCriteria != null && sSearchKeyWord != null && HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD') == sType) {
                    if (sCriteria == HEP_Utility.getConstantValue('HEP_STARTS_WITH')) {
                        //sSKUQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR SKU_Number__c LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                       sSKUQuery += ' LIKE \'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR UI_SKU_Number__c LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR Name LIKE\'' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                    } else if (sCriteria == HEP_Utility.getConstantValue('HEP_CONTAINS')) {
                        sSKUQuery += ' LIKE \'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR UI_SKU_Number__c LIKE\'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\' OR Name LIKE\'%' + String.escapeSingleQuotes(sSearchKeyWord) + '%\')';
                    }
                }
            }
        }

        else{
                //sSKUQuery += ' AND SKU_Title__c LIKE \''+String.escapeSingleQuotes(sSearchKeyWord)+'%\'';
                sSKUQuery += ' AND (SKU_Title__c LIKE \''+String.escapeSingleQuotes(sSearchKeyWord)+'%\' OR UI_SKU_Number__c LIKE\''+String.escapeSingleQuotes(sSearchKeyWord)+'%\')';
       
             }
            system.debug('**sSKUQuery' + sSKUQuery);
            sSKUQuery += ' LIMIT 10001';
            lstAllSKU = Database.Query(sSKUQuery);
            //to get the bumber of record to be displayed on screen
            objSKUWrapper.iSizeoflist = lstAllSKU.size();
            if (lstAllSKU != NULL && !lstAllSKU.isEmpty()) {
                for (HEP_SKU_Master__c objSKU: lstAllSKU) {
                    RowData objRowData = new RowData();
                    objRowData.sSKUName = objSKU.SKU_Title__c;
                    objRowData.sID = objSKU.UI_SKU_Number__c;   
                    objRowData.sType = objSKU.Media_Type__c;
                    objRowData.sTerritory = objSKU.Territory__r.Name;
                    objRowData.sRegion = objSKU.Territory__r.Region__c;
                    objRowData.sLicensor = objSKU.Catalog_Master__r.Licensor_Type__c;
                    objRowData.sSKURecordID = objSKU.id;
                    objSKUWrapper.lstRowData.add(objRowData);
                }
            }
            return objSKUWrapper;
        }
        Catch(Exception ex) {
            return NULL;
        }
    }
}