/**
 * HEP_SKU_SiebelUpdate --- Class for Calling the API to update SKU details
 * @author  Nidhin VK
 */
public class HEP_SKU_SiebelUpdate implements HEP_IntegrationInterface{
    
    public static String sCHANNEL, sFORMAT, sCOMPILATION, sPERMANENT_RANGE, sCONFIGURATION;
    static{
        sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
        sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
        sCOMPILATION = HEP_Utility.getConstantValue('HEP_SKU_COMPILATION_TYPE');
        sPERMANENT_RANGE = HEP_Utility.getConstantValue('HEP_SKU_PERMANENT_RANGE');
        sCONFIGURATION = HEP_Utility.getConstantValue('HEP_SKU_CONFIGURATION_TYPE');
    }
    
    /**
    * Perform the API call.
    * @param  empty response
    * @return 
    * @author Nidhin VK
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        list<String> lstRecordIds = new list<string>();
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                if(String.isNotBlank(objTxnResponse.sSourceId))
                    lstRecordIds = objTxnResponse.sSourceId.split(',');
                
                system.debug('lstRecordIds ---- ' + lstRecordIds);
                httpRequest.setBody(createRequestBody(lstRecordIds));
                                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                String sStatus = httpResp.getStatus();
                System.debug('status+++'+sStatus);
                if(sStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED'))
                    || sStatus.equals(HEP_Utility.getConstantValue('HEP_STATUS_OK'))){  
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }
    }
    
    /**
    * Perform the API call.
    * @param  Ids
    * @return Stringified
    * @author Nidhin VK
    */
    public String createRequestBody(List<String> lstSKUIds){
    
        SKUWrapper objSKUWrapper = new SKUWrapper();
        objSKUWrapper.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
        objSKUWrapper.EventType = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
        objSKUWrapper.EventName = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        
		List<String> lstLOVTypes = new List<String>{sCHANNEL, sFORMAT, sCOMPILATION, sPERMANENT_RANGE, sCONFIGURATION};
        Map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(
        										lstLOVTypes, 
        										HEP_Utility.getConstantValue('HEP_NAME_FIELD'), 
        										HEP_Utility.getConstantValue('HEP_LOV_FIELD_VALUES'));
        for(HEP_SKU_Master__c objSKU : [SELECT 
                                            Id, CreatedDate, CreatedBy.Name, SKU_Configuration__c, SKU_Permanent_Range__c, Type__c,
                                            LastModifiedDate, LastModifiedBy.Name, Record_Status__c, SKU_Compilation__c, SKU_Title__c,
                                            SKU_Number__c, Channel__c, Format__c, Catalog_Master__c, Catalog_Master__r.CatalogId__c,
                                            Territory__c, Territory__r.Name
                                        FROM
                                            HEP_SKU_Master__c
                                        WHERE 
                                            Id IN :lstSKUIds
                                        AND
                                        	INTL_ETL_SKU__c = false]){
                                            
            Item objItem = new Item();
            objItem.objectType = HEP_Utility.getConstantValue('HEP_SKU').toLowerCase();
            objItem.createdDate = String.ValueOf(objSKU.CreatedDate);
            objItem.createdBy = objSKU.CreatedBy.Name;
            objItem.updatedDate = String.ValueOf(objSKU.LastModifiedDate);
            objItem.updatedBy = objSKU.LastModifiedBy.Name;
            objItem.recordId = objSKU.Id;
            objItem.recordStatus = objSKU.Record_Status__c;
            objItem.territoryId = objSKU.Territory__c;
            objItem.territoryName = objSKU.Territory__r.Name;
            objItem.catalogId = objSKU.Catalog_Master__c;
            objItem.catalogNumber = objSKU.Catalog_Master__r.CatalogId__c;
            objItem.skuNumber = objSKU.SKU_Number__c;
            objItem.skuName = objSKU.SKU_Title__c;
            if(String.isBlank(objSKU.Channel__c))
            	objItem.channel = '';
            else
            	objItem.channel = mapLOVMapping.containsKey(sCHANNEL + '*' + objSKU.Channel__c)? mapLOVMapping.get(sCHANNEL + '*' + objSKU.Channel__c) : objSKU.Channel__c;
            if(String.isBlank(objSKU.Format__c))
            	objItem.format = '';
            else
            	objItem.format = mapLOVMapping.containsKey(sFORMAT + '*' + objSKU.Format__c)? mapLOVMapping.get(sFORMAT + '*' + objSKU.Format__c) : objSKU.Format__c;
            if(String.isBlank(objSKU.SKU_Configuration__c))
            	objItem.skuConfiguration = '';
            else
            	objItem.skuConfiguration = mapLOVMapping.containsKey(sCONFIGURATION + '*' + objSKU.SKU_Configuration__c)? mapLOVMapping.get(sCONFIGURATION + '*' + objSKU.SKU_Configuration__c) : objSKU.SKU_Configuration__c;
            if(String.isBlank(objSKU.SKU_Permanent_Range__c))
            	objItem.skuPermanentRange = '';
            else
            	objItem.skuPermanentRange = mapLOVMapping.containsKey(sPERMANENT_RANGE + '*' + objSKU.SKU_Permanent_Range__c)? mapLOVMapping.get(sPERMANENT_RANGE + '*' + objSKU.SKU_Permanent_Range__c) : objSKU.SKU_Permanent_Range__c;
            if(String.isBlank(objSKU.SKU_Compilation__c))
            	objItem.skuCompilation = '';
            else
            	objItem.skuCompilation = mapLOVMapping.containsKey(sCOMPILATION + '*' + objSKU.SKU_Compilation__c)? mapLOVMapping.get(sCOMPILATION + '*' + objSKU.SKU_Compilation__c) : objSKU.SKU_Compilation__c;
            objItem.type = objSKU.Type__c;  
            objSKUWrapper.Data.Payload.items.add(objItem);
        }
        System.debug('JSON BOSY IS THIS ---- '+objSKUWrapper);
        return JSON.serialize(objSKUWrapper);
    }
    
    /**
    * SKUWrapper --- Wrapper class for holding the data for the entire request body
    * @author    Nidhin VK
    */
    public class SKUWrapper {
        public string ProducerID;
        public string EventType;
        public string EventName;
        public Data Data;
        public SKUWrapper(){
            Data = new Data();
        }
    }
    
    public class Item {
        public string objectType;
        public String createdDate;
        public string createdBy;
        public String updatedDate;
        public string updatedBy;
        public string recordId;
        public string recordStatus;
        public string territoryId;
        public string territoryName;
        public string catalogId;
        public string catalogNumber;
        public string skuNumber;
        public string skuName;
        public string channel;
        public string format;
        public string skuConfiguration;
        public string skuPermanentRange;
        public string skuCompilation;
        public string type;
    }
    
    public class Payload {
        public List<Item> items;
        public Payload(){
            items = new List<Item>();
        }
    }
    
    public class Data {
        public Payload Payload;
        public Data(){
            Payload = new Payload();
        }
    }
    
    /**
    * ErrorResponseWrapper --- Wrapper class for response parse
    * @author    Nidhin VK
    */
    public class ErrorResponseWrapper {
        public String status;
        public String errorMessage;
        public ErrorResponseWrapper(String status, String errorMessage) {
            this.status = status;
            this.errorMessage = errorMessage;
        }
    }
}