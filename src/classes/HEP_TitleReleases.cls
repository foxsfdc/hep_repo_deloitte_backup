/*------------------------------------Fox HEP----------------------------------
This class handles the inbound request from Prophet, Copper & Foxipedia for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
@RestResource(urlMapping='/v1/HEP_TitleReleases/*')
global class HEP_TitleReleases {
	/**
    * @param finTitleId - Financial title ID
    * @param territories - Comma delimited territory code (i.e. 'AU,GP,JP')
    * @param systemName - Name of boundary system that makes API call to HEP application
    * @return JSON string of array of Title releases' related fields for each territory
    */
	@HttpPost
	global static void getTitleReleases(String finTitleId, String territories, String systemName, String catalogId) {
		if (HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME') != null && HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME').Value__c != null && 
			HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME') != null && HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME').Value__c != null && 
			HEP_Constants__c.getValues('HEP_FOXIPEDIA_SYSTEM_NAME') != null && HEP_Constants__c.getValues('HEP_FOXIPEDIA_SYSTEM_NAME').Value__c != null) {

			TitleReleasesRequest trpReq = new TitleReleasesRequest(finTitleId, territories);
			if (systemName == HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME').Value__c) {			
				HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(trpReq),'',HEP_Constants__c.getValues('HEP_TITLERELEASES_PROPHET_INBOUND').Value__c);		
			}
				else if (systemName == HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME').Value__c) {
				HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(catalogId,'',HEP_Constants__c.getValues('HEP_TITLERELEASES_COPPER_INBOUND').Value__c);
			}
		}

		
	}

	@HttpGet
	global static void getFoxipediaTitleReleases() {
		RestRequest req = RestContext.request; 
		RestResponse res = RestContext.response; 

		String territories = RestContext.request.params.get('territories');
		String finTitleId = req.requestURI.split('/')[3];
		TitleReleasesRequest trpReq = new TitleReleasesRequest(finTitleId, territories);
		HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(trpReq),'',HEP_Constants__c.getValues('HEP_TITLERELEASES_FOXIPEDIA_INBOUND').Value__c);
	}

	public class TitleReleasesRequest {
		public String finTitleId;
		public String territories; 

		public TitleReleasesRequest(String finTitleId, String territories) {
			this.finTitleId = finTitleId; 
			this.territories = territories; 
		}
	}
}