public class HEP_Search_Promotions_Controller {

    public static String sActive;
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    //wrapper class that stores theJson
    public class HEP_Search_Wrapper{
        list<searchRowData> lstSearchData;
        string sKeywordFilter;
        string sStartsWithFilter;
        string sSearch;
        integer iNoOfResults;
        List<String> lstKeyWords;
        List<String> lstCriteria;
        List<String> lstSpendCriteria;

    //contructor
    public HEP_Search_Wrapper(){
        lstSearchData = new list<searchRowData>();
        lstKeyWords = new List<String>();
        lstCriteria = new List<String>();
        iNoOfResults = 0;
    }
}
//wrapper which holds the data of rows displayed
public class searchRowData{
    string sPromoId;
    string sImageURL;
    string sName;
    string sLocalPromoCode;
    string sTerritory;
    string sCustomer;
    string sLOB;
    date dtFAD;
    string sManager;
    string sStatus;
    
}


@RemoteAction
public static HEP_Search_Wrapper getPromotionData(String sLocaleDateFormat, string sKeyword, string sCriteria, string sSearchKey, Date startDate, Date endDate){
    try{
    HEP_Search_Wrapper objSearchWrapper = new HEP_Search_Wrapper();
    List<HEP_Promotion__c> lstAllPromotions = new List<HEP_Promotion__c>();
    List<HEP_Market_Spend__c> lstSpend = new List<HEP_Market_Spend__c>();
    String defaultImageURL = HEP_Utility.getPromoDefaultImageUrl();
    Set<Id> setSpendPromotions = new Set<Id>();
    
    String keywords = HEP_Constants__c.getValues('SEARCH_PROMOTIONS_KEYWORD_VALUES').Value__c;
    objSearchWrapper.lstKeyWords = keywords.split(',');
    
    String criteria = HEP_Constants__c.getValues('SEARCH_CRITERIA_VALUES').Value__c;
    objSearchWrapper.lstCriteria = criteria.split(',');
    
    String spendCriteria = HEP_Constants__c.getValues('SEARCH_PROMOTION_CRITERIA_FOR_SPEND').Value__c;
    objSearchWrapper.lstSpendCriteria = spendCriteria.split(',');
    
//Query All Promotions
set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_PROMOTIONS'));
System.debug('setOfUserTerritory ;;' + setOfUserTerritory);
string sSpendQuery;
string sRegionQuery;
set<Id> setPromoRegions = new set<Id>();
string sQuery = 'SELECT ID,Name,PromotionName__c,Status__c,MinPromotionDt_Date__c,LocalPromotionCode__c,Customer_Promotion_Status__c,Promotion_Type__c,Record_Status__c,Title__r.Name,Title__r.FIN_PROD_ID__c,Image_URL__c,LineOfBusiness__r.Name,FirstAvailableDate__c,Domestic_Marketing_Manager__r.Name,Customer__r.CustomerName__c,Territory__c,Territory__r.Name,Territory__r.Region__c FROM HEP_Promotion__c WHERE (Record_Status__c=:sActive) AND (Territory__c IN :setOfUserTerritory) ';
if(sKeyWord != null && sCriteria != null && String.isNotBlank(sKeyWord) && String.isNotBlank(sCriteria) ){
    if(sKeyword != null){
        if(HEP_Constants__c.getValues('HEP_TERRITORY').Value__c.equals(sKeyword)){
            sQuery += ' AND Territory__r.name';
        }
        else if(HEP_Constants__c.getValues('HEP_PROMOTION_NAME').Value__c.equals(sKeyword)){
            sQuery += ' AND PromotionName__c';
        }
        else if(HEP_Constants__c.getValues('HEP_LOCALPROMOCODE').Value__c.equals(sKeyword)){
            sQuery += ' AND LocalPromotionCode__c';
        }
        else if(HEP_Constants__c.getValues('HEP_PROMOTION_STATUS').Value__c.equals(sKeyword)){
            sQuery += ' AND (Customer_Promotion_Status__c';
        }
        else if(HEP_Constants__c.getValues('HEP_FAD').Value__c.equals(sKeyword)){
            sQuery += ' AND ((FirstAvailableDate__c';
        }
        else if(HEP_Constants__c.getValues('HEP_MARKETING_MANAGER').Value__c.equals(sKeyword)){
            sQuery += ' AND Domestic_Marketing_Manager__r.Name';
        }
        else if(HEP_Constants__c.getValues('HEP_LINEOFBUSINESS').Value__c.equals(sKeyword)){
            sQuery += ' AND LineOfBusiness__r.Name';
        }
        else if(HEP_Constants__c.getValues('HEP_TITLE_NAME').Value__c.equals(sKeyword)){
            sQuery += ' AND Title__r.Name';
        }
        else if(HEP_Constants__c.getValues('HEP_FINANCIAL_TITLE_ID').Value__c.equals(sKeyword)){
            sQuery += ' AND Title__r.FIN_PROD_ID__c';
        }
        else if(HEP_Constants__c.getValues('HEP_SEARCH_REGION').Value__c.equals(sKeyword)){
             sRegionQuery = 'SELECT Parent_Promotion__r.Id FROM HEP_Promotion_Region__c WHERE Territory__r.Parent_Territory__c!= null AND Territory__r.Name ';
            
        }
        else if(HEP_Constants__c.getValues('HEP_SEARCH_MARKET_SPEND').Value__c.equals(sKeyword)){
            sSpendQuery = 'SELECT Promotion__r.Id from HEP_Market_Spend__c WHERE Approved_Amount__c ';
        }
        else if(HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword)){
            sQuery += ' AND (PromotionName__c ';
        }
    }
    
        if(sCriteria != null && !( HEP_Constants__c.getValues('HEP_PROMOTION_STATUS').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_SEARCH_REGION').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_SEARCH_MARKET_SPEND').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_FAD').Value__c.equals(sKeyword) || HEP_Constants__c.getValues('HEP_FINANCIAL_TITLE_ID').Value__c.equals(sKeyword))){
            if(HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria)){
                sQuery +=' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\'';
                //sQuery +=' LIKE \''+sSearchKey+'%\'';
            }
            else if(HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria)){
                sQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\'';
                //sQuery +=' LIKE \'%'+sSearchKey+'%\'';
            }
        }
        else 
        {   
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_PROMOTION_STATUS').Value__c.equals(sKeyword)){
                if(HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria)){
                    sQuery +=' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\' OR Status__c LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\')';
                }
                else if(HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria)){
                    sQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\' OR Status__c LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\')';
            }
            }
            
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_SEARCH_REGION').Value__c.equals(sKeyword)){
                if(HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria))
                    sRegionQuery +=' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\'';
                else if(HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria))
                    sRegionQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\'';
                    
                List<HEP_Promotion_Region__c> lstPromoRegion = Database.Query(sRegionQuery);
                for (HEP_Promotion_Region__c promoreg : lstPromoRegion){
                    setPromoRegions.add(promoreg.Parent_Promotion__r.Id);
                }
                sQuery += 'AND Id IN :setPromoRegions';
            }
            
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_FINANCIAL_TITLE_ID').Value__c.equals(sKeyword)){
                sQuery +=' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\'';
            }
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_FAD').Value__c.equals(sKeyword)){
                
                sQuery += '>=:startDate  AND FirstAvailableDate__c <=: endDate) OR (MinPromotionDt_Date__c >=: startDate AND MinPromotionDt_Date__c <=:endDate))';
            } 
            
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_SEARCH_MARKET_SPEND').Value__c.equals(sKeyword)){
                if(HEP_Constants__c.getValues('SEARCH_GREATER_THAN').Value__c.equals(sCriteria))
                    sSpendQuery+= ' >'+Integer.valueOf(sSearchKey);
                else if(HEP_Constants__c.getValues('SEARCH_LESSER_THAN').Value__c.equals(sCriteria))
                    sSpendQuery+= ' <'+Integer.valueOf(sSearchKey);
                else if(HEP_Constants__c.getValues('SEARCH_EQUALS').Value__c.equals(sCriteria))
                    sSpendQuery+= ' ='+Integer.valueOf(sSearchKey);
                        else if(HEP_Constants__c.getValues('SEARCH_GREATER_THAN_OR_EQUALS').Value__c.equals(sCriteria))
                    sSpendQuery+= ' >='+Integer.valueOf(sSearchKey);
                else if(HEP_Constants__c.getValues('SEARCH_LESSER_THAN_OR_EQUALS').Value__c.equals(sCriteria))
                    sSpendQuery+= ' <='+Integer.valueOf(sSearchKey);
                    
                lstSpend = Database.query(sSpendQuery);
                for(HEP_Market_Spend__c spend : lstSpend){
                    setSpendPromotions.add(spend.Promotion__r.Id);
                }
                
                sQuery +=' AND Id IN :setSpendPromotions';
                
            }
            if(sCriteria != null && HEP_Constants__c.getValues('HEP_STARTS_WITH').Value__c.equals(sCriteria) && sKeyword != null && HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword)){
                sQuery += ' LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\' OR LocalPromotionCode__c LIKE\''+String.escapeSingleQuotes(sSearchKey)+'%\')';
            }
            else if(sCriteria != null && HEP_Constants__c.getValues('HEP_CONTAINS').Value__c.equals(sCriteria) && sKeyword != null && HEP_Constants__c.getValues('HEP_SEARCH_KEYWORD').Value__c.equals(sKeyword)){
                sQuery += ' LIKE \'%'+String.escapeSingleQuotes(sSearchKey)+'%\' OR LocalPromotionCode__c LIKE\'%'+String.escapeSingleQuotes(sSearchKey)+'%\')';
            }
            
        }        
}
else{
    sQuery += ' AND (PromotionName__c LIKE \''+String.escapeSingleQuotes(sSearchKey)+'%\' OR LocalPromotionCode__c LIKE\''+String.escapeSingleQuotes(sSearchKey)+'%\')';
}
        
        
  
    sQuery += ' Limit 10001';

    //'LIKE \'%' + String.escapeSingleQuotes(sPromotionName) + '%\'';
    system.debug('---------->Query'+sQuery);
    lstAllPromotions = Database.Query(sQuery);
	System.debug('lstAllPromotions ::' + lstAllPromotions);
    objSearchWrapper.iNoOfResults = lstAllPromotions.size();
    for(HEP_Promotion__c objPromotion : lstAllPromotions){
        searchRowData objSearchRowData = new searchRowData();
        
        objSearchRowData.sPromoId = objPromotion.ID;
        if(objPromotion.Image_URL__c != null){
                 //objSearchRowData.sImageURL = (!String.isBlank(promo.Promotion_Image_URL__c)) ? promo.Promotion_Image_URL__c : defaultImageURL;
                 objSearchRowData.sImageURL = objPromotion.Image_URL__c;
                }
                else{
                    objSearchRowData.sImageURL = defaultImageURL;
                }
                if(objPromotion.PromotionName__c != null){
                    objSearchRowData.sName = objPromotion.PromotionName__c;
                }
                if(objPromotion.LocalPromotionCode__c != null){
                    objSearchRowData.sLocalPromoCode = objPromotion.LocalPromotionCode__c;
                }
                if(objPromotion.Territory__r.Name != null){
                    objSearchRowData.sTerritory = objPromotion.Territory__r.Name;
                }
                if(objPromotion.LineOfBusiness__r.Name != null){
                    objSearchRowData.sLOB = objPromotion.LineOfBusiness__r.Name;
                }
                if(objPromotion.FirstAvailableDate__c != null && !HEP_Constants__c.getValues('PROMOTION_TYPE_NATIONAL').Value__c.equals(objPromotion.Promotion_Type__c)){
                    objSearchRowData.dtFAD = objPromotion.FirstAvailableDate__c;
                }
                else if(objPromotion.MinPromotionDt_Date__c != null){
                    objSearchRowData.dtFAD = objPromotion.MinPromotionDt_Date__c;
                }
                if(objPromotion.Domestic_Marketing_Manager__r.Name != null){
                    objSearchRowData.sManager = objPromotion.Domestic_Marketing_Manager__r.Name;
                }
                if(objPromotion.Customer__r.CustomerName__c != null){
                    objSearchRowData.sCustomer = objPromotion.Customer__r.CustomerName__c;  
                }
                //if(objPromotion.Promotion_Type__c != null && HEP_Constants__c.getValues('PROMOTION_TYPE_CUSTOMER').Value__c.equals(objPromotion.Promotion_Type__c))
                if(String.isBlank(objPromotion.Customer_Promotion_Status__c)){
                    if(String.isNotBlank(objPromotion.Status__c) )
                        objSearchRowData.sStatus = objPromotion.Status__c;
                    else
                         objSearchRowData.sStatus = '-';
                }
                else
                { 
                    objSearchRowData.sStatus = objPromotion.Customer_Promotion_Status__c;
                }
                   

            //system.debug();
            objSearchWrapper.lstSearchData.add(objSearchRowData);

        }
        system.debug('wrapper----------->'+objSearchWrapper);
        return objSearchWrapper;
    }
    Catch(Exception ex) {
            return NULL;
    }
    }

}