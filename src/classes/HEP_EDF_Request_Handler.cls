/**
* HEP_EDF_Request_Handler -- This is the REST class which will be invoked by EDF to trigger any EDF inbound services in HEP.
* @author    Gaurav Mehrishi
*/
@RestResource(urlMapping = '/v1/HEP_EDF_Request_Handler/*')
global class HEP_EDF_Request_Handler {

    
    //EDFRequestRoot--Wrapper for Root of JSON being received from EDF    
    public class EDFRequestRoot{
        public string EventName;
        public string EventType;
        public string ProducerId;
    }
    
    /**
     * getEDFRequest -- takes the request and calls the related class based on conditions
     * @param no input
     * @return response
     */
    @HttpPost
    global static String getEDFRequest() {

        RestRequest objReq;
        String sJSON, sEventName;
        EDFRequestRoot objRequest;       
        String sResponse;
        String sHEP_FOXIPEDIA_CATALOG = HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG');
        String sHEP_FMCBroadcastEvent = HEP_Utility.getConstantValue('HEP_FMCBroadcastEvent');
        String sHEP_FMC_TITLEIMAGES = HEP_Utility.getConstantValue('HEP_FMC_TITLEIMAGES');
        String sHEP_FoxipediaChangeEvent = HEP_Utility.getConstantValue('HEP_FoxipediaChangeEvent');
        String sHEP_TitleChange = HEP_Utility.getConstantValue('HEP_TitleChange'); 
        String sHEP_Production_Company_Interface = HEP_Utility.getConstantValue('HEP_Production_Company_Interface');
        
        try {
            objReq = RestContext.request;
            System.debug('request+++++'+RestContext.request);
            sJSON = objReq.requestbody.toString();
            system.debug('---------------' + sJSON);

            objRequest = (EDFRequestRoot) JSON.deserialize(sJSON, EDFRequestRoot.class);
            system.debug('request: ' + objRequest);
            if (objRequest.EventType.equals(System.Label.HEP_Integ_Product_EventType) && objRequest.EventName.equals(System.Label.HEP_Integ_Product_EventName)) {
                //redirect to execute integration class
                System.debug('FoxipediaChangeEvent');
                if(sHEP_FOXIPEDIA_CATALOG != null)
                HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',sHEP_FOXIPEDIA_CATALOG);
            }
            else if (sHEP_FMCBroadcastEvent != null && objRequest.EventName.equals(sHEP_FMCBroadcastEvent)){
                //redirect to execute integration class HEP_FMC_TitleImages
                System.debug('in request handler in if block');
                if(sHEP_FMC_TITLEIMAGES != null)
                HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',sHEP_FMC_TITLEIMAGES);
            }
            else if(objRequest.EventName.equals(sHEP_FoxipediaChangeEvent) && objRequest.EventType.equals(sHEP_TitleChange))
            {
                HEP_InterfaceTxnResponse objIntTxnRsp = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',sHEP_Production_Company_Interface);
            }
            
            system.debug('request: ' + objRequest);
        } catch (Exception e) {

            String sId = HEP_Error_Log.genericException('HEP_EDF_Request_Handler Exception', 'Integration Inbound', e,
                'HEP_EDF_Request_Handler', 'getEDFRequest', '', '');
            HEP_Error_Log.InsertAttachment(sId, 'EDF Request payload', sJSON);
            sResponse = 'Exception occurred';
        }
        return sResponse;
    }
}