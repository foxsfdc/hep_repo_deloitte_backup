/**
* HEP_BoxFolders_Queueable --- Class to perform callouts to create Box folders for promotions
* @author    Tejaswini Veliventi
*/
public class HEP_BoxFolders_Queueable implements Queueable, Database.AllowsCallouts {
    
    List<HEP_Territory__c> lstTerritoryRecords = new List<HEP_Territory__c>();
    Map<String,String> mapParentIds =new Map<String,String>();
    HEP_Promotion__c promotion = new HEP_Promotion__c();
    Boolean isGlobalTerritory = false;
    Boolean bIsEnd = true;
   
    /**
    * Class constructor to initialize the class variables
    * @return nothing
    */  
    public HEP_BoxFolders_Queueable(List<HEP_Territory__c> lstTerritoryRecords,Map<String,String> mapParentIds,HEP_Promotion__c promotion, Boolean isGlobalTerritory){
        //this.lstTerritoryRecords.addAll(lstTerritoryRecords);
        for(HEP_Territory__c objTerritory : lstTerritoryRecords){
            if(objTerritory.Territory_Code__c.equalsIgnoreCase('World Wide') && objTerritory.Name.equalsIgnoreCase('Global')){
                HEP_Territory__c objWorldWideCopyTerr = objTerritory.clone();
                objWorldWideCopyTerr.Territory_Code__c = 'World Wide Clone';
                this.lstTerritoryRecords.add(objWorldWideCopyTerr);
            }
            this.lstTerritoryRecords.add(objTerritory);
        }
        this.mapParentIds.putAll(mapParentIds);
        this.promotion = promotion;
        this.isGlobalTerritory = isGlobalTerritory;
        System.debug('this.lstTerritoryRecords ----> ' + this.lstTerritoryRecords.size());
        System.debug('this.lstTerritoryRecords ----> ' + JSON.serializePretty(this.lstTerritoryRecords));
    }
    
    /**
    * implements the execute method of Queueable interface. Creates folders for territory and departments under it. Chains a new job based on the governor limits
    * @return nothing
    */ 
    public void execute(QueueableContext qContext) {
        Map<String,String> mapParentFolders = new Map<String,String>();
        HEP_FolderWrapper.TokenWrapper wrap = new HEP_FolderWrapper.TokenWrapper();
        wrap = (HEP_FolderWrapper.TokenWrapper)json.deserialize(HEP_BoxServices.getToken(),HEP_FolderWrapper.TokenWrapper.class);
        try{
            String sResult;
            String sResult1;
            //Integer i = 1/0;
            List<String> lstDept = new List<String>();
            List<HEP_FolderWrapper.RequestFolder> lstProcessingRecords = new List<HEP_FolderWrapper.RequestFolder>();
            HEP_FolderWrapper.RequestFolder territoryWrapper = new HEP_FolderWrapper.RequestFolder();
            Boolean WWfinance = false;
            
            List<HEP_Territory__c> lstTerritoryTemp = new List<HEP_Territory__c>(lstTerritoryRecords);
            System.debug('Territories for Integrations : ' + JSON.serializePretty(lstTerritoryTemp));
            System.debug('Territories for Integrations : ' + JSON.serializePretty(lstTerritoryRecords));
            System.debug('Territories for Integrations : ' + lstTerritoryTemp.size());
            System.debug('Territories for Integrations : ' + lstTerritoryRecords.size());
            Integer iCounter = 0;
            for(HEP_Territory__c territory : lstTerritoryTemp){
                lstDept.clear();
                iCounter++;
                if(territory.DepartmentList__c != null){
                    lstDept = territory.DepartmentList__c.split(',');
                    iCounter = iCounter+lstDept.size();
                }
                
                System.debug('0. Checking for Limits -- Territory Name ---> ' + territory.Name);
                System.debug('1. Checking for LImits -- iCounter ----> ' + iCounter);
                System.debug('2. Checking for LImits -- Limits.getLimitCallouts() ----> ' + Limits.getLimitCallouts());

                if(iCounter < Limits.getLimitCallouts()){

                    System.debug('0.0. Checking for Limits -- Territory Name ---> ' + territory.Name);
                    if(territory.Territory_Code__c != 'World Wide')
                    {
                        if(territory.Territory_Code__c.equalsIgnoreCase('World Wide Clone')){
                            territoryWrapper = HEP_BoxServices.parseRequestFolder('WorldWide '+promotion.PromotionName__c,mapParentIds.get('General'));
                        }else{
                            territoryWrapper = HEP_BoxServices.parseRequestFolder(territory.Territory_Code__c+' '+promotion.PromotionName__c,mapParentIds.get('General'));
                        }
                    }
                    else if(territory.Territory_Code__c == 'World Wide' && territory.Name == 'Global'){
                       WWfinance = true;
                       territoryWrapper = HEP_BoxServices.parseRequestFolder(territory.Territory_Code__c+' '+promotion.PromotionName__c,mapParentIds.get('Financial')); 
                       /*HEP_FolderWrapper.RequestFolder territoryWrapper1 = HEP_BoxServices.parseRequestFolder(territory.Territory_Code__c+' '+promotion.PromotionName__c,mapParentIds.get('General')); 
                       sResult1 = HEP_BoxServices.createFolder(json.serializePretty(territoryWrapper1),wrap.access_token);
                       System.debug('sResult123 ----> ' + sResult);*/
                    }
                    //Continue if there are other World Wide Folders which is not GLOBAL.
                    else{
                        continue;
                    }

                    System.debug('territoryWrapper ----> ' + territoryWrapper);
                    Integer iIndex = lstTerritoryRecords.indexOf(territory);

                    //Removing this logic as this needs to be on the Root and Finance Folder level
                    /*if(isGlobalTerritory && !(territoryWrapper.name).startsWithIgnoreCase('World Wide')){
                        territoryWrapper.name = 'World Wide '+ territoryWrapper.name;
                    }*/

                    System.debug('Parent Wrapper Name : ' + territoryWrapper.name);

                    sResult = HEP_BoxServices.createFolder(json.serializePretty(territoryWrapper),wrap.access_token);
                    System.debug('sResult ----> ' + sResult);
                    HEP_FolderWrapper.FolderDetails territoryFolder = (HEP_FolderWrapper.FolderDetails)JSON.deserialize(sResult, HEP_FolderWrapper.FolderDetails.class);

                    System.debug('Before error');
                    //integer i = 1/0;
                    System.debug('After error');
                    if(territoryFolder.type =='error'){
                        System.debug(territoryFolder);
                        if(territoryFolder.context_info != null && territoryFolder.context_info.conflicts != NULL &&territoryFolder.context_info.conflicts.size()>0){
                            territoryFolder.id = territoryFolder.context_info.conflicts[0].id;
                        }
                    }
                    if(lstDept.size()>0 && territoryFolder.id != null && !WWfinance){
                        for(String dept:lstDept){
                            String sFolderName;
                            if(territory.Territory_Code__c != 'World Wide'){
                                sFolderName = dept+' '+territory.Territory_Code__c+' '+promotion.PromotionName__c;
                            }
                            else{
                                sFolderName = dept+' '+promotion.PromotionName__c;
                            }
                            System.debug('Child Wrapper Name : ' + sFolderName);
                            lstProcessingRecords.add(HEP_BoxServices.parseRequestFolder(sFolderName, territoryFolder.id));
                        }
                    }// only for WW Finance
                    else if(WWfinance)
                    {
                        lstProcessingRecords.add(HEP_BoxServices.parseRequestFolder('Domestic'+' '+promotion.PromotionName__c, territoryFolder.id));
                        lstProcessingRecords.add(HEP_BoxServices.parseRequestFolder('International'+' '+promotion.PromotionName__c, territoryFolder.id));
                        WWfinance = false;
                    }               
                    lstTerritoryRecords.remove(iIndex);
                }
                else{
                    bIsEnd = false;
                    break;
                }
            }
            
            System.debug('iCounter before calling the Integration ----> ' + iCounter);
            if(lstProcessingRecords.size()>0){
                System.debug('lstProcessingRecords.size() --->' + lstProcessingRecords.size());
                System.debug('lstProcessingRecords --->' + JSON.serializePretty(lstProcessingRecords));
                System.debug('2. Checking for LImits -- Limits.getCallouts() ----> ' + Limits.getCallouts());
                for(HEP_FolderWrapper.RequestFolder req : lstProcessingRecords){
                    /*if(isGlobalTerritory && !req.name.startsWithIgnoreCase('World Wide')){
                        req.name = 'World Wide '+ req.name;
                    }*/
                    System.debug('iCounter for Direct Integration -----> ' + iCounter);
                    sResult = HEP_BoxServices.createFolder(json.serializePretty(req),wrap.access_token);
                    System.debug('sResult v sResult ---> ' + sResult);
                    HEP_FolderWrapper.FolderDetails resultFolder = (HEP_FolderWrapper.FolderDetails)JSON.deserialize(sResult, HEP_FolderWrapper.FolderDetails.class);
                }
            }
            
            if(!bIsEnd){
                ID jobId = System.enqueueJob(new HEP_BoxFolders_Queueable(lstTerritoryRecords,mapParentIds,promotion, isGlobalTerritory));
                System.debug(jobId);
                promotion.Box_Create_Status__c = 'In Progress';
                update promotion;
            }
            else{
                //update the box status
                promotion.Box_Create_Status__c = 'Completed';
                update promotion;
                //publish an event to notify the batch is complete
                HEP_Box_Create_Event__e event1 = new HEP_Box_Create_Event__e();
                event1.Promotion_Id__c=promotion.id;
                Database.saveResult sr = EventBus.publish(event1);
                if(sr.isSuccess()){
                    System.debug('event published successfully');
                }
                else{
                    for(Database.error er:sr.getErrors()){
                        System.debug('Error is :'+er.getMessage());
                    }
                }
            }
            if(wrap.refresh_token !=null){
                HEP_BoxServices.updateRefreshToken(wrap);
            }
        }
        catch(Exception ex){
            System.debug('Inside Catch' + wrap);
            if(wrap.refresh_token !=null){
                HEP_BoxServices.updateRefreshToken(wrap);
            }
            System.debug(ex.getLineNumber()+' MESSAGE :' +ex.getMessage() + ex.getStackTraceString());
            //HEP_Error_Log.genericException('Error occured during Box folders Creation from Queueable Class','Queueable Class',ex,'HEP_BoxFolders_Queueable','fetchNationalPromotionDefaultValues',null,'');

            //Create a Failure record under Interface Object.
            List<HEP_Interface__c> objBoxInterface = [SELECT Id FROM HEP_Interface__c WHERE Name = 'HEP_Box_Integration'];

            if(objBoxInterface != NULL && !objBoxInterface.isEmpty()){
                //Create HEP Interface Trsancation Object..
                HEP_Interface_Transaction__c objInterfaceTransaction = new HEP_Interface_Transaction__c(Status__c = 'Failure' , Transaction_Datetime__c = System.now());
                objInterfaceTransaction.HEP_Interface__c = objBoxInterface[0].Id;
                insert objInterfaceTransaction;

                //Create HEP Interface Trsancation Error Object.
                HEP_Interface_Transaction_Error__c objInterfaceTransactionError = new HEP_Interface_Transaction_Error__c(Status__c = 'Failure' , Error_Description__c = ex.getMessage() , Error_Datetime__c = System.now());
                objInterfaceTransactionError.HEP_Interface_Transaction__c = objInterfaceTransaction.Id;
                insert objInterfaceTransactionError;

                System.debug('Transaction Record ---> ' + objInterfaceTransaction);
                System.debug('Transaction Error Record ---> ' + objInterfaceTransactionError);
            }
            //throw ex;
        }finally{
            HEP_BoxServices.updateRefreshToken(wrap);
        }
    }
}