@isTest
public class HEP_Promotion_Rights_Controller_Test {
    @testSetup
    static void createUsers() {
        list < HEP_Constants__c > objHEPConstant = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void extractRights() {
        //Inserting Territory Record
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '980', 'JDE', true);       
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Promotion_Right', 'HEP_Promotion_Rights_Extractor', true, 10, 10, 'Outbound-Pull', true, true, true);
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
       
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true);
        //Create PRoduct Type Object.
        EDM_REF_PRODUCT_TYPE__c objEDM_REF_PRODUCT_TYPE = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature', 'FEATR', true);
        //Create Title Object.
        EDM_GLOBAL_TITLE__c objFinancialTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('TESTFinancial', '908763', 'PUBLC', objEDM_REF_PRODUCT_TYPE.Id, true);
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 1', 'National', null, objTerritory.Id, objLOB.Id, null, null, false);
        objPromotion.Title__c = objFinancialTitle.id;
        insert objPromotion;
        //HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.id, objTerritory.Id, true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TESTTAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Title_EDM__c = objFinancialTitle.ID;
        insert objCatalog;
        HEP_Promotion_Catalog__c objprocat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, null, true);
        //insert HEP Territory Rights record
        List < HEP_Territory_Rights__c > lstrights = new List < HEP_Territory_Rights__c > {
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'VOD', '4001', 'Active', 'Argentina', true),
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'DHD', '4001', 'Active', 'Argentina1', true),
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'Physical', '4001', 'Active', 'Argentina2', true),
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'VOD', '4001', 'Active', 'Argentina3', true),
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'DHD', '4001', 'Active', 'Argentina4', true),
            HEP_Test_Data_Setup_Utility.createTerritoryRights('Argentina - DHD', objTerritory.Id, '43', '36', 'Physical', '4001', 'Active', 'Argentina5', true)
        };
    }
    static testMethod void test1() {
        extractRights();
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Catalog__c objCatalog = [SELECT id, Title_EDM__c FROM HEP_Catalog__c WHERE Catalog_Name__c = : 'TESTTAL'];
        HEP_Promotion__c objPromotion = [SELECT id FROM HEP_Promotion__c WHERE PromotionName__c = : 'National Promotion 1'];
        System.runAs(u) {
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Promotion_Rights_Controller.getRightsForTitle(objCatalog.Title_EDM__c, objPromotion.id, 'Y');
            HEP_Promotion_Rights_Controller.getRightsForCatalog(objCatalog.id);
            HEP_Promotion_Rights_Controller.getCatalogTitle(objPromotion.id);
            HEP_Promotion_Rights_Controller.getWarningDescriptionFromCode('HOL,LIC');
            test.stopTest();
        }
    }
}