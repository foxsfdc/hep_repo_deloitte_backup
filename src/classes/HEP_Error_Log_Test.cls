@istest
public class HEP_Error_Log_Test {
	@testsetup
	static void createusers()
	{
		User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'jaggi', 'abc_xyz@deloitte.com','majaggi','mayank','majaggi','', true);
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
        objRoleOperations.Destination_User__c = u.Id;
        objRoleOperations.Source_User__c = u.Id;
        insert objRoleOperations;
        HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
        objRoleFADApprover.Destination_User__c = u.Id;
        objRoleFADApprover.Source_User__c = u.Id;
        insert objRoleFADApprover;
	}
	@istest
	static void testbusinessException()
	{
		User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
		System.runAs(testUser)
        {
		String sErrorName = 'Error Name' ;
		String sErrorType = 'Error Type' ;
		String sErrorMessage = 'Error Message' ;
		String sErrorDesc = 'Error Desc' ;
	    String sClassName = 'Class Name' ;
	    String sClassMethod = 'Class Method' ;
	    String sObjectId = 'Object ID' ;
	    String sURL = 'URL' ;

	    test.startTest();
	    String errorLogId = HEP_Error_Log.businessException(sErrorName, sErrorType, sErrorMessage, sErrorDesc,
                                            sClassName, sClassMethod, sObjectId, sURL);
	    HEP_Error_Log__c objError = [select Id,Name,Error_Type__c,Exception_Type__c,HEP_Class_Method__c,HEP_Class_Name__c,HEP_Error_Message__c,
	    HEP_Object_Id__c,Message__c,Source_URL__c,Username__c from HEP_Error_Log__c where id = :errorLogId];

	    system.assertEquals(sErrorName, objError.Name);
	    system.assertEquals(sErrorType, objError.Error_Type__c);
	    system.assertEquals(sErrorMessage, objError.HEP_Error_Message__c);
	    system.assertEquals(sErrorDesc, objError.Message__c);
	    system.assertEquals(sClassMethod, objError.HEP_Class_Method__c);
	    system.assertEquals(sObjectId, objError.HEP_Object_Id__c);
	    system.assertEquals(sURL, objError.Source_URL__c);
	    system.assertEquals(sClassName, objError.HEP_Class_Name__c);

	    sErrorName = 'greaterthan79charactersgreaterthan79charactersgreaterthan79charactersgreaterthan79characters';
	    String errorLogId1 = HEP_Error_Log.businessException(sErrorName, sErrorType, sErrorMessage, sErrorDesc,
                                            sClassName, sClassMethod, sObjectId, sURL);
	    HEP_Error_Log__c objError1 = [select Id,Name,Error_Type__c,Exception_Type__c,HEP_Class_Method__c,HEP_Class_Name__c,HEP_Error_Message__c,
	    HEP_Object_Id__c,Message__c,Source_URL__c,Username__c from HEP_Error_Log__c where id = :errorLogId1];
	    system.assertEquals('greaterthan79charactersgreaterthan79charactersgreaterthan79charactersgreatertha', objError1.Name);
	    test.stopTest();
	}
	}
	@istest
	static void testgenericException()
	{
		User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
		System.runAs(testUser)
        {
		String sErrorName = 'Error Name' ;
		String sErrorType = 'inbound' ;
		//String sErrorMessage = 'Error Message' ;
		//String sErrorDesc = 'Error Desc' ;
	    String sClassName = 'Class Name' ;
	    String sClassMethod = 'Class Method' ;
	    String sObjectId = 'Object ID' ;
	    String sURL = 'URL' ;
	    System.DmlException e = null;

	    test.startTest();
	    Account[] accts = new Account[]{new Account(billingcity = 'San Jose')};
		try {
		    insert accts;
		} catch (System.DmlException exc) {e=exc;}
	    String errorLogId = HEP_Error_Log.genericException(sErrorName, sErrorType, e,
                                          sClassName, sClassMethod, sObjectId, sURL);
	    HEP_Error_Log__c objError = [select Id,Name,Error_Type__c,Exception_Type__c,HEP_Class_Method__c,HEP_Class_Name__c,HEP_Error_Message__c,
	    HEP_Object_Id__c,Message__c,Source_URL__c,Username__c from HEP_Error_Log__c where id = :errorLogId];

	    system.assertEquals(sErrorName, objError.Name);
	    system.assertEquals(sErrorType, objError.Error_Type__c);
	    //system.assertEquals(sErrorMessage, objError.HEP_Error_Message__c);
	    //system.assertEquals(sErrorDesc, objError.Message__c);
	    system.assertEquals(sClassMethod, objError.HEP_Class_Method__c);
	    system.assertEquals(sObjectId, objError.HEP_Object_Id__c);
	    system.assertEquals(sURL, objError.Source_URL__c);
	    system.assertEquals(sClassName, objError.HEP_Class_Name__c);
	    //system.assertEquals(e, objError.Exception_Type__c);
	    sErrorName = 'greaterthan79charactersgreaterthan79charactersgreaterthan79charactersgreaterthan79characters';
	    sErrorType = 'inboundinboundinboundinboundinboundinbound' ;// greater than 39

	    String errorLogId1 = HEP_Error_Log.genericException(sErrorName, sErrorType, null,
                                          sClassName, sClassMethod, sObjectId, sURL);
	    HEP_Error_Log__c objError1 = [select Id,Name,Error_Type__c,Exception_Type__c,HEP_Class_Method__c,HEP_Class_Name__c,HEP_Error_Message__c,
	    HEP_Object_Id__c,Message__c,Source_URL__c,Username__c from HEP_Error_Log__c where id = :errorLogId1];

        system.assertEquals('No Exception', objError1.HEP_Error_Message__c);
        system.assertEquals('No Exception', objError1.Message__c);
        system.assertEquals('No Exception', objError1.Exception_Type__c);

        String errorLogId2 = HEP_Error_Log.genericException(sErrorName, sErrorType, null,
                                          sClassName, sClassMethod, sObjectId, sURL);

	    HEP_Error_Log__c objError2 = [select Id,Name,Error_Type__c,Exception_Type__c,HEP_Class_Method__c,HEP_Class_Name__c,HEP_Error_Message__c,
	    HEP_Object_Id__c,Message__c,Source_URL__c,Username__c from HEP_Error_Log__c where id = :errorLogId2];
	    test.stopTest();
	}
	}

	@istest
	static void testlogDMLerrors()
	{
		User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
		System.runAs(testUser)
        {
		HEP_Error_Log__c errorLog = new HEP_Error_Log__c();
		insert errorLog;
		Account acc = new Account(name = 'name');
		insert acc;
		String sErrorId = errorLog.id;
		List<Database.SaveResult> lstSaveResult = null;
		List<Database.UpsertResult> lstUpsertResult = null;
		List<Database.UpsertResult> lstUpsertResult1 = null;
		List<Database.UpsertResult> lstUpsertResult3 = null;

		Account[] accts = new List<Account>{new Account(Name='Account1', billingcity = 'Bombay'), new Account (Name = 'Account2', billingcity = 'Bombay'), new Account (billingcity = 'Bombay')};
		lstSaveResult = Database.insert(accts, false);
		system.debug('lstSaveResult---' + lstSaveResult);

		Account[] acc2 = [select id from Account where Name = 'Account1'];
		try{
			acc2[0].billingcity = 'Mumbai';
			lstUpsertResult1 = Database.upsert(acc2, true);
			system.debug('try lstUpsertResult1---' + lstUpsertResult1);
		} catch (System.DmlException exc) {system.debug('catch lstUpsertResult1---' + exc);}

		HEP_Error_Log.logDMLerrors(sErrorId, lstSaveResult, lstUpsertResult1);
	}
	}

	@istest
	static void testinsertAttachment()
	{
		User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
		System.runAs(testUser)
        {
		HEP_Error_Log__c errorLog = new HEP_Error_Log__c();
		insert errorLog;
		Account acc = new Account(name = 'name');
		insert acc;
		String sErrorId = errorLog.id;
		system.debug('sErrorId before passing -- '+sErrorId);
		String sFileName = 'filename';
		String sErrorText = 'errortext';
		HEP_Error_Log.insertAttachment(sErrorId, sFileName, sErrorText);
		Attachment objAtt = [select Id, Body, Name, parentId from Attachment where name = 'filename.txt'];
		system.assertEquals(Blob.valueOf(sErrorText), objAtt.Body);
		system.assertEquals(sFileName + '.txt', objAtt.Name);
		system.assertEquals(Blob.valueOf(sErrorText), objAtt.Body);
	}
	}
	

}