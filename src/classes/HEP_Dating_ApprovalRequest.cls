/**
 * HEP_Dating_ApprovalRequest --- Dynamically fetch records in Email Template
 * @author    Nishit Kedia
 */

public class HEP_Dating_ApprovalRequest
{
    //Promotion dating record id is passed in this variable
    public Id recordApproverIdForDating {get; set;}
    
    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
     
    //This variable is used to render table on Email template
    public ApprovalEmailWrapper objApprovalwrapper{ 
        get{
            if(objApprovalwrapper == null){
                fetchPromotionDatingDetails();
            }
            return objApprovalwrapper;
        }
        set;
    }
    
    //Global variable
    public  String  sRequestType = HEP_Utility.getConstantValue('HEP_DATING_REQUEST_TYPE');
    
    /**
    * fetchPromotionDatingDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author    Nishit Kedia
    */ 
    public void fetchPromotionDatingDetails(){
        
        //------------------------------------//
        try{
            //Instantiate the Wrapper
            objApprovalwrapper = new ApprovalEmailWrapper();   
            //------------------------------------//         
            if(recordApproverIdForDating != null){
                //-----------------------------------//
                //Query the Record based on Record Id               
                List<HEP_Record_Approver__c> lstRecordApproverForDating = [SELECT   Id,
                                                                                    Approval_Record__r.CreatedDate,
                                                                                    Approval_Record__r.CreatedBy.Name,
                                                                                    Approver__r.Name,
                                                                                    Approver_Role__r.Name, 
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.LocalPromotionCode__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Territory__r.Name,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Date_Type__c,
                                                                                    Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c
                                                                                    FROM HEP_Record_Approver__c
                                                                                    Where Id =: recordApproverIdForDating
                                                                                    AND Record_Status__c =:sACTIVE  
                                                                                    LIMIT 1];
                                                                            
                // Action required By is the Approver: This will change after Approval Framework is Done               
                //String sApprover = System.Label.HEP_Dating_Approver_Name;              
                //-----------------------------------//
                //Validate the List and Populate the Wrapper
                if(lstRecordApproverForDating != null && !lstRecordApproverForDating.isEmpty()){    
                    //--------------------------------------//
                    System.debug('Dating List------->' + lstRecordApproverForDating);
                    HEP_Record_Approver__c objDatingRecordApprover = lstRecordApproverForDating.get(0);
                        objApprovalwrapper.sActionReqdBy = objDatingRecordApprover.Approver_Role__r.Name;
                        objApprovalwrapper.sPromotionCode = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.LocalPromotionCode__c;
                        objApprovalwrapper.sPromotionName = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Promotion__r.PromotionName__c;
                        objApprovalwrapper.sRequestType = sRequestType;
                        DateTime dtRaisedRequsetTime = objDatingRecordApprover.Approval_Record__r.CreatedDate; 
                        
                        objApprovalwrapper.sRequestDate = dtRaisedRequsetTime.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                        objApprovalwrapper.sComment = objDatingRecordApprover.Approval_Record__r.HEP_Promotion_Dating__r.Approval_Comments__c;
                        objApprovalwrapper.sRequestor = objDatingRecordApprover.Approval_Record__r.CreatedBy.Name;
                    
                }//-------------------------------//
                
            }//-----------------------------------//
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
    }
    
    
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author    Nishit Kedia
    */
    public class ApprovalEmailWrapper{
        
        public String   sActionReqdBy  {get; set;}
        public String   sPromotionCode {get; set;}
        public String   sPromotionName {get; set;}
        public String   sRequestType   {get; set;}
        public String   sRequestDate   {get; set;}
        public String   sRequestor     {get; set;}
        public String   sComment       {get; set;}
        
    }

}