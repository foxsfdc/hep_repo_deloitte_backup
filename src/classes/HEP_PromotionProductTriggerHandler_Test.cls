@isTest
private class HEP_PromotionProductTriggerHandler_Test {
    @TestSetup
    static void testSetupMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();

        //Create User
        User objUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator','Test Last Name','test@deloitte.com',
                                                              'test@deloitte.com','testL','testL','Fox - New Release',true);

        //Create Line of Business
        HEP_Line_Of_Business__c objLineOfBusiness = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release','New Release',true);

        //Create Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','Promotion',true);

        //Create EDM Product Type.
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

        //Create EDM Title Record.
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '32321' , 'PUBLC' , objProductType.Id, true);

        //Create Territory
        List<HEP_Territory__c> lstTerritory = new List<HEP_Territory__c>{HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null,null,'USD','WW','189','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null,null,'EUR','DE','182','JDE',false),
                                                                        HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','EMEA','Subsidiary',null,null,'ARS','DE','921','JDE',false)};

        lstTerritory[0].LegacyId__c = lstTerritory[0].Name;
        lstTerritory[1].LegacyId__c = lstTerritory[1].Name;
        lstTerritory[2].LegacyId__c = lstTerritory[2].Name;
        insert lstTerritory;
        System.debug('Territory : ' + lstTerritory);

        //Create User Role
        List<HEP_User_Role__c> lstUserRoles = new List<HEP_User_Role__c>{HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[0].Id,objRole.Id,objUser.Id,false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[1].Id,objRole.Id,objUser.Id,false),
                                                                         HEP_Test_Data_Setup_Utility.createHEPUserRole(lstTerritory[2].Id,objRole.Id,objUser.Id,false)};

        insert lstUserRoles;

        //Create Promotion
        List<HEP_Promotion__c> lstPromotions = new List<HEP_Promotion__c>{HEP_Test_Data_Setup_Utility.createPromotion('Customer Promotion','Customer',null,lstTerritory[0].Id,objLineOfBusiness.Id, null,objUser.Id, false)//,
                                                                          /*HEP_Test_Data_Setup_Utility.createPromotion('Germany Promotion','National',null,lstTerritory[1].Id,objLineOfBusiness.Id, null,objUser.Id, false),
                                                                          HEP_Test_Data_Setup_Utility.createPromotion('DHE Promotion','National',null,lstTerritory[2].Id,objLineOfBusiness.Id, null,objUser.Id, false)*/};
        
        lstPromotions[0].FirstAvailableDate__c = Date.today();
        lstPromotions[0].Title__c = objTitle.Id;
        lstPromotions[0].Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');/*
        lstPromotions[1].FirstAvailableDate__c = Date.today();
        lstPromotions[1].Title__c = objTitle.Id;
        lstPromotions[2].FirstAvailableDate__c = Date.today();
        lstPromotions[2].Title__c = objTitle.Id;*/
        insert lstPromotions;

        //Create MDP Products
        List<HEP_MDP_Promotion_Product__c> lstPromotionProducts = new List<HEP_MDP_Promotion_Product__c>{HEP_Test_Data_Setup_Utility.createPromotionProduct(lstPromotions[0].Id, objTitle.id,lstTerritory[0].Id , false),
                                                                                                         HEP_Test_Data_Setup_Utility.createPromotionProduct(lstPromotions[0].Id, objTitle.id,lstTerritory[0].Id , false)};

        lstPromotionProducts[0].Product_Start_Date__c = Date.today().addDays(-1);
        lstPromotionProducts[0].Approval_Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        lstPromotionProducts[1].Product_Start_Date__c = Date.today().addDays(1);
        lstPromotionProducts[1].Approval_Status__c = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        insert lstPromotionProducts;

        

        //Create Notification Record..
        HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',true);
    }
    
    @isTest static void updatePromoProduct3() {
        List<HEP_MDP_Promotion_Product__c> lstPromotionProducts = [SELECT ID , Product_Start_Date__c , Approval_Status__c , Product_End_Date__c
                                                                   FROM HEP_MDP_Promotion_Product__c];

        lstPromotionProducts[0].Product_Start_Date__c = Date.today().addDays(-6);
        lstPromotionProducts[0].Product_End_Date__c = Date.today().addDays(5);
        lstPromotionProducts[0].Approval_Status__c = 'Rejected';
        update lstPromotionProducts;

        
    }
    
    @isTest static void updatePromoProduct2() {
        List<HEP_MDP_Promotion_Product__c> lstPromotionProducts = [SELECT ID , Product_Start_Date__c , Approval_Status__c , Product_End_Date__c
                                                                   FROM HEP_MDP_Promotion_Product__c];

        //lstPromotionProducts[0].Product_Start_Date__c = Date.today().addDays(6);
        lstPromotionProducts[0].Product_End_Date__c = Date.today().addDays(-4);
        lstPromotionProducts[1].Approval_Status__c = 'Draft';

        update lstPromotionProducts;

         

     }
    
    @isTest static void updatePromoProduct() {
        List<HEP_MDP_Promotion_Product__c> lstPromotionProducts = [SELECT ID , Product_Start_Date__c , Approval_Status__c , Product_End_Date__c
                                                                   FROM HEP_MDP_Promotion_Product__c];

        test.starttest();
        //lstPromotionProducts[0].Product_Start_Date__c = Date.today().addDays(6);
        lstPromotionProducts[0].Approval_Status__c = 'Approved';
        update lstPromotionProducts;   
         

        Test.stopTest();
    }
}