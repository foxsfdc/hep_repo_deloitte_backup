/**
* HEP_Catalog_TriggerHandler_Test-- Test class for the HEP_Catalog_TriggerHandler for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Catalog_TriggerHandler_Test{
    /**
    * HEP_Catalog_Genre_TriggerHandler_Test --  Test method to get the Catalog Genre Details on insert of records
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_Catalog_TriggerHandler_Test() {
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification','HEP_Promotion_Dating__c','Test Body','Test Type','Active','Draft',null,false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification; 
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','Test Body','Test Type','Active','Draft',null,false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification; 
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objTerritory.Flag_Country_Code__c = 'US';
        insert objTerritory;
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, null, null,null,false);
        insert objPromotion;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog; 
        System.assertEquals('Active',objCatalog.Record_Status__c); 
        HEP_CheckRecursive.clearSetIds();
        HEP_Catalog__c objCatalogRet = [SELECT Record_Status__c FROM HEP_Catalog__c WHERE ID =: objCatalog.Id];
        objCatalogRet.Record_Status__c = 'InActive'; 
        update objCatalogRet;
        Test.stoptest();
    }
}