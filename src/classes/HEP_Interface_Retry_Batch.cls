/**
* HEP_Interface_Retry_Batch --- Batch class to pick up failed transaction and invoke Retry Mechanism
* @author    Nishit Kedia
*/
public class HEP_Interface_Retry_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    //Global Variables to be used in Batch processing
    public Id cronId;
    public string sFailure = HEP_Integration_Util.getConstantValue('HEP_FAILURE');
    public String sInboundInterfaceType = HEP_Integration_Util.getConstantValue('HEP_INBOUND_INTERFACE_TYPE');
    
    
    /**
    * start --- Batch start method which is invoked when batch is scheduled
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @return   Query Locator which will return query processed on batch size 1
    * @author   Nishit Kedia
    */ 
    public Database.QueryLocator start(Database.BatchableContext objBatchableContext){
        
        Datetime dtCurrentDate = System.now();
        
        //Get all the Failed Interface Transaction eligible for a retry     
        String sbuildQuery='';
        
        //Interface Transaction obj fields
        sbuildQuery += 'Select id,Object_Id__c,Object_Name__c,Retry_Count__c,Status__c,Transaction_Datetime__c,HEP_Interface__c,HEP_Interface__r.Class__c,HEP_Interface__r.Retry_Flag__c,HEP_Interface__r.Retry_Limit__c,HEP_Interface__r.Retry_Interval__c,HEP_Interface__r.Integration_Name__c';
        
        //Object: Pickup records from Txn object
        sbuildQuery += ' From HEP_Interface_Transaction__c where ';
        
        //Filter Criteria
        sbuildQuery += ' HEP_Interface__r.Retry_Flag__c=true And Retry_Available__c=true AND HEP_Interface__r.Type__c != \'' + sInboundInterfaceType + '\' And Status__c=\'' + sFailure +'\' And Next_Retry_Time__c <=:dtCurrentDate';
        
        // Sorting Criteria
        sbuildQuery += ' order by Transaction_Datetime__c asc';
        
        System.debug('Query Built-->' + sbuildQuery);
        return Database.getQueryLocator(sbuildQuery);   
    }
    
    
    /**
    * execute --- Batch execute method which is kept in thread after start finishes
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @param    lstfailedTransactions which was picked by batch to process and retry
    * @return   nothing
    * @author   Nishit Kedia
    */ 
    public void execute(Database.BatchableContext objBatchableContext, List<HEP_Interface_Transaction__c> lstfailedTransactions){
        
        try{
            //Check whether any Transaction records are picked in Batch
            if(lstfailedTransactions != null && !lstfailedTransactions.isEmpty() ){
                
                //Instantion of failed Transaction Wrapper
                HEP_Interface_Transaction__c objFailedTxn = lstfailedTransactions.get(0);          
                String sSourceId = objFailedTxn.Object_Id__c;
                String sInterfaceTxnId = objFailedTxn.Id;
                String sInterfaceName = objFailedTxn.HEP_Interface__r.Integration_Name__c;
                
                //Invoke the Retry Mechanism Api
                HEP_InterfaceTxnResponse objResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId, sInterfaceTxnId, sInterfaceName);                                   
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            HEP_Error_Log.genericException('Error occured during Retry','Batch Class', e,'HEP_Interface_Retry_Batch','execute',null,'');
        }        
    }
    
    
    /**
    * finish --- Batch finish method which will schedule the batch after every 10 mins
    * @param    objBatchableContext is the Batchable context of the batch Instance 
    * @return   nothing
    * @author   Nishit Kedia
    */ 
    public void finish(Database.BatchableContext objBatchableContext){
        
        //Abort this Scheduled Job
        if(cronId != null){
            System.debug('cronId ' + cronId);
            System.abortJob(cronId);
        }
        
        //Instantiate the Scheduler Class
        Datetime dtCurrentBatchTime = System.Now();        
        HEP_Interface_Retry_Scheduler objRetryScheduler = new HEP_Interface_Retry_Scheduler();  
        
        //Fetch the add minutes from the Custom Label that determines the time interval in which the batch job runs for Next scheduling.
        if(System.Label.HEP_Integ_Retry_Interval != null){
            dtCurrentBatchTime = dtCurrentBatchTime.addMinutes(Integer.valueOf(System.Label.HEP_Integ_Retry_Interval)); 
            String sScheduledRuntimeExpression = String.valueOf(dtCurrentBatchTime.second()) + ' ' + String.valueOf(dtCurrentBatchTime.minute()) + ' ' + String.valueOf(dtCurrentBatchTime.hour()) + ' '  + String.valueOf(dtCurrentBatchTime.day()) +  ' ' + String.valueOf(dtCurrentBatchTime.month()) + ' '  + '? ' + String.valueOf(dtCurrentBatchTime.Year());
            
            //Schedule the Batch from Scheduler
            if(!Test.isRunningTest())
                System.schedule('HEP_Interface_Retry_Scheduler ' + String.valueof(dtCurrentBatchTime), sScheduledRuntimeExpression, objRetryScheduler);
        }
    }
    
}