@isTest
private class HEP_ApprovalInputWrapper_Test {
    
    static TestMethod void testWrapper() {
        
        Test.startTest();
        //Spend
        HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = 'Test';
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = 'Australia';
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = null;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        
        Test.stoptest();
    }
}