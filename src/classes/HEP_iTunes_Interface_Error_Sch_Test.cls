@isTest
private class HEP_iTunes_Interface_Error_Sch_Test{
    public static testmethod void ScheduleTest(){
        Test.StartTest();
        HEP_iTunes_Interface_Error_Notify_Sch schedule = new HEP_iTunes_Interface_Error_Notify_Sch();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '30 49 07 08 04 ?';
        String jobID = System.schedule('itunes interface error', sch, schedule);
        CronTrigger objCronTrigger = [SELECT Id,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id =: jobId];
        System.assertEquals(sch,objCronTrigger.CronExpression);
        Test.stopTest();
    }
}