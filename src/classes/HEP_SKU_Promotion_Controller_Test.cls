/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest 
public class HEP_SKU_Promotion_Controller_Test {
    public class SKU_Promotion_Wrapper{
        public String sPromotionName;
        public String sPromoId;
        public String sRegion;
        public String sCustomer;
        public String sLOB;
        public Date dtFAD;
        public String sMarketingDA;
        public String sStatus;
        public String sPromotionId;
    }
    @testSetup
    static void Setup(){
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        list < HEP_Services__c > objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
        
    }
    @isTest
    static void testSKUPromo (){
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@deloitte.com', 'R', 'Vis', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Dating', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        insert objTerritory;
        
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release', 'New Release', true);
        HEP_User_Role__c VRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.id, objRole.id, u.id, true);
        //SKUTEMP 
        System.runAs(u){
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', null, objTerritory.Id, null, null, null, false);
        objPromotion.EndDate__c = System.today();
        objPromotion.StartDate__c = System.today();
        objPromotion.Record_Status__c = 'Active';
        objPromotion.LineOfBusiness__c = objLOB.id;
        objPromotion.FirstAvailableDate__c = System.today();
        objPromotion.LocalPromotionCode__c = '00098';
        objPromotion.Domestic_Marketing_Manager__c = u.id;
        objPromotion.Status__c = 'Draft';
        objPromotion.Territory__c = objTerritory.Id;
        insert objPromotion;
        //Creating Catalog  Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TESTTAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', true);
        HEP_Catalog__c objCatalog1 = HEP_Test_Data_Setup_Utility.createCatalog('12354', 'TESTTAL1', 'Single', null, objTerritory.Id, 'Approved', 'Request', true);
        
        HEP_Promotion_Catalog__c objPromoCat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objPromotion.id, NULL, true);
        HEP_Promotion_Catalog__c objPromoCat1 = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog1.id, objPromotion.id, NULL, true);
        
        HEP_SKU_Master__c skuMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id , '100001', 'KINGSMAN BD+DHD-V2', 'Master', 'Digital', 'RETAIL', 'HD', true);
        HEP_SKU_Master__c skuMaster1 = new HEP_SKU_Master__c();
        
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat.id, skuMaster.Id, 'Pending', 'Unlocked', true);
        //HEP_Promotion_SKU__c objPromoSKU1 = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCat1.id, skuMaster.Id, 'Pending', 'Unlocked', true);
        
        
        System.debug('User Roles :::::' + [SELECT Id,User__c from HEP_User_Role__c]);
        Test.startTest();
        HEP_SKU_Promotion_Controller.fetchPromotionSKUPageData(skuMaster.Id);
                HEP_SKU_Promotion_Controller.fetchPromotionSKUPageData(skuMaster1.Id);
        Test.stopTest();            
        }
        
    }
}