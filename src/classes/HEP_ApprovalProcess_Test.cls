/* 
 @HEP_ApprovalProcess_Test ---Test Class for Approval process.
 @author : Deloitte
 */

@isTest
public class HEP_ApprovalProcess_Test {
    
  @testSetup
  public static void createTestUserSetup(){
    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
    HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
    //Create additonal LOVS
    List<HEP_List_Of_Values__c> additionalLov = new List<HEP_List_Of_Values__c>();
    HEP_List_Of_Values__c lov1 = new HEP_List_Of_Values__c();
    lov1.Type__c = 'EMAIL_TO_ROLES';
    lov1.Name = 'HEP_Spends_Email_Notification';
    lov1.Values__c = 'Marketing Manager - TV';
    lov1.Parent_Value__c = 'SPEND';
    lov1.Record_Status__c = 'Active';
    additionalLov.add(lov1);
    
    HEP_List_Of_Values__c lov2 = new HEP_List_Of_Values__c();
    lov2.Type__c = 'EMAIL_CC_USERS';
    lov2.Name = 'HEP_Dating_Approval_Request';
    lov2.Values__c = 'niskedia@deloitte.com';
    lov2.Parent_Value__c = 'Germany';
    lov2.Record_Status__c = 'Active';
    additionalLov.add(lov2);
    
    HEP_List_Of_Values__c lov3 = new HEP_List_Of_Values__c();
    lov3.Type__c = 'NOTIFICATION_TO_ROLES';
    lov3.Name = 'HEP_Spends_Email_Notification';
    lov3.Values__c = 'Marketing Executive - TV';
    lov3.Parent_Value__c = 'SPEND';
    lov3.Record_Status__c = 'Active';
    additionalLov.add(lov3);
    
    HEP_List_Of_Values__c lov4 = new HEP_List_Of_Values__c();
    lov4.Type__c = 'EMAIL_CC_USERS';
    lov4.Name = 'HEP_SKU_Approval_Request';
    lov4.Values__c = 'niskedia@deloitte.com';
    lov4.Parent_Value__c = 'Germany';
    lov4.Record_Status__c = 'Active';
    additionalLov.add(lov4);
    
    HEP_List_Of_Values__c lov5 = new HEP_List_Of_Values__c();
    lov5.Type__c = 'EMAIL_CC_USERS';
    lov5.Name = 'HEP_Dating_Approval_Response';
    lov5.Values__c = 'niskedia@deloitte.com';
    lov5.Parent_Value__c = 'Germany';
    lov5.Record_Status__c = 'Active';
    additionalLov.add(lov5);
    
    insert additionalLov;
    //User Setup
    User objTestUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
	User objTestUser2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest2', 'ApprovalTestUser2@hep.com','ApprovalTest2','App2','ApTest2','', true);
	
	System.runAs (objTestUser) {
	
    //Territory Set up
    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182','ESCO', false);
    insert objTerritory;
    //Global Territory
    HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
	
    //Insert Line of Business records
    HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV','TV',true);
    
    //Role Set Up FOR SPEND
    List<HEP_Role__c> lstRoles = new List<HEP_Role__c>();
    HEP_Role__c objRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPRole('Data Admin', 'Promotions' ,false);
    objRoleAdmin.Destination_User__c = objTestUser.Id;
    objRoleAdmin.Source_User__c = objTestUser.Id;  
    lstRoles.add(objRoleAdmin) ;

    HEP_Role__c objRoleMktExec = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Executive - TV', 'Promotions' ,false);
    objRoleMktExec.Destination_User__c = objTestUser.Id;
    objRoleMktExec.Source_User__c = objTestUser.Id;  
    lstRoles.add(objRoleMktExec);

    HEP_Role__c objRoleMrktgMngr = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager - TV', 'Promotions' ,false);
    objRoleMrktgMngr.Destination_User__c = objTestUser.Id;
    objRoleMrktgMngr.Source_User__c = objTestUser.Id;  
    lstRoles.add(objRoleMrktgMngr);
    
    //ROLE setup for UNLOCK
    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations(International)', 'Promotions' ,false);
    lstRoles.add(objRoleOperations);
    insert lstRoles;
    
    //User role Set Up  
    List<HEP_User_Role__c > lstUserRole = new List<HEP_User_Role__c >();
    HEP_User_Role__c objUserRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleAdmin.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleAdmin);
    HEP_User_Role__c objUserRoleMktExec = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleMktExec.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleMktExec);
    HEP_User_Role__c objUserRoleMrktgMngr = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleMrktgMngr.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleMrktgMngr);
    HEP_User_Role__c objUserRolePsGermany = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleOperations.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRolePsGermany);
    
    insert lstUserRole;
    
    //Create Hierarchy Type
    HEP_Hierarchy_Type__c objHierarchyType = HEP_Test_Data_Setup_Utility.createHiearchyType(objLOB.Type__c,'Test Hierarchy',objTerritory.Id,true);
     
    //Create Approval Type: SPEND
    HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('SPEND Approval Type', 'HEP_Market_Spend__c', 'RecordStatus__c', 
                                                                                            'Budget Approved','Rejected','SPEND',
                                                                                            'Hierarchical','Pending',objHierarchyType.Id,
                                                                                            'HEP_Spends_Email_Notification','Budget_Update_Request_Notification',
                                                                                            false);
    objApprovalType.Email_App_Template__c = 'HEP_Spends_Email_Notification';
    objApprovalType.Email_Rej_Template__c = 'HEP_Spends_Email_Notification';
    objApprovalType.Notif_App_Template__c = 'Budget_Update_Approved';
    objApprovalType.Notif_Rej_Template__c = 'Budget_Update_Rejected';
    insert objApprovalType;
    
    //Create DAting Approval Type
    HEP_Approval_Type__c objApprovalTypeDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_Dating__c', 'Locked_Status__c', 
                                                                                            'Unlocked','Locked','DATING',
                                                                                            'Static','Pending',                                                                 
                                                                                            false);
    objApprovalTypeDating.Notif_App_Template__c = 'Dating_Unlock_Request_Approved';
    objApprovalTypeDating.Notif_Rej_Template__c = 'Dating_Unlock_Request_Rejected';
    objApprovalTypeDating.Email_App_Template__c = 'HEP_Dating_Approval_Response';
    objApprovalTypeDating.Email_Rej_Template__c = 'HEP_Dating_Approval_Response';
    objApprovalTypeDating.Email_Req_Template__c = 'HEP_Dating_Approval_Request';
    insert objApprovalTypeDating;
    
    //Create SKU Unlock Approval Type
    HEP_Approval_Type__c objApprovalTypeSKuUnlock = HEP_Test_Data_Setup_Utility.createApprovalType('Product Unlock', 'HEP_Promotion_SKU__c', 'Locked_Status__c', 
                                                                                            'Unlocked','Locked','PRODUCT UNLOCK',
                                                                                            'Static','Pending',                                                                 
                                                                                            false);
    objApprovalTypeSKuUnlock.Notif_App_Template__c = 'SKU_Unlock_Request_Approved';
    objApprovalTypeSKuUnlock.Notif_Rej_Template__c = 'SKU_Unlock_Request_Rejected';
    objApprovalTypeSKuUnlock.Email_App_Template__c = 'HEP_SKU_Approval_Response';
    objApprovalTypeSKuUnlock.Email_Rej_Template__c = 'HEP_SKU_Approval_Response';
    objApprovalTypeSKuUnlock.Email_Req_Template__c = 'HEP_SKU_Approval_Request';
    insert objApprovalTypeSKuUnlock;
    
    //Create Role Hierarchy 
    List<HEP_Role_Hierarchy__c> lstRoleHieararchies = new List<HEP_Role_Hierarchy__c>();
    HEP_Role_Hierarchy__c objRoleLevel1 = HEP_Test_Data_Setup_Utility.createRoleHierarchy(objRoleMrktgMngr.Id,objRoleMktExec.Id,objHierarchyType.Id,false);
    lstRoleHieararchies.add(objRoleLevel1);
    HEP_Role_Hierarchy__c objRoleLevel2 = HEP_Test_Data_Setup_Utility.createRoleHierarchy(objRoleMktExec.Id,objRoleAdmin.Id,objHierarchyType.Id,false);
    lstRoleHieararchies.add(objRoleLevel2);
    
    insert lstRoleHieararchies;
    
    //Rule and Criteria Setup
    List<HEP_Rule__c> lstRules = new List<HEP_Rule__c>();
    HEP_Rule__c objRule1 = HEP_Test_Data_Setup_Utility.createRule('SPEND BUDGET RULE (TV)','HEP_Market_Spend__c','(1 AND 2)',false);
    lstRules.add(objRule1) ;
    
    HEP_Rule__c objRule2 = HEP_Test_Data_Setup_Utility.createRule('SPEND BUDGET RULE (GENERAL)','HEP_Market_Spend__c','(1 AND 2)',false);
    lstRules.add(objRule2) ;
    
    HEP_Rule__c objRule3 = HEP_Test_Data_Setup_Utility.createRule('Dating Unlock Rule International','HEP_Promotion_Dating__c','(1 AND 2)',false);
    lstRules.add(objRule3) ;
    
    HEP_Rule__c objRule4 = HEP_Test_Data_Setup_Utility.createRule('PRODUCT Unlock Rule International','HEP_Promotion_SKU__c','(1 AND 2)',false);
    lstRules.add(objRule4) ;   
    insert lstRules;
    
    Id RecordTypeId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName().get('Compare Field Value').getRecordTypeId();
    List<HEP_Rule_Criterion__c> lstRuleCriterias = new List<HEP_Rule_Criterion__c>();
    HEP_Rule_Criterion__c objRuleCriteria1 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Approval_LOB__c','TV',HEP_Rule_Constants.HEP_STRING_EQUALS_TO,objRule1.Id,RecordTypeId, false);
    objRuleCriteria1.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria1.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria1);
    HEP_Rule_Criterion__c objRuleCriteria2 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','100',HEP_Rule_Constants.HEP_STRING_GREATER_THAN ,objRule1.Id,RecordTypeId, false);
    objRuleCriteria2.HEP_Source_SObject_Field_Type__c = 'DOUBLE';
    objRuleCriteria2.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria2);
    HEP_Rule_Criterion__c objRuleCriteria3 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Record_Status__c','Active',HEP_Rule_Constants.HEP_STRING_EQUALS_TO,objRule2.Id,RecordTypeId, false);
    objRuleCriteria3.HEP_Source_SObject_Field_Type__c ='PICKLIST';
    objRuleCriteria3.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria3);
    HEP_Rule_Criterion__c objRuleCriteria4 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Request_Amount__c','100',HEP_Rule_Constants.HEP_STRING_GREATER_THAN ,objRule2.Id,RecordTypeId, false);
    objRuleCriteria4.HEP_Source_SObject_Field_Type__c ='DOUBLE';
    objRuleCriteria4.HEP_Serial_Number__c=2;
    lstRuleCriterias.add(objRuleCriteria4);
    HEP_Rule_Criterion__c objRuleCriteria5 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Digital_Physical__c','Digital',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
    objRuleCriteria5.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria5.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria5);
    HEP_Rule_Criterion__c objRuleCriteria6 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CC_Email_Filter__c','Germany',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
    objRuleCriteria6.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria6.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria6);
    HEP_Rule_Criterion__c objRuleCriteria7 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Record_Status__c','Active',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule4.Id,RecordTypeId, false);
    objRuleCriteria7.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria7.HEP_Serial_Number__c = 1;
    lstRuleCriterias.add(objRuleCriteria7);
    HEP_Rule_Criterion__c objRuleCriteria8 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CC_Email_Filter__c','Germany',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule4.Id,RecordTypeId, false);
    objRuleCriteria8.HEP_Source_SObject_Field_Type__c= 'STRING';
    objRuleCriteria8.HEP_Serial_Number__c = 2;
    lstRuleCriterias.add(objRuleCriteria8);
    
    insert lstRuleCriterias;
    
    //Approval Type Role Setup
    List<HEP_Approval_Type_Role__c> lstApprovalTypeRole = new List<HEP_Approval_Type_Role__c>();
    HEP_Approval_Type_Role__c objApprovalTypeRole1 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalType.Id, objRoleMktExec.Id, 'SEQUENTIAL', false);
    objApprovalTypeRole1.Rule__c = objRule1.Id;
    objApprovalTypeRole1.Sequence__c = 1;
    lstApprovalTypeRole.add(objApprovalTypeRole1);

    HEP_Approval_Type_Role__c objApprovalTypeRole2 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalType.Id, objRoleAdmin.Id, 'ALWAYS', false);
    objApprovalTypeRole2.Rule__c = objRule2.Id;
    objApprovalTypeRole1.Sequence__c = 2;
    lstApprovalTypeRole.add(objApprovalTypeRole2);
    
    //Approver role For Dating
    HEP_Approval_Type_Role__c objApprovalTypeRole3 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeDating.Id, objRoleOperations.Id, 'ALWAYS', false);
    objApprovalTypeRole3.Rule__c = objRule3.Id;
    objApprovalTypeRole3.Sequence__c = 1;
    lstApprovalTypeRole.add(objApprovalTypeRole3);
    
    //Approver Role for SKU
    HEP_Approval_Type_Role__c objApprovalTypeRole4 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeSKuUnlock.Id, objRoleOperations.Id, 'ALWAYS', false);
    objApprovalTypeRole4.Rule__c = objRule4.Id;
    objApprovalTypeRole4.Sequence__c = 1;
    lstApprovalTypeRole.add(objApprovalTypeRole4);
    
    insert lstApprovalTypeRole;
    
    //Create Global Promotion:
	HEP_Promotion__c objTestGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Global','Global','',objGlobalTerritory.Id,objLOB.Id,'','',true);
	//Create National Promotion
	HEP_Promotion__c objTestNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Germany NP','National',objTestGlobalPromotion.Id,objTerritory.Id,objLOB.Id,'','',true);
	
	//Notification Template Creation
	HEP_Notification_Template__c objGlobalPromoTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Creation','HEP_Promotion__c','A global promotion has been created','Global promotion creation','Active','-','Global_Promotion_Creation',true);
	HEP_Notification_Template__c objSpendNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Request_Notification','HEP_Market_Spend__c','A new total budget request of $<Request_Amount__c> has been submitted for approval.','Budget Approved','Active','-','Budget_Update_Request_Notification',true);
	HEP_Notification_Template__c objSpendAppNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Approved','HEP_Market_Spend__c','A Budget has been Approved','Budget Request','Active','Rejected','Budget_Update_Approved',true);
	HEP_Notification_Template__c objSpendRejNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Rejected','HEP_Market_Spend__c','A new total budget request on <Promotion__r.Name> is rejected','Budget Request','Active','Budget Approved','Budget_Update_Rejected',true);
	HEP_Notification_Template__c objfadNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','A new FAD is done','FAD Date Change','Active','-','Global_FAD_Date_Change',true);
	
	}
  }

	public static TestMethod void  testDatingUnlockRequest(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	HEP_Territory__c objGerTerritory = [Select Id,
  											Name 
  											from HEP_Territory__c
  											where Name = 'Germany'];
  	
	  	//Run in the context of User:
  	System.runAs(objTestUser){
		HEP_Promotions_DatingMatrix__c objVODReleaseDate = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objGerTerritory.Id, objGerTerritory.Id, false);
        objVODReleaseDate.Media_Type__c ='Digital';
        objVODReleaseDate.Projected_FAD_Logic__c = 'Monday';
        insert objVODReleaseDate;
        
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objGerTerritory.Id, objTestPromotion.Id, objVODReleaseDate.Id, false);
        objDatingRecord.Status__c = 'Confirmed';
        objDatingRecord.Locked_Status__c = 'Locked';
        insert objDatingRecord;
        
		//Invoke Approval Process:
		String approvalRecordId = objDatingRecord.Id;
		String approvalType='DATING';
		String territoryId = objGerTerritory.Id;
		HEP_ApprovalProcess pr= new HEP_ApprovalProcess();
		Test.startTest();
		Boolean status = pr.invokeApprovalProcess(approvalRecordId,approvalType,territoryId);
		Test.stopTest();
  	}
  }
  
  public static TestMethod void  testDatingUnlockRequestAutoApprove(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	HEP_Territory__c objGerTerritory = [Select Id,
  											Name 
  											from HEP_Territory__c
  											where Name = 'Germany'];
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
		HEP_Promotions_DatingMatrix__c objVODReleaseDate1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objGerTerritory.Id, objGerTerritory.Id, false);
        objVODReleaseDate1.Projected_FAD_Logic__c = 'Monday';
        objVODReleaseDate1.Media_Type__c ='Physical';
        insert objVODReleaseDate1;
        
        HEP_Promotion_Dating__c objDatingRecord1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objGerTerritory.Id, objTestPromotion.Id, objVODReleaseDate1.Id, false);
        objDatingRecord1.Status__c = 'Confirmed';
        objDatingRecord1.Locked_Status__c = 'Locked';
        insert objDatingRecord1;
        
		//Invoke Approval Process:
		String approvalRecordId = objDatingRecord1.Id;
		String approvalType='DATING';
		String territoryId = objGerTerritory.Id;
		HEP_ApprovalProcess pr= new HEP_ApprovalProcess();
		Test.startTest();
		Boolean status = pr.invokeApprovalProcess(approvalRecordId,approvalType,territoryId);
		Test.stopTest();
  	}
  }
  	
	
  public static TestMethod void  testBudgetRequestApproved(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  												
  	//Fetch the created Test User:
  	User objTestUser2 = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser2@hep.com'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',true);
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = 2000;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Current';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = objTestUser2.Id;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
               
        //Check Approval Record is Created or Not:
        HEP_Approvals__c objApprovalRecordCreated = [Select Id,
        													Approval_Status__c,
        													Approval_Type__c,
        													Approval_Type__r.In_Progress_Status__c,
        													Approved_Count__c,
        													Total_Approval_Count__c,
        													HEP_Market_Spend__c 
        													from HEP_Approvals__c
        													where HEP_Market_Spend__c = :objTestMarketSpend.Id];
        //Check Approvers Count													
        System.assertEquals(2,objApprovalRecordCreated.Total_Approval_Count__c);
        
        //Fetch Spend Record:
        HEP_Market_Spend__c objUpdatedSpend = [Select Id,
        												Request_Amount__c,
        												RecordStatus__c 
        												from HEP_Market_Spend__c 
        												where Id =:objTestMarketSpend.Id];
        //Check parent Value is Set To In progress Status
        System.assertEquals(objApprovalRecordCreated.Approval_Type__r.In_Progress_Status__c,objUpdatedSpend.RecordStatus__c);
        													
        
        //Approve the Record As Marketing Exceutive:
        HEP_CheckRecursive.clearSetIds();
        HEP_Record_Approver__c objRecordToApproveLevel1 = [Select Id,
        													Approval_Record__c,
        													Status__c,
        													IsActive__c
        													from
        													HEP_Record_Approver__c
        													where 
        													Approval_Record__c =:objApprovalRecordCreated.Id
        													And
        													IsActive__c = true
        													And
        													Status__c = 'Pending'];
        
        //Approve:
        objRecordToApproveLevel1.Status__c = 'Approved';
        update objRecordToApproveLevel1;
        
         //Approve the Record As Data Admin:
       	HEP_CheckRecursive.clearSetIds();
        HEP_Record_Approver__c objRecordToApproveLevel2 = [Select Id,
        													Approval_Record__c,
        													Status__c,
        													IsActive__c
        													from
        													HEP_Record_Approver__c
        													where 
        													Approval_Record__c =:objApprovalRecordCreated.Id
        													And
        													IsActive__c = true
        													And
        													Status__c = 'Pending'];
        
        //Approve:
        objRecordToApproveLevel2.Status__c = 'Approved';
        update objRecordToApproveLevel2;
        
        
        HEP_Approvals__c objApprovalRecordUpdated= [Select Id,
        													Approval_Status__c,
        													Approval_Type__c,
        													Approval_Type__r.In_Progress_Status__c,
        													Approval_Type__r.Approved_Status__c,
        													Approved_Count__c,
        													Rejected_Count__c,
        													Total_Approval_Count__c,
        													HEP_Market_Spend__c,
        													HEP_Market_Spend__r.RecordStatus__c
        													from HEP_Approvals__c
        													where HEP_Market_Spend__c = :objTestMarketSpend.Id];
        													
        System.assertEquals(2,objApprovalRecordUpdated.Approved_Count__c);
        //Even though the Budget is Rejected it is Approved
        System.assertEquals(objApprovalRecordUpdated.Approval_Type__r.Approved_Status__c,objApprovalRecordUpdated.HEP_Market_Spend__r.RecordStatus__c);
        
       
        
        Test.stopTest();        
        	
  	}	
      
  }  
  
  public static TestMethod void  testBudgetRequestRejected(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  //Fetch the created Test User:
  	User objTestUser2 = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser2@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',true);
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = 2000;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Current';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = objTestUser2.Id;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
               
        //Check Approval Record is Created or Not:
        HEP_Approvals__c objApprovalRecordCreated = [Select Id,
        													Approval_Status__c,
        													Approval_Type__c,
        													Approval_Type__r.In_Progress_Status__c,
        													Approved_Count__c,
        													Total_Approval_Count__c,
        													HEP_Market_Spend__c 
        													from HEP_Approvals__c
        													where HEP_Market_Spend__c = :objTestMarketSpend.Id];
        //Check Approvers Count													
        System.assertEquals(2,objApprovalRecordCreated.Total_Approval_Count__c);
        
        //Fetch Spend Record:
        HEP_Market_Spend__c objUpdatedSpend = [Select Id,
        												Request_Amount__c,
        												RecordStatus__c 
        												from HEP_Market_Spend__c 
        												where Id =:objTestMarketSpend.Id];
        //Check parent Value is Set To In progress Status
        System.assertEquals(objApprovalRecordCreated.Approval_Type__r.In_Progress_Status__c,objUpdatedSpend.RecordStatus__c);
        													
        
        //Approve the Record As Marketing Exceutive:
        HEP_CheckRecursive.clearSetIds();
        HEP_Record_Approver__c objRecordToApproveLevel1 = [Select Id,
        													Approval_Record__c,
        													Status__c,
        													IsActive__c
        													from
        													HEP_Record_Approver__c
        													where 
        													Approval_Record__c =:objApprovalRecordCreated.Id
        													And
        													IsActive__c = true
        													And
        													Status__c = 'Pending'];
        
        //Approve:
        objRecordToApproveLevel1.Status__c = 'Approved';
        update objRecordToApproveLevel1;
        
         //Approve the Record As Data Admin:
       	HEP_CheckRecursive.clearSetIds();
        HEP_Record_Approver__c objRecordToApproveLevel2 = [Select Id,
        													Approval_Record__c,
        													Status__c,
        													IsActive__c
        													from
        													HEP_Record_Approver__c
        													where 
        													Approval_Record__c =:objApprovalRecordCreated.Id
        													And
        													IsActive__c = true
        													And
        													Status__c = 'Pending'];
        
        //Approve:
        objRecordToApproveLevel2.Status__c = 'Rejected';
        update objRecordToApproveLevel2;
        
        
        HEP_Approvals__c objApprovalRecordUpdated= [Select Id,
        													Approval_Status__c,
        													Approval_Type__c,
        													Approval_Type__r.In_Progress_Status__c,
        													Approval_Type__r.Rejected_Status__c,
        													Approved_Count__c,
        													Rejected_Count__c,
        													Total_Approval_Count__c,
        													HEP_Market_Spend__c,
        													HEP_Market_Spend__r.RecordStatus__c
        													from HEP_Approvals__c
        													where HEP_Market_Spend__c = :objTestMarketSpend.Id];
        													
        System.assertEquals(1,objApprovalRecordUpdated.Approved_Count__c);
        System.assertEquals(1,objApprovalRecordUpdated.Rejected_Count__c);
        //Even though the Budget is Rejected it is Approved
        System.assertEquals(objApprovalRecordUpdated.Approval_Type__r.Rejected_Status__c,objApprovalRecordUpdated.HEP_Market_Spend__r.RecordStatus__c);
        
       
        
        Test.stopTest();        
        	
  	}	
      
  }   
  
   public static TestMethod void  testBudgetRequestAutoApproved(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',false);
		objTestMarketSpend.Record_Status__c = 'Inactive';
		insert objTestMarketSpend;
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = -10;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Budget Update';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = null;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
        System.debug('Auto Approval Method status-->'+bApproval);
        //Fetch Spend Record:
        HEP_Market_Spend__c objUpdatedSpend = [Select Id,
        												Request_Amount__c,
        												RecordStatus__c 
        												from HEP_Market_Spend__c 
        												where Id =:objTestMarketSpend.Id];
        												
        //Even though the Budget is Rejected it is Approved
        System.assertEquals(0,objUpdatedSpend.Request_Amount__c);
        //System.assertEquals('Budget Approved',objUpdatedSpend.RecordStatus__c);
         
        
          
        Test.stopTest();        
        	
  	}	
      
  } 
  
  public static TestMethod void  testBudgetRequestAutoRejected(){
  	
  	//Fetch the created Test User:
  	User objTestUser = [Select 	Id,
  								Name 
  								from User 
  								where Email = 'ApprovalTestUser2@hep.com'];
  	
  	//fetch promotion:
  	HEP_Promotion__c objTestPromotion = [Select Id,
  												Territory__c,
  												Name 
  												from HEP_Promotion__c 
  												where Promotion_Type__c ='National'];
  	
  	
  	//Run in the context of User:
  	System.runAs(objTestUser){
  		//Create Spend:
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154',objTestPromotion.Id,'None',true);
		
		//GL Account
		HEP_GL_Account__c objGlAcc = HEP_Test_Data_Setup_Utility.createGLAccount('TestGL','Test GL Account','Test Account','Finance','Active', true);
		
		//create Spend Details
		HEP_Market_Spend_Detail__c objTestSpendDetails =  HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objTestMarketSpend.Id,objGlAcc.Id,'Active',false);
		objTestSpendDetails.RequestAmount__c = 0;
		objTestSpendDetails.Budget__c =50000;
		objTestSpendDetails.Type__c ='Current';
		insert objTestSpendDetails;
		
		//Invoke Approval Process:
		
		//Spend
		HEP_ApprovalInputWrapper objInputApprovalApi = new HEP_ApprovalInputWrapper();
        objInputApprovalApi.sApprovalRecordId = objTestMarketSpend.Id;
        objInputApprovalApi.sApprovalTypeName = 'SPEND';
        objInputApprovalApi.sterritoryId = objTestPromotion.Territory__c;
        objInputApprovalApi.sRequesterComment = 'Please Approve';
        objInputApprovalApi.sPriorSubmittersUserIds = null;
        objInputApprovalApi.sLineOfBuisness = 'TV';
        System.debug('Wrapper Data 1 --> ' + objInputApprovalApi);
        
        Test.startTest();
        
        Boolean bApproval = new HEP_ApprovalProcess().invokeApprovalProcess (objInputApprovalApi);
        
        //Fetch Spend Record:
        HEP_Market_Spend__c objUpdatedSpend = [Select Id,
        												Request_Amount__c,
        												RecordStatus__c 
        												from HEP_Market_Spend__c 
        												where Id =:objTestMarketSpend.Id];
        												
        //Even though the Budget is Rejected it is Approved
        System.assertEquals('Rejected',objUpdatedSpend.RecordStatus__c);
          
        Test.stopTest();        
        	
  	}	
      
  } 
  
}