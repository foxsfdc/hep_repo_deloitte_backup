/*
*	HEP_Csv_Parser -- Class to accept emails and then store the csv attachment in it
*	invoked from email service HEP_Csv_Parser
*	@author    Mayank Jaggi
*/

global class HEP_Csv_Parser implements Messaging.InboundEmailHandler 
{
	/*
	*	handleInboundEmail --- actual method that accepts the email and parses the csv attachment in it
	*	@param  instance of Messaging.inboundEmail class, instance of Messaging.InboundEnvelope class
    * 	@return instance of Messaging.InboundEmailResult class
	*	@author    Mayank Jaggi
	*/

	global Messaging.InboundEmailResult handleInboundEmail (Messaging.inboundEmail email, 
		Messaging.InboundEnvelope envelope)
	{
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.binaryAttachments;
		String csvbody=''; 
		String csvFileName = '';
		string sInboundEmailId = '';
		if(bAttachments.size() == 1){
			Messaging.InboundEmail.BinaryAttachment binAttachInstanceforError = bAttachments[0];
			 if(binAttachInstanceforError.filename.endsWith('.csv'))		
					csvbody = binAttachInstanceforError.body.toString();
		}
		try{
			if(bAttachments.size() == 1)
			{
				Messaging.InboundEmail.BinaryAttachment binAttachInstance = bAttachments[0];
				if(binAttachInstance.filename.endsWith('.csv'))		
					{ 
						system.debug('csv file name --> ' + binAttachInstance.filename);
						//csvFileName = binAttachInstance.filename;
						csvFileName = string.valueOf(datetime.now());
						csvbody = binAttachInstance.body.toString();
             				
             			HEP_Inbound_Email__c record = new HEP_Inbound_Email__c();
             			record.Email_Address__c = email.fromAddress;
             			record.Type__c = HEP_Utility.getConstantValue('HEP_ULTIMATE_CSV');		
             			insert record;
             			sInboundEmailId = string.valueof(record.id);

             			// call createNewFile in HEP_Integration_Util to save file
						HEP_Integration_Util.createNewFile(string.valueOf(record.id), csvbody, csvFileName);
			            system.debug('Attachment: ' + csvFileName + ' saved. Starting parsing now.');

			            // calling getCSVBody() in class HEP_Import_Data_From_CSV_Using_Email 
						//and sending body of attachment to it
						HEP_Import_Data_From_CSV_Using_Email.getCSVBody(csvbody);	
					}
					
			}
			system.debug('result of messaging class ---- ' + result);
			createTransactionEntry(HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'), sInboundEmailId, '', '');
			return result;
		}catch(exception ex){
             //Code to log error..
            HEP_Error_Log.genericException('Error occured while loading financial ultimate data', '', ex, 'HEP_Csv_Parser','handleInboundEmail', null, '');
            createTransactionEntry(HEP_Utility.getConstantValue('HEP_FAILURE'), '', 'Line no. : ' + ex.getLineNumber() + '  Error Type : ' + ex.getTypeName() + '  Error Msg : ' + ex.getMessage(), csvbody);
            return null;
        }    	
	}
	void createTransactionEntry(string sStatus, string sParentRecord, string sErrorMessage, string sFileContent){
		system.debug('sStatus : ' + sStatus);
		system.debug('sParentRecord : ' + sParentRecord);
		system.debug('sErrorMessage : ' + sErrorMessage);
    	string sHEP_Financials_Ultimate = 'HEP_Financials_Ultimate';
    	string sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
    	string sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
    	string sFileName = HEP_Utility.getConstantValue('HEP_ULTIMATE_CSV');

    	list<HEP_Interface__c> lstInterface = [select id from HEP_Interface__c where Integration_Name__c = :sHEP_Financials_Ultimate];
        if(lstInterface != null && !lstInterface.isEmpty()){
    		HEP_Interface_Transaction__c objIntTxn = new HEP_Interface_Transaction__c();
    		objIntTxn.HEP_Interface__c = lstInterface[0].id;
    		objIntTxn.Retry_Count__c = 1;   
    		objIntTxn.Transaction_Datetime__c = System.now(); 
        	if(string.isNotBlank(sParentRecord))
        		objIntTxn.Object_Id__c = sParentRecord;
        	if(sStatus == sHEP_Int_Txn_Response_Status_Success)
        		objIntTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
        	else
        		objIntTxn.Status__c = sHEP_FAILURE;
        	insert objIntTxn;
        	if(sStatus == sHEP_FAILURE){
        		HEP_Interface_Transaction_Error__c objIntTxnError = new  HEP_Interface_Transaction_Error__c();
	            objIntTxnError.Status__c = sHEP_FAILURE;
	            objIntTxnError.Error_Datetime__c = System.now();
	            objIntTxnError.Error_Description__c = String.valueof(sErrorMessage).left(32765); 
	            objIntTxnError.HEP_Interface_Transaction__c = objIntTxn.id;
	            insert objIntTxnError;
	            // call createNewFile in HEP_Integration_Util to save file
				HEP_Integration_Util.createNewFile(string.valueOf(objIntTxnError.id), sFileContent, sFileName);
        	}
        }	
	}
}