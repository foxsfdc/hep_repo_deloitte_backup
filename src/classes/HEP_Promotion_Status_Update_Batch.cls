/**
* HEP_Promotion_Status_Update -  Class for updating the Status of Customer promotion as "Live" and "Completed" on the basis of the product Start date and End Date.
* @author    Shantanu Mahajan
*/


global class HEP_Promotion_Status_Update_Batch implements Database.Batchable<sObject>{
    
    global final static String sCompletedStatus = HEP_Utility.getConstantValue('HEP_PROMOTION_STATUS_COMPLETED');
    global final static String sLiveStatus = HEP_Utility.getConstantValue('HEP_PROMOTION_STATUS_LIVE');
    global final static String sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'); 
    global final static String sApproved = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
    global final static String sCustomerPromotionType = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
    
    global Database.QueryLocator start(Database.BatchableContext BC){
       
        String sQuery = '';
        sQuery = 'SELECT id FROM HEP_Promotion__c WHERE Record_Status__c =: sActive AND Promotion_Type__c = :sCustomerPromotionType';
                
        return Database.getQueryLocator(sQuery);
        
    }
    
    global void execute(Database.BatchableContext BC, List<HEP_Promotion__c> lstPromotion){
        
        System.debug('lstPromotion>>' + lstPromotion);
        List<HEP_Promotion__c> lstPromoUpdate = new List<HEP_Promotion__c>();
        
        for(HEP_Promotion__c objPromotion : [SELECT id,Status__c,Name,Customer_Promotion_Status__c,(SELECT id,Product_Start_Date__c,Product_End_Date__c,Approval_Status__c FROM HEP_Promotion_MDP_Products__r WHERE Record_Status__c =: sActive) FROM HEP_Promotion__c WHERE id IN : lstPromotion]){
            Date dtMinDate = date.today().addDays(1);
            Date dtMaxDate = date.today().addDays(-1);
            Integer iApprovedProdCount = 0;
            for(HEP_MDP_Promotion_Product__c objProduct : objPromotion.HEP_Promotion_MDP_Products__r){
                if(objProduct.Approval_Status__c == sApproved){
                    iApprovedProdCount++;
                    if(dtMinDate >= objProduct.Product_Start_Date__c){
                        dtMinDate = objProduct.Product_Start_Date__c;
                    }
                    
                    if(dtMaxDate <= objProduct.Product_End_Date__c){
                        dtMaxDate = objProduct.Product_End_Date__c;
                    }
                }
            }
            System.debug('dtMinDate>>' + dtMinDate);
            System.debug('dtMaxDate>>' + dtMaxDate);
            if(objPromotion.Status__c == sApproved
                && dtMinDate <= date.today() 
                && dtMaxDate >= date.today() 
                && objPromotion.Customer_Promotion_Status__c != sLiveStatus){
                        
                System.debug('objPromotion>>' + objPromotion);
                objPromotion.Customer_Promotion_Status__c = sLiveStatus;
                lstPromoUpdate.add(objPromotion);
            }else if(objPromotion.Status__c == sApproved
                && dtMinDate <= date.today() 
                && dtMaxDate <= date.today()){
                System.debug('Name : ' +objPromotion.Name);
                objPromotion.Customer_Promotion_Status__c = sCompletedStatus;
                lstPromoUpdate.add(objPromotion);
                
            }else if(objPromotion.Status__c != sApproved
              && objPromotion.Customer_Promotion_Status__c == sLiveStatus
                && iApprovedProdCount > 0
                && iApprovedProdCount != objPromotion.HEP_Promotion_MDP_Products__r.size()){
                    
                objPromotion.Customer_Promotion_Status__c= sCompletedStatus;
                lstPromoUpdate.add(objPromotion);
            }else if(iApprovedProdCount == 0
                && String.isNotBlank(objPromotion.Customer_Promotion_Status__c)){
                objPromotion.Customer_Promotion_Status__c= NULL;
                lstPromoUpdate.add(objPromotion);
            }
        }       
        System.debug('lstPromoUpdate>>' + lstPromoUpdate);
        if(lstPromoUpdate.size() > 0){
            update lstPromoUpdate;
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}