/**
 * HEP_CatalogDetailsController_Test --- Test class for HEP_CatalogDetailsController
 * @author  Nidhin V K
 */
@isTest
private class HEP_CatalogDetailsController_Test {

    @testSetup static void setup() { 
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', NULL, NULL, 
                                        'EUR', 'DE', '182', 'ESCO', true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('SAMPLE123456', 'SAMPLE', 'Single', NULL, 
                                    objTerritory.Id, 'Approved', 'Master', false);
        objCatalog.Licensor__c = 'FOX';
        insert objCatalog;

        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=objCatalog.id;
        insert attach;
    }
    
    @isTest static void testBasicMethods() {
        HEP_Catalog__c objCatalog = [SELECT Id FROM HEP_Catalog__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_Catalog_Details')); 
        System.currentPageReference().getParameters().put('catId', objCatalog.Id);
        Test.startTest();
        HEP_CatalogDetailsController objCTRL = new HEP_CatalogDetailsController();
        if(objCTRL.bHasPageAccess)
            System.assertEquals(NULL, objCTRL.checkPermissions());
        else{
            PageReference retURL = new PageReference('/apex/HEP_AccessDenied');
            retURL.setRedirect(true);
            System.assertEquals(retURL.getUrl(), objCTRL.checkPermissions().getUrl());
        }
        Test.stopTest();
    }
    
    @isTest static void test_getHeaderDetails() {
        HEP_Catalog__c objCatalog = [SELECT Id FROM HEP_Catalog__c LIMIT 1];
        Test.startTest();
        HEP_CatalogDetailsController.Header objHeader = HEP_CatalogDetailsController.getHeaderDetails(objCatalog.Id,'');
        HEP_CatalogDetailsController obj =new HEP_CatalogDetailsController();
        HEP_CatalogDetailsController.getImageUrl(objCatalog.Id);
        obj.sCatalogId = objCatalog.id;
        obj.sBinaryData = 'Unit Test Attachment Body';
        obj.sFileName = 'test Title';
        obj.sDocumentDescription = 'Desc';
        obj.uploadFile();
        System.assertEquals(objCatalog.Id,objHeader.sCatalogId);
        Test.stopTest();
    }
}