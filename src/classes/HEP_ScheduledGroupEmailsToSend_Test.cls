@IsTest
public with sharing class HEP_ScheduledGroupEmailsToSend_Test {
	
@testSetup 
static void testSetupData() {
		/*User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;
	    
    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
    	
    		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
			HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

			HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

			HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
			objSpend.Unique_Id__c = 'test unique id';
			insert objSpend;
			//objApprovalSpend.HEP_Market_Spend__c = objSpend.id;
			//update objApprovalSpend;


			HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
	    	objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
	    	insert objUserRole;
	    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
	    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objUserRoleOperation;

	    	HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'Spend','Hierarchical', 'Pending', true);
	    	HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

	    	//HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
	    	HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

	    	HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

	    	HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
	    	objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objRecordApprover;

	    	system.debug('user role -----> '+objUserRoleOperation);
	    	system.debug('record approver -----> '+objRecordApprover);

			objApprovalUnlockDating.HEP_Promotion_Dating__c = objDatingRecordNational.id;
			update objApprovalUnlockDating;
			
			HEP_Outbound_Email__c outboundEmail = new HEP_Outbound_Email__c();
			outboundEmail.Record_Id__c = objRecordApprover.Id;
			outboundEmail.Email_Template_Name__c = 'HEP_Customer_Promotion_SUBMITTED';
			outboundEmail.Email_Sent__c = false;
			outboundEmail.Object_API__c = 'HEP_Record_Approver__c';
			outboundEmail.To_Email_Address__c = 'anupprakash9@deloitte.com';
			outboundEmail.CC_Email_Address__c = 'anupprakash9@deloitte.com';
			insert outboundEmail;
			*/
			HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
			
}	
		public static testmethod void  test_processGroupEmails(){
			User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
		    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
		    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
		    objRole.Destination_User__c = u.Id;
		    objRole.Source_User__c = u.Id;
		    insert objRole;
		    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
		    objRoleOperations.Destination_User__c = u.Id;
		    objRoleOperations.Source_User__c = u.Id;
		    insert objRoleOperations;
		    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
		    objRoleFADApprover.Destination_User__c = u.Id;
		    objRoleFADApprover.Source_User__c = u.Id;
		    insert objRoleFADApprover;
		    
		    //User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
	    	//HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
	    	//HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];
	
	    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
	    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);
	
	    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
	    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
    	
    		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
			HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

			HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

			HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
			objSpend.Unique_Id__c = 'test unique id';
			insert objSpend;


			HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
	    	objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
	    	insert objUserRole;
	    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
	    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objUserRoleOperation;

	    	HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'Spend','Hierarchical', 'Pending', true);
	    	HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

	    	HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

	    	HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

	    	HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
	    	objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objRecordApprover;

			objApprovalUnlockDating.HEP_Promotion_Dating__c = objDatingRecordNational.id;
			update objApprovalUnlockDating;
			
			HEP_Outbound_Email__c outboundEmail = new HEP_Outbound_Email__c();
			outboundEmail.Record_Id__c = objRecordApprover.Id;
			outboundEmail.Email_Template_Name__c = 'HEP_Customer_Promotion_SUBMITTED';
			outboundEmail.Email_Sent__c = false;
			outboundEmail.Object_API__c = 'HEP_Record_Approver__c';
			outboundEmail.To_Email_Address__c = 'anupprakash9@deloitte.com';
			outboundEmail.CC_Email_Address__c = 'anupprakash9@deloitte.com';
			insert outboundEmail; 
			Test.startTest();
		        Datetime dt = Datetime.now().addMinutes(1);
		        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		        System.schedule('Sample_Heading', CRON_EXP, new HEP_ScheduledGroupEmailsToSend () );   
		    Test.stopTest();
		}
		
		public static testmethod void  test_processGroupEmails_HEP_TPR_National_Promotion_SUBMITTED(){
			User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
		    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
		    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
		    objRole.Destination_User__c = u.Id;
		    objRole.Source_User__c = u.Id;
		    insert objRole;
		    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
		    objRoleOperations.Destination_User__c = u.Id;
		    objRoleOperations.Source_User__c = u.Id;
		    insert objRoleOperations;
		    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
		    objRoleFADApprover.Destination_User__c = u.Id;
		    objRoleFADApprover.Source_User__c = u.Id;
		    insert objRoleFADApprover;
		    
	    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
	    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);
	
	    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
	    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
    	
    		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
			HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

			HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

			HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
			objSpend.Unique_Id__c = 'test unique id';
			insert objSpend;


			HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
	    	objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
	    	insert objUserRole;
	    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
	    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objUserRoleOperation;

	    	HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'Spend','Hierarchical', 'Pending', true);
	    	HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

	    	HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

	    	HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

	    	HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
	    	objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objRecordApprover;

			objApprovalUnlockDating.HEP_Promotion_Dating__c = objDatingRecordNational.id;
			update objApprovalUnlockDating;
			
			HEP_Outbound_Email__c outboundEmail = new HEP_Outbound_Email__c();
			outboundEmail.Record_Id__c = objRecordApprover.Id;
			outboundEmail.Email_Template_Name__c = 'HEP_TPR_National_Promotion_SUBMITTED';
			outboundEmail.Email_Sent__c = false;
			outboundEmail.Object_API__c = 'HEP_Record_Approver__c';
			outboundEmail.To_Email_Address__c = 'anupprakash9@deloitte.com';
			outboundEmail.CC_Email_Address__c = 'anupprakash9@deloitte.com';
			insert outboundEmail; 
			Test.startTest();
		        Datetime dt = Datetime.now().addMinutes(1);
		        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		        System.schedule('Sample_Heading', CRON_EXP, new HEP_ScheduledGroupEmailsToSend () );   
		    Test.stopTest();
		}
		
		public static testmethod void  test_processGroupEmails_HEP_SKU_Lock_Email_Reminder(){
			User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
		    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
		    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
		    objRole.Destination_User__c = u.Id;
		    objRole.Source_User__c = u.Id;
		    insert objRole;
		    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
		    objRoleOperations.Destination_User__c = u.Id;
		    objRoleOperations.Source_User__c = u.Id;
		    insert objRoleOperations;
		    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
		    objRoleFADApprover.Destination_User__c = u.Id;
		    objRoleFADApprover.Source_User__c = u.Id;
		    insert objRoleFADApprover;
		    
	    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
	    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);
	
	    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
	    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
    	
    		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
			HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

			HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

			HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
			objSpend.Unique_Id__c = 'test unique id';
			insert objSpend;


			HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
	    	objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
	    	insert objUserRole;
	    	HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
	    	objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objUserRoleOperation;

	    	HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'Spend','Hierarchical', 'Pending', true);
	    	HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

	    	HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

	    	HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

	    	HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
	    	objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
	    	insert objRecordApprover;

			objApprovalUnlockDating.HEP_Promotion_Dating__c = objDatingRecordNational.id;
			update objApprovalUnlockDating;
			
			HEP_Outbound_Email__c outboundEmail = new HEP_Outbound_Email__c();
			outboundEmail.Record_Id__c = objRecordApprover.Id;
			outboundEmail.Email_Template_Name__c = 'HEP_SKU_Lock_Email_Reminder';
			outboundEmail.Email_Sent__c = false;
			outboundEmail.Object_API__c = 'HEP_Record_Approver__c';
			outboundEmail.To_Email_Address__c = 'anupprakash9@deloitte.com';
			outboundEmail.CC_Email_Address__c = 'anupprakash9@deloitte.com';
			insert outboundEmail; 
			Test.startTest();
		        Datetime dt = Datetime.now().addMinutes(1);
		        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		        System.schedule('Sample_Heading', CRON_EXP, new HEP_ScheduledGroupEmailsToSend () );   
		    Test.stopTest();
		}
    
}