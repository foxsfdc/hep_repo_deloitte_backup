/**
 * HEP_Approval_Component_Controller --- Component Controller for Approve and Reject Text Links 
 * @author    Nishit Kedia
 */
public class HEP_Approval_Component_Controller {

    //Promotion dating record id is passed in this variable
    public Id recordApproverId {
        get;
        set;
    }
    
     public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }

    //Approve text will fetch all dynamic Merge Variables for PAge when Loading the component
    public string sApproveText {
        get {
            if (String.isBlank(sApproveText)) {
                fetchSubjectDetails();
            }
            return sApproveText;
        }
        set;
    }

    //This variable is used to render table on Email template
    public String sEmailSubject {
        get;
        set;
    }

    //Global variable

    public string noReply {
        get;
        set;
    }
    
    //This variable stores the Email Address
    public String sGlobalEmailAddress{
        get;
        set;
    }

    public string sRequestType {
        get;
        set;
    }

    /**
     * fetchSubjectDetails --- Creation of Inline Subject and Body of Replying in MailTo Context
     * @return nothing
     * @author    Nishit Kedia
     */
    public void fetchSubjectDetails() {
        //------------------------------------//
        try {
            //Api Constant Strings

            //fetch the List of Record Approvers from the Id           
            List < HEP_Record_Approver__c > lstRecordApprovers = [SELECT Id,
                Name,
                Approval_Record__r.Approval_Status__c,
                Approval_Record__r.Approval_Type__r.Approval_Object__c,
                Approval_Record__r.Record_ID__c,
                Approval_Record__r.HEP_Market_Spend__c,
                Approval_Record__r.HEP_Promotion_Dating__c,
                Approval_Record__r.HEP_Promotion_SKU__c
                FROM HEP_Record_Approver__c
                Where Id =: recordApproverId
                AND Record_Status__c =:sACTIVE                
                LIMIT 1
            ];

            //------------------------------------//
            if (lstRecordApprovers != null && !lstRecordApprovers.isEmpty()) {
                //Fetch Record Approver Instance
                HEP_Record_Approver__c objRecordApproval = lstRecordApprovers.get(0);
                if (objRecordApproval.Approval_Record__r.Approval_Type__r != null && String.isNotBlank(objRecordApproval.Approval_Record__r.Approval_Type__r.Approval_Object__c)) {
                    String sObjectApiName = objRecordApproval.Approval_Record__r.Approval_Type__r.Approval_Object__c;

                    //Check If it was a Request for DATING Object                                      
                    if (sObjectApiName.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_PROMOTION_DATING'))) {
                        //Fetch the related Dating Record
                        List < HEP_Promotion_Dating__c > lstDatingRecord = [Select  Id,
                                                                                    Promotion__r.PromotionName__c,
                                                                                    Territory__r.Name
                                                                                    from
                                                                                    HEP_Promotion_Dating__c
                                                                                    where
                                                                                    Id =: objRecordApproval.Approval_Record__r.HEP_Promotion_Dating__c
                                                                                    AND Record_Status__c =:sACTIVE 
                                                                                    LIMIT 1
                                                                                ];

                        if (lstDatingRecord != null && !lstDatingRecord.isEmpty()) {
                            HEP_Promotion_Dating__c record = lstDatingRecord.get(0);

                            //Auto-Generate Subject Line text when user replies for Dating Records
                            sEmailSubject = HEP_Utility.getConstantValue('HEP_DATING_UNLOCK') + ' ' + record.Promotion__r.PromotionName__c + ' ';
                        }
                    } else if (sObjectApiName.equalsIgnorecase(HEP_Utility.getConstantValue('HEP_PROMOTION_SKU_OBJECT'))) {
                        //Fetch the Soend record Related to Approval
                        List < HEP_Promotion_SKU__c > lstPromoSKURecord = [Select   Id,
                                                                                SKU_Master__r.Name,
                                                                                Promotion_Catalog__r.Promotion__r.Name,
                                                                                Promotion_Catalog__r.Promotion__r.PromotionName__c                                                                              
                                                                                from
                                                                                HEP_Promotion_SKU__c
                                                                                where
                                                                                Id =: objRecordApproval.Approval_Record__r.HEP_Promotion_SKU__c
                                                                                AND Record_Status__c =:sACTIVE                                                                                
                                                                                LIMIT 1
                                                                            ];
                        if (lstPromoSKURecord != null && !lstPromoSKURecord.isEmpty()) {
                            HEP_Promotion_SKU__c record = lstPromoSKURecord.get(0);

                            //Auto-Generate Subject Line text when user replies for Spend Records
                            sEmailSubject = HEP_Utility.getConstantValue('HEP_SKU_UNLOCK') + ' ' + record.Promotion_Catalog__r.Promotion__r.PromotionName__c + ' ';
                            sRequestType = 'unlock Request';
                        }
                    }
                    
                    
                    sApproveText = HEP_Utility.getConstantValue('HEP_APPROVE_TEXT');
                    //Warning Text so that Subject Line is Not changed                   
                    noReply = HEP_Utility.getConstantValue('HEP_NOREPLY_MSG');
                    
                    String sEmailServiceName = HEP_Utility.getConstantValue('HEP_InboundEmail_Service');
                    sGlobalEmailAddress = HEP_Approval_Utility.fetchInboundEmailAddress(sEmailServiceName);
                }
            }

        } catch (Exception objException) {
            HEP_Error_Log.genericException('VF component Error for Approve/Reject template','DML Errors',objException,'HEP_Approval_Component_Controller','fetchSubjectDetails',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }
    }
}