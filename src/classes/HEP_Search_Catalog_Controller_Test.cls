/**
 * HEP_Search_Catalog_Controller_Test -- Test class for the HEP_Search_Catalog_Controller.
 * @author    Roshi Rai
 */
@isTest
public class HEP_Search_Catalog_Controller_Test {
    @testSetup
    static void createUsers() {
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User', 'rosh', 'roshirai@deloitte.com', 'Rai', 'ros', 'N', '', true);
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
    }
    static testMethod void extractAllCatalogData() {
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', false);
        insert objTerritory;
        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'rosh'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c = : u.Id];
        //Inserting Territory Record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id, objRole.Id, u.Id, true);
        //Map to see the access for territory
        set < id > setOfUserTerritory = HEP_Utility.fetchTerritorysForPage(HEP_Utility.getConstantValue('HEP_SEARCH_CATALOGS'));
        //Creating Catalog  Record
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TEST TAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        objCatalog.Record_Status__c = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        insert objCatalog;
        System.runAs(u) {
            test.startTest();
            //Calling extractAllCatalog method
            HEP_Search_Catalog_Controller.extractAllCatalog(HEP_Utility.getConstantValue('HEP_CATALOG_NAME'), HEP_Utility.getConstantValue('HEP_CONTAINS'), 'TAL');
            HEP_Search_Catalog_Controller.extractAllCatalog(HEP_Utility.getConstantValue('HEP_CATALOG_ID'), HEP_Utility.getConstantValue('HEP_CONTAINS'), '234');
            HEP_Search_Catalog_Controller.extractAllCatalog(HEP_Utility.getConstantValue('HEP_TYPE'), HEP_Utility.getConstantValue('HEP_CONTAINS'), 'Single');
            HEP_Search_Catalog_Controller.extractAllCatalog(HEP_Utility.getConstantValue('HEP_SEARCH_KEYWORD'), HEP_Utility.getConstantValue('HEP_CONTAINS'), '234');
            test.stopTest();
        }
    }
}