/**
 * HEP_SKUDetailsController --- Class for SKU Master page
 * @author  Nidhin VK
 */
public with sharing class HEP_SKUDetailsController {
    
    public map<String, String> mapPageAccess{get;set;}
    public Boolean bHasPageAccess{get;set;}
    public String sHeaderTabs{get;set;}
    public String sSKUId {get;set;}
    public String sBinaryData {get;set;}
    public String sFileName {get;set;}
    public String sDocumentDescription {get;set;}
    public String sFileType {get;set;}
    public String sCurrencyFormat {get;set;}
    public String sTerritoryId;
    public static String sSKU, sSKU_PAGE, sACTIVE, sCHANNEL, sFORMAT;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sSKU = HEP_Utility.getConstantValue('HEP_SKU');
        sSKU_PAGE = HEP_Utility.getConstantValue('HEP_SKU_PAGE');
        sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
        sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
    }
    
    /**
    * Class constructor to initialize the class variables
    * @return nothing
    * @author Sachin Agarwal
    */ 
    public HEP_SKUDetailsController (){
        bHasPageAccess = false;
        list<HEP_SKU_Master__c> lstSKUs = [SELECT Id, Territory__c, Territory__r.CurrencyCode__c 
                                           FROM HEP_SKU_Master__c
                                           WHERE Id = :ApexPages.CurrentPage().getParameters().get('skuId')];
                                           
        if(lstSKUs != NULL && !lstSKUs.isEmpty()){
            sTerritoryId = lstSKUs[0].Territory__c;
            
            // Getting the currency format applicable for the territory
            sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(lstSKUs[0].Territory__r.CurrencyCode__c);
                        
            // Calling the security framework to get the list of tabs to be shown on the UI 
            list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
            mapPageAccess = HEP_Security_Framework.getTerritoryAccess(sTerritoryId, HEP_Utility.getConstantValue('HEP_SKU_PAGE'), HEP_Utility.getConstantValue('HEP_SKU'), lstHeaderTabs);
            
            // If the user has read access to the page, then only load the page otherwise redirect the user to access denied page.          
            if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) && mapPageAccess.get(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) == 'TRUE'){
                bHasPageAccess = true;
            }
            
            sHeaderTabs = JSON.serialize(lstHeaderTabs);
        }       
    }
    
    
     /**
    * CheckPermissions --- To check if the current user have access to the page is in.
    * @author    Sachin Agarwal
    */
    public PageReference checkPermissions(){
        return new HEP_Security_Framework().redirectToAccessDenied(bHasPageAccess);
    }
    
    /**
    * Extracts all the data required on the header.
    * @param  sSKUId id of the catalog of which details are to be loaded
    * @return an instance of header class containing all the data required for the header portion
    * @author Nidhin VK
    */
    @RemoteAction 
    public static Header getHeaderDetails(String sSKUId, String sLocaleDateFormat){
        // Creating an instance of Header class
        Header objHeader = new Header();
        try{
            List<HEP_SKU_Master__c> lstSKU = new List<HEP_SKU_Master__c>();
            lstSKU = [SELECT 
                        Id, UI_SKU_Number__c, Barcode__c, Format__c,
                        Channel__c, SKU_Title__c, Territory__r.Flag_Country_Code__c
                    FROM 
                        HEP_SKU_Master__c
                    WHERE 
                        Id = :sSKUId
                    AND
                        Record_Status__c = :sACTIVE];
            if(lstSKU == NULL || lstSKU.isEmpty())
                return objHeader;
            List<String> lstLOVTypes = new List<String>{sCHANNEL, sFORMAT};
            Map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(
                                            lstLOVTypes, 
                                            HEP_Utility.getConstantValue('HEP_NAME_FIELD'), 
                                            HEP_Utility.getConstantValue('HEP_LOV_FIELD_VALUES'));
            HEP_SKU_Master__c objSKU = lstSKU[0];
            objHeader.sSKUId = objSKU.Id;
            objHeader.sSKUName = objSKU.SKU_Title__c;
            objHeader.sSKUNumber = objSKU.UI_SKU_Number__c;
            objHeader.sFormat = objSKU.Format__c;
            if(String.isNotBlank(objSKU.Channel__c))
            	objHeader.sChannel = mapLOVMapping.containsKey(sCHANNEL + '*' + objSKU.Channel__c)? mapLOVMapping.get(sCHANNEL + '*' + objSKU.Channel__c) : objSKU.Channel__c;
            if(String.isNotBlank(objSKU.Format__c))
            	objHeader.sFormat = mapLOVMapping.containsKey(sFORMAT + '*' + objSKU.Format__c)? mapLOVMapping.get(sFORMAT + '*' + objSKU.Format__c) : objSKU.Format__c;
            objHeader.sBarcode = objSKU.Barcode__c;
            if(objSKU.Territory__r != NULL && objSKU.Territory__r.Flag_Country_Code__c!=null)
                objHeader.sFlagCountryCode = objSKU.Territory__r.Flag_Country_Code__c.toLowerCase();
            return objHeader;
        } catch(Exception ex){
            HEP_Error_Log.genericException('Query Erros','Query Errors',ex,'HEP_SKUDetailsController','getHeaderDetails',null,null); 
            //throw error
            System.debug('Exception on Class : HEP_SKUDetailsController - getHeaderDetails, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            return NULL;
        }
    }
    
    /**
    * Its used to get the url of the latest SKU image uploaded by the user
    * @param sSKUNumber and sFlagCountryCode id of the catalog of which the image url is to be extracted
    * @return the url of the SKU image
    * @author Nidhin
    */
    @RemoteAction 
    public static String getImageUrl(String sSKUNumber, String sFlagCountryCode){
        try{
            if(sFlagCountryCode.equals('gb')){
                sFlagCountryCode = 'uk';
            }
            String sImageUrl = '';
            if(String.isNotBlank(sSKUNumber)
                && String.isNotBlank(sFlagCountryCode)){
                    
                //setting the params for Packshot request
                HEP_SKU_InputWrapper objPackshotReqWrapper = new HEP_SKU_InputWrapper();
                objPackshotReqWrapper.lstSkuObjectNumber = new List<String>{sSKUNumber};
                objPackshotReqWrapper.sTerritoryCode = sFlagCountryCode;
                //calling the external system for getting the packshot image
                HEP_InterfaceTxnResponse objAPIResponse = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(objPackshotReqWrapper),'','HEP_FoxRetail_SKUImage');
                HEP_SKU_ImageWrapper objResult = (HEP_SKU_ImageWrapper)JSON.deserialize(objAPIResponse.sResponse, HEP_SKU_ImageWrapper.class);
                
                if(objResult != NULL
                    && objResult.header != NULL
                    && String.isNotBlank(objResult.header.status)
                    && objResult.header.status.equals('ok')){
                    
                    if(objResult.all_items != NULL
                    && !objResult.all_items.isEmpty())
                    
                        sImageUrl = objResult.all_items[0].url;
                }
            }
            return (String.isBlank(sImageUrl)) ? HEP_Utility.getPromoDefaultImageUrl() : sImageUrl.replace('\\','');
        } catch(Exception ex){
            HEP_Error_Log.genericException('API Erros','API Errors',ex,'HEP_SKUDetailsController','getImageUrl',null,null); 
            //throw error
            System.debug('Exception on Class : HEP_SKUDetailsController - getImageUrl, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
            return HEP_Utility.getPromoDefaultImageUrl();
        }
    }
    
    /**
    * Header --- Wrapper class for holding the data for the entire header
    * @author    Nidhin VK
    */
    public class Header{
        public String sImageUrl, sVideoUrl, sSKUId, sSKUName, sSKUNumber; 
        public String sChannel, sFormat, sBarcode, sFlagCountryCode;
    }
}