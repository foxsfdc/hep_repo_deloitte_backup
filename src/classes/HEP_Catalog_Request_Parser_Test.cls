/**
 * HEP_Title_Details_Controller --- Class to get the data for title header
 * @author   -- Abhishek Mishra
 */
@isTest
public class HEP_Catalog_Request_Parser_Test{
    
    @testSetup
    static void createData() {

        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();

        //User
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Abhishek', 'AbhishekMishra9@deloitte.com', 'Abh', 'abhi', 'N','', true);
        
        //User Role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Products', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;

        //Global Territory
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', '', true);
        HEP_Territory__c objLocalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'JDE', true);
        HEP_Territory__c objAutoTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '183', 'JDE', true);
        objAutoTerritory.Auto_Catalog_Creation__c = true;
        update objAutoTerritory;

        HEP_Catalog__c objGlobalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Glocal Catalog', 'Single', null, string.valueof(objGlobalTerritory.id), 'Pending', 'Request', true);
        HEP_Catalog__c objLocalCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', 'Local Catalog', 'Single', string.valueof(objGlobalCatalog.id), string.valueof(objLocalTerritory.id), 'Pending', 'Request', true);

    }                                                   

    public static testMethod void test() {
        //Catalog Ids
        string sGlobalCatalogId;
        string sLocalCatalogId;

        User u = [SELECT Id, Name, DateFormat__c FROM User WHERE LastName = 'Abhishek'];
        
        list<HEP_Catalog__c> lstCatalogs = [SELECT Id, Catalog_Name__c FROM HEP_Catalog__c];
       

        for(HEP_Catalog__c objCatalog : lstCatalogs){
            if(objCatalog.Catalog_Name__c == 'Glocal Catalog'){
                sGlobalCatalogId = objCatalog.id;
            }
            if(objCatalog.Catalog_Name__c == 'Local Catalog'){
                sLocalCatalogId = objCatalog.id;
            }
        }

        system.debug('sGlobalCatalogId : ' + sGlobalCatalogId);
        system.debug('sLocalCatalogId : ' + sLocalCatalogId);

        string sGlobalCatalogResponseApproved = '{ "taskSuccess" : null, "taskId" : null, "status" : "Approved", "requestSalesforceId" : "", "rejectionReason" : null, "rejectionComment" : "", "originalCatalogNumber" : null, "Error" : null, "catalogRequestId" : "' + sGlobalCatalogId + '", "catalogNumber" : null }';
        string sGlobalCatalogResponseRejectedIncomplete = '{ "taskSuccess" : null, "taskId" : null, "status" : "Rejected", "requestSalesforceId" : "", "rejectionReason" : "Incomplete", "rejectionComment" : "The Reject Comments arent going.", "originalCatalogNumber" : null, "Error" : null, "catalogRequestId" : "' + sGlobalCatalogId + '", "catalogNumber" : null }';
        string sGlobalCatalogResponseRejected = '{ "taskSuccess" : null, "taskId" : null, "status" : "Rejected", "requestSalesforceId" : "", "rejectionReason" : "Duplicate", "rejectionComment" : "The Reject Comments arent going.", "originalCatalogNumber" : null, "Error" : null, "catalogRequestId" : "' + sGlobalCatalogId + '", "catalogNumber" : null }';
        string sLocalCatalogResponseApproved = '{ "taskSuccess" : null, "taskId" : null, "status" : "Approved", "requestSalesforceId" : "", "rejectionReason" : null, "rejectionComment" : "", "originalCatalogNumber" : null, "Error" : null, "catalogRequestId" : "' + sLocalCatalogId + '", "catalogNumber" : null }';

        System.runAs(u) {
            Test.startTest();
            HEP_Catalog_Request_Parser.parseNewCatalogResponse(sGlobalCatalogResponseApproved);
            HEP_Catalog_Request_Parser.parseNewCatalogResponse(sGlobalCatalogResponseRejectedIncomplete);
            HEP_Catalog_Request_Parser.parseNewCatalogResponse(sGlobalCatalogResponseRejected);
            HEP_Catalog_Request_Parser.parseNewCatalogResponse(sLocalCatalogResponseApproved);
            HEP_Catalog_Request_Parser.CatalogResponseFoxipediaWrapper objCatalogResponse = new HEP_Catalog_Request_Parser.CatalogResponseFoxipediaWrapper('','','','','','','','','','');
            Test.stoptest();
        }
    }
}