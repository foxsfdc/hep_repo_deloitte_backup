/**
* HEP_SKU_Promotion_Controller --- Class to get the data for SKU Promotions
* @author  Ayan Sarkar
*/
public class HEP_SKU_Promotion_Controller {


    /**
    * SKU_Details_Page_Data --- Wrapper class for holding the data for the entire page
    * @author    Sachin Agarwal
    */
    public class SKU_Promotion_Wrapper{
        public String sPromotionName;
        public String sPromoId;
        public String sRegion;
        public String sCustomer;
        public String sLOB;
        public Date dtFAD;
        public String sMarketingDA;
        public String sStatus;
        public String sPromotionId;
    }


    /**
     * Helps to get Data to be shown on screen on Page Load.
     * @return List of Picklist Wrapper 
     */
    @RemoteAction
    public static List<SKU_Promotion_Wrapper> fetchPromotionSKUPageData(String sSKUMasterId){
        System.debug('1. sSKUMasterId ---> ' + sSKUMasterId);
        List<SKU_Promotion_Wrapper> lstPromotionWrapper = new List<SKU_Promotion_Wrapper>();
        try{
            //Step - 1 : Find list of Promotion Catalog tagged to current SKU from Promotion SKU.
            List<HEP_Promotion_SKU__c> lstPromotionSKU = [SELECT Id, Promotion_Catalog__c , Catalog_Territory__c
                                                          FROM HEP_Promotion_SKU__c
                                                          WHERE SKU_Master__c =: sSKUMasterId
                                                          AND Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE')];

            System.debug('2. lstPromotionSKU ---> ' + lstPromotionSKU);

            Set<Id> setPromotionCatalogIds = new Set<Id>();
            Set<Id> setTerritoryIds = new Set<Id>();
            for(HEP_Promotion_SKU__c objPromtionSKU : lstPromotionSKU){
                setPromotionCatalogIds.add(objPromtionSKU.Promotion_Catalog__c);
            }

            //Get all the Territory that the logged in User has access to -- Security Code:
            String sTerritoryQueryWhereClause = ' WHERE User__c = \''+UserInfo.getUserId() + '\' AND Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\'';
            String sTerritoryQuery = HEP_Utility.buildQueryAllString('HEP_User_Role__c','SELECT Territory__r.Name, User__r.HEP_Line_Of_Business__c,',sTerritoryQueryWhereClause);
            System.debug('sTerritoryQuery' + sTerritoryQuery);
            LIST<HEP_User_Role__c> lstTerritoriesRecs = (LIST<HEP_User_Role__c>)Database.query(sTerritoryQuery);
            
            for(HEP_User_Role__c objTerrAccess : lstTerritoriesRecs){
                setTerritoryIds.add(objTerrAccess.Territory__c);
            }

            //Step - 2 : Fetch list of Promotions from the Promotion Catalog Junction Object.
            List<HEP_Promotion_Catalog__c> lstPromotions = [SELECT Id, Promotion__c, Promotion__r.PromotionName__c,
                                                            Promotion__r.Name, Promotion__r.LocalPromotionCode__c, 
                                                            Promotion__r.Territory__c, Promotion__r.Territory__r.Name,
                                                            Promotion__r.LineOfBusiness__r.Type__c, Promotion__r.FirstAvailableDate__c,
                                                            Promotion__r.FirstAvailableDateFormula__c,
                                                            Promotion__r.MinPromotionDt_Date__c,
                                                            Promotion__r.Customer__r.Name ,Promotion__r.Customer__r.Customer_Status__c , Promotion__r.LineOfBusiness__r.Name,
                                                            Promotion__r.Domestic_Marketing_Manager__r.Name
                                                            FROM HEP_Promotion_Catalog__c
                                                            WHERE Id in: setPromotionCatalogIds
                                                            AND Promotion__r.Territory__c =: setTerritoryIds];

            System.debug('3. lstPromotions ---> ' + lstPromotions);

            for(HEP_Promotion_Catalog__c objPromotion : lstPromotions){
                SKU_Promotion_Wrapper objPromotionWrap = new SKU_Promotion_Wrapper();
                objPromotionWrap.sPromotionName = objPromotion.Promotion__r.PromotionName__c;
                objPromotionWrap.sPromoId = objPromotion.Promotion__r.LocalPromotionCode__c;
                objPromotionWrap.sRegion = objPromotion.Promotion__r.Territory__r.Name;
                objPromotionWrap.sCustomer = objPromotion.Promotion__r.Customer__r.Name;
                objPromotionWrap.sLOB = objPromotion.Promotion__r.LineOfBusiness__r.Name;
                objPromotionWrap.dtFAD = objPromotion.Promotion__r.MinPromotionDt_Date__c;
                objPromotionWrap.sMarketingDA = objPromotion.Promotion__r.Domestic_Marketing_Manager__r.Name;
                objPromotionWrap.sStatus = objPromotion.Promotion__r.Customer__r.Customer_Status__c;
                objPromotionWrap.sPromotionId = objPromotion.Promotion__c;

                lstPromotionWrapper.add(objPromotionWrap);
            }

            System.debug('4. lstPromotionWrapper ---> ' + lstPromotionWrapper);
        }catch(Exception e){
         //Code to log error..
            HEP_Error_Log.genericException('Error occured for fetching the records',
                                             'VF Controller',
                                              e,
                                             'HEP_SKU_Promotion_Controller',
                                             'fetchPromotionSKUPageData',
                                             null,
                                             '');

            throw e;
        }
        return lstPromotionWrapper;
    }
}