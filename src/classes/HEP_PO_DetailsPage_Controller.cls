/**
*HEP_PO_DetailsPage_Controller - to process the PO's to be displayed on PO Details Page
*@author : Ashutosh Arora
*/
public class HEP_PO_DetailsPage_Controller {
        
       public String sCurrencyFormat {get; set;}
       public static String sTerritoryId;
        /**
        *HEP_PO_DetailsPage_Controller - Constructor to initialize values 
        *@author : Ashutosh Arora
        */  
      public HEP_PO_DetailsPage_Controller() {
        // Extracting all the matching promotions
        list < HEP_Promotion__c > lstPromotion = new list < HEP_Promotion__c > ();
        //bToastMessage=true;
        lstPromotion = [SELECT id,createdDate,lastModifiedDate, Territory__c, Promotion_Type__c, Territory__r.CurrencyCode__c
                        FROM   HEP_Promotion__c
                        WHERE   Record_Status__c=:HEP_Constants__c.getValues('HEP_RECORD_ACTIVE').Value__c AND id = : apexpages.currentpage().getparameters().get('promoId')];
        //Currency Format
        if (lstPromotion != null && lstPromotion.size() > 0 && lstPromotion[0].Territory__r!=null) {
            sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(lstPromotion[0].Territory__r.CurrencyCode__c);
            sTerritoryId = lstPromotion[0].Territory__c;
        }
        
        
        
    }
    
    public PageReference checkPermissions(){
        map<String,String> mapCustomPermissions = new map<String,String>();
        mapCustomPermissions = HEP_Utility.fetchCustomPermissions(sTerritoryId, HEP_Utility.getConstantValue('HEP_PO_Page'));
        if(mapCustomPermissions.get('READ').equalsIgnoreCase('FALSE')){
            return HEP_Utility.redirectToErrorPage();
        }
        else{
            return null;
        }
    }


    /**
    *getPODetails - To process the PO data to be displayed on PO Details Page
    *@return PODetails (PODetails Wrapper with PO and respective Line Data)
    *@author Ashutosh Arora
    */
    @remoteAction
    public static PODetails getPODetails(String sPromotionId, String sPONumber, string sDepartmentId) {
        PODetails objPoData = new PODetails();
        String sDepartmentName;
        String sNameDepartment;
        List<HEP_GL_Account__c> lstGLAccountId = new List<HEP_GL_Account__c>();
        if(sDepartmentId != null){
        lstGLAccountId = [SELECT Id, Account_Department__c
                            FROM HEP_GL_Account__c
                            WHERE Id =: sDepartmentId];
        sDepartmentName = lstGLAccountId[0].Account_Department__c;
        }
        String sCurrencyCode;
        HEP_InvoicePurchaseOrderData objPurchaseOData = new HEP_InvoicePurchaseOrderData();
        String sIdPromotion = sPromotionId.trim();
        //To get Currency code from Promotion Territory
        List < HEP_Promotion__c > lstCurrencies = [SELECT Territory__r.CurrencyCode__c FROM HEP_Promotion__c where id =: sIdPromotion];
        if(lstCurrencies != null && lstCurrencies.size() > 0 && lstCurrencies[0].Territory__r != NULL)
        {
            sCurrencyCode = lstCurrencies[0].Territory__r.CurrencyCode__c;
        }
        String sResponseonPromotion;
        if (sIdPromotion != null) {
            sResponseonPromotion = HEP_Integration_Util.retriveDatafromContent(sIdPromotion, HEP_Constants__c.getValues('HEP_PO_INVOICE_RESPONSE').Value__c);
        }
        if (sResponseonPromotion != null) {
            //Serialize the String reponse to HEP_InvoicePurchaseOrderData format
            objPurchaseOData = (HEP_InvoicePurchaseOrderData) JSON.deserialize(sResponseonPromotion, HEP_InvoicePurchaseOrderData.class);
        } 
        if (sDepartmentName != null && objPurchaseOData != null) {
            for (HEP_InvoicePurchaseOrderData.Department objDepart: objPurchaseOData.lstDepartments) {
                if (objDepart.sDepartmentName.equals(sDepartmentName)) {
                    for (HEP_InvoicePurchaseOrderData.ParentNode poData: objDepart.lstPurchaseOrders ) {
                        if (poData.sRequestNumber == sPONumber) {
                            objPoData.sVendorName = poData.sVendorName;
                            objPoData.sPONumber = poData.sRequestNumber;
                            objPoData.sRequestNumber = poData.sRequestNumber;
                            objPoData.sPOStatus = poData.sStatus;
                            objPoData.dtIssuedDate = poData.dtIssueDate;
                            //objPoData.dAmount = poData.dAmount;
                            objPoData.sCurrency = sCurrencyCode; //based on currency code for the territory associated with Promotion
                            objPoData.sRequestor = poData.sRequestor;
                            //objPoData.mapCustomPermissions = HEP_Utility.fetchCustomPermissions(sTerritoryId, HEP_Constants__c.getValues('HEP_PO_Page').Value__c);
                            //System.debug('objPoData.mapCustomPermissions : ' + objPoData.mapCustomPermissions);
                            for (HEP_InvoicePurchaseOrderData.Line objLineWrapper: poData.lstLines) {
                                LineData objLine = new LineData();
                                objLine.sLineNumber = objLineWrapper.sLineNumber;
                                objLine.sGLAccount = objLineWrapper.sGLAccount;
                                objLine.sFormat = objLineWrapper.sFormat;
                                objLine.sPOLineDesription = objLineWrapper.sLineDescription;
                                objLine.sLineStatus = objLineWrapper.sLineStatus;
                                objLine.dItemAmount = objLineWrapper.dAmount;
                                objLine.dActuals = objLineWrapper.dActuals;
                                objLine.dOpenAmount = objLineWrapper.dOpenAmount;
                                objLine.sCountry = objLineWrapper.sCountry;
                                objPoData.lstLines.add(objLine);
                                objPoData.dAmount+=objLine.dActuals;
                            }
                        }

                    }
                }
            }
        } 
        system.debug('objPoData : ' +objPoData);
        return objPoData;
    }
    /**
    *InvoiceDetails - Wrapper for the top level header values
    *@author : Ashutosh Arora
    */
    Public Class PODetails {
        String sPONumber;
        String sVendorName;
        String sPOStatus;
        String sRequestNumber;
        Date dtIssuedDate;
        Decimal dAmount;
        String sCurrency;
        String sComments;
        String sRequestor;
        //public Map < String, String > mapCustomPermissions;
        list < LineData > lstLines;
        public PODetails() {
            //mapCustomPermissions = new map<String,String>();
            lstLines = new list < LineData > ();
            dAmount =0;
        }

    }
    /**
    *LineData - Wrapper for Line data relating to a Invoice
    *@author : Ashutosh Arora
    */
    Public class LineData {
        String sLineNumber;
        String sGLAccount;
        String sFormat;
        String sPOLineDesription;
        String sLineStatus;
        String sPromotionId;
        String sActuals;
        String sOpenAmount;
        String sCountry;
        Decimal dItemAmount;
        Decimal dActuals;
        Decimal dOpenAmount;
    }

}