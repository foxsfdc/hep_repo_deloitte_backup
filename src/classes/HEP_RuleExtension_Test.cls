/**
 * HEP_RuleExtension_Test --- Test Class for HEP_RuleExtension
 * @author  Nidhin V K
 */
@isTest
private class HEP_RuleExtension_Test {
    
    @testSetup static void setupData() {
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createRule(TRUE);
    }
    
    @isTest static void testRuleMethods() {
        
        Test.startTest();
        PageReference pageRef = Page.HEP_CreateRule;
        Test.setCurrentPage(pageRef);
        HEP_Rule__c objRule = new HEP_Rule__c();
        ApexPages.StandardController objStdCntrl = new ApexPages.standardController(objRule);
        objRule = [SELECT 
                        Id, Name, HEP_Target_SObject_API_Name__c,
                        HEP_Priority__c, HEP_IsActive__c
                    FROM 
                        HEP_Rule__c 
                    LIMIT 1];
        System.debug('objRule>>' + objRule);
        objRule.HEP_IsActive__c = true;
        objStdCntrl = new ApexPages.standardController(objRule);
        objRule.HEP_IsActive__c = false;
        objStdCntrl = new ApexPages.standardController(objRule);
        
        HEP_RuleExtension objExtension = new HEP_RuleExtension(objStdCntrl);
        System.assert(objExtension.lstTargetObjects.size() > 0);
        
        PageReference pageRef1 = objExtension.cancel();
        System.assertEquals('/' + objRule.Id, pageRef1.getUrl());
        
        objExtension.toggleActivate();
        //System.assertEquals(objRule.HEP_IsActive__c, true);
        objRule.HEP_IsActive__c = true;
        HEP_Rule_Criterion__c objCriterion = HEP_Test_Data_Setup_Utility.createRuleCriteria(objRule.Id, FALSE);
        objCriterion.HEP_Serial_Number__c = 1;
        insert objCriterion;
        objExtension.toggleActivate();
        objRule.HEP_Criteria_Logic__c = '(1)';
        update objRule;
        objExtension.toggleActivate();
        objRule.HEP_Criteria_Logic__c = '(1 AND )2)';
        update objRule;
        objExtension.toggleActivate();
        
        PageReference pageRef2 = objExtension.deleteRule();
        System.assertEquals('/apex/HEP_ViewRuleList', pageRef2.getUrl());
        objExtension.deleteRule();
        Test.stopTest();
    }
}