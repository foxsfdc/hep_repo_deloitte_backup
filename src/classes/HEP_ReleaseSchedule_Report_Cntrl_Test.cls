@isTest
public class HEP_ReleaseSchedule_Report_Cntrl_Test {

	@testSetup
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    User u2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara1', 'arjnarayanan1@deloitte.com','arjnarayanan1','arj1','N1','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    
	    EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature','FEATR', true);
	    EDM_GLOBAL_TITLE__c objTitleEDM = HEP_Test_Data_Setup_Utility.createTitleEDM('Test', '12345', 'PUBLC', objProductType.id, true);

	    GTS_Territory__c objGTSTerritory = new GTS_Territory__c(Name__c = 'Test',Name = 'Test',GTS2_ERM_Territory_Id__c = '33');
	    insert objGTSTerritory;

		GTS_Title__c objGTSTitle = new GTS_Title__c(Name = 'Test',Foxipedia_ID__c = '1234', Title__c = 'Test',WPR__c= '12345');
		objGTSTitle.US_Release_Date__c =system.today().addDays(-1);
		insert objGTSTitle;

		GTS_Release__c objGTSRelease = new GTS_Release__c(Name = 'Test',Title__c = objGTSTitle.Id, Release_Date__c = system.today(), Territory__c = objGTSTerritory.id);
		insert objGTSRelease;

		HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
		HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
		objNationalTerritory.Parent_Territory__c = objNationalTerritory.id;
		insert objNationalTerritory;
		HEP_Territory__c objNationalTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','EMEA','Licensee',objNationalTerritory.Id, objNationalTerritory.Id,'EUR','AT','33',null, false);
		objNationalTerritoryRegion.Parent_Territory__c = objNationalTerritory.id;
		insert objNationalTerritoryRegion;
		
		HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
		HEP_Line_Of_Business__c objLOBTV = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks TV','TV',true);
  	}

  	static testMethod void testData(){

  		User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
  		User u2 = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara1'];
    	HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id];

    	HEP_ReleaseSchedule_Report_Controller.MultiPicklistWrapper objMultiPicklistWrapper = new HEP_ReleaseSchedule_Report_Controller.MultiPicklistWrapper();

    	LIST<HEP_Utility.MultiPicklistWrapper> lstTerritoriesData = new LIST<HEP_Utility.MultiPicklistWrapper>();
    	LIST<HEP_Utility.MultiPicklistWrapper> lstLOBData = new LIST<HEP_Utility.MultiPicklistWrapper>();

		//objMultiPicklistWrapper.lstTerritories = lstTerritoriesData;
		//objMultiPicklistWrapper.lstLOB = lstLOBData;

		//objMultiPicklistWrapper.lstTerritories.bSelected = true;
		//objMultiPicklistWrapper.lstLOB.bSelected = true;

    	HEP_Territory__c objTerritory = [SELECT id,Name from HEP_Territory__c WHERE Name = 'Global'];//HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
		HEP_Territory__c objNationalTerritory = [SELECT id,Name from HEP_Territory__c WHERE Name = 'Germany'];//HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);
		HEP_Territory__c objNationalTerritoryRegion = [SELECT id,Name from HEP_Territory__c WHERE Name = 'Austria'];//HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','EMEA','Licensee',objNationalTerritory.Id, objNationalTerritory.Id,'EUR','AT','33',null, true);

		HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
		HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);
		HEP_Promotions_DatingMatrix__c objDatingMatrixNationalRegion = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritoryRegion.Id, true);
		HEP_Promotions_DatingMatrix__c objDatingMatrixNationalVOD = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','VOD Release Date', objNationalTerritory.Id, objNationalTerritoryRegion.Id, true);
		HEP_Promotions_DatingMatrix__c objDatingMatrixNationalRetail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','Physical Release Date (Retail)', objNationalTerritory.Id, objNationalTerritoryRegion.Id, true);
		HEP_Promotions_DatingMatrix__c objDatingMatrixNationalRental = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','Physical Release Date (Rental)', objNationalTerritory.Id, objNationalTerritoryRegion.Id, true);
		

		HEP_Line_Of_Business__c objLOB = [SELECT id,Name from HEP_Line_Of_Business__c WHERE Name = 'New Release'];//HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
		HEP_Line_Of_Business__c objLOBTV = [SELECT id,Name from HEP_Line_Of_Business__c WHERE Name = 'Dreamworks TV'];//HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks TV','TV',true);

		String sStartDate = String.valueOf(System.today().addDays(-10));
		String sEndDate = String.valueOf(System.today().addYears(1));

		EDM_GLOBAL_TITLE__c objTitleEDM = [SELECT Id From EDM_GLOBAL_TITLE__c LIMIT 1];

		HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('Test', 'Test', 'Single', null, objNationalTerritory.id, 'Approved', 'Master', false);
		objCatalog.Title_EDM__c = objTitleEDM.id;
		insert objCatalog;

		HEP_Production_Company__c objProductionCompany = new HEP_Production_Company__c(Title__c = objTitleEDM.id,Production_Company_Description__c = 'FOX',Production_Company_Code__c = 'Test');
		insert objProductionCompany;
		HEP_Production_Company__c objProductionCompany2 = new HEP_Production_Company__c(Title__c = objTitleEDM.id,Production_Company_Description__c = 'FOX Studio',Production_Company_Code__c = 'Test1');
		insert objProductionCompany2;


		//EDM_GLOBAL_TITLE__c objTitleEDM = [SELECT Id FROM EDM_GLOBAL_TITLE__c LIMIT 1];

		GTS_Title__c objGTSTitle = [SELECT Id FROM GTS_Title__c WHERE Name = 'Test' LIMIT 1];

		GTS_Release__c objGTSRelease = [SELECT Id FROM GTS_Release__c LIMIT 1];

		
		lstTerritoriesData.add(new HEP_Utility.MultiPicklistWrapper(objTerritory.Name, objTerritory.id, true));
		lstTerritoriesData.add(new HEP_Utility.MultiPicklistWrapper(objNationalTerritory.Name, objNationalTerritory.id, true));
		lstTerritoriesData.add(new HEP_Utility.MultiPicklistWrapper(objNationalTerritoryRegion.Name, objNationalTerritoryRegion.id, true));
		lstLOBData.add(new HEP_Utility.MultiPicklistWrapper(objLOB.Name, objLOB.id, true));


		objMultiPicklistWrapper.lstTerritories = lstTerritoriesData.clone();
		objMultiPicklistWrapper.lstLOB = lstLOBData.clone();

		System.runAs(u){

			Test.startTest();
			system.debug('-------->'+objTitleEDM);
			HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,false);
			objPromotion.Domestic_Marketing_Manager__c = u.id;
			objPromotion.Title__c = objTitleEDM.Id;
			insert objPromotion;
			HEP_Promotion__c objPromotionTV = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOBTV.Id, null,null,false);
			objPromotionTV.Domestic_Marketing_Manager__c = u2.id;
			objPromotionTV.Title__c =objTitleEDM.id;
			insert objPromotionTV;
			HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', false);
			objNationalPromotion.Title__c = objTitleEDM.id;
			objNationalPromotion.Domestic_Marketing_Manager__c = u2.id;
			insert objNationalPromotion;
			HEP_Promotion__c objNationalPromotion2 = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 2', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', false);
			objNationalPromotion2.Title__c = objTitleEDM.id;
			objNationalPromotion2.Domestic_Marketing_Manager__c = u.id;
			insert objNationalPromotion2;
			HEP_Promotion__c objNationalPromotion3 = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion 3', 'National', objPromotionTV.Id, objNationalTerritory.Id, objLOBTV.Id,'','', true);

			HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordTV = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotionTV.Id, objDatingMatrixNational.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion3.Id, objDatingMatrixNational.Id, false);
			objDatingRecordNational3.Date__c = system.today();
			insert objDatingRecordNational3;
			HEP_Promotion_Dating__c objDatingRecordNationalRegion = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNationalRegionVOD = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion.Id, objDatingMatrixNationalVOD.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRETAIL = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion.Id, objDatingMatrixNationalRetail.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRENTAL = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion.Id, objDatingMatrixNationalRental.Id, true);

			//HEP_Promotion_Dating__c objDatingRecord2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
			HEP_Promotion_Dating__c objDatingRecordNational2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion2.Id, objDatingMatrixNational.Id, false);
			objDatingRecordNational2.Date__c = system.today().addDays(-1);
			objDatingRecordNational2.HEP_Catalog__c = objCatalog.id;
			insert objDatingRecordNational2;
			HEP_Promotion_Dating__c objDatingRecordNationalRegion2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion2.Id, objDatingMatrixNational.Id, false);
			objDatingRecordNationalRegion2.Date__c = system.today().addDays(-1);
			objDatingRecordNationalRegion2.HEP_Catalog__c = objCatalog.id;
			insert objDatingRecordNationalRegion2;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionVOD2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion2.Id, objDatingMatrixNationalVOD.Id, false);
			objDatingRecordNationalRegionVOD2.Date__c = system.today().addDays(-1);
			objDatingRecordNationalRegionVOD2.HEP_Catalog__c = objCatalog.id;
			insert objDatingRecordNationalRegionVOD2;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRETAIL2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion2.Id, objDatingMatrixNationalRetail.Id, false);
			objDatingRecordNationalRegionRETAIL2.Date__c = system.today().addDays(-1);
			objDatingRecordNationalRegionRETAIL2.HEP_Catalog__c = objCatalog.id;
			insert objDatingRecordNationalRegionRETAIL2;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRENTAL2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion2.Id, objDatingMatrixNationalRental.Id, false);
			objDatingRecordNationalRegionRENTAL2.Date__c = system.today().addDays(-1);
			objDatingRecordNationalRegionRENTAL2.HEP_Catalog__c = objCatalog.id;
			insert objDatingRecordNationalRegionRENTAL2;

			//HEP_Promotion_Dating__c objDatingRecordNational4 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion3.Id, objDatingMatrixNational.Id, false);
			//objDatingRecordNational4.Date__c = system.today().addDays(2);
			//insert objDatingRecordNational4;
			HEP_Promotion_Dating__c objDatingRecordNationalRegion3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion3.Id, objDatingMatrixNational.Id, false);
			objDatingRecordNationalRegion3.Date__c = system.today().addDays(-2);
			insert objDatingRecordNationalRegion3;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionVOD3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion3.Id, objDatingMatrixNationalVOD.Id, false);
			objDatingRecordNationalRegionVOD3.Date__c = system.today().addDays(2);
			insert objDatingRecordNationalRegionVOD3;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRETAIL3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion3.Id, objDatingMatrixNationalRetail.Id, false);
			objDatingRecordNationalRegionRETAIL3.Date__c = system.today().addDays(-2);
			insert objDatingRecordNationalRegionRETAIL3;
			HEP_Promotion_Dating__c objDatingRecordNationalRegionRENTAL3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritoryRegion.Id, objNationalPromotion3.Id, objDatingMatrixNationalRental.Id, false);
			objDatingRecordNationalRegionRENTAL3.Date__c = system.today().addDays(2);
			insert objDatingRecordNationalRegionRENTAL3; 


			HEP_ReleaseSchedule_Report_Controller.getPicklistData();
			HEP_ReleaseSchedule_Report_Controller.getPageData(sStartDate,sEndDate, objMultiPicklistWrapper);
			Test.stopTest();

		}
  	}

}