/**
 * HEP_Title_Promotions_Controller --  To fetch the Promotions in Title Master Promotions Subtab (Excluding Customer Promotions)
 * @author  :  Nishit Kedia    
 */
public with sharing class HEP_Title_Promotions_Controller {
    
    public static String sActive;
    static {
        sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * TitlePromotionsWrapper -- Final Outer Wrapper of TitlePromotions
    * @author    Nishit Kedia
    */
    public class TitlePromotionsWrapper{
        List<MasterTitlePromotionsRowWrapper> lstTitleRowData;
        
        public TitlePromotionsWrapper(){
            lstTitleRowData = new List<MasterTitlePromotionsRowWrapper>();
        }
        
        public TitlePromotionsWrapper(List<MasterTitlePromotionsRowWrapper> lstTitleRowData){
            this.lstTitleRowData= lstTitleRowData;
        }
    }
    
    /**
    * MasterTitlePromotionsRowWrapper -- Inner wrapper for TitlePromotions
    * @author    Nishit Kedia
    */
    public class MasterTitlePromotionsRowWrapper{
        
        String sPromotionId,sPromotionName, sRegion, sCustomer, sLineOfBusiness;
        Date dFirstAvailableDate;
        String sLocalPromotionCode,sMarketingManager,sStatus;
        
        public MasterTitlePromotionsRowWrapper(){
            
        }
        
        /*public MasterTitlePromotionsRowWrapper(String sPromotionId, String sPromotionName,String sRegion, String sCustomer, String sLineOfBusiness, Date dFirstAvailableDate,String sLocalPromotionCode,String sMarketingManager,String sStatus){
            
            this.sPromotionId=sPromotionId;
            this.sPromotionName=sPromotionName;
            this.sRegion=sRegion;
            this.sCustomer=sCustomer;
            this.sLineOfBusiness=sLineOfBusiness;
            this.dFirstAvailableDate=dFirstAvailableDate;
            this.sLocalPromotionCode=sLocalPromotionCode;
            this.sMarketingManager=sMarketingManager;
            this.sStatus=sStatus;
        }*/
        
    }
    
     /**
     * loadData -- Remote Action Method to fetch the data on Page Load
     * @param   Title Id passed as String
     * @return  Json data for TitlePromotionsWrapper 
     * @author  Nishit Kedia    
     */ 
    @RemoteAction
    public static TitlePromotionsWrapper loadData(String sTitleId){
        
        String sQuery = 'SELECT Id, PromotionName__c, Promotion_Type__c, Territory__r.Name, customer__c, LineOfBusiness__r.Type__c,MinPromotionDt_Date__c, FirstAvailableDate__c,LocalPromotionCode__c,Domestic_Marketing_Manager__r.Name, Status__c FROM HEP_Promotion__c ';
        
        //Users can only view Promotions for territories they have been given access for; meaning a territory filter must be applied
        //List<HEP_User_Role__c> lstTerritoryRolesAcccessible = HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId());
        Set<Id> setTerritorysAccessible = new Set<Id>();
        String sCurrentPageName = '';
        
        if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_TITLE_DETAILS'))){
            sCurrentPageName = HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_TITLE_DETAILS');
        }
        setTerritorysAccessible = HEP_Utility.fetchTerritorysForPage(sCurrentPageName);
        
        System.debug('setTerritorysAccessible--------->' + setTerritorysAccessible);           
                
        if(String.isNotBlank(sTitleId)){
            // Controller must perform the various logical queries, then merge the results, removing duplicate promotions: 
            Set <Id> setPromoIds = fetchAllActivePromotionIds(sTitleId);
            System.debug('Indirect promo Ids ----> ' + setPromoIds);       
            
            //Users can only view Promotions for territories they have been given access for; meaning a territory filter must be applied
            //Utility Code to fetch Territory Access Goes Here
            sQuery+=' Where   Record_Status__c=:sActive AND ';
            
            if(setPromoIds != null && !setPromoIds.isEmpty()){
                    sQuery+='((Territory__c In: setTerritorysAccessible'; 
            }
            else
                    sQuery+='(Territory__c In: setTerritorysAccessible';
            //◾Promotions have a Lookup F/K to Title
            sQuery+=' And Title__c = :sTitleId )';
            if(setPromoIds != null && !setPromoIds.isEmpty()){
                        
                //Fetch All Global Promotions
                sQuery+=' Or ( Id IN:setPromoIds ';
                
                //Fetch Child Promotions As well
                sQuery+=' Or Global_Promotion__c IN:setPromoIds ))';
                
            }
        }
        
        //Promotion sorting should by as follows:sort by FAD (desc), Territory (asc), Promotion Name (asc)
        sQuery+=' order by Territory__r.Territory_Custom_Sort__c Asc, FirstAvailableDate__c desc, Territory__r.Name asc, PromotionName__c asc ';        
        System.debug('Dynamic Query---->'+ sQuery);
        
        List<HEP_Promotion__c> lstHEPPromotionTitle = new List<HEP_Promotion__c>();
        system.debug('----------->Query'+sQuery);
        lstHEPPromotionTitle = Database.query(sQuery);
        
        MasterTitlePromotionsRowWrapper titlePromoRow;
        
        List<MasterTitlePromotionsRowWrapper> lstGlobalPromos = new List<MasterTitlePromotionsRowWrapper>();
        List<MasterTitlePromotionsRowWrapper> lstOtherPromos = new List<MasterTitlePromotionsRowWrapper>();
        List<MasterTitlePromotionsRowWrapper> lstTitleRowData = new List<MasterTitlePromotionsRowWrapper>();
        if(lstHEPPromotionTitle != null){
            for(HEP_Promotion__c promoTitle : lstHEPPromotionTitle){
                
                titlePromoRow = new MasterTitlePromotionsRowWrapper();
                titlePromoRow.sPromotionId = promoTitle.Id;
                
                if(String.isNotBlank(promoTitle.PromotionName__c ))
                    titlePromoRow.sPromotionName = promoTitle.PromotionName__c;
                
                if(String.isNotBlank(promoTitle.Territory__r.Name ))
                    titlePromoRow.sRegion = promoTitle.Territory__r.Name;
                
                if(String.isNotBlank(promoTitle.LineOfBusiness__c ))
                    titlePromoRow.sLineOfBusiness = promoTitle.LineOfBusiness__r.Type__c;
                    
                if(promoTitle.FirstAvailableDate__c != null)
                    titlePromoRow.dFirstAvailableDate = promoTitle.FirstAvailableDate__c;
                else
                    titlePromoRow.dFirstAvailableDate = promoTitle.MinPromotionDt_Date__c;
                
                if(String.isNotBlank(promoTitle.LocalPromotionCode__c ))
                    titlePromoRow.sLocalPromotionCode = promoTitle.LocalPromotionCode__c;    
                
                if(String.isNotBlank(promoTitle.Domestic_Marketing_Manager__c ) )
                    titlePromoRow.sMarketingManager = promoTitle.Domestic_Marketing_Manager__r.Name; 
                    
                if(String.isNotBlank(promoTitle.Status__c )){
                    if(HEP_Constants__c.getValues('PROMOTION_TYPE_CUSTOMER').Value__c.equals(promoTitle.Promotion_Type__c))
                        titlePromoRow.sStatus = promoTitle.Status__c; 
                }
                if(HEP_Constants__c.getValues('HEP_PROMOTION_TYPE_GLOBAL').Value__c.equals(promoTitle.Territory__r.Name))
                lstGLobalPromos.add(titlePromoRow);
                else 
                lstOtherPromos.add(titlePromoRow);   
            }
            if(lstGLobalPromos!=null && lstGLobalPromos.size()>0)
                lstTitleRowData.addAll(lstGLobalPromos);
            if(lstOtherPromos!=null && lstOtherPromos.size()>0)
                lstTitleRowData.addAll(lstOtherPromos);
            TitlePromotionsWrapper TitlePromoData = new TitlePromotionsWrapper(lstTitleRowData);
            return TitlePromoData;
        }
        return new TitlePromotionsWrapper();
    }
    
    
    /**
     * fetchAllActivePromotionIds --  To Apply Buisness Logics when fetching Elligible Promotion Ids
     * @param   Title Id passed as String
     * @return  Set Of Promotions Ids
     * @author  Nishit Kedia    
    */
    public static Set<Id> fetchAllActivePromotionIds(String sTitleId){
        
        //This Set will contain All Promo Ids that needs to be filtered
        Set<Id> setPromoId = new Set<Id>();
        
        if(String.isNotBlank(sTitleId)){
            
            String sActiveStatus = '';
            String sMasterCatalog ='';
            //Fetch Active Status String
            if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE'))){
                sActiveStatus = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
            }
            
            //Fetch Master String 
            if(String.isNotBlank(HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER'))){
                sMasterCatalog = HEP_Utility.getConstantValue('HEP_CATALOG_TYPE_MASTER');
            }
            
            //◾Promotion Catalogs have a Lookup F/K to Catalog, Catalog has a Lookup F/K to Title:
            
            List<HEP_Promotion_Catalog__c> lstPromotionCatalogs = [Select   Id,
                                                                            Promotion__c,
                                                                            Record_Status__c,
                                                                            Catalog__c
                                                                            from
                                                                            HEP_Promotion_Catalog__c
                                                                            where
                                                                            Catalog__r.Title_EDM__c = :sTitleId
                                                                            And
                                                                            Catalog__r.Record_Status__c =:sActiveStatus
                                                                            And
                                                                            Record_Status__c =:sActiveStatus
                                                                            And 
                                                                            Catalog__r.Type__c =:sMasterCatalog];
                                                                            
            //Store the promoIds from the Promotion Catalog List in the Set                                                         
            
            if(lstPromotionCatalogs != null && !lstPromotionCatalogs.isEmpty()){
                for(HEP_Promotion_Catalog__c objPromoCatalogs:lstPromotionCatalogs){
                    setPromoId.add(objPromoCatalogs.Promotion__c);
                }
            }
        }       
        //Return Promo ids
        return setPromoId;
    }
    
}