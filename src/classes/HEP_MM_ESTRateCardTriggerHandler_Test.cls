@isTest
private class HEP_MM_ESTRateCardTriggerHandler_Test {
	
	static testMethod void createPriceGrade() {

		HEP_Constants__c Enable_HEP_MM_ESTRateCardTrigger = new HEP_Constants__c();
		Enable_HEP_MM_ESTRateCardTrigger.Value__c = 'true';
		Enable_HEP_MM_ESTRateCardTrigger.name = 'Enable_HEP_MM_ESTRateCardTrigger';
		insert Enable_HEP_MM_ESTRateCardTrigger;
        
        HEP_MM_ESTRateCard__c objEstRateCard = new HEP_MM_ESTRateCard__c();
		objEstRateCard.DurationEnd__c = 7;
		objEstRateCard.DurationStart__c = 0;
		objEstRateCard.Product_Type__c = 'Film';
		objEstRateCard.Price__c = 3.99 ;
		objEstRateCard.MM_RateCardID__c = '8';
		objEstRateCard.Retail__c = 3.49;
		objEstRateCard.ProductSubType__c = 'Feature';

        Test.startTest();
        	insert objEstRateCard;

	        objEstRateCard.Retail__c = 3.09;
	        update objEstRateCard;
        Test.stoptest();
    }
}