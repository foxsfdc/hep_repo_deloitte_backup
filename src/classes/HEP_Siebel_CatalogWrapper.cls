/**
* HEP_Siebel_Catalog -- Wrapper to store Catalog details for interface Siebel 
* @author    Lakshman Jinnuri
*/
public class HEP_Siebel_CatalogWrapper{
    public String ProducerID;   
    public String EventType;    
    public String EventName;    
    public Data Data;

    /**
    * constructor to initialize the class variables
    * @return nothing 
    * @author Lakshman Jinnuri 
    */
    public HEP_Siebel_CatalogWrapper(){
        this.Data = new Data();
    }
    
    /**
    * Data -- Class to store Rating details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Payload Payload;

        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */ 
        public Data(){
            this.Payload = new Payload();
        }
    }

    /**
    * Payload -- Class to store Rating details 
    * @author    Lakshman Jinnuri
    */
    public class Payload {
        public list<Items> items;

        /**
        * constructor to initialize the class variables
        * @return nothing 
        * @author Lakshman Jinnuri 
        */
        public Payload(){
            items = new list<Items>();
        }
    }

    /**
    * Items -- Class to store Rating details 
    * @author    Lakshman Jinnuri
    */
    public class Items {
        public String objectType;   
        public String createdDate;  
        public String createdBy;    
        public String updatedDate;  
        public String updatedBy;    
        public String recordId; 
        public String recordStatus; 
        public String territoryId;  
        public String territoryName;    
        public String catalogNumber;   
        public String catalogName;  
        public String catalogType;  
        public String foxId;   
        public String titleNumber;  
        public String licensor; 
        public String licensorType;
        public String type; 
    }
}