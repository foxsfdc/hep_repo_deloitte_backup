@isTest(SeeAllData=false)
public class HEP_ReleaseChange_Report_Controller_Test {

	@testSetup
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
  	}

  	static testMethod void testPageData(){

  		HEP_ReleaseChange_Report_Controller_Test ReportClass = new HEP_ReleaseChange_Report_Controller_Test();

  		User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
    	HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id];
    	String sLocaleDateFormat =  u.DateFormat__c;

    	HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
      HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);

    	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

    	EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Series','SRIES', true);

    	EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Test Title', '12345', 'PUBLC', objProductType.id, true);

    	System.runAs(u){
    		Test.startTest();
    		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
    		objPromotion.Title__c = objTitle.id;
    		update objPromotion;
    		HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);
    		objNationalPromotion.Title__c = objTitle.id;
    		update objNationalPromotion;

    		//HEP_Promotion__History objPromotionHistory = new HEP_Promotion__History(ParentId = objNationalPromotion.Id, Field = 'title__c');
            //insert objPromotionHistory;

    		HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
    		objDatingRecord.Date__c = system.today();
    		update objDatingRecord;
    		objDatingRecord.Date__c = system.today().AddDays(1);
    		update objDatingRecord;
    		HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);
    		update objDatingRecordNational;
    		objDatingRecord.Date__c = system.today().AddDays(1);
    		update objDatingRecordNational;

    		HEP_Promotion__History[] PromotionHist = ReportClass.retrievePromotionHistory(objNationalPromotion.id);

    		HEP_ReleaseChange_Report_Controller.getPicklistData();
    		HEP_ReleaseChange_Report_Controller.getPageData(objNationalTerritory.id, sLocaleDateFormat);
    		Test.stopTest();
    	}

  	}

  	public HEP_Promotion__History[] retrievePromotionHistory(Id PromotionId){
     List<HEP_Promotion__History> lstPromotionHistory;
     if(Test.isRunningTest()){  //if TEST, create dummy AccountHistory
         lstPromotionHistory = new List<HEP_Promotion__History>{}; //OldValue, NewValue not writeable
         lstPromotionHistory.add(new HEP_Promotion__History(ParentId=PromotionId, Field='title__c'));
     }
     else 
         lstPromotionHistory=  [Select Parent.Id, Field, OldValue, NewValue from HEP_Promotion__History where Parent.Id = :PromotionId];


     return lstPromotionHistory;
	}

}