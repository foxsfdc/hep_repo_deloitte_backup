@isTest
public class HEP_SKU_Customer_Siebel_Wrapper_Test {
	@testSetup
 	 static void createUsers(){
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    List<HEP_List_Of_Values__c> lstLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
	    list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;

	    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
    	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

    	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

  		HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
		HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);


  		HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', '', 'Single', null, objNationalTerritory.Id, 'Pending', 'Master', true);

		HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objNationalPromotion.id, null, true);

		HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objNationalTerritory.Id, '', 'Test Title', 'Master', '', 'VOD', 'DVD', true);

		HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.id, objSKUMaster.id, 'Pending', 'Pending',true);

		HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Customer Test', objNationalTerritory.Id, '1234', 'SKU Customers', 'Test Date', true);
		HEP_Customer__c objCustomerForInactive = HEP_Test_Data_Setup_Utility.createHEPCustomers('Customer Test Inactive', objNationalTerritory.Id, '12345', 'SKU Customers', 'Test Date 2', true);

		HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.id, objSKUMaster.id, true);
		HEP_SKU_Customer__c objSKUCustomerInactive = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomerForInactive.id, objSKUMaster.id, false);
		objSKUCustomerInactive.Record_Status__c = 'Inactive';
		insert objSKUCustomerInactive;
  	}

  	static testMethod void testInterfactData(){

  		User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
  		system.runAs(u){
  			Test.startTest();
	    	HEP_SKU_Customer__c objSKUCustomer = [SELECT Id FROM HEP_SKU_Customer__c WHERE Record_Status__c = 'Active' LIMIT 1];
			

	  		HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
	  		objTxnResponse.sSourceId = objSKUCustomer.id;

	  		Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
	  		new HEP_SKU_Customer_Siebel_Wrapper().performTransaction(objTxnResponse);
	  		Test.stopTest();
	  	}	
  		
  	}
}