/**
* HEP_LivePromotionStatusUpdateScheduler -- class for Scheduling the HEP_Promotion_Status_Update_Batch
* @author Nidhin VK
*/
global class HEP_LivePromotionStatusUpdateScheduler implements Schedulable{
    
    global void execute(SchedulableContext sc) {
        HEP_Promotion_Status_Update_Batch objBatch = new HEP_Promotion_Status_Update_Batch();
        ID batchprocessid = Database.executeBatch(objBatch,200);           
    }
}