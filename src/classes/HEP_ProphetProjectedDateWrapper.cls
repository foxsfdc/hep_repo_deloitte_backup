/**
* HEP_ProphetProjectedDateWrapper -- Wrapper to store Projected Dates from Prophet Interface
* @author    Lakshman Jinnuri
*/

public class HEP_ProphetProjectedDateWrapper {
    //List to hold the Projected date details
    public list<ProjectedDate> projectedDate;
    public String calloutStatus;
    public String TxnId;
    public list<Errors> errors;
    
    /**
    * Attributes -- Class to hold Projected Date details 
    * @author    Lakshman Jinnuri
    */
    public class ProjectedDate {
        public String territoryId;
        public String territoryCode;
        public String title;
        public String projectedDate;
        public String territoryName;
        public String langId;
        public String channel;
        public String finTitleId;
    }
    
    /**
    * Errors -- Class to hold Error details 
    * @author     Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status; //400
    }

    /**
    * HEP_ProphetProjectedDateInputDetails  -- Class to hold related input details to be sent to the Prophet Interface
    * @author    Lakshman Jinnuri
    */
    public class ProphetProjectedDateInputDetails{
        public list<String> sFinTitleId{get;set;}
        public list<String> sTerritoryId{get;set;}
         /**
         * ProphetProjectedDateInputDetails  : Constructor to hold title Id'sand territory Code.
         * @param           : no values
         * @return          : nothing 
         */    
        public ProphetProjectedDateInputDetails(){  
            sFinTitleId = new list<String>();
            sTerritoryId =  new list<String>(); 
        } 
    }
}