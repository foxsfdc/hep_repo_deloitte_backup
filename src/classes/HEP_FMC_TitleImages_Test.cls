/**
* HEP_FMC_TitleImages_Test -- Test class for the HEP_RatingDetails for Foxipedia Interface. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_FMC_TitleImages_Test {
    /**
    * HEP_FMC_TitleImages_SuccessTest -- Test method to get Rating details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_FMC_TitleImages_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        objTitle.FIN_PROD_ID__c = '1AYB00';
        insert objTitle;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sRequest = HEP_Test_Data_Setup_Utility.generateSampleTitleImageWrapper('yyy8888800');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_TitleImages demo = new HEP_FMC_TitleImages();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

    /**
    * HEP_FMC_TitleImages_SuccessTest -- Test method to get Rating details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void HEP_FMC_TitleImages_SuccessTest2() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Season','SEASN',false);
        insert objProductType;
        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', objProductType.Id,false);
        objTitle.FIN_PROD_ID__c = '1AYB00';
        insert objTitle;
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sRequest = HEP_Test_Data_Setup_Utility.generateSampleTitleImageWrapper('yyy88888000');
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_TitleImages demo = new HEP_FMC_TitleImages();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

    /**
    * HEP_RatingDetails_InvalidAcessTokenTest -- Test method to get Rating details in case of invalid Access condition
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    /*@isTest static void HEP_FMC_TitleImages_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='FMC_OAuth';
        objService.Endpoint_URL__c = 'https://fmc-intapi.foxinc.com';
        objService.Service_URL__c = '/fmcproxy/fmcapi/v2/auth/authenticateFail';
        lstServices.add(objService);
        upsert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sRequest = HEP_Test_Data_Setup_Utility.generateSampleTitleImageWrapper('yyy88888000');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_FMC_TitleImages demo = new HEP_FMC_TitleImages();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }*/

}