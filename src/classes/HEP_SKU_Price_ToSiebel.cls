/*
 * HEP_SKU_Price_ToSiebel --- Class for Calling the API to insert/update HEP SKU Price records
 * @author  Himanshi Mayal
 */
public with sharing class HEP_SKU_Price_ToSiebel  implements HEP_IntegrationInterface {
    /**
    * SKUPriceWrapper --- main wrapper which holds the entire request
    * @author  Himanshi Mayal
    */
    public class SKUPriceWrapper{
    public String ProducerID;
    public String EventType;
    public String EventName;
    public Data Data;
    public SKUPriceWrapper(Data Data)
    {
      this.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID'); 
      this.EventType = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
      this.EventName = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
      this.Data = Data;  
    }    
  }
  public class Payload{
     List<Items> items;    
  }
  public class Data{
     public Payload Payload;
     public Data(Payload Payload){
      this.Payload = Payload;
     }
  } 
  public class ErrorResponseWrapper {
      public String status;
      public String errorMessage;
      public ErrorResponseWrapper(String status, String errorMessage){
          this.status = status;
          this.errorMessage = errorMessage;
      }
  }
    /**
    * Items --- record level wrapper to hold the record details
    * @author  Himanshi Mayal
    */
    public class Items{ 
        public String objectType;
        public String createdDate;
        public String createdBy;
        public String updatedDate;
        public String updatedBy;
        public String recordId;
        public String skuId;
        public String skuNumber;
        public String territoryId;
        public String territoryName;
        public String priceGrade;
        public Decimal dealerListPrice;
        public Date startDate;
        public String recordStatus;
        public String regionId;
        public String regionName;
        public Decimal units;
        public String promotionDatingId;
        public String legacyId;
        public String promotionSKUId;
        public String changedFlag;
        public Items(){
        this.objectType = HEP_Utility.getConstantValue('HEP_SIEBEL_SKUPRICEOBJ');
        }
    } 
    /**
    * Perform the API call.
    * @param empty response
    * @return 
    * @author  Himanshi Mayal
    */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        list<String> lstRecordIds = new list<string>();
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();

        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');

            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                
                if(String.isNotBlank(objTxnResponse.sSourceId))
                    lstRecordIds = objTxnResponse.sSourceId.split(',');
                
                system.debug('Request ---- ' + getSkuPriceRequestBody(lstRecordIds) );
                httpRequest.setBody(getSkuPriceRequestBody(lstRecordIds));
                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout

                if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK') || httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')){  
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }
    
    /**
    * Get the serialized json of the request to be sent
    * @param list of record ids
    * @return
    * @author  Himanshi Mayal
    */
    public static String getSkuPriceRequestBody(List<string> lstItemsId){
        String finalJSON = '';
        String recordStatusActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        List<HEP_SKU_Price__c> lstSkuPrice = new List<HEP_SKU_Price__c>();
        lstSkuPrice = [SELECT createdDate, createdBy.Name, lastModifiedDate, lastModifiedBy.Name, id,
                       SKU_Master__r.Territory__c, SKU_Master__r.Territory__r.Name, Unit__c, SKU_Master__c,
                       SKU_Master__r.SKU_Number__c, PriceGrade__r.PriceGrade__c, PriceGrade__r.Dealer_List_Price__c,
                       Start_Date__c, Record_Status__c, Region__c, Region__r.Name, Promotion_SKU__r.id, Promotion_Dating__r.id,
                       Legacy_Id__c, Changed_Flag__c FROM HEP_SKU_Price__c WHERE id in :lstItemsId];
        System.debug('LIST OF SKU PRICE QUERY : : :  '+lstSkuPrice);
        if(!lstSkuPrice.isEmpty()){
            
            List<Items> lstItem = new List<Items>();
            for(HEP_SKU_Price__c skuPrice : lstSkuPrice){
                Items objItem = new Items();
                objItem.createdDate = String.ValueOf(skuPrice.createdDate);
                objItem.createdBy = skuPrice.createdBy.Name;
                objItem.updatedDate = String.ValueOf(skuPrice.lastModifiedDate);
                objItem.updatedBy = skuPrice.lastModifiedBy.Name;
                objItem.recordId = skuPrice.id;
                objItem.skuId = skuPrice.SKU_Master__r.id;
                objItem.skuNumber = skuPrice.SKU_Master__r.SKU_Number__c;
                objItem.territoryId = skuPrice.SKU_Master__r.Territory__r.id;
                objItem.territoryName = skuPrice.SKU_Master__r.Territory__r.Name;
                objItem.priceGrade = skuPrice.PriceGrade__r.PriceGrade__c;
                objItem.dealerListPrice = skuPrice.PriceGrade__r.Dealer_List_Price__c;
                objItem.startDate = skuPrice.Start_Date__c;
                objItem.recordStatus = skuPrice.Record_Status__c;
                objItem.regionId = skuPrice.Region__r.id;
                objItem.regionName = skuPrice.Region__r.Name;                
                objItem.promotionSKUId = skuPrice.Promotion_SKU__r.id;
                objItem.promotionDatingId = skuPrice.Promotion_Dating__r.id;
                objItem.legacyId = skuPrice.Legacy_Id__c;
                objItem.units = skuPrice.Unit__c;
                if(skuPrice.Changed_Flag__c == true)
                    objItem.changedFlag = HEP_Utility.getConstantValue('HEP_Changed_Flag_Y');
                else if(skuPrice.Changed_Flag__c == false)
                    objItem.changedFlag = HEP_Utility.getConstantValue('HEP_Changed_Flag_N');
                lstItem.add(objItem);
            }
            Payload payl = new Payload();
            payl.items = lstItem;
            Data dat = new Data(payl);
            SKUPriceWrapper priceRoot = new SKUPriceWrapper(dat);
            finalJSON = JSON.serialize(priceRoot);
            System.debug(' RETURNED JSON :::: '+finalJSON);
        }
        return finalJSON;
    }
}