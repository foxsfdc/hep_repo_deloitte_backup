/**
* HEP_ProphetProjectedDateDetails_Test -- Test class for the HEP_ProphetProjectedDateDetails for Prophet Interfaces  
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_ProphetProjectedDateDetails_Test {
    /**
    * HEP_ProphetProjectedDateDetails_SuccessTest -- Test method in case of Successful response from Prophet Interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    private static testMethod void HEP_ProphetProjectedDateDetails_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date','HEP_ProphetProjectedDateDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('232932');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('232932932');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_Projected_Date');
        System.assertEquals('HEP_Prophet_Projected_Date',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_ProphetProjectedDateDetails_FailureTest -- Test method in case of Failure response from Prophet Interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    private static testMethod void HEP_ProphetProjectedDateDetails_FailureTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date','HEP_ProphetProjectedDateDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('1111');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('1111');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_Projected_Date');
        System.assertEquals('HEP_Prophet_Projected_Date',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_ProphetProjectedDateDetails_InvalidAcessTokenTest -- Test method in case of Invalid Access Token from Prophet Interface
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    private static testMethod void HEP_ProphetProjectedDateDetails_InvalidAcessTokenTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        HEP_Services__c objService = new HEP_Services__c();
         list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        objService.Name='Prophet_OAuth';
        objService.Endpoint_URL__c = 'https://feg-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date','HEP_ProphetProjectedDateDetails',true,10,10,'Outbound-Pull',true,true,true);
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objDateInputDetails = new HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        objDateInputDetails.sFinTitleId = new List<String>();
        objDateInputDetails.sFinTitleId.add('1111');
        objDateInputDetails.sTerritoryId = new List<String>();
        objDateInputDetails.sTerritoryId.add('1111');
        String sSourceId = JSON.serialize(objDateInputDetails);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sSourceId,'','HEP_Prophet_Projected_Date');
        System.assertEquals('HEP_Prophet_Projected_Date',objTxnResponse.sInterfaceName);
        Test.stoptest();
    }
    /**
    * HEP_ProphetProjectedDateWrapperTest -- Test method for the Wrapper
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    private static testMethod void HEP_ProphetProjectedDateWrapperTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        HEP_ProphetProjectedDateWrapper objProphetProjectWrapper = new HEP_ProphetProjectedDateWrapper();
        objProphetProjectWrapper.calloutStatus = null;
        objProphetProjectWrapper.TxnId = null;
        HEP_ProphetProjectedDateWrapper.ProjectedDate objProjectedDate = new HEP_ProphetProjectedDateWrapper.ProjectedDate();
        objProjectedDate.territoryId = '31';
        objProjectedDate.territoryCode = 'AUS';
        objProjectedDate.title = '12 Rounds 2: Reloaded';
        objProjectedDate.projectedDate = '2013-10-09';
        objProjectedDate.territoryName = 'Australia';
        objProjectedDate.langId = '2';
        objProjectedDate.channel = 'RENTAL';
        objProjectedDate.finTitleId = '132043';
        HEP_ProphetProjectedDateWrapper.Errors objErrors = new HEP_ProphetProjectedDateWrapper.Errors();
        objErrors.title = null;
        objErrors.detail = null;
        objErrors.status = null;
        HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails objInputDetails =  new  HEP_ProphetProjectedDateWrapper.ProphetProjectedDateInputDetails();
        Test.stoptest();
    }
}