/**
* HEP_Promotion_Details_Controller_Test --- Test class for HEP_Promotion_Details_Controller
* @author    Sachin Agarwal
*/
@isTest 
public with sharing class HEP_Promotion_Details_Controller_Test {
	/**
    * Its used to setting up the test data
    * @author Sachin Agarwal
    */
    @testSetup 
    public static void createData(){
        // List of HEP Constants
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        
       	//Create EDM Product Type.
		EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('test','test',true);

		//Create the interface record
		HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();
		
		//Create EDM Title Record.
		EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('Sample Title' , '197429' , 'PUBLC' , objProductType.Id, true);
        
        list<HEP_Territory__c> lstRegionTerritories = new list<HEP_Territory__c>(); 
        
        // Creating territories
        HEP_Territory__c objAustraliaTerritory  = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', true);
        HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '1', null, true);
        
        // Creating regional territories
        lstRegionTerritories.add(HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium', 'EMEA', 'Subsidiary', objAustraliaTerritory.id, null, 'EUR', 'BEL', '49', '', false));
        lstRegionTerritories.add(HEP_Test_Data_Setup_Utility.createHEPTerritory('Netherlands', 'EMEA', 'Subsidiary', objAustraliaTerritory.id, null, 'EUR', 'NL', '324', '', false));
        
        insert lstRegionTerritories;
        
        // Inserting role
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', null, true);
    
    	// Creating user role records
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objAustraliaTerritory.id, objRole.id, UserInfo.getUserId(), true);
        
        list<HEP_Line_Of_Business__c> lstLOBs = new list<HEP_Line_Of_Business__c>();
        
        //Create Line of Business
		HEP_Line_Of_Business__c objNewReleaseLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - New Release', 'New Release', false);
		HEP_Line_Of_Business__c objTVLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV', 'TV', false);
		HEP_Line_Of_Business__c objCatalogLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Catalog', 'Catalog', false);
		
		lstLOBs.add(objNewReleaseLOB);
		lstLOBs.add(objTVLOB);
		lstLOBs.add(objCatalogLOB);
		
		insert lstLOBs;
		
		// Creating global promotion
		HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion', 'Global', null, objGlobalTerritory.Id, lstLOBs[0].Id, null, UserInfo.getUserId(), false);
		objGlobalPromotion.Title__c = objTitle.id;
		insert objGlobalPromotion;
		
		// Creating national promotion
		HEP_Promotion__c objAustraliaPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Australia Promotion', 'National', objGlobalPromotion.id, objAustraliaTerritory.id, lstLOBs[0].Id, null, UserInfo.getUserId(), false);
		objAustraliaPromotion.Title__c = objTitle.id;
		insert objAustraliaPromotion;
		
		// Creating TPR promotion
		HEP_Promotion__c objTPRPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Australia Promotion', 'National', null, objAustraliaTerritory.id, lstLOBs[0].Id, null, UserInfo.getUserId(), false);
		objTPRPromotion.Title__c = objTitle.id;
		objTPRPromotion.TPR_Only__c = true;
		insert objTPRPromotion;
		
		// Creating Customer promotion
		HEP_Promotion__c objCustomerPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Australia Promotion', 'Customer', null, objAustraliaTerritory.id, lstLOBs[0].Id, null, UserInfo.getUserId(), false);
		objCustomerPromotion.Title__c = objTitle.id;
		insert objCustomerPromotion;
		
		list<HEP_Promotion_Region__c> lstPromoRegions = new list<HEP_Promotion_Region__c>();
		 
		lstPromoRegions.add(HEP_Test_Data_Setup_Utility.createPromotionRegion(objCustomerPromotion.id, lstRegionTerritories[0].id, false));
		lstPromoRegions.add(HEP_Test_Data_Setup_Utility.createPromotionRegion(objCustomerPromotion.id, lstRegionTerritories[1].id, false));
		
		// Creating promotion regions
		insert lstPromoRegions;
		
		//Create Notification Record..
		HEP_Notification_Template__c objNotification = HEP_Test_Data_Setup_Utility.createTemplate(HEP_Utility.getConstantValue('HEP_Global_FAD_Date_Change'), 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',true);
		
		HEP_Market_Spend__c objTestMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154', objAustraliaPromotion.Id, 'None', true);
		
		//Create Approval Records.
		HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Approved', 'Rejected', 'SPEND', 'Direct', 'Pending', true);
		HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalType.Id, null, false);
		objApproval.Record_ID__c = objTestMarketSpend.id;
		objApproval.HEP_Market_Spend__c = objTestMarketSpend.id;
		objApproval.History_Data__c = JSON.serializePretty(new HEP_Promotion_Spend_Controller.ApprovalHistory('Test Comment', 1000, 2000));
		insert objApproval;
		
		HEP_Record_Approver__c objRecordApprovers = HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, UserInfo.getUserId(), objRole.Id, 'Pending', true);
		
		HEP_Promotions_DatingMatrix__c objVODDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objAustraliaTerritory.Id, objAustraliaTerritory.Id, false);
      	objVODDatingMatrix.Projected_FAD_Logic__c = 'Monday';
      	objVODDatingMatrix.Media_Type__c ='Digital';
      	insert objVODDatingMatrix;
      
      	// Creating dating records
      	HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objAustraliaTerritory.Id, objAustraliaPromotion.Id, objVODDatingMatrix.Id, false);
      	objDatingRecord.Status__c = 'Confirmed';
      	objDatingRecord.Locked_Status__c = 'Locked';
      	insert objDatingRecord;
    }
    
    
    /**
    * Its used to test the class constructor
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testHEP_Promotion_Details_Controller(){
    	
    	PageReference promotionDetailsPage = Page.HEP_Promotion_Details;
		Test.setCurrentPage(promotionDetailsPage);
		
		list<HEP_Promotion__c> lstGlobalPromotions = new list<HEP_Promotion__c>();
		lstGlobalPromotions = [SELECT id,
									  (SELECT ID
									   FROM HEP_Promotions__r)
							   FROM HEP_Promotion__c
							   WHERE Promotion_Type__c = 'Global']; 
		
		// If there's a matching global promotion
		if(lstGlobalPromotions != null && !lstGlobalPromotions.isEmpty()){
			// Put Id into the current page Parameters
			ApexPages.currentPage().getParameters().put('promoId', lstGlobalPromotions[0].Id);
			HEP_Promotion_Details_Controller objController = new HEP_Promotion_Details_Controller();
			objController.checkPermissions();
		}		
    }
    
    
    /**
    * Its used to test getImageUrl method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testGetImageUrl(){
    	
		list<HEP_Promotion__c> lstGlobalPromotions = new list<HEP_Promotion__c>();
		lstGlobalPromotions = [SELECT id,
									  (SELECT ID
									   FROM HEP_Promotions__r)
							   FROM HEP_Promotion__c
							   WHERE Promotion_Type__c = 'Global']; 
		
		// If there's a matching global promotion
		if(lstGlobalPromotions != null && !lstGlobalPromotions.isEmpty()){
			
			HEP_Promotion_Details_Controller.getImageUrl(lstGlobalPromotions[0].id, null);
			HEP_Promotion_Details_Controller.getImageUrl(lstGlobalPromotions[0].id, 'abc');
		}		
    }
    
    
    /**
    * Its used to test geteaderDetails method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testGetHeaderDetails(){
    	
		list<HEP_Promotion__c> lstGlobalPromotions = new list<HEP_Promotion__c>();
		lstGlobalPromotions = [SELECT id,
									  (SELECT ID
									   FROM HEP_Promotions__r)
							   FROM HEP_Promotion__c
							   WHERE Promotion_Type__c = 'Global']; 
		
		// If there's a matching global promotion
		if(lstGlobalPromotions != null && !lstGlobalPromotions.isEmpty()){
		
			HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].id, null);
			if(lstGlobalPromotions[0].HEP_Promotions__r != null && !lstGlobalPromotions[0].HEP_Promotions__r.isEmpty()){
				HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);	
				
				list<HEP_Market_Spend__c> lstMarketSpend = new list<HEP_Market_Spend__c>();
				lstMarketSpend = [SELECT id, RecordStatus__c, Promotion__c
								  FROM HEP_Market_Spend__c
								  WHERE Promotion__c = :lstGlobalPromotions[0].HEP_Promotions__r[0].id];
				
				// If there's a matching market spend record
				if(lstMarketSpend != null && !lstMarketSpend.isEmpty()){ 
					lstMarketSpend[0].RecordStatus__c = 'Budget Approved';
					update lstMarketSpend;
				}
				HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);
				
				if(lstMarketSpend != null && !lstMarketSpend.isEmpty()){ 
					lstMarketSpend[0].RecordStatus__c = 'Rejected';
					update lstMarketSpend;
				}
				HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);
				if(lstMarketSpend != null && !lstMarketSpend.isEmpty()){ 
					lstMarketSpend[0].RecordStatus__c = 'Pending';
					update lstMarketSpend;
				}
				HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);
				
				list<HEP_Promotion__c> lstTPRPromotions = new list<HEP_Promotion__c>();
				lstTPRPromotions = [SELECT id,
									  	  (SELECT ID
									   	   FROM HEP_Promotions__r)
							   		FROM HEP_Promotion__c
							   		WHERE TPR_Only__c = true]; 
				if(lstTPRPromotions != null && !lstTPRPromotions.isEmpty()){			   
					HEP_Promotion_Details_Controller.getHeaderDetails(lstTPRPromotions[0].id, null);
				}
				
				list<HEP_Line_Of_Business__c> lstLOBs = new list<HEP_Line_Of_Business__c>();
				lstLOBs = [SELECT id
						   FROM HEP_Line_Of_Business__c
						   WHERE Type__c = 'TV' OR Type__c = 'Catalog'];
				
				if(lstLOBs != null && !lstLOBs.isEmpty()){		   
					HEP_Promotion__c objPromo = new HEP_Promotion__c();
					objPromo.id = lstGlobalPromotions[0].HEP_Promotions__r[0].id;
					objPromo.LineOfBusiness__c = lstLOBs[0].id;
					update objPromo;
					
					HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);
					
					if(lstLOBs.size() > 1){
						objPromo.LineOfBusiness__c = lstLOBs[1].id;
						update objPromo;
					
						HEP_Promotion_Details_Controller.getHeaderDetails(lstGlobalPromotions[0].HEP_Promotions__r[0].id, null);						
					}
				}
				
				list<HEP_Promotion__c> lstCustomerPromotions = new list<HEP_Promotion__c>();
				lstCustomerPromotions = [SELECT id,
									  	  		(SELECT ID
	    									   	 FROM HEP_Promotions__r)
							   			 FROM HEP_Promotion__c
							   			 WHERE Promotion_Type__c = 'Customer']; 
				
				if(lstCustomerPromotions != null && !lstCustomerPromotions.isEmpty()){			   
					HEP_Promotion_Details_Controller.getHeaderDetails(lstCustomerPromotions[0].id, null);
				}
			}
		}		
    }
    
    
    /**
    * Its used to test setResetFavourite method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testSetResetFavourite(){
    	
		list<HEP_Promotion__c> lstGlobalPromotions = new list<HEP_Promotion__c>();
		lstGlobalPromotions = [SELECT id,
									  (SELECT ID
									   FROM HEP_Promotions__r)
							   FROM HEP_Promotion__c
							   WHERE Promotion_Type__c = 'Global']; 
		
		// If there's a matching global promotion
		if(lstGlobalPromotions != null && !lstGlobalPromotions.isEmpty()){
		
			String sUserFavid = HEP_Promotion_Details_Controller.setResetFavourite(null, lstGlobalPromotions[0].id, true);
			HEP_Promotion_Details_Controller.setResetFavourite(sUserFavid, lstGlobalPromotions[0].id, false);
			HEP_Promotion_Details_Controller.setResetFavourite(sUserFavid, lstGlobalPromotions[0].id, true);
		}		
    }
    
    
    /**
    * Its used to test uploadFile method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testUploadFile(){
    	
		list<HEP_Promotion__c> lstGlobalPromotions = new list<HEP_Promotion__c>();
		lstGlobalPromotions = [SELECT id,
									  (SELECT ID
									   FROM HEP_Promotions__r)
							   FROM HEP_Promotion__c
							   WHERE Promotion_Type__c = 'Global']; 
		
		// If there's a matching global promotion
		if(lstGlobalPromotions != null && !lstGlobalPromotions.isEmpty()){
			HEP_Promotion_Details_Controller objPromoDetailsController = new HEP_Promotion_Details_Controller();
			
			objPromoDetailsController.sFileName = 'TestFile';
			
			String sFileData = 'I am String to be converted in base64 encoding!';
			objPromoDetailsController.sBinaryData = EncodingUtil.base64Encode(Blob.valueOf(sFileData));
			
			objPromoDetailsController.sPromotionId = lstGlobalPromotions[0].id;
			objPromoDetailsController.uploadFile();
			objPromoDetailsController.sFileName = 'TestFile';
			
			objPromoDetailsController.sBinaryData = EncodingUtil.base64Encode(Blob.valueOf(sFileData));
			objPromoDetailsController.uploadFile();
			
		}		
    }
    
    
    /**
    * Its used to test getTerritoryRegion method
    * @author Sachin Agarwal
    */
    @isTest 
    public static void testGetTerritoryRegion(){
    	list<HEP_Promotion__c> lstCustomerPromotion = new list<HEP_Promotion__c>();
    	 
    	lstCustomerPromotion = [SELECT id, Territory__c, Global_Promotion__c, Territory__r.Name, Territory__r.Type__c, 
                                  	   LineOfBusiness__c, Promotion_Image_URL__c,  Territory__r.Region__c, LocalPromotionCode__c,
                                  	   (SELECT Parent_Promotion__c, Territory__c, Territory__r.Name
                                   		FROM HEP_Promotion_Regions__r)
                                FROM HEP_Promotion__c
                                WHERE Promotion_Type__c = 'Customer'];
		
		// If there's a matching customer promotion
		if(lstCustomerPromotion != null && !lstCustomerPromotion.isEmpty()){                                  
			HEP_Promotion_Details_Controller.getTerritoryRegion(lstCustomerPromotion[0]);
		}
    	
    }
}