/**
* HEP_SKU_ImageWrapper-- Wrapper to store Image details
* @author    Nishit Kedia
*/

public class HEP_SKU_ImageWrapper{
    
    //Json outer Memebers 
    public HeaderWrapper header ;
    public List<ItemsWrappers> all_items;
    public list<Errors> errors;
    public String calloutStatus;
    public String TxnId;
    
    /**
    * HeaderWrapper -- Class to hold Header Status
    * @author    Nishit Kedia
    */
    public class HeaderWrapper {
        public String status;   //ok
        public String message ;  //
    }
    
    
    /**
    * ItemsWrappers -- Class to hold Items details 
    * @author    Nishit Kedia
    */
    public class ItemsWrappers {
        public String asset_id ; //35085
        public String catalog_num ;  //21708SDO
        public String product_name ; //TIGERLAND
        public String url ;  //http://www.foxretail.com.au/index.php?mod=ws_image_convert&assetId=35085&height=212&convertFunc=resize&genToken=y15N0Q9Lpgd%2FMq4ut2ErbzfKhFrVcjK1LIa6m%2FI%2BewI%3D
        public String width ;   //150
        public String height ;  //212
        public String image_angle ;  //2D
        public String hi_res_img ; //http://www.foxretail.com.au/index.php?mod=ws_image_convert&assetId=35085&width=1524&convertFunc=resize&src=original&genToken=DtJz5ZQqH99M9eHDKVcH72OQVx0MX0fH9TwGethk5rw%3D
        public String hi_res_width ; //1524
        public String hi_res_height ;   //2150
    }
    
       
    /**
    * Errors -- Class to hold related Error details 
    * @author    Nishit Kedia
    */
    public class Errors{
        public String title ;
        public String detail ;
        public Integer status ; //400
    }
}