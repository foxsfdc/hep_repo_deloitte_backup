/**
 *HEP_SKU_Customer_TriggerHandler --- trigger handler for SKU_Customer object trigger for integration with siebel
 *@author  Mayank Jaggi
 */

public with sharing class HEP_SKU_Customer_TriggerHandler extends TriggerHandler {

   /**
    * afterInsert -- Context specific afterInsert method
    * @return nothing
     *@author  Mayank Jaggi
    */
    public override void afterInsert()
    {
      System.debug('newMap>>' + Trigger.newMap);
      callQueuableMethod((Map<Id, HEP_SKU_Customer__c>) Trigger.newMap);
    }

     /**
    * afterUpdate -- Context specific afterUpdate method
    * @return nothing
     *@author  Mayank Jaggi
    */
    public override void afterUpdate()
    {
        if(Trigger.oldMap != Null)
        {
          System.debug('oldMap>>' + Trigger.oldMap);
          System.debug('newMap>>' + Trigger.newMap);        
          Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap,
              'HEP_SKU_Customer__c', 'SKU_Customer_Field_Set');
                      System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
          if(checkFieldUpdate)
              callQueuableMethod((Map<Id, HEP_SKU_Customer__c>) Trigger.newMap);
        }
        
    }

    /**
    * callQueuableMethod -- enqueues the job
    * @param map of ids and HEP_SKU_Customer__c records
    * @return nothing
     *@author  Mayank Jaggi
    */
    public static void callQueuableMethod(Map<Id, HEP_SKU_Customer__c> mapSKUs)
    {
      List<Id> lstSKUIds = new List<Id>();
     lstSKUIds.addAll(mapSKUs.keySet());
      if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
        System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstSKUIds, ','),
          HEP_Utility.getConstantValue('HEP_SIEBEL_SKUCUSTOMER')));
      }
    }
}