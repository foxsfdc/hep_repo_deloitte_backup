@isTest
public class HEP_Catalog_SKUs_Controller_Test{
    public static testmethod void getSKUTest(){
        //All HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant');
        //Create Territory Record
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Australia';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        objTerritory.SKU_Interface__c = 'ESCO';
        insert objTerritory;
        //Create HEP Catalog record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','FOX','SD',false);
        objSKUMaster.Barcode__c = 'bCode';
        insert objSKUMaster;
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create Test Role 
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Test Role', 'Promotions' , false);    
        insert objRole;
        //Create HEP Customer record
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        insert objCustomer;
        //Create SKU Customer Record
        HEP_SKU_Customer__c objSKUCust = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id,objSKUMaster.Id,false);
        insert objSKUCust;
        //Create test User Role record
        HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRole.Id,u.Id,false);
        insert objUserRole;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        insert objPromotion1;
        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
        insert objDatingMatrix;
        //Create SKU Price Grade Record
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id,'$99.99-$149.99',49.00,false);
        insert objPriceGrade;
        //Promotion Dating Record
        HEP_Promotion_Dating__c  objPromoDating = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id,objPromotion1.Id,objDatingMatrix.Id,false);
        objPromoDating.Date__c = Date.newInstance(2018, 07, 9);
        insert objPromoDating;
        //Create LOB Record_Status__c
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','Catalog',false);
        insert objLOB;
        //Create SKU Template Record
        HEP_SKU_Template__c  objSKUTemp = HEP_Test_Data_Setup_Utility.createSKUTemplate('SKU Template',objTerritory.Id,objLOB.Id,false);
        objSKUTemp.Record_Status__c = 'Active';
        //insert objSKUTemp;
        //Create Promotion Catalog record
        HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemp.Id,false);
        //insert objPromoCatalog;
        //Create Promotion SKU Record -> Approved
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Approved','Unlocked',false);
        //insert objPromoSKU;
        //Create SKU Price Record
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id,objSKUMaster.Id,objTerritory.Id,objPromoSKU.Id,objPromoDating.Id,'Active',false);
        //insert objSKUPrice;
        test.startTest();
        System.runAs(u){
            HEP_Catalog_SKUs_Controller.getSKU(objCatalog.Id);
            HEP_Catalog_SKUs_Controller.SKUWrapper objSKUWrap = new HEP_Catalog_SKUs_Controller.SKUWrapper('key','name','9','EST','VOD','barcode','Active','US','Amazon','99');
        }   
        test.stopTest();
    }
}