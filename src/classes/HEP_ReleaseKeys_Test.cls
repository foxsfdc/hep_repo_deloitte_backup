/**
 * ------------------------HEP_ReleaseKeys_Copper_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest (seeAllData = false)
public class HEP_ReleaseKeys_Test {
    @isTest
    static void testPost(){
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/HEP_ReleaseKeys/'; 
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HEP_ReleaseKeys.getReleaseKeys('2018/07/20T00:00:00','PROPHET');
        HEP_ReleaseKeys.getReleaseKeys('2018/07/20T00:00:00','COPPER');
        Test.stopTest();

    }
    
    @isTest
    static void testGet(){
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/v1/HEP_ReleaseKeys/'; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        HEP_ReleaseKeys.getFoxipediaReleaseKeys();
        Test.stopTest();
        
    }
}