@isTest
public class HEP_Notifications_Controller_Test {

  @testSetup
    static void createUsers()
    {   
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
          objInterface.Name = 'HEP_E1_SpendDetail';
          Update objInterface;
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        
      
      HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
      objRole.Destination_User__c = u.Id;
      objRole.Source_User__c = u.Id;
      insert objRole;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
        objNationalTerritory.Flag_Country_Code__c = 'DE';
        insert objNationalTerritory;

        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);

        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
        HEP_Line_Of_Business__c objLOBTV = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks TV','TV',true);

        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,false);
        objPromotion.Domestic_Marketing_Manager__c = u.id;
        insert objPromotion;
        HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

        HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234', objNationalPromotion.Id, 'Pending', true);

        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', '', 'Single', null, objNationalTerritory.Id, 'Pending', 'Master', true);

        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objNationalPromotion.id, null, true);

        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objNationalTerritory.Id, '1234', 'Test Title', 'Master', '', 'VOD', 'DVD', true);
        
        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.id, objSKUMaster.id, 'Pending', 'Pending',true);

        HEP_MDP_Promotion_Product__c objMDPProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objNationalPromotion.Id, null, objNationalTerritory.Id, true);

        HEP_Notification_Template__c objNotificationTemplateDating = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplateDating.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplateDating;
        HEP_Notification_Template__c objNotificationTemplateSpend = HEP_Test_Data_Setup_Utility.createTemplate('Budget_Update_Approved', 'HEP_Market_Spend__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplateSpend.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplateSpend;
        HEP_Notification_Template__c objNotificationTemplatePromotionSKU = HEP_Test_Data_Setup_Utility.createTemplate('SKU_Activation_Request_Rejected', 'HEP_Promotion_SKU__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplatePromotionSKU.Notifying_Field__c = 'Approval_Status__c';
        insert objNotificationTemplatePromotionSKU;
        HEP_Notification_Template__c objNotificationTemplatePromotion = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplatePromotion.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplatePromotion;
        HEP_Notification_Template__c objNotificationTemplateCatalog = HEP_Test_Data_Setup_Utility.createTemplate('Catalog_Approved', 'HEP_Catalog__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplateCatalog.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplateCatalog;
        HEP_Notification_Template__c objNotificationTemplatePromotionCatalog = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Catalog_Addition', 'HEP_Promotion_Catalog__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplatePromotionCatalog.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplatePromotionCatalog;
        HEP_Notification_Template__c objNotificationTemplateMDPProduct = HEP_Test_Data_Setup_Utility.createTemplate('TPR_Discount_Customer_Rejected', 'HEP_MDP_Promotion_Product__c','test body','test type' ,'Active','Approved', 'test',false);
        objNotificationTemplateMDPProduct.Notifying_Field__c = 'Record_Status__c';
        insert objNotificationTemplateMDPProduct;

        HEP_Notification__c objNotificationDating = new HEP_Notification__c(Notification_Template__c = objNotificationTemplateDating.id,HEP_Assignee_ID__c = 'test1',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Promotion_Dating__c',HEP_Record_ID__c = objDatingRecordNational.id);
        insert objNotificationDating;
        HEP_Notification__c objNotificationBudget = new HEP_Notification__c(Notification_Template__c = objNotificationTemplateSpend.id,HEP_Assignee_ID__c = 'test2',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Market_Spend__c',HEP_Record_ID__c = objMarketSpend.id);
        insert objNotificationBudget;
        HEP_Notification__c objNotificationPromotionSKU = new HEP_Notification__c(Notification_Template__c = objNotificationTemplatePromotionSKU.id,HEP_Assignee_ID__c = 'test3',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Promotion_SKU__c',HEP_Record_ID__c = objPromotionSKU.id);
        insert objNotificationPromotionSKU;
        HEP_Notification__c objNotificationPromotion = new HEP_Notification__c(Notification_Template__c = objNotificationTemplatePromotion.id,HEP_Assignee_ID__c = 'test4',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Promotion__c',HEP_Record_ID__c = objNationalPromotion.id);
        insert objNotificationPromotion;
        HEP_Notification__c objNotificationCatalog = new HEP_Notification__c(Notification_Template__c = objNotificationTemplateCatalog.id,HEP_Assignee_ID__c = 'test5',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Catalog__c',HEP_Record_ID__c = objCatalog.id);
        insert objNotificationCatalog;
        HEP_Notification__c objNotificationPromotionCatalog = new HEP_Notification__c(Notification_Template__c = objNotificationTemplatePromotionCatalog.id,HEP_Assignee_ID__c = 'test6',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_Promotion_Catalog__c',HEP_Record_ID__c = objPromotionCatalog.id);
        insert objNotificationPromotionCatalog;
        HEP_Notification__c objNotificationMDPProduct = new HEP_Notification__c(Notification_Template__c = objNotificationTemplateMDPProduct.id,HEP_Assignee_ID__c = 'test7',HEP_Message__c = 'test',HEP_Object_API__c = 'HEP_MDP_Promotion_Product__c',HEP_Record_ID__c = objMDPProduct.id);
        insert objNotificationMDPProduct;

        HEP_Notification_User_Action__c objNotificationUserActionDating = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationDating.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionDating;
        HEP_Notification_User_Action__c objNotificationUserActionSpend = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationBudget.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionSpend;
        HEP_Notification_User_Action__c objNotificationUserActionPromotionSKU = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationPromotionSKU.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionPromotionSKU;
        HEP_Notification_User_Action__c objNotificationUserActionPromotion = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationPromotion.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionPromotion;
        HEP_Notification_User_Action__c objNotificationUserActionCatalog = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationCatalog.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionCatalog;
        HEP_Notification_User_Action__c objNotificationUserActionPromotionCatalog = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationPromotionCatalog.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionPromotionCatalog;
        HEP_Notification_User_Action__c objNotificationUserActionMDPProduct = new HEP_Notification_User_Action__c(HEP_Notification__c = objNotificationMDPProduct.id,IsRead__c = true,Notification_User__c = u.id);
        insert objNotificationUserActionMDPProduct;
    }

    @isTest
    static void notificationTest(){
      
      User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
      HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id];

        HEP_Notification__c objNotificationDating = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test1'];
        HEP_Notification__c objNotificationBudget = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test2'];
        HEP_Notification__c objNotificationPromotionSKU = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test3'];
        HEP_Notification__c objNotificationPromotion = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test4'];
        HEP_Notification__c objNotificationCatalog = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test5'];
        HEP_Notification__c objNotificationPromotionCatalog = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test6'];
        HEP_Notification__c objNotificationMDPProduct = [SELECT Id,HEP_Assignee_ID__c FROM HEP_Notification__c WHERE HEP_Assignee_ID__c = 'test7'];

        HEP_Notification_User_Action__c objNotificationUserActionDating = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationDating.id];
        HEP_Notification_User_Action__c objNotificationUserActionBudget = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationBudget.id];
        HEP_Notification_User_Action__c objNotificationUserActionPromotionSKU = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationPromotionSKU.id];
        HEP_Notification_User_Action__c objNotificationUserActionPromotion = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationPromotion.id];
        HEP_Notification_User_Action__c objNotificationUserActionCatalog = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationCatalog.id];
        HEP_Notification_User_Action__c objNotificationUserActionPromotionCatalog = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationPromotionCatalog.id];
        HEP_Notification_User_Action__c objNotificationUserActionMDPProduct = [SELECT Id,CreatedDate,HEP_Notification__c,HEP_Notification__r.HEP_Message__c,HEP_Notification__r.Notification_Template__r.Type__c,HEP_Notification__r.Notification_Template__r.Status__c FROM HEP_Notification_User_Action__c WHERE HEP_Notification__c =: objNotificationMDPProduct.id];

      System.runAs(u){
        Test.startTest();


        HEP_Notifications_Controller.getNotificationDetails();
            HEP_Notifications_Controller.fillCommonFields(objNotificationUserActionDating);
        Test.stopTest();
      }
    }
     

}