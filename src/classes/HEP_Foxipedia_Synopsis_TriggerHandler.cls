/**
*HEP_Foxipedia_Synopsis_Publish_TriggerHandler --- HEP_Foxipedia_Synopsis_Publish_c Object trigger Handler
This will run everytime a HEP_Foxipedia_Synopsis_Publish_c record is inserted, updated, deleted or undeleted
*@author  Abhishek Mishra
*/
public Class HEP_Foxipedia_Synopsis_TriggerHandler extends TriggerHandler{    
    public static boolean run = true;
    /**
*Class Constructor to initialise class variables
*@return nothing
*/
    //public static Boolean executeTrigger;
    public HEP_Foxipedia_Synopsis_TriggerHandler() {
        
    }
    
    /**
* afterInsert -- Context specific afterinsert method
* @return nothing
*/
    public override void afterInsert() {
        /*System.debug('Data created in Synopsis...');
        System.assert(1==2 , Trigger.newMap);*/
        //HEP_Foxipedia_Synopsis_TriggerHandler.updateSynopsisRecord(JSON.serializePretty(Trigger.new[0]));

        if(run){
            sendSynopsisRecordForUpdate((List<HEP_Foxipedia_Synopsis_Publish__c>)Trigger.new);
            
        }
    }

    public override void afterUpdate(){
        if(run){
            sendSynopsisRecordForUpdate((List<HEP_Foxipedia_Synopsis_Publish__c>)Trigger.new);
            //run = false;
        }
        //sendSynopsisRecordForUpdate((List<HEP_Foxipedia_Synopsis_Publish__c>)Trigger.new);
    }

    public void sendSynopsisRecordForUpdate(List<HEP_Foxipedia_Synopsis_Publish__c> lstSynopsis){
        
        Set<Id> setSynopsiIds = new Set<Id>();
        Map<String , HEP_Foxipedia_Synopsis_Publish__c> mapMediaCodeToSynopsis = new Map<String , HEP_Foxipedia_Synopsis_Publish__c>();
        Map<String , String> mapMediaCodeToFederationId = new Map<String , String>();
        for(HEP_Foxipedia_Synopsis_Publish__c objSynopsisRec : lstSynopsis){
            if(String.isNotBlank(objSynopsisRec.Synopsis_Type__c) && String.isNotBlank(objSynopsisRec.Media_Code__c) 
                && objSynopsisRec.FOX_ID__c != NULL && String.isNotBlank(objSynopsisRec.Synopsis_Text__c)){
                mapMediaCodeToSynopsis.put(objSynopsisRec.Media_Code__c + '-' + objSynopsisRec.Synopsis_Type__c , objSynopsisRec);
                setSynopsiIds.add(objSynopsisRec.Id);
            }
        }

        Map<Id , HEP_Foxipedia_Synopsis_Publish__c> mapSynopsis = new Map<Id , HEP_Foxipedia_Synopsis_Publish__c>([SELECT Id, LastModifiedById , LastModifiedBy.FederationIdentifier, Media_Code__c , Synopsis_Type__c
                                                                                                                   FROM HEP_Foxipedia_Synopsis_Publish__c
                                                                                                                   WHERE Id in: setSynopsiIds]);

        for(HEP_Foxipedia_Synopsis_Publish__c objSynopsisRec : mapSynopsis.values()){
            mapMediaCodeToFederationId.put(objSynopsisRec.Media_Code__c + '-' + objSynopsisRec.Synopsis_Type__c , objSynopsisRec.LastModifiedBy.FederationIdentifier);
        }

        System.debug('mapMediaCodeToSynopsis ----> ' + mapMediaCodeToSynopsis);

        System.debug('mapMediaCodeToFederationId ----> ' + mapMediaCodeToFederationId);

        //Check for EST LONG
        if(mapMediaCodeToSynopsis.containsKey('EST-longText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('EST-longText')) , 'EST' , mapMediaCodeToFederationId.get('EST-longText'));
        }else if(mapMediaCodeToSynopsis.containsKey('EST-LongPreOrderText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('EST-LongPreOrderText')) , 'EST' , mapMediaCodeToFederationId.get('EST-LongPreOrderText'));
        }

        //Check for EST SHORT
        if(mapMediaCodeToSynopsis.containsKey('EST-ShortText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('EST-ShortText')) , 'EST' , mapMediaCodeToFederationId.get('EST-ShortText'));
        }else if(mapMediaCodeToSynopsis.containsKey('EST-ShortPreOrderText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('EST-ShortPreOrderText')) , 'EST' , mapMediaCodeToFederationId.get('EST-ShortPreOrderText'));
        }
        //Check for VID Long
        if(mapMediaCodeToSynopsis.containsKey('VID-longText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('VID-longText')) , 'VID' , mapMediaCodeToFederationId.get('VID-longText'));
        }else if(mapMediaCodeToSynopsis.containsKey('VID-LongPreOrderText')){
            updateSynopsisRecord(JSON.serialize(mapMediaCodeToSynopsis.get('VID-LongPreOrderText')) , 'VID' , mapMediaCodeToFederationId.get('VID-LongPreOrderText'));
        }
    }

    public static EMD_Synopsis.Attributes generateRequestWrapper(String sSynopsisRowId , 
                                                String sRowIdVersion , 
                                                String sMediaCode , 
                                                String sSynopsisTypeCode , 
                                                String sSynopsisText , 
                                                String sLastModifiedFederationId){

        EMD_Synopsis.Attributes objAttributes = new EMD_Synopsis.Attributes();
        if(String.isNotBlank(sSynopsisRowId)){
            objAttributes.rowIdObject = sSynopsisRowId;
        }
        objAttributes.rowIdTitleVersion = sRowIdVersion.replaceAll('\"','');
        objAttributes.synopsisTypeCode = sSynopsisTypeCode;
        objAttributes.countryCode = 'US';
        objAttributes.languageCode = 'ENG';
        objAttributes.mediaCode = sMediaCode;
        objAttributes.synopsisText = sSynopsisText;
        objAttributes.userId = sLastModifiedFederationId;

        return objAttributes;
    } 

    @future(callout = true)
    public static void updateSynopsisRecord(String sSynopsisRec , String sSynopsisTypeCode, String sFederationId){
        run = false;
        Map<String , String> mapMediaType = new Map<String , String>{'ShortPreOrderText' => 'SHRT',
                                                                     'LongPreOrderText' => 'LNG',
                                                                     'ShortText' => 'SHRT',
                                                                     'longText' => 'LNG',
                                                                     'LongPreOrderText' => 'LNG',
                                                                     'longText' => 'LNG'};
        //HEP_Foxipedia_Synopsis_Publish__c objEDMSynopsis = (HEP_Foxipedia_Synopsis_Publish__c) JSON.deserialize(sSynopsisRec, HEP_Foxipedia_Synopsis_Publish__c.class);
        

        HEP_Foxipedia_Synopsis_Publish__c objCurrentSynopsisRecord = (HEP_Foxipedia_Synopsis_Publish__c) JSON.deserialize(sSynopsisRec , HEP_Foxipedia_Synopsis_Publish__c.class);
        Decimal sFoxIdForGet = objCurrentSynopsisRecord.FOX_ID__c;
        System.debug('1. Changed Synopsis Record : ' + objCurrentSynopsisRecord);
        System.debug('1.1 sSynopsisTypeCode : ' + sSynopsisTypeCode);
        
        //Get request to get the rowIdVersion.
        String sRowIdVersion = fetchKeyInformation(String.valueOf(sFoxIdForGet));
        System.debug(' Fetched Row Id Version ----> ' + sRowIdVersion);
        
        //PUSH request for updating

        //Creating Body Dynamically...
        String sSynopsisRowId = (String.isNotBlank(objCurrentSynopsisRecord.Synopsis_Row_Id__c) ? objCurrentSynopsisRecord.Synopsis_Row_Id__c : null);



        //EMD_Synopsis.Attributes objAttributes = new EMD_Synopsis.Attributes();
        EMD_Synopsis.Attributes objAttributes = generateRequestWrapper(sSynopsisRowId,
                                                                       sRowIdVersion,
                                                                       sSynopsisTypeCode,
                                                                       mapMediaType.get(objCurrentSynopsisRecord.Synopsis_Type__c),
                                                                       objCurrentSynopsisRecord.Synopsis_Text__c,
                                                                       sFederationId
                                                                       );

        System.debug('objAttributes ------> ' + objAttributes);
        //JSON to be sent as Body.
        //sSynopsisRec
        HEP_Synopsis.HEP_SynopsisWrapper objSynopsisWrapper = new HEP_Synopsis.HEP_SynopsisWrapper(objAttributes , objCurrentSynopsisRecord);
        HEP_InterfaceTxnResponse objPOIntegration = HEP_ExecuteIntegration.executeIntegMethod(JSON.serialize(objSynopsisWrapper), '', 'HEP_Synopsis');

    }



    /**
* fetchKeyInformation -- Method to get the access token and the rowIdVersion
* @return nothing
*/
    
    public static String fetchKeyInformation(String foxId){
        System.debug('Fox Id Received in the method -----> ' + Integer.valueOf(foxId));
        EMD_Title_KeyInformation_Wrapper wrapObj = new EMD_Title_KeyInformation_Wrapper();
        EMD_Utilities.Message message;
        
        //Step 1: Get The Token
        EMD_RestService_Authentication_Util.AccessTokenWrapper accessTokenObj = EMD_RestService_Authentication_Util.restCalloutDetails('TitleKeyInformation');
        System.debug('accessTokenObj ---> ' + accessTokenObj);
        //Step 2 : Verify if Token is received and the Proceed.
        if(null != accessTokenObj.message &&
           String.isNotBlank(accessTokenObj.message.messageType) &&
           accessTokenObj.message.messageType.equalsIgnoreCase('Success') &&
           null != accessTokenObj.sandbox_url && null != accessTokenObj.service_url){
               
               HttpRequest req = new HttpRequest(); 
               req.setMethod('GET');
               req.setEndpoint(accessTokenObj.sandbox_url+accessTokenObj.service_url+'?foxId='+Integer.valueOf(foxId)); 
               req.setHeader(  'Authorization', 'Bearer ' + accessTokenObj.access_token);
               req.setHeader('system', System.Label.HEP_SYSTEM_FOXIPEDIA);
               req.setTimeout(20000);
               Http http = new Http();
               System.debug('Endpoint ---> ' + req.getEndpoint());

               try{
                   HTTPResponse res = http.send(req);
                   System.debug('res ----- -- -- > ' + res);
                   String responseBody = res.getBody();
                   System.debug('Getting the access token ----> ' + responseBody);
                   message = EMD_Service_utility.getResponse(res);
                   
                   if(message.messageType != 'Error'){
                       wrapObj = (EMD_Title_KeyInformation_Wrapper)Json.deserialize(responseBody , EMD_Title_KeyInformation_Wrapper.class);
                   }
    
               }
               catch(CalloutException excep){
                   message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , excep.getMessage());
               }
               catch(Exception excep){
                   message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , excep.getMessage());
               }
           }
        else{
            message = new EMD_Utilities.Message('Error', System.Label.EMD_Error_Message , 'Can not find custom Setting Entry for Authentication');
        }
        wrapObj.message = message;
        System.debug('Test Class ----> '  + wrapObj);
        //System.debug('returned value of access token -----> ' + JSON.serializePretty(wrapObj.data[0].attributes.rowIdVersion));
        return JSON.serializePretty(wrapObj.data[0].attributes.rowIdVersion);
        
    }
    

    
}