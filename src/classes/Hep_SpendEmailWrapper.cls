/**
    * EmailWrapper --- Class to hold data related fields displayed on email template
    * @author    Abinaya Elangovan
    */   
    public with sharing class Hep_SpendEmailWrapper{
        public String sEmailTitle {get;set;}
        public String sPromoName {get; set;}
        public String sPromoCode {get; set;}
        public String sReqDate {get; set;}
        public String sPromoFAD {get;set;}
        public String sBrandManager{get;set;}
        public String sComments{get;set;}
        public String sRejReason{get;set;}
        
    }