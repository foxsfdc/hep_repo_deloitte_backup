/**
 * HEP_Approval_Utility ---  Apex class to process Inbound emails for Dating Records
 * @author    Nishit Kedia
 */
public class HEP_Approval_Utility {


    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author Nishit Kedia
     */
    public HEP_Approval_Utility() {

    }
    
    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }

    /**
     * fetchApproversToNotify --- Method to fetch set of Approvers tagged to Approval Record.
     * @param mapUserRolesToNotify is the map of User Role Search_Id__c tagged to set of approval record IDs.
     * @return mapApproversToNotify is the map of ApprovalIds tagged to the set of notifiers.
     * @author  Gaurav Mehrishi
     */
    public static Map < Id, Set < Id >> fetchApproversToNotify(Map < String, Set < Id >> mapUserRolesToNotify) {


        //Approval Id is Key and User id are the notifiers
        Map < Id, Set < Id >> mapApproversToNotify = new Map < Id, Set < Id >> ();
    
        System.debug('Search id key Set in Approval Utility-------->' + mapUserRolesToNotify.keySet());
        //Loop in user role object
        for (HEP_User_Role__c objUserRole: [Select  Id,
                                                    Role__c,                                    
                                                    Search_Id__c,
                                                    Territory__c,
                                                    User__c
                                                    FROM
                                                    HEP_User_Role__c
                                                    WHERE
                                                    Search_Id__c IN: mapUserRolesToNotify.keySet()
                                                    AND Record_Status__c =:sACTIVE 
                                                    ]) {
            Id userId = objUserRole.User__c;
            String sSearchId = objUserRole.Search_Id__c;
            if (mapUserRolesToNotify.containsKey(sSearchId)) {
                //Set of Approval Ids
                Set < Id > setApprovalIds = mapUserRolesToNotify.get(sSearchId);
                for (Id ApprId: setApprovalIds) {
                    if (mapApproversToNotify.containsKey(ApprId)) {
                        //Insert the Map with Approval Id and Set of User Ids
                        mapApproversToNotify.get(ApprId).add(userId);
                    } else {
                        //Insert the Map with Approval Id and Set of User Ids
                        mapApproversToNotify.put(ApprId, new set < Id > {userId});
                    }

                }
            }

        }
        return mapApproversToNotify;
    }

    /**
     * queueRecordsToApproval --- Creation of records in HEP_Approvals Object.
     * @param recordForApprovalId is the Parent id which invoked for Approval
     * @param approvalTypeId is the type of Approval identified from Approval Process
     * @param mapApproversInSequence is the map with (Key->Approver User Id, Value-->Sequence Number of Approver)
     * @return nothing
     * @author    Nishit Kedia
     */
    public static void queueRecordsToApproval(Id recordForApprovalId, Id approvalTypeId, Map < Id, Integer > mapApproversInSequence, String sterritoryId) {
    
    Savepoint objSavepoint = Database.setSavepoint();
        try {
            // Fetch constant from HEP_Constant custom setting
            String sPendingStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');           

            //Null checking for Map before Invoking Logic
            if (mapApproversInSequence != null && !mapApproversInSequence.isEmpty() && recordForApprovalId != null && approvalTypeId != null) {

                //make a Set of Approvers which contains the Key->Approver User Id            
                Set < Id > setApprovers = mapApproversInSequence.keySet();

                //SOQL to retreive the Approval Record pre-Existing in Approval Object with same recordForApprovalId
                 Integer iCheckExistingApprovalRecord = [select   Count()
                                          from HEP_Approvals__c
                                          where Approval_Type__c =: approvalTypeId
                                          and Record_ID__c =: recordForApprovalId
                                          and Approval_Status__c =: sPendingStatus
                                          AND Record_Status__c =:sACTIVE 
                                          
                                      ];

                System.debug('list of Pre-Existing Approvals-->' + iCheckExistingApprovalRecord);

                //[Database Sanity Check]:If there is No record found then Only Invoke logic for Approval record Creation    
                if (iCheckExistingApprovalRecord == 0) {

                    //Instantiante an Approval object Entity for Insertion          
                    HEP_Approvals__c objApprovalRecord = new HEP_Approvals__c();
                    objApprovalRecord.Approval_Type__c = approvalTypeId;
                    objApprovalRecord.Record_ID__c = recordForApprovalId;
                    objApprovalRecord.Approval_Status__c = sPendingStatus;

                    //Fetch the lookup Api to link to parent Object which invoked for Approval
                    String sDynamicLookupFieldApi = recordForApprovalId.getSObjectType().getDescribe().getName();

                    // Instantiate Sobject reference to dynamically insert Lookup field in Approval Object lookup Field
                    Sobject objApprovalGenericSobject = objApprovalRecord;
                    objApprovalGenericSobject.put(sDynamicLookupFieldApi, recordForApprovalId);

                    //Insert the Approval Record                    
                    Database.SaveResult objSaveResult = Database.insert(objApprovalGenericSobject, false);

                    //List Instantiated for record Approvers that will be Inserted for Above Approval Record
                    List < HEP_Record_Approver__c > lstRecordApproversToInsert = new List < HEP_Record_Approver__c > ();

                    //If update is Successful Update parent Record on which Approval was fired and Create Approvers in Record Approvers                                                          
                    if (objSaveResult.isSuccess()) {

                        //When record is Inserted Successfully (Always 1 Approval Record)
                        List < HEP_Approvals__c > lstApprovalRecordInserted = [Select Id,
                            Approval_Type__r.Approval_Field__c,
                            Approval_Type__r.In_Progress_Status__c,
                            Approval_Type__r.Approval_Object__c
                            from HEP_Approvals__c
                            where Id =: objSaveResult.getId()
                            AND Record_Status__c =:sACTIVE 
                            LIMIT 1
                        ];

                        if (lstApprovalRecordInserted != null && !lstApprovalRecordInserted.isEmpty()) {

                            //fetch the Approval Object Record
                            HEP_Approvals__c objApprovalRecordInserted = lstApprovalRecordInserted.get(0);
                            //Update the Parent Object status on which approval was actually triggered
                            System.debug('Approval Record Inserted-->' + objApprovalRecordInserted);
                            //Insatntiate Dynamic Sobject based on Parent record Id
                             if(objApprovalRecordInserted.Approval_Type__r != null && String.isNotBlank(objApprovalRecordInserted.Approval_Type__r.Approval_Field__c) && String.isNotBlank(objApprovalRecordInserted.Approval_Type__r.In_Progress_Status__c)){
                              sObject objParentApproval = recordForApprovalId.getSobjectType().newSObject(recordForApprovalId);
                              objParentApproval.put(objApprovalRecordInserted.Approval_Type__r.Approval_Field__c, objApprovalRecordInserted.Approval_Type__r.In_Progress_Status__c);
                              //Update parent Approval
                              update objParentApproval;
                             }

                            // populate record Approvers for Insertion  
                            Integer iSequenceCount = 1;                                              
                            for (Id roleId: mapApproversInSequence.keyset()) {
                                //Instantiate record Approvers                                                                                     
                                HEP_Record_Approver__c objRecordApprover = new HEP_Record_Approver__c();
                                //Stamp the Approval Lookup id
                                objRecordApprover.Approval_Record__c = objSaveResult.getId();
                                //Stamp the Approver with User Id 
                                //objRecordApprover.Approver__c = userId;

                                //Stamp the Approver with Role Id 
                                objRecordApprover.Approver_Role__c = roleId;
                                //Stamp the Search Id
                                objRecordApprover.Search_Id__c = roleId + ' / ' + sterritoryId;
                                
                                if (mapApproversInSequence.get(roleId) != null) {
                                    objRecordApprover.Sequence__c = mapApproversInSequence.get(roleId);
                                    //To make the 1st Record Approver as currently Assigned for Approval
                                    if (iSequenceCount == 1) {
                                        objRecordApprover.IsActive__c = true;
                                    }
                                    iSequenceCount++;
                                }
                                objRecordApprover.Status__c = sPendingStatus;
                                lstRecordApproversToInsert.add(objRecordApprover);
                            }
                            //Insert Approvers for the Approval Record and hereafter Trigger gets fired.
                            if (lstRecordApproversToInsert != NULL && lstRecordApproversToInsert.size() > 0) {
                                insert lstRecordApproversToInsert;
                                system.debug('lstRecordApproversToInsert ' + lstRecordApproversToInsert);
                            }
                        }
                    }
                } else {
                    system.debug('Approval record for this record ID is already pending for approval');
                }
            }
        } catch (Exception objException) {
        	
          	Database.rollback( objSavepoint );
          	HEP_Error_Log.genericException('Exception occured when creating Approval Records','DML Errors',objException,'HEP_Approval_Utility','queueRecordsToApproval',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            throw objException;
        }
    }


    //********************Approval Role Hierarchy Changes [Starts]********************************//
    /**
     * queueRecordsToApproval --- Creation of records in HEP_Approvals Object.
     * @param recordForApprovalId is the Parent id which invoked for Approval
     * @param approvalTypeId is the type of Approval identified from Approval Process
     * @param mapApproversInSequence is the map with (Key->Approver User Id, Value-->Sequence Number of Approver)
     * @return nothing
     * @author    Nishit Kedia
     */
    public static void queueRecordsToApproval(Id approvalTypeId, Map < Id, Integer > mapApproversInSequence, HEP_ApprovalInputWrapper  objInputApprovalApi) {
    
    Savepoint objSavepoint = Database.setSavepoint();
        try {
            // Fetch constant from HEP_Constant custom setting
            String sPendingStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
      Id recordForApprovalId = Id.valueOf(objInputApprovalApi.sApprovalRecordId);
            //Null checking for Map before Invoking Logic
            if (mapApproversInSequence != null && !mapApproversInSequence.isEmpty() && recordForApprovalId != null && approvalTypeId != null) {

                //make a Set of Approvers which contains the Key->Approver User Id            
                Set < Id > setApprovers = mapApproversInSequence.keySet();

                //SOQL to retreive the Approval Record pre-Existing in Approval Object with same recordForApprovalId
                Integer iCheckExistingApprovalRecord = [select   Count()
                                          from HEP_Approvals__c
                                          where Approval_Type__c =: approvalTypeId
                                          and Record_ID__c =: recordForApprovalId
                                          and Approval_Status__c =: sPendingStatus
                                          AND Record_Status__c =:sACTIVE 
                                      ];

                System.debug('list of Pre-Existing Approvals-->' + iCheckExistingApprovalRecord);

                //[Database Sanity Check]:If there is No record found then Only Invoke logic for Approval record Creation    
                if (iCheckExistingApprovalRecord == 0) {

                    //Instantiante an Approval object Entity for Insertion          
                    HEP_Approvals__c objApprovalRecord = new HEP_Approvals__c();
                    objApprovalRecord.Approval_Type__c = approvalTypeId;
                    objApprovalRecord.Record_ID__c = recordForApprovalId;
                    objApprovalRecord.Approval_Status__c = sPendingStatus;
                    objApprovalRecord.Prior_Submitter__c = objInputApprovalApi.sPriorSubmittersUserIds;
                    objApprovalRecord.Comments__c = objInputApprovalApi.sRequesterComment;

                    //Fetch the lookup Api to link to parent Object which invoked for Approval
                    String sDynamicLookupFieldApi = recordForApprovalId.getSObjectType().getDescribe().getName();

                    // Instantiate Sobject reference to dynamically insert Lookup field in Approval Object lookup Field
                    Sobject objApprovalGenericSobject = objApprovalRecord;
                    objApprovalGenericSobject.put(sDynamicLookupFieldApi, recordForApprovalId);

                    //Insert the Approval Record                    
                    Database.SaveResult objSaveResult = Database.insert(objApprovalGenericSobject, false);

                    //List Instantiated for record Approvers that will be Inserted for Above Approval Record
                    List < HEP_Record_Approver__c > lstRecordApproversToInsert = new List < HEP_Record_Approver__c > ();

                    //If update is Successful Update parent Record on which Approval was fired and Create Approvers in Record Approvers                                                          
                    if (objSaveResult.isSuccess()) {

                        //When record is Inserted Successfully (Always 1 Approval Record)
                        List < HEP_Approvals__c > lstApprovalRecordInserted = [Select Id,
                            Approval_Type__r.Approval_Field__c,
                            Approval_Type__r.In_Progress_Status__c,
                            Approval_Type__r.Approval_Object__c
                            from HEP_Approvals__c
                            where Id =: objSaveResult.getId()
                            LIMIT 1
                        ];

                        if (lstApprovalRecordInserted != null && !lstApprovalRecordInserted.isEmpty()) {

                            //fetch the Approval Object Record
                            HEP_Approvals__c objApprovalRecordInserted = lstApprovalRecordInserted.get(0);
                            //Update the Parent Object status on which approval was actually triggered
                            System.debug('Approval Record Inserted-->' + objApprovalRecordInserted);
                            //Insatntiate Dynamic Sobject based on Parent record Id
                            if(objApprovalRecordInserted.Approval_Type__r != null && String.isNotBlank(objApprovalRecordInserted.Approval_Type__r.Approval_Field__c) && String.isNotBlank(objApprovalRecordInserted.Approval_Type__r.In_Progress_Status__c)){
                              sObject objParentApproval = recordForApprovalId.getSobjectType().newSObject(recordForApprovalId);
                              objParentApproval.put(objApprovalRecordInserted.Approval_Type__r.Approval_Field__c, objApprovalRecordInserted.Approval_Type__r.In_Progress_Status__c);
                              //Update parent Approval
                              update objParentApproval;
                            }

                            // populate record Approvers for Insertion  
                            Integer iSequenceCount = 1;                                              
                            for (Id roleId: mapApproversInSequence.keyset()) {
                                //Instantiate record Approvers                                                                                     
                                HEP_Record_Approver__c objRecordApprover = new HEP_Record_Approver__c();
                                //Stamp the Approval Lookup id
                                objRecordApprover.Approval_Record__c = objSaveResult.getId();
                                //Stamp the Approver with User Id 
                                //objRecordApprover.Approver__c = userId;

                                //Stamp the Approver with Role Id 
                                objRecordApprover.Approver_Role__c = roleId;
                                //Stamp the Search Id
                                objRecordApprover.Search_Id__c = roleId + ' / ' + objInputApprovalApi.sterritoryId;

                                
                                if (mapApproversInSequence.get(roleId) != null) {
                                    objRecordApprover.Sequence__c = mapApproversInSequence.get(roleId);
                                    //To make the 1st Record Approver as currently Assigned for Approval
                                    if (iSequenceCount == 1) {
                                        objRecordApprover.IsActive__c = true;
                                    }
                                    iSequenceCount++;
                                }
                                objRecordApprover.Status__c = sPendingStatus;
                                lstRecordApproversToInsert.add(objRecordApprover);
                            }
                            //Insert Approvers for the Approval Record and hereafter Trigger gets fired.
                            if (lstRecordApproversToInsert != NULL && lstRecordApproversToInsert.size() > 0) {
                                insert lstRecordApproversToInsert;
                                system.debug('lstRecordApproversToInsert ' + lstRecordApproversToInsert);
                            }
                        }
                    }
                } else {
                    system.debug('Approval record for this record ID is already pending for approval');
                }
            }

        }catch (Exception objException) {
        	
          	Database.rollback( objSavepoint );
          	HEP_Error_Log.genericException('Exception occured when creating Approval Records For Role Hierarchy','DML Errors',objException,'HEP_Approval_Utility','queueRecordsToApproval',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            throw objException;
        }
    }
    //********************Approval Role Hierarchy Changes [Ends]**********************************//
    
     /**
     * fetchGlobalEmailAddress --- To fetch the email Address for Inbound Emails to SFDC
     * @param sEmailServiceName is the String with Email Address name
     * @return Email Address for the Service
     * @author  Nishit Kedia
     */
    public static String fetchInboundEmailAddress(String sEmailServiceName) {
      String sHEPInboundEmailAddress = '';
      if(String.isNotBlank(sEmailServiceName)){
        EmailServicesAddress objEmailAddress = [SELECT   Id,
                                Function.FunctionName,
                                LocalPart,
                              EmailDomainName,
                              FunctionId
                              FROM EmailServicesAddress where Function.FunctionName = :sEmailServiceName
                              LIMIT 1
                          ];
      if(objEmailAddress != null){
        String sLocalPart = objEmailAddress.LocalPart;
        String sDomainPart = objEmailAddress.EmailDomainName;
        sHEPInboundEmailAddress = sLocalPart +'@'+ sDomainPart;
        }
    
      }     
      System.debug('Inbound Email---> ' + sHEPInboundEmailAddress);
      return  sHEPInboundEmailAddress;
    }
    
    /**
     * autoApproval --- To auto-approve from the Framework
     * @param sParentRecordId is the RecordId of the Parent Approval Record
     * @param sApprovalFieldApi is the Field Api Name of the Parent Record
     * @param sApprovedValue is the value to be put in field
     * @return nothing
     * @author  Nishit Kedia
     */
      public static void autoApproval(Id sParentRecordId, String sApprovalFieldApi, String sApprovedValue, String sApprovalStatus, String sApprovalTypeId){
        try{
        	if(sParentRecordId != null && String.isNotBlank(sApprovalFieldApi) && String.isNotBlank(sApprovedValue)){
	            sObject objParentApproval = sParentRecordId.getSobjectType().newSObject(sParentRecordId);
	            objParentApproval.put(sApprovalFieldApi, sApprovedValue);   
	            update objParentApproval;
	            
	            //Create a Dummy Approval record And make the status Approved/Rejected.
	             HEP_Approvals__c objAutoApprovalRecord = new  HEP_Approvals__c();
	             objAutoApprovalRecord.Record_ID__c = sParentRecordId;
	             objAutoApprovalRecord.Approval_Status__c = sApprovalStatus;
	             objAutoApprovalRecord.Approval_Type__c	 = sApprovalTypeId;
	             String objParentApprovalField = sParentRecordId.getSObjectType().getDescribe().getName();
	             objAutoApprovalRecord.put(objParentApprovalField,sParentRecordId);
	             insert objAutoApprovalRecord;
        	}
        } catch (Exception objException) {
	      
	          HEP_Error_Log.genericException('Exception occured when Updating parent Status','DML Errors',objException,'HEP_Approval_Utility','autoApproval',null,null); 
	          System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
	          throw objException;
        }                             
     }
}