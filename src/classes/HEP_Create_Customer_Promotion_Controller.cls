/**
* HEP_Create_Customer_Promotion_Controller --- Class for the Customer Promotion Set-Up Page
* @author    Ayan Sarkar
*/
public class HEP_Create_Customer_Promotion_Controller {

    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    } 
    
    /**
    * Picklists --- Class to hold all the picklist field values 
    * @author    Ayan Sarkar
    */
    public class Picklists{
        public LIST<TerritoryWrapper> lstTerritory;
        public LIST<HEP_Utility.PicklistWrapper> lstPromoType;
        /**
        * Picklists Contructor --- Constructor to initialize the List vairables
        * @return   N/A
        */
        public Picklists(){
            lstTerritory = new LIST<TerritoryWrapper>();
            lstPromoType = new LIST<HEP_Utility.PicklistWrapper>();
        }
    }
    
    /* Helps to check if the current user has access to HEP_Create_Natinal_Promotion.Page
     * @return Map of Permission Name and its value 
     */
    public PageReference checkPermissions(){
        String sTerritoryQueryWhereClause = ' WHERE User__c = \''+UserInfo.getUserId() + '\'  AND Territory__r.Name != \'Global\' AND Role__r.Name = \'Digital Account Manager\' AND Write__c = TRUE AND Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\' ORDER BY Sequence__c ASC NULLS LAST ';
        String sTerritoryQuery = HEP_Utility.buildQueryAllString('HEP_User_Role__c','SELECT Territory__r.Name, ', sTerritoryQueryWhereClause);
        LIST<HEP_User_Role__c> lstTerritoriesRecs = (LIST<HEP_User_Role__c>)Database.query(sTerritoryQuery);

        if(lstTerritoriesRecs == NULL || lstTerritoriesRecs.isEmpty()){
            //System.assert(1==2 , lstTerritoriesRecs);
            return HEP_Utility.redirectToErrorPage();
        }
        else
            return null;
    }

    /**
    * TerritoryWrapper --- Class to hold data related to the TerritoryWrapper that we select on the Screen. 
    * @author    Ayan Sarkar
    */
    public class TerritoryWrapper{
        public String sKey;
        public String sValue;
        public LIST<HEP_Utility.MultiPicklistWrapper> lstRegions;
        public LIST<HEP_Utility.PicklistWrapper> lstCustomers;
        public TerritoryWrapper(){}
        /**
        * TerritoryWrapper Contructor --- Constructor to initialize the vairables
        * @param  sKey Salesforce id
        * @param  sValue Name field
        * @return   N/A
        */
        public TerritoryWrapper(String sKey, String sValue){
            this.sKey = sKey;
            this.sValue = sValue;
            lstRegions = new LIST<HEP_Utility.MultiPicklistWrapper>();
            lstCustomers = new LIST<HEP_Utility.PicklistWrapper>();
        }
    }
    
     
    /**
    * CustomerPromotionPageData --- Class to represent single instance of a Customer Promotion
    * @author    Ayan Sarkar
    */
    public class CustomerPromotionPageData{ 
        public String sName;
        public HEP_Utility.PicklistWrapper objCustomer; 
        public String sStartDate;
        public String sEndDate;
        public Date dtStartDate;
        public Date dtEndDate;
        public TerritoryWrapper objTerritory;
        public HEP_Utility.PicklistWrapper objRequestor;
        public HEP_Utility.PicklistWrapper objPromoType;
        public String sNotes;
        public String sUserLocale;
        public String sDateFormat;
        public String sExpenditedApproval;      
        public Picklists objPicklist;
        public Map<String, String> mapErrorMessages;
        /**
        * CustomerPromotionPageData Contructor --- Constructor to initialize the vairables
        * @return   N/A
        */
        public CustomerPromotionPageData(){
            mapErrorMessages = HEP_Utility.getAllCreatePromotionErrorMessages();
        }
    }
    
    /**
     * Helps to initialize the initial data loaded on the screen
     * @return CustomerPromotionPageData Wrapper Class Object.
     */
    @RemoteAction
    public static CustomerPromotionPageData fetchCustomerPromotionDefaultValues(){
        CustomerPromotionPageData custPromotionPageData = new CustomerPromotionPageData();
        custPromotionPageData.sName = '';
        custPromotionPageData.objCustomer = new HEP_Utility.PicklistWrapper('','');
        custPromotionPageData.sStartDate = '';
        custPromotionPageData.sEndDate = '';
        custPromotionPageData.objRequestor = new HEP_Utility.PicklistWrapper(UserInfo.getUserId(),UserInfo.getName());
        custPromotionPageData.objTerritory = new TerritoryWrapper();
        custPromotionPageData.sExpenditedApproval = '';
        custPromotionPageData.sNotes = '';
        custPromotionPageData.sDateFormat = HEP_Utility.getDateTimeLocaleFormat(true, false);
        custPromotionPageData.sUserLocale = UserInfo.getLocale();
        custPromotionPageData.objPicklist = new Picklists();
        
        try{
        //Pre-Populating the Picklist Values.
        //1. Territory Picklist Values..
        String sTerritoryQueryWhereClause = ' WHERE User__c = \''+UserInfo.getUserId() + '\'  AND Territory__r.Name != \'Global\' AND Territory__r.Parent_Territory__c = null AND Role__r.Name = \'Digital Account Manager\' AND Write__c = TRUE AND Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\' ORDER BY Sequence__c ASC NULLS LAST ';
        String sTerritoryQuery = HEP_Utility.buildQueryAllString('HEP_User_Role__c','SELECT Territory__r.Name, ', sTerritoryQueryWhereClause);
        LIST<HEP_User_Role__c> lstTerritoriesRecs = (LIST<HEP_User_Role__c>)Database.query(sTerritoryQuery);
        Map<String, TerritoryWrapper> mapTerritory = new Map<String, TerritoryWrapper>(); 
        for(HEP_User_Role__c userTerritory : lstTerritoriesRecs){
            if(userTerritory.Write__c){
                TerritoryWrapper territory = new TerritoryWrapper(userTerritory.Territory__c, userTerritory.Territory__r.Name);
                mapTerritory.put(userTerritory.Territory__r.Name, territory);
            }
        }


        custPromotionPageData.objPicklist.lstTerritory.addAll(mapTerritory.values());

        //2. Promot Type Values..
        String sPromoWhereClause = ' WHERE Type__c = \'HEP_Customer_Promotion_Type\' AND Record_Status__c = \'' + sACTIVE + '\'';
        String sPromoTypeQuery = HEP_Utility.buildQueryAllString('HEP_List_Of_Values__c' , null , sPromoWhereClause);

        List<HEP_List_Of_Values__c> lstListOfValues = Database.query(sPromoTypeQuery);

        for(HEP_List_Of_Values__c objListOfValues : lstListOfValues){
            custPromotionPageData.objPicklist.lstPromoType.add(new HEP_Utility.PicklistWrapper(objListOfValues.Values__c , objListOfValues.Values__c));
        }

        }catch(Exception e){
            throw e;
        }
        
        
        return custPromotionPageData;
    }   
    
    /**
     * Helps to save data to Data Base.
     * @param  objPageData Wrapper class having all the required values to be updated in the Database
     * @return String 
     */
    @RemoteAction
    public static String saveCustomerPromotion(CustomerPromotionPageData objPageData){
        HEP_Promotion__c objPromotion;
        String sToReturn = null;
        Map<String, String> mapCustomPermissions = HEP_Utility.fetchCustomPermissions(null, 'HEP_Create_Customer_Promotion');
        try{
              if(!HEP_Security_Framework.getMultipleTerritoryAccess().get(objPageData.objTerritory.sValue)){
                HEP_Utility.redirectToErrorPage();
            }
            /*if(mapCustomPermissions == NULL || mapCustomPermissions.isEmpty() || !mapCustomPermissions.containsKey('Save')){
                HEP_Utility.redirectToErrorPage();
                System.debug('!@#$%^ Error Happened : ');
            }*/else{
                //Create the record for Customer Promotion.
                objPromotion = new HEP_Promotion__c();
                objPromotion.PromotionName__c = objPageData.sName;
                objPromotion.Customer__c = String.isNotBlank(objPageData.objCustomer.sKey) ? objPageData.objCustomer.sKey : null;
                objPromotion.StartDate__c =  objPageData.dtStartDate != null ? objPageData.dtStartDate : null;
                objPromotion.EndDate__c =  objPageData.dtEndDate != null ? objPageData.dtEndDate : null;
                objPromotion.Requestor__c = UserInfo.getUserId();
                objPromotion.Territory__c = objPageData.objTerritory.sKey;
                objPromotion.Priority__c = objPageData.sExpenditedApproval.equalsIgnoreCase('Yes') ? true : false;
                objPromotion.CustomerNotes__c = objPageData.sNotes;
                objPromotion.Promotion_Type__c = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
                objPromotion.Promo_Type__c = objPageData.objPromoType.sKey;
                objPromotion.Status__c = 'Draft';
                //Insert the Promotion
                insert objPromotion;
                LIST<HEP_Promotion_Region__c> lstRegionsToInsert = new LIST<HEP_Promotion_Region__c>(); 
                //Code to create Promotion Region Records.
                for(HEP_Utility.MultiPicklistWrapper objRegion : objPageData.objTerritory.lstRegions){
                    if(objRegion.bSelected){
                        lstRegionsToInsert.add(new HEP_Promotion_Region__c(Parent_Promotion__c = objPromotion.Id, Territory__c = objRegion.sKey));
                    }
                }
                insert lstRegionsToInsert;
                sToReturn = objPromotion.Id;
            }
        }catch(Exception e){
            HEP_Error_Log.genericException('Error occured during Creation of Customer Promotion','VF Controller',e,'HEP_Create_Customer_Promotion','saveCustomerPromotion',null,'');
            throw e;
        }
        return objPromotion.Id;
    }
    
    /**
     * Helps to get List of Customers which are related to a territory
     * @param  territoryId - Salesforce Id of the TerritoryWrapper Selected.
     * @return List of Picklist Wrapper 
     */
    @RemoteAction
    public static LIST<HEP_Utility.PicklistWrapper> getCustomers(String sTerritoryId){
        LIST<HEP_Utility.PicklistWrapper> lstCustomers = new LIST<HEP_Utility.PicklistWrapper>();
        //Populate Customers for selected Territory.
        try{
            //Query to get all the Customer Names
            String sCustomerQuery = HEP_Utility.buildQueryAllString('HEP_Customer__c', null , 'WHERE Territory__c = \'' + sTerritoryId + '\' AND Record_Status__c = \'' + HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') + '\' AND Customer_Type__c = \'' + HEP_Utility.getConstantValue('HEP_CUSTOMER_TYPE_MDP') + '\'');
            LIST<HEP_Customer__c> lstCustomerRecs = (LIST<HEP_Customer__c>)Database.query(sCustomerQuery);
            System.debug('Query ----> ' + sCustomerQuery);
            System.debug('lstCustomerRecs ----> ' + lstCustomerRecs);
            //Update the JSON to have List of Customers
            for(HEP_Customer__c customer : lstCustomerRecs){
                HEP_Utility.PicklistWrapper objCustomer = new HEP_Utility.PicklistWrapper(customer.Id, customer.CustomerName__c);
                lstCustomers.add(objCustomer);
            }
        }catch(Exception e){
            HEP_Error_Log.genericException('Error occured during Creation of Customer Promotion','VF Controller',e,'HEP_Create_Customer_Promotion','getCustomers',sTerritoryId,'');

            throw e;
        }
        return lstCustomers;
    }
    
    
    /**
     * Helps to get all data related to Selected TerritoryWrapper
     * @param  sTerritoryId - Salesforce Id of the TerritoryWrapper Selected.
     * @param  sTerritoryId - Salesforce Id of the TerritoryWrapper Selected.
     * @return TerritoryWrapper Wrapper Object
     */
    @RemoteAction
    public static TerritoryWrapper getTerritoriesDefaultValues(String sTerritoryId, String sTerritoryName){
        Map<Id, TerritoryWrapper> territoryDataToReturn = new Map<Id, TerritoryWrapper>();
        TerritoryWrapper objTerritoryData;
        map<String,HEP_Utility.PicklistWrapper > mapCustomers = new map<String, HEP_Utility.PicklistWrapper>();
        try{
            //Pre-Populating all Customer related to this Territory.
            objTerritoryData = new TerritoryWrapper(sTerritoryId, sTerritoryName);
            for(HEP_Customer__c customer : [Select id, CustomerName__c,Parent_Customer__r.Territory__c, Parent_Customer__c, Parent_Customer__r.CustomerName__c , Territory__c FROM HEP_Customer__c WHERE Territory__c =: sTerritoryId AND Customer_Type__c =: 'MDP Child Customers']){
                mapCustomers.put(customer.Parent_Customer__c , new HEP_Utility.PicklistWrapper(customer.Parent_Customer__c ,customer.Parent_Customer__r.CustomerName__c));
            }
            objTerritoryData.lstCustomers = mapCustomers.values();
            //Query to fetch all the Territories that the Loggin User has access to.
            String sQueryRegions = HEP_Utility.buildQueryAllString('HEP_Territory__c', 'SELECT Parent_Territory__r.Name, ', 'WHERE Parent_Territory__c = \'' + sTerritoryId + '\' AND Parent_Territory__r.Name != \'Global\'');
            LIST<HEP_Territory__c> lstRegionsRecs = (LIST<HEP_Territory__c>)Database.query(sQueryRegions);
            
            //Loop for assigning the HEP_Territory__c values for Regions.
            //for(HEP_Territory__c terr : lstRegionsRecs){
            //  objTerritoryData.lstRegions.add(new HEP_Utility.MultiPicklistWrapper(terr.Name, terr.Id, false));
            //}
            //Code to add Regions for each parent Territory
            for(HEP_Territory__c objTerritoty : lstRegionsRecs){
                if(!sTerritoryName.equalsIgnoreCase('Germany'))
                    objTerritoryData.lstRegions.add(new HEP_Utility.MultiPicklistWrapper(objTerritoty.Name, objTerritoty.Id, false));
                else
                    objTerritoryData.lstRegions.add(new HEP_Utility.MultiPicklistWrapper(objTerritoty.Name, objTerritoty.Id, true));
            }
            if(lstRegionsRecs != NULL && !lstRegionsRecs.isEmpty()){
                if(!sTerritoryName.equalsIgnoreCase('DHE')){
                    if(sTerritoryName.equalsIgnoreCase('Germany'))
                    objTerritoryData.lstRegions.add(new HEP_Utility.MultiPicklistWrapper(sTerritoryName, sTerritoryId, true));
                    else
                    objTerritoryData.lstRegions.add(new HEP_Utility.MultiPicklistWrapper(sTerritoryName, sTerritoryId, false));
                }
            }
        }catch(Exception e){
            throw e;
            //Code to log error..
            HEP_Error_Log.genericException('Error occured during Creation of Customer Promotion','VF Controller',e,'HEP_Create_Customer_Promotion','getTerritoriesDefaultValues',sTerritoryId,'');
        }
        return objTerritoryData;
    }

    @RemoteAction
    public static LIST<HEP_Utility.PicklistWrapper> getRegionAssociatedCustomers1(List<String> lstRegionIds){
        system.debug('lstRegionIds--->'+lstRegionIds);
        Integer iNumberOfRegion = lstRegionIds.Size();
        LIST<HEP_Utility.PicklistWrapper> lstCustomersToReturn = new LIST<HEP_Utility.PicklistWrapper>(); 
        Map<String,Set<Id>> mapCustomerToRegion = new map<String,set<ID>>();
        map<String, String> mapCustomerId = new map<String, String>();
        
        for(HEP_Customer__c customer : [Select id, CustomerName__c, Territory__c FROM HEP_Customer__c WHERE Territory__c IN: lstRegionIds AND Customer_Type__c =: HEP_Utility.getConstantValue('HEP_CUSTOMER_TYPE_MDP') ]){
            // lstCustomersToReturn.add(customer.CustomerName__c);
            Set<Id> setCustomerRegion = new Set<Id>();
            if(mapCustomerToRegion.containsKey(customer.CustomerName__c)){
                setCustomerRegion = mapCustomerToRegion.get(customer.CustomerName__c);
            }
            setCustomerRegion.add(customer.Territory__c);
            mapCustomerToRegion.put(customer.CustomerName__c ,setCustomerRegion);
            mapCustomerId.put(customer.CustomerName__c , customer.id);
        } 
        system.debug('mapCustomerToRegion--->'+mapCustomerToRegion.size() );
        if(mapCustomerToRegion != null && mapCustomerToRegion.Size()>0){
            for(String customer : mapCustomerToRegion.keyset()){
                if(mapCustomerToRegion.get(customer).Size() == iNumberOfRegion){
                    system.debug('eher');
                    HEP_Utility.PicklistWrapper objCustomer = new HEP_Utility.PicklistWrapper(mapCustomerId.get(customer), customer);
                    lstCustomersToReturn.add(objCustomer);
                }
            }
        }
        return lstCustomersToReturn;
    }

    @RemoteAction
    public static LIST<HEP_Utility.PicklistWrapper> getRegionAssociatedCustomers(List<String> lstRegionIds){
        system.debug('lstRegionIds--->'+lstRegionIds);
        Integer iNumberOfRegion = lstRegionIds.Size();
        LIST<HEP_Utility.PicklistWrapper> lstCustomersToReturn = new LIST<HEP_Utility.PicklistWrapper>(); 
        Map<String,Set<Id>> mapCustomerToRegion = new map<String,set<ID>>();
        map<String, String> mapCustomerId = new map<String, String>();
        
        for(HEP_Customer__c customer : [Select id, CustomerName__c,Parent_Customer__r.Territory__c, Parent_Customer__c, Parent_Customer__r.CustomerName__c , Territory__c FROM HEP_Customer__c WHERE Territory__c IN: lstRegionIds AND Customer_Type__c =: 'MDP Child Customers']){
            Set<Id> setCustomerRegion = new Set<Id>();
            if(mapCustomerToRegion.containsKey(customer.Parent_Customer__r.CustomerName__c)){
                setCustomerRegion = mapCustomerToRegion.get(customer.Parent_Customer__r.CustomerName__c);
            }
            setCustomerRegion.add(customer.Territory__c);
            mapCustomerToRegion.put(customer.Parent_Customer__r.CustomerName__c ,setCustomerRegion);
            mapCustomerId.put(customer.Parent_Customer__r.CustomerName__c , customer.Parent_Customer__c);
        } 
        system.debug('mapCustomerToRegion--->'+mapCustomerToRegion.size() );
        if(mapCustomerToRegion != null && mapCustomerToRegion.Size()>0){
            for(String customer : mapCustomerToRegion.keyset()){
                if(mapCustomerToRegion.get(customer).Size() == iNumberOfRegion){
                    system.debug('eher');
                    HEP_Utility.PicklistWrapper objCustomer = new HEP_Utility.PicklistWrapper(mapCustomerId.get(customer) , customer);
                    lstCustomersToReturn.add(objCustomer);
                }
            }
        }
        return lstCustomersToReturn;
    }
}