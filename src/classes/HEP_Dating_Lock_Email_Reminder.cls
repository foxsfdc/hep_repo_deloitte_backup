/**
 * HEP_Dating_Lock_Email_Reminder --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_Dating_Lock_Email_Reminder
{
    //Promotion dating record id is passed in this variable
    public Id prDatingId {get; set;}
     
    //This variable is used to render table on Email template
    public ApprovalEmailWrapper objPrDatingWrap{ 
        get{
            if(objPrDatingWrap == null){
                fetchPrDatingDetails();
            }
            return objPrDatingWrap;
        }
        set;
    }
    
    /**
    * fetchPrDatingDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author Gaurav Mehrishi
    */ 
    public void fetchPrDatingDetails(){
        try{
            //Instantiate the Wrapper
            objPrDatingWrap = new ApprovalEmailWrapper();           
            if(prDatingId != null){
                //Query the Record based on Record Id               
                HEP_Promotion_Dating__c objPrDating = [SELECT Id, Promotion__r.PromotionName__c,
                                Promotion__r.Domestic_Marketing_Manager__r.Name , Promotion__r.International_Marketing_Manager__r.Name,
                                Promotion__r.FirstAvailableDate__c, Locked_Date__c,DateType__c
                                FROM HEP_Promotion_Dating__c
                                WHERE Id =: prDatingId];


                objPrDatingWrap.sPromotionName = objPrDating.Promotion__r.PromotionName__c;
                objPrDatingWrap.sDateType = objPrDating.DateType__c;
                DateTime dttFad = DateTime.newInstance(objPrDating.Promotion__r.FirstAvailableDate__c.year(), objPrDating.Promotion__r.FirstAvailableDate__c.month(), objPrDating.Promotion__r.FirstAvailableDate__c.day());
                objPrDatingWrap.sFAD = dttFad.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
                objPrDatingWrap.sDomesticMarketingManager = objPrDating.Promotion__r.Domestic_Marketing_Manager__r.Name;
                objPrDatingWrap.sInternationalMarketingManager = objPrDating.Promotion__r.International_Marketing_Manager__r.Name;
                DateTime dttLock = DateTime.newInstance(objPrDating.Locked_Date__c.year(), objPrDating.Locked_Date__c.month(), objPrDating.Locked_Date__c.day());
                objPrDatingWrap.sLockedDate = dttLock.format(HEP_Utility.getConstantValue('HEP_DATETIME_FORMAT'));
            }
        }catch(Exception e){
            System.debug('Exception occurred because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
    }
    
    
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class ApprovalEmailWrapper{
        
        public String   sPromotionName {get; set;}
        public String   sLockedDate {get; set;}
        public String   sFAD   {get; set;}
        public String   sDateType {get; set;}
        public String   sDomesticMarketingManager   {get; set;}
        public String   sInternationalMarketingManager     {get; set;}  
    }

}