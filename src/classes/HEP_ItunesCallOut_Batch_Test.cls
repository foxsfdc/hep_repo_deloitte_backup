@isTest
Public class HEP_ItunesCallOut_Batch_Test{

    @testSetup 
    Static void setup(){
         HEP_Territory__c objUSTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','509',null, false);
        objUSTerritory.Flag_Country_Code__c = 'US';
        insert objUSTerritory;

        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objUSTerritory.Id, null, null,null,false);
        insert objPromotion;

        HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,null,objUSTerritory.id,false);
        insert objPromotionProduct;

        HEP_MDP_Promotion_Product_Detail__c objPromotionDetials = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'HD','EST',false);
        insert objPromotionDetials; 

    }

    @isTest 
    Public static void HEP_ItunesCallOut_Batch_TestMethod1() {
    
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        Map<id,HEP_MDP_Promotion_Product__c> mapPromotionProduct = new Map<id,HEP_MDP_Promotion_Product__c>();
        HEP_MDP_Promotion_Product__c objPromotionProduct = [select id from  HEP_MDP_Promotion_Product__c limit 1];
        mapPromotionProduct.put(objPromotionProduct.id,objPromotionProduct);
            
        Test.starttest();      
            HEP_ItunesCallOut_Batch batch1 = new HEP_ItunesCallOut_Batch(mapPromotionProduct);
            Database.executeBatch(batch1);      
        Test.stoptest();  
    }
    
    @isTest 
    Public static void HEP_ItunesCallOut_Batch_TestMethod2() {
    
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        Map<id,HEP_MDP_Promotion_Product_Detail__c> mapPromotionProductDetails = new Map<id,HEP_MDP_Promotion_Product_Detail__c>();
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetials = [select id from HEP_MDP_Promotion_Product_Detail__c limit 1];
        mapPromotionProductDetails.put(objPromotionDetials.id,objPromotionDetials);
        
        Test.starttest();
            HEP_ItunesCallOut_Batch batch3 = new HEP_ItunesCallOut_Batch(mapPromotionProductDetails);           
            Database.executeBatch(batch3);   
        Test.stoptest();   
    }
    
//   @isTest 
//     Public static void HEP_ItunesCallOut_Batch_TestMethod3() {
//         Map<id,Sobject> mapEmpty = new Map<id,Sobject>();
        
//         Test.starttest();
//         try{
//             HEP_ItunesCallOut_Batch batch2 = new HEP_ItunesCallOut_Batch(mapEmpty);           
//             Database.executeBatch(batch2); 
//          }  
//          catch(exception e){}
//         Test.stoptest();  
//     }      
}