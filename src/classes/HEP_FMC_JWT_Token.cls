/**
 * HEP_FMC_JWT_Token -- class will be used to get the FMC JWT Token.
 * @author    Lakshman Jinnuri
 */
public class HEP_FMC_JWT_Token implements HEP_IntegrationInterface {

    /**
     * performTransaction -- makes the callout and updates the response object with the JWT Token details
     * @param HEP_InterfaceTxnResponse object 
     * @return no return value
     * @author Lakshman Jinnuri    
     */
    public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse) {
        String sParameters = '';
        String sSourceName;
        Map<String,String> sessionIdTokenId = HEP_FMC_TitleImages.HEP_getSessionIdAuthId();
        //Variables to hold the custom Settings data
        String sHEP_SIEBEL_PRODUCERID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
        String sHEP_TOKEN_BEARER = HEP_Utility.getConstantValue('HEP_TOKEN_BEARER');
        String sHEP_SUCCESS = HEP_Utility.getConstantValue('HEP_STATUS_OK');
        String sHEP_STATUS_ACCEPTED = HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED');
        String sHEP_SUCCESS_CODE = HEP_Utility.getConstantValue('HEP_SUCCESS_CODE');
        String sHEP_FAILURE = HEP_Utility.getConstantValue('HEP_FAILURE');
        String sHEP_INVALID_TOKEN = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
        String sHEP_FMC_JWT_TOKEN = HEP_Utility.getConstantValue('HEP_FMC_JWT_TOKEN');
        String sHEP_SOURCE_NAME = HEP_Utility.getConstantValue('HEP_SOURCE_NAME');
        String sHEP_SESSION_ID = HEP_Utility.getConstantValue('HEP_SESSION_ID');
        String sHEP_ACCESS_TOKEN = HEP_Utility.getConstantValue('HEP_ACCESS_TOKEN');
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success'); 
        //String sSourceName =  '{"source_name":"FMC"}';HEP_SOURCE_NAME
        if(sHEP_SOURCE_NAME != null && sHEP_SIEBEL_PRODUCERID != null)
            sSourceName = '{' + sHEP_SOURCE_NAME + ' : ' +  sHEP_SIEBEL_PRODUCERID + '}';
        System.debug('sessionIdTokenId in Jwt token:'+sessionIdTokenId);
        if(sHEP_SESSION_ID != null && sHEP_ACCESS_TOKEN != null && sHEP_TOKEN_BEARER != null && sessionIdTokenId.get(sHEP_SESSION_ID) != null 
            && sessionIdTokenId.get(sHEP_ACCESS_TOKEN) != null && sessionIdTokenId.get(sHEP_ACCESS_TOKEN).startsWith(sHEP_TOKEN_BEARER) && objTxnResponse != null){
            HTTPRequest objHttpRequest = new HTTPRequest();
            HEP_Services__c objServiceDetails;
            //Gets the Details from custom setting for getting the access token
            if(sHEP_FMC_JWT_TOKEN != null && sHEP_FMC_JWT_TOKEN != null) 
                objServiceDetails = HEP_Services__c.getValues(sHEP_FMC_JWT_TOKEN);
            System.debug('objServiceDetails :'+objServiceDetails);
            if(objServiceDetails != null){
                objHttpRequest.setEndpoint(objServiceDetails.Endpoint_URL__c + objServiceDetails.Service_URL__c);
                objHttpRequest.setMethod('POST');
                objHttpRequest.setHeader('Content-Type','application/json');
                objHttpRequest.setHeader('Authorization',sessionIdTokenId.get(sHEP_ACCESS_TOKEN));
                objHttpRequest.setHeader('JSESSIONID',sessionIdTokenId.get(sHEP_SESSION_ID));
                objHttpRequest.setBody(sSourceName);
                objHttpRequest.setTimeout(integer.ValueOf(System.Label.HEP_TIMEOUT));
                System.debug('Request is :' + objHttpRequest);
                objTxnResponse.sRequest = objHttpRequest.toString() + '\n Body : \t' + objHttpRequest.getBody(); //request details
                HTTP objHttp = new HTTP();
                HTTPResponse objHttpResp = objHttp.send(objHttpRequest);
                objTxnResponse.sResponse = objHttpResp.getBody(); //Response from callout
                System.debug('objHttpResp.getBody() :' + objHttpResp.getBody());
                System.debug('objHttpResp.getStatusCode()' + objHttpResp.getStatusCode());
                if(sHEP_SUCCESS_CODE != null && objHttpResp.getStatusCode() == (Integer.ValueOf(sHEP_SUCCESS_CODE))){
                    //Gets the details from of the JWT Token from FMC System
                    if(sHEP_Int_Txn_Response_Status_Success != null)         
                        objTxnResponse.sStatus = HEP_Constants__c.getValues('HEP_Int_Txn_Response_Status_Success').Value__c;
                    objTxnResponse.bRetry = false;
                }
                else{
                    if(sHEP_FAILURE != null)
                        objTxnResponse.sStatus = sHEP_FAILURE;
                    if(objHttpResp.getStatus() != null && objHttpResp.getStatus().length() > 254)
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus().substring(0, 254);
                    else    
                        objTxnResponse.sErrorMsg = objHttpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }        
            }
        }
        //if the token is not valid or if we get the invalid transaction response
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            if(sHEP_INVALID_TOKEN != null && sHEP_FAILURE != null){
                objTxnResponse.sStatus = sHEP_FAILURE;
                objTxnResponse.sErrorMsg = sHEP_INVALID_TOKEN;
                objTxnResponse.bRetry = true;
            }    
        }
    }              
}