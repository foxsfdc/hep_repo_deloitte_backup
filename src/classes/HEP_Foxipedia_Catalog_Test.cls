@isTest
public class HEP_Foxipedia_Catalog_Test {

    
    @testSetup
    static void testDataSetUpMethod(){
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
    }
    
   @isTest 
    static void HEP_Foxipedia_Catalog_InvalidAcessTokenTest() {
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='Foxipedia_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        
        RestRequest req = new RestRequest();
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG","Operation": "UPDATE","Change": {"chldCtlgId": "15047271","prntCtlgId": "88721"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        String sJSON = req.requestbody.toString();
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_Foxipedia_Catalog.EDFRequestRoot objEDF = new HEP_Foxipedia_Catalog.EDFRequestRoot();
        objEDF.EventName = 'FoxipediaChangeEvent';
        objEDF.EventType = 'ProductChange';
        objEDF.ProducerId = '1234';
        HEP_Foxipedia_Catalog.DataWrapper objData = new HEP_Foxipedia_Catalog.DataWrapper();
        HEP_Foxipedia_Catalog.PayloadWrapper objPayload = new HEP_Foxipedia_Catalog.PayloadWrapper();
        objPayload.MessageID = '62997277';
        objPayload.TimeStamp = '2017-12-13T16:07:49.945-08:00';
        objPayload.ChangedEntity = 'Product.ProductGroup';
        objPayload.BaseObject = 'C_B_CTLG_GRP';
        objPayload.Operation = 'UPDATE';
        HEP_Foxipedia_Catalog.ChangeWrapper objChange = new HEP_Foxipedia_Catalog.ChangeWrapper();
        objChange.rowidObject = '95';
        objChange.chldRowidCtlg = '9391';
        objChange.chldCtlgId = '15047271';
        objChange.chldCtlgTypCd = 'SNGL';
        objChange.prntRowidCtlg = '18585';
        objChange.prntCtlgId = '15047271';
        objChange.prntCtlgTypCd = 'BXSET';
        objChange.hubStateInd = '1';
        objChange.level1 = 'ProductGroup';
        objChange.changedEntity = 'ProductGroup';
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG'));
        HEP_Foxipedia_Catalog demo = new HEP_Foxipedia_Catalog();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
     @isTest 
     static void HEP_Foxipedia_Catalog_Single_SuccessTest() {
        RestRequest req = new RestRequest();
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "88721","prntCtlgId": "88721"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        String sJSON = req.requestbody.toString();
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_Foxipedia_Catalog.EDFRequestRoot objEDF = new HEP_Foxipedia_Catalog.EDFRequestRoot();
        objEDF.EventName = 'FoxipediaChangeEvent';
        objEDF.EventType = 'ProductChange';
        objEDF.ProducerId = '1234';
        HEP_Foxipedia_Catalog.DataWrapper objData = new HEP_Foxipedia_Catalog.DataWrapper();
        HEP_Foxipedia_Catalog.PayloadWrapper objPayload = new HEP_Foxipedia_Catalog.PayloadWrapper();
        objPayload.MessageID = '62997277';
        objPayload.TimeStamp = '2017-12-13T16:07:49.945-08:00';
        objPayload.ChangedEntity = 'Product.ProductGroup';
        objPayload.BaseObject = 'C_B_CTLG_GRP';
        objPayload.Operation = 'UPDATE';
        HEP_Foxipedia_Catalog.ChangeWrapper objChange = new HEP_Foxipedia_Catalog.ChangeWrapper();
        objChange.rowidObject = '95';
        objChange.chldRowidCtlg = '9391';
        objChange.chldCtlgId = '43892';
        objChange.chldCtlgTypCd = 'SNGL';
        objChange.prntRowidCtlg = '18585';
        objChange.prntCtlgId = '43892';
        objChange.prntCtlgTypCd = 'BXSET';
        objChange.hubStateInd = '1';
        objChange.level1 = 'ProductGroup';
        objChange.changedEntity = 'ProductGroup';
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG'));
        HEP_Foxipedia_Catalog demo = new HEP_Foxipedia_Catalog();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    @isTest 
     static void HEP_Foxipedia_Catalog_Coupling_SuccessTest() {
        RestRequest req = new RestRequest();
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "33234","prntCtlgId": "33234"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        String sJSON = req.requestbody.toString();
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_Foxipedia_Catalog.EDFRequestRoot objEDF = new HEP_Foxipedia_Catalog.EDFRequestRoot();
        objEDF.EventName = 'FoxipediaChangeEvent';
        objEDF.EventType = 'ProductChange';
        objEDF.ProducerId = '1234';
        HEP_Foxipedia_Catalog.DataWrapper objData = new HEP_Foxipedia_Catalog.DataWrapper();
        HEP_Foxipedia_Catalog.PayloadWrapper objPayload = new HEP_Foxipedia_Catalog.PayloadWrapper();
        objPayload.MessageID = '62997277';
        objPayload.TimeStamp = '2017-12-13T16:07:49.945-08:00';
        objPayload.ChangedEntity = 'Product.ProductGroup';
        objPayload.BaseObject = 'C_B_CTLG_GRP';
        objPayload.Operation = 'UPDATE';
        HEP_Foxipedia_Catalog.ChangeWrapper objChange = new HEP_Foxipedia_Catalog.ChangeWrapper();
        objChange.rowidObject = '95';
        objChange.chldRowidCtlg = '9391';
        objChange.chldCtlgId = '43892';
        objChange.chldCtlgTypCd = 'SNGL';
        objChange.prntRowidCtlg = '18585';
        objChange.prntCtlgId = '43892';
        objChange.prntCtlgTypCd = 'BXSET';
        objChange.hubStateInd = '1';
        objChange.level1 = 'ProductGroup';
        objChange.changedEntity = 'ProductGroup';
        List < HEP_Territory__c > lstTerritory = new List < HEP_Territory__c >();
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'Global';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.Auto_Catalog_Creation__c = False;
        //objTerritory.Record_Status__c = 'Active';
        insert objTerritory;
        //Create another Territory record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'DHE';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code1';
        objTerritory1.Territory_Code_Integration__c = '0001';
        objTerritory1.Auto_Catalog_Creation__c = True;
        //objTerritory1.Record_Status__c = 'Active';
        insert objTerritory1;
        lstTerritory.add(objTerritory1);
        lstTerritory.add(objTerritory);
        //Create EDM Product Type.
        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('TV','TV',true);
        Map < Id, EDM_GLOBAL_TITLE__c > mapTitle = new Map < Id, EDM_GLOBAL_TITLE__c >();
        EDM_GLOBAL_TITLE__c objTitle = new EDM_GLOBAL_TITLE__c();
        objTitle.SEAS_NM__c = '20';
        objTitle.TITLE_SUB_TYPE_CD__c = 'EPSD';
        objTitle.Name = 'Sample Title';
        objTitle.FIN_PROD_ID__c = '5AFF14';
        objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC';
        objTitle.PROD_TYP_CD__c = objProductType.id;
        insert objTitle;
        EDM_GLOBAL_TITLE__c objTitle1 = new EDM_GLOBAL_TITLE__c();
        objTitle.SEAS_NM__c = '22';
        objTitle.TITLE_SUB_TYPE_CD__c = 'EPSD';
        objTitle.Name = 'Sample Title';
        objTitle.FIN_PROD_ID__c = '5AFF13';
        objTitle.LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC';
        objTitle.PROD_TYP_CD__c = objProductType.id;
        insert objTitle1;
        mapTitle.put(objTitle.id,objTitle);
        mapTitle.put(objTitle1.id,objTitle1);
        Set < Id > setIds = new Set < Id >();
        setIds.add(objTitle.id);
        setIds.add(objTitle1.id);
        List < EDM_GLOBAL_TITLE_HIERARCHY__c > lstTitleHierarchy = new List < EDM_GLOBAL_TITLE_HIERARCHY__c >();
        EDM_GLOBAL_TITLE_HIERARCHY__c objEDM = new EDM_GLOBAL_TITLE_HIERARCHY__c();
        objEDM.CHILD_COUNT__c = 22.0;
		objEDM.PARENT_FOX_ID__c = objTitle1.id;
		objEDM.CHILD_FOX_ID__c = objTitle.id;
		objEDM.HIERARCHY_CODE__c = 'BCAST';
		insert objEDM;
        lstTitleHierarchy.add(objEDM);
        String sParentKey_Maximumchildren = objTitle.id;
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG'));
        HEP_Foxipedia_Catalog demo = new HEP_Foxipedia_Catalog();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest 
     static void HEP_Foxipedia_Catalog_Bundle_SuccessTest() {
        RestRequest req = new RestRequest();
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_TITLE_VER","Operation": "UPDATE","Change": {"chldCtlgId": "10000135","prntCtlgId": "10000135"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        String sJSON = req.requestbody.toString();
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_Foxipedia_Catalog.EDFRequestRoot objEDF = new HEP_Foxipedia_Catalog.EDFRequestRoot();
        objEDF.EventName = 'FoxipediaChangeEvent';
        objEDF.EventType = 'ProductChange';
        objEDF.ProducerId = '1234';
        HEP_Foxipedia_Catalog.DataWrapper objData = new HEP_Foxipedia_Catalog.DataWrapper();
        HEP_Foxipedia_Catalog.PayloadWrapper objPayload = new HEP_Foxipedia_Catalog.PayloadWrapper();
        objPayload.MessageID = '62997277';
        objPayload.TimeStamp = '2017-12-13T16:07:49.945-08:00';
        objPayload.ChangedEntity = 'Product.ProductGroup';
        objPayload.BaseObject = 'C_B_CTLG_GRP';
        objPayload.Operation = 'UPDATE';
        HEP_Foxipedia_Catalog.ChangeWrapper objChange = new HEP_Foxipedia_Catalog.ChangeWrapper();
        objChange.rowidObject = '95';
        objChange.chldRowidCtlg = '9391';
        objChange.chldCtlgId = '43892';
        objChange.chldCtlgTypCd = 'SNGL';
        objChange.prntRowidCtlg = '18585';
        objChange.prntCtlgId = '43892';
        objChange.prntCtlgTypCd = 'BXSET';
        objChange.hubStateInd = '1';
        objChange.level1 = 'ProductGroup';
        objChange.changedEntity = 'ProductGroup';
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG'));
        HEP_Foxipedia_Catalog demo = new HEP_Foxipedia_Catalog();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    @isTest 
    static void HEP_Foxipedia_Catalog_FailureTest() {
        RestRequest req = new RestRequest();
        String requestBody = '{"EventName": "FoxipediaChangeEvent","EventType": "ProductChange","Data": {"Payload": {"MessageID": "62997277","TimeStamp": "2017-12-13T16:07:49.945-08:00","ChangedEntity": "Product.ProductGroup","BaseObject": "C_B_CTLG_GRP","Operation": "UPDATE","Change": {"chldCtlgId": "15047271","prntCtlgId": "aer567854444"}}}}';
        req.requestBody = Blob.valueOf(requestBody);
        String sJSON = req.requestbody.toString();
        RestContext.request = req;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Foxipedia_Catalog','HEP_Foxipedia_Catalog',true,10,10,'Inbound',true,true,true);
        HEP_Foxipedia_Catalog.EDFRequestRoot objEDF = new HEP_Foxipedia_Catalog.EDFRequestRoot();
        objEDF.EventName = 'FoxipediaChangeEvent';
        objEDF.EventType = 'ProductChange';
        objEDF.ProducerId = '1234';
        HEP_Foxipedia_Catalog.DataWrapper objData = new HEP_Foxipedia_Catalog.DataWrapper();
        HEP_Foxipedia_Catalog.PayloadWrapper objPayload = new HEP_Foxipedia_Catalog.PayloadWrapper();
        objPayload.MessageID = '62997277';
        objPayload.TimeStamp = '2017-12-13T16:07:49.945-08:00';
        objPayload.ChangedEntity = 'Product.ProductGroup';
        objPayload.BaseObject = 'C_B_CTLG_GRP';
        objPayload.Operation = 'UPDATE';
        HEP_Foxipedia_Catalog.ChangeWrapper objChange = new HEP_Foxipedia_Catalog.ChangeWrapper();
        objChange.rowidObject = '95';
        objChange.chldRowidCtlg = '9391';
        objChange.chldCtlgId = '15047271';
        objChange.chldCtlgTypCd = 'SNGL';
        objChange.prntRowidCtlg = '18585';
        objChange.prntCtlgId = '15047271';
        objChange.prntCtlgTypCd = 'BXSET';
        objChange.hubStateInd = '1';
        objChange.level1 = 'ProductGroup';
        objChange.changedEntity = 'ProductGroup';
        Test.starttest ();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout()); 
        HEP_InterfaceTxnResponse objTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sJSON,'',HEP_Utility.getConstantValue('HEP_FOXIPEDIA_CATALOG'));        
        HEP_Foxipedia_Catalog demo = new HEP_Foxipedia_Catalog();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }

}