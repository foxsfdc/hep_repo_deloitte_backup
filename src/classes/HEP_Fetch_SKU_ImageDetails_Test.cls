@isTest
private class HEP_Fetch_SKU_ImageDetails_Test {
    
    @isTest
    public static void testFetchSKuImageDetails() {
        
        HEP_SKU_InputWrapper objSkuInput = new HEP_SKU_InputWrapper();
        objSkuInput.sTerritoryCode = 'au';
        //objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDO','34879SDW','36604SDO','36724SDO','31174SDO'};
        objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDO'};
        
        List<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_Title_Asset__c titleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('4234234', 'Asset Name', true);
        
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = JSON.serialize(objSkuInput);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Fetch_SKU_ImageDetails demo = new HEP_Fetch_SKU_ImageDetails();
        demo.performTransaction(objTxnResponse);
        //objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDI'};
        //objTxnResponse.sSourceId = JSON.serialize(objSkuInput);        
        //demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest
    public static void testFetchSKUImageDetailsError(){
    	
        
        HEP_SKU_InputWrapper objSkuInput = new HEP_SKU_InputWrapper();
        objSkuInput.sTerritoryCode = 'au';
        objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDI'};
        
        List<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_Title_Asset__c titleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('4234234', 'Asset Name', true);
        
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = JSON.serialize(objSkuInput);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Fetch_SKU_ImageDetails demo = new HEP_Fetch_SKU_ImageDetails();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest
    public static void testFetchSKUImageDetailsFailure(){
    
        HEP_SKU_InputWrapper objSkuInput = new HEP_SKU_InputWrapper();
        objSkuInput.sTerritoryCode = 'au';
        //objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDO','34879SDW','36604SDO','36724SDO','31174SDO'};
        objSkuInput.lstSkuObjectNumber = new List<String>{'21708SDE'};
        
        List<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_Title_Asset__c titleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('4234234', 'Asset Name', true);
        
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = JSON.serialize(objSkuInput);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Fetch_SKU_ImageDetails demo = new HEP_Fetch_SKU_ImageDetails();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest
    public static void testFetchSKUImageDetailsSourceNull(){
        
        List<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        //HEP_Title_Asset__c titleAsset = HEP_Test_Data_Setup_Utility.createTitleAsset('4234234', 'Asset Name', true);
        
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse;
        //objTxnResponse.sSourceId = JSON.serialize(objSkuInput);
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Fetch_SKU_ImageDetails demo = new HEP_Fetch_SKU_ImageDetails();
        demo.performTransaction(objTxnResponse);
        Test.stoptest();
    }
    
    @isTest
    static void testInvalidAcessToken(){
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_Siebel_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/token/tokenfail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
       
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_Fetch_SKU_ImageDetails demo = new HEP_Fetch_SKU_ImageDetails();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();        
        System.assertEquals('Invalid Access Token Or Invalid txnResponse data sent', objTxnResponse.sErrorMsg);         
    }  
    
}