/**
* HEP_Title_Details_Controller --- Class to get the data for title header
* @author    Sachin Agarwal
*/

public without sharing class HEP_Title_Details_Controller {
	
    public map<String, String> mapPageAccess{get;set;}
    public Boolean bHasPageAccess{get;set;}
    public String sHeaderTabs{get;set;}
    public String sCurrencyFormat {get;set;}
    public String sTerritoryId;    
    
    /**
	* Header --- Wrapper class for holding header details
	* @author    Sachin Agarwal
	*/
    public class Header{
    	
    	String sImageUrl;
    	String sTitleAssetId;
    	String sFoxipediaUrl;
    	String sTitleName;
    	String sFinancialTitleId;
    	String sFinancialDivision;
    	String sTitleProductType;
    	String sProductType;
    	String sUSReleaseDate;
    	String sUSInitialAirDate;
    	String sOriginalMedia;
    	String sLifecycleStatus;
    	String sCountryOfOrigin;
    	String sDirectors; 
    	String sExecutiveProducers;
    }
        
    /**
    * Class constructor to initialize the class variables
    * @return nothing
    * @author Sachin Agarwal
    */ 
    public HEP_Title_Details_Controller (){
    	
    	bHasPageAccess = false;
    	
    	// Calling the security framework to get the list of tabs to be shown on the UI 
        list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
        mapPageAccess = HEP_Security_Framework.getTerritoryAccess(null, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_TITLE_DETAILS'), HEP_Utility.getConstantValue('HEP_HEADER_PAGE_TITLE'), lstHeaderTabs);
        
        // If the user has read access to the page, then only load the page otherwise redirect the user to access denied page.	        
        if(mapPageAccess.containsKey(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) && mapPageAccess.get(HEP_Utility.getConstantValue('HEP_PERMISSION_READ')) == 'TRUE'){
        	bHasPageAccess = true;
        }
        
        sHeaderTabs = JSON.serialize(lstHeaderTabs); 
        
        sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(null);
    }
    
    
     /**
    * CheckPermissions --- To check if the current user have access to the page is in.
    * @author    Sachin Agarwal
    */
    public PageReference checkPermissions(){
        return new HEP_Security_Framework().redirectToAccessDenied(bHasPageAccess);
    }
    
    
    /**
    * Extracts all the data required on the header.
    * @param  sTitleId id of the title of which details are to be loaded
    * @return an instance of header class containing all the data required for the title portion
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static Header getHeaderDetails(String sTitleId, String sLocaleDateFormat){
    	// Creating an instance of Header class
        Header objHeader = new Header();
        
        // Checking if the title is non-empty or not
        if(String.isNotEmpty(sTitleId)){
        	list<EDM_GLOBAL_TITLE__c> lstTitles;
	        list<GTS_Title__c> lstGTSTitles;
	       
	        lstTitles = [SELECT id, Name, FIN_PROD_ID__c, FIN_DIV_CD__r.DIV_DESC__c, FOX_ID__c,
	        					PRIM_PROD_CNTRY_CD__r.CNTRY_DESC__c, PROD_TYP_CD__c, PROD_TYP_CD__r.PROD_TYP_DESC__c, ORIG_MEDIA_CD__r.MEDIA_DESC__c,
	        					PROD_TYP_CD__r.PROD_TYP_CD__c,
	        					LIFE_CYCL_STAT_CD__r.LIFE_CYCL_STAT_DESC__c, DIR_NM__c,
	        					(SELECT Asset_Id__c, Media_Id__c, Preview_Id__c, Thumbnail_Id__c 
	        					 FROM HEP_Title_Assets__r order by lastmodifieddate desc),
	        					(SELECT Id, Country_Code__c, TVD_Release_Date__c 
	        					 FROM TVD_Release_Dates__r
	        					 WHERE Country_Code__c = :HEP_Utility.getConstantValue('HEP_COUNTRY_CODE_US') limit 1)
	        			 FROM EDM_GLOBAL_TITLE__c
	                     WHERE id = :sTitleId AND LIFE_CYCL_STAT_GRP_CD__c != :HEP_Utility.getConstantValue('HEP_LIFECYCLE_STATUS_GRP_CD_CNFDL')];
	        
	        set<String> setDirectorsProducers = new set<String>();
	        set<String> setNameComponents = new set<String>();  
	        String sName = '';
	        // If there's a  matching title                     
	        if(lstTitles != null && !lstTitles.isEmpty()){
	            
	            String sShowDirectorProducer;
	            objHeader.sTitleProductType = lstTitles[0].PROD_TYP_CD__r.PROD_TYP_DESC__c;
	            String sProductTypeCode = lstTitles[0].PROD_TYP_CD__r.PROD_TYP_CD__c;
	            
	            if(lstTitles[0].HEP_Title_Assets__r != null && !lstTitles[0].HEP_Title_Assets__r.isEmpty()){
	            	objHeader.sTitleAssetId = lstTitles[0].HEP_Title_Assets__r[0].id;
	            }
	            if(sProductTypeCode != null){
	            	if(sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_DTV') || 
	            		sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_FEATR') || 
	            		sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_MKGOF') || 
	            		sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_NMFLM')){
	            		
	            		sShowDirectorProducer = HEP_Utility.getConstantValue('HEP_ACTOR_TYPE_DIRECTOR');           		
	            		
	            		objHeader.sProductType = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_FEATURE');
	            	}
	            
	            	else if(sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_FRMT') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_MINIS') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_MOW') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SHRTS') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SPCL') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SPRTS') || 
	            			sProductTypeCode == HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_CODE_SEASN')){
	            		
	            		sShowDirectorProducer = HEP_Utility.getConstantValue('HEP_ACTOR_TYPE_PRODUCER');
	            		objHeader.sProductType = HEP_Utility.getConstantValue('HEP_TITLE_PRODUCT_TYPE_TV');
	            		
	            		if(lstTitles[0].TVD_Release_Dates__r != null && !lstTitles[0].TVD_Release_Dates__r.isEmpty()){
	            			objHeader.sUSInitialAirDate = HEP_Utility.getFormattedDate(lstTitles[0].TVD_Release_Dates__r[0].TVD_Release_Date__c, sLocaleDateFormat);
	            		}
	            	}
	            }
	            
	            if(String.isNotBlank(lstTitles[0].FOX_ID__c) && String.isNotBlank(sShowDirectorProducer)){
	            	
	            	HEP_InterfaceTxnResponse objResponse = HEP_ExecuteIntegration.executeIntegMethod(lstTitles[0].FOX_ID__c, '', 'HEP_Foxipedia_Talent');
	            	//HEP_InterfaceTxnResponse objResponse = HEP_ExecuteIntegration.executeIntegMethod('2431818', '', 'HEP_Foxipedia_Talent');
	            	
	            	if(objResponse.sStatus == HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success')){
	            		HEP_Foxipedia_TalentWrapper objFoxipediaTalendWrapper = (HEP_Foxipedia_TalentWrapper)JSON.deserialize(objResponse.sResponse, HEP_Foxipedia_TalentWrapper.class);
	            		
	            		if(objFoxipediaTalendWrapper != null){
	            			for(HEP_Foxipedia_TalentWrapper.Data objData : objFoxipediaTalendWrapper.data){
	            				HEP_Foxipedia_TalentWrapper.Attributes objAttribute = objData.attributes;
	            				if(objAttribute != null && objAttribute.talentTypeDescription != null && objAttribute.talentTypeDescription.containsIgnoreCase(sShowDirectorProducer)){
	            					
	            					setNameComponents.clear();
	            					
	            					if(String.isNotBlank(objAttribute.talentFirstName) ){
	            						setNameComponents.add(objAttribute.talentFirstName);
	            					}
	            					if(String.isNotBlank(objAttribute.talentMiddleName) ){
	            						setNameComponents.add(objAttribute.talentMiddleName);
	            					}
	            					if(String.isNotBlank(objAttribute.talentLastName) ){
	            						setNameComponents.add(objAttribute.talentLastName);
	            					}            					
	            					if(setNameComponents.size() > 0){
	            						setDirectorsProducers.add(String.join(new list<String>(setNameComponents), ' '));
	            					}             					
	            				} 
	            			}
	            		}
	            	}
	            }
	            
	            if(setDirectorsProducers.size() > 0){
	            	
	            	if(sShowDirectorProducer == HEP_Utility.getConstantValue('HEP_ACTOR_TYPE_DIRECTOR')){
		            	objHeader.sDirectors = String.join(new list<String>(setDirectorsProducers), ', ');
		            }
		            else if(sShowDirectorProducer == HEP_Utility.getConstantValue('HEP_ACTOR_TYPE_PRODUCER')){
		            	objHeader.sExecutiveProducers = String.join(new list<String>(setDirectorsProducers), ', ');
		            }
	            }            
	            
	            lstGTSTitles = [SELECT Id, US_Release_Date__c, Total_GBO_Rollup__c
		                        FROM GTS_Title__c
		                        WHERE WPR__c = :lstTitles[0].FIN_PROD_ID__c];
	           
	            objHeader.sTitleName = lstTitles[0].Name;
	            objHeader.sFinancialTitleId = lstTitles[0].FIN_PROD_ID__c;
	            objHeader.sFinancialDivision = lstTitles[0].FIN_DIV_CD__r.DIV_DESC__c;
	            
	            objHeader.sOriginalMedia = lstTitles[0].ORIG_MEDIA_CD__r.MEDIA_DESC__c;
	            objHeader.sLifecycleStatus = lstTitles[0].LIFE_CYCL_STAT_CD__r.LIFE_CYCL_STAT_DESC__c;
	            objHeader.sCountryOfOrigin = lstTitles[0].PRIM_PROD_CNTRY_CD__r.CNTRY_DESC__c;
	                        
	            objHeader.sFoxipediaUrl = HEP_Utility.getConstantValue('HEP_FOXIPEDIA_URL') + objHeader.sFinancialTitleId;
	            
	            // If there's a  matching GTS title
	            if(lstGTSTitles != null && !lstGTSTitles.isEmpty()){
	            	objHeader.sUSReleaseDate = HEP_Utility.getFormattedDate(lstGTSTitles[0].US_Release_Date__c, sLocaleDateFormat);
	            }
	        }
        }
        
        // returning the extracted data
        return objHeader;
    }
    
        
    /**
    * Extracts the image blob
    * @param  sTitleAssetId id of the title asset which was latest received
    * @return image blob
    * @author Sachin Agarwal
    */
    @RemoteAction 
    public static String getImage(String sTitleAssetId){
    	if(!String.isEmpty(sTitleAssetId)){
    		try{
		    	HEP_InterfaceTxnResponse objInterfaceTxnResponse = HEP_ExecuteIntegration.executeIntegMethod(sTitleAssetId, null, 'HEP_FMC_Apigee_Rendition');
		    	if(objInterfaceTxnResponse.sStatus == HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success')){
		    		if(String.isEmpty(objInterfaceTxnResponse.sResponse)){
		    			return HEP_Utility.getPromoDefaultImageUrl();
		    		}
		    		return objInterfaceTxnResponse.sResponse;
		    	}		
	    	}
	    	catch(Exception ex){   		
	    	}
    	}
    	else{
    		return HEP_Utility.getPromoDefaultImageUrl();
    	}
    
    	return HEP_Utility.getPromoDefaultImageUrl();
    }
}