/*------------------------------------Fox HEP----------------------------------
This class handles the inbound request from Prophet, Copper & Foxipedia for HEP application. 
-------------------------------------------------------------------------------
API Name:
Created: 03.30.18 by Deloitte
Version: 1.0
-----------------------------------------------------------
Change Log:
-----------------------------------------------------------
v1.0     03.30.18    Created by Deloitte     LieLy
------------------------------------------------------------------------------*/
@RestResource(urlMapping='/v1/HEP_ReleaseKeys/*')
global class HEP_ReleaseKeys {

	/**
    * @param modifiedSince - Datetime which HEP_Promotion_Dating__c record was modified on or after
    * @param systemName - Name of boundary system that makes API call to HEP application
    * @return JSON string of : (1) distinct titles & comma delimited territories that associated 
    * with each title for Prophet system (2) array of Catalog IDs for Copper 
    */
	@HttpPost
	global static void getReleaseKeys(String modifiedSince, String systemName) {
		if (HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME') != null && HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME').Value__c != null && 
			HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME') != null && HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME').Value__c != null ) {

			if (systemName == HEP_Constants__c.getValues('HEP_PROPHET_SYSTEM_NAME').Value__c){	
				HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(modifiedSince,'',HEP_Constants__c.getValues('HEP_RELEASEKEYS_PROPHET_INBOUND').Value__c);
			}
			else if (systemName == HEP_Constants__c.getValues('HEP_COPPER_SYSTEM_NAME').Value__c){
				HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(modifiedSince,'',HEP_Constants__c.getValues('HEP_RELEASEKEYS_COPPER_INBOUND').Value__c);
			}

		}
	}

	@HttpGet
	global static void getFoxipediaReleaseKeys() {
		RestRequest req = RestContext.request; 
		RestResponse res = RestContext.response; 

		String modifiedSince = RestContext.request.params.get('modifiedSince');
		HEP_InterfaceTxnResponse objIntTxnResp = HEP_ExecuteIntegration.executeIntegMethod(modifiedSince,'',HEP_Constants__c.getValues('HEP_RELEASEKEYS_FOXIPEDIA_INBOUND').Value__c);
	}
}