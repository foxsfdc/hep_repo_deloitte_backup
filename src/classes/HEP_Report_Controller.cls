/**
 * HEP_Report_Controller --- Class to get the Report Link
 * @author Ayan Sarkar
 */

public with sharing class HEP_Report_Controller {
    //This variable is used to get the URL for Fox_Release_Schedule
    public String sFoxReleaseSchedule{ 
        get{
            String sReportType = ApexPages.currentPage().getParameters().get('reportType');
            /*if(ApexPages.currentPage().getUrl().substringAfter('apex/').equalsIgnoreCase('HEP_Report_Fox_Release_Schedule'))
                return HEP_Services__c.getValues('Fox Release Schedule').Endpoint_URL__c;
            else if(ApexPages.currentPage().getUrl().substringAfter('apex/').equalsIgnoreCase('HEP_Report_UpNext'))
                return HEP_Services__c.getValues('Up Next').Endpoint_URL__c;
            else if(ApexPages.currentPage().getUrl().substringAfter('apex/').equalsIgnoreCase('Competitive Release Schedule'))
                return HEP_Services__c.getValues('Competitive Release Schedule').Endpoint_URL__c;*/
                
            return HEP_Services__c.getValues(sReportType).Endpoint_URL__c;
        }
        set;
    }
}