/**
 * HEP_SKU_Activation_Rejection --- Dynamically fetch records in Email Template
 * @author  Gaurav Mehrishi
 */

public class HEP_SKU_Activation_Rejection{
    
    //SKU Promotion record id is passed in this variable
    public Id recordApproverIdForSKUPromotion  {get; set;}
    
    //This variable is used to render table on Email template
    public ApprovalEmailWrapper objApprovalwrapper{ 
        get{
            if(objApprovalWrapper== null){
                fetchSKUPromotionDetails();
            }
            return objApprovalwrapper;
        }
        set;
    }
    //Static Block For Active Status
    public static String sACTIVE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    } 
    
    /**
    * fetchSKUPromotionDetails --- fetch the Values of Merge fields used in Template
    * @return nothing
    * @author  Gaurav Mehrishi
    */  
    public void fetchSKUPromotionDetails(){
        
        try{  
            this.objApprovalwrapper= new ApprovalEmailWrapper();          
            String sSKUPromotionId; 
            String sPromoId;
            String sApprovalRole;  
            Set < Id > setSKUMasterId = new Set < Id > ();
            map < String, List < HEP_SKU_Price__c >> mapPriceRegionSKU = new map < String, List < HEP_SKU_Price__c >> ();
            map < String, List < HEP_SKU_Customer__c >> mapSKUCustomers = new map < String, List < HEP_SKU_Customer__c >> ();           
            if(recordApproverIdForSKUPromotion != null){          
                List<HEP_Record_Approver__c> lstRecordApproverForSKUPr = [SELECT Id,
                                                    Name,
                                                    Approval_Record__r.CreatedDate,
                                                    Approval_Record__r.CreatedBy.Name,
                                                    Approver__r.Name, 
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.FirstAvailableDateFormula__c,
                                                    Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.Territory__r.Name,
                                                    Approver_Role__r.Name                                                   
                                                    FROM HEP_Record_Approver__c
                                                    Where Id =: recordApproverIdForSKUPromotion And Record_Status__c =: sACTIVE
                                                    LIMIT 1];              
                //Validate the List and Populate the Wrapper
                if(lstRecordApproverForSKUPr != null && !lstRecordApproverForSKUPr.isEmpty()){    
                    System.debug('+++++++++' + lstRecordApproverForSKUPr);
                    sSKUPromotionId = lstRecordApproverForSKUPr[0].Approval_Record__r.HEP_Promotion_SKU__c;
                    sPromoId = lstRecordApproverForSKUPr[0].Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__c;
                    //List of Digital dating records related to promotion id
                    List < HEP_Promotion_Dating__c > lstPromotionDatingDigital = [Select Id, Date__c, Digital_Physical__c
                                   FROM HEP_Promotion_Dating__c
                                   WHERE Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND Promotion__c =: sPromoId AND Digital_Physical__c =: HEP_Utility.getConstantValue('HEP_DIGITAL_MEDIA_TYPE')  AND (FAD_Approval_Status__c !=: HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED') OR FAD_Approval_Status__c !=: HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING')) ORDER BY Date__c asc nulls last
                    ];
                    //List of Physical/Digital dating records related to promotion id
                    List < HEP_Promotion_Dating__c > lstPromotionDating = [Select Id, Date__c, Digital_Physical__c
                                   FROM HEP_Promotion_Dating__c
                                   WHERE Record_Status__c =: HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE') AND Promotion__c =: sPromoId AND (FAD_Approval_Status__c !=: HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED') OR FAD_Approval_Status__c !=: HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING')) ORDER BY Date__c asc nulls last
                    ];
                    system.debug('Promotion Dating Records' + lstPromotionDating);
                    system.debug('Promotion Dating Records--->' + lstPromotionDatingDigital);
                    sApprovalRole = lstRecordApproverForSKUPr[0].Approver_Role__r.Name; 
                    HEP_Record_Approver__c objSKURecordApprover = lstRecordApproverForSKUPr.get(0);
                    objApprovalwrapper.sLocalMarketingManager = objSKURecordApprover.Approver__r.Name;
                    objApprovalwrapper.sPromotionCode = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.LocalPromotionCode__c;
                    objApprovalwrapper.sPromotionName = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.PromotionName__c;
                    //objApprovalwrapper.sTerritoryFAD = HEP_Utility.getFormattedDate(objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.FirstAvailableDateFormula__c,'dd-MMM-yyyy');
                    if(lstPromotionDating[0].Date__c != null) objApprovalwrapper.sTerritoryFAD = HEP_Utility.getFormattedDate(lstPromotionDating[0].Date__c,'dd-MMM-yyyy');
                    if(lstPromotionDatingDigital[0].Date__c != null) objApprovalwrapper.sDigitalFAD = HEP_Utility.getFormattedDate(lstPromotionDatingDigital[0].Date__c,'dd-MMM-yyyy');
                    objApprovalWrapper.sTerritory = objSKURecordApprover.Approval_Record__r.HEP_Promotion_SKU__r.Promotion_Catalog__r.Promotion__r.Territory__r.Name;
                }
                if(sApprovalRole.startsWith(HEP_Utility.getConstantValue('HEP_MARKETING_MANAGER'))){
                    objApprovalwrapper.sEmailTitle = 'The following SKUs have been rejected by Brand Manager for your promotion';
                } else if(sApprovalRole.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_DATA_ADMIN'))){
                    objApprovalwrapper.sEmailTitle = 'The following SKUs have been rejected by Data Admin for your promotion';
                }
                //Query to get all the Price Regions records for the promotion SKU record
                List<HEP_SKU_Price__c> lstSKUPriceRegion = [SELECT Id,
                                                    PriceGrade__c,
                                                    PriceGrade__r.Name,
                                                    PriceGrade__r.PriceGrade__c,
                                                    PriceGrade__r.Dealer_List_Price__c,
                                                    Region__c,
                                                    Region__r.Name,
                                                    SKU_Master__r.Format__c,
                                                    SKU_Master__r.Channel__c,
                                                    SKU_Master__r.SKU_Title__c,
                                                    SKU_Master__r.SKU_Number__c,
                                                    SKU_Master__r.SKU_Configuration__c,
                                                    SKU_Master__c,
                                                    SKU_Master__r.Comments__c                                                   
                                                    FROM HEP_SKU_Price__c
                                                    WHERE Promotion_SKU__c =: sSKUPromotionId And Record_Status__c =: sACTIVE];
                Map<Id,HEP_SKU_Price__c> mapSKUPriceRegion = new Map < Id, HEP_SKU_Price__c >([SELECT Id,
                                                    PriceGrade__c,
                                                    PriceGrade__r.Name,
                                                    PriceGrade__r.PriceGrade__c,
                                                    PriceGrade__r.Dealer_List_Price__c,
                                                    Region__c,
                                                    Region__r.Name,
                                                    SKU_Master__r.Format__c,
                                                    SKU_Master__r.Channel__c,
                                                    SKU_Master__r.SKU_Title__c,
                                                    SKU_Master__r.SKU_Number__c,
                                                    SKU_Master__r.SKU_Configuration__c,
                                                    SKU_Master__c,
                                                    SKU_Master__r.Comments__c                                                   
                                                    FROM HEP_SKU_Price__c
                                                    WHERE Promotion_SKU__c =: sSKUPromotionId And Record_Status__c =: sACTIVE]);
                objApprovalwrapper.lstPriceRegion = new List<PriceRegionWrapper>();
                               
                if(lstSKUPriceRegion != null && !lstSKUPriceRegion.isEmpty()){
                
                    for (HEP_SKU_Price__c objSKUPriceRegion: lstSKUPriceRegion) {
                        setSKUMasterId.add(objSKUPriceRegion.SKU_Master__c);
                        if (mapPriceRegionSKU.containsKey(objSKUPriceRegion.SKU_Master__c)) {
                            mapPriceRegionSKU.get(objSKUPriceRegion.SKU_Master__c).add(objSKUPriceRegion);
                        } else {
                        mapPriceRegionSKU.put(objSKUPriceRegion.SKU_Master__c, new List < HEP_SKU_Price__c > {objSKUPriceRegion});
                        }
                    }
                    //Query all the customer records for the SKU master
                    List < HEP_SKU_Customer__c > lstSKUCustomers = [SELECT Id,
                                                            Customer__c,
                                                            SKU_Master__c,
                                                            Customer__r.CustomerName__c
                                                            FROM HEP_SKU_Customer__c
                                                            WHERE SKU_Master__c IN: setSKUMasterId
                                                            ];
                    if (lstSKUCustomers != NULL && !lstSKUCustomers.isEmpty()) {
                        for (HEP_SKU_Customer__c objSKUCustomer: lstSKUCustomers) {
                            if (mapSKUCustomers.containsKey(objSKUCustomer.SKU_Master__c)) {
                                mapSKUCustomers.get(objSKUCustomer.SKU_Master__c).add(objSKUCustomer);
                            } else {
                                mapSKUCustomers.put(objSKUCustomer.SKU_Master__c, new List < HEP_SKU_Customer__c > {objSKUCustomer});
                            }
                        }
                    system.debug('---------' + mapSKUCustomers);
                    }
                    for (String sOuterKey: mapSKUPriceRegion.keyset()) {
                        PriceRegionWrapper objPriceRegion = new PriceRegionWrapper();
                        List < String > lstCustomers = new List < String > ();
                        String sSKUMasterId;
                        HEP_SKU_Price__c objPrRegion = mapSKUPriceRegion.get(sOuterKey);
                        objPriceRegion.sRegion = objPrRegion.Region__r.Name;
                        objPriceRegion.sPriceGrade = objPrRegion.PriceGrade__r.PriceGrade__c;
                        objPriceRegion.sChannel = objPrRegion.SKU_Master__r.Channel__c;
                        objPriceRegion.sFormat = objPrRegion.SKU_Master__r.Format__c;
                        objPriceRegion.sSKUName = objPrRegion.SKU_Master__r.SKU_Title__c;
                        objPriceRegion.sSKUNumber = objPrRegion.SKU_Master__r.SKU_Number__c;
                        objPriceRegion.sComments = objPrRegion.SKU_Master__r.Comments__c;
                        sSKUMasterId = objPrRegion.SKU_Master__c;                                                               
                        if (mapSKUCustomers.containsKey(sSKUMasterId)) {
                            for (HEP_SKU_Customer__c objSKUCustomer: mapSKUCustomers.get(sSKUMasterId)) {
                                lstCustomers.add(objSKUCustomer.Customer__r.CustomerName__c);
                                system.debug('@@@' + lstCustomers);
                            }
                        }
                        if (lstCustomers.size() > 0) {
                            objPriceRegion.sCustomer = listToString(lstCustomers);
                            system.debug('objPriceRegion.sCustomer' + objPriceRegion.sCustomer);
                        }
                      
                        objApprovalwrapper.lstPriceRegion.add(objPriceRegion);                 
                    }
                }
                
                
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
            throw e;
        }
    }

    //this method converts list to String with values separated by comma
    public static String listToString(List < String > lstTemp) {
        String sCommaSeparated = '';
        for (String str: lstTemp) {
            sCommaSeparated += str + ',';
        }
        sCommaSeparated = sCommaSeparated.trim().removeEnd(',');
        system.debug('sCommaSeparated' + sCommaSeparated);
        return sCommaSeparated;

    }   
    /*
    **  ApprovalEmailWrapper--- Wrapper Class to hold Page Variables
    **  @author Gaurav Mehrishi
    */
    public class ApprovalEmailWrapper{
        
        public String   sLocalMarketingManager     {get; set;}
        public String   sPromotionCode    {get; set;}
        public String   sPromotionName    {get; set;}       
        public String   sPromotionFAD     {get; set;}  
        public String   sTerritory        {get; set;}
        public String   sTerritoryFAD     {get; set;}
        public String   sDigitalFAD       {get; set;}
        public String   sEmailTitle       {get;set;}
        public List<PriceRegionWrapper> lstPriceRegion {get; set;}
    }
    public class PriceRegionWrapper{
            public String sRegion     {get; set;} 
            public String sPriceGrade {get; set;}
            public String sSKUName    {get; set;}
            public String sSKUNumber  {get; set;}
            public String sChannel    {get; set;}
            public String sFormat     {get; set;}
            public String sCustomer   {get; set;}
            public String sSKUConfig  {get; set;}
            public String sComments   {get; set;}
             
    }

}