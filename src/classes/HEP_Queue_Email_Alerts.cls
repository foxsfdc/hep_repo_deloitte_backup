/**
 * HEP_Queue_Email_Alerts ---  Queueable Apex to Process Emails and Alerts to people in Approval Chain 
 * @author  Nishit Kedia
 */
public class HEP_Queue_Email_Alerts implements Queueable {

    //Approval Id is Key and User id are the notifiers
    public Map < Id, Set < Id >> mapApproversToNotify = new Map < Id, Set < Id >> ();
    //Additional Role SetUp
    public static Map < String, Set<String>> mapAdditionalRolesToEmail;
	public static Map < String, Set<Id>> mapAdditionalRolesToNotify;
    
    Set<Id> setApproverRoleIds = new Set<Id>();
    //Global constants for Approval f/W
    public static String sPendingStatus;
    public static String sApprovedStatus;
    public static String sRejectedStatus;
    public static string sCCEmailFilterApi;
    public static string sACTIVE;
    
    
    static{    
      sPendingStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
      sApprovedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
      sRejectedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
      sCCEmailFilterApi = HEP_Utility.getConstantValue('HEP_APPROVAL_EMAILCC_TERRITORY_FILTER');
      sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author  Nishit Kedia
     */
    public HEP_Queue_Email_Alerts(Map < Id, Set < Id >> mapApproversToNotify) {
        this.mapApproversToNotify = mapApproversToNotify;
        
    }    
    
    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author  Nishit Kedia
     */
    public HEP_Queue_Email_Alerts(Map < Id, Set < Id >> mapApproversToNotify, Set<Id> setApproverRoleIds) {
        this.mapApproversToNotify = mapApproversToNotify;
        this.setApproverRoleIds = setApproverRoleIds;
    }


    /**
     * execute --- Exceute Method will process the Map to Notify user with Email and Alert
     * @param objQueueableContext is the Context in which the Job was Enqueued
     * @return Nothing
     * @author  Nishit Kedia
     */
    public void execute(QueueableContext objQueueableContext) {

        try {
            //This is the main map with Key-->Approval Id and value is JSON wrapper
            Map < Id, ApprovalNotificationWrapper > mapProcessNotificationsToUsers = new Map < Id, ApprovalNotificationWrapper > ();

            if (mapApproversToNotify != null && !mapApproversToNotify.isEmpty()) {

                //To Store Set of Approval Ids which came in Enqueued.
                Set < Id > setApprovalId = mapApproversToNotify.keySet();

                //Set to hold User Ids who needs to be notified.
                Set < Id > setApprovers = new Set < Id > ();

                //To store Notification Template Names:
                Set < String > setNotificationTemplates = new Set < String > ();

                //Map to hold Parent Id and Keycombination of EmailTemplateName and Territory Name
                Map < String, String > mapEmailTemplateFilter = new Map < String, String > ();

                //Map to store Keycombination of EmailTemplateName and Territory Name and Value as Email CC lis
                Map < String, String > mapTemplateCCList = new Map < String, String > ();

                //To store Parent Record Ids: The parent Ids on which approval was Invoked
                Set < Id > setParentIds = new Set < Id > ();

                //Map to store parent Record Id and their Respective Record Details to be replaced in Template Body;
                Map < Id, Sobject > mapParentSobject = new Map < Id, Sobject > ();

                //Map to store Notification Templates name and Notification Object format.
                Map < String, HEP_Notification_Template__c > mapTemplateforBody = new Map < String, HEP_Notification_Template__c > ();

                //Populate the Set of Approvers
				String sApprovalIdToDetermineApprovalType ='';
                for (Id objApproverId: setApprovalId) {
                    setApprovers.addAll(mapApproversToNotify.get(objApproverId));
					sApprovalIdToDetermineApprovalType = objApproverId;
                }

                //Map of Approval Object : All approval Object Relevant details are fetched
                Map < Id, HEP_Approvals__c > mapApprovals = new Map < Id, HEP_Approvals__c > ([SELECT   Id,
                                                                                                        Approval_Type__r.Email_App_Template__c,
                                                                                                        Approval_Type__r.Email_Rej_Template__c,
                                                                                                        Approval_Type__r.Email_Req_Template__c,
                                                                                                        Approval_Type__r.Notif_App_Template__c,
                                                                                                        Approval_Type__r.Notif_Rej_Template__c,
                                                                                                        Approval_Type__r.Notif_Req_Template__c,
																										Approval_Type__r.Type__c,
                                                                                                        Approval_Status__c,
                                                                                                        Record_ID__c
                                                                                                        FROM HEP_Approvals__c
                                                                                                        WHERE Id IN: setApprovalId
                                                                                                        AND Record_Status__c =:sACTIVE
                                                                                                    ]);

                //Map of Record Approvers: The one who approved/Rejected and those who are in Intermediate
                Map < Id, HEP_Record_Approver__c > mapRecordApprovers = new Map < Id, HEP_Record_Approver__c > ([Select Id,
                                                                                                                        Status__c,
                                                                                                                        Approver_EmailId__c,
                                                                                                                        Approver__c,
																														Search_Id__c,
                                                                                                                        Approval_Record__c
                                                                                                                        From 
                                                                                                                        HEP_Record_Approver__c
                                                                                                                        Where
                                                                                                                        Approval_Record__c IN: setApprovalId
                                                                                                                        And
                                                                                                                        Approver_Role__c IN: setApproverRoleIds 
                                                                                                                        AND Record_Status__c =:sACTIVE
                                                                                                                        Order by Approval_Record__c, Sequence__c desc
                                                                                                                    ]);

                //Map Of user Ids with Emails:
                Map < Id, User > mapUserId = new Map < Id, User > ([Select  Id,
                                                                            Email
                                                                            from User
                                                                            where
                                                                            Id In: setApprovers
                                                                        ]);
																		
				//Fetch the Type Of Approval
				if(mapApprovals.containsKey(sApprovalIdToDetermineApprovalType)){
					HEP_Approvals__c objApproval = mapApprovals.get(sApprovalIdToDetermineApprovalType);
					setUpAdditionalUserRolesForEmailsNotification(objApproval.Approval_Type__r.Type__c);
				}

                String sObjectApiName = ''; // One Object per Approval is The assumption:                        
                if (mapRecordApprovers != null && !mapRecordApprovers.isEmpty()) {
                    for (Id recordApproverId: mapRecordApprovers.keySet()) {
                        //fetch Record Approver Object:
                        HEP_Record_Approver__c objrecordApprover = mapRecordApprovers.get(recordApproverId);
                        //Store Approval Id from It:
                        Id approvalId = objrecordApprover.Approval_Record__c;
                        if (!mapProcessNotificationsToUsers.containsKey(approvalId)) {
                            //fetch Approval Object
                            HEP_Approvals__c objApproval = mapApprovals.get(approvalId);
                            //Instantiate wrapper:
                            ApprovalNotificationWrapper objApprovalNotification = new ApprovalNotificationWrapper();
                            //get notified User id set from the main map
                            Set < Id > setUsersToBeNotified = mapApproversToNotify.get(approvalId);

                           

                            if (objApproval.Approval_Status__c.equalsIgnoreCase(sApprovedStatus) || objApproval.Approval_Status__c.equalsIgnoreCase(sRejectedStatus)) {
                                //If Approved
                                if (objApproval.Approval_Status__c.equalsIgnoreCase(sApprovedStatus)) {
                                    objApprovalNotification.sEmailTemplateName = objApproval.Approval_Type__r.Email_App_Template__c != null ? objApproval.Approval_Type__r.Email_App_Template__c : '';
                                    objApprovalNotification.sNotificationTemplateName = objApproval.Approval_Type__r.Notif_App_Template__c != null ? objApproval.Approval_Type__r.Notif_App_Template__c : '';

                                } else {
                                    objApprovalNotification.sEmailTemplateName = objApproval.Approval_Type__r.Email_Rej_Template__c != null ? objApproval.Approval_Type__r.Email_Rej_Template__c : '';
                                    objApprovalNotification.sNotificationTemplateName = objApproval.Approval_Type__r.Notif_Rej_Template__c != null ? objApproval.Approval_Type__r.Notif_Rej_Template__c : '';
                                }

                            } else if (objApproval.Approval_Status__c.equalsIgnoreCase(sPendingStatus)) {
                                objApprovalNotification.sEmailTemplateName = objApproval.Approval_Type__r.Email_Req_Template__c != null ? objApproval.Approval_Type__r.Email_Req_Template__c : '';
                                objApprovalNotification.sNotificationTemplateName = objApproval.Approval_Type__r.Notif_Req_Template__c != null ? objApproval.Approval_Type__r.Notif_Req_Template__c : '';
                            }
							
							 //Populate wrapper 
                            objApprovalNotification.setNotifiedUsersId = setUsersToBeNotified;
                            objApprovalNotification.parentSobjectId = objApproval.Record_ID__c;
                            objApprovalNotification.recordApproverId = recordApproverId;
							
							
							
                            // Populate Email Ids To whom Emails will be sent
                            Set < String > setEmailAddressToBeNotified = new Set < String > ();
                            //These are the requestors who submitted for Approval Originally
                            for (Id userId: setUsersToBeNotified) {
                                User objUserDetails = mapUserId.get(userId);
                                setEmailAddressToBeNotified.add(objUserDetails.Email);
                            }
							
							//[MP-1122][STARTS][Framework To support Additional Roles To Notify]
							//To Notify Additional Users from LOV Mapping and Send them Alerts;
							System.debug('mapAdditionalRolesToEmail---->'+ mapAdditionalRolesToEmail);
							if(mapAdditionalRolesToEmail != null && !mapAdditionalRolesToEmail.isEmpty()){
								if(String.isNotBlank(objApprovalNotification.sEmailTemplateName)){
									String sSearchKey = objApprovalNotification.sEmailTemplateName + ' / '+ objrecordApprover.Search_Id__c.substringAfter(' / ');
									if(mapAdditionalRolesToEmail.containsKey(sSearchKey)){
										setEmailAddressToBeNotified.addAll(mapAdditionalRolesToEmail.get(sSearchKey));
										System.debug('setEmailAddressToBeNotified-->'+setEmailAddressToBeNotified);
									}
								}
								
							}
							System.debug('mapAdditionalRolesToNotify---->'+ mapAdditionalRolesToNotify);
							//To Notify Additional Users from LOV Mapping and Send them Emails
							if(mapAdditionalRolesToNotify != null && !mapAdditionalRolesToNotify.isEmpty()){
								if(String.isNotBlank(objApprovalNotification.sNotificationTemplateName)){
									String sSearchKey = objApprovalNotification.sNotificationTemplateName + ' / '+ objrecordApprover.Search_Id__c.substringAfter(' / ');
									if(mapAdditionalRolesToNotify.containsKey(sSearchKey)){
										objApprovalNotification.setNotifiedUsersId.addAll(mapAdditionalRolesToNotify.get(sSearchKey));
										System.debug('setToBeNotified-->'+objApprovalNotification.setNotifiedUsersId);
									}
								}
								
							}
							//[MP-1122][ENDS][Framework To support Additional Roles To Notify]
							
                            //Assign Email Adress
                            objApprovalNotification.setToEmailAddress = setEmailAddressToBeNotified;

                            //Add the Template Names of Notifications in Set
                            setNotificationTemplates.add(objApprovalNotification.sNotificationTemplateName);

                            //Insert the Instance in Map
                            mapProcessNotificationsToUsers.put(approvalId, objApprovalNotification);

                            //Store parent Record Ids in a Set.
                            Id parentRecordId = Id.valueOf(objApprovalNotification.parentSobjectId);
                            setParentIds.add(parentRecordId);

                            //Store Object Api Name of Prent triggering Approval Record
                            sObjectApiName = parentRecordId.getSObjectType().getDescribe().getName();

                            //Map Parent Id to key Combination of Email Template Name
                            mapEmailTemplateFilter.put(parentRecordId, objApprovalNotification.sEmailTemplateName);
                        }
                    }
                }

                // Notification Template fetching on Single Object
                List < HEP_Notification_Template__c > lstFetchTemplates = [SELECT Id,
                    Name,
                    HEP_Body__c
                    FROM HEP_Notification_Template__c
                    WHERE Name =: setNotificationTemplates
                    And
                    HEP_Object__c =: sObjectApiName
                    And
                    Record_Status__c =: sACTIVE
                ];

                // Common filter field for filtering CC users byterritory
                String sfieldApiCommaSeparated = sCCEmailFilterApi;

                //Fetch Field Apis and populate for Notification Templates                             
                if (lstFetchTemplates != null && !lstFetchTemplates.isEmpty()) {
                    Set < String > setTemplatesFieldsApi = new Set < String > ();
                    for (HEP_Notification_Template__c objTemplate: lstFetchTemplates) {
                        String sNotifBody = objTemplate.HEP_Body__c;
                        setTemplatesFieldsApi.addAll(fetchFieldApisFromTemplate(sNotifBody));
                        if (!mapTemplateforBody.containsKey(objTemplate.Name)) {
                            mapTemplateforBody.put(objTemplate.Name, objTemplate );
                        }
                    }
                    //Generate All field Apis in comma Separated way from Notification Body
                    if (setTemplatesFieldsApi != null && !setTemplatesFieldsApi.isEmpty()) {
                        for (String sfield: setTemplatesFieldsApi) {
                            sfieldApiCommaSeparated += ',' + sfield;
                        }
                    }
                }
                //Build Dynamic Query and filter with record Ids of Parent Apporoval Ids
                String sBuildDynamicQuery = 'Select Id,' + sfieldApiCommaSeparated + ' from ' + sObjectApiName + ' where Id In :setParentIds';
                System.debug('Query Built--->' + sBuildDynamicQuery);

                List < Sobject > lstParentSobject = Database.Query(sBuildDynamicQuery);
                System.debug('lstParentSobject--->' + lstParentSobject);

                for (Sobject objparent: lstParentSobject) {
                    //parent record Of Approval
                    Id parentRecordId = objparent.Id;
                    //Map parent Id to parent Sobject details 
                    if (!mapParentSobject.containsKey(parentRecordId)) {
                        mapParentSobject.put(parentRecordId, objparent);
                    }

                    //Re-Map Parent Id to key Combination of (Email Template Name and territory Name)
                    if (mapEmailTemplateFilter.containsKey(parentRecordId)) {
                        mapEmailTemplateFilter.put(parentRecordId, mapEmailTemplateFilter.get(parentRecordId) + objparent.get(sCCEmailFilterApi));
                    }
                }

                if (mapEmailTemplateFilter != null && !mapEmailTemplateFilter.isEmpty()) {
                    for (HEP_List_Of_Values__c objTempLstRec: [Select Values__c, Name_And_Parent__c from HEP_List_Of_Values__c where Name_And_Parent__c IN: mapEmailTemplateFilter.values() And Record_Status__c =: sACTIVE]) {
                        if (mapTemplateCCList.containsKey(objTempLstRec.Name_And_Parent__c)) {
                            mapTemplateCCList.put(objTempLstRec.Name_And_Parent__c, mapTemplateCCList.get(objTempLstRec.Name_And_Parent__c) + ' ; ' + objTempLstRec.Values__c);
                        } else {
                            mapTemplateCCList.put(objTempLstRec.Name_And_Parent__c, objTempLstRec.Values__c);
                        }

                    }
                }
                System.debug('mapTemplateCCList--->' + mapTemplateCCList + ' and mapEmailTemplateFilter ' + mapEmailTemplateFilter);

                sendOutboundEmailToApprover(mapProcessNotificationsToUsers, mapEmailTemplateFilter, mapTemplateCCList);
                createGenericNotifications(mapProcessNotificationsToUsers, mapParentSobject, mapTemplateforBody);
            }
        }catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured when Executing Approval Queueable Class','DML Errors',objException,'HEP_Queue_Email_Alerts','execute',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            
        }
    }


    /**
     * sendOutboundEmailToApprover --- Method to Insert records in Outbound Email Object and Send Email Notifications
     * @param mapProcessNotificationsToUsers is the map with Approval Id and key is JSOn wrapper holding email Notifiers Id
     * @return Nothing
     * @author  Nishit Kedia
     */
    public static void sendOutboundEmailToApprover(Map < Id, ApprovalNotificationWrapper > mapProcessNotificationsToUsers, Map < String, String > mapEmailTemplateFilter, Map < String, String > mapTemplateCCList) {
        //--------------------------------------//      
        try {
            if (mapProcessNotificationsToUsers != null && !mapProcessNotificationsToUsers.isEmpty()) {
                //Outbound emails to be inserted
                List < HEP_Outbound_Email__c > lstOutboundEmailsToInsert = new List < HEP_Outbound_Email__c > ();
                //looping through Map key Set
                for (Id approvalId: mapProcessNotificationsToUsers.keySet()) {
                    //Fetch json Wrapper
                    ApprovalNotificationWrapper objApprovalNotifWrapper = mapProcessNotificationsToUsers.get(approvalId);
                    if(String.isNotBlank(objApprovalNotifWrapper.sEmailTemplateName)){
                        String sUsersInEMailCC = '';
                        //[CODE change: Log Email as 1 records]
    					HEP_Outbound_Email__c objOutboundEmail = new HEP_Outbound_Email__c();
    					objOutboundEmail.Record_Id__c = objApprovalNotifWrapper.recordApproverId;
                        objOutboundEmail.Object_API__c = objApprovalNotifWrapper.recordApproverId.getSObjectType().getDescribe().getName();
                        objOutboundEmail.Email_Template_Name__c = objApprovalNotifWrapper.sEmailTemplateName;
                        
    					//[CODE change: Log Email as 1 records]
                        //Fetch user need to kept in Email CC for the Territory
                        if (mapEmailTemplateFilter.containsKey(objApprovalNotifWrapper.parentSobjectId)) {
                            String sKey = mapEmailTemplateFilter.get(objApprovalNotifWrapper.parentSobjectId);
                            if (mapTemplateCCList.containsKey(sKey)) {
                                sUsersInEMailCC = mapTemplateCCList.get(sKey);
                                //Set the CC address
                                objOutboundEmail.CC_Email_Address__c = sUsersInEMailCC;
                            }
                        } 
                        //Insert Records in Outbound Emails for Every single Notifiers                      
                        if(objApprovalNotifWrapper.setToEmailAddress != null){
                        	String sEmailAddresses = '';                  
	                        for (String SEmailId: objApprovalNotifWrapper.setToEmailAddress) {
	                            sEmailAddresses+= SEmailId+';';
	                            //Add in List                           
	                        }
	                        objOutboundEmail.To_Email_Address__c = sEmailAddresses.substring(0,sEmailAddresses.length()-1);
                        }
                        lstOutboundEmailsToInsert.add(objOutboundEmail);
                    }
                }
                if (lstOutboundEmailsToInsert != null && !lstOutboundEmailsToInsert.isEmpty()) {
                    insert lstOutboundEmailsToInsert;
                    HEP_Send_Emails.sendOutboundEmails(lstOutboundEmailsToInsert);
                }
            }
            //--------------------------------------//
        } catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured when Sending outbound Emails','DML Errors',objException,'HEP_Queue_Email_Alerts','sendOutboundEmailToApprover',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
            
        } //--------------------------------------//
    } //--------------------------------------//


    /**
     * createGenericNotifications --- Method to Insert records in Notifications and Its Related Users
     * @param mapProcessNotificationsToUsers is the map with Approval Id and key is JSOn wrapper holding email Notifiers Id
     * @param mapParentSobject is the map with Approval Id and key and Parent details of the one which invoked Approval
     * @param mapTemplateforBody is the map with Notification Template Name and Its Relevant Object Format Id as Value
     * @return Nothing
     * @author  Nishit Kedia
     */
    public static void createGenericNotifications(Map < Id, ApprovalNotificationWrapper > mapProcessNotificationsToUsers, Map < Id, Sobject > mapParentSobject, Map < String, HEP_Notification_Template__c > mapTemplateforBody) {
        
        Savepoint objSavepoint = Database.setSavepoint();
        try {
            if (mapProcessNotificationsToUsers != null && !mapProcessNotificationsToUsers.isEmpty()) {
                //List of Notifications To be Inserted;
                //List<HEP_Notification__c> lstNotificationAlerts = new List<HEP_Notification__c>();
                //List of Notifications user Actions that will be get the parent notifications visible
                List < HEP_Notification_User_Action__c > lstNotificationsVisibleTo = new List < HEP_Notification_User_Action__c > ();
                Map < String, HEP_Notification__c > mapNotificationList = new Map < String, HEP_Notification__c > ();

                //Map<String,List<HEP_Notification_User_Action__c>> mapNotifandUsersList = new Map<String,List<HEP_Notification_User_Action__c>>();

                for (Id approvalId: mapProcessNotificationsToUsers.keySet()) {
                    //fetch Json wrapper for that Approval id
                    ApprovalNotificationWrapper objApprovalNotifWrapper = mapProcessNotificationsToUsers.get(approvalId);
                    // Fetch Template Name
                    String sTemplateName = objApprovalNotifWrapper.sNotificationTemplateName;
                    //If template is Balnk. No one will be Notified
                    if (String.IsNotBlank(sTemplateName) && mapTemplateforBody.containskey(sTemplateName)) {
                        //Create notifications
                        HEP_Notification__c objNotificationAlert = new HEP_Notification__c();
                        HEP_Notification_Template__c objNotificationTemplate = mapTemplateforBody.get(sTemplateName);
						String sNotifBody = objNotificationTemplate.HEP_Body__c;
                        Id parentId = objApprovalNotifWrapper.parentSobjectId;
                        Sobject objParentDetails = mapParentSobject.get(parentId);
                        objNotificationAlert.HEP_Message__c = createNotificationBody(objParentDetails, sNotifBody);
                        objNotificationAlert.HEP_Template_Name__c = sTemplateName;
                        objNotificationAlert.HEP_Object_API__c = parentId.getSObjectType().getDescribe().getName();
                        objNotificationAlert.HEP_Record_ID__c = parentId;
						objNotificationAlert.Notification_Template__c = objNotificationTemplate.Id;

                        //lstNotificationAlerts.add(objNotificationAlert);
                        mapNotificationList.put(approvalId, objNotificationAlert);

                        //Create user actions             
                    }
                }

                if (mapNotificationList != NULL && !mapNotificationList.isEmpty()) {
                    System.debug('Map-->' + mapNotificationList);
                    System.debug('Maps Keyset is-->' + mapNotificationList.keySet());
                    //lstNotificationAlerts.addAll(mapNotifandUsersList.keySet());
                    //System.debug('lstNotificationAlerts is the notification map--->'+lstNotificationAlerts);
                    insert mapNotificationList.values();

                    for (Id approvalId: mapProcessNotificationsToUsers.keySet()) {
                        if (mapNotificationList.containsKey(approvalId)) {
                            String sParentRecordId = mapNotificationList.get(approvalId).Id;

                            if (mapProcessNotificationsToUsers.get(approvalId).setNotifiedUsersId != null) {
                                for (Id approversUserId: mapProcessNotificationsToUsers.get(approvalId).setNotifiedUsersId) {
                                    HEP_Notification_User_Action__c objNotificationVisibleToUsers = new HEP_Notification_User_Action__c();
                                    objNotificationVisibleToUsers.Notification_User__c = approversUserId;
                                    objNotificationVisibleToUsers.HEP_Notification__c = sParentRecordId;
                                    //lstNotificationsVisibleTo.add(objNotificationVisibleToUsers);
                                    lstNotificationsVisibleTo.add(objNotificationVisibleToUsers);
                                }
                            }
                        }
                    }

                    if (lstNotificationsVisibleTo.size() > 0) {
                        insert lstNotificationsVisibleTo;
                        system.debug('lstNotificationAlerts Visible To----> ' + lstNotificationsVisibleTo);
                    }
                }
            }
        } catch (Exception objException) {
            Database.rollback( objSavepoint );
            HEP_Error_Log.genericException('Exception occured when creating Notification Records','DML Errors',objException,'HEP_Queue_Email_Alerts','createGenericNotifications',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }
    }


    /**
     * createNotificationBody --- To create Notification Body Message
     * @param objectRecordDetails is the parent Sobject Details on which Approval was invoked
     * @param notificationBody is the body format of the Notification Template
     * @return String containing appropriate Message Body
     * @author  Nishit Kedia
     */
    public static String createNotificationBody(Sobject objectRecordDetails, String sNotificationBody) {

        String sBodyString = '';
        try {
            //Split the string based on the Separator Format in the Message Body
            String[] lstSplitString = sNotificationBody.split('<');
            System.debug(lstSplitString);
            if (lstSplitString.size() > 0) {
                //Iterate the String and build the Dynamic Message Body for Notification
                for (String sExtractedString: lstSplitString) {
                    if (String.isNotBlank(sExtractedString)) {
                        //For Merge Fields in message the format is <Field Api>
                        if (sExtractedString.contains('>')) {
                            String sFieldApi = sExtractedString.substringBefore('>');
                            String sFieldValue = '';
                            //For lookup Field Api's
                            if (sFieldApi.contains('.')) {
                                String sParent = sFieldApi.substringBefore('.');
                                String sChild = sFieldApi.substringAfter('.');
                                //To extract relationship fields
                                sFieldValue = String.valueOf(objectRecordDetails.getSObject(sParent).get(sChild));
                            } else {
                                //For Simple field Api's
                                sFieldValue = String.valueOf(objectRecordDetails.get(sFieldApi));
                            }
                            sBodyString += sFieldValue + '';
                            sBodyString += sExtractedString.substringAfter('>') + '';
                        } else {
                            //If the String doesn't contains field Api's to merge
                            System.debug(sExtractedString);
                            sBodyString += sExtractedString + '';
                        }
                    }
                }
            }
        } catch (Exception objException) {
            HEP_Error_Log.genericException('Exception occured when creating Notification Body Message','DML Errors',objException,'HEP_Queue_Email_Alerts','createNotificationBody',null,null); 
            System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
        }
        return sBodyString;
    }


    /**
     * fetchFieldApisFromTemplate --- To fetch field Apis from Notification Template Body
     * @param notificationBody is the body format of the Notification Template
     * @param notificationBody is the body format of the Notification Template
     * @return setfieldApi string set which contains all unique fieldApi
     * @author  Nishit Kedia
     */
    public static Set < String > fetchFieldApisFromTemplate(String sNotificationBody) {

        Set < String > setfieldApi = new Set < String > ();
        //Split the string based on the Separator Format in the Message Body
        if (String.isNotBlank(sNotificationBody)) {
            String[] lstSplitString = sNotificationBody.split('<');
            System.debug(lstSplitString);
            if (lstSplitString.size() > 0) {
                //Iterate the String and build the Dynamic Message Body for Notification
                for (String sExtractedString: lstSplitString) {
                    if (String.isNotBlank(sExtractedString)) {
                        //For Merge Fields in message the format is <Field Api>
                        if (sExtractedString.contains('>')) {
                            String sfieldApi = sExtractedString.substringBefore('>');
                            setfieldApi.add(sfieldApi);
                        }
                    }
                }
            }
        }
        return setfieldApi;
    }
	
	/**
     * setUpAdditionalUsersToNotify --- Method to Extract Users based on Additional Roles to Notify from LOV
     * @param sApprovalType is the Unique Approval Type Name
     * @return Nothing
     * @author  Nishit Kedia
    */
	public static void setUpAdditionalUserRolesForEmailsNotification(String sApprovalType){
		//[MP-1122][STARTS][Framework To support Additional Roles To Notify]
		if(String.isNotBlank(sApprovalType)){
			try{
				String sEmailToRolesType = HEP_Utility.getConstantValue('HEP_EMAIL_TO_ROLES');
				String sNotifyToRolesType = HEP_Utility.getConstantValue('HEP_NOTIFICATION_TO_ROLES');
				mapAdditionalRolesToEmail = new Map < String, Set<String>>();
				mapAdditionalRolesToNotify = new Map < String, Set<Id>>();
				
				List<HEP_List_Of_Values__c> listOfValues = [Select  Name,
																	Values__c,
																	Type__c, 
																	Name_And_Parent__c 
																	from HEP_List_Of_Values__c 
																	where 
																	Parent_Value__c = :sApprovalType
																	And
																	(Type__c =: sEmailToRolesType 
																	OR 
																	Type__c =: sNotifyToRolesType)															
																	And Record_Status__c =: sACTIVE
															];
																		
				if(listOfValues!=null && !listOfValues.isEmpty()){
					
					Map<String,Set<String>> mapTemplateNameAndAdditionalRoleNames = new Map<String,Set<String>>();
					for(HEP_List_Of_Values__c objLovValue :listOfValues ){
						List<String> lstRoleName = objLovValue.Values__c.split(';');
						Set<String> setRoleNames = new Set<String>();
						setRoleNames.addAll(lstRoleName);
						
						if(mapTemplateNameAndAdditionalRoleNames.containsKey(objLovValue.Name)){
							mapTemplateNameAndAdditionalRoleNames.get(objLovValue.Name).addAll(setRoleNames);
						}else{
							mapTemplateNameAndAdditionalRoleNames.put(objLovValue.Name,setRoleNames);
						}
					}
					
					Set<String> setAllRoleNames = new Set<String>();
					if(!mapTemplateNameAndAdditionalRoleNames.isEmpty()){
						for(String sName : mapTemplateNameAndAdditionalRoleNames.keyset() ){
							setAllRoleNames.addAll(mapTemplateNameAndAdditionalRoleNames.get(sName));
						}	
					
						if(!setAllRoleNames.isEmpty()){
							List<HEP_User_Role__c> lstUserRoles = [Select Id,
																			Role__c,
																			User__r.Email,
																			Search_Id__c,
																			Territory__c,
																			Territory__r.Name 
																			from
																			HEP_User_Role__c
																			where 
																			Role__r.Name In: setAllRoleNames 
																			And Record_Status__c =: sACTIVE  
																	];
																	
							if(lstUserRoles != null && !lstUserRoles.isEmpty()){
								
								for(HEP_List_Of_Values__c objLovValue :listOfValues ){
									String sTemplateName = objLovValue.Name;
									for(HEP_User_Role__c userRoleObject:lstUserRoles){
										String sSearchKeyId = sTemplateName + ' / '+ userRoleObject.Territory__c;
										if(objLovValue.Type__c.equalsIgnoreCase(sEmailToRolesType)){
											if(mapAdditionalRolesToEmail.containskey(sSearchKeyId)){
												mapAdditionalRolesToEmail.get(sSearchKeyId).add(userRoleObject.User__r.Email);
											} else{
												mapAdditionalRolesToEmail.put(sSearchKeyId, new Set<String>{userRoleObject.User__r.Email});
											}
										} else if (objLovValue.Type__c.equalsIgnoreCase(sNotifyToRolesType)){
											if(mapAdditionalRolesToNotify.containskey(sSearchKeyId)){
												mapAdditionalRolesToNotify.get(sSearchKeyId).add(userRoleObject.User__c);
											} else{
												mapAdditionalRolesToNotify.put(sSearchKeyId, new Set<Id>{userRoleObject.User__c});
											}
										}
									}
								}
							}
						}
					}			
				}
			} catch (Exception objException) {
				HEP_Error_Log.genericException('Exception occured when creating Notification Body Message','DML Errors',objException,'HEP_Queue_Email_Alerts','setUpAdditionalUserRolesForEmailsNotification',null,null); 
				System.debug('Exception occured because of -->' + objException.getMessage() + ' @ Line Number-->' + objException.getLineNumber());
			}				
		}
		//[MP-1122][STARTS][Framework To support Additional Roles To Notify]
	}
	

    //Wrapper Class
    public class ApprovalNotificationWrapper {

        //For Notification  Alert
        public Id parentSobjectId;
        public String sNotificationTemplateName;
        public Set < Id > setNotifiedUsersId;

        //For Email template
        public Id recordApproverId;
        public String sEmailTemplateName;
        public Set < String > setToEmailAddress;
        //public String sEmailCCedUsers;
    }
}