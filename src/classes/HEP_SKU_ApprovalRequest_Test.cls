/**
 * Test_HEP_SKU_ApprovalRequest --- Test class for HEP_SKU_ApprovalRequest
 * @author : Abhishek Kumar
 */

@isTest(seeAllData = False)
private class HEP_SKU_ApprovalRequest_Test {
    
   /**
    * dataSetup --- Test data setup method 
    * areturn  : nothing
    * @author  : Abhishek Kumar
    */
    
    @testSetup static void dataSetup()
    {   
        HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Test_Data_Setup_Utility.createHEPServices();
        User objUser1 =  HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'ltest', 'ltest@abc.com', 'utest', 'ntest', 'atest', 'Fox - Catalog', true);
        User objUser2 =  HEP_Test_Data_Setup_Utility.createUser('HEP Standard User', 'ltest1', 'ltest1@abc.com', 'utest1', 'ntest1', 'atest1', 'Fox - Catalog', true);
        HEP_Territory__c objTerritory =  HEP_Test_Data_Setup_Utility.createHEPTerritory('Italy', 'EMEA', 'Subsidiary', Null, Null, 'USD', 'IT', '571', 'ESCO',TRUE);
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox - Catalog' , 'Catalog', true);
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Test Promo for Approval', 'Global', '', objTerritory.Id , objLOB.id, '', objUser1.ID, true);
        HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Test Template', objTerritory.Id, objLOB.Id, true);
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('16186', 'JANUARY MAN', 'Single', Null, objTerritory.Id, 'Approved', 'Master', true);
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objPromotion.Id, objSKUTemplate.Id, true);
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory.Id, 'TestSKUExt01', 'Test Title', 'Master', 'Physical', 'FOX', 'HD', true);
        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Approved', 'Unlocked', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Brand Manager', 'Products', true);
        HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('Product Unlock', 'HEP_Promotion_SKU__c', 'Locked_Status__c', 'Unlocked' , 'Locked', 'PRODUCT UNLOCK', 'Static', 'Approved', true);
        HEP_Approval_Type_Role__c objApprvalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalType.Id, objRole.Id, 'ALWAYS', true);
        HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Approved', objApprovalType.Id, objPromotionSKU.Id, true);
        HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApproval.Id, objUser1.Id, objRole.Id,'Approved',true);
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id, 'Test Grade', 20.99, true);
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKUMaster.Id, objTerritory.Id, objPromotionSKU.Id, Null, 'Active', true);

    }
    
    /**
     * testFetchSKUPromotionDetails --- Test method for successful fetching of field values from template
     * @return  : nothing
     * @author  : Abhishek Kumar
     */
    
    @isTest
    public static void testFetchSKUPromotionDetails(){
        
        HEP_Record_Approver__c objHEPRecordApprover = [Select Id from HEP_Record_Approver__c where Status__c = 'Approved' limit 1];
        Id recordApproverIdForSKUPromotion =objHEPRecordApprover.Id;
        HEP_SKU_ApprovalRequest objSKUApprovalRequest = new HEP_SKU_ApprovalRequest();
        objSKUApprovalRequest.recordApproverIdForSKUPromotion = recordApproverIdForSKUPromotion;
        HEP_SKU_ApprovalRequest.PriceRegionWrapper objPriceRegionWrapper = new HEP_SKU_ApprovalRequest.PriceRegionWrapper();
        HEP_SKU_ApprovalRequest.ApprovalEmailWrapper objApprovalWrapper = objSKUApprovalRequest.objApprovalwrapper;
        List<HEP_SKU_ApprovalRequest.ApprovalEmailWrapper> lstApprovalwrapper = objSKUApprovalRequest.lstApprovalwrapper;
        System.assert(objApprovalWrapper != Null);
        System.assert(objPriceRegionWrapper != Null);
    }
}