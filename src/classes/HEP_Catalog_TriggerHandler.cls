/**
 *HEP_Catalog_TriggerHandler --- Catalog Object trigger Handler
 *@author  Lakshman Jinnuri
 */
public class HEP_Catalog_TriggerHandler extends TriggerHandler{
    list<Id> lstIds = new list<Id>();
    
    /**
    * afterInsert -- Context specific afterInsert method
    * @return nothing
    */
    public override void afterInsert() {
       triggerEnqueueJob();
    }
    
    /**
    * afterUpdate -- Context specific afterUpdate method
    * @return nothing
    */
    public override void afterUpdate() {
    System.debug('check after update');
        Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Catalog__c', 'Catalog_Field_Set');
        System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);        
        if(checkFieldUpdate)
            triggerEnqueueJob();
    }

   /**
    * triggerEnqueueJob -- send the details to make the call out to Queueable class
    * @param  nothing
    * @return nothing  
    * @author Lakshman Jinnuri      
    */
    public void triggerEnqueueJob() {
    System.debug('check in enque job');
        for(SObject objSobj : Trigger.new){
            HEP_Catalog__c objCatalog = (HEP_Catalog__c)objSobj;
            lstIds.add(objCatalog.Id);
        }
        if(HEP_Constants__c.getValues('HEP_SIEBEL_CATALOG') != null && HEP_Constants__c.getValues('HEP_SIEBEL_CATALOG').Value__c != null){ 
            if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){    
                System.enqueueJob(new HEP_Siebel_Queueable(JSON.serialize(lstIds),HEP_Constants__c.getValues('HEP_SIEBEL_CATALOG').Value__c));
            }
        }
    }    
}