@isTest
public class HEP_My_Approvals_Panel_Controller_Test {

    @testSetup
     static void createUsers(){
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
        List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
        objRoleOperations.Destination_User__c = u.Id;
        objRoleOperations.Source_User__c = u.Id;
        insert objRoleOperations;
        HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
        objRoleFADApprover.Destination_User__c = u.Id;
        objRoleFADApprover.Source_User__c = u.Id;
        insert objRoleFADApprover;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_SpendDetail',true,10,10,'Inbound',true,true,true); 
        objInterface.Name = 'HEP_E1_SpendDetail';
        Update objInterface;
    }

    @isTest
    private static void testClass()
    {
    HEP_Promotion_MyApprovalsController controller = new HEP_Promotion_MyApprovalsController() ;
    }

    static testMethod void testApprovalData(){

        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleOperations = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Operations (International)'];

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

        System.runAs(u){
            Test.startTest();
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

            HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
            objSpend.Unique_Id__c = 'test unique id';
            insert objSpend;
            //objApprovalSpend.HEP_Market_Spend__c = objSpend.id;
            //update objApprovalSpend;


            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
            objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objUserRole;
            HEP_User_Role__c objUserRoleOperation = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleOperations.id, u.id, false);
            objUserRoleOperation.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
            insert objUserRoleOperation;

            HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'SPEND','Hierarchical', 'Pending', true);
            HEP_Approval_Type__c objApprovalTypeUnlockDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_dating__c', 'Locked_Status__c', 'Unlocked','Locked', 'DATING','Static', 'Pending', true);

            //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
            HEP_Approvals__c objApprovalUnlockDating = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockDating.id, null, true);

            HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockDating.id, objRoleOperations.id, 'ALWAYS', true);

            HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockDating.id, null, objRoleOperations.id, 'Pending', false);
            objRecordApprover.Search_Id__c = objRoleOperations.id+' / '+objNationalTerritory.Id;
            insert objRecordApprover;

            system.debug('user role -----> '+objUserRoleOperation);
            system.debug('record approver -----> '+objRecordApprover);

            objApprovalUnlockDating.HEP_Promotion_Dating__c = objDatingRecordNational.id;
            update objApprovalUnlockDating;

            String sUnlockDating = 'Unlock';

            HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
            Test.stopTest();
        }   
    }

    static testMethod void testApprovalData2(){
        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleFADApprover = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'FAD Approvers-Brand Manager'];

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

        System.runAs(u){
            Test.startTest();
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

            HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
            objSpend.Unique_Id__c = 'test unique id';
            insert objSpend;
            //objApprovalSpend.HEP_Market_Spend__c = objSpend.id;
            //update objApprovalSpend;


            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
            objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objUserRole;
            HEP_User_Role__c objUserRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRoleFADApprover.id, u.id, false);
            objUserRoleFADApprover.Search_Id__c = objRoleFADApprover.id+' / '+objNationalTerritory.Id;
            insert objUserRoleFADApprover;

            //HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'SPEND','Hierarchical', 'Pending', true);
            HEP_Approval_Type__c objApprovalTypeFAD = HEP_Test_Data_Setup_Utility.createApprovalType('Global FAD Override', 'HEP_Promotion_dating__c', 'FAD_Approval_Status__c', 'Approved','Rejected', 'FAD Override','Static', 'Pending', true);

            //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
            HEP_Approvals__c objApprovalFAD = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeFAD.id, null, true);

            HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeFAD.id, objRoleFADApprover.id, 'ALWAYS', true);

            HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalFAD.id, null, objRoleFADApprover.id, 'Pending', false);
            objRecordApprover.Search_Id__c = objRoleFADApprover.id+' / '+objNationalTerritory.Id;
            insert objRecordApprover;

            system.debug('user role -----> '+objUserRoleFADApprover);
            system.debug('record approver -----> '+objRecordApprover);

            objApprovalFAD.HEP_Promotion_Dating__c = objDatingRecordNational.id;
            update objApprovalFAD;
            
            String sFADDating = 'FAD';

            HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
            Test.stopTest();
        }
    }

    static testMethod void testApprovalData3(){
        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleFADApprover = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'FAD Approvers-Brand Manager'];

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

        System.runAs(u){
            Test.startTest();
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

            HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
            objSpend.Unique_Id__c = 'test unique id';
            insert objSpend;
            //objApprovalSpend.HEP_Market_Spend__c = objSpend.id;
            //update objApprovalSpend;


            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
            objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objUserRole;
            
            HEP_Approval_Type__c objApprovalTypeSpend = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Market_Spend__c', 'RecordStatus__c', 'Budget Approved','Rejected', 'SPEND','Hierarchical', 'Pending', true);
            //HEP_Approval_Type__c objApprovalTypeFAD = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Promotion_dating__c', 'FAD_Approval_Status__c', 'Approved','Rejected', 'FAD Override','Static', 'Pending', true);

            //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
            HEP_Approvals__c objApprovalSPEND = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, null, false);
            objApprovalSPEND.Record_ID__c = objSpend.id;
            insert objApprovalSPEND;

            HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeSpend.id, objRole.id, 'ALWAYS', true);

            HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalSPEND.id, null, objRole.id, 'Pending', false);
            objRecordApprover.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objRecordApprover;

            system.debug('user role -----> '+objUserRole);
            system.debug('record approver -----> '+objRecordApprover);

            objApprovalSPEND.HEP_Market_Spend__c = objSpend.id;
            update objApprovalSPEND;

            String sSpend = 'Budgets';

            HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
            Test.stopTest();
        }
    }

    static testMethod void testApprovalData4(){
        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleFADApprover = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'FAD Approvers-Brand Manager'];

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

        System.runAs(u){
            Test.startTest();
            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

            HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', '', 'Single', null, objNationalTerritory.Id, 'Pending', 'Master', true);

            HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objNationalPromotion.id, null, true);

            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objNationalTerritory.Id, '', 'Test Title', 'Master', '', 'VOD', 'DVD', true);

            HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.id, objSKUMaster.id, 'Pending', 'Pending',true);

            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
            objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objUserRole;
            
            HEP_Approval_Type__c objApprovalTypeUnlockProduct = HEP_Test_Data_Setup_Utility.createApprovalType('Product Unlock', 'HEP_Promotion_SKU__c', 'Locked_Status__c', 'Unlocked','Locked', 'PRODUCT UNLOCK','Static', 'Pending', true);
            //HEP_Approval_Type__c objApprovalTypeFAD = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Promotion_dating__c', 'FAD_Approval_Status__c', 'Approved','Rejected', 'FAD Override','Static', 'Pending', true);

            //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
            HEP_Approvals__c objApprovalUnlockProducts = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeUnlockProduct.id, null, true);

            HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeUnlockProduct.id, objRole.id, 'ALWAYS', true);

            HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalUnlockProducts.id, null, objRole.id, 'Pending', false);
            objRecordApprover.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objRecordApprover;

            system.debug('user role -----> '+objUserRole);
            system.debug('record approver -----> '+objRecordApprover);

            objApprovalUnlockProducts.HEP_Promotion_SKU__c = objPromotionSKU.id;
            update objApprovalUnlockProducts;

            String sUnlockProducts = 'Unlock Products';
            String sRejectComment = 'Test';

            list<String> lstRequestIds = new list<string>();
            lstRequestIds.add(objRecordApprover.id);

            HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
            Test.stopTest();
        }
    }

    static testMethod void testApprovalData5(){

        User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
        HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
        HEP_Role__c objRoleFADApprover = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'FAD Approvers-Brand Manager'];

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

        System.runAs(u){
            Test.startTest();
            //HEP_Promotion_MyApprovalsController.PendingApprovalsData objWrapperReturned = new HEP_Promotion_MyApprovalsController.PendingApprovalsData();
            //HEP_Promotion_MyApprovalsController.ApprovalRecord objApprovalWrapper = new HEP_Promotion_MyApprovalsController.ApprovalRecord();

            HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
            HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

            HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, objDatingMatrix.Id, true);
            HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

            HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('', '', 'Single', null, objNationalTerritory.Id, 'Pending', 'Master', true);

            HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objNationalPromotion.id, null, true);

            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objNationalTerritory.Id, '', 'Test Title', 'Master', '', 'VOD', 'DVD', true);

            HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.id, objSKUMaster.id, 'Pending', 'Pending',true);

            HEP_User_Role__c objUserRole = HEP_Test_Data_Setup_Utility.createHEPUserRole(objNationalTerritory.Id, objRole.id, u.id, false);
            objUserRole.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objUserRole;
            
            HEP_Approval_Type__c objApprovalTypeProductRequest = HEP_Test_Data_Setup_Utility.createApprovalType('DHE Product Request', 'HEP_Promotion_SKU__c', 'Approval_Status__c', 'Approved','Rejected', 'PRODUCT REQUEST','Hierarchical', 'Pending', true);
            //HEP_Approval_Type__c objApprovalTypeFAD = HEP_Test_Data_Setup_Utility.createApprovalType('Germany Spend', 'HEP_Promotion_dating__c', 'FAD_Approval_Status__c', 'Approved','Rejected', 'FAD Override','Static', 'Pending', true);

            //HEP_Approvals__c objApprovalSpend = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeSpend.id, '', true);
            HEP_Approvals__c objApprovalProductRequest = HEP_Test_Data_Setup_Utility.createApproval('Pending', objApprovalTypeProductRequest.id, null, true);

            HEP_Approval_Type_Role__c objApprovalTypeRole = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeProductRequest.id, objRole.id, 'ALWAYS', true);

            HEP_Record_Approver__c objRecordApprover = HEP_Test_Data_Setup_Utility.createRecordApprover(objApprovalProductRequest.id, null, objRole.id, 'Pending', false);
            objRecordApprover.Search_Id__c = objRole.id+' / '+objNationalTerritory.Id;
            insert objRecordApprover;

            system.debug('user role -----> '+objUserRole);
            system.debug('record approver -----> '+objRecordApprover);

            objApprovalProductRequest.HEP_Promotion_SKU__c = objPromotionSKU.id;
            update objApprovalProductRequest;

            HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objNationalTerritory.Id, '$55-$22', 1.00, true);

            HEP_SKU_Price__c objSKURegioPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMaster.id, objNationalTerritory.Id, objPromotionSKU.id, null, 'Active', true);

            String sProductRequest = 'Product Request';
            String sRejectComment = 'Test';

            list<String> lstRequestIds = new list<string>();
            lstRequestIds.add(objRecordApprover.id);

            
            
            //String sJSON = JSON.serialize(objWrapperReturned);

            HEP_My_Approvals_Panel_Controller.getPendingApprovalRecordsData();
            Test.stopTest();
        }
    }


}