@isTest
public class HEP_Navigation_Menu_Component_Test {

	@testSetup
 	 static void createUsers(){
	    
	    User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'nara', 'arjnarayanan@deloitte.com','arjnarayanan','arj','N','', true);
	    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
	    List<HEP_List_Of_Values__c> lstLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
	    HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
	    objRole.Destination_User__c = u.Id;
	    objRole.Source_User__c = u.Id;
	    insert objRole;
	    HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
	    objRoleOperations.Destination_User__c = u.Id;
	    objRoleOperations.Source_User__c = u.Id;
	    insert objRoleOperations;
	    HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
	    objRoleFADApprover.Destination_User__c = u.Id;
	    objRoleFADApprover.Source_User__c = u.Id;
	    insert objRoleFADApprover;
	    HEP_Test_Data_Setup_Utility.createHEPSpendDetailInterfaceRec();

	    
  	}

  	@isTest
    private static void testClass()
    {
	HEP_Navigation_Menu_Component_Controller controller = new HEP_Navigation_Menu_Component_Controller();
    }

    static testMethod void testNavComponent(){
    	//User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
    	User u = [SELECT Id,Name, DateFormat__c FROM User WHERE LastName = 'nara'];
    	HEP_Role__c objRole = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'Marketing Manager'];
    	HEP_Role__c objRoleFADApprover = [SELECT Id, Name FROM HEP_Role__c WHERE Source_User__c =: u.Id AND Name = 'FAD Approvers-Brand Manager'];

    	PermissionSet pr = [SELECT ID FROM PermissionSet where Label='HEP Permissions'];

    	PermissionSetAssignment ObjPermissionSetAssignment = new PermissionSetAssignment();
        ObjPermissionSetAssignment.AssigneeId = u.Id;
        ObjPermissionSetAssignment.PermissionSetId = pr.Id;
        insert ObjPermissionSetAssignment;
    	
    	System.runAs(u){
			Test.startTest();
			
			HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
	        HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, false);
	        objNationalTerritory.Flag_Country_Code__c = 'DE';
	        insert objNationalTerritory;

	        HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);

	        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
	        HEP_Line_Of_Business__c objLOBTV = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Dreamworks TV','TV',true);

	        EDM_REF_PRODUCT_TYPE__c objProductType = HEP_Test_Data_Setup_Utility.createEDMProductType('Feature','FEATR', true);
	    	EDM_GLOBAL_TITLE__c objTitleEDM = HEP_Test_Data_Setup_Utility.createTitleEDM('Test', '12345', 'PUBLC', objProductType.id, true);

	        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1 Test', 'Global', null,objTerritory.Id, objLOB.Id, null,null,false);
	        objPromotion.Domestic_Marketing_Manager__c = u.id;
	        insert objPromotion;
	        HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion.Id, objNationalTerritory.Id, objLOB.Id,'','', true);

	        HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objNationalTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

	        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('1234', objNationalPromotion.Id, 'Pending', true);

	        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('Test', '', 'Single', null, objNationalTerritory.Id, 'Pending', 'Master', true);

	        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.id, objNationalPromotion.id, null, true);

	        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.id, objNationalTerritory.Id, '1234', 'Test Title', 'Master', '', 'VOD', 'DVD', true);
	        
	        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.id, objSKUMaster.id, 'Pending', 'Pending',true);

	        HEP_MDP_Promotion_Product__c objMDPProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objNationalPromotion.Id, null, objNationalTerritory.Id, true);
			HEP_Navigation_Menu_Component_Controller.searchUniversal('Test');
			HEP_Navigation_Menu_Component_Controller.logError('test','test','test','test');
			HEP_Navigation_Menu_Component_Controller.getNotificationDetails();
			HEP_Navigation_Menu_Component_Controller.getPendingApprovalRecordsData();
			HEP_Navigation_Menu_Component_Controller.getUserMenuAccess();
			HEP_Navigation_Menu_Component_Controller.getNavData('report','report');
			Test.stopTest();
		}
    }

}