/**
* HEP_Expand_UserAccess_Batch_Test -- Test class for Expnsion of User Access
* @author    Deloitte
*/
@isTest 
private class HEP_Expand_UserAccess_Batch_Test{
    /**
    * CreateData --  Test method to get Create the Interface Records
    * @return  :  no return value
    */
    @testSetup
    public static void createTestDataSetup(){
    
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        
        User objTestUser1 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
		User objTestUser2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest2', 'ApprovalTestUser2@hep.com','ApprovalTest2','App2','ApTest2','', true);
	
		System.runAs (objTestUser1) {
	
    		//Territory Set up
		    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182','ESCO', true);
		    //Global Territory
		    HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
			
		    HEP_Role__c objRoleDefault = HEP_Test_Data_Setup_Utility.createHEPRole('Default Access', 'Promotions' ,true);
		    
		    List<HEP_User_Role__c > lstUserRole = new List<HEP_User_Role__c >();
		    HEP_User_Role__c objUserAccess1= HEP_Test_Data_Setup_Utility.createHEPUserRole(objGlobalTerritory.Id,objRoleDefault.Id,objTestUser1.Id,false);
		    lstUserRole.add(objUserAccess1);
		    HEP_User_Role__c objUserAccess2 = HEP_Test_Data_Setup_Utility.createHEPUserRole(objGlobalTerritory.Id,objRoleDefault.Id,objTestUser2.Id,false);
		    lstUserRole.add(objUserAccess2);	    
		    insert lstUserRole;
			
		}
        
    }
    
    @isTest 
    static void testExpandBatch() {
      
      Test.startTest();
      Database.executeBatch(new HEP_Expand_UserAccess_Batch(),10);
      Test.stopTest();
    } 
 }