@isTest
Public class HEP_MDP_ExportProducts_xlsx_Test{

    @testSetup 
    Static void setup(){
        

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;
        // list<HEP_Services__c> lstServices = new list<HEP_Services__c>();
        // HEP_Services__c objService = new HEP_Services__c();
        // objService.Name='Foxipedia_OAuth';
        // objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        // objService.Service_URL__c = '/v1/edf/events1';
        // lstServices.add(objService);
        // insert lstServices;
        // list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices(); 
        
        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('DHE','Domestic','Subsidiary',null, null,'EUR','USD','509',null,false);
        objTerritory.Flag_Country_Code__c = 'DH';
        objTerritory.MM_Territory_Code__c = 'DHE';
        insert objTerritory;
        HEP_Territory__c objTerritory2 = HEP_Test_Data_Setup_Utility.createHEPTerritory('US','Domestic','Subsidiary',null, null,'EUR','USD','510',null,false); 
        objTerritory2.Flag_Country_Code__c = 'US';
        objTerritory2.MM_Territory_Code__c = 'US';
        insert objTerritory2;
        
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Test User',objTerritory.id,'1111','MDP Customers',NUll,False);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        
        // HEP_Notification_Template__c objNotification = new HEP_Notification_Template__c(Name = 'Global_FAD_Date_Change',
        //                                                 HEP_Body__c = '<old Global FAD> updated to <new Global FAD>',HEP_Object__c = 'HEP_Promotion__c',
        //                                                 Record_Status__c ='Active',Type__c = 'FAD Date Change',Unique__c = 'Global_FAD_Date_Change',Status__c='-');
        

        // insert objNotification;

        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Customer 1', 'Customer', null,objTerritory.Id, null, null,null,false);
        objPromotion.Record_Status__c= 'Active'; 
        objPromotion.StartDate__c = date.today();
        objPromotion.EndDate__c = date.today()+20;
        objPromotion.Customer__c = objCustomer.id;
        insert objPromotion;
         HEP_Promotion__c objPromotionNational = HEP_Test_Data_Setup_Utility.createPromotion('Global National 1', 'National', null,objTerritory.Id, null, null,null,false);
        objPromotionNational.Record_Status__c= 'Active'; 
        objPromotionNational.StartDate__c = date.today();
        objPromotionNational.EndDate__c = date.today()+20;
        // objPromotion.Customer__c = objCustomer.id;
        insert objPromotionNational;
        // HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('12345',objPromotion.Id,'Active', false);
        // objSpend.RecordStatus__c ='Pending';
        // objSpend.RequestDate__c = System.today();
        // insert objSpend;
        // objSpend.RecordStatus__c ='Budget Approved';
        // Update objSpend;
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        objCatalog.Title_EDM__c = objTitle.id;
        objCatalog.Record_Status__c = 'Active';
        insert objCatalog;      

        HEP_MDP_Promotion_Product__c objPromotionProduct = HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,objTitle.id,objTerritory2.id,false);
        objPromotionProduct.Product_End_Date__c = date.today()+30;
        objPromotionProduct.Product_Start_Date__c = date.today(); 
        objPromotionProduct.Approval_Status__c = 'Approved';
        objPromotionProduct.Record_Status__c = 'Active';
        objPromotionProduct.HEP_Catalog__c = objCatalog.id;
        objPromotionProduct.customer_id__c = '694505519';
        insert objPromotionProduct;
        
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsHD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'HD','EST',false);
        objPromotionDetialsHD.Promo_WSP__c = 10;
        objPromotionDetialsHD.Promo_SRP__c = 10;
        objPromotionDetialsHD.MM_Contract_Name__c ='test12345';
        objPromotionDetialsHD.Record_Status__c = 'Active';
        objPromotionDetialsHD.CustomerId__c = '694505519';
        insert objPromotionDetialsHD;
        
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsSD = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'SD','EST',false);
        objPromotionDetialsSD.Promo_WSP__c = 10;
        objPromotionDetialsSD.Promo_SRP__c = 10;
        objPromotionDetialsSD.MM_Contract_Name__c ='test12345';
        objPromotionDetialsSD.Record_Status__c = 'Active';
        objPromotionDetialsSD.CustomerId__c = '694505519';
        insert objPromotionDetialsSD;
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetials4K = HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct.id,'4K','EST',false);
        objPromotionDetials4K.Promo_WSP__c = 10;
        objPromotionDetials4K.Promo_SRP__c = 10;
        objPromotionDetials4K.MM_Contract_Name__c ='test12345';
        objPromotionDetials4K.Record_Status__c = 'Active';
        objPromotionDetials4K.CustomerId__c = '694505519';
        insert objPromotionDetials4K;
        HEP_MDP_Promotion_Product__c objPromotionProduct1= HEP_Test_Data_Setup_Utility.createPromotionProduct(objPromotion.id,objTitle.id,objTerritory2.id,false);
        objPromotionProduct1.Product_End_Date__c = date.today()+30;
        objPromotionProduct1.Product_Start_Date__c = date.today(); 
        objPromotionProduct1.Approval_Status__c = 'Approved';
        objPromotionProduct1.Record_Status__c = 'Active';
        objPromotionProduct1.Title_EDM__c= objTitle.id;
        objPromotionProduct1.customer_id__c = '694505519';
        insert objPromotionProduct1; 
        
        HEP_MDP_Promotion_Product_Detail__c objPromotionDetialsHD1= HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct1.id,'HD','VOD',false);
        objPromotionDetialsHD1.Promo_WSP__c = 10;
         objPromotionDetialsHD1.Promo_SRP__c = 10;
        objPromotionDetialsHD1.MM_Contract_Name__c ='test12345';
        objPromotionDetialsHD1.Record_Status__c = 'Active';
        objPromotionDetialsHD1.CustomerId__c = '694505519';
        insert objPromotionDetialsHD1;
         HEP_MDP_Promotion_Product_Detail__c objPromotionDetials= HEP_Test_Data_Setup_Utility.createPromotionProductDetails(objPromotionProduct1.id,'4K',
        'VOD',false);
        objPromotionDetials.Promo_WSP__c = 10;
         objPromotionDetials.Promo_SRP__c = 10;
        objPromotionDetials.MM_Contract_Name__c ='test12345';
        objPromotionDetials.Record_Status__c = 'Active';
        objPromotionDetials.CustomerId__c = '694505519';
        insert objPromotionDetials;
        
       


        HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, false);
        objDatingMatrix.Dating_Flag__c=true;
        // objDatingMatrix.channel__c ='EST';
        insert objDatingMatrix;
        HEP_Promotions_DatingMatrix__c testdata = [SELECT channel__c
from HEP_Promotions_DatingMatrix__c where id=:objDatingMatrix.Id];
        system.debug('testdata'+testdata);
        HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory2.id,objPromotion.id,objDatingMatrix.id,False);
        objDatingRecord.Record_Status__c = 'Active';
        objDatingRecord.Status__c =  'Confirmed';
        objDatingRecord.Date_Type__c =  'EST Release Date';
        
        objDatingRecord.HEP_Catalog__c = objCatalog.id;
        objDatingRecord.Customer__c = objCustomer.id;
        insert objDatingRecord;
    //     	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    // 	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory2.Id, objTerritory2.Id, true);



//     	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);

//     	HEP_Promotion__c objPromotion123 = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion 1', 'Global', null,objTerritory.Id, objLOB.Id, null,null,true);
// 		HEP_Promotion__c objNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objPromotion123.Id, objTerritory.Id, objLOB.Id,'','', true);

// 		HEP_Promotion_Dating__c objDatingRecord = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory2.Id, objPromotion123.Id, objDatingMatrix.Id, true);
// 		HEP_Promotion_Dating__c objDatingRecordNational = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objNationalPromotion.Id, objDatingMatrixNational.Id, true);

// 		HEP_Market_Spend__c objSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('', objNationalPromotion.id, 'Pending', false);
// 		objSpend.Unique_Id__c = 'test unique id';
// 		insert objSpend;

        HEP_Promotions_DatingMatrix__c objDatingMatrix1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','VOD Release Date', objTerritory.Id, objTerritory.Id, false);
        objDatingMatrix1.Dating_Flag__c=true;
        // objDatingMatrix1.channel__c ='EST';
        insert objDatingMatrix1;
        
        HEP_Promotion_Dating__c objDatingRecord1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory2.id,objPromotionNational.id,objDatingMatrix1.id,false);
        objDatingRecord1.Record_Status__c = 'Active';
        objDatingRecord1.Status__c =  'Confirmed';
        objDatingRecord1.Date_Type__c =  'VOD Release Date';
        objDatingRecord1.HEP_Catalog__c = objCatalog.id;
        // objDatingRecord1.channel__c ='EST';
        objDatingRecord1.Customer__c = objCustomer.id;
        insert objDatingRecord1; 


        HEP_MM_ESTCustomerPricing__c objMMESTCustomerPricing = HEP_Test_Data_Setup_Utility.createMMESTCustomerPricing(NULL,NULL,'123',false);
        objMMESTCustomerPricing.CustomerID__c = '694505519';
        objMMESTCustomerPricing.Contract_Name__c ='test12345';
        objMMESTCustomerPricing.GLCountry__c = 'US';
        objMMESTCustomerPricing.MM_RateCardID__c = '123';
        objMMESTCustomerPricing.Resolution__c='HD';
        insert objMMESTCustomerPricing;   
     HEP_MM_ESTCustomerPricing__c objMMESTCustomerPricing1= HEP_Test_Data_Setup_Utility.createMMESTCustomerPricing(NULL,NULL,'123',false);
        objMMESTCustomerPricing1.CustomerID__c = '694505519';
        objMMESTCustomerPricing1.Contract_Name__c ='test12345';
        objMMESTCustomerPricing1.GLCountry__c = 'US';
        objMMESTCustomerPricing1.MM_RateCardID__c = '123';
        objMMESTCustomerPricing1.Resolution__c='HD';
        insert objMMESTCustomerPricing1;   


         HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id,'10', 768.00,False);
         objPriceGrade.Type__c ='MDP_EST';
         objPriceGrade.MM_RateCard_ID__c ='123';
         objPriceGrade.MDP_Duration_Start__c = '10';
         objPriceGrade.MDP_Duration_End__c = '20';
         objPriceGrade.MDP_PromoWSP__c = '111';
         insert objPriceGrade;


        HEP_List_Of_Values__c objlistValueHD = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='HD');
        insert objlistValueHD;
        

        HEP_List_Of_Values__c objlistValueSD = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='SD');
        insert objlistValueSD; 

        HEP_List_Of_Values__c objlistValue4k = new HEP_List_Of_Values__c(Values__c = '10',Order__c = '25',Type__c = 'MDP_EST_TIER',Parent_Value__c='4K');
        insert objlistValue4k;         

        HEP_List_Of_Values__c objlistValue1 = new HEP_List_Of_Values__c(Values__c = 'DERIVE:Product_Type__c',
            Order__c = '25', Type__c = 'MDP_EXCEL_EXPORT_REFERVALUE', Parent_Value__c='Standard');
        insert objlistValue1;  
        HEP_List_Of_Values__c objlistValue12= new HEP_List_Of_Values__c(Values__c = 'DERIVE:Product_Type__c',
            Order__c = '25', Type__c = 'MDP_EXCEL_EXPORT_INNERLOOKUP', Parent_Value__c='Amazon');
        insert objlistValue12;  
        HEP_List_Of_Values__c objlistValue13= new HEP_List_Of_Values__c(Values__c = 'DERIVE:Product_Type__c,HEP_MDP_Product_Mapping__r,Series_Title__c',
            Order__c = '25', Type__c = 'MDP_EXCEL_EXPORT_REFERVALUE', Parent_Value__c='Standard');
        insert objlistValue13;  

        HEP_List_Of_Values__c objlistValue2 = new HEP_List_Of_Values__c(Values__c = '{ "Values": [ { "Object_Api": "HEP_MDP_Product_Mapping__c", "Field_Api": "Local_Description__c" }, { "Object_Api": "HEP_MDP_Product_Mapping__c", "Field_Api": "Product_Start_Date__c" }, { "Object_Api": "HEP_MDP_Product_Mapping__c"',
            Order__c = '25', Type__c = 'Itunes', Parent_Value__c='Itunes');
        insert objlistValue2;  
                            

        HEP_SKU_Master__c objSKUMasterSD = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory2.Id, null, 'VOD SKU', 'Request', 'Physical', 'EST', 'SD', null);
        objSKUMasterSD.Record_Status__c = 'Active';
        insert objSKUMasterSD;

        HEP_SKU_Master__c objSKUMasterHD = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objTerritory2.Id, null, 'VOD SKU', 'Request', 'Physical', 'EST', 'HD', null);
        objSKUMasterHD.Record_Status__c = 'Active';
        insert objSKUMasterHD;      
        
       
        HEP_SKU_Price__c objSKUPriceSD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterSD.Id, objTerritory2.Id, null, null, 'Active', false);
        objSKUPriceSD.Start_Date__c = date.today()-30;
        insert objSKUPriceSD;       
        
        HEP_SKU_Price__c objSKUPriceHD = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.id, objSKUMasterHD.Id, objTerritory2.Id, null, null, 'Active', false);
        objSKUPriceHD.Start_Date__c = date.today()-30;
        insert objSKUPriceHD;


        HEP_MM_Contract__c objMMContract = HEP_Test_Data_Setup_Utility.createMMContract('US Test User',null,null,false);
        insert objMMContract;

        // list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_List_Of_Values__c> lstHEPListofValues = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
        
 
       list<HEP_INT_JDE_PromoType__c> objIntJdePromoType = HEP_Test_Data_Setup_Utility.createHepJDEPromoTypes();
        list<HEP_Services__c> objHEPServices1 = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_JDE_Promotion_Dating_Date_Type__c> objDateType = HEP_Test_Data_Setup_Utility.createHEPJDEPromotionDatingDateType(); 
        // HEP_Constants__c cp= new HEP_Constants__c();
        // cp.Name = 'HEP_ReferValue';
        // cp.Value = 'HEP_ReferValue';

    }
    
    @isTest 
    static void Test1(){   
    //   HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null,'USD','WW','1',null, true);
    // 	HEP_Territory__c objNationalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'EUR','DE','182',null, true);

    // 	HEP_Promotions_DatingMatrix__c objDatingMatrix = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objTerritory.Id, objTerritory.Id, true);
    // 	HEP_Promotions_DatingMatrix__c objDatingMatrixNational = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release','Release Dates','EST Release Date', objNationalTerritory.Id, objNationalTerritory.Id, true);



    // 	HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('New Release','New Release',true);
        Test.startTest();
        system.debug('1');
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c where PromotionName__c = 'Global Customer 1' limit 1];
        system.debug('12');
        Test.setCurrentPageReference(new PageReference('Page.HEP_ExportProductPage'));
        system.debug('13');
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        system.debug('14');
        System.currentPageReference().getParameters().put('type','Itunes');
        system.debug('15');
        HEP_MDP_ExportProducts_xlsx testObj = New  HEP_MDP_ExportProducts_xlsx();
        system.debug('16');
        Test.stoptest();
       
    }     

    @isTest 
    static void Test2(){   
       
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c where PromotionName__c = 'Global Customer 1' limit 1];
        HEP_MDP_Promotion_Product__c objPrmotionProduct = [select id from  HEP_MDP_Promotion_Product__c limit 1];
        objPrmotionProduct.Title_EDM__c = NULL;
        update objPrmotionProduct;
        Test.setCurrentPageReference(new PageReference('Page.HEP_ExportProductPage'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        System.currentPageReference().getParameters().put('type','Standard');
        HEP_MDP_ExportProducts_xlsx testObj = New  HEP_MDP_ExportProducts_xlsx();
        Test.stoptest();    
    }   

    @isTest 
    static void Test3(){   
       
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c where PromotionName__c = 'Global Customer 1' limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_ExportProductPage'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        System.currentPageReference().getParameters().put('type','Google');
        HEP_MDP_ExportProducts_xlsx testObj = New  HEP_MDP_ExportProducts_xlsx();
        Test.stoptest();
       
    }   
    @isTest 
    static void Test4(){   
       
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c where PromotionName__c = 'Global Customer 1' limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_ExportProductPage'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        System.currentPageReference().getParameters().put('type','Amazon');
        // HEP_Utility mp = new HEP_Utility();
        HEP_Utility.calculateEverydaySRP(objPromotion.Id);
        HEP_MDP_ExportProducts_xlsx testObj = New  HEP_MDP_ExportProducts_xlsx();
        Test.stoptest();
       
    } 
    @isTest 
    static void Test5(){  
       
        Test.startTest();
        HEP_Promotion__c objPromotion = [select id from HEP_Promotion__c where PromotionName__c = 'Global Customer 1' limit 1];
        Test.setCurrentPageReference(new PageReference('Page.HEP_ExportProductPage'));
        System.currentPageReference().getParameters().put('promoId',objPromotion.id);
        System.currentPageReference().getParameters().put('type','Itunes Premiere');
        HEP_MDP_ExportProducts_xlsx testObj = New  HEP_MDP_ExportProducts_xlsx();
        Test.stoptest();
       
    } 
}