/**
* HEP_E1_Spend_Conversion_Batch --- Class to perform one-time data sending to E1
* @author Kunal Choudhury
*/
public class HEP_E1_Spend_Conversion_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    String territory = '';
    Integer lim = 0;
    List<String> listPromoCodes = new List<String>();
    Map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRecord = new Map<String,HEP_Interface_Transaction__c>();

    //constructor to use when a list of Promo Codes need to be sent
    public HEP_E1_Spend_Conversion_Batch(List<String> listPromoCodes) {
        this.listPromoCodes = listPromoCodes;
    }
    //constructor to use when Promos to be sent by Territory
    public HEP_E1_Spend_Conversion_Batch(String teri, Integer lim) {
        this.territory = teri;
        this.lim = lim;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {

        String query;

        if(listPromoCodes.isEmpty()){
            List<HEP_Market_Spend__c> listAppSpend= [SELECT id, name, Promotion__c
                                                    from HEP_Market_Spend__c 
                                                    where RecordStatus__c = 'Budget Approved' 
                                                    and Promotion__r.Territory__r.Name = :territory];

            Set<String> setPromoIds = new Set<String>();
            for(HEP_Market_Spend__c spend:listAppSpend){
                setPromoIds.add(spend.Promotion__c);
            }

            query = 'SELECT id, Name from HEP_Promotion__c WHERE Territory__r.E1_Code__c != NULL';
            query += ' AND Territory__r.Name=:territory';
            query += ' AND Id IN :setPromoIds';

            if(lim!=null && lim!=0)
                query += ' LIMIT '+lim;
        }
        else{
            query = 'SELECT id, Name from HEP_Promotion__c WHERE Territory__r.E1_Code__c != NULL';
            query += ' AND LocalPromotionCode__c IN :listPromoCodes';
        }
        
        List<HEP_Promotion__c> listPromos = Database.Query(query);
        system.debug('generated query #### '+query);
        system.debug('returned records number #### '+listPromos.size());
        system.debug('returned records #### '+listPromos);

        String sHEP_E1_SpendDetail = HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'); 
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        HEP_Interface__c objInterfaceRec = [SELECT id FROM HEP_Interface__c WHERE Name =: sHEP_E1_SpendDetail]; 
        Map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRec = new Map<String,HEP_Interface_Transaction__c>();
        if(listPromos != NULL && !listPromos.isEmpty() && objInterfaceRec.Id != NULL){
            for(HEP_Promotion__c promo : listPromos)
            {
                HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
                objTxn.Object_Id__c = promo.Id;
                objTxn.HEP_Interface__c = objInterfaceRec.Id;
                objTxn.Transaction_Datetime__c = System.now();
                objTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
                mapPromoIdInterfaceRec.put(promo.Id,objTxn);
            }
            insert mapPromoIdInterfaceRec.Values();

            for(String promoId: mapPromoIdInterfaceRec.keySet()){
                mapPromoIdInterfaceRecord.put(promoId, mapPromoIdInterfaceRec.get(promoId));   
            }
            system.debug('mapPromoIdInterfaceRecord in start---- '+mapPromoIdInterfaceRecord);
        }    

        return DataBase.getQueryLocator(query);              
    }

    //Execute Method
    public void execute(Database.BatchableContext BC,List<Sobject>Scope) {

        system.debug('mapPromoIdInterfaceRecord in execute---- '+mapPromoIdInterfaceRecord);
        String sTransactionId = '';
        if(Scope!= NULL && !Scope.IsEmpty()){
            if(mapPromoIdInterfaceRecord.containsKey(Scope[0].id))
                sTransactionId = mapPromoIdInterfaceRecord.get(Scope[0].id).Id;

            System.debug('===sTransactionId==='+sTransactionId);
            if(sTransactionId  != NULL)
                HEP_ExecuteIntegration.executeIntegMethod(Scope[0].id,sTransactionId, HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'));  
        }       

        Long milliseconds = 5000;
        Long timeDiff;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);     
    }

    public void finish(Database.BatchableContext BC) {
    }   
}