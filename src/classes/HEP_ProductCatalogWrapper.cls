/**
* HEP_ProductCatalogWrapper -- Class to store Rating details 
* @author    Lakshman Jinnuri
*/
public class HEP_ProductCatalogWrapper{
    public list<data> data;
    public LinksOuter links;
    public String calloutStatus {get;set;}
    public String TxnId {get;set;}
    public list<Errors> errors { get; set; }      

    /**
    * Data -- Class to store Data details 
    * @author    Lakshman Jinnuri
    */
    public class Data {
        public Attributes attributes;
        public String id;   
        public String type; 
        public Relationships relationships;
    }

    /**
    * Attributes -- Class to store Attributes details 
    * @author    Lakshman Jinnuri
    */
    public class Attributes {
        public String type; 
        public String foxId;    
        public String primaryClassificationCode;
        public String productTypeCode;  
        public String productTypeDescription;   
        public String productId;    
        public String productName;
        public String productStatusCode;    
        public String productStatusDescription; 
        public String globalEpisodeConfigIndicator;
        public String productStatusComment; 
        public String productComment;   
        public String primaryCatalogId; 
        public String primaryCatalogName;   
        public String primaryClassificationDescription;
    }

    /**
    * Relationships -- Class to store Relationships details 
    * @author    Lakshman Jinnuri
    */
    public class Relationships {
        public title title;
    }

    /**
    * Title -- Class to store Title details 
    * @author    Lakshman Jinnuri
    */
    public class title {
        public Links links;
    }

    /**
    * Links -- Class to store Links details 
    * @author    Lakshman Jinnuri
    */
    public class Links {
        public String related;
    }

    /**
    * Links_Outer -- Class to store Links details 
    * @author    Lakshman Jinnuri
    */
    public class LinksOuter {
        public String self;
    }    

     /**
    * Errors -- Class to hold related Error details 
    * @author    Lakshman Jinnuri
    */
    public class Errors{
        public String title;
        public String detail;
        public Integer status; //400
    }
    
}