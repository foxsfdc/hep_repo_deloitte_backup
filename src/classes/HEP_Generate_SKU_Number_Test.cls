/* 
 @HEP_Generate_SKU_Number_Test ---Test Class for Smart SKU Number Algo.
 @author : Deloitte
 */

@isTest
public class HEP_Generate_SKU_Number_Test {
    
  @testSetup
  public static void createTestUserSetup(){
    //constant Data
    List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
    List<HEP_List_Of_Values__c> lstHEPLOV = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
    
    //User Setup
    User objTestUser = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest', 'ApprovalTestUser@hep.com','ApprovalTest','App','ApTest','', true);
    User objTestUser2 = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'AppTest2', 'ApprovalTestUser2@hep.com','ApprovalTest2','App2','ApTest2','', true);
    
    System.runAs (objTestUser) {
    
    //Territory Set up
    HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null, 'EUR', 'DE' , '182','ESCO', false);
    insert objTerritory;
     
    //Global Territory
    HEP_Territory__c objGlobalTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global','Domestic','Global',null, null, 'USD', 'WW' , '9000','NON-ESCO', true);
    
    //Insert Line of Business records
    HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox TV','TV',true);
    
    //Role Set Up FOR SPEND
    List<HEP_Role__c> lstRoles = new List<HEP_Role__c>();
    HEP_Role__c objRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPRole('Data Admin', 'Promotions' ,false);
    objRoleAdmin.Destination_User__c = objTestUser.Id;
    objRoleAdmin.Source_User__c = objTestUser.Id;  
    lstRoles.add(objRoleAdmin) ;

    insert lstRoles;
    
    //User role Set Up  
    List<HEP_User_Role__c > lstUserRole = new List<HEP_User_Role__c >();
    HEP_User_Role__c objUserRoleAdmin = HEP_Test_Data_Setup_Utility.createHEPUserRole(objTerritory.Id,objRoleAdmin.Id,objTestUser.Id,false);
    lstUserRole.add(objUserRoleAdmin);
    insert lstUserRole;
    
    
    //Create Global Promotion:
    HEP_Promotion__c objTestGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Global','Global','',objGlobalTerritory.Id,objLOB.Id,'','',true);
    //Create National Promotion
    HEP_Promotion__c objTestNationalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('The Darkest Mind-Germany NP','National',objTestGlobalPromotion.Id,objTerritory.Id,objLOB.Id,'','',true);
    
    //Notification Template Creation
    HEP_Notification_Template__c objGlobalPromoTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_Promotion_Creation','HEP_Promotion__c','A global promotion has been created','Global promotion creation','Active','-','Global_Promotion_Creation',true);
    HEP_Notification_Template__c objfadNotifTemplate = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change','HEP_Promotion__c','A new FAD is done','FAD Date Change','Active','-','Global_FAD_Date_Change',true);
    
    }
  }
  
  static TestMethod void testgenerateSmartSKUSimilar() {
        //Fetch the created Test User:
	  	User objTestUser = [Select 	Id,
	  								Name 
	  								from User 
	  								where Email = 'ApprovalTestUser@hep.com'];
	  	
	  	//fetch promotion:
	  	HEP_Promotion__c objTestPromotion = [Select Id,
	  												Territory__c,
	  												Name 
	  												from HEP_Promotion__c 
	  												where Promotion_Type__c ='National'];
	  	
	  	HEP_Territory__c objGerTerritory = [Select Id,
	  											Name 
	  											from HEP_Territory__c
	  											where Name = 'Germany'];
	  	
	  	HEP_Territory__c objTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium','EMEA','Subsidiary',objGerTerritory.Id, null, 'EUR', 'BE' , '49','ESCO', false);
    	insert objTerritoryRegion;
    	
	  	//Run in the context of User:
	  	System.runAs(objTestUser){
			
		  //Create Catalog Record
	      HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
	      objCatalog.Licensor_Type__c = 'Lucas';
	      objCatalog.Licensor__c = 'ABLO';
	      insert objCatalog;
	      
	      HEP_Catalog_Genre__c objCatGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , true);
	      
	      //Create Promotion Catalog Record
	      HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
	        
	       	//Create SKU master Record
	       	List<HEP_SKU_Master__c> lstSkuMaster = new List<HEP_SKU_Master__c>();
	       	for(Integer i= 1;i<10;i++){
	       		String skuNumber = 'D001234URG0'+i;
		       	HEP_SKU_Master__c objMasterSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, skuNumber, 'New SKU', 'Master', 'Digital', 'Rental', 'DVD', false);
			    objMasterSKU.Barcode__c = 'Barcode';
			    objMasterSKU.SKU_Compilation__c = 'SKUCompilation';
			    objMasterSKU.SKU_Permanent_Range__c ='NL (NL)';
			    objMasterSKU.Comments__c ='Test Comments';
			    objMasterSKU.SKU_Configuration__c = '4K';
			    objMasterSKU.Current_Release_Date__c = System.today();
			    objMasterSKU.INTL_Stock_in_Warehouse__c = 100;
			    objMasterSKU.Rental_Ready__c = 'RRD';
			    lstSkuMaster.add(objMasterSKU);
	       	}
		    
	       
	       
	       //Create SKU Request Record
		    HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '456', 'New SKU', 'Request', 'Digital', 'Rental', 'DVD', false);
		    objSKU.Barcode__c = 'Barcode';
		    objSKU.SKU_Compilation__c = 'SKUCompilation';
		    objSKU.SKU_Permanent_Range__c ='NL (NL)';
		    objSKU.Comments__c ='Test Comments';
		    objSKU.SKU_Configuration__c = '4K';
		    objSKU.Current_Release_Date__c = System.today();
		    objSKU.INTL_Stock_in_Warehouse__c = 100;
		    objSKU.Rental_Ready__c = 'RRD';
		    lstSkuMaster.add(objSKU);
		    insert lstSkuMaster;
		    
		    
    
        	//Create Promotion SKU record
    		HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,'Draft','Locked', true);  
			//Create Price Grade
			HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGerTerritory.Id,'$20-$30', 54, true);
			//SKU price 
			HEP_SKU_Price__c objSKUPrice = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                            	SKU_Master__c = objSKU.Id,
                                                            	Region__c = objGerTerritory.Id,
                                                           		Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                            	Record_Status__c ='Active');
            insert objSKUPrice;
			//Invoke Approval Process
			Test.startTest();
			HEP_Generate_SKU_Number.generateUniqueSKUNumber(new Set<Id>{objSKU.Id});
			Test.stopTest();
	  	}
    }
    
    static TestMethod void testgenerateSmartSKUUnique() {
        //Fetch the created Test User:
	  	User objTestUser = [Select 	Id,
	  								Name 
	  								from User 
	  								where Email = 'ApprovalTestUser@hep.com'];
	  	
	  	//fetch promotion:
	  	HEP_Promotion__c objTestPromotion = [Select Id,
	  												Territory__c,
	  												Name 
	  												from HEP_Promotion__c 
	  												where Promotion_Type__c ='National'];
	  	
	  	HEP_Territory__c objGerTerritory = [Select Id,
	  											Name 
	  											from HEP_Territory__c
	  											where Name = 'Germany'];
	  	
	  	//Run in the context of User:
	  	System.runAs(objTestUser){
			
		  //Create Catalog Record
	      HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
	      objCatalog.Licensor_Type__c = 'Lucas';
	      objCatalog.Licensor__c = 'ABLO';
	      insert objCatalog;
	      
	      HEP_Catalog_Genre__c objCatGenre1 = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
	      objCatGenre1.Genre__c = 'Action';
	      insert objCatGenre1;
	      
	      HEP_Catalog_Genre__c objCatGenre2 = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , false);
	      objCatGenre2.Genre__c = 'Drama';
	      insert objCatGenre2;
	      
	      //Create Promotion Catalog Record
	      HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
	       
	       //Create SKU Request Record
		    HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '456', 'New SKU', 'Request', 'Digital', 'Rental', 'DVD', false);
		    objSKU.Barcode__c = 'Barcode';
		    objSKU.SKU_Compilation__c = 'SKUCompilation';
		    objSKU.SKU_Permanent_Range__c ='NL (NL)';
		    objSKU.Comments__c ='Test Comments';
		    objSKU.SKU_Configuration__c = 'Dig_Ins';
		    objSKU.Current_Release_Date__c = System.today();
		    objSKU.INTL_Stock_in_Warehouse__c = 100;
		    objSKU.Rental_Ready__c = 'RRD';
		    insert objSKU;
		    
		    
    
        	//Create Promotion SKU record
    		HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,'Draft','Locked', true);  
			//Create Price Grade
			HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGerTerritory.Id,'$20-$30', 54, true);
			//SKU price 
			HEP_SKU_Price__c objSKUPrice = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                            	SKU_Master__c = objSKU.Id,
                                                            	Region__c = objGerTerritory.Id,
                                                           		Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                            	Record_Status__c ='Active');
            insert objSKUPrice;
			//Invoke Approval Process
			Test.startTest();
			HEP_Generate_SKU_Number.generateUniqueSKUNumber(new Set<Id>{objSKU.Id});
			Test.stopTest();
	  	}
    }
    
    
    static TestMethod void testgenerateSmartSKUGroupRegion() {
        //Fetch the created Test User:
	  	User objTestUser = [Select 	Id,
	  								Name 
	  								from User 
	  								where Email = 'ApprovalTestUser@hep.com'];
	  	
	  	//fetch promotion:
	  	HEP_Promotion__c objTestPromotion = [Select Id,
	  												Territory__c,
	  												Name 
	  												from HEP_Promotion__c 
	  												where Promotion_Type__c ='National'];
	  	
	  	HEP_Territory__c objGerTerritory = [Select Id,
	  											Name 
	  											from HEP_Territory__c
	  											where Name = 'Germany'];
	  	
	  	HEP_Territory__c objTerritoryRegion = HEP_Test_Data_Setup_Utility.createHEPTerritory('Belgium','EMEA','Subsidiary',objGerTerritory.Id, null, 'EUR', 'BE' , '49','ESCO', false);
    	insert objTerritoryRegion;
    	
    	HEP_Territory__c objTerritoryRegion2 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Austria','EMEA','Subsidiary',objGerTerritory.Id, null, 'EUR', 'AT' , '33','ESCO', false);
    	insert objTerritoryRegion2;
    	
	  	//Run in the context of User:
	  	System.runAs(objTestUser){
			
		  //Create Catalog Record
	      HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234','New Catalog','Single',null,objGerTerritory.Id, 'Approved', 'Master', false);
	      objCatalog.Licensor_Type__c = 'Lucas';
	      objCatalog.Licensor__c = 'ABLO';
	      insert objCatalog;
	      
	      HEP_Catalog_Genre__c objCatGenre = HEP_Test_Data_Setup_Utility.createCatalogGenre(objCatalog.Id , true);
	      
	      //Create Promotion Catalog Record
	      HEP_Promotion_Catalog__c objPromoCatalog =  HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id, objTestPromotion.Id, null, true);
	        
	       	//Create SKU master Record
	       	List<HEP_SKU_Master__c> lstSkuMaster = new List<HEP_SKU_Master__c>();
	       	HEP_SKU_Master__c objMasterSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, 'D001234URB101', 'New SKU', 'Master', 'Digital', 'RENTAL', 'DVD', false);
		    objMasterSKU.Barcode__c = 'Barcode';
		    objMasterSKU.SKU_Compilation__c = 'SKUCompilation';
		    objMasterSKU.SKU_Permanent_Range__c ='NL (NL)';
		    objMasterSKU.Comments__c ='Test Comments';
		    objMasterSKU.SKU_Configuration__c = '4K';
		    objMasterSKU.Current_Release_Date__c = System.today();
		    objMasterSKU.INTL_Stock_in_Warehouse__c = 100;
		    objMasterSKU.Rental_Ready__c = 'RRD';
		    lstSkuMaster.add(objMasterSKU);
		    
	       //Create SKU Request Record
		    HEP_SKU_Master__c objSKU = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id, objGerTerritory.Id, '456', 'New SKU', 'Request', 'Digital', 'Retail', 'DVD', false);
		    objSKU.Barcode__c = 'Barcode';
		    objSKU.SKU_Compilation__c = 'SKUCompilation';
		    objSKU.SKU_Permanent_Range__c ='NL (NL)';
		    objSKU.Comments__c ='Test Comments';
		    objSKU.SKU_Configuration__c = '4K';
		    objSKU.Current_Release_Date__c = System.today();
		    objSKU.INTL_Stock_in_Warehouse__c = 100;
		    objSKU.Rental_Ready__c = 'RRD';
		    lstSkuMaster.add(objSKU);
		    insert lstSkuMaster;
		    
		    
    
        	//Create Promotion SKU record
    		HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id, objSKU.Id,'Draft','Locked', true);  
			//Create Price Grade
			HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objGerTerritory.Id,'$20-$30', 54, true);
			//SKU price 
			
			HEP_SKU_Price__c objSKUPriceGermany = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                            	SKU_Master__c = objSKU.Id,
                                                            	Region__c = objGerTerritory.Id,
                                                           		Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                            	Record_Status__c ='Active');
            insert objSKUPriceGermany;                                              	
            HEP_SKU_Price__c objSKUPriceRegion = new HEP_SKU_Price__c(PriceGrade__c = objPriceGrade.Id,
                                                            	SKU_Master__c = objSKU.Id,
                                                            	Region__c = objTerritoryRegion.Id,
                                                           		Promotion_SKU__c = objPromotionSKU.Id,                                                        
                                                            	Record_Status__c ='Active');
            insert objSKUPriceRegion;
			//Invoke Approval Process
			Test.startTest();
			HEP_Generate_SKU_Number.generateUniqueSKUNumber(new Set<Id>{objSKU.Id});
			Test.stopTest();
	  	}
    }
}

//D001234URP