@isTest
public class hep_po_invoice_data_controller_Test{
    public static testmethod void getResponseTest(){
        String POResponse = '{"purchaseOrders": [{"OPEN": "0.00","RUSHCODE": "","POLINE_NO": "50000","NAME": "IMAGINE FULFILLMENT SERVICES","PO_NO": "27933","COUNTRY": "DHE","OBJECT_NO": "53202","ITEM_AMT": "1333.33","LINE_DESC": "STORAGE & FULFILLMENT SVCS","PROMO": "27933","ACTUAL": "0.00","REQ_DATE": "02/19/2017","POSTATUS": "915","NETWORKID": "CHRISBES","RUSHGPCODE": "","FORMAT": "910","REQUESTOR": "CHRIS BESS","VENDOR": "116258"},{"OPEN": "300.00","RUSHCODE": "","POLINE_NO": "4.000","NAME": "RDP Limited","PO_NO": "27933","COUNTRY": "DHE","OBJECT_NO": "630100","ITEM_AMT": "300.00","LINE_DESC": "Other Promo Costs-Misc","PROMO": "40550","ACTUAL": "0.00","REQ_DATE": "11/22/2016","POSTATUS": "Open","NETWORKID": "SARIKAR","RUSHGPCODE": "","FORMAT": null,"REQUESTOR": null,"POTYPE": "OR","VENDOR": "71389"}]}';
        test.startTest();
        hep_po_invoice_data_controller.getResponse(POResponse);
        hep_po_invoice_data_controller.lineData objLine = new hep_po_invoice_data_controller.lineData();
        hep_po_invoice_data_controller.headerData objHeader = new hep_po_invoice_data_controller.headerData();
        hep_po_invoice_data_controller.Department objDep = new hep_po_invoice_data_controller.Department();
        hep_po_invoice_data_controller.lineData objLineWrap = new hep_po_invoice_data_controller.lineData();
        objLineWrap.sLineNo  = 'aa';
        objLineWrap.sGLAccount = 'gl';
        objLineWrap.sLineDesc = 'desc';
        objLineWrap.sFormat = 'HD';
        objLineWrap.sLinestatus = 'ls';
        objLineWrap.sAmount = 'amt';
        objLineWrap.sActuals = 'actuals';
        objLineWrap.sOpenAmount = 'open';
        objLineWrap.sPoNum = 'po_no';
        objLineWrap.sCountry = 'country';
        
        hep_po_invoice_data_controller.headerData objHeaderWrap = new hep_po_invoice_data_controller.headerData();
        objHeaderWrap.sPoNum = 'ponum';
        objHeaderWrap.sStatus = 'status';
        objHeaderWrap.dtIssueData = System.today();
        objHeaderWrap.sAmount = 'amt';
        objHeaderWrap.sCurrency = 'inr';
        objHeaderWrap.sRequestor = 'me';
        objHeaderWrap.sGLAccount = '60100';
        objHeaderWrap.sFormat = 'SD';
        objHeaderWrap.sComments = 'comments';
        
        hep_po_invoice_data_controller.Department objDepWrap = new hep_po_invoice_data_controller.Department();
        objDepWrap.depName = 'depName';
        objDepWrap.sGLAccount = '60100';
        test.stopTest();


        HEP_purchaseOrderWrapper.PurchaseOrders objWrap910 = new HEP_purchaseOrderWrapper.PurchaseOrders();
        objWrap910.FORMAT = '910';

        HEP_purchaseOrderWrapper.PurchaseOrders objWrap950 = new HEP_purchaseOrderWrapper.PurchaseOrders();
        objWrap950.FORMAT = '950';

        HEP_purchaseOrderWrapper.PurchaseOrders objWrap960 = new HEP_purchaseOrderWrapper.PurchaseOrders();
        objWrap960.FORMAT = '960';

        HEP_purchaseOrderWrapper.PurchaseOrders objWrap980 = new HEP_purchaseOrderWrapper.PurchaseOrders();
        objWrap980.FORMAT = '980';

        List<HEP_purchaseOrderWrapper.PurchaseOrders> lstWraps = new List<HEP_purchaseOrderWrapper.PurchaseOrders>();
        lstWraps.add(objWrap980);
        lstWraps.add(objWrap910);
        lstWraps.add(objWrap950);
        lstWraps.add(objWrap960);

        HEP_purchaseOrderWrapper obj = new HEP_purchaseOrderWrapper();
        obj.purchaseOrders = lstWraps;

        hep_po_invoice_data_controller.getPODetails(obj);

        hep_po_invoice_data_controller.headerData obj1 = new hep_po_invoice_data_controller.headerData();
        obj1.lstLineData = null;
    }
}