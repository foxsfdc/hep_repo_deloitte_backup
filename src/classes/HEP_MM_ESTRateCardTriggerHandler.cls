/**
 *HEP_MM_ESTRateCardTriggerHandler --- Price Grade Object trigger Handler
 *@author  Lakshman Jinnuri
 */
public class HEP_MM_ESTRateCardTriggerHandler extends TriggerHandler{
    
    //static values referenced from constants object
    public static String sEST = HEP_Utility.getConstantValue('HEP_CHANNEL_EST');
    public static String sActive = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    public static String sESTType = HEP_Utility.getConstantValue('HEP_MDP_EST_TYPE');
    list<HEP_Price_Grade__c> lstPriceGrade = new list<HEP_Price_Grade__c>();
    
    /**
    * afterInsert -- Context specific afterInsert method
    * @return nothing
    */
    public override void afterInsert() {
        upsertPriceGradeRecords();
    }
    
    /**
    * afterUpdate -- Context specific afterUpdate method
    * @return nothing
    */
    public override void afterUpdate() {
        if(Trigger.oldMap != Null)
        {
            upsertPriceGradeRecords();
        }    
    }  

    /**
    *  -- send the details to make the call out to Queueable class
    * @param  nothing
    * @return nothing  
    * @author Lakshman Jinnuri      
    */
    public void upsertPriceGradeRecords() { 
        //Getting the Details from the LOV object for Product Sub Type
        String sTitleSubType = HEP_Utility.getConstantValue('FOX_TITLE_SUB_TYPE');
        String sFeature = HEP_Utility.getConstantValue('HEP_INT_JDE_FEATURE');
        List<String> lstTitleSubType = new List<String>{sTitleSubType};
        map<String, String> mapLOVMapping = HEP_Utility.getLOVMapping(lstTitleSubType, 'Name', 'Values__c');
        System.debug('mapLOVMapping'+mapLOVMapping);
        for(SObject objSobj : Trigger.new){
            System.debug('Statement 1---');
          HEP_MM_ESTRateCard__c objEstRateCard = (HEP_MM_ESTRateCard__c)objSobj;
          //if(objEstRateCard.Price__c != NULL && objEstRateCard.Retail__c != NULL){       
          if(objEstRateCard.Retail__c != NULL){            
            HEP_Price_Grade__c objPriceGrade = new HEP_Price_Grade__c();
            objPriceGrade.Channel__c = sEST;
            objPriceGrade.MDP_Duration_End__c = String.ValueOf(Integer.ValueOf(objEstRateCard.DurationEnd__c));
            objPriceGrade.MDP_Duration_Start__c = String.ValueOf(Integer.ValueOf(objEstRateCard.DurationStart__c));
            objPriceGrade.MDP_Product_Type__c = objEstRateCard.Product_Type__c;
            objPriceGrade.MDP_PromoWSP__c = String.ValueOf(objEstRateCard.Price__c.setScale(2));
            objPriceGrade.MM_RateCard_ID__c = String.ValueOf(Integer.ValueOf(objEstRateCard.MM_RateCardID__c));
            objPriceGrade.PriceGrade__c = String.ValueOf(objEstRateCard.Retail__c.setScale(2));
            //Kunal 8/5 - stamp DLP same as Price grade
            if(objPriceGrade.PriceGrade__c!=null)
                objPriceGrade.Dealer_List_Price__c = Decimal.valueOf(objPriceGrade.PriceGrade__c);
            objPriceGrade.Order__c = objEstRateCard.Retail__c*100;
            objPriceGrade.Record_Status__c = sActive;
            objPriceGrade.Type__c = sESTType;
            objPriceGrade.MM_RateCard_SFID__c = objEstRateCard.id;

            /*if(objEstRateCard.ProductSubType__c != null){
                String sLOVExists = sTitleSubType + '*' + objEstRateCard.ProductSubType__c;
                System.debug('sLOVExists'+sLOVExists);
                objPriceGrade.MDP_Product_Sub_Type__c = mapLOVMapping.containsKey(sLOVExists) ? mapLOVMapping.get(sLOVExists) : objEstRateCard.ProductSubType__c;
            }else*/
            if(objEstRateCard.ProductSubType__c == NULL || String.isEmpty(objEstRateCard.ProductSubType__c))
                 objPriceGrade.MDP_Product_Sub_Type__c = sFeature;
            else     
                objPriceGrade.MDP_Product_Sub_Type__c = objEstRateCard.ProductSubType__c;

            //objPriceGrade.Unique_Id__c = sESTType+'/'+''+'/'+ sEST +'/'+ ''+'/'+ objEstRateCard.Retail__c +'/'+ '' +'/'+ objEstRateCard.Product_Type__c +'/'+ objPriceGrade.MDP_Product_Sub_Type__c +'/'+ String.ValueOf(Integer.ValueOf(objEstRateCard.DurationStart__c)) +'/'+ String.ValueOf(Integer.ValueOf(objEstRateCard.DurationEnd__c)) +'/'+ objEstRateCard.Price__c +'/'+ String.ValueOf(Integer.ValueOf(objEstRateCard.MM_RateCardID__c));
            lstPriceGrade.add(objPriceGrade);
            System.debug('Statement 2----'+lstPriceGrade);
          }  
        }
        if(!lstPriceGrade.isEmpty()){
            System.debug('lstPriceGrade'+lstPriceGrade);
            Schema.SObjectField externalID = HEP_Price_Grade__c.Fields.MM_RateCard_SFID__c;
            Database.UpsertResult[] results =  Database.upsert(lstPriceGrade, externalID, false);
            System.debug('results :'+results);
        } 

    }
}