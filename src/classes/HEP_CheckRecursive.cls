public with sharing class HEP_CheckRecursive {
    public static boolean run = true;
    public static set <String> setOfIds = new set<String>();
    
    public static boolean runOnce() {
        if(run) {
            run = false;
            return true;
        } else {
            return run;
        }
    }
    // clearing the set before second update
    public static void clearSetIds()
    {
        setOfIds = new set<String>();
    }
    
    public static boolean runOnce(Map<Id,sObject> oldMap, Map<Id,sObject> newMap, Boolean isBefore) {
        Set<Id> oldLst = new Set<Id>();
        Set<Id> newLst = new Set<Id>();
        if( oldMap!=null)
            oldLst = oldMap.keySet(); 
        if( newMap!=null)
            newLst = newMap.keySet();
        return runOnceIds(oldLst, newLst, isBefore);
    }
    
    
    public static boolean runOnceIds(Set<Id> oldLst, Set<Id> newLst, Boolean isBefore) {
        Set<Id> setToProcess;
        if(oldLst.size()>0)
            setToProcess= oldLst;
        else
            setToProcess= newLst;
      
          for(String sId:setToProcess)
          {             
                if(setOfIds.contains(sId+isBefore)) {   
                    system.debug('setOfIds'+setOfIds + 'newLst ' +newLst.Size() + 'oldLst ' + oldLst.Size()+ 'FALSE' +isBefore+ ' ');        
                    return false;
                } else {
                    setOfIds.add(sId+isBefore);
                    //return true;
                }
          }
          system.debug('setOfIds'+setOfIds + 'newLst ' +newLst.Size() + 'oldLst ' + oldLst.Size()+ 'TRUE' +isBefore+ ' ');
          return true;
    }

}