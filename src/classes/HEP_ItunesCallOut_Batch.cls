global class HEP_ItunesCallOut_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    Map<Id, Sobject> objMap = new Map<Id,Sobject>();
    list<Id> lstObjId = new list<Id>();
    global HEP_ItunesCallOut_Batch(Map<Id, Sobject> mapObjID) {
        objMap = mapObjID;
        //for(id ids:mapObjID.Keyset()){
        //    lstObjId.add(ids);
        //}   
    }

    //Quuery method.
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(objMap != NULL && !objMap.keySet().IsEmpty()){
            //if(lstObjId[0].getSObjectType().getDescribe().getName() == HEP_Utility.getConstantValue('HEP_MDP_Promotion_Product_Detail')){        
            //    return DataBase.getQueryLocator([SELECT Id FROM HEP_MDP_Promotion_Product_Detail__c WHERE ID IN : objMap.keySet()]);          
            //}
            //else
            //{
            
            return DataBase.getQueryLocator([SELECT Id FROM HEP_MDP_Promotion_Product__c WHERE ID IN : objMap.keySet()]);
            
            //}
        } 
        return NULL;  
    }

    //Execute Method.
    global void execute(Database.BatchableContext BC,List<Sobject>Scope) {
        if(Scope!= NULL && !Scope.IsEmpty()){
             System.debug('===Scope==='+Scope);
             HEP_ExecuteIntegration.executeIntegMethod(Scope[0].id,'','HEP_iTunes_Price');  
        }       
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('Finished');
    }   
}