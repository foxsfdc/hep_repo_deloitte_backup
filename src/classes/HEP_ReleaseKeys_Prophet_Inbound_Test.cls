/**
 * ------------------------HEP_ReleaseKeys_Prophet_Inbound_Test---------------------------------
 * Author: Vishnu Raghunath
 *
 * */
@isTest (seeAllData = false)
public class HEP_ReleaseKeys_Prophet_Inbound_Test {
    @testSetup
    static void setupDate(){
        List < HEP_Constants__c > lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        
    }
    @isTest
    static void testPerformTransaction (){
        //Create LOB
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        
        //Creating Territory
        HEP_Territory__c Bulgaria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Bulgaria', 'EMEA', 'Licensee', null, null, 'BGN', 'BG', '31', null, false);
        Bulgaria.Send_to_Prophet__c = true;
        insert Bulgaria;
        HEP_Territory__c GlobalTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', null, false);
        GlobalTerr.Send_to_Prophet__c = true;
        insert GlobalTerr; 
        
        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', GlobalTerr.Id, Bulgaria.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
        insert VODRelease;
        System.debug('$%^ VODRelease.Date_Type__c ::'+VODRelease.Date_Type__c);
        
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Release Date', GlobalTerr.Id, Bulgaria.Id, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
        insert ESTRelease;
        
        HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TESTTAL', 'Single', null, Bulgaria.Id, 'Pending', 'Request', false);
        //System.debug('objCatalog *Y(*& '+ objCatalog.Id);
         
        //Creating a title ID
        EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
        EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadette', '197429', 'PUBLC', prodType.id, true);
        objCatalog.TITLE_EDM__c = Title.Id;
        insert objCatalog;
        
        //Setting up Promotion and dating records
        HEP_CheckRecursive.clearSetIds();
        
        HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'National', null, Bulgaria.id, FoxNewRelease.Id, null, null, false);
        testPromo.Title__c = Title.Id;
        String FADDate = '2018-07-12';
        testPromo.FirstAvailableDate__c = Date.valueOf(FADDate); 
        System.debug('testPromo.FirstAvailableDate__c *&** '+ testPromo.FirstAvailableDate__c);
        insert testPromo;
        
        //Create dating records 
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, VODRelease.id, false);
        DatingRec1.Status__c = 'Projected';
        //DatingRec1.DateType__c = 'VOD Release Date';
        DatingRec1.Date__c = Date.valueOf('2018-07-03');
        DatingRec1.HEP_Catalog__c = objCatalog.Id;
        insert DatingRec1;
        
        HEP_Promotion_Dating__c date1 = [SELECT Id,Date__c,HEP_Catalog__c,DateType__c,LastModifiedDate,Territory__r.Send_to_Prophet__c FROM HEP_Promotion_Dating__c WHERE Id =: DatingRec1.Id];
        update date1;
        System.debug('$%^ VODRelease.DateType__c ::'+date1);
        System.debug('$%^ VODRelease.DateType__c.Date__c ::'+date1.Date__c);
        System.debug('$%^ VODRelease.DateType__c.HEP_Catalog__c ::'+date1.HEP_Catalog__c);
        System.debug('$%^ VODRelease.DateType__c.DateType__c ::'+date1.DateType__c);
        System.debug('$%^ VODRelease.DateType__c.LastModified ::'+date1.LastModifiedDate);
        System.debug('$%^ VODRelease.DateType__c.LastModified ::'+date1.Territory__r.Send_to_Prophet__c);
        
        HEP_Promotion_Dating__c DatingRec2 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, ESTRelease.id, false);
        DatingRec2.Status__c = 'Projected';
        //DatingRec2.DateType__c = 'EST Release Date';
        DatingRec2.Date__c = Date.valueOf('2019-07-10');
        DatingRec2.HEP_Catalog__c = objCatalog.Id;
        insert DatingRec2; 
        
        HEP_Promotion_Dating__c date2 = [SELECT Id,Date__c,HEP_Catalog__c,DateType__c,LastModifiedDate,Territory__r.Send_to_Prophet__c FROM HEP_Promotion_Dating__c WHERE Id =: DatingRec2.Id];
        update date2;
        System.debug('$%^ ESTRelease.DateType__c ::'+date2);
        System.debug('$%^ ESTRelease.DateType__c.Date__c ::'+date2.Date__c);
        System.debug('$%^ ESTRelease.DateType__c.HEP_Catalog__c ::'+date2.HEP_Catalog__c);
        System.debug('$%^ ESTRelease.DateType__c.DateType__c ::'+date2.DateType__c);
        System.debug('$%^ ESTRelease.DateType__c.LastModified ::'+date2.LastModifiedDate);
        System.debug('$%^ ESTRelease.DateType__c.LastModified ::'+date2.Territory__r.Send_to_Prophet__c);
        
        
        System.debug(';;; '+ DatingRec2.DateType__c);       
       
        HEP_InterfaceTxnResponse objWrap = new HEP_InterfaceTxnResponse();
        objWrap.sRequest = '2017-07-20T00:00:00Z';
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        HEP_ReleaseKeys_Prophet_Inbound proph = new HEP_ReleaseKeys_Prophet_Inbound();
        proph.performTransaction(objWrap);
        System.debug('String value lmd :: '+ objWrap.sRequest +'  Date value lmd'+Date.valueOf(objWrap.sRequest));
        objWrap.sRequest = '2017-06-01T00:00:00Z';
        HEP_ReleaseKeys_Prophet_Inbound proph1 = new HEP_ReleaseKeys_Prophet_Inbound();
        System.debug('String value lmd :: '+ objWrap.sRequest +'  Date value lmd'+Date.valueOf(objWrap.sRequest));
        proph1.performTransaction(objWrap);
        Test.stopTest();
        
    }
}