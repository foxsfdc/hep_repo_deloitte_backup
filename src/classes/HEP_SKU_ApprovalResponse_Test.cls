@isTest
public class HEP_SKU_ApprovalResponse_Test{
    public static testmethod void fetchSKUPromotionDetails_Test(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Territory Record
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'canada';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        insert objTerritory;
        //Create Promotion Dating Matrix record
        HEP_Promotions_DatingMatrix__c objPromoDatingMtrx = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
        objPromoDatingMtrx.Media_Type__c = 'Digital';
        insert objPromoDatingMtrx;
        //Create Promotion Dating Matrix record
        HEP_Promotions_DatingMatrix__c objPromoDatingMtrx1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
        objPromoDatingMtrx1.Media_Type__c = 'Physical';
        insert objPromoDatingMtrx1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        insert objPromotion1;
        //Create Catalog Record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
        objSKUMaster.Record_Status__c  = 'Active'; 
        insert objSKUMaster;
        HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Tst',objTerritory.Id,null,false);
        insert objSKUTemplate;
        //Create Promotion Catalog Record
        HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemplate.Id,false);
        insert objPromoCatalog;
        //Create Promotion SKU Record
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Pending','Unlocked',false);
        insert objPromoSKU;
        //Creating Promotion Dating record
        HEP_Promotion_Dating__c objPromoDating = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory1.Id,objPromotion1.Id,objPromoDatingMtrx1.Id,false);
        objPromoDating.Date__c = System.today();
        insert objPromoDating;
        //Creating Promotion Dating record - Digital
        HEP_Promotion_Dating__c objPromoDating1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id,objPromotion1.Id,objPromoDatingMtrx.Id,false);
        objPromoDating1.Date__c = System.today();
        //Digital_Physical__c
        insert objPromoDating1;
        HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test Approval type','tst obj','Approval_Status__c','Approved','Rejected','MDP CUSTOMER','Hierarchical','Submitted',false);
        insert objApprovalType;
        ///SKU Master Record with Media Type
        List<HEP_SKU_Master__c> lstSKUMaster = [SELECT Catalog_Master__c,Territory__c,SKU_Number__c,
                                                SKU_Title__c,Type__c,SKU_Type__c,Channel__c,Format__c,
                                                Record_Status__c,Media_Type__c 
                                                FROM HEP_SKU_Master__c 
                                                WHERE Id =: objSKUMaster.Id];
        System.debug('*********' + lstSKUMaster);
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMasterP = new HEP_SKU_Master__c();
        objSKUMasterP = lstSKUMaster[0];
        objSKUMasterP.Id = null;
        objSKUMasterP.SKU_Number__c = '99';
        objSKUMasterP.SKU_Type__c = 'tst';
        objSKUMasterP.Format__c = '4K';
        System.debug('objSKUMasterP' + objSKUMasterP);
        insert objSKUMasterP;
        // Create Approval record
        HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending',objApprovalType.Id,objPromoSKU.Id,false);
        objApproval.HEP_Promotion_SKU__c = objPromoSKU.Id;
        insert objApproval;
        //SKU Price Grade
        HEP_Price_Grade__c  objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id,'01',99.99,false);
        insert objPriceGrade;
        //SKU Price Record
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id,objSKUMasterP.Id,objTerritory.Id,objPromoSKU.Id,objPromoDating.Id,'Active',false);
        insert objSKUPrice;
        //Role for approver
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('HEP_MARKETING_MANAGER','',false);
        insert objRole;
        //SKU Customer 
        HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id,objSKUMasterP.Id,false);
        insert objSKUCustomer;
        //Create Record Approver 
        HEP_Record_Approver__c objRecApprover = HEP_Test_Data_Setup_Utility.createHEPRecordApprover(objApproval.Id,u.Id,false);
        objRecApprover.Approver_Role__c = objRole.Id;
        insert objRecApprover;
        //Create Outbound Email Record
        HEP_Outbound_Email__c objOutEmail = new HEP_Outbound_Email__c();
        objOutEmail.HEP_Promotion__c = objPromotion1.Id;
        objOutEmail.Record_Id__c = objRecApprover.Id;
        objOutEmail.Email_Sent__c = false;
        objOutEmail.Email_Template_Name__c = 'HEP_SKU_Activation_Request';
        objOutEmail.Object_API__c = objSKUCustomer.Id; //Adding any random Id to required field
        insert objOutEmail;
        test.startTest();
        HEP_SKU_ApprovalResponse objAppResponse = new HEP_SKU_ApprovalResponse();
        objAppResponse.recordApproverIdForSKUPromotion = objRecApprover.Id;
        objAppResponse.fetchSKUPromotionDetails();
        test.stopTest();
    }
    public static testmethod void fetchSKUPromotionDetails_Test1(){
        //HEP Constants
        List<HEP_Constants__c> lstConstants = Test.loadData(HEP_Constants__c.sObjectType,'testHEPConstant'); 
        //Create Test User
        User u = HEP_Test_Data_Setup_Utility.createUser('Standard User','Deloitte','fox@deloitte.com','foxTest','21cf','fox21c','MGM - Bond',true);
        //Create HEP Territory Record
        HEP_Territory__c objTerritory1 = new HEP_Territory__c();
        objTerritory1.Name = 'US';
        objTerritory1.Region__c = 'APAC';
        objTerritory1.Type__c = 'Subsidiary';
        objTerritory1.CurrencyCode__c = 'USD';
        objTerritory1.ERMTerritoryCode__c = 'Test Code122';
        objTerritory1.Territory_Code_Integration__c = '001';
        objTerritory1.MM_Territory_Code__c = 'abc';
        insert objTerritory1;
        //Create HEP Territory Record
        HEP_Territory__c objTerritory = new HEP_Territory__c();
        objTerritory.Name = 'canada';
        objTerritory.Region__c = 'APAC';
        objTerritory.Type__c = 'Subsidiary';
        objTerritory.CurrencyCode__c = 'AUD';
        objTerritory.ERMTerritoryCode__c = 'Test Code';
        objTerritory.Territory_Code_Integration__c = '000';
        objTerritory.MM_Territory_Code__c = 'a';
        insert objTerritory;
        //Create Promotion Dating Matrix record
        HEP_Promotions_DatingMatrix__c objPromoDatingMtrx = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
        objPromoDatingMtrx.Media_Type__c = 'Digital';
        insert objPromoDatingMtrx;
        //Create Promotion Dating Matrix record
        HEP_Promotions_DatingMatrix__c objPromoDatingMtrx1 = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('Catalog','Promotion','End Date',objTerritory.Id,objTerritory.Id,false);
        objPromoDatingMtrx1.Media_Type__c = 'Physical';
        insert objPromoDatingMtrx1;
        //Create HEP Customer 
        HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Amazon',objTerritory.Id,'4','MDP Customers','',false);
        objCustomer.Record_Status__c = 'Active';
        insert objCustomer;
        //Create Promotion Record
        HEP_Promotion__c objPromotion1 = new HEP_Promotion__c();
        objPromotion1.PromotionName__c = 'Promo';
        objPromotion1.Promotion_Type__c  = 'Customer';
        objPromotion1.Record_Status__c  = 'Active';
        objPromotion1.Requestor__c = u.Id;
        objPromotion1.Customer__c = objCustomer.Id;
        objPromotion1.Record_Status__c = 'Active';
        insert objPromotion1;
        //Create Catalog Record
        HEP_Catalog__c objCatalog = new HEP_Catalog__c();
        objCatalog.Catalog_Name__c = 'Primary Catalog';
        objCatalog.Product_Type__c = 'TV';
        objCatalog.Catalog_Type__c = 'Bundle';
        objCatalog.Territory__c = objTerritory.Id;  
        objCatalog.Record_Status__c  = 'Active';
        objCatalog.Status__c = 'Draft';
        objCatalog.Type__c = 'Master';
        insert objCatalog;
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCatalog.Id,objTerritory.Id,'090','Test Title','Master','SKU Type','VOD','HD',false);
        objSKUMaster.Record_Status__c  = 'Active'; 
        insert objSKUMaster;
        HEP_SKU_Template__c objSKUTemplate = HEP_Test_Data_Setup_Utility.createSKUTemplate('Tst',objTerritory.Id,null,false);
        insert objSKUTemplate;
        //Create Promotion Catalog Record
        HEP_Promotion_Catalog__c objPromoCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCatalog.Id,objPromotion1.Id,objSKUTemplate.Id,false);
        insert objPromoCatalog;
        //Create Promotion SKU Record
        HEP_Promotion_SKU__c objPromoSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromoCatalog.Id,objSKUMaster.Id,'Pending','Locked',false);
        insert objPromoSKU;
        //Creating Promotion Dating record
        HEP_Promotion_Dating__c objPromoDating = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory1.Id,objPromotion1.Id,objPromoDatingMtrx1.Id,false);
        objPromoDating.Date__c = System.today();
        insert objPromoDating;
        //Creating Promotion Dating record - Digital
        HEP_Promotion_Dating__c objPromoDating1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id,objPromotion1.Id,objPromoDatingMtrx.Id,false);
        objPromoDating1.Date__c = System.today();
        //Digital_Physical__c
        insert objPromoDating1;
        HEP_Approval_Type__c objApprovalType = HEP_Test_Data_Setup_Utility.createApprovalType('test Approval type','tst obj','Approval_Status__c','Approved','Rejected','MDP CUSTOMER','Hierarchical','Submitted',false);
        insert objApprovalType;
        ///SKU Master Record with Media Type
        List<HEP_SKU_Master__c> lstSKUMaster = [SELECT Catalog_Master__c,Territory__c,SKU_Number__c,
                                                SKU_Title__c,Type__c,SKU_Type__c,Channel__c,Format__c,
                                                Record_Status__c,Media_Type__c 
                                                FROM HEP_SKU_Master__c 
                                                WHERE Id =: objSKUMaster.Id];
        System.debug('*********' + lstSKUMaster);
        //Create SKU Master Record
        HEP_SKU_Master__c objSKUMasterP = new HEP_SKU_Master__c();
        objSKUMasterP = lstSKUMaster[0];
        objSKUMasterP.Id = null;
        objSKUMasterP.SKU_Number__c = '99';
        objSKUMasterP.SKU_Type__c = 'tst';
        objSKUMasterP.Format__c = '4K';
        System.debug('objSKUMasterP' + objSKUMasterP);
        insert objSKUMasterP;
        // Create Approval record
        HEP_Approvals__c objApproval = HEP_Test_Data_Setup_Utility.createApproval('Pending',objApprovalType.Id,objPromoSKU.Id,false);
        objApproval.HEP_Promotion_SKU__c = objPromoSKU.Id;
        insert objApproval;
        //SKU Price Grade
        HEP_Price_Grade__c  objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id,'01',99.99,false);
        insert objPriceGrade;
        //SKU Price Record
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id,objSKUMasterP.Id,objTerritory.Id,objPromoSKU.Id,objPromoDating.Id,'Active',false);
        insert objSKUPrice;
        //Role for approver
        HEP_role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('HEP_MARKETING_MANAGER','',false);
        insert objRole;
        //SKU Customer 
        HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id,objSKUMasterP.Id,false);
        insert objSKUCustomer;
        //Create Record Approver 
        HEP_Record_Approver__c objRecApprover = HEP_Test_Data_Setup_Utility.createHEPRecordApprover(objApproval.Id,u.Id,false);
        objRecApprover.Approver_Role__c = objRole.Id;
        insert objRecApprover;
        //Create Outbound Email Record
        HEP_Outbound_Email__c objOutEmail = new HEP_Outbound_Email__c();
        objOutEmail.HEP_Promotion__c = objPromotion1.Id;
        objOutEmail.Record_Id__c = objRecApprover.Id;
        objOutEmail.Email_Sent__c = false;
        objOutEmail.Email_Template_Name__c = 'HEP_SKU_Activation_Request';
        objOutEmail.Object_API__c = objSKUCustomer.Id; //Adding any random Id to required field
        insert objOutEmail;
        test.startTest();
        HEP_SKU_ApprovalResponse objAppResponse = new HEP_SKU_ApprovalResponse();
        objAppResponse.recordApproverIdForSKUPromotion = objRecApprover.Id;
        HEP_SKU_ApprovalResponse.ApprovalEmailWrapper objApprvlEmailWrap = new HEP_SKU_ApprovalResponse.ApprovalEmailWrapper();
        objApprvlEmailWrap.sReleaseDate = String.valueOf(System.today());
        objApprvlEmailWrap.sRegion = 'US';
        objApprvlEmailWrap.sPriceGrade = '99.99';
        objApprvlEmailWrap.iIndex = 99;
        objAppResponse.fetchSKUPromotionDetails();
        test.stopTest();
    }
}