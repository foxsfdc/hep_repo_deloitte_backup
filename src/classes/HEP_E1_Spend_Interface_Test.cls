@isTest
public class HEP_E1_Spend_Interface_Test {
    @isTest static void HEP_E1_Spend_Interface_SuccessTest()
    {
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_Constants__c> lstConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Constants__c con = new HEP_Constants__c();
        con.Name = 'HEP_E1_SPEND_DETAIL';
        con.Value__c = 'HEP_E1_SpendDetail';
        insert con;
        
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_Spend_Interface',true,10,10,'Outbound-Push',true,true,true);
        objInterface.Name = 'HEP_E1_SpendDetail';
        update objInterface;

        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',null);
        insert objLOB;

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'AED','DE','182','ESCO',null);
        objTerritory.E1_Code__c = '340';
        insert objTerritory;
        
        HEP_Territory__c objTerritory1 = HEP_Test_Data_Setup_Utility.createHEPTerritory('Australia', 'EMEA', 'Subsidiary', null, null, 'ARS', 'AUS', 'AUS', '', false);
        objTerritory1.E1_Code__c = '410';
        insert objTerritory1;

        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',null);
        objGlobalPromotion.Title__c = objTitle.Id;
        objGlobalPromotion.LocalPromotionCode__c = '12345';
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',null);
        objPromotion.Title__c = objTitle.Id;
        objPromotion.LocalPromotionCode__c = '12345678';
        insert objPromotion;
        System.debug('objPromotion>> ' + objPromotion);
        
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id, null);
        insert objPromotionRegion;


        HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        insert objCat;
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerritory.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
        objSKUMaster.Revenue_Share__c = False;
        insert objSKUMaster;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
        insert objPromotionCatalog;

        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', '', false);
        objPromotionSKU.Approval_Status__c = 'Approved';
        objPromotionSKU.Legacy_Id__c = 'ABC123';
        objPromotionSKU.Release_Id__c = '1234';
        insert objPromotionSKU;

        HEP_GL_Account__c objGl = HEP_Test_Data_Setup_Utility.createGLAccount('SampleName', '182638', 'Sample GL Account For Testing', 'Marketing Spend', 'Active', false);
        insert objGl;

        

       HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Germany Customers',objTerritory.Id, '124', 'Dating Customers', 'VOD Release Date', true);
        
                
        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', objTerritory.Id, objTerritory.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
         VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Release Date', objTerritory.Id, objTerritory.Id, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
         ESTRelease.Media_Type__c = 'Digital';
        insert ESTRelease;
        HEP_Promotions_DatingMatrix__c PhysicalRetailRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'Physical Release Date (Retail)', objTerritory.Id, objTerritory.Id, false);
         PhysicalRetailRelease.Media_Type__c = 'Physical';
        PhysicalRetailRelease.Projected_FAD_Logic__c = 'Monday';
        insert PhysicalRetailRelease;
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'VOD Avail Date', objTerritory.Id, objTerritory.Id, false);
         VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset__c = 30;
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
        HEP_Promotions_DatingMatrix__c ESTAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'EST Avail Date', objTerritory.Id, objTerritory.Id, false);
         ESTAvail.Media_Type__c = 'Digital';
        ESTAvail.Date_Offset__c = 30;
        ESTAvail.Date_Offset_Source__c = 'EST Release Date';
        insert ESTAvail;
        
        //Create dating records 
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, VODRelease.id, false);
        DatingRec1.HEP_Catalog__c = objCat.Id;
        DatingRec1.Customer__c = objCustomer.Id;
        DatingRec1.Status__c = 'Tentative';
        insert DatingRec1;
        
        HEP_Promotion_Dating__c DatingRec2 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, ESTRelease.id, false);
       
        DatingRec2.Status__c = 'Projected';
        insert DatingRec2;
        
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, PhysicalRetailRelease.id, false);
        DatingRec3.Status__c = 'Projected';
        insert DatingRec3;
        
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Projected';
        insert DatingRec4;
        
        HEP_Promotion_Dating__c DatingRec5 = HEP_Test_Data_Setup_Utility.createDatingRecord(objTerritory.Id, objPromotion.Id, ESTAvail.id, false);
        DatingRec5.Status__c = 'Projected';
        insert DatingRec5;
       
       
        HEP_Price_Grade__c objPriceGrade = HEP_Test_Data_Setup_Utility.createHEPPriceGrade(objTerritory.Id, '$44.95 - $31.50', 44.95, false);
        insert objPriceGrade;
        
         
        
        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154', objPromotion.Id, 'Pending', false);
        insert objMarketSpend;
        objMarketSpend.RecordStatus__c = 'Budget Approved';
        update objMarketSpend;
        
        HEP_SKU_Price__c objSKUPrice = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKUMaster.Id, objTerritory.Id, objPromotionSKU.Id, null, 'Active', false);
        objSKUPrice.Changed_Flag__c = true;
        objSKUPrice.Promotion_Dating__c = DatingRec1.Id;
        objSKUPrice.Unit__c = 10;
        insert objSKUPrice;
        
        HEP_SKU_Price__c objSKUPrice1 = HEP_Test_Data_Setup_Utility.createHEPSkuPrice(objPriceGrade.Id, objSKUMaster.Id, objTerritory1.Id, objPromotionSKU.Id, null, 'Active', false);
        objSKUPrice1.Changed_Flag__c = true;
        objSKUPrice1.Promotion_Dating__c = DatingRec1.Id;
        objSKUPrice1.Unit__c = 20;
        insert objSKUPrice1;
        
        HEP_Market_Spend_Detail__c objMarketSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objMarketSpend.Id, objGl.Id, 'Active', false);
        objMarketSpendDetail.Type__c = 'Current';
        objMarketSpendDetail.Budget__c = 1254;
        insert objMarketSpendDetail;

        HEP_MarketSpendTriggerHandler.E1Wrapper wrap = new HEP_MarketSpendTriggerHandler.E1Wrapper();
        wrap.sPromoId = 'test';
        wrap.sTransactionId = 'testtranx';

        Map<Id, HEP_Market_Spend__c> oldMap = new Map<Id, HEP_Market_Spend__c>();
        Map<Id, HEP_Market_Spend__c> newMap = new Map<Id, HEP_Market_Spend__c>();
        List<id> lstPromoSkuId = new List<id>();
        lstPromoSkuId.add(objPromotionSKU.Id);
        oldMap.put(objMarketSpend.Id,objMarketSpend);
        newMap.put(objMarketSpend.Id,objMarketSpend);
        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = String.valueOf(objPromotion.Id); 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_E1_Spend_Interface demo = new HEP_E1_Spend_Interface();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        HEP_MarketSpendTriggerHandler.createSpendHistory(oldMap,newMap);
        HEP_Promotion_SKU_TriggerHandler.callQueueableForE1(lstPromoSkuId);
        Test.stoptest();        


    }

    @isTest static void HEP_E1_Spend_Interface_FailureTest()
    {
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_Constants__c> lstConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',null);
        insert objLOB;

        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_E1_SpendDetail','HEP_E1_Spend_Interface',true,10,10,'Outbound-Push',true,true,true);
        objInterface.Name = 'HEP_E1_SpendDetail';
        update objInterface;

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'AED','DE','182','ESCO',null);
        insert objTerritory;

        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',null);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',null);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        System.debug('objPromotion>> ' + objPromotion);
        
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id, null);
        insert objPromotionRegion;


        HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        insert objCat;
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerritory.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
        objSKUMaster.Revenue_Share__c = False;
        insert objSKUMaster;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
        insert objPromotionCatalog;

        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', '', false);
        objPromotionSKU.Approval_Status__c = 'Approved';
        objPromotionSKU.Legacy_Id__c = 'ABC123';
        objPromotionSKU.Release_Id__c = '1234';
        insert objPromotionSKU;

        HEP_GL_Account__c objGl = HEP_Test_Data_Setup_Utility.createGLAccount('SampleName', '182638', 'Sample GL Account For Testing', 'Marketing Spend', 'Active', false);
        insert objGl;

        HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154', objPromotion.Id, 'Pending', false);
        insert objMarketSpend;

        HEP_Market_Spend_Detail__c objMarketSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objMarketSpend.Id, objGl.Id, 'Active', false);
        objMarketSpendDetail.Type__c = 'Current';
        objMarketSpendDetail.Budget__c = 1254;
        insert objMarketSpendDetail;


        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_E1_Service';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/v1/edf/events/failure';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = String.valueOf(objPromotion.Id); 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_E1_Spend_Interface demo = new HEP_E1_Spend_Interface();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();        

    }
    @isTest static void HEP_E1_Spend_Interface_InvalidAccessToken()
    {
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        list<HEP_Services__c> lstServices = new list<HEP_Services__c>();        
        HEP_Services__c objService = new HEP_Services__c();
        objService.Name='HEP_E1_OAuth';
        objService.Endpoint_URL__c = 'https://ms-devapi.foxinc.com';
        objService.Service_URL__c = '/oauth/oauth20/tokenFail';
        lstServices.add(objService);
        insert lstServices;
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        //objTxnResponse.sSourceId = 'aer54444'; 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_E1_Spend_Interface demo = new HEP_E1_Spend_Interface();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);
        Test.stoptest();        
        System.assertEquals('Invalid Access Token Or Invalid txnResponse data sent', objTxnResponse.sErrorMsg);                 
    }
    @isTest static void HEP_E1_Spend_Interface_EmptyRequestBody()
    {
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        List<HEP_Constants__c> lstConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_Line_Of_Business__c objLOB = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('LOB','New Release',null);
        insert objLOB;

        EDM_GLOBAL_TITLE__c objTitle = HEP_Test_Data_Setup_Utility.createTitleEDM('The Simpsons','123','',null,null);        
        insert objTitle;

        HEP_Territory__c objTerritory = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary',null, null,'AED','DE','182','ESCO',null);
        insert objTerritory;

        HEP_Promotion__c objGlobalPromotion = HEP_Test_Data_Setup_Utility.createPromotion('Global Promotion','Global','',objTerritory.Id,objLOB.Id,'','',null);
        objGlobalPromotion.Title__c = objTitle.Id;
        insert objGlobalPromotion;
        
        HEP_Promotion__c objPromotion = HEP_Test_Data_Setup_Utility.createPromotion('National Promotion', 'National', objGlobalPromotion.Id, objTerritory.Id, objLOB.Id,'','',null);
        objPromotion.Title__c = objTitle.Id;
        insert objPromotion;
        System.debug('objPromotion>> ' + objPromotion);
        
        HEP_Promotion_Region__c objPromotionRegion = HEP_Test_Data_Setup_Utility.createPromotionRegion(objPromotion.Id, objTerritory.Id, null);
        insert objPromotionRegion;


        HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerritory.Id, 'Approved', 'Request', null);
        insert objCat;
        HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerritory.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
        objSKUMaster.Revenue_Share__c = False;
        insert objSKUMaster;
        HEP_Promotion_Catalog__c objPromotionCatalog = HEP_Test_Data_Setup_Utility.createPromotionCatalog(objCat.Id, objPromotion.Id, null, null);
        insert objPromotionCatalog;

        HEP_Promotion_SKU__c objPromotionSKU = HEP_Test_Data_Setup_Utility.createPromotionSKU(objPromotionCatalog.Id, objSKUMaster.Id, 'Pending', '', false);
        objPromotionSKU.Approval_Status__c = 'Approved';
        objPromotionSKU.Legacy_Id__c = 'ABC123';
        objPromotionSKU.Release_Id__c = '1234';
        insert objPromotionSKU;

        HEP_GL_Account__c objGl = HEP_Test_Data_Setup_Utility.createGLAccount('SampleName', '182638', 'Sample GL Account For Testing', 'Marketing Spend', 'Active', false);
        insert objGl;

        /*HEP_Market_Spend__c objMarketSpend = HEP_Test_Data_Setup_Utility.createHEPMarketSpend('154', objPromotion.Id, 'Pending', false);
        insert objMarketSpend;

        HEP_Market_Spend_Detail__c objMarketSpendDetail = HEP_Test_Data_Setup_Utility.createHEPMarketSpendDetail(objMarketSpend.Id, objGl.Id, 'Active', false);
        objMarketSpendDetail.Type__c = 'Current';
        objMarketSpendDetail.Budget__c = 1254;
        insert objMarketSpendDetail;*/


        Test.startTest();
        HEP_InterfaceTxnResponse objTxnResponse = new HEP_InterfaceTxnResponse();
        objTxnResponse.sSourceId = String.valueOf(objPromotion.Id); 
        Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
        HEP_E1_Spend_Interface demo = new HEP_E1_Spend_Interface();
        System.debug('RESPONSE BEFOREE -- '+objTxnResponse);
        demo.performTransaction(objTxnResponse);
        System.debug('RESPONSE AFTERRR -- '+objTxnResponse);

        Test.stoptest();                
    }
    
    @isTest
    static void testClassVariables(){
        List < HEP_Constants__c > lstHEP = HEP_Test_Data_Setup_Utility.createHEPConstants();
        HEP_E1_Spend_Interface.SpendHeader objSpend1 = new HEP_E1_Spend_Interface.SpendHeader();
        HEP_E1_Spend_Interface.SpendHeader objSpend2 = new HEP_E1_Spend_Interface.SpendHeader('Test',10,'Test','Test');
        HEP_E1_Spend_Interface.SpendDetails objSpendDetail1 = new HEP_E1_Spend_Interface.SpendDetails();
        HEP_E1_Spend_Interface.Payload objPayload1 = new HEP_E1_Spend_Interface.Payload();
        HEP_E1_Spend_Interface.Data objData1 = new HEP_E1_Spend_Interface.Data(objPayload1);
        HEP_E1_Spend_Interface.E1Wrapper objE1 = new HEP_E1_Spend_Interface.E1Wrapper(objData1);
        HEP_E1_Spend_Interface.ErrorResponseWrapper objError = new HEP_E1_Spend_Interface.ErrorResponseWrapper('Status','Error');
        Time objTime = Time.newInstance(1, 2, 3, 4);
        HEP_E1_Spend_Interface.timeInHHMMSS(objTime);
        
        
        
        
    }
}