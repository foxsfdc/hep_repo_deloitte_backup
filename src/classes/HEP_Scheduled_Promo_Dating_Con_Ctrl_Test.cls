/**
* HEP_Scheduled_Promo_Dating_Con_Ctrl_Test-- Test class for the HEP_Scheduled_Promotion_Dating_Con_Ctrl. 
* @author    Lakshman Jinnuri
*/
@isTest
private class HEP_Scheduled_Promo_Dating_Con_Ctrl_Test{
    /**
    * ScheduleTest --  Test method to Schedule the Weekly and Daily Batches
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    public static testmethod void ScheduleTest(){
        Test.StartTest();
        HEP_Scheduled_Promotion_Dating_Con_Ctrl schedule = new HEP_Scheduled_Promotion_Dating_Con_Ctrl();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '30 49 07 08 04 ?';
        String jobID = System.schedule('HEP_DailyEmail_Test', sch, schedule);
        CronTrigger objCronTrigger = [SELECT Id,CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id =: jobId];
        System.assertEquals(sch,objCronTrigger.CronExpression);
        Test.stopTest();
     }
    
}