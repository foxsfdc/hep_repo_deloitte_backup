/*
CLASS NAME : HEP_Promotion_Products_PIS
PURPOSE	   : To fetch the SKU records for PIS view in Products Subtab - Promotion Details
AUTHOR     : Abinaya Elangovan
*/
public class HEP_Promotion_Products_PIS {


	public static String sACTIVE; 
	public static String sJDE;
	public static String sLOVName;
    public static String sLOVValues, sCHANNEL, sFORMAT;  
    public static Map<String,String> mapLOVValues;
    public static map<String,HEP_Promotion_SKU__c> mapSKUMasterIds;
    public static map<String,HEP_Promotion_SKU__c> mapSKUChildIds;
    static{
       sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
       sJDE = HEP_Utility.getConstantValue('HEP_JDE');
       sCHANNEL = HEP_Utility.getConstantValue('HEP_FOX_CHANNEL');
       sFORMAT = HEP_Utility.getConstantValue('HEP_FOX_FORMAT');
       sLOVName = HEP_Utility.getConstantValue('HEP_NAME_FIELD');
       sLOVValues = HEP_Utility.getConstantValue('HEP_LOV_FIELD_VALUES');  
       mapLOVValues = new Map<String, String>();
       mapSKUMasterIds = new map<String, HEP_Promotion_SKU__c>();
   	   mapSKUChildIds = new map<String, HEP_Promotion_SKU__c>();
    }

    /*
    Method : fetchSKURecords
    Purpose : to fetch the SKU Records for PIS view
    Params : @sPromoId - Promotion ID of the page

    */
	public static ProductPIS fetchSKURecords(String sPromoId){

		Map<String,Set<Id>> mapParentChildSKUs = new Map<String, Set<Id>>();
		Set<Id> setAllElligibleSKUIds = new Set<Id>();
		ProductPIS objProductPIS = new ProductPIS();
		objProductPIS.lstSKURecords = new List<SKURecord>();
		List<String> lstLOVFields = new List<String>{sCHANNEL, sFORMAT};
        mapLOVValues = HEP_Utility.getLOVMapping(lstLOVFields, sLOVName, sLOVValues);
		SKURecord objSKURecord;	
		try{	

			if(sPromoId!=null){
		
				objProductPIS.sPromotionID = sPromoId;		
						
				List<HEP_Promotion_SKU__c> lstPromotionSKUs = [SELECT Id,
																	  Name,
																	  HEP_Promotion__c,
																	  Promotion_Catalog__c,
																	  Promotion_Catalog__r.Catalog__c,
																	  Promotion_Catalog__r.Catalog__r.CatalogId__c,
																	  SKU_Master__c,
																	  Record_Status__c
																	  FROM HEP_Promotion_SKU__c
																	  WHERE Record_Status__c =: sACTIVE
																	  AND Approval_Status__c =: HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED')
																	  AND HEP_Promotion__c =: sPromoId];
		
				
				if(lstPromotionSKUs != null && !lstPromotionSKUs.isEmpty()){
					for(HEP_Promotion_SKU__c objPromoSku : lstPromotionSKUs){
						if(objPromoSku.SKU_Master__c != null){
							mapSKUMasterIds.put(objPromoSku.SKU_Master__c, objPromoSku);							
						}			
					}
					
					List<HEP_SKU_BOM__c> lstSKUBOM = [SELECT Id,
															 Component_SKU__c,
															 Components_SKU_Number__c,
															 Parent_SKU__c,
															 Parent_SKU_Number__c,
															 Record_Status__c
															 FROM HEP_SKU_BOM__c
															 WHERE Parent_SKU__c IN: mapSKUMasterIds.keySet()
															 AND Record_Status__c =: sACTIVE];

					if(lstSKUBOM!=NULL && !lstSKUBOM.isEmpty()){
						for(HEP_SKU_BOM__c objSKUBOM : lstSKUBOM){							
							if(mapParentChildSKUs.containsKey(objSKUBOM.Parent_SKU__c)){								
								mapParentChildSKUs.get(objSKUBOM.Parent_SKU__c).add(objSKUBOM.Component_SKU__c);
							}else{
								mapParentChildSKUs.put(objSKUBOM.Parent_SKU__c, new Set<Id>{objSKUBOM.Component_SKU__c});
							}
							mapSKUChildIds.put(objSKUBOM.Component_SKU__c, mapSKUMasterIds.get(objSKUBOM.Parent_SKU__c));							
						}
					}

					
					system.debug('MAP PARENT CHILD SKUS..'+mapParentChildSKUs);					
					Map<String,HEP_SKU_Master__c> mapElligibleSKUDetails = new Map<String,HEP_SKU_Master__c> ([SELECT   Id,
																											   Name,
																											   Catalog_Master__c,
																											   Catalog_Master__r.Catalog_Name__c,
																											   Catalog_Master__r.CatalogId__c,
																											   Channel__c,
																											   Format__c,
																											   Barcode__c,
																											   Same_Base_UPC__c,
																											   SKU_Number__c,
																											   SKU_Title__c,
																											   Territory__c,
																											   UI_SKU_Number__c,
																											   Territory__r.Name,
																											   No_Of_Discs__c,
																											   JDE_Additional_Media__c,
																											   JDE_Canadian_Rating__c,
																											   JDE_Quebec_Rating__c,
																											   JDE_Feature_Run__c,
																											   JDE_SMF_Run__c,
																											   JDE_Total_Run__c,
																											   SKU_Configuration__c,
																											   JDE_Packaging__c,
																											   JDE_Primary_Language__c,
																											   JDE_MPAA_Rating__c,
																											   JDE_BD_Plus__c
																											   FROM HEP_SKU_Master__c
																											   WHERE (Id IN :mapSKUMasterIds.keySet() 																					   
																											   AND Territory__r.SKU_Interface__c =: sJDE
																											   AND Record_Status__c =: sACTIVE
																											   AND SKU_Number__c!=NULL)
																											   OR (Id IN :mapSKUChildIds.keySet()
																											   AND Record_Status__c =: sACTIVE)]);
																											   	
					
					if(mapElligibleSKUDetails!=null && !mapElligibleSKUDetails.isEmpty()){
						Set<String> setAllRecordIDs = new Set<String>();
						if(mapParentChildSKUs!=null && !mapParentChildSKUs.isEmpty()){
							setAllRecordIDs.addAll(mapParentChildSKUs.keySet());
							setAllRecordIDs.addAll(mapSKUMasterIds.keySet());
						}else{
							setAllRecordIDs.addAll(mapSKUMasterIds.keySet());
						}
					   	for(String sParentSkusId : setAllRecordIDs){
						   //First Populate the Parent SKU
							if(mapElligibleSKUDetails.containsKey(sParentSkusId)){
							  	HEP_SKU_Master__c objParentSkuRecordToPopulate = mapElligibleSKUDetails.get(sParentSkusId);					
								objProductPIS.lstSKURecords.add(populateWrapperFieldsWithSkuDetails(objParentSkuRecordToPopulate));
							
								//Populate all children for that parent SKU
								if(mapParentChildSKUs.containsKey(sParentSkusId)){
    								for(String sRelatedChildSku : mapParentChildSKUs.get(sParentSkusId)){
    									if(mapElligibleSKUDetails.containsKey(sRelatedChildSku)){
    										HEP_SKU_Master__c objChildSkuRecordToPopulate = mapElligibleSKUDetails.get(sRelatedChildSku);					
    										objProductPIS.lstSKURecords.add(populateWrapperFieldsWithSkuDetails(objChildSkuRecordToPopulate));
    									}
    								}
								}
							}															
						}
					}	
					
				}
			}
		}
		catch(Exception e){
			HEP_Error_Log.genericException('Exception occured when fetching SKUs for PIS view','Errors',e,'HEP_Promotion_Products_PIS','fetchSKURecords',null,null); 
          	System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
          
		}		
		
		return objProductPIS;
	}

	public class SKURecord{

		public String sCatalogId;
		public String sCatalogRecordId;
		public String sChannel;
		public String sFormat;
		public String sUPC;
		public String sSKUNumber;
		public String sTerritory;
		public Integer iNoOfDiscs;
		public String sAdditionalMedia;
		public String sCARating;
		public String sQBRating;
		public String sFeatureRun;
		public String sSMFRun;
		public String sTotalRun;
		public String sBDPlus;
		public String s3D;
		public String sDescription;
		public String sBDLive;
		public String sUSRating;
		public String sUVCode;
		public String sPkgLang;
		public String sParentID;
		public String sSortOrder;
		public SKURecord(){
			this.s3D = HEP_Utility.getConstantValue('HEP_Changed_Flag_N');
			this.sUVCode = HEP_Utility.getConstantValue('HEP_Changed_Flag_N');
			this.sParentID = '';
		}

	}

	//Wrapper structure that stores a SKU Record for display
	public class ProductPIS{

		public String sPromotionID;
		public List<SKURecord> lstSKURecords;

	}
	
	public static SKURecord populateWrapperFieldsWithSkuDetails(HEP_SKU_Master__c objSKU){
		//Start Wrapper Populating
		SKURecord objSKURecord = new SKURecord();
		//To keep the catalog number as constant between parent and child SKUs
		if(mapSKUChildIds.containsKey(objSKU.Id)){
			objSKURecord.sCatalogId = mapSKUChildIds.get(objSKU.Id).Promotion_Catalog__r.Catalog__r.CatalogId__c;
			objSKURecord.sCatalogRecordId = mapSKUChildIds.get(objSKU.Id).Promotion_Catalog__r.Catalog__c;
		}
		if(mapSKUMasterIds.containsKey(objSKU.Id)){
			objSKURecord.sCatalogId = mapSKUMasterIds.get(objSKU.Id).Promotion_Catalog__r.Catalog__r.CatalogId__c;
			objSKURecord.sCatalogRecordId = mapSKUMasterIds.get(objSKU.Id).Promotion_Catalog__r.Catalog__c;
		}
		objSKURecord.sChannel =  mapLOVValues.containsKey(sCHANNEL+'*'+objSKU.Channel__c)?mapLOVValues.get(sCHANNEL+'*'+objSKU.Channel__c):objSKU.Channel__c;
		objSKURecord.sFormat =  mapLOVValues.containsKey(sFORMAT+'*'+objSKU.Format__c)?mapLOVValues.get(sFORMAT+'*'+objSKU.Format__c):objSKU.Format__c;
		objSKURecord.sUPC = objSKU.Barcode__c;
		objSKURecord.sSKUNumber =objSKU.UI_SKU_Number__c;		
		objSKURecord.sTerritory = objSKU.Territory__r.Name;
		if(objSKU.No_Of_Discs__c!=null){
			objSKURecord.iNoOfDiscs = Integer.valueOf(objSKU.No_Of_Discs__c);
		}
		
		objSKURecord.sAdditionalMedia = objSKU.JDE_Additional_Media__c;
		objSKURecord.sCARating = objSKU.JDE_Canadian_Rating__c;
		objSKURecord.sQBRating = objSKU.JDE_Quebec_Rating__c;
		objSKURecord.sFeatureRun = objSKU.JDE_Feature_Run__c;
		objSKURecord.sSMFRun = objSKU.JDE_SMF_Run__c;
		objSKURecord.sTotalRun = objSKU.JDE_Total_Run__c;
		objSKURecord.sBDPlus = objSKU.JDE_BD_Plus__c;		
		objSKURecord.sBDLive = objSKU.JDE_BD_Plus__c;
		objSKURecord.sDescription = objSKU.SKU_Title__c;
		objSKURecord.sPkgLang = objSKU.JDE_Primary_Language__c;
		objSKURecord.sUSRating = objSKU.JDE_MPAA_Rating__c;

		if(objSKU.SKU_Configuration__c!=null){
			if(objSKU.SKU_Configuration__c.containsIgnoreCase('3D')){
				objSKURecord.s3D = HEP_Utility.getConstantValue('HEP_Changed_Flag_Y');
			}
			if(objSKU.SKU_Configuration__c.containsIgnoreCase('Dig Ins')){
				objSKURecord.sUVCode = HEP_Utility.getConstantValue('HEP_Changed_Flag_Y');
			}
		}
		return objSKURecord;
	}
}