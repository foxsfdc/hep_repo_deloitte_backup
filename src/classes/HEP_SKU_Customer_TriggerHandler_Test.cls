/**
* HEP_SKU_Customer_TriggerHandler_Test -- Test class for HEP_SKU_Customer_TriggerHandler
* 
* @author    Mayank Jaggi
**/ 

@istest 
public class HEP_SKU_Customer_TriggerHandler_Test {

  @testSetup
        static void createUsers(){
            User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'jaggi', 'abc_xyz@deloitte.com','majaggi','mayank','majaggi','', true);
            List<HEP_Constants__c> lstHepConstants = HEP_Test_Data_Setup_Utility.createHEPConstants();
            list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
            HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager','', false);
            objRole.Destination_User__c = u.Id;
            objRole.Source_User__c = u.Id;
            insert objRole;
            HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations (International)','', false);
            objRoleOperations.Destination_User__c = u.Id;
            objRoleOperations.Source_User__c = u.Id;
            insert objRoleOperations;
            HEP_Role__c objRoleFADApprover = HEP_Test_Data_Setup_Utility.createHEPRole('FAD Approvers-Brand Manager','', false);
            objRoleFADApprover.Destination_User__c = u.Id;
            objRoleFADApprover.Source_User__c = u.Id;
            insert objRoleFADApprover;
            HEP_Territory__c objTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Germany','EMEA','Subsidiary', null, null,'EUR','DE','182', 'ESCO', false);
            // createHEPTerritory('Germany', 'EMEA', 'Subsidiary', null, null, 'EUR', 'DE', '182', 'ESCO', false)
            // createHEPTerritory(String sTerritoryName, String sRegion, string sType, String sParentId, String sCatalogTerritoryId, String sCurrencyCode, String sTerritoryCodeIntg, String sERMTerrCode, string sSKUInterface, boolean bInsert)
            insert objTerr;
        }

  @istest
  public static void TEST_HEP_SKU_Customer_TriggerHandler_insert()
  {
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            HEP_Territory__c objTerr = [select id from HEP_Territory__c where name = 'Germany' limit 1];
            HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
            insert objCat;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerr.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
            objSKUMaster.Revenue_Share__c = False;
            insert objSKUMaster;
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple', objTerr.Id, '1234', 'SKU Customers', 'EST Release Date', False);
            insert objCustomer;
            HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id, objSKUMaster.Id, false);
            objSKUCustomer.Legacy_Id__c = String.ValueOf(123);
            objSKUCustomer.Record_Status__c = 'Active';
            insert objSKUCustomer;
            HEP_SKU_Customer__c objQueryRecord = [select Id, Record_Status__c from HEP_SKU_Customer__c where Id = :objSKUCustomer.Id limit 1];
            test.startTest();
            system.assertEquals(objSKUCustomer.Record_Status__c, objQueryRecord.Record_Status__c);
            test.stopTest();
        }
    }

    @istest
  public static void TEST_HEP_SKU_Customer_TriggerHandler_update()
  {
        User testUser = [SELECT Id FROM User where Email = 'abc_xyz@deloitte.com'];
        System.runAs(testUser)
        {
            HEP_Territory__c objTerr = [select id from HEP_Territory__c where name = 'Germany' limit 1];
            HEP_Catalog__c objCat = HEP_Test_Data_Setup_Utility.createCatalog('345','Catalog Sample', 'Box Set', null, objTerr.Id, 'Approved', 'Request', null);
            insert objCat;
            HEP_SKU_Master__c objSKUMaster = HEP_Test_Data_Setup_Utility.createSKUMaster(objCat.Id, objTerr.Id, String.ValueOf(1234), 'VOD SKU', 'Request', 'Physical', 'RENTAL', 'BD', False);
            objSKUMaster.Revenue_Share__c = False;
            insert objSKUMaster;
            HEP_Customer__c objCustomer = HEP_Test_Data_Setup_Utility.createHEPCustomers('Apple', objTerr.Id, '1234', 'SKU Customers', 'EST Release Date', False);
            insert objCustomer;
            HEP_SKU_Customer__c objSKUCustomer = HEP_Test_Data_Setup_Utility.createSKUCustomers(objCustomer.Id, objSKUMaster.Id, false);
            objSKUCustomer.Legacy_Id__c = String.ValueOf(123);
            objSKUCustomer.Record_Status__c = 'Active';
            insert objSKUCustomer;
            HEP_SKU_Customer__c objQueryRecord = [select Id, Record_Status__c from HEP_SKU_Customer__c where Id = :objSKUCustomer.Id limit 1];
            objQueryRecord.Record_Status__c = 'Inactive';
            HEP_CheckRecursive.clearSetIds();
            update objQueryRecord;
            test.startTest();
            HEP_SKU_Customer__c objQueryRecordOnUpdate = [select Id, Record_Status__c from HEP_SKU_Customer__c where Id = :objSKUCustomer.Id limit 1];
            system.assertEquals(objQueryRecord.Record_Status__c, objQueryRecordOnUpdate.Record_Status__c);
            test.stopTest();
        }
    }
}