/**
 * HEP_Approvals_Controller --- Class to get the data for all the approval records
 * @author    Arjun Narayanan
 */
public class HEP_Approvals_Controller {

    public map<String, String> mapPageAccess{get;set;}
    
    public String sHeaderTabs {
        get;
        set;
    }
    public String sCurrencyFormat {
        get;
        set;
    }
    public String sTerritoryId;
    public static list<HEP_User_Role__c> lstUserRoles;
    public static String sPENDING, sCUSTOMER, sNATIONAL, sAPPROVED, sREJECTED, sACTIVE;
    public static String sMDP_NATIONAL_PRODUCT_APPROVAL_TYPE, sREPORTEE_HIERARCHY;
    public Static String sMDP_CUSTOMER_PRODUCT_APPROVAL_TYPE;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
        sPENDING = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
        sCUSTOMER = HEP_Utility.getConstantValue('PROMOTION_TYPE_CUSTOMER');
        sNATIONAL = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
        sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sREJECTED = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
        sMDP_CUSTOMER_PRODUCT_APPROVAL_TYPE = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_CUSTOMER');
        sMDP_NATIONAL_PRODUCT_APPROVAL_TYPE = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_MDP_NATIONAL');
        sREPORTEE_HIERARCHY = HEP_Utility.getConstantValue('HEP_REPORTEE_HIERARCHY');
        lstUserRoles = [Select Id, Role__c, Role__r.LOB__c, Search_Id__c, Territory__c, User__c
            FROM HEP_User_Role__c
            Where User__c =: UserInfo.getUserId()
            And Record_Status__c =: sACTIVE
        ];
    }

    /**
     * PendingApprovalsData --- Wrapper class to hold all the page data
     * @author    Arjun Narayanan
     */
    public class PendingApprovalsData {
        Integer iNoOfPendingApprovals;
        Integer iPendingFAD;
        Integer iPendingUnlock;
        Integer iPendingBudget;
        Integer iPendingProductRequests;
        Integer iPendingUnlockProducts;
        Integer iPendingCustomerPromoProducts;
        Integer iPendingNationalPromoProducts;
        //list<ApprovalRecord> lstApprovalRecords;

        /**
         * Class Constructor
         * @return nothing
         * @author Arjun Narayanan
         */
        public PendingApprovalsData() {
            //lstApprovalRecords = new list<ApprovalRecord>();
            iNoOfPendingApprovals = 0;
            iPendingFAD = 0;
            iPendingUnlock = 0;
            iPendingBudget = 0;
            iPendingProductRequests = 0;
            iPendingUnlockProducts = 0;
            iPendingCustomerPromoProducts = 0;
            iPendingNationalPromoProducts = 0;
        }
    }

    /**
     * Class constructor to initialize the class variables
     * @return nothing
     * @author Arjun Narayanan
     */
    public HEP_Approvals_Controller() {
         
        PendingApprovalsData objPendingApprovalsData = new PendingApprovalsData();
        objPendingApprovalsData = getPendingApprovalRecordsData(); 
       
        // Calling the security framework to get the list of tabs to be shown on the UI 
        list<HEP_Security_Framework.Tab> lstHeaderTabs = new list<HEP_Security_Framework.Tab>();
        list<HEP_Security_Framework.Tab> lstHeaderTabsUI = new list<HEP_Security_Framework.Tab>();
        mapPageAccess = HEP_Security_Framework.getTerritoryAccess(null, HEP_Utility.getConstantValue('HEP_PAGE_NAME_HEP_APPROVALS'), HEP_Utility.getConstantValue('HEP_FILTER_MY_APPROVALS'), lstHeaderTabs);
        if(objPendingApprovalsData.iPendingFAD == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsFADDates','FALSE');

        }
        if(objPendingApprovalsData.iPendingUnlock == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsUnlockDates','FALSE');
        }
        if(objPendingApprovalsData.iPendingBudget == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsBudget','FALSE');
        }
        if(objPendingApprovalsData.iPendingProductRequests == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsProductRequests','FALSE');
        }
        if(objPendingApprovalsData.iPendingUnlockProducts == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsUnlockProducts','FALSE');
        }
        if(objPendingApprovalsData.iPendingCustomerPromoProducts == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsCustomer','FALSE'); 
        }
        if(objPendingApprovalsData.iPendingNationalPromoProducts == 0){
            mapPageAccess.put('HEP_Promotion_MyApprovalsNational','FALSE');
        } 

        for(HEP_Security_Framework.Tab objTab : lstHeaderTabs){
            if(objPendingApprovalsData.iPendingFAD == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsFADDates')
                    objTab.sTabName = null;
            }
            if(objPendingApprovalsData.iPendingUnlock == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsUnlockDates')
                   objTab.sTabName = null;
            }
            if(objPendingApprovalsData.iPendingBudget == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsBudget')
                    objTab.sTabName = null;
            }
            if(objPendingApprovalsData.iPendingProductRequests == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsProductRequests')
                    objTab.sTabName = null;
            }
            if(objPendingApprovalsData.iPendingUnlockProducts == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsUnlockProducts')
                    objTab.sTabName = null;
            }
            if(objPendingApprovalsData.iPendingCustomerPromoProducts == 0){
               if(objTab.sPageName == 'HEP_Promotion_MyApprovalsCustomer')
                    objTab.sTabName = null; 
            }
            if(objPendingApprovalsData.iPendingNationalPromoProducts == 0){
                if(objTab.sPageName == 'HEP_Promotion_MyApprovalsNational')
                    objTab.sTabName = null;
            } 
            if(objTab.sTabName != null)
                lstHeaderTabsUI.add(objTab);
        }

        system.debug('---->mapPageAccess--->'+mapPageAccess); 
        system.debug('lstHeaderTabs---------->'+lstHeaderTabs);       
        sHeaderTabs = JSON.serialize(lstHeaderTabsUI); 
        system.debug('---->sHeaderTabs----->'+sHeaderTabs);
        sCurrencyFormat = HEP_Utility.getUserCurrencyFormat(null);
    }



    /**
     * Its used to get the page data
     * @param sTabName name of the tab of which data is to be loaded
     * @return the url of the promotion image
     * @author Sachin Agarwal
     */
    @RemoteAction
    public static PendingApprovalsData getPendingApprovalRecordsData() {
        //system.debug(sRequestType);
        //list<String> lstApprovalTypes = new list<String>(); 
        String sHierarchyTypeFAD = '';
        String sHierarchyTypeUnlock = '';
        String sHierarchyTypeBudget = '';
        String sHierarchyTypeUnlockProducts = '';
        String sHeirarchyTypeProductRequests = '';
        Set<String> setSearchIds = new Set<String>();

        //lists to store approval records of each type
        list<HEP_Record_Approver__c> lstFADApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstUnlockApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstBudgetApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstUnlockProductsApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstProductRequestApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstCustomerPromoProductsApprovals = new list<HEP_Record_Approver__c>();
        list<HEP_Record_Approver__c> lstNationalPromoProductsApprovals = new list<HEP_Record_Approver__c>();

        sHierarchyTypeFAD = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_FAD_OVERRIDE');
        sHierarchyTypeUnlock = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_DATING');
        sHierarchyTypeBudget = HEP_Utility.getConstantValue('HEP_SPEND_TYPE');
        sHierarchyTypeUnlockProducts = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_UNLOCK');
        sHeirarchyTypeProductRequests = HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_REQUESTS');
        PendingApprovalsData objPendingApprovalsData = new PendingApprovalsData();

     
        Set<String> setLOBs = new Set<String>();
    
    for(HEP_User_Role__c objUserRole : HEP_Utility.getTerritoryUserRoleForUser(UserInfo.getUserId())){
            setSearchIds.add(objUserRole.Search_Id__c);
                if(String.isNOtBlank(objUserRole.Role__r.LOB__c))
                    setLOBs.addAll(objUserRole.Role__r.LOB__c.split(';'));
        }

        system.debug('setsearchids----->'+setsearchids);
        
        // Approval records assigned to me which are pending
        for (HEP_Record_Approver__c objRecordApprover: [SELECT 
                                                            Id, Approval_Record__r.Approval_Type__r.Type__c,
                                                            Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.Promotion_Type__c,
                                                            Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.LineOfBusiness__r.Type__c,
                                                            Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.StartDate__c,
                                                            Approval_Record__r.HEP_MDP_Promotion_Product__r.Record_Status__c,
                                                            Approval_Record__r.HEP_MDP_Promotion_Product__r.Product_Start_Date__c,
                                                            IsActive__c
                                                        FROM 
                                                            HEP_Record_Approver__c
                                                        WHERE 
                                                            Status__c =: sPENDING 
                                                        AND 
                                                            IsActive__c = true
                                                        AND 
                                                            Approval_Record__r.Approval_Status__c =: sPENDING
                                                        AND 
                                                            Search_Id__c in: setSearchIds
                                                        AND 
                                                            Record_Status__c =: sACTIVE
                                                        AND ((Approval_Record__r.HEP_Market_Spend__r.Record_Status__c = :sACTIVE) OR 
                                                            (Approval_Record__r.HEP_Promotion_Dating__r.Record_Status__c = :sACTIVE) OR                                     
                                                            (Approval_Record__r.HEP_MDP_Promotion_Product__r.Record_Status__c = :sACTIVE) OR
                                                            (Approval_Record__r.HEP_Promotion_SKU__r.Record_Status__c = :sACTIVE))]) {
                                                                
            String sApprovalType = objRecordApprover.Approval_Record__r.Approval_Type__r.Type__c;
            String sPromotionType = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.Promotion_Type__c;
            String sLOB = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.HEP_Promotion__r.LineOfBusiness__r.Type__c;
            Date dtProductStartDate = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.Product_Start_Date__c;
            String sPromoProductStatus = objRecordApprover.Approval_Record__r.HEP_MDP_Promotion_Product__r.Record_Status__c;
            
            //To get the count of pending approval requests of each type
            if (HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_FAD_OVERRIDE').equalsIgnoreCase(sApprovalType)) {
                lstFADApprovals.add(objRecordApprover);
            } else if (HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_DATING').equalsIgnoreCase(sApprovalType)) {
                lstUnlockApprovals.add(objRecordApprover);
            } else if (HEP_Utility.getConstantValue('HEP_SPEND_TYPE').equalsIgnoreCase(sApprovalType)) {
                lstBudgetApprovals.add(objRecordApprover);
            } else if (HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_UNLOCK').equalsIgnoreCase(sApprovalType)) {
                lstUnlockProductsApprovals.add(objRecordApprover);
            } else if(HEP_Utility.getConstantValue('HEP_APPROVAL_TYPE_PRODUCT_REQUESTS').equalsIgnoreCase(sApprovalType)){
                lstProductRequestApprovals.add(objRecordApprover);
            } else if (sMDP_NATIONAL_PRODUCT_APPROVAL_TYPE.equalsIgnoreCase(sApprovalType)
                && sNATIONAL.equals(sPromotionType)
                && sACTIVE.equals(sPromoProductStatus)) {
                        
                    lstNationalPromoProductsApprovals.add(objRecordApprover);
            } else if (sMDP_CUSTOMER_PRODUCT_APPROVAL_TYPE.equalsIgnoreCase(sApprovalType)
                && sCUSTOMER.equals(sPromotionType)
                && sACTIVE.equals(sPromoProductStatus)) {
                
                lstCustomerPromoProductsApprovals.add(objRecordApprover);
            }
        
        }

        //objPendingApprovalsData.iNoOfPendingApprovals = lstMyRecordApproverRecords.size();
        objPendingApprovalsData.iPendingFAD = lstFADApprovals.size();
        objPendingApprovalsData.iPendingUnlock = lstUnlockApprovals.size();
        objPendingApprovalsData.iPendingBudget = lstBudgetApprovals.size();
        objPendingApprovalsData.iPendingProductRequests = lstProductRequestApprovals.size();
        objPendingApprovalsData.iPendingUnlockProducts = lstUnlockProductsApprovals.size();
        objPendingApprovalsData.iPendingCustomerPromoProducts = lstCustomerPromoProductsApprovals.size();
        objPendingApprovalsData.iPendingNationalPromoProducts = lstNationalPromoProductsApprovals.size();
        system.debug('@@@@@@@@----->' + objPendingApprovalsData.iPendingFAD);
        system.debug('########----->' + objPendingApprovalsData.iPendingUnlock);
        system.debug('$$$$$$$$----->' + objPendingApprovalsData.iPendingBudget);
        system.debug('%%%%%%%%----->' + objPendingApprovalsData.iPendingProductRequests);
        system.debug('!!!!!!!!UnlockProduct----->' + objPendingApprovalsData.iPendingUnlockProducts);
        system.debug('!!!!!!!!----->Customer' + objPendingApprovalsData.iPendingCustomerPromoProducts);
        system.debug('!!!!!!!!----->national' + objPendingApprovalsData.iPendingNationalPromoProducts);

        return objPendingApprovalsData;
    }
}