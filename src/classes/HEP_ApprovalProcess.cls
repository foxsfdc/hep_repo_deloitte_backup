/**
 * HEP_ApprovalProcess ---  Detremine the Approvers for a Record when the Approval Process is Invoked
 * @author  Nishit Kedia
 */
public class HEP_ApprovalProcess{

	//Member variables for the class
    public static Map <Id,HEP_Rule__c> mapAllActiveRules;
    public Map<Id,Integer> mapAproverRolesInSequence;
    
    
    /**
    * Class constructor to initialize the class variables
    * @return nothing
    * @author Nishit Kedia
    */ 
    public HEP_ApprovalProcess(){
        mapAllActiveRules = new Map <Id,HEP_Rule__c>();
        mapAproverRolesInSequence = new Map<Id,Integer>();
        
    }
    
    public static String sPendingStatus;
    public static String sApprovedStatus;
    public static String sRejectedStatus;
    //Static Block For Active Status
    public static String sACTIVE;
    static{
		sPendingStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_PENDING');
	  	sApprovedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
	  	sRejectedStatus = HEP_Utility.getConstantValue('HEP_APPROVAL_REJECTED');
	   	sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
    }
    
    /**
    * invokeApprovalProcess --- To initiate approval routing process for queueing records into approval
    * @param sApprovalRecordId is the Parent id which was invoked for Approval
    * @param sApprovalTypeName is the Unique name for Type Of Approval 
    * @param sterritoryId is the Territory Id which requires Approval
    * @return Boolean Indicating status of process
    * @author    Nishit Kedia
    */ 
    public Boolean invokeApprovalProcess(String sApprovalRecordId, String sApprovalTypeName, String sterritoryId){ 
    	 
        Boolean bResponse = false;
        try{
            if(String.isNotBlank(sApprovalRecordId) && String.isNotBlank(sApprovalTypeName)){
            	//Make a dynamic Query for the  Parent Id which was invoked for Approval
                String sQuery= buidDynamicSOQLQuery(sApprovalRecordId);
                System.debug('Query Built is-->' + sQuery);
                //Fetch the values in an Sobject List
                List<Sobject> lstParentApprovalSobject=Database.Query(sQuery);
                
                if(lstParentApprovalSobject != null && !lstParentApprovalSobject.isEmpty() && String.isNotBlank(sApprovalTypeName)){
                	// Retreive the parent Sobject Details.(Only for 1 Approval Object)          
                    Sobject objParentRecordDetails=lstParentApprovalSobject.get(0);
                    System.debug('Check the Parent Record Details-->' + objParentRecordDetails);
                    
                    // Determine the Approval Type and Route Based on its Routing Type
		            List<HEP_Approval_Type__c> lstApprovalType = [Select Id,
	                                                                     Name,
	                                                                     Approval_Field__c,
	                                                                     Approved_Status__c,
	                                                                     Rejected_Status__c,
	                                                                     Hierarchy_Type__c,
	                                                                     /*Assignment_Rule__c,*/
	                                                                     Routing_Type__c,
	                                                                     Hierarchy_Type__r.Territory__c,
	                                                                     Type__c
	                                                                     from HEP_Approval_Type__c
	                                                                     where                                                                   
	                                                                     Type__c =: sApprovalTypeName
	                                                                     AND Record_Status__c =:sACTIVE                                                                        
                                                                ];
                                                                
                    System.debug('Approval type found->' + lstApprovalType);                                          
                    if(lstApprovalType!=null && !lstApprovalType.isEmpty()){
                        //Approval Types will be unique therefore only Single Record is Fetched
                        if(lstApprovalType.size() == 1){
                        	HEP_Approval_Type__c objApprovalType = lstApprovalType.get(0);                            
                            if (objApprovalType.Routing_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATIC_ROUTING'))){
                            	// Route based on Static routing (E.g :DATING UNLOCK)
                                bResponse = processHandlingForTypeStatic(objParentRecordDetails,objApprovalType,sterritoryId);
                            }                       
                        }                       
                    }
                }
            }
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
        return bResponse;        
    }
    
    
    /*****************************************Approval Role Hierarchy Changes **********************************/
	//Overloading the function to 
	//Extra parameter: comments from the requester ,List of user Ids who were the prior submitters and send the LOB
	
	/**
    * invokeApprovalProcess --- To initiate approval routing process for queueing records into approval
    * @param sApprovalRecordId is the Parent id which was invoked for Approval
    * @param sApprovalTypeName is the Unique name for Type Of Approval 
    * @param sterritoryId is the Territory Id which requires Approval
    * @return Boolean Indicating status of process
    * @author    Nishit Kedia
    */ 
    public Boolean invokeApprovalProcess(HEP_ApprovalInputWrapper  objInputApprovalApi){ 
    	 
        Boolean bResponse = false;
        try{
			if(objInputApprovalApi != null){
				
				//Fetvh wrapper variables
				String sApprovalRecordId = objInputApprovalApi.sApprovalRecordId;
				String sApprovalTypeName = objInputApprovalApi.sApprovalTypeName;
				String sterritoryId = objInputApprovalApi.sterritoryId;
				String sLOB = objInputApprovalApi.sLineOfBuisness;
				
				String sRoutingType = HEP_Utility.getConstantValue('HEP_HIERARCHICAL_ROUTING');
				if(String.isNotBlank(sApprovalRecordId) && String.isNotBlank(sApprovalTypeName)){
					//Make a dynamic Query for the  Parent Id which was invoked for Approval
					String sQuery= buidDynamicSOQLQuery(sApprovalRecordId);
					System.debug('Query Built is-->' + sQuery);
					//Fetch the values in an Sobject List
					List<Sobject> lstParentApprovalSobject=Database.Query(sQuery);
					
					if(lstParentApprovalSobject != null && !lstParentApprovalSobject.isEmpty() && String.isNotBlank(sApprovalTypeName)){
						// Retreive the parent Sobject Details.(Only for 1 Approval Object)          
						Sobject objParentRecordDetails=lstParentApprovalSobject.get(0);
						System.debug('Check the Parent Record Details-->' + objParentRecordDetails);
						
						// Determine the Approval Type and Route Based on its Routing Type
						List<HEP_Approval_Type__c> lstApprovalType = [Select Id,
																			Name,
																			Approval_Field__c,
	                                                                     	Approved_Status__c,
	                                                                     	Rejected_Status__c,
																			Hierarchy_Type__c,
																			/*Assignment_Rule__c,*/
																			Routing_Type__c,
																			Hierarchy_Type__r.Territory__c,
																			Hierarchy_Type__r.LOB__c,
																			Type__c
																			from HEP_Approval_Type__c
																			where                                                                   
																			Type__c =: sApprovalTypeName
																			And
																			Hierarchy_Type__r.Territory__c =: sterritoryId
																			And
																			Hierarchy_Type__r.LOB__c INCLUDES (:sLOB)
																			And
																			Routing_Type__c =: sRoutingType
																			AND Record_Status__c =:sACTIVE
																		];
																	
						  System.debug('Approval type found->' + lstApprovalType);   
						  
						if(lstApprovalType!=null && !lstApprovalType.isEmpty()){							                         							
							//Global and DHE have Same Approval Type, therefore Route based on Hierarchy Type :Hierarchichal
                            HEP_Approval_Type__c objApprovalType = lstApprovalType.get(0);                            
                            if(objApprovalType.Routing_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATIC_ROUTING'))){
                              //Route Based on Direct Routing (E.g: FAD OVERRIDE)
                                bResponse = processHandlingForTypeStatic(objParentRecordDetails,objApprovalType,sterritoryId);
                            }
                              else if (objApprovalType.Routing_Type__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_HIERARCHICAL_ROUTING'))){
                              // Route based on Static routing (E.g :SPEND)
                                bResponse = processHandlingHierarchical(objParentRecordDetails,objApprovalType,objInputApprovalApi);
                            }                           															
						}							
					}					
				}
			}
        }catch(Exception e){
            System.debug('Exception occured because of -->' + e.getMessage() + ' @ Line Number-->' + e.getLineNumber());
        }
        return bResponse;        
    }
	
	/**
    * processHandlingForTypeDirect ---  For Approval Type Hierarchichal Evaluate Users Based on Resource Hierarchy and Assignment Type from Approval Type user
    * @param objParentRecordDetails is the Parent Record details which was invoked for Approval
    * @param sApprovalTypeName is the Unique name for Type Of Approval 
    * @return Boolean Indicating status of process
    * @author    Nishit Kedia
    */
    public Boolean processHandlingHierarchical(Sobject objParentRecordDetails, HEP_Approval_Type__c objApprovalType, HEP_ApprovalInputWrapper  objInputApprovalApi){ 
	
	   System.debug('Inside hierarchial routing via Roles');
	   Boolean bInvokationStatus = false;
	   List<String> lstUserRolesId = new List<String>();
        //Validate Attributes passed are not null
        if(objApprovalType != null && objParentRecordDetails != null){
            System.debug('Record Details are-->' + objParentRecordDetails);
            
            //This ideally will fetch the user hierarchy list
            //Submitter is the Context User on which this Approval Process is Executing  
            String sRequestorId = Userinfo.getUserId();         
            //String sRequestorId= (String)objParentRecordDetails.get('LastModifiedById');           
            String sHierarchyType = objApprovalType.Hierarchy_Type__c;
            
			//fetch all roles Under the LOB received as Input
			for(HEP_User_Role__c objUserRole: [Select 	Id,
														Role__c,
														Role__r.LOB__c,
														Search_Id__c,
														Territory__c,
														User__c
														from 
														HEP_User_Role__c
														Where
														User__c =: sRequestorId
														And
														Role__r.LOB__c INCLUDES (:objInputApprovalApi.sLineOfBuisness)
														And
														Territory__c =: objInputApprovalApi.sterritoryId
														AND Record_Status__c =:sACTIVE]){
				
				lstUserRolesId.add(objUserRole.Role__c);
			}
						
            //Instantiate Role Hierarchy 
            HEP_Role_Hierarchy objroleHierarchy = new HEP_Role_Hierarchy();
			
            //Generate Hierachy for the Requestor's Managers
			objroleHierarchy.GenerateRoleHierarchy(sHierarchyType, HEP_Utility.getConstantValue('HEP_MANAGER_HIERARCHY'));
            //fetch All manager Roles for Requestor                
            objroleHierarchy.GetRoleHierarchyList(lstUserRolesId,  HEP_Utility.getConstantValue('HEP_MANAGER_HIERARCHY'));
            Set<Id> lstManagerRolesForRequestor = new Set<Id>();
			
			for(String objUserRoleId :lstUserRolesId){
				lstManagerRolesForRequestor.addAll(objroleHierarchy.mapRolesInHierarchy.get(objUserRoleId));
			}
			           
            System.debug('all Managers Roles List---->' + lstManagerRolesForRequestor);           
            System.debug('Approval Type-->'+objApprovalType);           
            
            // First determine the list of Approvers tied with the Rules under the assigned Approval Type for this mangerlist
            String sAlwaysAssigned='ALWAYS';
            
            //If any Role Matches for Approval Role then proceed further
            //The Approval Type Id
            Id approvalTypeId = objApprovalType.Id;
            //The parent record Id which came for Approval
            Id approvalRecordId = objParentRecordDetails.Id;
            
            if(lstManagerRolesForRequestor != null && !lstManagerRolesForRequestor.isEmpty()){
	            Map<Id,HEP_Approval_Type_Role__c> mapApprovalTypeRoles= new Map<Id,HEP_Approval_Type_Role__c> ([Select   id,
							                                                                                            Name,
							                                                                                            Approval_Type__c,
							                                                                                            Approval_Type__r.Name,
							                                                                                            Approver_Role__c,
							                                                                                            Rule__r.Name,
							                                                                                            Rule__c,
																														Approver_Territory__c,
							                                                                                            Sequence__c                                                                                                                                                                                    
							                                                                                            From HEP_Approval_Type_Role__c
							                                                                                            where
							                                                                                            Approval_Type__c=:objApprovalType.Id
							                                                                                            AND 
							                                                                                            Record_Status__c =:sACTIVE
							                                                                                            And
							                                                                                            (Approver_Role__c in :lstManagerRolesForRequestor
							                                                                                            OR
							                                                                                            Assignment_Type__c = :sAlwaysAssigned
							                                                                                            )
							                                                                                            order by Sequence__c Asc     
							                                                                                    ]);                                                                                                                                                                         
	            System.debug('ATRole size is -->'+mapApprovalTypeRoles.size()+'Resource Map is-->'+mapApprovalTypeRoles);           
	                                                                        
	            mapAproverRolesInSequence = buildRecordApproversMap(mapApprovalTypeRoles,objParentRecordDetails);    
	            //Queue the records into approval
	            if(mapAproverRolesInSequence != null && !mapAproverRolesInSequence.isEmpty()){             
	                HEP_Approval_Utility.queueRecordsToApproval(approvalTypeId,mapAproverRolesInSequence, objInputApprovalApi);
	                bInvokationStatus= true;
	            } else{
	            	//Auto Approve if there are No Approvers Found
	            	HEP_Approval_Utility.autoApproval(approvalRecordId, objApprovalType.Approval_Field__c, objApprovalType.Approved_Status__c,sApprovedStatus,approvalTypeId );
	                bInvokationStatus = true;
	            }   
            }else{
            	//Make the record Rejected to before
            	//Auto Approve if there are No Approvers Found
            	HEP_Approval_Utility.autoApproval(approvalRecordId, objApprovalType.Approval_Field__c, objApprovalType.Rejected_Status__c,sRejectedStatus,approvalTypeId);
                bInvokationStatus = true;
            }       
        }
        System.debug('bInvokationStatus--->'+bInvokationStatus);
        return bInvokationStatus;
    }   
    
    
    /**
    * processHandlingForTypeStatic ---  For Approval Type Static Evaluate Users Based on Rules from Approval Type users Directly
    * @param objParentRecordDetails is the Parent Record details which was invoked for Approval
    * @param sApprovalTypeName is the Unique name for Type Of Approval 
    * @return Boolean Indicating status of process
    * @author    Nishit Kedia
    */ 
    public Boolean processHandlingForTypeStatic(Sobject objParentRecordDetails, HEP_Approval_Type__c objApprovalType,String sterritoryId){
    	
        Boolean bInvokationStatus= false;
        String sAlwaysAssigned = 'ALWAYS';
        //Validate Attributes passed are not null
        if(objApprovalType != null && objParentRecordDetails != null ){           
            //----------------------------------------------------//
            //Fetch all those Approval Type Users Tagged to a Rule which falls under that objApprovalType
            Map<Id,HEP_Approval_Type_Role__c> mapApprovalTypeUser = new Map<Id,HEP_Approval_Type_Role__c> ([Select  Id,
						                                                                                            Name,
						                                                                                            Approval_Type__c,
						                                                                                            Approval_Type__r.Name,					                                                                  
																													Approver_Role__c,
						                                                                                            Sequence__c,
						                                                                                            Rule__r.Name,
						                                                                                            Rule__c                                                                                                
						                                                                                            From HEP_Approval_Type_Role__c
						                                                                                            where 
						                                                                                            Approval_Type__c=:objApprovalType.Id
						                                                                                            AND
						                                                                                            Approval_Routing_Type__c=:objApprovalType.Routing_Type__c  
						                                                                                            And
						                                                                                            Assignment_Type__c = :sAlwaysAssigned 
						                                                                                            AND 
						                                                                                            Record_Status__c =:sACTIVE       
						                                                                                            order by Sequence__c Asc
						                                                                                        ]);
            System.debug('Resource Map is--> ' + mapApprovalTypeUser); 
            //The Approval Type Id
            Id approvalTypeId = objApprovalType.Id;
            //The parent record Id which came for Approval
            Id approvalRecordId = objParentRecordDetails.Id;                                                                        
         	//Build the Approvers map after Validating the Rules  tied to Approval Type Users                                                                        
            mapAproverRolesInSequence = buildRecordApproversMap(mapApprovalTypeUser,objParentRecordDetails);    
            //Queue the records into approval
            if(mapAproverRolesInSequence != null && !mapAproverRolesInSequence.isEmpty()){             
                HEP_Approval_Utility.queueRecordsToApproval(approvalRecordId, approvalTypeId, mapAproverRolesInSequence,sterritoryId);
                bInvokationStatus = true;
            } else{
            	//Auto Approve if there are No Approvers Found
            	HEP_Approval_Utility.autoApproval(approvalRecordId, objApprovalType.Approval_Field__c, objApprovalType.Approved_Status__c,sApprovedStatus,approvalTypeId);
                bInvokationStatus = true;
            }
        }
        System.debug('bInvokationStatus--->'+bInvokationStatus);
        return bInvokationStatus;
    }   
    
	
    
    

    /**
    * buildRecordApproversMap ---  For Approval Type Hierarchichal Evaluate Users Based on Resource Hierarchy and Assignment Type from Approval Type user
    * @param mapApprovalTypeUser is the approver type user map tagged to Rules
    * @param objParentRecordDetails is the Parent Record details which was invoked for Approval   
    * @return mapAproverRolesInSequence which has key-->ApproverId and Value as Sequence Number for the Approver
    * @author    Nishit Kedia
    */
    public  Map<Id,Integer> buildRecordApproversMap(Map<Id,HEP_Approval_Type_Role__c> mapApprovalTypeUser,Sobject objParentRecordDetails){
        
        if(mapApprovalTypeUser != null && mapAllActiveRules != null){  
        	
        	// This list will contain all rules which requires Processing for the Approval Type Roles                                   
            List<HEP_Rule__c> lstRulesToBeEvaluated= new List<HEP_Rule__c>();
            //Filtering Rules from Master Rule Set of mapAllActiveRules
            for(Id approvalResourceId:mapApprovalTypeUser.keySet()){                     
                HEP_Approval_Type_Role__c objApprovalResource= mapApprovalTypeUser.get(approvalResourceId);
                if(mapAllActiveRules.containsKey(objApprovalResource.Rule__c)){
                    lstRulesToBeEvaluated.add(mapAllActiveRules.get(objApprovalResource.Rule__c));
                }                      
            }                               
            System.debug('Filtering Rules from total rules under the object' + lstRulesToBeEvaluated);
        
            // Fetch the elligible Rules that matches the Rule Criteria for Approval
            List<HEP_Rule__c> lstEligibleApprovalRules=HEP_Rule_Utility.evaluateRuleCriteria(objParentRecordDetails,lstRulesToBeEvaluated);
            
            //This Set contains all the Rule Id which has matched the Rules Criteria
            Set<Id> setApprovalResourceRules= new Set<Id>();
            if(lstEligibleApprovalRules!=null && !lstEligibleApprovalRules.isEmpty()){                    
                for(HEP_Rule__c objEligibleRule:lstEligibleApprovalRules){                       
                    setApprovalResourceRules.add(objEligibleRule.Id);                     
                }
                
                System.debug('Step 9 Rule Id matching the Rule criteria for Approval'+setApprovalResourceRules);
                if(!setApprovalResourceRules.isEmpty()){
                    //Now Identify the Approvers from Approval Type Users based on the Filtered Rule Id Set
                    for(Id approvalResourceId:mapApprovalTypeUser.keySet()){                           
                        HEP_Approval_Type_Role__c objApprovalResource= mapApprovalTypeUser.get(approvalResourceId);
                        if(setApprovalResourceRules.contains(objApprovalResource.Rule__c)){
                            mapAproverRolesInSequence.put(objApprovalResource.Approver_Role__c,Integer.valueOf(objApprovalResource.Sequence__c));
                        }                       
                    }
                    System.debug('Final Approvers are-->'+mapAproverRolesInSequence);
                }
            }               
        }
        System.debug('Approver Map--->'+mapAproverRolesInSequence);            
        return mapAproverRolesInSequence;
    }
    
    
    /**
    * buidDynamicSOQLQuery ---  To build a string with field api's for Parent Approval Id
    * @param sApprovalRecordId is the Parent id which was invoked for Approval  
    * @return sBuildDynamicQuery is the Dynamic Query String
    * @author    Nishit Kedia
    */
    public static String buidDynamicSOQLQuery(String sApprovalRecordId){
    	
    	//Determine Object Api 
        Id approvalId = Id.valueOf(sApprovalRecordId);
        String sObjectApiName=approvalId.getSObjectType().getDescribe().getName();
        
        // Fetch All the Active Rules in the Static List
        mapAllActiveRules = HEP_Rule_Utility.getActiveRules(sObjectApiName);
        //Build Query String
        String sBuildDynamicQuery = 'SELECT Id,';
        String sFieldApiList = '';
        //Fetch All Field Apis from Rule List for that Object 
        if(mapAllActiveRules != null && !mapAllActiveRules.isEmpty()){
            for(Id ruleId:mapAllActiveRules.keySet()){
                HEP_Rule__c objParentRule = mapAllActiveRules.get(ruleId);
                for(HEP_Rule_Criterion__c objChildCriteriaRule:objParentRule.HEP_Rule_Criteria__r ){
                    if(!sFieldApiList.contains(objChildCriteriaRule.HEP_Source_SObject_Field__c))
                        sFieldApiList += objChildCriteriaRule.HEP_Source_SObject_Field__c + ',';
                }
            }
            sBuildDynamicQuery += sFieldApiList + 'LastModifiedById From ' + sObjectApiName + ' Where id=\'' + sApprovalRecordId + '\'';
            
        }
        return sBuildDynamicQuery;        
    }
}