/**
* Hep_SpendEmailWrappers_Test -- Test class for the Hep_SpendEmailWrapper. 
* @author    Lakshman Jinnuri
*/
@isTest
private class Hep_SpendEmailWrapper_Test {
    /**
    * Hep_SpendEmailWrapper_SuccessTest-- Test method to get Hep_SpendEmailWrapper test coverage
    * @return  :  no return value
    * @author  :  Lakshman Jinnuri
    */
    @isTest static void Hep_SpendEmailWrapper_SuccessTest() {
        list<HEP_Constants__c> objHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> objHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        Test.startTest();
        Hep_SpendEmailWrapper objSpendWrapper = new Hep_SpendEmailWrapper();
        objSpendWrapper.sEmailTitle = 'Boss Baby Title';
        objSpendWrapper.sPromoName = 'Boss Baby';
        objSpendWrapper.sPromoCode = '56736';
        objSpendWrapper.sReqDate = '12-4-2009';
        objSpendWrapper.sPromoFAD = 'yes';
        objSpendWrapper.sBrandManager= 'Mukarjee';
        objSpendWrapper.sComments= 'valid promo';
        objSpendWrapper.sRejReason= 'NA';
        Test.stoptest();
    }
}