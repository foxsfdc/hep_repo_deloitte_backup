/*
 * HEP_Promotion_ToSiebel --- Class for Calling the API to insert/update HEP SKU Promotion records
 * @author  Himanshi Mayal
 */
public with sharing class HEP_Promotion_ToSiebel implements HEP_IntegrationInterface {
    /**
    * PromotionWrapper --- main wrapper which holds the entire request
    * @author  Himanshi Mayal
    */
    public class PromotionWrapper{
        public String ProducerID;
        public String EventType;
        public String EventName;
        public Data Data;
        public PromotionWrapper(Data Data)
        {
        this.ProducerID = HEP_Utility.getConstantValue('HEP_SIEBEL_PRODUCERID');
        this.EventType = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTTYPE');
        this.EventName = HEP_Utility.getConstantValue('HEP_SIEBEL_EVENTNAME');
        this.Data = Data;  
        }
    }
    public class Payload{
        List<Items> items;
    }
    public class Data{
        public Payload Payload;
        public Data(Payload Payload){
        this.Payload = Payload;
        }       
    }
    /**
    * Items --- record level wrapper to hold the record details
    * @author  Himanshi Mayal
    */
    public class Items{
        public String objectType;
        public String createdDate;
        public String createdBy;
        public String updatedDate;
        public String updatedBy;
        public String recordId;
        public String recordStatus;
        public String territoryId;
        public String territoryName;
        public String promotionName;
        public String promotionCode;
        public Date startDate;
        public String lineOfBusiness;
        public String ownerLogin;
        public String parentId;
        public String legacyId;
        public String regionName;

        public string titleNumber;       // added here 5/9 

        public Items(){
        this.objectType = HEP_Utility.getConstantValue('HEP_SIEBEL_PROMOTIONOBJ');      
    }
    }    
      public class ErrorResponseWrapper {
      public String status;
      public String errorMessage;
      public ErrorResponseWrapper(String status, String errorMessage){
          this.status = status;
          this.errorMessage = errorMessage;
      }
  }
    /**
    * Perform the API call.
    * @param empty response
    * @return 
    * @author  Himanshi Mayal
    */  
        public void performTransaction(HEP_InterfaceTxnResponse objTxnResponse){
        String sParameters = '';
        String sAccessToken;
        ErrorResponseWrapper objWrapper; 
        list<String> lstRecordIds = new list<string>();
        system.debug('HEP_InterfaceTxnResponse : ' + objTxnResponse);        
        HEP_Interface_Transaction_Error__c objInterfaceError = new HEP_Interface_Transaction_Error__c();
        sAccessToken = HEP_Integration_Util.getAuthenticationToken('HEP_Siebel_OAuth'); 
        if(sAccessToken!= null && sAccessToken.startsWith(HEP_Utility.getConstantValue('HEP_TOKEN_BEARER')) && objTxnResponse != null){
            HTTPRequest httpRequest = new HTTPRequest();
            HEP_Services__c serviceDetails = HEP_Services__c.getInstance('HEP_Siebel_Service');
            if(serviceDetails != null){
                httpRequest.setEndpoint(serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                httpRequest.setMethod('POST');
                httpRequest.setTimeout(120000);
                httpRequest.setHeader('Content-type', 'application/json');
                httpRequest.setHeader('Authorization',sAccessToken);
                if(String.isNotBlank(objTxnResponse.sSourceId)){
                    lstRecordIds = objTxnResponse.sSourceId.split(',');
                    System.debug('List of record IDS ----- ' + lstRecordIds );                    
                }
                httpRequest.setBody(getPromotionRequestBody(lstRecordIds));

                HTTP http = new HTTP();
                system.debug('serviceDetails.Endpoint_URL__c : ' + serviceDetails.Endpoint_URL__c + serviceDetails.Service_URL__c);
                system.debug('sAccessToken : ' + sAccessToken);
                system.debug('httpRequest : ' + httpRequest);
                System.debug('getPromotionRequestBody(lstRecordIds)'+getPromotionRequestBody(lstRecordIds));
                HTTPResponse httpResp = http.send(httpRequest);
                system.debug('httpResp : ' + httpResp);
                objTxnResponse.sRequest = httpRequest.toString()+'\n Body : \t'+httpRequest.getBody(); //request details
                objTxnResponse.sResponse = httpResp.getBody(); //Response from callout
                if(httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_OK') || httpResp.getStatus() == HEP_Utility.getConstantValue('HEP_STATUS_ACCEPTED')){  
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
                    objTxnResponse.bRetry = false;
                } else{
                    objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
                    objTxnResponse.sErrorMsg = httpResp.getStatus();
                    objTxnResponse.bRetry = true;
                }       
            }
        }
        else{
            System.debug('Invalid Access Token Or Invalid txnResponse data sent');
            objTxnResponse.sStatus = HEP_Utility.getConstantValue('HEP_FAILURE');
            objTxnResponse.sErrorMsg = HEP_Utility.getConstantValue('HEP_INVALID_TOKEN');
            objTxnResponse.bRetry = true;
        }    
    }
    /**
    * Get the serialized json of the request to be sent
    * @param list of record ids
    * @return
    * @author  Himanshi Mayal
    */    
    public static String getPromotionRequestBody(List<string> lstItemsId)
    {
        System.debug('List of RECORD ITEM IDS------- '+lstItemsId);
        String finalJSON = '';
        Boolean valueFalse = false;
        List<HEP_Promotion__c> lstPromotion = new List<HEP_Promotion__c>();
        String sGlobal = HEP_Utility.getConstantValue('CATALOG_TYPE_GLOBAL');
        lstPromotion = [SELECT createdDate, createdBy.Name, lastModifiedDate, lastModifiedBy.Name, id, Record_Status__c,
                       Territory__c, Territory__r.Name, PromotionName__c, LocalPromotionCode__c, StartDate__c,
                       Title__r.FIN_PROD_ID__c,     // added here 5/9
                       LineofBusiness__r.Name, Owner.Name, Global_Promotion__c, Name, Promotion_Type__c, Legacy_Id__c, TPR_Only__c,
                       Domestic_Marketing_Manager__r.FederationIdentifier,MinPromotionDt_Date__c,FirstAvailableDateFormula__c,  
                       (SELECT Parent_Promotion__c, Territory__c, Territory__r.Name FROM HEP_Promotion_Regions__r ORDER BY Territory__r.Name)
                       FROM HEP_Promotion__c WHERE id in :lstItemsId];       
        System.debug('LIST OF PROMOTION QUERY : : :  '+lstPromotion);   
        if(!lstPromotion.isEmpty()){
        
        List<Items> lstItem = new List<Items>();
        for(HEP_Promotion__c promotion : lstPromotion)
        {
            Items objItem = new Items();
            objItem.createdDate = String.ValueOf(promotion.createdDate);
            objItem.createdBy = promotion.createdBy.Name;
            objItem.updatedDate = String.ValueOf(promotion.lastModifiedDate);
            objItem.updatedBy = promotion.lastModifiedBy.Name;
            objItem.recordId = promotion.id;
            objItem.recordStatus = promotion.Record_Status__c;
            objItem.territoryId = promotion.Territory__r.id;
            objItem.territoryName = promotion.Territory__r.Name;
            objItem.promotionName = promotion.PromotionName__c;
            if(promotion.LocalPromotionCode__c != null)
                objItem.promotionCode = promotion.LocalPromotionCode__c;
            else
                objItem.promotionCode = promotion.Name.right(promotion.Name.length()-3) ;
            if(promotion.Promotion_Type__c != sGlobal)
                objItem.startDate = promotion.StartDate__c;
            else
                objItem.startDate = promotion.FirstAvailableDateFormula__c;
            objItem.ownerLogin = (promotion.Domestic_Marketing_Manager__r.FederationIdentifier) != null ? (promotion.Domestic_Marketing_Manager__r.FederationIdentifier).touppercase() : '';
            objItem.lineOfBusiness = promotion.LineofBusiness__r.Name;
            objItem.parentId = promotion.Global_Promotion__c;
            objItem.legacyId = promotion.Legacy_Id__c;

            objItem.titleNumber = promotion.Title__r.FIN_PROD_ID__c;  // added here

            String tempRegionName = '';
            if(!promotion.HEP_Promotion_Regions__r.isEmpty()){
                for(Hep_Promotion_region__c objPromoRegion : promotion.HEP_Promotion_Regions__r)
                {
                    tempRegionName = tempRegionName + objPromoRegion.Territory__r.Name + ';';
                }
                if(tempRegionName.length()>100)
                    tempRegionName = tempRegionName.substring(0, 99);
                System.debug('String containing concatenated Region Names : : '+tempRegionName);
                objItem.regionName = tempRegionName;
                
            }else{
                objItem.regionName = promotion.Territory__r.Name;  
            }  
            lstItem.add(objItem);             
        }
            Payload payl = new Payload();
            payl.items = lstItem;
            Data dat = new Data(payl);
            PromotionWrapper priceRoot = new PromotionWrapper(dat);
            finalJSON = JSON.serialize(priceRoot);
            System.debug(' RETURNED JSON :::: '+finalJSON);
    }
            return finalJSON;
    }
}