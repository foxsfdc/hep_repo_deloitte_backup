@isTest 
public class HEP_Global_LIO_Page_Controller_Test {
    @testSetup
    public static void testSetup(){
        List<HEP_List_Of_Values__c> lstListofvalues  = HEP_Test_Data_Setup_Utility.createHEPListOfValues();
        list<HEP_Constants__c> lstHEPConstant= HEP_Test_Data_Setup_Utility.createHEPConstants();
        list<HEP_Services__c> lstHEPServices = HEP_Test_Data_Setup_Utility.createHEPServices();
        
        User u = HEP_Test_Data_Setup_Utility.createUser('System Administrator', 'Vishnu', 'vishnu@deloitte.com', 'R', 'Vis', 'N', '', true);
        HEP_Role__c objRole = HEP_Test_Data_Setup_Utility.createHEPRole('Marketing Manager', 'Dating', false);
        objRole.Destination_User__c = u.Id;
        objRole.Source_User__c = u.Id;
        insert objRole;
        HEP_Interface__c objInterface = HEP_Test_Data_Setup_Utility.createInterface('HEP_Prophet_Projected_Date', 'HEP_ProphetProjectedDateDetails', true, 10, 10, 'Outbound-Pull', true, true, true);
        
        HEP_Notification_Template__c objTempLockedNotification = HEP_Test_Data_Setup_Utility.createTemplate('Date_Locked_Notification', 'HEP_Promotion_Dating__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempLockedNotification.Unique__c = 'Lockedtemplate';
        insert objTempLockedNotification;
        HEP_Notification_Template__c objTempGlobalFADnotification = HEP_Test_Data_Setup_Utility.createTemplate('Global_FAD_Date_Change', 'HEP_Promotion__c', 'Test Body', 'Test Type', 'Active', 'Draft', null, false);
        objTempGlobalFADnotification.Unique__c = 'templateFAD';
        insert objTempGlobalFADnotification;
    }
    @isTest
    public static void testLIOloaddata(){
        
        User u = [SELECT Id from User where LastName = 'Vishnu'];
        // HTTPRequest request = new HTTPRequest();
        // request.setEndPoint('https://feg-devapi.foxinc.com/v1/prophet/read/api/projectedDate?finTitleIds=197429&territoryIds=79');
        // request.setMethod('GET');
        System.runAs(u) {
        
        //Creating Territory
        HEP_Territory__c Bulgaria = HEP_Test_Data_Setup_Utility.createHEPTerritory('Bulgaria', 'EMEA', 'Licensee', null, null, 'BGN', 'BG', '31', null, true);
        HEP_Territory__c GlobalTerr = HEP_Test_Data_Setup_Utility.createHEPTerritory('Global', 'Domestic', 'Global', null, null, 'USD', 'WW', '9000', null, true);
        
        //ROLE setup for UNLOCK
        HEP_Role__c objRoleOperations = HEP_Test_Data_Setup_Utility.createHEPRole('Operations(International)', 'Promotions' ,true);
        
        
        
        //User role setup 
        HEP_User_Role__c objUserRolePsBulgaria = HEP_Test_Data_Setup_Utility.createHEPUserRole(Bulgaria.Id,objRoleOperations.Id,u.Id,true);
        
        
        //Create LOB
        HEP_Line_Of_Business__c FoxNewRelease = HEP_Test_Data_Setup_Utility.createHEPLineOfBusiness('Fox-New Release', 'New Release', true);
        
        
        //Creating Dating Matrix records
        HEP_Promotions_DatingMatrix__c VODRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'VOD Release Date', GlobalTerr.Id, Bulgaria.Id, false);
        VODRelease.Projected_FAD_Logic__c = 'Same as US DHD/FAD';
         VODRelease.Media_Type__c = 'Digital';
        insert VODRelease;
        HEP_Promotions_DatingMatrix__c ESTRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'EST Release Date', GlobalTerr.Id, Bulgaria.Id, false);
        ESTRelease.Projected_FAD_Logic__c = 'Monday';
         ESTRelease.Media_Type__c = 'Digital';
        insert ESTRelease;
        HEP_Promotions_DatingMatrix__c PhysicalRetailRelease = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Release Dates', 'Physical Release Date (Retail)', GlobalTerr.Id, Bulgaria.Id, false);
         PhysicalRetailRelease.Media_Type__c = 'Physical';
        PhysicalRetailRelease.Projected_FAD_Logic__c = 'Monday';
        insert PhysicalRetailRelease;
        HEP_Promotions_DatingMatrix__c VODAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'VOD Avail Date', GlobalTerr.Id, Bulgaria.Id, false);
         VODAvail.Media_Type__c = 'Digital';
        VODAvail.Date_Offset__c = 30;
        VODAvail.Date_Offset_Source__c = 'VOD Release Date';
        insert VODAvail;
        HEP_Promotions_DatingMatrix__c ESTAvail = HEP_Test_Data_Setup_Utility.createHEPPromotionsDatingMatrix('New Release', 'Avail and Announce Dates', 'EST Avail Date', GlobalTerr.Id, Bulgaria.Id, false);
         ESTAvail.Media_Type__c = 'Digital';
        ESTAvail.Date_Offset__c = 30;
        ESTAvail.Date_Offset_Source__c = 'EST Release Date';
        insert ESTAvail;
        
        //Creating a title ID
        EDM_REF_PRODUCT_TYPE__c prodType = HEP_Test_Data_Setup_Utility.createEDMProductType('Compilation Episode','CMPEP', true);
        EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadette', '197429', 'PUBLC', prodType.id, true);
        EDM_GLOBAL_TITLE__c Title1 = HEP_Test_Data_Setup_Utility.createTitleEDM('Deadpool 3', '20004', 'PUBLC', prodType.id, true);
        //EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Whered You Go Bernadette', '197429', '', prodType.id, true);
        //EDM_GLOBAL_TITLE__c Title = HEP_Test_Data_Setup_Utility.createTitleEDM('Title Test', '', '', prodType.Id,false);
        //insert Title;
        
        //Setting up Promotion and dating records
        HEP_CheckRecursive.clearSetIds();
        
        HEP_Promotion__c testPromo = HEP_Test_Data_Setup_Utility.createPromotion('Where\'d you go Bernadette' , 'Global', null, GlobalTerr.id, FoxNewRelease.Id, null, null, false);
        testPromo.Title__c = Title.Id;
        String FADDate = '2018-07-12';
        testPromo.FirstAvailableDate__c = Date.valueOf(FADDate); 
        System.debug('testPromo.FirstAvailableDate__c *&** '+ testPromo.FirstAvailableDate__c);
        insert testPromo;
        
        HEP_Catalog__c newCatalog = HEP_Test_Data_Setup_Utility.createCatalog('100001', 'Red Tide', 'Single', null, Bulgaria.Id,'Pending', 'Master', true);
        
        HEP_Promotion_Catalog__c promocat = HEP_Test_Data_Setup_Utility.createPromotionCatalog(newCatalog.Id , testPromo.Id, null, true);
        newCatalog.Title_EDM__c = Title1.Id;
        //Create dating records 
        HEP_Promotion_Dating__c DatingRec1 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, VODRelease.id, false);
        DatingRec1.HEP_Catalog__c = newCatalog.Id;
        DatingRec1.Status__c = 'Projected';
        insert DatingRec1;
        
        HEP_Promotion_Dating__c DatingRec2 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, ESTRelease.id, false);
       
        DatingRec2.Status__c = 'Projected';
        insert DatingRec2;
        
        HEP_Promotion_Dating__c DatingRec3 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, PhysicalRetailRelease.id, false);
        DatingRec3.Status__c = 'Projected';
        insert DatingRec3;
        
        HEP_Promotion_Dating__c DatingRec4 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, VODAvail.id, false);
        DatingRec4.Status__c = 'Projected';
        insert DatingRec4;
        
        HEP_Promotion_Dating__c DatingRec5 = HEP_Test_Data_Setup_Utility.createDatingRecord(Bulgaria.Id, testPromo.Id, ESTAvail.id, false);
        DatingRec5.Status__c = 'Projected';
        insert DatingRec5;
        
        DatingRec5.Status__c = 'Tentative';
        //HEP_Catalog__c objCatalog = HEP_Test_Data_Setup_Utility.createCatalog('1234', 'TESTTAL', 'Single', null, objTerritory.Id, 'Pending', 'Request', false);
        
        //Create DAting Approval Type
        HEP_Approval_Type__c objApprovalTypeDating = HEP_Test_Data_Setup_Utility.createApprovalType('UNLOCK_DATING', 'HEP_Promotion_Dating__c', 'Locked_Status__c', 
                                                                                                'Unlocked','Locked','DATING',
                                                                                                'Static','Pending',                                                                 
                                                                                                false);
        objApprovalTypeDating.Notif_App_Template__c = 'Dating_Unlock_Request_Approved';
        objApprovalTypeDating.Notif_Rej_Template__c = 'Dating_Unlock_Request_Rejected';
        objApprovalTypeDating.Email_App_Template__c = 'HEP_Dating_Approval_Response';
        objApprovalTypeDating.Email_Rej_Template__c = 'HEP_Dating_Approval_Response';
        objApprovalTypeDating.Email_Req_Template__c = 'HEP_Dating_Approval_Request';
        insert objApprovalTypeDating;
         
         HEP_Rule__c objRule3 = HEP_Test_Data_Setup_Utility.createRule('Dating Unlock Rule International','HEP_Promotion_Dating__c','(1 AND 2)',true);
        Id RecordTypeId = Schema.SObjectType.HEP_Rule_Criterion__c.getRecordTypeInfosByName().get('Compare Field Value').getRecordTypeId();
        
        List<HEP_Rule_Criterion__c> lstRuleCriterias = new List<HEP_Rule_Criterion__c>();
        HEP_Rule_Criterion__c objRuleCriteria5 = HEP_Test_Data_Setup_Utility.createRuleCriteria('Digital_Physical__c','Digital',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
        objRuleCriteria5.HEP_Source_SObject_Field_Type__c= 'STRING';
        objRuleCriteria5.HEP_Serial_Number__c = 1;
        lstRuleCriterias.add(objRuleCriteria5);
        HEP_Rule_Criterion__c objRuleCriteria6 = HEP_Test_Data_Setup_Utility.createRuleCriteria('CC_Email_Filter__c','Bulgaria',HEP_Rule_Constants.HEP_STRING_EQUALS_TO ,objRule3.Id,RecordTypeId, false);
        objRuleCriteria6.HEP_Source_SObject_Field_Type__c= 'STRING';
        objRuleCriteria6.HEP_Serial_Number__c = 2;
        lstRuleCriterias.add(objRuleCriteria6);
        insert lstRuleCriterias;
        
        //Approval Type Role Setup
    List<HEP_Approval_Type_Role__c> lstApprovalTypeRole = new List<HEP_Approval_Type_Role__c>();
    
    //Approver role For Dating
    HEP_Approval_Type_Role__c objApprovalTypeRole3 = HEP_Test_Data_Setup_Utility.createApprovalTypeRole(objApprovalTypeDating.Id, objRoleOperations.Id, 'ALWAYS', false);
    objApprovalTypeRole3.Rule__c = objRule3.Id;
    objApprovalTypeRole3.Sequence__c = 1;
    lstApprovalTypeRole.add(objApprovalTypeRole3);
    
    
    insert lstApprovalTypeRole;
        
        
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HEP_Utility_HttpMockCallout());
            HEP_Global_LIO_Page_Controller.GlobalLIOData dataobj = HEP_Global_LIO_Page_Controller.loadData(testPromo.Id, 'DD-MMM-YYYY');
            HEP_Global_LIO_Page_Controller.loadData(testPromo.Id, 'DD-MMM-YYYY');
            //List<HEP_Global_LIO_Page_Controller.DateRow> dateRows = dataobj.lstDateRows;
            for(HEP_Global_LIO_Page_Controller.DateRow rowDate : dataobj.lstDateRows){
                rowDate.sStatus = 'Confirmed';
                rowDate.sLockedStatus = 'Locked';
            }
            HEP_Global_LIO_Page_Controller.saveData(dataObj);
            HEP_Global_LIO_Page_Controller.GlobalLIOData dataobj1 = HEP_Global_LIO_Page_Controller.loadData(testPromo.Id, 'DD-MMM-YYYY');
            
            HEP_Global_LIO_Page_Controller.saveUnlockComments('Unlock it', DatingRec5.Id, Bulgaria.Id, 'DD-MM-YYYY');
            HEP_Global_LIO_Page_Controller.saveData(dataObj1);
            HEP_Global_LIO_Page_Controller.getDateHistory(DatingRec5.Id);
            Test.Stoptest();
    }
    }
    
}