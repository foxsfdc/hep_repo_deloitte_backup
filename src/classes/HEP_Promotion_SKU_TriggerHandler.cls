public without sharing class HEP_Promotion_SKU_TriggerHandler extends TriggerHandler
{
    
    public static String sACTIVE;
    public static string sAPPROVED;
    public static string sDRAFT;
    public static string sREQUEST;
    public static string sESCOSET;
    public static string sJDESET, sDELETED;
    static{
       sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');
       sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
       sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
       sDRAFT = HEP_Utility.getConstantValue('DRAFT');
       sREQUEST = HEP_Utility.getConstantValue('HEP_SKU_MASTER_TYPE_REQUEST');
       sESCOSET = HEP_Utility.getConstantValue('HEP_ESCO');
       sJDESET = HEP_Utility.getConstantValue('HEP_JDE');
    }
    
    /**
    * afterInsert -- Context specific afterInsert method
    * @return nothing
     *@author  Mayank Jaggi
    */
    public override void afterInsert()
    {
        List<id> lstId = new List<id>();
        for(HEP_Promotion_SKU__c objPromoSku : (List<HEP_Promotion_SKU__c>)Trigger.new)
        {
            lstId.add(objPromoSku.Id);
        }
        //callQueueableForE1(lstId);
        System.debug('newMap>>' + Trigger.newMap);
        callQueuableMethod((Map<Id, HEP_Promotion_SKU__c>) Trigger.newMap);
    }
    
    
    /**
    * BeforUpdate -- Context specific Before Update method
    * @return nothing
     *@author  Nishit Kedia
    */
    public override void beforeUpdate()
    {
        if(Trigger.oldMap != Null)
        {
             /*System.debug('newMap>>' + Trigger.newMap);
              callQueuableMethod((Map<Id, HEP_Promotion_SKU__c>) Trigger.newMap);*/
              
             //------------------------------------------------------//
             // Update the Last Approved Date to System Date when ApprovalStatus is Approved
     
             for(Id skupromoId : Trigger.newMap.keySet()){
                HEP_Promotion_SKU__c objNewPromoSku = (HEP_Promotion_SKU__c)Trigger.newMap.get(skupromoId);
                HEP_Promotion_SKU__c objOldPromoSku = (HEP_Promotion_SKU__c)Trigger.oldMap.get(skupromoId);
                if(objNewPromoSku.Approval_Status__c.equalsIgnoreCase(sAPPROVED) && !objOldPromoSku.Approval_Status__c.equalsIgnoreCase(sAPPROVED)){
                    objNewPromoSku.Last_Approved_Date__c = System.Today();
                }
             }
     //-----------------------------------------------------//
        }
    }
    

     /**
    * afterUpdate -- Context specific afterUpdate method
    * @return nothing
     *@author  Mayank Jaggi
    */
    public override void afterUpdate()
    {
        Set<String> territorySet = new Set<String>{'US', 'Canada', 'DHE'};
        
        if(Trigger.oldMap != Null)
        {
            System.debug('oldMap>>' + Trigger.oldMap);
            System.debug('newMap>>' + Trigger.newMap);
            Map<Id, HEP_Promotion_SKU__c> oldMap = (Map<Id, HEP_Promotion_SKU__c>)Trigger.oldMap;
            String sRecordInactive = HEP_Utility.getConstantValue('HEP_RECORD_STATUS_INACTIVE');
            Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_Promotion_SKU__c', 'Promotion_SKU_Field_Set');
                    System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
            if(checkFieldUpdate)
                callQueuableMethod((Map<Id, HEP_Promotion_SKU__c>) Trigger.newMap);
             
             //---------------------------------------------//
             Map<Id, HEP_Promotion_SKU__c> mapSkuPromotions = new Map<Id, HEP_Promotion_SKU__c>([Select Id,
                                                                                                        Locked_Status__c,
                                                                                                        SKU_Master__c,
                                                                                                        SKU_Master__r.SKU_Number__c,
                                                                                                        Approval_Status__c,
                                                                                                        SKU_Master__r.Territory__r.SKU_Interface__c,
                                                                                                        SKU_Master__r.Territory__r.Name,
                                                                                                        SKU_Master__r.Type__c, Record_Status__c,
                                                                                                        Promotion_Catalog__r.Promotion__c
                                                                                                        from
                                                                                                        HEP_Promotion_SKU__c
                                                                                                        where
                                                                                                        Id In:Trigger.newMap.keySet()
                                                                                                    ]);
             Set<Id> setSkusUnlockedInLockPeriod = new Set<Id>();
             Set<Id> setSmartESCOSkusNoToBeGenerated = new Set<Id>();
             Set<Id> setSKUsToBeSentToESCO = new Set<Id>();
             Set<Id> setSKUsToBeSentToJDE = new Set<Id>();
             Set<Id> set_PromotionIds = new Set<Id>();
             List<id> lstId = new List<id>();
             
             if(mapSkuPromotions != null && !mapSkuPromotions.isEmpty()){
                 for(Id sPromoSkuRecordId: mapSkuPromotions.KeySet()){
                    HEP_Promotion_SKU__c objPromotionSku = mapSkuPromotions.get(sPromoSkuRecordId) ;
                    
                    //If Status is Send for Approval Auto Submit them for Approval [Auto Submission in Lock Period]
                    if(objPromotionSku.Locked_Status__c != null && objPromotionSku.Locked_Status__c.equalsIgnoreCase(HEP_Utility.getConstantValue('HEP_STATUS_SEND_FOR_APPROVAL'))){
                        setSkusUnlockedInLockPeriod.add(objPromotionSku.Id);
                    }
                    
                    // If Status is Approved and ESCO is set for SKU Request with Blank Sku Number then send for Smart SKU number Generation
                    if(objPromotionSku.SKU_Master__r != null && String.isBlank(objPromotionSku.SKU_Master__r.SKU_Number__c) && objPromotionSku.Approval_Status__c.equalsIgnoreCase(sAPPROVED) 
                        && objPromotionSku.SKU_Master__r.Type__c.equalsIgnoreCase(sREQUEST) &&  objPromotionSku.SKU_Master__r.Territory__r.SKU_Interface__c.equalsIgnoreCase(sESCOSET)){
                        setSmartESCOSkusNoToBeGenerated.add(objPromotionSku.SKU_Master__c);
                    }
                    
                    // If Status is Approved and ESCO is set for SKU Request: Log in ETL Object
                    if(objPromotionSku.SKU_Master__r != null //&& String.isNotBlank(objPromotionSku.SKU_Master__r.SKU_Number__c) 
                        && objPromotionSku.Approval_Status__c.equalsIgnoreCase(sAPPROVED) 
                        && objPromotionSku.SKU_Master__r.Territory__r.SKU_Interface__c.equalsIgnoreCase(sESCOSET)){
                        setSKUsToBeSentToESCO.add(objPromotionSku.Id);
                    }
                    
                    //If Status is Approved and JDE is set for SKU: Then Change JDE Pending Flag
                    if(objPromotionSku.SKU_Master__r != null && objPromotionSku.Approval_Status__c.equalsIgnoreCase(sAPPROVED) 
                        &&  objPromotionSku.SKU_Master__r.Territory__r.SKU_Interface__c.equalsIgnoreCase(sJDESET)){
                        setSKUsToBeSentToJDE.add(objPromotionSku.Id);
                    }
                    
                    //Avinash : If Promotion SKU is approved or Moved from Approved to Draft
                    if(oldMap != null &&  territorySet.contains(objPromotionSku.SKU_Master__r.Territory__r.Name) && 
                    sJDESET.equalsIgnoreCase(objPromotionSku.SKU_Master__r.Territory__r.SKU_Interface__c) && (sAPPROVED.equalsIgnoreCase(objPromotionSku.Approval_Status__c) && !sAPPROVED.equalsIgnoreCase(oldMap.get(objPromotionSku.Id).Approval_Status__c)) ||
                        (sDRAFT.equalsIgnoreCase(objPromotionSku.Approval_Status__c) && sAPPROVED.equalsIgnoreCase(oldMap.get(objPromotionSku.Id).Approval_Status__c))
                        || (sDELETED.equalsIgnoreCase(objPromotionSku.Record_Status__c) && !sDELETED.equalsIgnoreCase(oldMap.get(objPromotionSku.Id).Record_Status__c)))
                        set_PromotionIds.add(objPromotionSku.Promotion_Catalog__r.Promotion__c);

                    //Himanshi : If Record is Inactivated, send record for E1 Spend Integration till line 149
                    if((objPromotionSku.Record_Status__c != (oldMap.get(sPromoSkuRecordId)).Record_Status__c) && (objPromotionSku.Record_Status__c == 'Deleted')){
                        lstId.add(sPromoSkuRecordId);
                    }              
                    //Kunal : If Promotion SKU is Approved or Draft then send to E1 (don't send when it is going through Approval process)
                    if(sAPPROVED.equalsIgnoreCase(objPromotionSku.Approval_Status__c) || sDRAFT.equalsIgnoreCase(objPromotionSku.Approval_Status__c)){
                        lstId.add(objPromotionSku.Id);        
                    }
                 }
                 if((lstId != Null) && (!lstId.isEmpty()))
                 {
                    callQueueableForE1(lstId);
                 }
         }
         
         /* Auto Submission to Approval Framework when new SKU Request is  
            Unlocked By Operations while Promotion has Gone in the Lock 
            Period :[MP-145]
         */
         if(setSkusUnlockedInLockPeriod != null && !setSkusUnlockedInLockPeriod.isEmpty()){
            HEP_SKU_Utility.autoApprovalSubmissionInLockPeriod(setSkusUnlockedInLockPeriod);
         }
         
         /* If the sku is ESCO and Unique Number has not been Generated then
            generate the SKu number based on Smart Algorithm :[MP-152]
         */
         if(setSmartESCOSkusNoToBeGenerated != null && !setSmartESCOSkusNoToBeGenerated.isEmpty()){
            HEP_Generate_SKU_Number.generateUniqueSKUNumber(setSmartESCOSkusNoToBeGenerated);
         }
         
         /* For Approved ESCO SKU(s) the Records are picked up by
            ETL object and is sent to ESCO for Mapping :[MP-406]
         */
         if(setSKUsToBeSentToESCO != null && !setSKUsToBeSentToESCO.isEmpty()){
            HEP_SKU_Utility.sendSKUDetailsForESCO(setSKUsToBeSentToESCO);
         }
         
         /* For Approved JDE SKU(s) the Price Regions will be sent
            to JDE: Update JDE Pending Flag: [MP-152]
         */
         if(setSKUsToBeSentToJDE != null && !setSKUsToBeSentToJDE.isEmpty()){
            HEP_SKU_Utility.updatePriceRegionsJDEFlag(setSKUsToBeSentToJDE);
         }
         
         if(!set_PromotionIds.isEmpty()){
            for(Id promoId : set_PromotionIds){
                System.enqueueJob(new HEP_INT_JDE_Integration_Queueable(promoId));
            }
         }
         
         //---------------------------------------------//
        }
    }
    
    /**
    * callQueuableMethod -- enqueues the job
    * @param map of ids and HEP_Promotion_SKU__c records
    * @return nothing
     *@author  Mayank Jaggi
    */
    public static void callQueuableMethod(Map<Id, HEP_Promotion_SKU__c> mapSKUs)
    {
        String promotionTypeGlobal = HEP_Utility.getConstantValue('HEP_PROMOTION_TYPE_GLOBAL');
        String promotionTypeNational = HEP_Utility.getConstantValue('PROMOTION_TYPE_NATIONAL');
      List<Id> lstSKUIds = new List<Id>();
      for(HEP_Promotion_SKU__c promoSKU : [Select Id, Promotion_Catalog__r.Promotion__r.Promotion_Type__c from HEP_Promotion_SKU__c Where Id IN :mapSKUs.keySet() AND  (Promotion_Catalog__r.Promotion__r.Promotion_Type__c = :promotionTypeGlobal OR Promotion_Catalog__r.Promotion__r.Promotion_Type__c = :promotionTypeNational)] ){
            lstSKUIds.add(promoSKU.Id);
        }  
     //lstSKUIds.addAll(mapSKUs.keySet());
        if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
         System.enqueueJob(new HEP_Siebel_Queueable(String.join(lstSKUIds, ','),
                 HEP_Constants__c.getValues('HEP_PROMOTION_SKU').Value__c));
        }
    }
    /**
    * callQueueableForE1 -- send the details to make the call out to Queueable class
    * @param  List od PromotionSku Ids
    * @return 
    * @author Himanshi Mayal     
    */
    public static void callQueueableForE1(List<id> lstPromoSkuId)
    {
        //List<id> lstPromoId = new List<id>();
        String sHEP_Int_Txn_Response_Status_Success = HEP_Utility.getConstantValue('HEP_Int_Txn_Response_Status_Success');
        String sHEP_E1_SpendDetail = HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL'); 
        HEP_Interface__c objInterfaceRec = [SELECT id FROM HEP_Interface__c WHERE Name =: sHEP_E1_SpendDetail]; 
        map<String,HEP_Interface_Transaction__c> mapPromoIdInterfaceRecord = new map<String,HEP_Interface_Transaction__c>();
        Set<Id> setPromoId = new Set<Id>();
        for(HEP_Promotion_SKU__c objPromoSKU : [SELECT id, Promotion_Catalog__r.Promotion__c FROM HEP_Promotion_SKU__c WHERE id IN :lstPromoSkuId AND Promotion_Catalog__r.Promotion__r.Territory__r.E1_Code__c != NULL])
        {
            //lstPromoId.add(objPromoSKU.Promotion_Catalog__r.Promotion__c);
            setPromoId.add(objPromoSKU.Promotion_Catalog__r.Promotion__c);
        }
        if(setPromoId != NULL && !setPromoId.isEmpty() && objInterfaceRec.Id != NULL){
            //Creation of interface transaction records are being done as the autonumber is being used as the unique number 
            for(String promoId : setPromoId)
            {
                HEP_Interface_Transaction__c objTxn = new HEP_Interface_Transaction__c();
                objTxn.Object_Id__c = promoId;
                objTxn.HEP_Interface__c = objInterfaceRec.Id;
                objTxn.Transaction_Datetime__c = System.now();
                objTxn.Status__c = sHEP_Int_Txn_Response_Status_Success;
                mapPromoIdInterfaceRecord.put(promoId,objTxn);
            }
            insert mapPromoIdInterfaceRecord.Values();
            for(String promoId : setPromoId)
            {   
                //String sjson = json.serialize(lstPromoId);
                HEP_MarketSpendTriggerHandler.E1Wrapper objE1Wrapper = new HEP_MarketSpendTriggerHandler.E1Wrapper();
                if(mapPromoIdInterfaceRecord.containsKey(promoId)){
                    objE1Wrapper.sPromoId = promoId;
                    objE1Wrapper.sTransactionId = mapPromoIdInterfaceRecord.get(promoId).Id;
                }
                String sSerlizedWrap = JSON.serialize(objE1Wrapper);    
                System.debug('STRING --- '+promoId);
                if(!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())){
                    if(objE1Wrapper.sPromoId != NULL && objE1Wrapper.sTransactionId != NULL){
                        System.debug('GOING FOR QUEUEABLE-----');
                        System.enqueueJob(new HEP_E1_Queueable(sSerlizedWrap , HEP_Utility.getConstantValue('HEP_E1_SPEND_DETAIL')));
                    }
                }
            }
        }                
    }
}