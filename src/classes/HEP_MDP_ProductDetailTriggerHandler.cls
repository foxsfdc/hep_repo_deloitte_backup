/**
 * HEP_MDP_ProductDetailTriggerHandler --- Handler class for HEP_MDP_ProductDetailTrigger
 * @author  Nidhin V K
 */
public class HEP_MDP_ProductDetailTriggerHandler extends TriggerHandler{
    
    public static String sAPPROVED, sPUBLISHED, sACTIVE, sSUBMITTED, sDELETED;
    static{
        sACTIVE = HEP_Utility.getConstantValue('HEP_RECORD_ACTIVE');       
        sDELETED = HEP_Utility.getConstantValue('HEP_RECORD_DELETED');
        sAPPROVED = HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED');
        sPUBLISHED = HEP_Utility.getConstantValue('HEP_PROMOTION_PUBLISHED');
        sSUBMITTED = HEP_Utility.getConstantValue('HEP_PROMOTION_SUBMITTED');
    }
    
    
    public override void beforeInsert(){
        
        for(HEP_MDP_Promotion_Product_Detail__c objProductDetail : (List<HEP_MDP_Promotion_Product_Detail__c>)Trigger.new){
            objProductDetail.HEP_Unique_Id__c = objProductDetail.HEP_Promotion_MDP_Product__c+ ' / '+(objProductDetail.Channel__c!=null?String.valueOf(objProductDetail.Channel__c):'')+' /'+(objProductDetail.Format__c!=null?' '+String.valueOf(objProductDetail.Format__c):'') ;
        }
    }
    
     
     public override void beforeUpdate(){         
        for(HEP_MDP_Promotion_Product_Detail__c objProductDetail :(List<HEP_MDP_Promotion_Product_Detail__c>)  Trigger.new){
            objProductDetail.HEP_Unique_Id__c = objProductDetail.HEP_Promotion_MDP_Product__c+ ' / '+(objProductDetail.Channel__c!=null?String.valueOf(objProductDetail.Channel__c):'')+' /'+(objProductDetail.Format__c!=null?' '+String.valueOf(objProductDetail.Format__c):'') ;
        }
     }
    
    
    /**
    * afterInsert --- all after insert logic
    * @param nothing
    * @exception Any exception
    * @return void
    */
    public override void afterInsert(){
        
        Map<id,HEP_MDP_Promotion_Product_Detail__c> mapPromotion_Details = new Map<id,HEP_MDP_Promotion_Product_Detail__c>();
       
        //updates report status
        updateReportStatus(NULL, (Map<Id, HEP_MDP_Promotion_Product_Detail__c>) Trigger.newMap);
        //inovke Itune Integration on Insert  
        
        mapPromotion_Details =  getUpdateditem((Map<Id, HEP_MDP_Promotion_Product_Detail__c>) Trigger.newMap);
        system.debug('mapPromotion_Details ********'+mapPromotion_Details );
        
       if(mapPromotion_Details != NULL && !mapPromotion_Details.isEmpty()){
           if(HEP_CheckRecursive.runOnce()){ 
                // Database.executeBatch(new HEP_ItunesCallOut_Batch((Map<Id, HEP_MDP_Promotion_Product_Detail__c>) mapPromotion_Details),1);
            }    
        }     
    }
     
   
     
     
    /**
    * afterUpdate --- all after update logic
    * @param
    * @exception Any exception
    * @return void
    */ 
    public override void afterUpdate(){
        
        
        Map<id,HEP_MDP_Promotion_Product_Detail__c> mapPromotion_Details = new Map<id,HEP_MDP_Promotion_Product_Detail__c>();

        //updates report status
        updateReportStatus((Map<Id, HEP_MDP_Promotion_Product_Detail__c>) Trigger.oldMap, (Map<Id, HEP_MDP_Promotion_Product_Detail__c>) Trigger.newMap);
        
        // check if Prome Price date is changed 
        Boolean checkFieldUpdate = HEP_Utility.checkFieldUpdation(Trigger.newMap, Trigger.oldMap, 'HEP_MDP_Promotion_Product_Detail__c', 'Promotion_Product_Detail_Field_Set');
        System.debug('THE VALUE FOR THE FLAG FIELD------ '+checkFieldUpdate);
        if(checkFieldUpdate){
         
           mapPromotion_Details =  getUpdateditem((Map<Id, HEP_MDP_Promotion_Product_Detail__c>) Trigger.newMap);
            system.debug('mapPromotion_Details ********'+mapPromotion_Details );
            
            if(mapPromotion_Details != NULL && !mapPromotion_Details.isEmpty()){
                if(HEP_CheckRecursive.runOnce()){
                    // Database.executeBatch(new HEP_ItunesCallOut_Batch((Map<Id, HEP_MDP_Promotion_Product_Detail__c>) mapPromotion_Details),1);
                }
            }         
        }
    }
        
    
    /**
    * updateReportStatus --- updates the customer promotion report Status
    * @param Map<Id, HEP_MDP_Promotion_Product_Detail__c> oldMap, Map<Id, HEP_MDP_Promotion_Product_Detail__c> newMap
    * @exception Any exception
    * @return void
    */
    public static void updateReportStatus(Map<Id, HEP_MDP_Promotion_Product_Detail__c> oldMap, Map<Id, HEP_MDP_Promotion_Product_Detail__c> newMap){
        try{
            System.debug('oldMap>>' + Trigger.oldMap);
            System.debug('newMap>>' + Trigger.newMap);
            Map<Id, HEP_Promotion__c> mapPromotions = new Map<Id, HEP_Promotion__c>();
            for(HEP_MDP_Promotion_Product_Detail__c objNewDetail : [SELECT 
                                                                        Id, HEP_Promotion_MDP_Product__r.HEP_Promotion__c, 
                                                                        HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Suisse_Updated__c,
                                                                        Promo_SRP__c,  Promo_WSP__c 
                                                                    FROM 
                                                                        HEP_MDP_Promotion_Product_Detail__c 
                                                                    WHERE 
                                                                        Id IN :newMap.keySet()
                                                                    AND
                                                                        Record_Status__c IN (:sACTIVE, :sDELETED)]){
                HEP_MDP_Promotion_Product_Detail__c objOldDetail = (oldMap == NULL) ? NULL : oldMap.get(objNewDetail.Id);
                if((oldMap == NULL
                    || objOldDetail.Promo_SRP__c != objNewDetail.Promo_SRP__c
                    || objOldDetail.Promo_WSP__c != objNewDetail.Promo_WSP__c)
                    && objNewDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__r.Suisse_Updated__c)
                    
                    mapPromotions.put(objNewDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__c, 
                        new HEP_Promotion__c(Id = objNewDetail.HEP_Promotion_MDP_Product__r.HEP_Promotion__c, Suisse_Updated__c = false));
            }
            if(mapPromotions.size() > 0)
                update mapPromotions.values();
        } catch(Exception ex){
            HEP_Error_Log.genericException('DML Erros','DML Errors',ex,'HEP_MDP_ProductDetailTriggerHandler','updatePromotionApprovalStatus',null,null); 
            
            System.debug('Exception on Class : HEP_MDP_ProductDetailTriggerHandler - updatePromotionApprovalStatus, Error : ' 
                            + ex.getMessage() 
                            + ' Line Number : ' 
                            + ex.getLineNumber()
                            + ' Cause : '
                            + ex.getCause()
                            + ' Type : '
                            + ex.getTypeName());
        }
    }
    
    
    public static map<id,HEP_MDP_Promotion_Product_Detail__c> getUpdateditem(Map<Id, HEP_MDP_Promotion_Product_Detail__c> TriggernewMap){
    
        Map<id,list<HEP_MDP_Promotion_Product_Detail__c>>  mapPromotionsId_ProDetails= new Map<id,list<HEP_MDP_Promotion_Product_Detail__c>>();
        Map<id,HEP_MDP_Promotion_Product_Detail__c> mapUpdatedPromotion_Details = new Map<id,HEP_MDP_Promotion_Product_Detail__c>();
        list<HEP_MDP_Promotion_Product_Detail__c> lstPromotions_Details = new list<HEP_MDP_Promotion_Product_Detail__c>();  
        list<HEP_MDP_Promotion_Product_Detail__c> lstPromotions_Details1 = new list<HEP_MDP_Promotion_Product_Detail__c>();  
        Set<Id> promotionID = new Set<Id>();
    
             for(HEP_MDP_Promotion_Product_Detail__c obj: (List<HEP_MDP_Promotion_Product_Detail__c>) TriggernewMap.Values())
            {
                promotionID.add(obj.HEP_Promotion_MDP_Product__c);
            }
            
            lstPromotions_Details = [SELECT id,Promo_SRP__c,Format__c,Channel__c,Record_Status__c,Promo_WSP__c,HEP_Promotion_MDP_Product__c,HEP_Promotion_MDP_Product__r.Approval_Status__c FROM HEP_MDP_Promotion_Product_Detail__c WHERE 
                                    Channel__c =: HEP_Utility.getConstantValue('HEP_CHANNEL_EST') AND 
                                    (Format__c =: HEP_Utility.getConstantValue('HEP_HD') OR Format__c =: HEP_Utility.getConstantValue('HEP_SD')) 
                                    AND  HEP_Promotion_MDP_Product__c IN:promotionID AND HEP_Promotion_MDP_Product__r.Approval_Status__c =: HEP_Utility.getConstantValue('HEP_APPROVAL_APPROVED')];
                                    
            System.debug('lstPromotions_Details '+lstPromotions_Details );
            
            for(HEP_MDP_Promotion_Product_Detail__c obj:lstPromotions_Details){
                if(mapPromotionsId_ProDetails.Containskey(obj.HEP_Promotion_MDP_Product__c)){
                mapPromotionsId_ProDetails.get(obj.HEP_Promotion_MDP_Product__c).add(obj);
                }
                else{
                    mapPromotionsId_ProDetails.put(obj.HEP_Promotion_MDP_Product__c,new List<HEP_MDP_Promotion_Product_Detail__c>{obj});
                }   
              }  
            
          
           system.debug('mapPromotionsId_ProDetails********'+mapPromotionsId_ProDetails); 
           
            
           for(String Pro_id: mapPromotionsId_ProDetails.keyset()){
                if(mapPromotionsId_ProDetails.get(Pro_id).size()>1){
                    lstPromotions_Details1 = mapPromotionsId_ProDetails.get(Pro_id);
                    for(HEP_MDP_Promotion_Product_Detail__c Prod_Details: lstPromotions_Details1){
                        if(Trigger.newMap.get(Prod_Details.id) != NULL)
                             mapUpdatedPromotion_Details.put(Prod_Details.id,(HEP_MDP_Promotion_Product_Detail__c)Trigger.newMap.get(Prod_Details.id));
                    }
                 }         
            }
            
            return mapUpdatedPromotion_Details;
    
    }    
}