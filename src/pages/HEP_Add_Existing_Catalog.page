<!-- PAGE NAME : HEP_Add_Existing_Catalog -->
<apex:page applyHtmlTag="false" applyBodyTag="false" showHeader="false" docType="html-5.0" standardStylesheets="false" controller="HEP_Add_Existing_Catalog_Controller" action="{!checkPermissions}">
     <c:HEP_Navigation_Menu_Component />
 
     <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="" />
        <meta name="author" content="" />
    </head>
    <body ng-app="PromotionsApp" class="hep-request-catalog-body">
        <div ng-controller="HEP_Add_Catalog_Ctrl">
        <div class="minwrap-container hep-request-catalog-container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hep-padding0">
                    <header class="hep-header">
                            <h1 class="hep-normal"><span class="hep-bolder">Add Catalogs</span> to {{sPromoName}}</h1>
                            <span class="hep-button-group hep-flex-center" ng-show="">
                                <a class="hep-cancel-link pointer-cursor" href="" ng-show="" ng-click="redirectBackButton();">Cancel</a>
                                <button class="hep-btn" ng-show="" ng-click="saveCatalogData();">Add</button>
                            </span>
                    </header>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hep-padding0">
                    <div class="hep-line"></div>
                </div>
            </div>
            <div class="hep-modal-overlay" ng-cloak="true" ng-show="showDuplicateCatalogModal">
                <div class="hep-modal">
                    <div class=" content-section">
                        <h3 class="subHeader">Following Catalogs are already Added to Promotion</h3>
                                <div ng-repeat="catalog in lstDuplicateCatalogs">{{catalog.sCatalog}} - {{catalog.sCatalogName}}</div>
                        <div class="text-right">
                            <button type="button" class="hep-btn" ng-click="redirectOkButton();">OK</button>
                        </div>
                        </div>
                </div>
            </div>
            <div class="row hep-request-catalog-grid-container">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hep-search-catalog-container hep-padding0 hep-flex-center">
                            <input type="text" tabindex="1" class="hep-search-catalog"  ng-model="searchCatalogValue" ng-paste="formatViewValue($event);"  placeholder="Search by Name or Catalog#" typeahead-min-length="3" typeahead-on-select="getCatalogData($item)" typeahead-focus-first="false" typeahead-editable="true" uib-typeahead="x.sValue for x in searchCatalog($viewValue)" ng-keypress="callSearchCatalog($event,searchCatalogValue)"/> 
                            <div class="hep-clear-search pointer-cursor" ng-click="clearSearch()"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 hep-paddingLeft0">
                            <div ag-grid="gridOptionsCatalog" class="ag-theme-material gt-table hep-request-catalog-grid"></div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 paddingZero selectedDataContainer">
                            <div class="bold-row selectedCountContainer">{{selectedRowsCount}} Catalogs selected</div>
                            <div class="selectedRowsContainer">
                                <div class="indvSelectedData hep-flex-spaceBtn hep-break-word" ng-repeat="catalog in lstSelectedCatalogs">{{catalog.sCatalogName}}<span class="closeBtnSmall pointer-cursor" ng-click="uncheckPromotion(catalog)"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </body>
    
    <script>
        agGrid.LicenseManager.setLicenseKey(sAgGridLicenseKey); 
        agGrid.initialiseAgGridWithAngular1(angular);       
        var pageName = "{!$CurrentPage.name}";
        // load app only if its not already loaded so that its not overwritten
        if(app)
            {}
        else
            var app = angular.module("PromotionsApp", ['agGrid','ui.bootstrap']);
        
       
        function catalogFactory($q){
            // Method to search through the catalogs
             var searchCatalog = function(sCatalog,sCatalogTerritory){
                var deferred = $q.defer();
                    HEP_Add_Existing_Catalog_Controller.searchCatalog(sCatalog,sCatalogTerritory,
                        function(result, event) {
                            if (event.status) {
                                hideShowProgress(true,  false);
                               deferred.resolve(angular.fromJson(result));
                            } else {
                                deferred.reject('Error: Could not fetch the data.');
                                hideShowProgress(false,  false);
                            }
                        },
                        {escape: false}
                    );
                return deferred.promise;
            }
            // Method to retrieve catalog data
            var getCatalogData = function(sValue,sCatalogTerritory){
                 var deferred = $q.defer();
                HEP_Add_Existing_Catalog_Controller.getCatalogData(sValue,sCatalogTerritory,function(result,event){
                 if(event.status){
                    hideShowProgress(true,  false);
                     deferred.resolve(angular.fromJson(result));
                 } else {
                     deferred.reject('Error: Could not fetch the data.');
                     hideShowProgress(false,  false);
                 }
                },
                {escape:false});
                  return deferred.promise;
            }
            // Method to save catalog data
            var saveCatalogData = function(lstCatalogs,sPromoId,sCatalogTerritory){
                 var deferred = $q.defer();
                HEP_Add_Existing_Catalog_Controller.saveCatalogData(lstCatalogs,sPromoId,sCatalogTerritory,function(result,event){
                 if(event.status){
                    hideShowProgress(true,  false);
                     deferred.resolve(angular.fromJson(result));
                 } else {
                     deferred.reject('Error: Could not fetch the data.');
                     hideShowProgress(false,  false);
                 }
                },
                {escape:false});
                  return deferred.promise;
            }
            return{
                searchCatalog : searchCatalog,
                getCatalogData  :getCatalogData,
                saveCatalogData  :saveCatalogData
            }
        }
        
        app.factory('catalogFactory', catalogFactory);
        // Controller for Add Catalog Page
        app.controller("HEP_Add_Catalog_Ctrl", HEP_Add_Catalog_Ctrl);
        function HEP_Add_Catalog_Ctrl($scope, $timeout,catalogFactory,$rootScope){
            $scope.selectedRows = [];
            $scope.selectedRowsCount = 0;
            $scope.catalogMap = new Map();
            $scope.lstId = [];
            $scope.sPromoName = "{!sPromotionName}";
            $scope.sPromoId = "{!$CurrentPage.parameters.promoId}";
            $scope.sCatalogTerritory = "{!sCatalogTerritory}";
            $scope.lstSelectedCatalogs = [];
            $scope.showDuplicateCatalogModal = false;
            // called on pageload
            $scope.loadData = function(){
                hideShowProgress (true,false);
                $scope.showDuplicateCatalogModal = false;
                $scope.lstId = [];
                $scope.lstSelectedCatalogs = [];
            }
            
         // Method to handle the redirection to PO or Invoice pages
         $scope.redirectBackButton = function() {
                        hideShowProgress(false,  true);
                        window.location.href = '/apex/HEP_Promotion_Details?promoId='+$scope.sPromoId+'&tab=HEP_Promotion_Products';
                }
                 // Method to handle the redirection to PO or Invoice pages
         $scope.redirectOkButton = function() {
                        hideShowProgress(false,  true);
                        window.location.href = '/apex/HEP_Promotion_Details?promoId='+$scope.sPromoId+'&tab=HEP_Promotion_Products';
                }
            // Method to clear search field
            $scope.clearSearch = function() {
               $('.hep-search-catalog').val('');
           }
           
            // Object to broadcast to Navigation Component to show Back Button
            $scope. displayBackButton={
                     redirectionPage:'HEP_Promotion_Details?promoId='+$scope.sPromoId+'&tab=HEP_Promotion_Products',
                     displayText:"Back To Promotion"
            };
            $rootScope.$broadcast('displayBackButton',$scope.displayBackButton);
            // Method to save catalog data
            $scope.saveCatalogData = function(){
                    if($scope.lstSelectedCatalogs.length > 0){
                       var promise = catalogFactory.saveCatalogData(angular.toJson($scope.lstSelectedCatalogs),$scope.sPromoId,$scope.sCatalogTerritory);
                       promise.then(function(obj){
                           $scope.lstDuplicateCatalogs = angular.copy(obj);
                            if($scope.lstDuplicateCatalogs.length>0){
                                $scope.showDuplicateCatalogModal = true;
                            }
                            else{
                                window.location.href = '/apex/HEP_Promotion_Details?promoId='+$scope.sPromoId+'&tab=HEP_Promotion_Products'
                            }
                            return obj;
                       },
                       function(reason){
                           alert('Error : ' + reason.message);
                            throw reason;
                       },
                       function(message){});
                       return promise;
                    }
                    else{
                         toastr.options.positionClass = "toast-top-center";
                         toastr.options.timeOut = 2000;
                         toastr.options.preventDuplicates = true;
                         toastr.error('Please Add Atleast 1 Catalogs in Cart to proceed');
                    }
            }
            
               
            // Method to handle search functionality
             $scope.callSearchCatalog = function(event,value){
                if(value !== undefined && value != ''){
                    if(event.keyCode == 13)
                    {   var item = {sKey:value,sValue:value};
                        $scope.getCatalogData(item);
                    }
                }
             }
            // Method for searching catalog
            $scope.searchCatalog = function(viewValue){
               var promise = catalogFactory.searchCatalog(viewValue,$scope.sCatalogTerritory);
               promise.then(function(obj){
                hideShowProgress(true,  false);
                    return obj;
                },
                function(reason){
                    alert('Error : ' + reason.message);
                    hideShowProgress(false,  false);
                            throw reason;
                },
                function(message){});
                return promise;
           }
           // Method to format the view value
            $scope.formatViewValue = function($event){
              var searchValue = $event.originalEvent.clipboardData.getData('text/plain');
             
               if(searchValue.search("\n") != -1){
                    while((searchValue.indexOf("\n") != -1)&&(searchValue.indexOf("\n") != searchValue.length - 1)){
                     searchValue = searchValue.replace("\n",",");
                   }
               }

              $event.preventDefault();
              $scope.searchCatalogValue = searchValue;
              $scope.gridOptionsCatalog.api.redrawRows();
            }

            // Method for retrieving catalogs
           $scope.getCatalogData = function(item){
               var promise = catalogFactory.getCatalogData(item.sKey,$scope.sCatalogTerritory);
               promise.then(function(obj){
                   $scope.catalogData = angular.copy(obj.lstCatalog);
                   if(obj.sNoRecord != ''){
                         toastr.options.positionClass = "toast-top-center";
                         toastr.options.timeOut = 2000;
                         toastr.options.preventDuplicates = true;
                         toastr.error('No Record Found For - '+obj.sNoRecord);
                   }
                   $scope.gridOptionsCatalog.api.setRowData($scope.catalogData);
                   
                   $scope.gridOptionsCatalog.api.forEachNode( function (node) {
                        if ($scope.lstId.indexOf(node.data.sCatalogId ) != -1) {
                            node.setSelected(true);
                        }
                        else{
                            node.setSelected(false);
                        }
                    });
                    $scope.gridOptionsCatalog.api.sizeColumnsToFit();

                },
                function(reason){
                    alert('Error : ' + reason.message);
                    hideShowProgress(false,  false);
                            throw reason;
                },
                function(message){});
           }    

        var columnDefs = [
        {headerName: "Catalog #",filter:"agSetColumnFilter", field: "sCatalog",
                filter: "agTextColumnFilter",
        menuTabs:['filterMenuTab','generalMenuTab'],
                checkboxSelection: true,
                headerCheckboxSelection: true,
                headerCheckboxSelectionFilteredOnly: true,cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }},
        {headerName: "Catalog Name", field: "sCatalogName",filter:"agSetColumnFilter",
        width: 200,
        menuTabs:['filterMenuTab','generalMenuTab'],
        cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }},
        {headerName: "Catalog Type", field: "sType",filter:"agSetColumnFilter",
        menuTabs:['filterMenuTab','generalMenuTab'],
        cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }},
        {headerName: "WPR ID", field: "sWPRId",filter:"agSetColumnFilter",
        menuTabs:['filterMenuTab','generalMenuTab'],
        cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }},
        {headerName: "Licensor Type", field: "sLicensorType",filter:"agSetColumnFilter",
        menuTabs:['filterMenuTab','generalMenuTab'],
        cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }},
        {headerName: "Region", field: "sRegion",filter:"agSetColumnFilter",
        menuTabs:['filterMenuTab','generalMenuTab'],
        cellRenderer: function(params){
                        if(params.value=="" || params.value==null)
                                return '-';
                        else return params.value;
                    }}
        ];
        //grid options defining the ag-grid
        $scope.gridOptionsCatalog  = {
            defaultColDef: {
                checkboxSelection: isFirstColumn,
                headerCheckboxSelection: isFirstColumn,
                filterParams: {clearButton:true}
            },
            domLayout: 'autoHeight',
            rowHeight: 40,
            rowBuffer:10000,
            enableFilter: true,
            enableSorting: true,
            enableColResize:true,
            suppressRowClickSelection: true,
            toolPanelSuppressSideButtons: true,
            rowSelection: 'multiple',
            columnDefs: columnDefs,
            suppressLoadingOverlay: true,
            angularCompileRows: true,
            suppressHorizontalScroll: true,
            onRowSelected: onRowSelected,
            pagination: true,
            paginationPageSize: 25,
            suppressMovableColumns: false,
            onSelectionChanged : onSelectionChanged,
            onGridReady: function() {
                $scope.gridOptionsCatalog.api.hideOverlay();
                $scope.gridOptionsCatalog.api.sizeColumnsToFit();
            }
        }
        // Handle the checkbox selection for first row of data
        function isFirstColumn(params) {
            var displayedColumns = params.columnApi.getAllDisplayedColumns();
            var thisIsFirstColumn = displayedColumns[0] === params.column;
            return thisIsFirstColumn;
        }
        
        
        // Method for handling row selection
        function onRowSelected(event) {
                if(event.node.selected){
                       if($scope.lstId.indexOf(event.node.data.sCatalogId) != -1){
                       } 
                       else{
                           $scope.lstId.push(event.node.data.sCatalogId);
                           $scope.lstSelectedCatalogs.push(event.node.data);
                       
                           $scope.selectedRowsCount = $scope.lstSelectedCatalogs.length;
                       }
                }
                else if(!event.node.selected){
                    if($scope.lstId.indexOf(event.node.data.sCatalogId) != -1){
                         $scope.lstSelectedCatalogs.splice($scope.lstId.indexOf(event.node.data.sCatalogId),1);
                         $scope.lstId.splice($scope.lstId.indexOf(event.node.data.sCatalogId),1);
                        }
                      $scope.selectedRowsCount = $scope.lstSelectedCatalogs.length;  
                }

                
        }
        // Method to update the view on catalog selection
        function onSelectionChanged(event) {
            $scope.gridOptionsCatalog.api.redrawRows();
        }
        // Method to update the view on deselecting catalogs
        $scope.uncheckPromotion = function (catalog) {
             $scope.gridOptionsCatalog.api.forEachNode( function (node) {
                if (node.data.sCatalogId === catalog.sCatalogId ) {
                    node.setSelected(false);
                   
                }
            });
            if($scope.lstId.indexOf(catalog.sCatalogId) != -1){
             $scope.lstSelectedCatalogs.splice($scope.lstId.indexOf(catalog.sCatalogId),1);
             $scope.lstId.splice($scope.lstId.indexOf(catalog.sCatalogId),1);
            }
            $scope.selectedRowsCount = $scope.lstSelectedCatalogs.length;
             $scope.gridOptionsCatalog.api.refreshCells();

        }
        // For responsive horizontal scrolling
        fetchWindowSize();
        $scope.loadData();
        $(window).resize(function() {
            $timeout(function() {
            fetchWindowSize();
            });
        });
    }
    </script>
</apex:page>