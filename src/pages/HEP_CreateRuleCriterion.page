<!-- 
 * Page Name    : HEP_CreateRuleCriterion
 * Description  : Rule Criteria Create/Edit Page.    
 * Created By   : Nidhin V K
 * Created On   : 07-26-2016

 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date        Modification ID      Description 
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Nidhin V K            07-26-2016        1000            Initial version
 *
 *
 -->
<apex:page standardcontroller="HEP_Rule_Criterion__c"
    extensions="HEP_RuleCriterionExtension">

    <apex:sectionHeader title="Rule Criteria {!$Label.HEP_LABEL_EDIT}"
        subtitle="{!HEP_Rule_Criterion__c.Name}" />
    <!-- Go Back Link -->
    <div class="ptBreadcrumb">
        &nbsp;«&nbsp;
        <apex:outputLink value="/apex/HEP_ViewRule?id={!HEP_Rule_Criterion__c.HEP_Rule__r.Id}">Back to : Rule
        </apex:outputLink>
    </div>
    <apex:form >
        <apex:pageMessages rendered="{!bShowError}" />
        <apex:pageBlock tabStyle="HEP_Rule_Criterion__c"
            title="{!$Label.HEP_CREATE} Rule Criteria" mode="edit" id="theBlock">

            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!save}" />
                <apex:commandButton action="{!cancel}" value="Cancel" />
            </apex:pageBlockButtons>

            <apex:pageBlockSection title="{!$Label.HEP_MAP_THE_FIELDS}"
                columns="3" collapsible="false">

                <!-- Show only if Rule Type is Task have to change
                    <apex:selectList id="recordTypeId" value="{!selectedVal}" label="{!JSENCODE($Label.HEP_Select_Type)}" size="1" rendered="{!bTaskType}">
                        <apex:selectOptions value="{!recordTypeSelectOptions}"/>
                        <apex:actionSupport action="{!enableDisableTaskField}" event="onchange" rerender="theBlock"/>
                    </apex:selectList>

                <apex:selectList label="{!$Label.HEP_TARGET_TASK_FIELD}"
                    id="taskFieldID"
                    value="{!HEP_Rule_Criterion__c.HEP_Target_Task_Field__c}" size="1"
                    rendered="{!bShowTask}">
                    <apex:selectOptions value="{!lstTaskFields}" />
                    <apex:actionSupport action="{!enableDisableTaskField}"
                        event="onchange" rerender="theBlock" />
                </apex:selectList>-->

                <apex:selectList label="{!$Label.HEP_SOURCE_SOBJECT_FIELD}"
                    id="sObjFieldID"
                    value="{!HEP_Rule_Criterion__c.HEP_Source_SObject_Field__c}" size="1"
                    rendered="{!bShowSourceField}">
                    <apex:selectOptions value="{!lstTargetObjectFields}" />
                </apex:selectList>

                <apex:inputField label="{!$Label.HEP_VALUE_TO_MAP}" id="mapValueID"
                    value="{!HEP_Rule_Criterion__c.HEP_Value_to_Map__c}"
                    rendered="{!bShowMapValue}" />

                <apex:inputText id="dueDateValId"
                    label="{!JSENCODE($Label.HEP_ADD_DAYS_WITH_TODAY)}"
                    value="{!sDaysToAdd}" rendered="{!bShowDueDate}" />

                <apex:inputField label="{!$Label.HEP_OPERATOR}" id="operatorID"
                    value="{!HEP_Rule_Criterion__c.HEP_Operator__c}"
                    rendered="{!bShowValueCompare}" />

                <apex:inputField label="{!$Label.HEP_VALUE_TO_COMPARE}" id="valueID"
                    value="{!HEP_Rule_Criterion__c.HEP_Value_To_Compare__c}"
                    rendered="{!bShowValueCompare}" />

            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>

</apex:page>