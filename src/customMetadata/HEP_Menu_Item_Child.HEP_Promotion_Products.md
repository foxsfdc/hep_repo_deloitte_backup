<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Products</label>
    <protected>false</protected>
    <values>
        <field>Column_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Filter_Criteria__c</field>
        <value xsi:type="xsd:string">Global New Release; Global TV; Global Catalog; National New Release; National TV; National Catalog</value>
    </values>
    <values>
        <field>HEP_Menu_Item__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Number_of_Records__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Parent__c</field>
        <value xsi:type="xsd:string">HEP_Promotion_Details</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Sub Tabs</value>
    </values>
    <values>
        <field>URL_Param_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>VF_Page__c</field>
        <value xsi:type="xsd:string">HEP_Promotion_Products</value>
    </values>
</CustomMetadata>
