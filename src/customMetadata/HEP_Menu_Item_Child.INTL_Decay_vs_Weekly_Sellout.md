<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>INTL Decay vs. Weekly Sellout</label>
    <protected>false</protected>
    <values>
        <field>Column_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">INTL Decay vs. Weekly Sellout</value>
    </values>
    <values>
        <field>Filter_Criteria__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>HEP_Menu_Item__c</field>
        <value xsi:type="xsd:string">Reports</value>
    </values>
    <values>
        <field>Number_of_Records__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Parent__c</field>
        <value xsi:type="xsd:string">WW_Sales_Tracking</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Sub-Grouping</value>
    </values>
    <values>
        <field>URL_Param_Type__c</field>
        <value xsi:type="xsd:string">External URL</value>
    </values>
    <values>
        <field>VF_Page__c</field>
        <value xsi:type="xsd:string">https://app.box.com/embed/preview/295eng8rqwf2e65jh1o4m0r9ez60yp2i?theme=dark</value>
    </values>
</CustomMetadata>
