<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DD-MM-YYYY</label>
    <protected>false</protected>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Date Format</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">dd-MMM-yyyy, dd-MM-yyyy, dd/MM/yyyy, yyyy-MM-dd</value>
    </values>
</CustomMetadata>
