<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>HEP_Promotions</label>
    <protected>false</protected>
    <values>
        <field>Grouping__c</field>
        <value xsi:type="xsd:string">Release_Schedules,DWA_Reports,WW_Ultimates,TV_Reports,WW_Sales_Tracking,DHE_Sales_Tracking,FY_Budget,Catalog_Reports</value>
    </values>
    <values>
        <field>Menu_Item__c</field>
        <value xsi:type="xsd:string">My_Approvals,Promotions,Reports,Search</value>
    </values>
    <values>
        <field>Menu_Standalone__c</field>
        <value xsi:type="xsd:string">Approvals,Promotions,Search</value>
    </values>
    <values>
        <field>Menu_Sub_Grouping__c</field>
        <value xsi:type="xsd:string">Fox_Release_Schedule,INTL_Release_Schedule_Trade,INTL_Release_Schedule_Internal,Competitive_Release_Schedule,Consolidated_Release_Report,UpNext,Release_Change_Tracker,Domestic_Ultimates,INTL_FY_Budget,DWA_International,INTL_Weekly_DHD_Tracking_vs_U,WW_Monthly_Ultimate_Summary,INTL_Sellout_BD_Share,INTL_Weekly_Decay_DIG,DWA_Domestic.DHE_Combined_DHD_and_VOD_Track,WW_Monday_Morning_Reports,DHE_Combined_Exec_Summary,Quarterly_TV_Ultimate,Monthly_TV_WW_MFE_Dashboard,DHE_Combined_VOD_Tracking,DWA_Digital_Film_Report,DHE_Combined_DHD_Tracking,WW_Weekly_CSO,DHE_Mike_Dunn_Report,Catalog_Marketflash_Report,Sales_Tracking,DWA_HV_Disc_Report,INTL_Decay_vs_Weekly_Sellout,Catalog_International_Rolling,DHE_FY_Budget,Catalog_WW_Star_Wars</value>
    </values>
</CustomMetadata>
