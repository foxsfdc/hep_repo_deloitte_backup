<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SEK</label>
    <protected>false</protected>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Currency Format</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">{&quot;sSymbol&quot;: &apos;kr&apos;,&quot;sPlacement&quot;: &apos;suffix&apos;,&quot;iNoOfDecimalPlaces&quot;: 2}</value>
    </values>
</CustomMetadata>
